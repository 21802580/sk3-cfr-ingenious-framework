
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class MoveFileReader {


	public static void main(String[] args){
		long startTime = System.currentTimeMillis();
        readMoveFile();
        System.out.println(System.currentTimeMillis() - startTime);
	}	
	
	public static void readMoveFile() {
        int[] powers = new int[8];
        for (int i = 0; i < 8; i++) {
            powers[i] = (int) Math.pow(3, i);
        }

        Scanner s = null;
        try {
            s = new Scanner(new File("moves.txt"));
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        while (s.hasNext()) {
            int player = s.nextInt();
            int length = s.nextInt();
            int index = s.nextInt();
            int src = s.nextInt();
            int dest = s.nextInt();
            boolean capture = s.nextBoolean();
        }
        s.close();
    }
}
