package loa.logic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;

import datastructure.RandomSet;
import statics.Constants;
import statics.Data;

public class MoveTable {

	// Indices into the move table for each horizontal, vertical and diagonal line
	private int[] horizontal;
	private int[] vertical;
	private int[] positiveDiagonal;
	private int[] negativeDiagonal;
	

	public MoveTable(){
		horizontal = new int[8];
		vertical = new int[8];
		positiveDiagonal = new int[15];
		negativeDiagonal = new int[15];
		populateLines();
	}
	
	public MoveTable(MoveTable mt){
		horizontal = new int[8];
		vertical = new int[8];
		positiveDiagonal = new int[15];
		negativeDiagonal = new int[15];
		
		for (int i = 0; i < 15; i++){
			if (i < 8){
				horizontal[i] = mt.horizontal[i];
				vertical[i] = mt.vertical[i];
			}
			positiveDiagonal[i] = mt.positiveDiagonal[i];
			negativeDiagonal[i] = mt.negativeDiagonal[i];
		}
	}
	
	private int get_pow(int exp){
		return Data.powers[exp];
	}

	private void populateLines(){
		for (int i = 1; i < 7; i++){
			horizontal[0] += Constants.PIECE_BLACK * get_pow(i);
			horizontal[7] += Constants.PIECE_BLACK * get_pow(i);
			vertical[0] += Constants.PIECE_WHITE * get_pow(i);
			vertical[7] += Constants.PIECE_WHITE * get_pow(i);

			horizontal[i] = Constants.PIECE_WHITE * (get_pow(0) + get_pow(7));
			vertical[i] = Constants.PIECE_BLACK * (get_pow(0) + get_pow(7));
		}
		for (int i = 1; i < 7; i++){
			positiveDiagonal[i] = (Constants.PIECE_WHITE * get_pow(0)) + (Constants.PIECE_BLACK * get_pow(i));
			negativeDiagonal[i] = (Constants.PIECE_BLACK * get_pow(0)) + (Constants.PIECE_WHITE * get_pow(i));
		}
		int cnt = 1;
		for (int i = 8; i < 15; i++){
			positiveDiagonal[i] = (Constants.PIECE_BLACK * get_pow(0)) + (Constants.PIECE_WHITE * get_pow(7 - cnt));
			negativeDiagonal[i] = (Constants.PIECE_WHITE * get_pow(0)) + (Constants.PIECE_BLACK * get_pow(7 - cnt));
			cnt++;
		}
	}

	public void makeMove(Move m, int player){
		Coord src = m.getFrom();
		Coord dest = m.getTo();
		int rf = src.getRow();
		int cf = src.getCol();
		int rt = dest.getRow();
		int ct = dest.getCol();
		boolean capture = m.isCapture();

		horizontal[rf] -= player * get_pow(cf);
		horizontal[rt] += player * get_pow(ct);
		vertical[cf] -= player * get_pow(rf);
		vertical[ct] += player * get_pow(rt);

		if (rf + cf <= 7){
			positiveDiagonal[rf + cf] -= player * get_pow(cf);
		} else {
			positiveDiagonal[rf + cf] -= player * get_pow(7 - rf);
		}
		if (rt + ct <= 7){
			positiveDiagonal[rt + ct] += player * get_pow(ct);
		} else {
			positiveDiagonal[rt + ct] += player * get_pow(7 - rt);
		}

		if (cf - rf + 7 <= 7){
			negativeDiagonal[cf - rf + 7] -= player * get_pow(7 - rf);
		} else {
			negativeDiagonal[cf - rf + 7] -= player * get_pow(7 - cf);
		}
		if (ct - rt + 7 <= 7){
			negativeDiagonal[ct - rt + 7] += player * get_pow(7 - rt);
		} else {
			negativeDiagonal[ct - rt + 7] += player * get_pow(7 - ct);
		}
		

		if (capture){
			int opponent = Constants.PIECE_BLACK;
			if (player == Constants.PIECE_BLACK){
				opponent = Constants.PIECE_WHITE;
			}
			horizontal[rt] -= opponent * get_pow(ct);
			vertical[ct] -= opponent * get_pow(rt);
			if (rt + ct <= 7){
				positiveDiagonal[rt + ct] -= opponent * get_pow(ct);
			} else {
				positiveDiagonal[rt + ct] -= opponent * get_pow(7 - rt);
			}
			if (ct - rt + 7 <= 7){
				negativeDiagonal[ct - rt + 7] -= opponent * get_pow(7 - rt);
			} else {
				negativeDiagonal[ct - rt + 7] -= opponent * get_pow(7 - ct);
			}
		}
	}

	public void undoMove(Move m, int player){
		Coord src = m.getTo();
		Coord dest = m.getFrom();
		int rf = src.getRow();
		int cf = src.getCol();
		int rt = dest.getRow();
		int ct = dest.getCol();
		boolean capture = m.isCapture();

		horizontal[rf] -= player * get_pow(cf);
		horizontal[rt] += player * get_pow(ct);
		vertical[cf] -= player * get_pow(rf);
		vertical[ct] += player * get_pow(rt);

		if (rf + cf <= 7){
			positiveDiagonal[rf + cf] -= player * get_pow(cf);
		} else {
			positiveDiagonal[rf + cf] -= player * get_pow(7 - rf);
		}
		if (rt + ct <= 7){
			positiveDiagonal[rt + ct] += player * get_pow(ct);
		} else {
			positiveDiagonal[rt + ct] += player * get_pow(7 - rt);
		}

		if (cf - rf + 7 <= 7){
			negativeDiagonal[cf - rf + 7] -= player * get_pow(7 - rf);
		} else {
			negativeDiagonal[cf - rf + 7] -= player * get_pow(7 - cf);
		}
		if (ct - rt + 7 <= 7){
			negativeDiagonal[ct - rt + 7] += player * get_pow(7 - rt);
		} else {
			negativeDiagonal[ct - rt + 7] += player * get_pow(7 - ct);
		}

		if (capture){
			int opponent = Constants.PIECE_BLACK;
			if (player == Constants.PIECE_BLACK){
				opponent = Constants.PIECE_WHITE;
			}
			horizontal[rf] += opponent * get_pow(cf);
			vertical[cf] += opponent * get_pow(rf);
			if (rf + cf <= 7){
				positiveDiagonal[rf + cf] += opponent * get_pow(cf);
			} else {
				positiveDiagonal[rf + cf] += opponent * get_pow(7 - rf);
			}
			if (cf - rf + 7 <= 7){
				negativeDiagonal[cf - rf + 7] += opponent * get_pow(7 - rf);
			} else {
				negativeDiagonal[cf - rf + 7] += opponent * get_pow(7 - cf);
			}
		}
	}
	
	public Move getRandomMove(RandomSet<Coord> pieces, int player){
		Random r = new Random();
		
		ArrayList<HashMap<Integer, ArrayList<LineMove>>> map = null;
		if (player == Constants.PIECE_BLACK){
			map = Data.blackMoves;
		} else {
			map = Data.whiteMoves;
		}
		
		for (int i = 0; i < 3; i++){
			Coord c = pieces.peekRandom(r);
			int dir = r.nextInt(4);
			int row = c.getRow();
			int col = c.getCol();
			if (dir == 0){
				// Horizontal
				int ind = horizontal[row];
				ArrayList<LineMove> lineMoves = map.get(8).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
					Coord src = Data.coords[row][d.getSource()];
					Coord dest = Data.coords[row][d.getDestination()];
					Move m = new Move(src, dest, d.isCapture());
					return m;
				}
			} else if (dir == 1){
				// Vertical
				int ind = vertical[col];
				ArrayList<LineMove> lineMoves = map.get(8).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
					Coord src = Data.coords[d.getSource()][col];
					Coord dest = Data.coords[d.getDestination()][col];
					Move m = new Move(src, dest, d.isCapture());
					return m;
				}
			} else if (dir == 2){
				// Positive diagonal
				int ind = positiveDiagonal[row + col];
				int lineLength = 0;
				if (row + col <= 7){
					lineLength = row + col + 1;
				} else {
					lineLength = 15 - (row + col);
				}
				ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
					Coord src = null;
					Coord dest = null;
					if (row + col <= 7){
						src = Data.coords[row + col - d.getSource()][d.getSource()];
						dest = Data.coords[row + col - d.getDestination()][d.getDestination()];
					} else {
						src = Data.coords[7 - d.getSource()][row + col - 7 + d.getSource()];
						dest = Data.coords[7 - d.getDestination()][row + col - 7 + d.getDestination()];
					}
					Move m = new Move(src, dest, d.isCapture());
					return m;
				}
			} else {
				// Negative diagonal
				int ind = negativeDiagonal[col - row + 7];
				int lineLength = 0;
				if (col - row + 7 <= 7){
					lineLength = col - row + 8;
				} else {
					lineLength = 15 - (col - row + 7);
				}
				ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
					Coord src = null;
					Coord dest = null;
					if (col - row + 7 <= 7){
						src = Data.coords[7 - d.getSource()][col - row + 7 - d.getSource()];
						dest = Data.coords[7 - d.getDestination()][col - row + 7 - d.getDestination()];
					} else {
						src = Data.coords[7 - (col - row) - d.getSource()][7 - d.getSource()];
						dest = Data.coords[7 - (col - row) - d.getDestination()][7 - d.getDestination()];
					}
					Move m = new Move(src, dest, d.isCapture());
					return m;
				}
			}
		}
		return getRandomMoveExhaustive(pieces, player);
	}

	public Move getRandomMoveExhaustive(RandomSet<Coord> pieces, int player){
		// Sets to keep track of indices which have already been checked
		HashSet<Integer> hor = new HashSet<Integer>();
		HashSet<Integer> ver = new HashSet<Integer>();
		HashSet<Integer> pd = new HashSet<Integer>();
		HashSet<Integer> nd = new HashSet<Integer>();

		ArrayList<HashMap<Integer, ArrayList<LineMove>>> map = null;
		if (player == Constants.PIECE_BLACK){
			map = Data.blackMoves;
		} else {
			map = Data.whiteMoves;
		}

		ArrayList<Integer> directionOrder = new ArrayList<Integer>(4);
		for (int i = 0; i < 4; i++){
			directionOrder.add(i);
		}
		Collections.shuffle(directionOrder);
		
		Random r = new Random();
		for (Coord c : pieces){
			int row = c.getRow();
			int col = c.getCol();
			for (int i : directionOrder){
				if (!hor.contains(row) && i == 0){
					hor.add(row);
					int ind = horizontal[row];
					ArrayList<LineMove> lineMoves = map.get(8).get(ind);
					if ((lineMoves != null) && (!lineMoves.isEmpty())){
						LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
						Coord src = Data.coords[row][d.getSource()];
						Coord dest = Data.coords[row][d.getDestination()];
						Move m = new Move(src, dest, d.isCapture());
						return m;
					}
				}
				if (!ver.contains(col) && i == 1){
					ver.add(col);
					int ind = vertical[col];
					ArrayList<LineMove> lineMoves = map.get(8).get(ind);
					if ((lineMoves != null) && (!lineMoves.isEmpty())){
						LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
						Coord src = Data.coords[d.getSource()][col];
						Coord dest = Data.coords[d.getDestination()][col];
						Move m = new Move(src, dest, d.isCapture());
						return m;
					}
				}
				if (!pd.contains(row + col) && i == 2){
					pd.add(row + col);
					int ind = positiveDiagonal[row + col];
					int lineLength = 0;
					if (row + col <= 7){
						lineLength = row + col + 1;
					} else {
						lineLength = 15 - (row + col);
					}
					ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
					if ((lineMoves != null) && (!lineMoves.isEmpty())){
						LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
						Coord src = null;
						Coord dest = null;
						if (row + col <= 7){
							src = Data.coords[row + col - d.getSource()][d.getSource()];
							dest = Data.coords[row + col - d.getDestination()][d.getDestination()];
						} else {
							src = Data.coords[7 - d.getSource()][row + col - 7 + d.getSource()];
							dest = Data.coords[7 - d.getDestination()][row + col - 7 + d.getDestination()];
						}
						Move m = new Move(src, dest, d.isCapture());
						return m;
					}
				}
				if (!nd.contains(col - row + 7) && i == 3){
					nd.add(col - row + 7);
					int ind = negativeDiagonal[col - row + 7];
					int lineLength = 0;
					if (col - row + 7 <= 7){
						lineLength = col - row + 8;
					} else {
						lineLength = 15 - (col - row + 7);
					}
					ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
					if ((lineMoves != null) && (!lineMoves.isEmpty())){
						LineMove d = lineMoves.get(r.nextInt(lineMoves.size()));
						Coord src = null;
						Coord dest = null;
						if (col - row + 7 <= 7){
							src = Data.coords[7 - d.getSource()][col - row + 7 - d.getSource()];
							dest = Data.coords[7 - d.getDestination()][col - row + 7 - d.getDestination()];
						} else {
							src = Data.coords[7 - (col - row) - d.getSource()][7 - d.getSource()];
							dest = Data.coords[7 - (col - row) - d.getDestination()][7 - d.getDestination()];
						}
						Move m = new Move(src, dest, d.isCapture());
						return m;
					}
				}
			}
		}
		return null;
	}

	public RandomSet<Move> getAllMoves(RandomSet<Coord> pieces, int player){
		RandomSet<Move> moves = new RandomSet<Move>();

		// Sets to keep track of indices which have already been checked
		HashSet<Integer> hor = new HashSet<Integer>();
		HashSet<Integer> ver = new HashSet<Integer>();
		HashSet<Integer> pd = new HashSet<Integer>();
		HashSet<Integer> nd = new HashSet<Integer>();
		
		ArrayList<HashMap<Integer, ArrayList<LineMove>>> map = null;
		if (player == Constants.PIECE_BLACK){
			map = Data.blackMoves;
		} else {
			map = Data.whiteMoves;
		}

		for (Coord c : pieces){
			int row = c.getRow();
			int col = c.getCol();
			if (!hor.contains(row)){
				hor.add(row);
				int ind = horizontal[row];
				ArrayList<LineMove> lineMoves = map.get(8).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					for (LineMove d : lineMoves){
						Coord src = Data.coords[row][d.getSource()];
						Coord dest = Data.coords[row][d.getDestination()];
						Move m = new Move(src, dest, d.isCapture());
						moves.add(m);
					}
				}
			}
			if (!ver.contains(col)){
				ver.add(col);
				int ind = vertical[col];
				ArrayList<LineMove> lineMoves = map.get(8).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					for (LineMove d : lineMoves){
						Coord src = Data.coords[d.getSource()][col];
						Coord dest = Data.coords[d.getDestination()][col];
						Move m = new Move(src, dest, d.isCapture());
						moves.add(m);
					}
				}
			}
			if (!pd.contains(row + col)){
				pd.add(row + col);
				int ind = positiveDiagonal[row + col];
				int lineLength = 0;
				if (row + col <= 7){
					lineLength = row + col + 1;
				} else {
					lineLength = 15 - (row + col);
				}
				ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					for (LineMove d : lineMoves){
						Coord src = null;
						Coord dest = null;
						if (row + col <= 7){
							src = Data.coords[row + col - d.getSource()][d.getSource()];
							dest = Data.coords[row + col - d.getDestination()][d.getDestination()];
						} else {
							src = Data.coords[7 - d.getSource()][row + col - 7 + d.getSource()];
							dest = Data.coords[7 - d.getDestination()][row + col - 7 + d.getDestination()];
						}
						Move m = new Move(src, dest, d.isCapture());
						moves.add(m);
					}
				}
			}
			if (!nd.contains(col - row + 7)){
				nd.add(col - row + 7);
				int ind = negativeDiagonal[col - row + 7];
				int lineLength = 0;
				if (col - row + 7 <= 7){
					lineLength = col - row + 8;
				} else {
					lineLength = 15 - (col - row + 7);
				}
				ArrayList<LineMove> lineMoves = map.get(lineLength).get(ind);
				if ((lineMoves != null) && (!lineMoves.isEmpty())){
					for (LineMove d : lineMoves){
						Coord src = null;
						Coord dest = null;
						if (col - row + 7 <= 7){
							src = Data.coords[7 - d.getSource()][col - row + 7 - d.getSource()];
							dest = Data.coords[7 - d.getDestination()][col - row + 7 - d.getDestination()];
						} else {
							src = Data.coords[7 - (col - row) - d.getSource()][7 - d.getSource()];
							dest = Data.coords[7 - (col - row) - d.getDestination()][7 - d.getDestination()];
						}
						Move m = new Move(src, dest, d.isCapture());
						moves.add(m);
					}
				}
			}
		}
		return moves;
	}

//	private void populateTable(){
//		for (int i = 0; i <= 8; i++){
//			blackMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
//			whiteMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
//		}
//
//		Scanner s = null;
//		try {
//			s = new Scanner(new File("moves.txt"));
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		while (s.hasNext()){
//			int player = s.nextInt();
//			int length = s.nextInt();
//			int index = s.nextInt();
//			int src = s.nextInt();
//			int dest = s.nextInt();
//			boolean capture = s.nextBoolean();
//			if (player == Constants.PIECE_BLACK){
//				HashMap<Integer, ArrayList<LineMove>> hm = blackMoves.get(length);
//				ArrayList<LineMove> moves = hm.get(index);
//				if (moves == null){
//					moves = new ArrayList<LineMove>();
//					hm.put(index, moves);
//				}
//				moves.add(new LineMove(src, dest, capture));
//			} else {
//				HashMap<Integer, ArrayList<LineMove>> hm = whiteMoves.get(length);
//				ArrayList<LineMove> moves = hm.get(index);
//				if (moves == null){
//					moves = new ArrayList<LineMove>();
//					hm.put(index, moves);
//				}
//				moves.add(new LineMove(src, dest, capture));
//			}
//		}
//		s.close();
//	}
}
