package loa.logic;

import statics.Constants;
import statics.Data.QuadType;

public class QuadTable {
	public QuadType[][] whiteQuads;
	public QuadType[][] blackQuads;
	
	public int[] whiteTypes;
	public int[] blackTypes;
	
//	public static void main(String[] args){
//		QuadTable t = new QuadTable();
//		QuadTable t2 = new QuadTable(t);
//		System.out.println(t.blackQuads[0][1]);
//		t.applyMove(new Move(new Coord(0, 1), new Coord(2, 1), false), Constants.PIECE_BLACK);
//		System.out.println(t.blackQuads[0][1]);
//		System.out.println(t2.blackQuads[0][1]);
////		for (int i = 0; i < 6; i++){
////			System.out.println(i + ": " + t.blackTypes[i]);
////		}
////		System.out.println(t.getEuler(Constants.PIECE_WHITE));
////		System.out.println();
////		t.applyMove(new Move(new Coord(0, 2), new Coord (2, 2), false), Constants.PIECE_BLACK);
////		t.undoMove(new Move(new Coord(0, 2), new Coord (2, 2), false), Constants.PIECE_BLACK);
////		for (int i = 0; i < 6; i++){
////			System.out.println(i + ": " + t.blackTypes[i]);
////		}
////		System.out.println(t.getEuler(Constants.PIECE_WHITE));
//	}

	public QuadTable(){
		whiteQuads = new QuadType[9][9];
		blackQuads = new QuadType[9][9];
		whiteTypes = new int[6];
		blackTypes = new int[6];
		populateBlack();
		populateWhite();
	}
	
	public QuadTable(QuadTable q){
		whiteQuads = new QuadType[9][9];
		blackQuads = new QuadType[9][9];
		whiteTypes = new int[6];
		blackTypes = new int[6];
		for (int i = 0; i < 9; i++){
			for (int j = 0; j < 9; j++){
				whiteQuads[i][j] = q.whiteQuads[i][j];
				blackQuads[i][j] = q.blackQuads[i][j];
			}
			if (i < 6){
				blackTypes[i] = q.blackTypes[i];
				whiteTypes[i] = q.whiteTypes[i];
			}
		}
	}

	public void applyMove(Move m, int colour){
		QuadType[][] myQuads = blackQuads;
		QuadType[][] opponentQuads = whiteQuads;
		int[] myTypes = blackTypes;
		int[] opponentTypes = whiteTypes;
		if (colour == Constants.PIECE_WHITE){
			myQuads = whiteQuads;
			opponentQuads = blackQuads;
			myTypes = whiteTypes;
			opponentTypes = blackTypes;
		}
		
		int srcRow = m.getFrom().getRow();
		int srcCol = m.getFrom().getCol();
		int destRow = m.getTo().getRow();
		int destCol = m.getTo().getCol();
		boolean capture = m.isCapture();
		
		int currentPosition = 3;
		for (int r = srcRow; r < srcRow + 2; r++){
			for (int c = srcCol; c < srcCol + 2; c++){
				myTypes[myQuads[r][c].getCategory()]--;
				myQuads[r][c] = Quad.removePiece(myQuads[r][c], currentPosition);
				myTypes[myQuads[r][c].getCategory()]++;
				currentPosition--;
			}
		}
		
		currentPosition = 3;
		for (int r = destRow; r < destRow + 2; r++){
			for (int c = destCol; c < destCol + 2; c++){
				myTypes[myQuads[r][c].getCategory()]--;
				myQuads[r][c] = Quad.addPiece(myQuads[r][c], currentPosition);
				myTypes[myQuads[r][c].getCategory()]++;
				currentPosition--;
			}
		}
		
		if (capture){
			currentPosition = 3;
			for (int r = destRow; r < destRow + 2; r++){
				for (int c = destCol; c < destCol + 2; c++){
					opponentTypes[opponentQuads[r][c].getCategory()]--;
					opponentQuads[r][c] = Quad.removePiece(opponentQuads[r][c], currentPosition);
					opponentTypes[opponentQuads[r][c].getCategory()]++;
					currentPosition--;
				}
			}
		}
	}
	
	public void undoMove(Move m, int colour){
		QuadType[][] myQuads = blackQuads;
		QuadType[][] opponentQuads = whiteQuads;
		int[] myTypes = blackTypes;
		int[] opponentTypes = whiteTypes;
		if (colour == Constants.PIECE_WHITE){
			myQuads = whiteQuads;
			opponentQuads = blackQuads;
			myTypes = whiteTypes;
			opponentTypes = blackTypes;
		}
		
		int srcRow = m.getTo().getRow();
		int srcCol = m.getTo().getCol();
		int destRow = m.getFrom().getRow();
		int destCol = m.getFrom().getCol();
		boolean capture = m.isCapture();
		
		int currentPosition = 3;
		for (int r = srcRow; r < srcRow + 2; r++){
			for (int c = srcCol; c < srcCol + 2; c++){
				myTypes[myQuads[r][c].getCategory()]--;
				myQuads[r][c] = Quad.removePiece(myQuads[r][c], currentPosition);
				myTypes[myQuads[r][c].getCategory()]++;
				currentPosition--;
			}
		}
		
		currentPosition = 3;
		for (int r = destRow; r < destRow + 2; r++){
			for (int c = destCol; c < destCol + 2; c++){
				myTypes[myQuads[r][c].getCategory()]--;
				myQuads[r][c] = Quad.addPiece(myQuads[r][c], currentPosition);
				myTypes[myQuads[r][c].getCategory()]++;
				currentPosition--;
			}
		}
		
		if (capture){
			currentPosition = 3;
			for (int r = srcRow; r < srcRow + 2; r++){
				for (int c = srcCol; c < srcCol + 2; c++){
					opponentTypes[opponentQuads[r][c].getCategory()]--;
					opponentQuads[r][c] = Quad.addPiece(opponentQuads[r][c], currentPosition);
					opponentTypes[opponentQuads[r][c].getCategory()]++;
					currentPosition--;
				}
			}
		}
	}

	public double getEuler(int colour){
		if (colour == Constants.PIECE_BLACK){
			return ((double)blackTypes[1] - (double)blackTypes[3] - (2 * (double)blackTypes[5])) / 4.0;
		} else {
			return ((double)whiteTypes[1] - (double)whiteTypes[3] - (2 * (double)whiteTypes[5])) / 4.0;
		}
	}
	
	private void populateBlack(){
		blackQuads[0][0] = QuadType.EMPTY_0;
		blackQuads[0][1] = QuadType.BOTTOM_RIGHT_1;
		blackQuads[1][0] = QuadType.EMPTY_0;
		blackQuads[1][1] = QuadType.TOP_RIGHT_1;
		for (int i = 2; i < 7; i++){
			blackQuads[0][i] = QuadType.BOTTOM_2;
			blackQuads[1][i] = QuadType.TOP_2;
		}
		blackQuads[0][7] = QuadType.BOTTOM_LEFT_1;
		blackQuads[0][8] = QuadType.EMPTY_0;
		blackQuads[1][7] = QuadType.TOP_LEFT_1;
		blackQuads[1][8] = QuadType.EMPTY_0;
		
		for (int i = 2; i < 7; i++){
			for (int j = 0; j < 9; j++){
				blackQuads[i][j] = QuadType.EMPTY_0;
			}
		}
		
		blackQuads[7][0] = QuadType.EMPTY_0;
		blackQuads[7][1] = QuadType.BOTTOM_RIGHT_1;
		blackQuads[8][0] = QuadType.EMPTY_0;
		blackQuads[8][1] = QuadType.TOP_RIGHT_1;
		for (int i = 2; i < 7; i++){
			blackQuads[7][i] = QuadType.BOTTOM_2;
			blackQuads[8][i] = QuadType.TOP_2;
		}
		blackQuads[7][7] = QuadType.BOTTOM_LEFT_1;
		blackQuads[7][8] = QuadType.EMPTY_0;
		blackQuads[8][7] = QuadType.TOP_LEFT_1;
		blackQuads[8][8] = QuadType.EMPTY_0;
		
		blackTypes[0] = 53;
		blackTypes[1] = 8;
		blackTypes[2] = 20;
		blackTypes[3] = 0;
		blackTypes[4] = 0;
		blackTypes[5] = 0;
	}
	
	private void populateWhite(){
		whiteQuads[0][0] = QuadType.EMPTY_0;
		whiteQuads[1][0] = QuadType.BOTTOM_RIGHT_1;
		whiteQuads[0][1] = QuadType.EMPTY_0;
		whiteQuads[1][1] = QuadType.BOTTOM_LEFT_1;
		for (int i = 2; i < 7; i++){
			whiteQuads[i][0] = QuadType.RIGHT_2;
			whiteQuads[i][1] = QuadType.LEFT_2;
		}
		whiteQuads[7][0] = QuadType.TOP_RIGHT_1;
		whiteQuads[7][1] = QuadType.TOP_LEFT_1;
		whiteQuads[8][0] = QuadType.EMPTY_0;
		whiteQuads[8][1] = QuadType.EMPTY_0;
		
		for (int i = 0; i < 9; i++){
			for (int j = 2; j < 7; j++){
				whiteQuads[i][j] = QuadType.EMPTY_0;
			}
		}
		
		whiteQuads[0][7] = QuadType.EMPTY_0;
		whiteQuads[1][7] = QuadType.BOTTOM_RIGHT_1;
		whiteQuads[0][8] = QuadType.EMPTY_0;
		whiteQuads[1][8] = QuadType.BOTTOM_LEFT_1;
		for (int i = 2; i < 7; i++){
			whiteQuads[i][7] = QuadType.RIGHT_2;
			whiteQuads[i][8] = QuadType.LEFT_2;
		}
		whiteQuads[7][7] = QuadType.TOP_RIGHT_1;
		whiteQuads[7][8] = QuadType.TOP_LEFT_1;
		whiteQuads[8][7] = QuadType.EMPTY_0;
		whiteQuads[8][8] = QuadType.EMPTY_0;
		
		whiteTypes[0] = 53;
		whiteTypes[1] = 8;
		whiteTypes[2] = 20;
		whiteTypes[3] = 0;
		whiteTypes[4] = 0;
		
	
//	private void populateBlack(){
//		blackQuads[0][0] = new Quad(false, false, false, false);
//		blackQuads[0][1] = new Quad(false, false, false, true);
//		blackQuads[1][0] = new Quad(false, false, false, false);
//		blackQuads[1][1] = new Quad(false, true, false, false);
//		for (int i = 2; i < 7; i++){
//			blackQuads[0][i] = new Quad(false, false, true, true);
//			blackQuads[1][i] = new Quad(true, true, false, false);
//		}
//		blackQuads[0][7] = new Quad(false, false, true, false);
//		blackQuads[0][8] = new Quad(false, false, false, false);
//		blackQuads[1][7] = new Quad(true, false, false, false);
//		blackQuads[1][8] = new Quad(false, false, false, false);
//		
//		for (int i = 2; i < 7; i++){
//			for (int j = 0; j < 9; j++){
//				blackQuads[i][j] = new Quad(false, false, false, false);
//			}
//		}
//		
//		blackQuads[7][0] = new Quad(false, false, false, false);
//		blackQuads[7][1] = new Quad(false, false, false, true);
//		blackQuads[8][0] = new Quad(false, false, false, false);
//		blackQuads[8][1] = new Quad(false, true, false, false);
//		for (int i = 2; i < 7; i++){
//			blackQuads[7][i] = new Quad(false, false, true, true);
//			blackQuads[8][i] = new Quad(true, true, false, false);
//		}
//		blackQuads[7][7] = new Quad(false, false, true, false);
//		blackQuads[7][8] = new Quad(false, false, false, false);
//		blackQuads[8][7] = new Quad(true, false, false, false);
//		blackQuads[8][8] = new Quad(false, false, false, false);
//		
//		blackTypes[0] = 53;
//		blackTypes[1] = 8;
//		blackTypes[2] = 20;
//		blackTypes[3] = 0;
//		blackTypes[4] = 0;
//		blackTypes[5] = 0;
//	}
//	
//	private void populateWhite(){
//		whiteQuads[0][0] = new Quad(false, false, false, false);
//		whiteQuads[1][0] = new Quad(false, false, false, true);
//		whiteQuads[0][1] = new Quad(false, false, false, false);
//		whiteQuads[1][1] = new Quad(false, false, true, false);
//		for (int i = 2; i < 7; i++){
//			whiteQuads[i][0] = new Quad(false, true, false, true);
//			whiteQuads[i][1] = new Quad(true, false, true, false);
//		}
//		whiteQuads[7][0] = new Quad(false, true, false, false);
//		whiteQuads[7][1] = new Quad(true, false, false, false);
//		whiteQuads[8][0] = new Quad(false, false, false, false);
//		whiteQuads[8][1] = new Quad(false, false, false, false);
//		
//		for (int i = 0; i < 9; i++){
//			for (int j = 2; j < 7; j++){
//				whiteQuads[i][j] = new Quad(false, false, false, false);
//			}
//		}
//		
//		whiteQuads[0][7] = new Quad(false, false, false, false);
//		whiteQuads[1][7] = new Quad(false, false, false, true);
//		whiteQuads[0][8] = new Quad(false, false, false, false);
//		whiteQuads[1][8] = new Quad(false, false, true, false);
//		for (int i = 2; i < 7; i++){
//			whiteQuads[i][7] = new Quad(false, true, false, true);
//			whiteQuads[i][8] = new Quad(true, false, true, false);
//		}
//		whiteQuads[7][7] = new Quad(false, true, false, false);
//		whiteQuads[7][8] = new Quad(true, false, false, false);
//		whiteQuads[8][7] = new Quad(false, false, false, false);
//		whiteQuads[8][8] = new Quad(false, false, false, false);
//		
//		whiteTypes[0] = 53;
//		whiteTypes[1] = 8;
//		whiteTypes[2] = 20;
//		whiteTypes[3] = 0;
//		whiteTypes[4] = 0;
//		whiteTypes[5] = 0;
	}
}
