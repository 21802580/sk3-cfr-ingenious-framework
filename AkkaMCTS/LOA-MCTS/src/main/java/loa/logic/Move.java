package loa.logic;

import statics.Constants;
import statics.Data;

public class Move {
	private final Coord from;
	private final Coord to;
	private final boolean capture;
	private int moveCategory;

	@SuppressWarnings("unused")
	public Move(Coord from, Coord to, boolean capture){
		this.from = from;
		this.to = to;
		this.capture = capture;
		if (Constants.MOVE_CATEGORY_BIAS || Constants.MOVE_CATEGORY_UNPRUNING){
			makeMoveCategory();
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (capture ? 1231 : 1237);
		result = prime * result + ((from == null) ? 0 : from.hashCode());
		result = prime * result + ((to == null) ? 0 : to.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Move other = (Move) obj;
		if (capture != other.capture)
			return false;
		if (from == null) {
			if (other.from != null)
				return false;
		} else if (!from.equals(other.from))
			return false;
		if (to == null) {
			if (other.to != null)
				return false;
		} else if (!to.equals(other.to))
			return false;
		return true;
	}



	/*
	 * TODO: Find a neater way to do this
	 */
	public void makeMoveCategory(){
		Region fromRegion = getRegion(from);
		Region toRegion = getRegion(to);
		int mc = 0;
		if (fromRegion == Region.CENTRAL && toRegion == Region.CORNER){
			mc = 1;
		} else if (fromRegion == Region.INNER && toRegion == Region.CORNER){
			mc = 2;
		} else if (fromRegion == Region.OUTER && toRegion == Region.CORNER){
			mc = 3;
		} else if (fromRegion == Region.CORNER && toRegion == Region.CORNER){
			mc = 4;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.OUTER){
			mc = 5;
		} else if (fromRegion == Region.INNER && toRegion == Region.OUTER){
			mc = 6;
		} else if (fromRegion == Region.OUTER && toRegion == Region.OUTER){
			mc = 7;
		} else if (fromRegion == Region.CORNER && toRegion == Region.OUTER){
			mc = 8;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.INNER){
			mc = 9;
		} else if (fromRegion == Region.INNER && toRegion == Region.INNER){
			mc = 10;
		} else if (fromRegion == Region.OUTER && toRegion == Region.INNER){
			mc = 11;
		} else if (fromRegion == Region.CORNER && toRegion == Region.INNER){
			mc = 12;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.CENTRAL){
			mc = 13;
		} else if (fromRegion == Region.INNER && toRegion == Region.CENTRAL){
			mc = 14;
		} else if (fromRegion == Region.OUTER && toRegion == Region.CENTRAL){
			mc = 15;
		} else if (fromRegion == Region.CORNER && toRegion == Region.CENTRAL){
			mc = 16;
		}
		if (capture){
			mc += 16;
		}
		this.moveCategory = mc;
	}
	
	public double getTransitionProbability(){
		if (Data.categoryTable.hasCategory(moveCategory)){
			return Data.categoryTable.getTransitionProbability(moveCategory);
		} else {
			return 0.0;
		}
	}
	
	public int getMoveCategory(){
		return this.moveCategory;
	}
	
	private Region getRegion(Coord c){
		int row = c.getRow();
		int col = c.getCol();
		if ((row == 0 && col == 0) || (row == 0 && col == 7) || (row == 7 && col == 0) || (row == 7 && col == 7)){
			return Region.CORNER;
		} else if (row == 7 || row == 0 || col == 7 || col == 0){
			return Region.OUTER;
		} else if ((row == 3 && col == 3) || (row == 3 && col == 4) || (row == 4 && col == 3) || (row == 4 && col == 4)){
			return Region.CENTRAL;
		} else {
			return Region.INNER;
		}
	}

	public Coord getFrom() {
		return from;
	}

	public Coord getTo() {
		return to;
	}
	
	public boolean isCapture(){
		return this.capture;
	}
	
	public int getDirection(){
		int rf = from.getRow();
		int cf = from.getCol();
		int rt = to.getRow();
		int ct = to.getCol();
		int direction = Constants.UP;
		if ((cf == ct) && (rf < rt)){
			direction = Constants.DOWN;
		} else if ((cf < ct) && (rf == rt)){
			direction = Constants.RIGHT;
		} else if ((cf > ct) && (rf == rt)){
			direction = Constants.LEFT;
		} else if ((cf < ct) && (rf > rt)){
			direction = Constants.UP_RIGHT;
		} else if ((cf < ct) && (rf < rt)){
			direction = Constants.DOWN_RIGHT;
		} else if ((cf > ct) && (rf < rt)){
			direction = Constants.DOWN_LEFT;
		} else if ((cf > ct) && (rf > rt)){
			direction = Constants.UP_LEFT;
		}
		return direction;
	}

	public String toString(){
		return from.toString() + " -> " + to.toString();
	}
}
