package loa.logic;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class MoveCategoryTable {
	private HashMap<Integer, Double> transitionProbabilities;
	
	public MoveCategoryTable(){
		FileReader input = null;
		try {
			input = new FileReader("move_categories.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader buf = new BufferedReader(input);
		transitionProbabilities = new HashMap<Integer, Double>();
		populateTable(buf);
	}
	
	private void populateTable(BufferedReader buf){
		String line = null;
		try {
			while ((line = buf.readLine()) != null){
				String[] entry = line.split(":");
				int mc = Integer.parseInt(entry[0]);
				double pmc = Double.parseDouble(entry[1].trim());
				transitionProbabilities.put(mc, pmc);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public double getTransitionProbability(int category){
		return transitionProbabilities.get(category);
	}
	
	public boolean hasCategory(int category){
		if (transitionProbabilities.containsKey(category)){
			return true;
		}
		return false;
	}
}
