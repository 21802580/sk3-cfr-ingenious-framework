package loa.logic;

import statics.Data.QuadType;

public class Quad {
	public static void main(String[] args){
		System.out.println(Quad.addPiece(QuadType.BOTTOM_2, 1));
	}
	
	// Untidy but necessary to minimise object creation and memory footprint
	public static QuadType addPiece(QuadType q, int pos){
		QuadType type = null;
		switch (q) {
		case EMPTY_0:
			switch(pos){
			case 0: type = QuadType.TOP_LEFT_1; break;
			case 1: type = QuadType.TOP_RIGHT_1; break;
			case 2: type = QuadType.BOTTOM_LEFT_1; break;
			case 3: type = QuadType.BOTTOM_RIGHT_1; break;
			}
			break;	
		case TOP_LEFT_1:
			switch(pos){
			case 1: type = QuadType.TOP_2; break;
			case 2: type = QuadType.LEFT_2; break;
			case 3: type = QuadType.DIAG_NEG; break;
			}
			break;
		case TOP_RIGHT_1:
			switch(pos){
			case 0: type = QuadType.TOP_2; break;
			case 2: type = QuadType.DIAG_POS; break;
			case 3: type = QuadType.RIGHT_2; break;
			}
			break;
		case BOTTOM_LEFT_1:
			switch(pos){
			case 0: type = QuadType.LEFT_2; break;
			case 1: type = QuadType.DIAG_POS; break;
			case 3: type = QuadType.BOTTOM_2; break;
			}
			break;
		case BOTTOM_RIGHT_1:
			switch(pos){
			case 0: type = QuadType.DIAG_NEG; break;
			case 1: type = QuadType.RIGHT_2; break;
			case 2: type = QuadType.BOTTOM_2; break;
			}
			break;
		case TOP_2:
			switch(pos){
			case 2: type = QuadType.BOTTOM_RIGHT_3; break;
			case 3: type = QuadType.BOTTOM_LEFT_3; break;
			}
			break;
		case BOTTOM_2:
			switch(pos){
			case 0: type = QuadType.TOP_RIGHT_3; break;
			case 1: type = QuadType.TOP_LEFT_3; break;
			}
			break;
		case LEFT_2:
			switch(pos){
			case 1: type = QuadType.BOTTOM_RIGHT_3; break;
			case 3: type = QuadType.TOP_RIGHT_3; break;
			}
			break;
		case RIGHT_2:
			switch(pos){
			case 0: type = QuadType.BOTTOM_LEFT_3; break;
			case 2: type = QuadType.TOP_LEFT_3; break;
			}
			break;
		case DIAG_NEG:
			switch(pos){
			case 1: type = QuadType.BOTTOM_LEFT_3; break;
			case 2: type = QuadType.TOP_RIGHT_3; break;
			}
			break;
		case DIAG_POS:
			switch(pos){
			case 0: type = QuadType.BOTTOM_RIGHT_3; break;
			case 3: type = QuadType.TOP_LEFT_3; break;
			}
			break;
		default:
			if (q.getCategory() == 3){
				return QuadType.FULL_4;
			} else {
				System.out.println("ATTEMPTED TO ADD TO FULL QUAD");
			}
			break;
		}
		return type;
	}
	
	public static QuadType removePiece(QuadType q, int pos){
		QuadType type = null;
		switch (q) {
		case TOP_2:
			switch(pos){
			case 0: type = QuadType.TOP_RIGHT_1; break;
			case 1: type = QuadType.TOP_LEFT_1; break;
			}
			break;
		case BOTTOM_2:
			switch(pos){
			case 2: type = QuadType.BOTTOM_RIGHT_1; break;
			case 3: type = QuadType.BOTTOM_LEFT_1; break;
			}
			break;
		case LEFT_2:
			switch(pos){
			case 0: type = QuadType.BOTTOM_LEFT_1; break;
			case 2: type = QuadType.TOP_LEFT_1; break;
			}
			break;
		case RIGHT_2:
			switch(pos){
			case 1: type = QuadType.BOTTOM_RIGHT_1; break;
			case 3: type = QuadType.TOP_RIGHT_1; break;
			}
			break;
		case DIAG_NEG:
			switch(pos){
			case 0: type = QuadType.BOTTOM_RIGHT_1; break;
			case 3: type = QuadType.TOP_LEFT_1; break;
			}
			break;
		case DIAG_POS:
			switch(pos){
			case 1: type = QuadType.BOTTOM_LEFT_1; break;
			case 2: type = QuadType.TOP_RIGHT_1; break;
			}
			break;
		case TOP_LEFT_3:
			switch(pos){
			case 1: type = QuadType.BOTTOM_2; break;
			case 2: type = QuadType.RIGHT_2; break;
			case 3: type = QuadType.DIAG_POS; break;
			}
			break;
		case TOP_RIGHT_3:
			switch(pos){
			case 0: type = QuadType.BOTTOM_2; break;
			case 2: type = QuadType.DIAG_NEG; break;
			case 3: type = QuadType.LEFT_2; break;
			}
			break;
		case BOTTOM_LEFT_3:
			switch(pos){
			case 0: type = QuadType.RIGHT_2; break;
			case 1: type = QuadType.DIAG_NEG; break;
			case 3: type = QuadType.TOP_2; break;
			}
			break;
		case BOTTOM_RIGHT_3:
			switch(pos){
			case 0: type = QuadType.DIAG_POS; break;
			case 1: type = QuadType.LEFT_2; break;
			case 2: type = QuadType.TOP_2; break;
			}
			break;
		case FULL_4:
			switch(pos){
			case 0: type = QuadType.TOP_LEFT_3; break;
			case 1: type = QuadType.TOP_RIGHT_3; break;
			case 2: type = QuadType.BOTTOM_LEFT_3; break;
			case 3: type = QuadType.BOTTOM_RIGHT_3; break;
			}
			break;
		default:
			if (q.getCategory() == 1){
				return QuadType.EMPTY_0;
			} else {
				System.out.println("ATTEMPTED TO REMOVE FROM EMPTY QUAD");
			}
			break;
		}
		return type;
	}
	
	public static int getCategory(QuadType q){
		return q.getCategory();
	}
	
//	private int type;
//	private boolean[] state;
//	
//	public static void main(String[] args){
//		Quad q = new Quad(true, true, true, true);
//		q.addPiece(1);
//		q.addPiece(1);
//		System.out.println(q.getType());
//	}
//
//	public Quad(boolean topLeft, boolean topRight, boolean bottomLeft, boolean bottomRight){
//		state = new boolean[4];
//		state[0] = topLeft;
//		state[1] = topRight;
//		state[2] = bottomLeft;
//		state[3] = bottomRight;
//		int numTrue = 0;
//		for (int i = 0; i < 4; i++){
//			if(state[i]){
//				numTrue++;
//			}
//		}
//		type = numTrue;
//
//		// Check for diagonal type
//		if (numTrue == 2){
//			if ((state[0] && state[3]) || (state[1] && state[2])){
//				type = 5;
//			}
//		}
//	}
//	
//	public Quad(Quad q){
//		type = q.type;
//		state = new boolean[4];
//		state[0] = q.state[0];
//		state[1] = q.state[1];
//		state[2] = q.state[2];
//		state[3] = q.state[3];
//	}
//
//	/*
//	 * Location == 0 => top left
//	 * Location == 1 => top right
//	 * Location == 2 => bottom left
//	 * Location == 3 => bottom right
//	 */
//	public void removePiece(int location){
//		state[location] = false;
//		if (type == 5){
//			type = 1;
//		} else if (type == 3){
//			if ((state[0] && state[3]) || (state[1] && state[2])){
//				type = 5;
//			} else {
//				type = 2;
//			}
//		} else {
//			type--;
//		}
//	}
//	
//	public void addPiece(int location){
//		state[location] = true;
//		if (type == 1){
//			if ((state[0] && state[3]) || (state[1] && state[2])){
//				type = 5;
//			} else {
//				type = 2;
//			}
//		} else if (type == 5){
//			type = 3;
//		} else {
//			type++;
//		}
//	}
//	
//	public String toString(){
//		return state[0] + " " + state[1] + "\n" + state[2] + " " + state[3];
//	}
//	
//	public int getType(){
//		return type;
//	}
}