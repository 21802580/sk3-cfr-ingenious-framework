package loa.logic;
import java.util.ArrayDeque;
import java.util.HashSet;
import java.util.LinkedList;

import datastructure.RandomSet;
import statics.Constants;
import statics.Data;
import statics.MoveTableInitialiser;

public class Board {
	private int[][] state;
	public int currentPlayer;
	public ArrayDeque<Move> movesMade;
	public RandomSet<Coord> whitePieces;
	public RandomSet<Coord> blackPieces;
	public QuadTable quadTable;
	public MoveTable moveTable;

	// Lookup tables to keep track of the number of pieces in each line
	//	private int[] horizontal;
	//	private int[] vertical;
	//	private int[] positiveDiagonal;
	//	private int[] negativeDiagonal;



	public static void main(String[] args){
		MoveTableInitialiser.initialise();
		Board b = new Board();
		long startTime = System.currentTimeMillis();
		int playouts = 0;
		while (System.currentTimeMillis() - startTime < 10000){
			b.doPlayout();
			playouts++;
		}
		System.out.println(playouts / 10);
	}

	public Board(){
		currentPlayer = Constants.PIECE_BLACK;
		movesMade = new ArrayDeque<Move>();
		whitePieces = new RandomSet<Coord>();
		blackPieces = new RandomSet<Coord>();
		quadTable = new QuadTable();
		moveTable = new MoveTable();
		initialiseBoard();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (state[i][j] == Constants.PIECE_BLACK){
					blackPieces.add(Data.coords[i][j]);
				} else if (state[i][j] == Constants.PIECE_WHITE){
					whitePieces.add(Data.coords[i][j]);
				}
			}
		}
	}
	
	public Board(Board b){
		state = new int[8][8];
		for (int i = 0; i < state.length; i++){
			for (int j = 0; j < state.length; j++){
				state[i][j] = b.state[i][j];
			}
		}
		currentPlayer = b.currentPlayer;
		movesMade = b.movesMade.clone();
		blackPieces = new RandomSet<Coord>(b.blackPieces);
		whitePieces = new RandomSet<Coord>(b.whitePieces);
		quadTable = new QuadTable(b.quadTable);
		moveTable = new MoveTable(b.moveTable);
	}

	public int[][] getState(){
		return this.state;
	}
	
	public Move peekLastMove(){
		return this.movesMade.peekFirst();
	}

	public void applyMove(Move m){
		//System.out.println("Applying move " + m);
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		boolean capture = m.isCapture();
		movesMade.push(m);
		quadTable.applyMove(m, state[rf][cf]);	
		moveTable.makeMove(m, state[rf][cf]);
		state[rt][ct] = state[rf][cf];
		state[rf][cf] = Constants.PIECE_EMPTY;
		if (currentPlayer == Constants.PIECE_BLACK){
			//System.out.println("Applying black move " + m);
			blackPieces.remove(m.getFrom());
			blackPieces.add(m.getTo());
			if (capture){
				whitePieces.remove(m.getTo());
			}
			currentPlayer = Constants.PIECE_WHITE;
		} else {
			//System.out.println("Applying white move " + m);
			whitePieces.remove(m.getFrom());
			whitePieces.add(m.getTo());
			if (capture){
				blackPieces.remove(m.getTo());
			}
			currentPlayer = Constants.PIECE_BLACK;
		}
	}

	public Move undoMove(){
		Move m = movesMade.pop();
		int rt = m.getFrom().getRow();
		int ct = m.getFrom().getCol();
		int rf = m.getTo().getRow();
		int cf = m.getTo().getCol();
		boolean capture = m.isCapture();
		quadTable.undoMove(m, state[rf][cf]);
		moveTable.undoMove(m, state[rf][cf]);
		state[rt][ct] = state[rf][cf];
		if (state[rt][ct] == Constants.PIECE_BLACK){
			//System.out.println("Undoing black move " + m);
			blackPieces.add(m.getFrom());
			blackPieces.remove(m.getTo());
			if (capture){
				whitePieces.add(m.getTo());
				state[rf][cf] = Constants.PIECE_WHITE;
			} else {
				state[rf][cf] = Constants.PIECE_EMPTY;
			}
			currentPlayer = Constants.PIECE_BLACK;
		} else {
			//System.out.println("Undoing white move " + m);
			whitePieces.add(m.getFrom());
			whitePieces.remove(m.getTo());
			if (capture){
				blackPieces.add(m.getTo());
				state[rf][cf] = Constants.PIECE_BLACK;
			} else {
				state[rf][cf] = Constants.PIECE_EMPTY;
			}
			currentPlayer = Constants.PIECE_WHITE;
		}
		return m;
	}

	/*
	 * Returns true if the given co-ordinate is in the same line as the given move
	 */
	private boolean inLine(Coord c, Move m){
		int direction = m.getDirection();
		boolean inline = false;
		if ((direction == Constants.RIGHT) || (direction == Constants.LEFT)){
			// Horizontal
			inline = (c.getRow() == m.getFrom().getRow());
		} else if ((direction == Constants.DOWN) || (direction == Constants.UP)){
			// Vertical
			inline = (c.getCol() == m.getFrom().getCol());
		} else if ((direction == Constants.DOWN_LEFT) || (direction == Constants.UP_RIGHT)){
			// Positive diagonal
			inline = (c.getRow() + c.getCol() == m.getFrom().getRow() + m.getFrom().getCol());
		} else if ((direction == Constants.DOWN_RIGHT) || (direction == Constants.UP_LEFT)){
			// Negative diagonal
			inline = (c.getCol() - c.getRow() + 7 == m.getFrom().getCol() - m.getFrom().getRow() + 7);
		}
		return inline;
	}

	public int getCurrentPlayer(){
		return this.currentPlayer;
	}

	public int pieceAt(Coord c){
		return state[c.getRow()][c.getCol()];
	}

	// Initialise the board with the starting LOA position
	private void initialiseBoard(){
		int boardSize = 8;
		state = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					state[i][j] = Constants.PIECE_BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					state[i][j] = Constants.PIECE_WHITE;
				} else {
					state[i][j] = Constants.PIECE_EMPTY;
				}
			}
		}
	}

	// Returns false if move is illegal, true otherwise.
	public boolean moveLegal(Move m){
		int rF = m.getFrom().getRow();
		int cF = m.getFrom().getCol();
		int rT = m.getTo().getRow();
		int cT = m.getTo().getCol();
		int player = state[rF][cF];

		// No piece to move or trying to move opponent's piece
		if ((state[rF][cF] == Constants.PIECE_EMPTY) || (state[rF][cF] != player)){
			return false;
		}

		// Landing on own piece
		if (state[rF][cF] == state[rT][cT]) {
			return false;
		}

		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (rF == rT){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < 8; i++){
				if (state[rF][i] != Constants.PIECE_EMPTY){
					numPieces++;
					if (cT > cF){
						if ((i > cF) && (i < cT) && (state[rF][i] != player)){
							return false;
						}
					} else if (cF > cT) {
						if ((i > cT) && (i < cF) && (state[rF][i] != player)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(cT - cF)){
				return false;
			}
		} else if (cF == cT){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < 8; i++){
				if (state[i][cF] != Constants.PIECE_EMPTY){
					numPieces++;
					if (rT > rF){
						if ((i > rF) && (i < rT) && (state[i][cF] != player)){
							return false;
						}
					} else if (rF > rT) {
						if ((i > rT) && (i < rF) && (state[i][cF] != player)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(rT - rF)){
				return false;
			}
		} else {
			if ((double)(rF - rT) / (double)(cF - cT) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i < 8 && j < 8; i++, j++){
					if (state[i][j] != Constants.PIECE_EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != player)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != player)){
								return false;
							}
						}
					}
				}
				for (int i = rF - 1, j = cF - 1; i >= 0 && j >= 0; i--, j--){
					if (state[i][j] != Constants.PIECE_EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != player)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != player)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rT - rF)){
					return false;
				}
			} else if ((double)(rF - rT) / (double)(cF - cT) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i >= 0 && j < 8; i--, j++){
					if (state[i][j] != Constants.PIECE_EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != player)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != player)){
								return false;
							}
						}
					}
				}
				for (int i = rF + 1, j = cF - 1; i < 8 && j >= 0; i++, j--){
					if (state[i][j] != Constants.PIECE_EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != player)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != player)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rF - rT)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}

	// Returns the winning player if terminal, the empty player if a draw and -1 if not terminal
	public int isTerminal(){
		if (whitePieces.size() == 1 && blackPieces.size() == 1){
			// Draw
			return Constants.PIECE_EMPTY;
		} else if (whitePieces.size() == 1){
			return Constants.PIECE_WHITE;
		} else if (blackPieces.size() == 1){
			return Constants.PIECE_BLACK;
		} else if (quadTable.getEuler(Constants.PIECE_BLACK) > 1.0 && quadTable.getEuler(Constants.PIECE_BLACK) > 1.0){
			return -1;
		} else {
			Coord b = blackPieces.get(0);
			Coord w = whitePieces.get(0);
			boolean bwins = connected(b);
			boolean wwins = connected(w);
			if (bwins && wwins){
				return Constants.PIECE_EMPTY;
			} else if (bwins){
				return Constants.PIECE_BLACK;
			} else if (wwins) {
				return Constants.PIECE_WHITE;
			} else {
				return -1;
			}
		}
	}

	public String toString(){
		String s = "";
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 7; j++){
				switch (state[i][j]) {
				case Constants.PIECE_WHITE:
					s = s + "W ";
					break;
				case Constants.PIECE_BLACK:
					s = s + "B ";
					break;
				case Constants.PIECE_EMPTY:
					s = s + ". ";
					break;
				}
			}
			switch (state[i][7]) {
			case Constants.PIECE_WHITE:
				s = s + "W\n";
				break;
			case Constants.PIECE_BLACK:
				s = s + "B\n";
				break;
			case Constants.PIECE_EMPTY:
				s = s + ".\n";
				break;
			}
		}
		return s;
	}

	public boolean connected(Coord c){
		int colour = state[c.getRow()][c.getCol()];
		LinkedList<Coord> q = new LinkedList<Coord>();
		HashSet<Coord> checked = new HashSet<Coord>();
		q.add(c);
		while(!q.isEmpty()){
			Coord current = q.remove();
			int row = current.getRow();
			int col = current.getCol();
			if (state[row][col] == colour){
				checked.add(current);
				Coord n = row > 0 ? Data.coords[row - 1][col] : null;
				Coord s = row < state.length - 1 ? Data.coords[row + 1][col] : null;
				Coord e = col < state.length - 1 ? Data.coords[row][col + 1] : null;
				Coord w = col > 0 ? Data.coords[row][col - 1] : null;
				Coord ne = (row > 0) && (col < state.length - 1) ? Data.coords[row - 1][col + 1] : null;
				Coord nw = (row > 0) && (col > 0) ? Data.coords[row - 1][col - 1] : null;
				Coord se = (col < state.length - 1) && (row < state.length - 1) ? Data.coords[row + 1][col + 1] : null;
				Coord sw = (col > 0) && (row < state.length - 1)? Data.coords[row + 1][col - 1] : null;
				if (n != null && !checked.contains(n)){
					q.add(n);
				}
				if (s != null && !checked.contains(s)){
					q.add(s);	
				}
				if (e != null && !checked.contains(e)){
					q.add(e);
				}
				if (w != null && !checked.contains(w)){
					q.add(w);
				}
				if (ne != null && !checked.contains(ne)){
					q.add(ne);
				}
				if (nw != null && !checked.contains(nw)){
					q.add(nw);
				}
				if (se != null && !checked.contains(se)){
					q.add(se);
				}
				if (sw != null && !checked.contains(sw)){
					q.add(sw);
				}
			}
		}
		if (colour == Constants.PIECE_WHITE){
			return checked.size() == whitePieces.size();
		} else {
			return checked.size() == blackPieces.size();
		}
	}

	public int doPlayout(){
		int n = 0;
		int numPassesBlack = 0;
		int numPassesWhite = 0;
		Board b2 = new Board(this);
		//Board b2 = this;
		int winner = b2.isTerminal();
		while (winner == -1){
			RandomSet<Coord> pieces = new RandomSet<Coord>();
			if (b2.currentPlayer == Constants.PIECE_BLACK){
				pieces = b2.blackPieces;
			} else {
				pieces = b2.whitePieces;
			}
			Move m = b2.moveTable.getRandomMove(pieces, b2.currentPlayer);
			if (m == null){
				if (b2.currentPlayer == Constants.PIECE_BLACK){
					numPassesBlack++;
					if (numPassesBlack == 3){
						winner = Constants.PIECE_EMPTY;
						break;
					} else {
						b2.currentPlayer = Constants.PIECE_WHITE;
						continue;
					}
				} else {
					numPassesWhite++;
					if (numPassesWhite == 3){
						winner = Constants.PIECE_EMPTY;
						break;
					} else {
						b2.currentPlayer = Constants.PIECE_BLACK;
						continue;
					}
				}
			} else {
				if (b2.currentPlayer == Constants.PIECE_BLACK){
					numPassesBlack = 0;
				} else {
					numPassesWhite = 0;
				}
				b2.applyMove(m);
				winner = b2.isTerminal();
				n++;
			}
			if (n > 200){
				winner = Constants.PIECE_EMPTY;
				break;
			}
		}
//		for (int i = 0; i < n; i++){
//			undoMove();
//		}
		return winner;
	}

	public RandomSet<Move> getPossibleMoves() {
		RandomSet<Coord> pieces = null;
		if (currentPlayer == Constants.PIECE_BLACK){
			pieces = blackPieces;
		} else {
			pieces = whitePieces;
		}
		return moveTable.getAllMoves(pieces, currentPlayer);
	}

	public double getHeuristicValue(Move m){
		double hb = 0;
		double hw = 0;
		applyMove(m);
		Coord bCOM = getCentreOfMass(Constants.PIECE_BLACK);
		Coord wCOM = getCentreOfMass(Constants.PIECE_WHITE);
		for (Coord c : blackPieces){
			hb += getDistance(c, bCOM);
		}
		for (Coord c : whitePieces){
			hw += getDistance(c, wCOM);
		}
		double sodB = hb - Data.sumOfMinimalDistances[blackPieces.size()];
		double sodW = hw - Data.sumOfMinimalDistances[whitePieces.size()];
		double ret = 0;
		if (currentPlayer == Constants.PIECE_BLACK){
			ret = (1.0 / sodB) - (1.0 / sodW);
		} else {
			ret = (1.0 / sodW) - (1.0 / sodB);
		}
		return ret;
	}

	public double getDistance(Coord c1, Coord c2){
		double x1 = (double)c1.getRow();
		double x2 = (double)c2.getRow();
		double y1 = (double)c1.getCol();
		double y2 = (double)c2.getCol();
		return Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
	}

	public Coord getCentreOfMass(int colour){
		double cx = 0;
		double cy = 0;
		RandomSet<Coord> pieces = whitePieces;
		if (colour == Constants.PIECE_BLACK){
			pieces = blackPieces;
		}
		for (Coord c : pieces){
			cx += c.getRow();
			cy += c.getCol();
		}
		cx /= (double)pieces.size();
		cy /= (double)pieces.size();
		int row = (int)Math.round(cx);
		int col = (int)Math.round(cy);
		Coord cc = Data.coords[row][col];
		double minDist = 20;
		Coord com = Data.coords[0][0];
		for (Coord c : pieces){
			double dist = getDistance(c, cc);
			if (dist < minDist){
				minDist = dist;
				com = c;
			}
		}
		return com;
	}

	public boolean moveIsDecisive(Move m){
		applyMove(m);
		boolean decisive = false;
		if (isTerminal() == Constants.PIECE_BLACK || isTerminal() == Constants.PIECE_WHITE){
			decisive = true;
		}
		undoMove();
		return decisive;
	}

	// Return the last move made by the given colour
	public Move getLastMoveByCurrentPlayer(){
		if (movesMade.size() < 2){
			return null;
		}
		Move m = movesMade.pop();
		Move ret = movesMade.peekFirst();
		movesMade.push(m);
		return ret;
	}
}
