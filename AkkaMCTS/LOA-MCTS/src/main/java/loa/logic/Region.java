package loa.logic;

public enum Region {
	CORNER, OUTER, INNER, CENTRAL
}
