package loa.logic;

public class LineMove {
	private int source;
	private int destination;
	private boolean capture;
	
	public LineMove(int source, int destination, boolean capture){
		this.source = source;
		this.destination = destination;
		this.capture = capture;
	}

	public int getSource() {
		return source;
	}

	public int getDestination() {
		return destination;
	}

	public boolean isCapture() {
		return capture;
	}
	
	public String toString(){
		return source + " -> " + destination;
	}
}
