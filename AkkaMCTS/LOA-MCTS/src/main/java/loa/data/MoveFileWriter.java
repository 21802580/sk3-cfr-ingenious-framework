//package loa.data;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MoveFileWriter {
	private static BufferedWriter writer;

	public static void main(String[] args){
		//final File file = new File("moves.txt");
		//writer = null;
		//try {
		//	if (!file.exists()){
		//		file.createNewFile();
		//	}
		//	FileWriter fw = new FileWriter(file.getAbsoluteFile(), false);
		//	writer = new BufferedWriter(fw);
		//} catch (IOException e1) {
		//	// TODO Auto-generated catch block
		//	e1.printStackTrace();
		//}
		long startTime = System.currentTimeMillis();
		for (int i0 = 0; i0 < 3; i0++){
			for (int i1 = 0; i1 < 3; i1++){
				for (int i2 = 0; i2 < 3; i2++){
					for (int i3 = 0; i3 < 3; i3++){
						for (int i4 = 0; i4 < 3; i4++){
							for (int i5 = 0; i5 < 3; i5++){
								for (int i6 = 0; i6 < 3; i6++){
									for (int i7 = 0; i7 < 3; i7++){
										int[] line = new int[] {i0, i1, i2, i3, i4, i5, i6, i7};
										addMoves(line);
									}
									int[] line = new int[] {i0, i1, i2, i3, i4, i5, i6};
									addMoves(line);
								}
								int[] line = new int[] {i0, i1, i2, i3, i4, i5};
								addMoves(line);
							}
							int[] line = new int[] {i0, i1, i2, i3, i4};
							addMoves(line);
						}
						int[] line = new int[] {i0, i1, i2, i3};
						addMoves(line);
					}
					int[] line = new int[] {i0, i1, i2};
					addMoves(line);
				}
				int[] line = new int[] {i0, i1};
				addMoves(line);
			}
		}
		System.out.println(System.currentTimeMillis() - startTime);
		//try {
		//	writer.close();
		//} catch (IOException e) {
		//	// TODO Auto-generated catch block
		//	e.printStackTrace();
		//}
	}
	
	private static void addMoves(int[] line){
		int lineLength = line.length;
		int index = 0;
		int numPieces = 0;
		for (int i = 0; i < lineLength; i++){
			index += line[i] * Math.pow(3, i);
			if (line[i] != 0){
				numPieces++;
			}
		}
		
		for (int i = 0; i < lineLength; i++){
			int dest1 = i + numPieces;
			int dest2 = i - numPieces;
			if (line[i] == 1){
				if (moveLegal(line, i, dest1)){
					//try {
						boolean capture = line[dest1] == 0 ? false : true;
					//    writer.write(Constants.PIECE_BLACK + " " + lineLength + " " + index + " " + i + " " + dest1 + " " + capture + "\n");
					//} catch (IOException e) {
					//  e.printStackTrace();
					//} 
				}
				if (moveLegal(line, i, dest2)){			
					//try {
						boolean capture = line[dest2] == 0 ? false : true;
					//    writer.write(Constants.PIECE_BLACK + " " + lineLength + " " + index + " " + i + " " + dest2 + " " + capture + "\n");
					//} catch (IOException e) {
					//  e.printStackTrace();	
					//} 
				}
			} else if (line[i] == 2){
				if (moveLegal(line, i, dest1)){
					//try {
						boolean capture = line[dest1] == 0 ? false : true;
					//    writer.write(Constants.PIECE_WHITE + " " + lineLength + " " + index + " " + i + " " + dest1 + " " + capture + "\n");
					//} catch (IOException e) {
					//  e.printStackTrace();	
					//} 
				}
				if (moveLegal(line, i, dest2)){
					//try {
						boolean capture = line[dest2] == 0 ? false : true;
					//    writer.write(Constants.PIECE_WHITE + " " + lineLength + " " + index + " " + i + " " + dest2 + " " + capture + "\n");
					//} catch (IOException e) {
					//  e.printStackTrace();	
					//} 
				}
			}
		}
	}
	
	public static boolean moveLegal(int[] line, int src, int dest){
		int player = line[src];

		if (src == dest){
			return false;
		}
		if (src < 0 || src > line.length - 1 || dest < 0 || dest > line.length - 1){
			return false;
		}
		if (player == 0){
			return false;
		}
		if (line[dest] == player) {
			return false;
		}
		if (src < dest){
			for (int i = src; i < dest; i++){
				if (line[i] != player && line[i] != 0){
					return false;
				}
			}
		} else {
			for (int i = dest + 1; i < src; i++){
				if (line[i] != player && line[i] != 0){
					return false;
				}
			}
		}
		return true;
	}
}
