package loa.data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import loa.logic.Board;
import loa.logic.Coord;
import loa.logic.Move;

public class MonaYLTranslator {
	static Board b;
	
	public static void main(String[] args){
		String srcPath = args[0];
		FileReader input = null;
		try {
			input = new FileReader(srcPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader buf = new BufferedReader(input);
		String line = null;
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("mona_yl_translated.txt")));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			b = new Board();
			int turn = 0;
			while ((line = buf.readLine()) != null){
				if (line.isEmpty()){
					b = new Board();
					turn = 0;
				} else {
					String[] turns = line.split(" ");
					String newLine = "";
					for (String s : turns){
						if (s.matches("[a-z][1-8][a-z][1-8]")){
							String newMove = getMove(s);
							if (turn % 2 == 0){
								if (turn < 18){
									newLine += "   " + ((turn / 2) + 1) + ". " + newMove;
								} else {
									newLine += "  " + ((turn / 2) + 1) + ". " + newMove;
								}
							} else {
								newLine += " " + newMove;
							}
							turn++;
						}
					}
					writer.write(newLine + '\n');
					newLine = "";
				}
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String getMove(String s){
		int colFrom = (int)s.charAt(0) - 97;
		int rowFrom = Math.abs((s.charAt(1) - '0') - 8);
		int colTo = (int)s.charAt(2) - 97;
		int rowTo = Math.abs((s.charAt(3) - '0') - 8);
		boolean capture = false;
		if (b.pieceAt(new Coord(rowTo, colTo)) != 0){
			capture = true;
		}
		Move m = new Move(new Coord(rowFrom, colFrom), new Coord(rowTo, colTo), capture);
		b.applyMove(m);
		String newString = "";
		if (capture){
			newString = s.substring(0, 2) + ":" + s.substring(2);
		} else {
			newString = s.substring(0, 2) + "-" + s.substring(2);
		}
		return newString;
	}
}
