package loa.data;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;

import datastructure.RandomSet;
import loa.logic.Board;
import loa.logic.Coord;
import loa.logic.Move;
import loa.logic.MoveTable;
import statics.Constants;

public class MatchParser {
	static Board b;
	static int sumOfLengths = 0;
	static int numMatches = 0;
	static HashMap<Integer, Fraction> pmc;
	
	public static void main(String[] args){
		String srcPath = args[0];
		FileReader input = null;
		try {
			input = new FileReader(srcPath);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		BufferedReader buf = new BufferedReader(input);
		pmc = new HashMap<Integer, Fraction>();
		populateMap(buf);
		generateMCFile();
	}
	
	private static void populateMap(BufferedReader buf){
		String line = null;
		try {
			b = new Board();
			while ((line = buf.readLine()) != null){
				if (line.isEmpty()){
					b = new Board();
					numMatches++;
				} else {
					String[] turns = line.split(" ");
					for (String s : turns){
						if (s.matches("[a-z][1-8][:-][a-z][1-8]")){
							sumOfLengths++;
							Move m = getMove(s);
							RandomSet<Move> possibleMoves = b.getPossibleMoves();
							b.applyMove(m);
							int mc = m.getMoveCategory();
							if (pmc.containsKey(mc)){
								Fraction f = pmc.get(mc);
								f.incDenominator();
								f.incNumerator();
							} else {
								pmc.put(mc, new Fraction(1, 1));
							}
							for (Move mv : possibleMoves){
								if (mv.equals(m)){
									continue;
								}
								mc = mv.getMoveCategory();
								if (pmc.containsKey(mc)){
									Fraction f = pmc.get(mc);
									f.incDenominator();
								} else {
									pmc.put(mc, new Fraction(0, 1));
								}
							}
						}
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static void generateMCFile(){
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("move_categories.txt")));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		for (int mc : pmc.keySet()){
			System.out.println(mc + ": " + pmc.get(mc).getValue());
			try {
				writer.write(mc + ": " + pmc.get(mc).getValue() + "\n");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println();
		System.out.println("Number of matches parsed: " + numMatches);
		System.out.println("Average match length: " + (double)sumOfLengths / (double)numMatches);
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static Move getMove(String s){
		int colFrom = (int)s.charAt(0) - 97;
		int rowFrom = Math.abs((s.charAt(1) - '0') - 8);
		int colTo = (int)s.charAt(3) - 97;
		int rowTo = Math.abs((s.charAt(4) - '0') - 8);
		boolean capture = false;
		if (b.pieceAt(new Coord(rowTo, colTo)) != 0){
			capture = true;
		}
		return new Move(new Coord(rowFrom, colFrom), new Coord(rowTo, colTo), capture);
	}
	
	private static class Fraction {
		public int numerator;
		public int denominator;
		
		public Fraction(int num, int den){
			numerator = num;
			denominator = den;
		}
		
		public void incNumerator(){
			numerator++;
		}
		
		public void incDenominator(){
			denominator++;
		}
		
		public String toString(){
			return numerator + "/" + denominator;
		}
		
		public double getValue(){
			return (double)numerator / (double)denominator;
		}
	}
}
