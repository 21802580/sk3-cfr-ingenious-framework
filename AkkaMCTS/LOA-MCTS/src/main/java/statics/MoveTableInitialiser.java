package statics;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import loa.logic.LineMove;

public class MoveTableInitialiser {
	
	public static void initialise(){
		Data.powers = new int[8];
		for (int i = 0; i < 8; i++){
			Data.powers[i] = (int) Math.pow(3, i);
		}
		Data.blackMoves = new ArrayList<HashMap<Integer, ArrayList<LineMove>>>(9);
		Data.whiteMoves = new ArrayList<HashMap<Integer, ArrayList<LineMove>>>(9);
		for (int i = 0; i <= 8; i++){
			Data.blackMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
			Data.whiteMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
		}

		Scanner s = null;
		try {
			s = new Scanner(new File("moves.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (s.hasNext()){
			int player = s.nextInt();
			int length = s.nextInt();
			int index = s.nextInt();
			int src = s.nextInt();
			int dest = s.nextInt();
			boolean capture = s.nextBoolean();
			if (player == Constants.PIECE_BLACK){
				HashMap<Integer, ArrayList<LineMove>> hm = Data.blackMoves.get(length);
				ArrayList<LineMove> moves = hm.get(index);
				if (moves == null){
					moves = new ArrayList<LineMove>();
					hm.put(index, moves);
				}
				moves.add(new LineMove(src, dest, capture));
			} else {
				HashMap<Integer, ArrayList<LineMove>> hm = Data.whiteMoves.get(length);
				ArrayList<LineMove> moves = hm.get(index);
				if (moves == null){
					moves = new ArrayList<LineMove>();
					hm.put(index, moves);
				}
				moves.add(new LineMove(src, dest, capture));
			}
		}
		s.close();
	}
}
