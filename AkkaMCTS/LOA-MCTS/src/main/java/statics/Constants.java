package statics;

import java.util.concurrent.ThreadLocalRandom;

public class Constants {
	// Colour constants
	public static final int PIECE_EMPTY = 0;
	public static final int PIECE_BLACK = 1;
	public static final int PIECE_WHITE = 2;
	
	// Direction constants
	public static final int RIGHT = 0;
	public static final int LEFT = 1;
	public static final int UP = 2;
	public static final int DOWN = 3;
	public static final int UP_RIGHT = 4;
	public static final int UP_LEFT = 5;
	public static final int DOWN_RIGHT = 6;
	public static final int DOWN_LEFT = 7;
		
	// Player information
	public static int PLAYER_COLOUR;
	public static String PLAYER_TYPE;
	public static int TIMEOUT;
	
	// Akka/TDS information
	public static int NUM_SEARCH_RANKS;
	public static int NUM_WORKERS_PER_RANK;
	public static int NUM_PARALLEL_SEARCHES;
	public static int TT_CAPACITY;
	public static String MASTER_NAME;
	public static String SEARCH_RANK_NAME;
	public static String BROADCAST_RANK_NAME;
	public static String ACTOR_SYSTEM_NAME;
	public static Class<?> SEARCH_RANK_CLASS;
	public static Class<?> WORKER_CLASS;
	public static Class<?> BROADCASTER_CLASS;
	public static Class<?> NODE_CLASS;
	
	// MCTS Constants
	public static double UCB_CONSTANT;
	public static double FPU;
	public static boolean PROGRESSIVE_BIAS;
	public static boolean PROGRESSIVE_UNPRUNING;
	public static int INITIAL_UNPRUNING_WINDOW;
	public static double BIAS_CONSTANT;
	public static double UNPRUNING_CONSTANT;
	public static boolean MOVE_CATEGORY_UNPRUNING;
	public static boolean MOVE_CATEGORY_BIAS;
	public static boolean DECISIVE_MOVES;
	public static int PLAYOUT_THRESHOLD;
	
	// Schaefers duplication/synchronisation constants
	public static int N_DUP;
	public static double SYNC_REDUCTION_RATE;
	public static double SYNC_ALPHA;
	public static int CACHE_CAPACITY;
	public static int NUM_BROADCAST_RANKS;
	
	// Root parallelisation duplication constants
	public static int ROOT_DUPLICATION_PLY;
	public static int ROOT_DUPLICATION_THRESHOLD; // As a percentage of the total playouts
	public static double ROOT_DUPLICATION_PERIOD;
	
	// Random object
	public static ThreadLocalRandom RANDOM = ThreadLocalRandom.current();
}
