package statics;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import player.tds.core.TDSNode;
import player.tds.schaefers.SchaefersBroadcastRank;
import player.tds.schaefers.SchaefersNode;
import player.tds.schaefers.SchaefersSearchRank;
import player.tds.schaefers.SchaefersWorker;
import player.tds.vanilla.TDSSearchRank;
import player.tds.vanilla.TDSWorker;

public class MatchConfig {
	private boolean logging;
	private JsonArray players;
	
	public static void main(String[] args){
		MatchConfig c = new MatchConfig("/home/marc/testing/matchconfig.json");
		c.populatePlayerConfig(0);
		System.out.println(Constants.BIAS_CONSTANT);
	}
	
	public MatchConfig(String path){
		try {
			JsonElement jt = new JsonParser().parse(new FileReader(path));
			JsonObject jo = jt.getAsJsonObject();
			logging = jo.get("logging").getAsBoolean();
			players = jo.getAsJsonArray("players");
		} catch (JsonIOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonSyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public boolean matchLogging(){
		return logging;
	}
	
	public void populatePlayerConfig(int player){
		JsonObject jo = players.get(player).getAsJsonObject();
		Constants.PLAYER_TYPE = jo.get("type").getAsString();
		Constants.PLAYER_COLOUR = player + 1;
		Constants.TIMEOUT = jo.get("timeout").getAsInt();
		Constants.UCB_CONSTANT = jo.get("ucb_constant").getAsDouble();
		Constants.FPU = jo.get("fpu").getAsDouble();
		Constants.PROGRESSIVE_BIAS = jo.get("progressive_bias").getAsBoolean();
		Constants.PROGRESSIVE_UNPRUNING = jo.get("progressive_unpruning").getAsBoolean();
		Constants.INITIAL_UNPRUNING_WINDOW = jo.get("initial_unpruning_window").getAsInt();
		Constants.BIAS_CONSTANT = jo.get("bias_constant").getAsDouble();
		Constants.UNPRUNING_CONSTANT = jo.get("unpruning_constant").getAsInt();
		Constants.MOVE_CATEGORY_UNPRUNING = jo.get("move_category_unpruning").getAsBoolean();
		Constants.MOVE_CATEGORY_BIAS = jo.get("move_category_bias").getAsBoolean();
		Constants.DECISIVE_MOVES = jo.get("decisive_moves").getAsBoolean();
		Constants.PLAYOUT_THRESHOLD = jo.get("playout_threshold").getAsInt();
		Constants.NUM_SEARCH_RANKS = jo.get("num_search_ranks").getAsInt();
		Constants.NUM_WORKERS_PER_RANK = jo.get("num_workers_per_rank").getAsInt();
		Constants.NUM_PARALLEL_SEARCHES = jo.get("num_parallel_searches").getAsInt();
		Constants.NUM_BROADCAST_RANKS = jo.get("num_broadcast_ranks").getAsInt();
		Constants.N_DUP = jo.get("n_dup").getAsInt();
		Constants.SYNC_REDUCTION_RATE = jo.get("sync_reduction_rate").getAsDouble();
		Constants.SYNC_ALPHA = (1.0 / Math.pow(Constants.SYNC_REDUCTION_RATE, 2)) - 1;
		Constants.TT_CAPACITY = jo.get("transposition_table_capacity").getAsInt();
		Constants.CACHE_CAPACITY = jo.get("duplicated_node_capacity").getAsInt();
		Constants.ROOT_DUPLICATION_PLY = jo.get("root_duplication_ply").getAsInt();
		Constants.ROOT_DUPLICATION_THRESHOLD = jo.get("root_duplication_threshold").getAsInt();
		Constants.ROOT_DUPLICATION_PERIOD = jo.get("root_duplication_periond").getAsDouble();
		populateTDSInformation(player);
	}
	
	private void populateTDSInformation(int player){
		switch(Constants.PLAYER_TYPE){
		case "tds":
			Constants.ACTOR_SYSTEM_NAME = "TDSSystem-" + player;
			Constants.MASTER_NAME = "tds_master-" + player;
			Constants.SEARCH_RANK_NAME = "tds_search_rank-" + player;
			Constants.SEARCH_RANK_CLASS = TDSSearchRank.class;
			Constants.WORKER_CLASS = TDSWorker.class;
			Constants.NODE_CLASS = TDSNode.class;
			break;
		case "schaefers":
			Constants.ACTOR_SYSTEM_NAME = "SchaefersSystem-" + player;
			Constants.MASTER_NAME = "schaefers_master-" + player;
			Constants.SEARCH_RANK_NAME = "schaefers_search_rank-" + player;
			Constants.BROADCAST_RANK_NAME = "schaefers_broadcast_rank-" + player;
			Constants.SEARCH_RANK_CLASS = SchaefersSearchRank.class;
			Constants.WORKER_CLASS = SchaefersWorker.class;
			Constants.BROADCASTER_CLASS = SchaefersBroadcastRank.class;
			Constants.NODE_CLASS = SchaefersNode.class;
			break;
		}
	}
}
