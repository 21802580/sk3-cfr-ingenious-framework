package statics;

import java.util.ArrayList;
import java.util.HashMap;

import loa.logic.Coord;
import loa.logic.LineMove;
import loa.logic.MoveCategoryTable;

public class Data {
	public static void main(String[] args){
		QuadType[][] q = new QuadType[2][2];
		System.out.println(QuadType[][].class);
	}
	
	// Table containing transition probabilities for each move category
	public static MoveCategoryTable categoryTable = new MoveCategoryTable();

	// Move tables
	public static ArrayList<HashMap<Integer, ArrayList<LineMove>>> blackMoves;
	public static ArrayList<HashMap<Integer, ArrayList<LineMove>>> whiteMoves;
	public static int[] powers = {1, 3, 9, 27, 81, 243, 729, 2187};
	
	// Lookup table for sum-of-minimal-distances (heuristic function)
	public static int[] sumOfMinimalDistances = new int[] {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14};

	// Static co-ordinate array
	public static Coord[][] coords = {
			{
				new Coord(0, 0), new Coord(0, 1), new Coord(0, 2), new Coord(0, 3),
				new Coord(0, 4), new Coord(0, 5), new Coord(0, 6), new Coord(0, 7) 
			},
			{
				new Coord(1, 0), new Coord(1, 1), new Coord(1, 2), new Coord(1, 3),
				new Coord(1, 4), new Coord(1, 5), new Coord(1, 6), new Coord(1, 7) 
			},
			{
				new Coord(2, 0), new Coord(2, 1), new Coord(2, 2), new Coord(2, 3),
				new Coord(2, 4), new Coord(2, 5), new Coord(2, 6), new Coord(2, 7) 
			},
			{
				new Coord(3, 0), new Coord(3, 1), new Coord(3, 2), new Coord(3, 3),
				new Coord(3, 4), new Coord(3, 5), new Coord(3, 6), new Coord(3, 7) 
			},
			{
				new Coord(4, 0), new Coord(4, 1), new Coord(4, 2), new Coord(4, 3),
				new Coord(4, 4), new Coord(4, 5), new Coord(4, 6), new Coord(4, 7) 
			},
			{
				new Coord(5, 0), new Coord(5, 1), new Coord(5, 2), new Coord(5, 3),
				new Coord(5, 4), new Coord(5, 5), new Coord(5, 6), new Coord(5, 7) 
			},
			{
				new Coord(6, 0), new Coord(6, 1), new Coord(6, 2), new Coord(6, 3),
				new Coord(6, 4), new Coord(6, 5), new Coord(6, 6), new Coord(6, 7) 
			},
			{
				new Coord(7, 0), new Coord(7, 1), new Coord(7, 2), new Coord(7, 3),
				new Coord(7, 4), new Coord(7, 5), new Coord(7, 6), new Coord(7, 7) 
			}
	};

	// Quad types
	public static enum QuadType {
		EMPTY_0(0),
		TOP_LEFT_1(1),
		TOP_RIGHT_1(1),
		BOTTOM_LEFT_1(1),
		BOTTOM_RIGHT_1(1),
		TOP_2(2),
		BOTTOM_2(2),
		LEFT_2(2),
		RIGHT_2(2),
		TOP_LEFT_3(3),
		TOP_RIGHT_3(3),
		BOTTOM_LEFT_3(3),
		BOTTOM_RIGHT_3(3),
		FULL_4(4),
		DIAG_NEG(5),
		DIAG_POS(5);

		private int category;

		private QuadType(int category){
			this.category = category;
		}

		public int getCategory(){
			return this.category;
		}
	}

}
