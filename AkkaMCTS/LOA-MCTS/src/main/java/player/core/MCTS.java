package player.core;

import loa.logic.Board;
import loa.logic.Move;
import player.serial.SerialNode;

/*
 * Helper class for common MCTS components
 */

public class MCTS {
	
	/*
	 * Find and return the best expanded move for the given node
	 */
	public static <N extends MCTSNode> Move getBestExpandedMove(N node){
		return null;
	}
	
	/*
	 * Return the best unexpanded move for the given node
	 */
	public static <N extends MCTSNode> Move getBestUnexpandedMove(N node){
		return null;
	}
	
	/*
	 * Add the given reward to the relevant child of the node and add the provided number of visits
	 */
	public static <N extends MCTSNode> void updateReward(N node, Move child, double reward, int numVisits){
		
	}
	
	/*
	 * Perform a playout on the provided board and return the reward
	 */
	public static double performPlayout(Board board){
		return 0.0;
	}
}
