package player.core;

import loa.logic.Board;
import loa.logic.Coord;
import loa.logic.Move;
import statics.Constants;
import statics.Data;

public class MoveTranslation {
	
	public static Move getMoveFromString(String moveString, Board state){
		int rf = Character.getNumericValue(moveString.charAt(0));
		int cf = Character.getNumericValue(moveString.charAt(1));
		int rt = Character.getNumericValue(moveString.charAt(2));
		int ct = Character.getNumericValue(moveString.charAt(3));
		Coord to = Data.coords[rt][ct];
		boolean capture = false;
		if (state.pieceAt(to) != Constants.PIECE_EMPTY){
			capture = true;
		}
		return new Move(Data.coords[rf][cf], Data.coords[rt][ct], capture);
	}
	
	public static String getStringFromMove(Move move){
		String rf = move.getFrom().getRow() + "";
		String cf = move.getFrom().getCol() + "";
		String rt = move.getTo().getRow() + "";
		String ct = move.getTo().getCol() + "";
		return  rf + cf + rt + ct;
	}
}
