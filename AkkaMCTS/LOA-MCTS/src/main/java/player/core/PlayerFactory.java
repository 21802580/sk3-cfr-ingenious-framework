package player.core;

import player.leaf.LeafPlayer;
import player.root.RootPlayer;
import player.serial.SerialPlayer;
import player.tds.core.TDSPlayer;
import statics.Constants;

public class PlayerFactory {
	
	public static LOAPlayer getInstance(String seedHost, String seedPort){
		LOAPlayer p = null;
		switch(Constants.PLAYER_TYPE){
		case "serial":
			p = new SerialPlayer();
			break;
		case "tds":
			p = new TDSPlayer(seedHost, seedPort);
			break;
		case "schaefers":
			p = new TDSPlayer(seedHost, seedPort);
			break;
		case "leaf":
			p = new LeafPlayer(seedHost, seedPort);
			break;
		case "root":
			p = new RootPlayer(seedHost, seedPort);
			break;
		}
		return p;
	}
}
