package player.core;

import java.util.ArrayList;
import java.util.HashMap;

import datastructure.RandomSet;
import loa.logic.Board;
import loa.logic.Move;
import statics.Constants;
import statics.Data;

public class MCTSNode {
	protected Move lastMove;
	protected RandomSet<Move> unexpandedMoves;
	protected ArrayList<Move> expandedMoves;
	protected ArrayList<Integer> n;
	protected ArrayList<Double> w;
	protected int t;
	protected HashMap<Move, Double> h;
	protected HashMap<Move, Integer> moveIndices;
	protected Move decisive;
	protected int windowSize;
	
	public static void main(String[] args){
		Move move1 = new Move(Data.coords[4][6], Data.coords[6][6], false);
		Move move2 = new Move(Data.coords[6][6], Data.coords[4][6], false);
		System.out.println((move1.getFrom()).equals(move2.getTo()) &&
				(move1.getTo()).equals(move2.getFrom()));
	}
	
	public MCTSNode(Move lastMove, Board b){
		this.unexpandedMoves = b.getPossibleMoves();
		this.expandedMoves = new ArrayList<Move>(36);
		this.lastMove = lastMove;
		this.n = new ArrayList<Integer>(50);
		this.w = new ArrayList<Double>(50);
		this.decisive = null;
		this.h = new HashMap<Move, Double>(50);
		this.moveIndices = new HashMap<Move, Integer>(50);
		if (Constants.PROGRESSIVE_BIAS || Constants.PROGRESSIVE_UNPRUNING || Constants.DECISIVE_MOVES){
			for (Move m : unexpandedMoves){
				if (Constants.PROGRESSIVE_BIAS || Constants.PROGRESSIVE_UNPRUNING){
					h.put(m, b.getHeuristicValue(m));
				}
				if (Constants.DECISIVE_MOVES && b.moveIsDecisive(m)){
					decisive = m;
					if (!(Constants.PROGRESSIVE_BIAS || Constants.PROGRESSIVE_UNPRUNING)){
						break;
					}
				}
			}
		}
		this.t = 0;
		this.windowSize = Constants.INITIAL_UNPRUNING_WINDOW;
	}
	
	public MCTSNode(MCTSNode node){
		this.unexpandedMoves = new RandomSet<Move>(node.unexpandedMoves);
		this.expandedMoves = new ArrayList<Move>(node.expandedMoves);
		this.lastMove = node.lastMove;
		this.n = new ArrayList<Integer>(node.n);
		this.w = new ArrayList<Double>(node.w);
		this.decisive = node.decisive;
		this.h = new HashMap<Move, Double>(node.h);
		this.moveIndices = new HashMap<Move, Integer>(node.moveIndices);
		this.t = node.t;
		this.windowSize = node.windowSize;
	}
	
	public boolean isFullyExpanded() {
		if (Constants.PROGRESSIVE_UNPRUNING || Constants.MOVE_CATEGORY_UNPRUNING){
			return (expandedMoves.size() == windowSize) || (unexpandedMoves.size() == 0);
		} else {
			return unexpandedMoves.size() == 0;
		}
	}
	
	public boolean isLeaf(){
		return expandedMoves.size() == 0;
	}

	public Move getLastMove() {
		return lastMove;
	}

	public ArrayList<Move> getExpandedMoves() {
		return expandedMoves;
	}
	
	public RandomSet<Move> getUnexpandedMoves(){
		return unexpandedMoves;
	}
	
	public int getVisits(){
		return this.t;
	}
	
	public int getN(int index){
		return n.get(index);
	}
	
	public double getW(int index){
		return w.get(index);
	}
	
	public int getChildIndex(Move child){
		return moveIndices.get(child);
	}
	
	public void incVisits(){
		t++;
		windowSize = 1 + (int)(Math.log(t) / Math.log(Constants.UNPRUNING_CONSTANT));
	}
	
	public void addReward(double reward, Move child){
		//n.set(idx, n.get(idx) + 1);
		int idx = moveIndices.get(child);
		w.set(idx, w.get(idx) + reward);
	}
	
	public void incN(int idx){
		n.set(idx, n.get(idx) + 1);
	}
	
	public void addVisits(int numVisits){
		this.t += numVisits;
	}
	
	public void addN(int idx, int num){
		n.set(idx, n.get(idx) + num);
	}
	
	public double getUCB(int child){
		double ucb = (getW(child) / (double)(getN(child))) + (Constants.UCB_CONSTANT * Math.sqrt((Math.log((double)getVisits())) / ((double)(getN(child)))));
		if (Constants.PROGRESSIVE_BIAS){
			Move m = expandedMoves.get(child);
			ucb += ((Constants.BIAS_CONSTANT * h.get(m)) / (double)(getN(child)));
		} else if (Constants.MOVE_CATEGORY_BIAS){
			ucb += ((Constants.BIAS_CONSTANT * expandedMoves.get(child).getTransitionProbability()) / (double)(getN(child)));
		}
		return ucb;
	}
	
	public int getUnpruningWindow(){
		return this.windowSize;
	}
	
	public int getBestMoveIndex(){
		if (Constants.DECISIVE_MOVES && decisive != null){
			return expandedMoves.indexOf(decisive);
		}
		double bestUCB = getUCB(0);
		int bestIndex = 0;
		for (int i = 1; i < expandedMoves.size(); i++){
			double ucb = getUCB(i);
			if (ucb > bestUCB){
				bestUCB = ucb;
				bestIndex = i;
			}
		}
		return bestIndex;
	}
	
	public Move getMoveAt(int idx){
		return expandedMoves.get(idx);
	}
	
	public int getNewestIndex(){
		return expandedMoves.size() - 1;
	}
	
	public Move expandRandomMove(){
		Move m = unexpandedMoves.pollRandom(Constants.RANDOM);
		expandedMoves.add(m);
		n.add(1);
		w.add(0.0);
		moveIndices.put(m, expandedMoves.size() - 1);
		return m;
	}
	
	public Move expandBestMove(){
		Move m = null;
		if (decisive != null && Constants.DECISIVE_MOVES && !expandedMoves.contains(decisive)){
			m = decisive;
		} else if (Constants.PROGRESSIVE_UNPRUNING){
			double bestH = Integer.MIN_VALUE;
			for (Move mv : unexpandedMoves){
				double curH = h.get(mv);
				if (curH > bestH){
					bestH = curH;
					m = mv;
				}
			}
		} else if (Constants.MOVE_CATEGORY_UNPRUNING){
			double bestProb = 0.0;
			for (int i = 0; i < unexpandedMoves.size(); i++){
				Move mv = unexpandedMoves.get(i);
				if (mv.getTransitionProbability() > bestProb){
					bestProb = mv.getTransitionProbability();
					m = mv;
				}
			}
		} else {
			return expandRandomMove();
		}
		if (m != null){
			unexpandedMoves.remove(m);
			expandedMoves.add(m);
			n.add(1);
			w.add(0.0);
			moveIndices.put(m, expandedMoves.size() - 1);
		}
		return m;
	}
}
	
