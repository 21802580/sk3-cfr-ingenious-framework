package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import akka.routing.BroadcastPool;
import loa.logic.Board;
import player.leaf.messages.SimulationRequestMessage;
import player.leaf.messages.SimulationResponseMessage;
import statics.Constants;

public class LeafPlayoutSimulator extends AbstractActor {
	Cluster cluster = Cluster.get(getContext().system());
	private ActorRef router;
	private ActorRef traverser;
	private double totalResult;
	private int numResultsReceived;
	
	public LeafPlayoutSimulator() {
		router = getContext().actorOf(new BroadcastPool(Constants.NUM_WORKERS_PER_RANK)
					.props(Props.create(Worker.class, () -> new Worker()))
					.withDispatcher("akka.local-router-dispatcher"), "router" + getSelf().hashCode());
		numResultsReceived = 0;
		totalResult = 0;
	}
	
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	@Override
	public void postStop() {
		cluster.leave(cluster.selfAddress());
	}
	
	public void register(Member member) {
		int p = Constants.PLAYER_COLOUR - 1;
		getContext().actorSelection(member.address() + "/user/leaf_master-" + p).tell("simulator_registration", getSelf());
	}
	
	private void addResult(double reward){
		totalResult += reward;
		if (++numResultsReceived == Constants.NUM_WORKERS_PER_RANK){
			traverser.tell(new SimulationResponseMessage(totalResult), getSelf());
			numResultsReceived = 0;
			totalResult = 0;
		}
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SimulationRequestMessage.class, msg -> {
					traverser = getSender();
					router.tell(msg, getSelf());
				})
				.match(Double.class, msg -> {
					addResult(msg);
				})
				.match(MemberUp.class, msg -> {
					register(msg.member());
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
	
	private class Worker extends AbstractActor {
		
		private void doPlayout(SimulationRequestMessage msg){
			Board b = msg.getBoard();
			double reward = b.doPlayout();
			getSender().tell(reward, getSelf());
		}

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.match(SimulationRequestMessage.class, msg ->{
						doPlayout(msg);
					})
					.matchAny(msg -> {
						unhandled(msg);
					})
					.build();
		}
		
	}

}
