package player.leaf;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.logic.Board;
import loa.logic.Move;
import player.core.LOAPlayer;
import player.core.MoveTranslation;
import player.leaf.messages.SearchStartMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import statics.Constants;
import statics.MoveTableInitialiser;

public class LeafPlayer implements LOAPlayer {
	private Board board;
	protected ActorSystem system;
	protected ActorRef master;

	public LeafPlayer(String seedHost, String seedPort){
		MoveTableInitialiser.initialise();
		int p = Constants.PLAYER_COLOUR - 1;
		Config config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + seedHost).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + seedPort)).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [leaf_master-" + p + "]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://LeafSystem-" + p + "@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("LeafSystem-" + p, config);
		master = system.actorOf(Props.create(LeafMaster.class)
				.withDispatcher("akka.control-aware-dispatcher"), "leaf_master-" + p);
		board = new Board();
	}
	
	@Override
	public String genMove(long timeout) {
		Timeout t = new Timeout(Duration.create(60 * 20, "seconds"));
		Future<Object> fut = Patterns.ask(master, new SearchStartMessage(board), t);
		Move m = null;
		try {
			m = (Move) Await.result(fut, t.duration());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		board.applyMove(m);
		return MoveTranslation.getStringFromMove(m);
	}

	@Override
	public void playMove(String move) {
		if (move.equals("pass")){
			return;
		} else if (move.equals("resign")){
			master.tell("match_over", null);
		} else {
			Move m = MoveTranslation.getMoveFromString(move, board);
			board.applyMove(m);
		}
	}

	@Override
	public void gameOver(int terminationCode) {
		system.stop(master);
	}
	
}
