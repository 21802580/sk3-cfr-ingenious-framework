package player.leaf.messages;

import loa.logic.Board;

public class SimulationRequestMessage {
	private Board board;
	
	public SimulationRequestMessage(Board b){
		this.board = b;
	}
	
	public Board getBoard(){
		return this.board;
	}
}
