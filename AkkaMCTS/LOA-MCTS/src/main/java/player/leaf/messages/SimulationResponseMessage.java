package player.leaf.messages;

public class SimulationResponseMessage {
	private final double reward;
	
	public SimulationResponseMessage(double score){
		this.reward = score;
	}
	
	public double getReward(){
		return this.reward;
	}
}
