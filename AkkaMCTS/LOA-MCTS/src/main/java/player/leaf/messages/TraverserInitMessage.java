package player.leaf.messages;

import akka.actor.ActorRef;
import loa.logic.Board;

public class TraverserInitMessage {
	private final ActorRef[] simulators;
	private Board b;
	
	public TraverserInitMessage(ActorRef[] simulators, Board b){
		this.simulators = simulators;
		this.b = b;
	}
	
	public ActorRef[] getSimulators(){
		return this.simulators;
	}
	
	public Board getBoard(){
		return this.b;
	}
}
