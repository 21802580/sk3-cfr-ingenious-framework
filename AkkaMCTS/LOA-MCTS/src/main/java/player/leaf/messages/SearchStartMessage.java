package player.leaf.messages;

import loa.logic.Board;

public class SearchStartMessage {
	private Board board;
	
	public SearchStartMessage(Board b){
		this.board = b;
	}
	
	public Board getBoard(){
		return this.board;
	}
}
