package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import loa.logic.Board;
import loa.logic.Move;
import player.leaf.messages.SearchStartMessage;
import player.leaf.messages.TraverserInitMessage;
import player.serial.SerialNode;
import statics.Constants;

public class LeafMaster extends AbstractActor {
	private ActorRef returnActor;
	private ActorRef traverser;
	private ActorRef[] simulators;
	private Board board;
	private int numSimulatorsConnected;
	private boolean startReceived;
	Cluster cluster = Cluster.get(getContext().system());
	
	public LeafMaster(){
		numSimulatorsConnected = 0;
		simulators = new ActorRef[Constants.NUM_SEARCH_RANKS];
		startReceived = false;
	}
	
	private boolean allConnected(){
		return (numSimulatorsConnected == Constants.NUM_SEARCH_RANKS) && 
				(traverser != null);
	}
	
	@Override
	public void postStop() {
		getContext().system().stop(traverser);
		for (ActorRef a : simulators){
			getContext().system().stop(a);
		}
		cluster.leave(cluster.selfAddress());
	}
	
	private void initSearch(SearchStartMessage ssm){
		returnActor = getSender();
		board = ssm.getBoard();
		startReceived = true;
		startIfReady();
	}

	private void startIfReady(){
		if (allConnected() && startReceived){
			traverser.tell(new TraverserInitMessage(simulators, board), getSelf());
		}
	}
	
	private void registerTraverser(){
		traverser = getSender();
		getContext().watch(traverser);
		startIfReady();
	}
	
	private void registerSimulator(){
		ActorRef simulator = getSender();
		getContext().watch(simulator);
		simulators[numSimulatorsConnected++] = simulator;
		startIfReady();
	}
	
	private void sendMove(SerialNode node){
		System.out.println("Playouts: " + node.getVisits());
		for (int i = 0; i < node.getExpandedMoves().size(); i++){
			Move m = node.getMoveAt(i);
			System.out.println(m + " : " + node.getUCB(i));
		}
		returnActor.tell(node.getMoveAt(node.getBestMoveIndex()), getSelf());
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SearchStartMessage.class, msg -> {
					initSearch(msg);
				})
				.match(SerialNode.class, msg -> {
					sendMove(msg);
				})
				.matchEquals("simulator_registration", msg -> {
					registerSimulator();
				})
				.matchEquals("traverser_registration", msg -> {
					registerTraverser();
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
