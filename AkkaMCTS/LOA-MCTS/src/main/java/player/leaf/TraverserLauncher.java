package player.leaf;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import statics.MatchConfig;
import statics.MoveTableInitialiser;

public class TraverserLauncher {
	public static void main(String[] args){
		String seedHost = args[0];
		String seedPort = args[1];
		int playerNumber = Integer.parseInt(args[2]);
		String cfgPath = args[3];
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MatchConfig cfg = new MatchConfig(cfgPath);
		cfg.populatePlayerConfig(playerNumber);
		MoveTableInitialiser.initialise();
		Config config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + 0)).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [leaf_traverser-" + playerNumber + "]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://LeafSystem-" + playerNumber + "@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("LeafSystem-" + playerNumber, config);
		system.actorOf(Props.create(LeafTreeTraverser.class)
				.withDispatcher("akka.control-aware-dispatcher"), "leaf_traverser-" + playerNumber);
	}
}
