package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import loa.logic.Move;
import player.leaf.messages.SimulationRequestMessage;
import player.leaf.messages.SimulationResponseMessage;
import player.leaf.messages.TraverserInitMessage;
import player.serial.SerialMCTS;
import player.serial.SerialNode;
import statics.Constants;

public class LeafTreeTraverser extends AbstractActor {
	private ActorRef[] simulators;
	private ActorRef master;
	private SerialMCTS mcts;
	private long startTime;
	private int numResultsReceived;
	private double totalW;
	private SerialNode currentNode;
	Cluster cluster = Cluster.get(getContext().system());
	
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	@Override
	public void postStop() {
		cluster.leave(cluster.selfAddress());
	}
	
	public void register(Member member) {
		int p = Constants.PLAYER_COLOUR - 1;
		getContext().actorSelection(member.address() + "/user/leaf_master-" + p).tell("traverser_registration", getSelf());
	}
	
	private void genMove(TraverserInitMessage msg){
		this.master = getSender();
		this.startTime = System.currentTimeMillis();
		this.simulators = msg.getSimulators();
		this.mcts = new SerialMCTS(msg.getBoard());
		this.currentNode = new SerialNode(null, null, msg.getBoard());
		doMCTS();
	}
	
	private void doMCTS(){
		this.numResultsReceived = 0;
		this.totalW = 0;
		if (System.currentTimeMillis() - startTime > Constants.TIMEOUT - 100){
			master.tell(currentNode, getSelf());
			return;
		}
		currentNode = mcts.treePolicy(currentNode);
		requestPlayouts();
	}
	
	private void requestPlayouts(){
		for (ActorRef a : simulators){
			a.tell(new SimulationRequestMessage(mcts.getCurrentBoard()), getSelf());
		}
	}
	
	private void addNewPlayoutResult(SimulationResponseMessage msg){
		totalW += msg.getReward();
		if (++numResultsReceived == simulators.length){
			// All playouts are complete. Backup result while adding the number of simulations performed to each
			// node's visit count (the BasicMCTS object only increments visits at each iteration)
			int visits = (Constants.NUM_SEARCH_RANKS * Constants.NUM_WORKERS_PER_RANK) - 1;
			currentNode.addVisits(visits);
			SerialNode parent = currentNode.getParent();
			while (parent != null){
				Move child = currentNode.getLastMove();
				int idx = parent.getChildIndex(child);
				parent.addReward(totalW, child);
				parent.addN(idx, visits);
				parent.addVisits(visits);
				currentNode = parent;
				parent = currentNode.getParent();
				mcts.getCurrentBoard().undoMove();
			}
			doMCTS();
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(TraverserInitMessage.class, msg -> {
					genMove(msg);
				})
				.match(MemberUp.class, msg -> {
					register(msg.member());
				})
				.match(SimulationResponseMessage.class, msg ->{
					addNewPlayoutResult(msg);
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
