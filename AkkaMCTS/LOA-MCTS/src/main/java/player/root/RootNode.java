package player.root;

import java.util.ArrayList;

import loa.logic.Board;
import loa.logic.Move;
import player.serial.SerialNode;
import statics.Constants;

public class RootNode extends SerialNode {
	private int hash;
	private int tDelta;
	private ArrayList<Integer> nDelta;
	private ArrayList<Double> wDelta;
	
	public RootNode(SerialNode parent, Move lastMove, Board b, int hash) {
		super(parent, lastMove, b);
		this.hash = hash;
		this.tDelta = 0;
		this.nDelta = new ArrayList<Integer>();
		this.wDelta = new ArrayList<Double>();
	}

	
	public ArrayList<Integer> getNDelta(){
		return nDelta;
	}
	
	public ArrayList<Double> getWDelta(){
		return wDelta;
	}
	
	public int getTDelta(){
		return tDelta;
	}
	
	public int getHash(){
		return this.hash;
	}
	
	public void zeroDeltas(int idx){
		for (int i = 0; i < nDelta.size(); i++){
			nDelta.set(i, 0);
			wDelta.set(i, 0.0);
		}
		tDelta = 0;
	}
	
	@Override
	public void incVisits(){
		super.incVisits();
		tDelta++;
	}
	
	@Override
	public void addReward(double reward, Move child){
		//n.set(idx, n.get(idx) + 1);
		int idx = moveIndices.get(child);
		w.set(idx, w.get(idx) + reward);
		wDelta.set(idx, wDelta.get(idx) + reward);
	}
	
	@Override
	public void incN(int idx){
		n.set(idx, n.get(idx) + 1);
		nDelta.set(idx, nDelta.get(idx) +1);
	}
	
	/*
	 * Expand the provided move and return its index. Used for cases when a duplicated node
	 * has not expanded the required child.
	 */
	public int expandMove(Move m){
		unexpandedMoves.remove(m);
		expandedMoves.add(m);
		n.add(0);
		w.add(0.0);
		nDelta.add(1);
		wDelta.add(0.0);
		moveIndices.put(m, expandedMoves.size() - 1);
		return expandedMoves.size() - 1;
	}

	@Override
	public Move expandRandomMove(){
		Move m = super.expandRandomMove();
		nDelta.add(0);
		wDelta.add(0.0);
		return m;
	}

	@Override
	public Move expandBestMove(){
		Move m = super.expandBestMove();
		nDelta.add(0);
		wDelta.add(0.0);
		return m;
	}
}
