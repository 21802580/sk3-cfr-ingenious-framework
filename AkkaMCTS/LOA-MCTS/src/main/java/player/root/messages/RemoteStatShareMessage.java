package player.root.messages;

import loa.logic.Move;
import statics.Constants;

public class RemoteStatShareMessage {
	private final int hash;
	private final Move moveMade;
	private final Move child;
	private final int n;
	private final double w;
	private final boolean player; // Black = true, white = false
	
	public RemoteStatShareMessage(LocalStatShareMessage msg){
		this.hash = msg.getHash();
		this.moveMade = msg.getMoveMade();
		this.child = msg.getChild();
		this.n = msg.getN();
		this.w = msg.getW();
		this.player = msg.getPlayer() == Constants.PIECE_BLACK ? true : false;
	}
	
	public int getHash(){
		return hash;
	}

	public Move getMoveMade() {
		return moveMade;
	}
	
	public Move getChild(){
		return this.child;
	}

	public int getN() {
		return n;
	}

	public double getW() {
		return w;
	}
	
	public int getPlayer(){
		return player ? Constants.PIECE_BLACK : Constants.PIECE_WHITE;
	}
}
