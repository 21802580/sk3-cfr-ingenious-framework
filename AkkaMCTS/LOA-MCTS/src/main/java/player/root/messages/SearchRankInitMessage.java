package player.root.messages;

import akka.actor.ActorRef;
import akka.dispatch.ControlMessage;

public class SearchRankInitMessage implements ControlMessage{
	private final ActorRef[] searchRanks;
	
	public SearchRankInitMessage(ActorRef[] searchRanks){
		this.searchRanks = searchRanks;
	}
	
	public ActorRef[] getSearchRanks(){
		return this.searchRanks;
	}
}
