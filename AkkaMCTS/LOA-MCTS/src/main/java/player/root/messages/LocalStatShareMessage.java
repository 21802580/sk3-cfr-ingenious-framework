package player.root.messages;

import akka.actor.ActorRef;
import loa.logic.Move;
import statics.Constants;

public class LocalStatShareMessage {
	private final int hash;
	private final Move moveMade;
	private final Move child;
	private final int n;
	private final double w;
	private final boolean player; // Black = true, white = false
	private final ActorRef sender;
	
	public LocalStatShareMessage(int hash, Move moveMade, Move child, int n, double w, int player, ActorRef sender){
		this.hash = hash;
		this.moveMade = moveMade;
		this.child = child;
		this.n = n;
		this.w = w;
		this.player = player == Constants.PIECE_BLACK ? true : false;
		this.sender = sender;
	}
	
	public int getHash(){
		return hash;
	}

	public Move getMoveMade() {
		return moveMade;
	}
	
	public Move getChild(){
		return this.child;
	}

	public int getN() {
		return n;
	}

	public double getW() {
		return w;
	}
	
	public ActorRef getSender(){
		return sender;
	}
	
	public int getPlayer(){
		return player ? Constants.PIECE_BLACK : Constants.PIECE_WHITE;
	}
}
