package player.root;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import loa.logic.Board;
import loa.logic.Move;
import player.leaf.messages.SearchStartMessage;
import player.root.messages.SearchRankInitMessage;
import player.serial.SerialNode;
import statics.Constants;

public class RootMaster extends AbstractActor {
	private ActorRef returnActor;
	private ActorRef[] searchRanks;
	private Board board;
	private int numSearchRanksConnected;
	private boolean startReceived;
	Cluster cluster = Cluster.get(getContext().system());
	
	public RootMaster(){
		numSearchRanksConnected = 0;
		searchRanks = new ActorRef[Constants.NUM_SEARCH_RANKS];
		startReceived = false;
	}
	
	private boolean allConnected(){
		return (numSearchRanksConnected == Constants.NUM_SEARCH_RANKS);
	}
	
	@Override
	public void postStop() {
		for (ActorRef a : searchRanks){
			getContext().system().stop(a);
		}
		cluster.leave(cluster.selfAddress());
	}
	
	private void initSearch(SearchStartMessage ssm){
		returnActor = getSender();
		board = ssm.getBoard();
		startReceived = true;
		startIfReady();
	}

	private void startIfReady(){
		if (allConnected() && startReceived){
			for (ActorRef a : searchRanks){
				if (board.movesMade.size() == 0 || board.movesMade.size() == 1){
					// Send references for search ranks if it is the first search
					a.tell(new SearchRankInitMessage(searchRanks), getSelf());
				}
				a.tell(new SearchStartMessage(board), getSelf());
			}
		}
	}
	
	private void registerSearchRank(){
		ActorRef sr = getSender();
		getContext().watch(sr);
		searchRanks[numSearchRanksConnected++] = sr;
		startIfReady();
	}
	
	private void sendMove(SerialNode node){
		System.out.println("Playouts: " + node.getVisits());
		for (int i = 0; i < node.getExpandedMoves().size(); i++){
			Move m = node.getMoveAt(i);
			System.out.println(m + " : " + node.getUCB(i));
		}
		returnActor.tell(node.getMoveAt(node.getBestMoveIndex()), getSelf());
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SearchStartMessage.class, msg -> {
					initSearch(msg);
				})
				.match(SerialNode.class, msg -> {
					sendMove(msg);
				})
				.matchEquals("root_registration", msg -> {
					registerSearchRank();
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
