package player.root;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import loa.logic.Board;
import loa.logic.Move;
import player.leaf.messages.SearchStartMessage;
import player.root.messages.LocalStatShareMessage;
import player.root.messages.PerformIterationMessage;
import player.root.messages.RemoteStatShareMessage;
import player.serial.SerialMCTS;
import player.serial.SerialNode;
import statics.Constants;

public class RootWorker extends AbstractActor {
	private ActorRef searchRank;
	private HashMap<Integer, RootNode> nodes;
	private SerialMCTS mcts;
	private RootNode currentNode;
	private Zobrist zobrist;
	private int totalPlayouts;
	private long startTime;
	private long lastSyncTime;
	
	private void genMove(SearchStartMessage msg){
		searchRank = getSender();
		totalPlayouts = 0;
		nodes = new HashMap<Integer, RootNode>();
		Board b = msg.getBoard();
		mcts = new SerialMCTS(b);
		zobrist = new Zobrist();
		int rootHash = zobrist.getHash(b.getState());
		currentNode = new RootNode(null, null, b, rootHash);
		nodes.put(rootHash, currentNode);
		startTime = System.currentTimeMillis();
		lastSyncTime = System.currentTimeMillis();
		performIteration();
	}
	
	private void performIteration(){
		if (System.currentTimeMillis() - lastSyncTime > Constants.ROOT_DUPLICATION_PERIOD){
			// Synchronise near-root statistics
			lastSyncTime = System.currentTimeMillis();
			sendSync();
		}
		if (System.currentTimeMillis() - startTime < Constants.TIMEOUT - 100){
			// Alloted time has been used. Send result to master
			
		} else {
			// Perform MCTS iteration
			RootNode v = (RootNode)(mcts.treePolicy(currentNode));
			double reward = mcts.defaultPolicy();
			mcts.backup(v, reward);
			totalPlayouts++;
			getSelf().tell(new PerformIterationMessage(), getSelf());
		}
	}
	
	private void sendSync(){
		ArrayList<LocalStatShareMessage> toShare = getNodesToShare();
		for (LocalStatShareMessage msg : toShare){
			searchRank.tell(msg, getSelf());
		}
	}
	
	private int getOpponent(int player){
		if (player == Constants.PIECE_BLACK){
			return Constants.PIECE_WHITE;
		} else {
			return Constants.PIECE_BLACK;
		}
	}
	
	private ArrayList<LocalStatShareMessage> getNodesToShare(){
		LinkedList<RootNode> queue = new LinkedList<RootNode>();
		ArrayList<LocalStatShareMessage> messages = new ArrayList<LocalStatShareMessage>();
		queue.add(currentNode);
		RootNode leftMostNode = currentNode;
		int player = mcts.getCurrentBoard().getCurrentPlayer();
		int currentPly = 0;
		while(!queue.isEmpty()) {
			RootNode node = queue.remove();
			if (node == leftMostNode){
				// Depth increased
				currentPly++;
				player = getOpponent(player);
				if (currentPly == Constants.ROOT_DUPLICATION_PLY){
					break;
				}
			}
			int numChildren = node.getExpandedChildren().size();
			if (numChildren > 0){
				leftMostNode = (RootNode)node.getChildAt(0);
				for (int i = 0; i < numChildren; i++){
					RootNode child = (RootNode)node.getChildAt(i);
					queue.add(child);
					if (node.getN(i) / totalPlayouts > Constants.ROOT_DUPLICATION_THRESHOLD){
						LocalStatShareMessage msg = new LocalStatShareMessage(node.getHash(), node.getLastMove(), 
								node.getMoveAt(i), node.getNDelta(i), node.getWDelta(i), player, getSelf());
						node.zeroDeltas(i);
						messages.add(msg);
					}
				}	
			}
		}
		return messages;
	}
	
	private void processLocalStatShare(LocalStatShareMessage msg){
		if (!msg.getSender().equals(getSelf())){
			incorporateStatistics(msg.getHash(), msg.getMoveMade(), msg.getChild(), 
					msg.getN(), msg.getW(), msg.getPlayer());
		}
	}
	
	private void processRemoteStatShare(RemoteStatShareMessage msg){
		incorporateStatistics(msg.getHash(), msg.getMoveMade(), msg.getChild(), 
				msg.getN(), msg.getW(), msg.getPlayer());
	}
	
	private void incorporateStatistics(int hash, Move parentMove, Move childMove, int n, double w, int player){
		RootNode node = nodes.get(hash);
		if (node == null){
			int parentHash = zobrist.getParentHash(hash, parentMove, getOpponent(player));
			RootNode parent = nodes.get(parentHash);
			parent.expandMove(parentMove);
			node = new RootNode(parent, parentMove, currentState, parentHash);
			parent.addChild(node, false);
			
		} else {
			
		}
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SearchStartMessage.class, msg -> {
					genMove(msg);
				})
				.match(PerformIterationMessage.class, msg -> {
					performIteration();
				})
				.match(LocalStatShareMessage.class, msg -> {
					processLocalStatShare(msg);
				})
				.match(RemoteStatShareMessage.class, msg -> {
					processRemoteStatShare(msg);
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}

}
