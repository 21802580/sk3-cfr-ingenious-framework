package player.root;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import akka.routing.BroadcastPool;
import player.leaf.messages.SearchStartMessage;
import player.root.messages.LocalStatShareMessage;
import player.root.messages.RemoteStatShareMessage;
import player.root.messages.SearchRankInitMessage;
import statics.Constants;

public class RootSearchRank extends AbstractActor {
	Cluster cluster = Cluster.get(getContext().system());
	private ActorRef[] remoteSearchRanks;
	private ActorRef router;
	

	public RootSearchRank() {
		router = getContext().actorOf(new BroadcastPool(Constants.NUM_WORKERS_PER_RANK)
					.props(Props.create(RootWorker.class, () -> new RootWorker()))
					.withDispatcher("akka.local-router-dispatcher"), "router" + getSelf().hashCode());
		remoteSearchRanks = new ActorRef[Constants.NUM_SEARCH_RANKS - 1];
	}
	
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	@Override
	public void postStop() {
		cluster.leave(cluster.selfAddress());
	}
	
	public void register(Member member) {
		int p = Constants.PLAYER_COLOUR - 1;
		getContext().actorSelection(member.address() + "/user/root_master-" + p).tell("root_registration", getSelf());
	}
	
	private void startSearch(SearchStartMessage msg){
		router.tell(msg, getSelf());
	}
	
	private void initSearchRanks(SearchRankInitMessage msg){
		ActorRef[] allSearchRanks = msg.getSearchRanks();
		int i = 0;
		for (ActorRef a : allSearchRanks){
			if (!a.equals(getSelf())){
				remoteSearchRanks[i++] = a;
			}
		}
	}
	
	private void sendStatsToRemoteRanks(LocalStatShareMessage msg){
		for (ActorRef a : remoteSearchRanks){
			a.tell(new RemoteStatShareMessage(msg), getSelf());
		}
		router.tell(msg, getSelf());
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SearchRankInitMessage.class, msg -> {
					initSearchRanks(msg);
				})
				.match(SearchStartMessage.class, msg -> {
					startSearch(msg);
				})
				.match(LocalStatShareMessage.class, msg -> {
					sendStatsToRemoteRanks(msg);
				})
				.match(RemoteStatShareMessage.class, msg -> {
					router.tell(msg, getSelf());
				})
				.match(MemberUp.class, msg -> {
					register(msg.member());
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
