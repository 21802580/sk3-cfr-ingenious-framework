package player.root;

import java.util.Random;

import loa.logic.Move;
import statics.Constants;

public class Zobrist {
	private int[][][] randoms;

	public Zobrist() {
		randoms = new int[8][8][3];
		Random r = new Random(12345);
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 3; k++){
					int hash = Math.abs(r.nextInt());
					randoms[i][j][k] = hash;
				}
			}
		}
	}
	
	public int getHash(int[][] board){
		int h = 0;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (board[i][j] != Constants.PIECE_EMPTY){
					h = h ^ randoms[i][j][board[i][j]];
				}
			}
		}
		return h;
	}

	public int getChildHash(int hash, Move m, int player){
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		int opponent = Constants.PIECE_BLACK;
		if (player == Constants.PIECE_BLACK){
			opponent = Constants.PIECE_WHITE;
		}	
		hash ^= randoms[rf][cf][player];
		hash ^= randoms[rt][ct][player];		
		if (m.isCapture()){
			hash ^= randoms[rt][ct][opponent];
		}
		return hash;
	}

	public int getParentHash(int hash, Move m, int player){
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		int opponent = Constants.PIECE_BLACK;
		if (player == Constants.PIECE_BLACK){
			opponent = Constants.PIECE_WHITE;
		}
		hash ^= randoms[rt][ct][player];
		hash ^= randoms[rf][cf][player];
		if (m.isCapture()){
			hash ^= randoms[rt][ct][opponent];
		}
		return hash;
	}
}
