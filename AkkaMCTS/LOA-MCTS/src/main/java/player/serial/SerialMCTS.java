package player.serial;
import java.util.ArrayList;

import loa.logic.Board;
import loa.logic.Move;
import player.core.MoveTranslation;
import player.tds.core.TDSTranspositionTable;
import statics.Constants;
import statics.MoveTableInitialiser;

public class SerialMCTS {
	private Board currentState;
	private int maxDepth;
	private SerialNode n;
	private ArrayList<SerialNode> nodes;
	public int numSims;
	public int numSelections;

	public static void main(String[] args){
		SerialMCTS b = new SerialMCTS();
		TDSTranspositionTable tt = new TDSTranspositionTable(100);
		Board t = new Board();
		MoveTableInitialiser.initialise();
		b.getMove(1000, Constants.PIECE_BLACK);
		System.out.println(b.numSims);
	}

	public SerialMCTS(){
		this.currentState = new Board();
		this.maxDepth = 0;
		this.numSelections = 0;
		this.nodes = new ArrayList<SerialNode>();
		this.n = null;
	}
	
	public SerialMCTS(Board board){
		this.currentState = board;
		this.maxDepth = 0;
		this.numSelections = 0;
		this.nodes = new ArrayList<SerialNode>();
		this.n = null;
	}

//	private void produceDotFile(){
//		Writer writer = null;
//		try {
//			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tree.dot"), "utf-8"));
//			writer.write("digraph G {\n"
//					+ "    nodesep=0.3;\n"
//					+ "    ranksep=0.2;\n"
//					+ "    margin=0.1;\n"
//					+ "    node [shape=circle];\n"
//					+ "    edge [arrowsize=0.8];\n");
//			for (BasicNode n : nodes){
//				if (n.getParent() == null){
//					continue;
//				}
//				writer.write(nodes.indexOf(n.getParent()) + " -> " + nodes.indexOf(n)
//						+ " [label=" + n.getParent().getN(n.getParent().getExpandedChildren().indexOf(n)) + "];\n");	
//			}
//			writer.write("}");
//		} catch (UnsupportedEncodingException e) {
//			// TODO Auto-generated catch bloc
//			e.printStackTrace();
//		} catch (FileNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		} finally {
//			try {writer.close();} catch (Exception ex) {/*ignore*/}
//		}        
//	}

	public Board getCurrentBoard(){
		return this.currentState;
	}

	public void applyMove(String move){
		if (move.equals("pass") || (move.equals("resign"))){
			return;
		}
		Move m = MoveTranslation.getMoveFromString(move, currentState);
		currentState.applyMove(m);
	}

	public String getMove(long timeout, int myColour){
		n = new SerialNode(null, null, currentState);
		nodes.add(n);
		long time = System.currentTimeMillis();
		int playouts = 0;
		while (System.currentTimeMillis() - time < timeout - 100){
			SerialNode v = treePolicy(n);
			double reward = defaultPolicy();
			backup(v, reward);
			playouts++;
		}
		Move m = n.getMoveAt(n.getBestMoveIndex());
		currentState.applyMove(m);
		numSims += playouts;
		return MoveTranslation.getStringFromMove(m);
	}

	public double defaultPolicy(){
		double reward = 0;
		int winner = currentState.doPlayout();
		if (winner == Constants.PLAYER_COLOUR){
			reward = 1;
		} else if (winner == Constants.PIECE_EMPTY){
			reward = 0.5;
		} else {
			reward = 0;
		}
		return reward;
	}

	public SerialNode treePolicy(SerialNode n){
		n.incVisits();
		while(!n.isLeaf()){
			numSelections++;
			int bestChildIndex = n.getBestChildIndex();
			SerialNode child = n.getChildAt(bestChildIndex);
			Move moveMade = child.getLastMove();
			n.incN(bestChildIndex);
			double childScore = n.getUCB(bestChildIndex);
			if ((childScore < Constants.FPU) && (!n.isFullyExpanded())){
				break;
			}
			currentState.applyMove(moveMade);
			n = child;
			n.incVisits();
		}
		return expandNode(n);
	}
	
	public SerialNode expandNode(SerialNode parent){
		if (currentState.isTerminal() != -1){
			return parent;
		}
		Move m = parent.expandBestMove();
		boolean decisive = false;
		if (Constants.DECISIVE_MOVES){
			decisive = currentState.moveIsDecisive(m);
		}
		currentState.applyMove(m);
		SerialNode c = new SerialNode(parent, m, currentState);
		nodes.add(c);
		parent.addChild(c, decisive);
		parent.incN(parent.getNewestIndex());
		c.incVisits();
		return c;
	}

	public void backup(SerialNode v, double reward){
		SerialNode parent = v.getParent();
		int depth = 1;
		while (parent != null){
			Move child = v.getLastMove();
			parent.addReward(reward, child);
			v = parent;
			parent = v.getParent();
			currentState.undoMove();
			depth++;
		}
		if (depth > maxDepth){
			maxDepth = depth;
		}
	}
}
