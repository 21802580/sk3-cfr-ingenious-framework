package player.serial;

import player.core.LOAPlayer;
import statics.Constants;

public class SerialPlayer implements LOAPlayer{
	private int myColour = Constants.PLAYER_COLOUR;
	private SerialMCTS basic;

	public SerialPlayer(){
		basic = new SerialMCTS();
	}
	
	@Override
	public String genMove(long timeout) {
		return basic.getMove(timeout, myColour);	
	}

	@Override
	public void playMove(String move) {
		basic.applyMove(move);
	}

	@Override
	public void gameOver(int terminationCode) {
		
	}
	
}
