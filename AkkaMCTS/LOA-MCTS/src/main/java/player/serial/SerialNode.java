package player.serial;

import java.util.ArrayList;

import loa.logic.Board;
import loa.logic.Move;
import player.core.MCTSNode;

public class SerialNode extends MCTSNode {
	private SerialNode parent;
	protected ArrayList<SerialNode> expandedChildren;
	protected int decisiveIndex;
	
	public SerialNode(SerialNode parent, Move lastMove, Board b){
		super(lastMove, b);
		this.parent = parent;
		this.expandedChildren = new ArrayList<SerialNode>();
		this.decisiveIndex = -1;
	}
	
	public ArrayList<SerialNode> getExpandedChildren(){
		return this.expandedChildren;
	}

	public void addChild(SerialNode child, boolean isDecisive){
		expandedChildren.add(child);
		if (isDecisive){
			decisiveIndex = expandedChildren.size() - 1;	
		}
	}
	
	public int getBestChildIndex(){
		if (decisiveIndex != -1){
			return decisiveIndex;
		}
		double bestUCB = getUCB(0);
		//BasicNode bestChild = expandedChildren.get(0);
		int bestIndex = 0;
		for (int i = 1; i < expandedChildren.size(); i++){
			double ucb = getUCB(i);
			if (ucb > bestUCB){
				bestUCB = ucb;
				bestIndex = i;
				//bestChild = expandedChildren.get(i);
			}
		}
		return bestIndex;
	}
	
	public SerialNode getChildAt(int index){
		return expandedChildren.get(getBestChildIndex());
	}

	public SerialNode getParent() {
		return parent;
	}
}
