package player.tds.schaefers;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import player.tds.messages.control.BroadcastRankInitMessage;
import player.tds.messages.duplication.BroadcastInitiationMessage;
import player.tds.messages.duplication.NodeBroadcastMessage;
import player.tds.messages.duplication.NodeRemovalMessage;
import player.tds.messages.duplication.RemovalInitiationMessage;
import statics.Constants;

public class SchaefersBroadcastRank extends AbstractActor{
	private ActorRef[] remoteBroadcasters;
	private ActorRef[] searchRanks;
	Cluster cluster = Cluster.get(getContext().system());
	
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	@Override
	public void postStop() {
		cluster.leave(cluster.selfAddress());
	}
	
	public void register(Member member) {
		getContext().actorSelection(member.address() + "/user/" + Constants.MASTER_NAME).tell(
				Constants.BROADCAST_RANK_NAME + "_registration", getSelf());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(BroadcastRankInitMessage.class, msg -> {
					initBroadcaster(msg);
				})
				.match(BroadcastInitiationMessage.class, msg -> {
					initiateBroadcast(msg);
				})
				.match(NodeBroadcastMessage.class, msg -> {
					receiveBroadcast(msg);
				})
				.match(RemovalInitiationMessage.class, msg -> {
					initiateRemoval(msg);
				})
				.match(NodeRemovalMessage.class, msg -> {
					receiveRemoval(msg);
				})
				.match(MemberUp.class, msg -> {
					register(msg.member());
				})
				.build();
	}
	
	private void initBroadcaster(BroadcastRankInitMessage msg){
		ActorRef[] allBroadcasters = msg.getBroadcasters();
		this.remoteBroadcasters = new ActorRef[allBroadcasters.length - 1];
		int i = 0;
		for (ActorRef a : allBroadcasters){
			if (!a.equals(getSelf())){
				remoteBroadcasters[i++] = a;
			}
		}
		this.searchRanks = msg.getSearchRanks();
	}
	
	private void initiateRemoval(RemovalInitiationMessage msg){
		NodeRemovalMessage nrm = new NodeRemovalMessage(msg.getHash(), msg.getPlayer());
		for (int i = 0; i < remoteBroadcasters.length; i++){
			remoteBroadcasters[i].tell(nrm, getSelf());
		}
		ActorRef sendingRank = getSender();
		for (int i = 0; i < searchRanks.length; i++){
			ActorRef sr = searchRanks[i];
			if (!sr.equals(sendingRank)){
				searchRanks[i].tell(nrm, getSelf());	
			}
		}
	}
	
	private void receiveRemoval(NodeRemovalMessage msg){
		for (int i = 0; i < searchRanks.length; i++){
			searchRanks[i].tell(msg, getSelf());
		}
	}
	
	/*
	 * Send the provided node to all other broadcasters and search ranks associated with
	 * this broadcaster
	 */
	private void initiateBroadcast(BroadcastInitiationMessage msg){
		SchaefersNode node = msg.getNode();
		int player = msg.getPlayer();
		for (int i = 0; i < remoteBroadcasters.length; i++){
			remoteBroadcasters[i].tell(new NodeBroadcastMessage(node, player), getSelf());
		}
		ActorRef sendingRank = getSender();
		for (int i = 0; i < searchRanks.length; i++){
			ActorRef sr = searchRanks[i];
			if (!sr.equals(sendingRank)){
				searchRanks[i].tell(new NodeBroadcastMessage(new SchaefersNode(node), player), getSelf());	
			}
		}
	}
	
	/*
	 * Distribute the given node among all search ranks associated with
	 * this broadcaster
	 */
	private void receiveBroadcast(NodeBroadcastMessage msg){
		SchaefersNode node = msg.getNode();
		int player = msg.getPlayer();
		for (int i = 0; i < searchRanks.length; i++){
			searchRanks[i].tell(new NodeBroadcastMessage(new SchaefersNode(node), player), getSelf());
		}
	}
}
