package player.tds.schaefers;

import akka.actor.ActorRef;
import loa.logic.Board;
import player.tds.core.AbstractSearchRank;
import player.tds.core.TDSNode;
import player.tds.core.TDSTranspositionTable;
import player.tds.core.TDSTranspositionTable.PutResult;
import player.tds.messages.control.BroadcasterSupplyMessage;
import player.tds.messages.control.InitialSearchMessage;
import player.tds.messages.control.SearchRankInitMessage;
import player.tds.messages.duplication.BroadcastInitiationMessage;
import player.tds.messages.duplication.NodeBroadcastMessage;
import player.tds.messages.duplication.NodeRemovalMessage;
import player.tds.messages.duplication.RemovalInitiationMessage;
import player.tds.messages.search.ReportDuplicateMessage;
import player.tds.messages.search.SearchDuplicateMessage;
import statics.Constants;

public class SchaefersSearchRank extends AbstractSearchRank {
	public static TDSTranspositionTable blackCache;
	public static TDSTranspositionTable whiteCache;
	private ActorRef broadcaster;
	
	public SchaefersSearchRank() {
		super();
		blackCache = new TDSTranspositionTable(Constants.CACHE_CAPACITY);
		whiteCache = new TDSTranspositionTable(Constants.CACHE_CAPACITY);
	}

	@Override
	public void processMessage(Object msg) {
		if (msg instanceof BroadcasterSupplyMessage){
			broadcaster = ((BroadcasterSupplyMessage)msg).getBroadcaster();
		} else if (msg instanceof BroadcastInitiationMessage){
			BroadcastInitiationMessage m = (BroadcastInitiationMessage)msg;
			addSharedNode(m.getNode(), m.getPlayer());
			broadcaster.tell(msg, getSelf());
		} else if (msg instanceof NodeBroadcastMessage){
			NodeBroadcastMessage m = (NodeBroadcastMessage)msg;
			addSharedNode(m.getNode(), m.getPlayer());
		} else if (msg instanceof SearchDuplicateMessage){
			router.tell(msg, getSelf());
		} else if (msg instanceof ReportDuplicateMessage){
			router.tell(msg, getSelf());
		} else if (msg instanceof NodeRemovalMessage){
			removeSharedNode((NodeRemovalMessage)msg);
		}
	}
	
	/*
	 * The initial search job is processed after being distributed to all search ranks.
	 * No need to process the job here or put the node into the transposition table
	 */
	@Override
	protected void processInitialSearchJob(InitialSearchMessage isj){
		startTimer();
	}
	
	@Override
	public void initWorker(SearchRankInitMessage wim){
		blackCache.newSearch();
		whiteCache.newSearch();
		super.initWorker(wim);
	}

	@Override
	public TDSNode getNewNode(Board b, int hash) {
		return new SchaefersNode(b, hash);
	}
	
	public static TDSTranspositionTable getCurrentCache(int currentPlayer){
		TDSTranspositionTable tt = SchaefersSearchRank.blackCache;
		if (currentPlayer == Constants.PIECE_WHITE){
			tt = SchaefersSearchRank.whiteCache;
		}
		return tt;
	}
	
	private void addSharedNode(SchaefersNode node, int player){
		int hash = node.getNodeHash();
		TDSTranspositionTable cache = getCurrentCache(player);
		PutResult result = cache.putIfAbsent(hash, node);
		SchaefersNode n = (SchaefersNode)result.getNode();
		int replacedHash = result.getReplacedHash();
		if (replacedHash != -1){
			// A node that was previously shared has been overwritten
			// Tell all other ranks to remove the duplicated node
			broadcaster.tell(new RemovalInitiationMessage(replacedHash, player), getSelf());
		}
		n.makeShared();
		cache.releaseNode(hash);
	}
	
	private void removeSharedNode(NodeRemovalMessage msg){
		int hash = msg.getHash();
		int player = msg.getPlayer();
		TDSTranspositionTable cache = getCurrentCache(player);
		cache.removeNode(hash);
	}

	@Override
	public void sendFinalNode() {
		TDSTranspositionTable cache = getCurrentCache(Constants.PLAYER_COLOUR);
		SchaefersNode n = (SchaefersNode)cache.getNode(rootHash);
		master.tell(n, getSelf());
		cache.releaseNode(rootHash);
	}
}
