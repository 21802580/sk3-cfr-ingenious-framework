package player.tds.schaefers;

import java.util.ArrayList;

import loa.logic.Board;
import loa.logic.Move;
import player.tds.core.TDSNode;
import statics.Constants;

public class SchaefersNode extends TDSNode {
	private ArrayList<Integer> nDelta;
	private ArrayList<Double> wDelta;
	private int tDelta;
	private boolean shared;

	public SchaefersNode(Board b, int nodeHash) {
		super(b, nodeHash);
		shared = false;
		tDelta = 0;
		nDelta = new ArrayList<Integer>(50);
		wDelta = new ArrayList<Double>(50);
	}

	public SchaefersNode(SchaefersNode node) {
		super(node);
		this.shared = node.shared;
		this.tDelta = node.tDelta;
		this.nDelta = new ArrayList<Integer>(node.nDelta);
		this.wDelta = new ArrayList<Double>(node.wDelta);
	}
	
	public boolean isShared(){
		return shared;
	}
	
	public void makeShared(){
		shared = true;
	}
	
	@Override
	public void addReward(double reward, Move child){
		if (shared){
			addDeltaReward(reward, child);
		} else {
			if (!moveIndices.containsKey(child)){
				System.out.println("COULD NOT ADD REWARD FOR CHILD " + child);
				System.out.println("HASH: " + nodeHash);
				System.out.println("EXPANDED MOVES: ");
				for (Move m : expandedMoves){
					System.out.println(m);
				}
				System.out.println("UNEXPANDED MOVES: ");
				for (Move m : unexpandedMoves){
					System.out.println(m);
				}
			}
			int idx = moveIndices.get(child);
			w.set(idx, w.get(idx) + reward);
		}
	}
	
	@Override
	public void incN(int idx){
		if (shared){
			nDelta.set(idx, nDelta.get(idx) + 1);
		} else {
			n.set(idx, n.get(idx) + 1);
		}
	}

	@Override
	public int getN(int index){
		return n.get(index) + nDelta.get(index);
	}

	@Override
	public double getW(int index){
		return w.get(index) + wDelta.get(index);
	}
	
	@Override
	public int getVisits(){
		return t + tDelta;
	}
	
	@Override
	public void incVisits(){
		if (shared){
			tDelta++;
		} else {
			t++;
		}
		windowSize = 1 + (int)(Math.log(getVisits()) / Math.log(Constants.UNPRUNING_CONSTANT));	
	}
	
	public int getNDelta(int index){
		return nDelta.get(index);
	}
	
	private void addDeltaReward(double reward, Move child){
		int idx;
		try {
			idx = moveIndices.get(child);
		} catch (NullPointerException e){
			// This node has node expanded the move yet.
			idx = expandMove(child);
		}
		wDelta.set(idx, wDelta.get(idx) + reward);	
	}
	
	/*
	 * Expand the provided move and return its index. Used for cases when a duplicated node
	 * has not expanded the required child.
	 */
	private int expandMove(Move m){
		unexpandedMoves.remove(m);
		expandedMoves.add(m);
		n.add(0);
		w.add(0.0);
		nDelta.add(1);
		wDelta.add(0.0);
		moveIndices.put(m, expandedMoves.size() - 1);
		return expandedMoves.size() - 1;
	}

	@Override
	public Move expandRandomMove(){
		Move m = super.expandRandomMove();
		nDelta.add(0);
		wDelta.add(0.0);
		return m;
	}

	@Override
	public Move expandBestMove(){
		Move m = super.expandBestMove();
		nDelta.add(0);
		wDelta.add(0.0);
		return m;
	}
}
