package player.tds.schaefers;

import loa.logic.Board;
import loa.logic.Move;
import player.tds.core.AbstractSearchRank;
import player.tds.core.AbstractWorker;
import player.tds.core.TDSNode;
import player.tds.core.TDSTranspositionTable;
import player.tds.messages.duplication.BroadcastInitiationMessage;
import player.tds.messages.search.LocalReportMessage;
import player.tds.messages.search.LocalSearchMessage;
import player.tds.messages.search.RemoteSearchMessage;
import player.tds.messages.search.ReportDuplicateMessage;
import player.tds.messages.search.SearchDuplicateMessage;
import statics.Constants;

public class SchaefersWorker extends AbstractWorker {

	public SchaefersWorker(int rootHash, int rankID) {
		super(rootHash, rankID);
		System.out.println("RANK ID: " + rankID);
	}

	@Override
	public void processMessage(Object msg) {
		if (msg instanceof SearchDuplicateMessage){
			processDuplicatedSearchJob((SearchDuplicateMessage)msg);
		} else if (msg instanceof ReportDuplicateMessage){
			processDuplicatedReportJob((ReportDuplicateMessage)msg);
		} else {
			unhandled(msg);
		}
	}
	
	@Override
	public void processSearchJob(RemoteSearchMessage snm){
		int hash = snm.getNodeHash();
		Board b = snm.getBoard();
		int player = b.getCurrentPlayer();
		int turnNumber = snm.getTurnNumber();
		SchaefersNode node = (SchaefersNode)ttPutOrGet(hash, b);
		if ((node.getVisits() < Constants.PLAYOUT_THRESHOLD) && (b.movesMade.size() != turnNumber)){
			double reward = defaultPolicy(b);
			sendReportJob(b, hash, reward, turnNumber);
		} else if (node.isLeaf()){
			expandAndSend(node, b, turnNumber);
		} else {
			selectAndSend(node, b, turnNumber);
		}
		node.incVisits();
		if (!node.isShared() && node.getVisits() >= Constants.N_DUP){
			// Share node if the minimum number of visits is reached
			node.makeShared();
			getSender().tell(new BroadcastInitiationMessage(new SchaefersNode(node), player), getSelf());
		}
		ttRelease(hash, player);
	}

	
	@Override
	public void sendSearchJob(Board b, int parentHash, Move child, int turnNumber){
		int childHash = AbstractSearchRank.blackTT.getChildHash(parentHash, child, b.getCurrentPlayer());
		TDSTranspositionTable cache = SchaefersSearchRank.getCurrentCache(b.getCurrentPlayer());
		b.applyMove(child);
		if (cache.contains(childHash)){
			SearchDuplicateMessage sdnm = new SearchDuplicateMessage(b, childHash, turnNumber);
			getSender().tell(sdnm, getSelf());
		} else {
			LocalSearchMessage lsm = new LocalSearchMessage(b, childHash, turnNumber);
			getSender().tell(lsm, getSelf());	
		}
	}

	@Override
	public void sendReportJob(Board b, int childHash, double reward, int turnNumber){
		Move child = b.undoMove();
		int parentHash = AbstractSearchRank.blackTT.getParentHash(childHash, child, b.getCurrentPlayer());
		TDSTranspositionTable cache = SchaefersSearchRank.getCurrentCache(b.getCurrentPlayer());
		if (cache.contains(parentHash)){
			ReportDuplicateMessage rdnm = new ReportDuplicateMessage(b, child, parentHash, reward, turnNumber);
			getSender().tell(rdnm, getSelf());
		} else {
			LocalReportMessage lrm = new LocalReportMessage(b, child, parentHash, reward, turnNumber);
			getSender().tell(lrm, getSelf());
		}
	}
	
	public void processDuplicatedSearchJob(SearchDuplicateMessage snm){
		int hash = snm.getNodeHash();
		Board b = snm.getBoard();
		int player = b.getCurrentPlayer();
		int turnNumber = snm.getTurnNumber();
		SchaefersNode node = cacheGet(hash, player);
		if ((node.getVisits() < Constants.PLAYOUT_THRESHOLD) && (b.movesMade.size() != turnNumber)){
			double reward = defaultPolicy(b);
			sendReportJob(b, hash, reward, turnNumber);
		} else if (node.isLeaf()){
			expandAndSend(node, b, turnNumber);
		} else {
			selectAndSend(node, b, turnNumber);
		}
		node.incVisits();
		cacheRelease(hash, player);
	}

	public void processDuplicatedReportJob(ReportDuplicateMessage rm){
		int parentHash = rm.getParentHash();
		Board b = rm.getBoard();
		Move child = rm.getChild();
		int parentPlayer = b.getCurrentPlayer();
		int turnNumber = rm.getTurnNumber();
		double reward = rm.getReward();
		SchaefersNode node = cacheGet(parentHash, parentPlayer);
		node.addReward(reward, child);
		if (b.movesMade.size() > turnNumber){
			// Not at the root. Propagate the result
			sendReportJob(b, parentHash, reward, turnNumber);
		} else if (currentTurn == turnNumber){
			// At the root. Start a new search if the current search run is not complete
			selectAndSend(node, b, turnNumber);
			node.incVisits();
		}
		cacheRelease(parentHash, parentPlayer);
	}

	public void cacheRelease(int nodeHash, int player){
		TDSTranspositionTable cache = SchaefersSearchRank.getCurrentCache(player);
		cache.releaseNode(nodeHash);
	}

	public SchaefersNode cacheGet(int nodeHash, int player){
		TDSTranspositionTable cache = SchaefersSearchRank.getCurrentCache(player);
		return (SchaefersNode)cache.getNode(nodeHash);
	}

	@Override
	public TDSNode getNewNode(Board b, int hash) {
		return new SchaefersNode(b, hash);
	}

}
