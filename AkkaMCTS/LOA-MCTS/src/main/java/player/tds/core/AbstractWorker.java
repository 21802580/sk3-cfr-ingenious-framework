package player.tds.core;

import akka.actor.AbstractActor;
import loa.logic.Board;
import loa.logic.Move;
import player.tds.core.TDSTranspositionTable.PutResult;
import player.tds.messages.control.SearchCompleteMessage;
import player.tds.messages.search.LocalReportMessage;
import player.tds.messages.search.LocalSearchMessage;
import player.tds.messages.search.RemoteReportMessage;
import player.tds.messages.search.RemoteSearchMessage;
import statics.Constants;

public abstract class AbstractWorker extends AbstractActor {
	protected int rankID;
	protected int currentTurn;

	public AbstractWorker(int rootHash, int rankID){
		this.rankID = rankID;
		this.currentTurn = Constants.PLAYER_COLOUR == Constants.PIECE_BLACK ? 0 : 1;
	}

	@Override
	public Receive createReceive(){
		return receiveBuilder()
				.match(RemoteSearchMessage.class, msg -> {
//					System.out.println(this.hashCode() + " " + msg);
					processSearchJob(msg);
				})
				.match(RemoteReportMessage.class, msg -> {
//					System.out.println(this.hashCode() + " " + msg);
					processReportJob(msg);
				})
				.match(SearchCompleteMessage.class, msg -> {
					//System.out.println("ROUTEE COMPLETE");
					currentTurn += 2;
				})
				.matchAny(msg -> {
					processMessage(msg);
				})
				.build();
	}

	public void processSearchJob(RemoteSearchMessage snm){
		int hash = snm.getNodeHash();
		Board b = snm.getBoard();
		int player = b.getCurrentPlayer();
		int turnNumber = snm.getTurnNumber();
		TDSNode node = ttPutOrGet(hash, b);
		if ((node.getVisits() < Constants.PLAYOUT_THRESHOLD) && (b.movesMade.size() != turnNumber)){
			double reward = defaultPolicy(b);
			sendReportJob(b, hash, reward, turnNumber);
		} else if (node.isLeaf()){
			expandAndSend(node, b, turnNumber);
		} else {
			selectAndSend(node, b, turnNumber);
		}
		node.incVisits();
		ttRelease(hash, player);
	}

	public void processReportJob(RemoteReportMessage rm){
		int parentHash = rm.getParentHash();
		Board b = rm.getBoard();
		Move child = rm.getChild();
		int parentPlayer = b.getCurrentPlayer();
		int turnNumber = rm.getTurnNumber();
		double reward = rm.getReward();
		TDSNode node = null;
		node = ttGet(parentHash, parentPlayer);	
		node.addReward(reward, child);
		if (b.movesMade.size() > turnNumber){
			// Not at the root. Propagate the result
			sendReportJob(b, parentHash, reward, turnNumber);
		} else if (currentTurn == turnNumber){
			// At the root. Start a new search if the current search run is not complete
			selectAndSend(node, b, turnNumber);
			node.incVisits();
		}
		ttRelease(parentHash, parentPlayer);
	}

	protected int getOpponent(int player){
		if (player == Constants.PIECE_BLACK){
			return Constants.PIECE_WHITE;
		} else {
			return Constants.PIECE_BLACK;
		}
	}

	/*
	 * Return the node represented by nodeHash. If the table does not contain the node, 
	 * insert it and return the new node
	 */
	public TDSNode ttPutOrGet(int nodeHash, Board b){
		TDSTranspositionTable tt = AbstractSearchRank.getCurrentTT(b.getCurrentPlayer());
		PutResult result = tt.putIfAbsent(nodeHash, getNewNode(b, nodeHash));
		return result.getNode();
	}

	public void ttRelease(int nodeHash, int player){
		TDSTranspositionTable tt = AbstractSearchRank.getCurrentTT(player);
		tt.releaseNode(nodeHash);
	}

	public TDSNode ttGet(int nodeHash, int player){
		TDSTranspositionTable tt = AbstractSearchRank.getCurrentTT(player);
		return tt.getNode(nodeHash);
	}

	public void sendSearchJob(Board b, int parentHash, Move child, int turnNumber){
		int childHash = AbstractSearchRank.blackTT.getChildHash(parentHash, child, b.getCurrentPlayer());
		b.applyMove(child);
		LocalSearchMessage lsm = new LocalSearchMessage(b, childHash, turnNumber);
		getSender().tell(lsm, getSelf());
	}

	public void sendReportJob(Board b, int childHash, double reward, int turnNumber){
		Move child = b.undoMove();
		int parentHash = AbstractSearchRank.blackTT.getParentHash(childHash, child, b.getCurrentPlayer());
		LocalReportMessage lrm = new LocalReportMessage(b, child, parentHash, reward, turnNumber);
		getSender().tell(lrm, getSelf());
	}

	protected void selectAndSend(TDSNode node, Board b, int turnNumber){
		int hash = node.getNodeHash();
		int bestIndex = node.getBestMoveIndex();
		Move bestMove = node.getMoveAt(bestIndex);
		double childScore = node.getUCB(bestIndex);
		if ((childScore < Constants.FPU) && (!node.isFullyExpanded())){
			// Expand a new move if the best move is not as good as the FPU constant
			expandAndSend(node, b, turnNumber);
		} else {
			// Select the best expanded move
			node.incN(bestIndex); // Virtual loss
			sendSearchJob(b, hash, bestMove, turnNumber);
		}
	}

	protected void expandAndSend(TDSNode node, Board b, int turnNumber){
		if (b.isTerminal() != -1){
			// Node is terminal. Perform a playout and propagate
			double reward = defaultPolicy(b);
			sendReportJob(b, node.getNodeHash(), reward, turnNumber);
		} else {
			// Expand a child and send a new node message
			Move m = node.expandBestMove();
			sendSearchJob(b, node.getNodeHash(), m, turnNumber);	
		}
	}

	public double defaultPolicy(Board b){
		double reward = 0;
		int winner = b.doPlayout();
		if (winner == Constants.PLAYER_COLOUR){
			reward = 1;
		} else if (winner == Constants.PIECE_EMPTY){
			reward = 0.5;
		} else {
			reward = 0;
		}
		return reward;
	}

	public abstract TDSNode getNewNode(Board b, int hash);

	public abstract void processMessage(Object msg);

}
