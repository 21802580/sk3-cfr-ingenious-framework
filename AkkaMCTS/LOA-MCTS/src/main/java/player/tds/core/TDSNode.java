package player.tds.core;

import loa.logic.Board;
import player.core.MCTSNode;

public class TDSNode extends MCTSNode {
	protected final int nodeHash;
	private int lastAccess;
	
	public TDSNode(Board b, int nodeHash){
		super(null, b);
		this.nodeHash = nodeHash;
		this.lastAccess = 1;
	}
	
	public TDSNode(TDSNode node){
		super(node);
		this.nodeHash = node.nodeHash;
		this.lastAccess = node.lastAccess;

	}
	
	public int getNodeHash(){
		return this.nodeHash;
	}
	
	public int getLastAccess(){
		return lastAccess;
	}
	
	public void setLastAccess(int value){
		lastAccess = value;
	}
}
