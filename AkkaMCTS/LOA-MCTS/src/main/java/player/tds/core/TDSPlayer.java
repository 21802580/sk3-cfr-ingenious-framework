package player.tds.core;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.logic.Board;
import loa.logic.Move;
import player.core.LOAPlayer;
import player.core.MoveTranslation;
import player.tds.messages.control.SearchStartMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import statics.Constants;
import statics.MoveTableInitialiser;

public class TDSPlayer implements LOAPlayer {
	protected ActorSystem system;
	protected ActorRef master;
	private Board[] boards;
	private int turnNumber;
	
	public TDSPlayer(String seedHost, String seedPort){
		MoveTableInitialiser.initialise();
		Config config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + seedHost).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + seedPort)).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + Constants.MASTER_NAME + "]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://" + Constants.ACTOR_SYSTEM_NAME+ "@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create(Constants.ACTOR_SYSTEM_NAME, config);
		master = system.actorOf(Props.create(TDSMaster.class)
				.withDispatcher("akka.control-aware-dispatcher"), Constants.MASTER_NAME);
		boards = new Board[Constants.NUM_PARALLEL_SEARCHES];
		for (int i = 0; i < boards.length; i++){
			boards[i] = new Board();
		}
		turnNumber = 0;
	}

	public String genMove(long timeout) {
		Timeout t = new Timeout(Duration.create(60 * 20, "seconds"));
		Future<Object> fut = Patterns.ask(master, new SearchStartMessage(boards, turnNumber), t);
		Move m = null;
		try {
			m = (Move) Await.result(fut, t.duration());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		turnNumber++;
		for (int i = 0; i < boards.length; i++){
			boards[i].applyMove(m);
		}
		return MoveTranslation.getStringFromMove(m);
	}

	public void playMove(String move) {
		if (move.equals("pass")){
			turnNumber++;
			return;
		} else if (move.equals("resign")){
			master.tell("match_over", null);
		} else {
			turnNumber++;
			Move m = MoveTranslation.getMoveFromString(move, boards[0]);
			for (int i = 0; i < boards.length; i++){
				boards[i].applyMove(m);
			}
		}
	}

	public void gameOver(int terminationCode) {
		system.stop(master);
	}
}
