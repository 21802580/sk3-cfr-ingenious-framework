package player.tds.core;

import java.util.ArrayList;
import java.util.HashMap;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import loa.logic.Board;
import loa.logic.Move;
import player.tds.messages.control.BroadcastRankInitMessage;
import player.tds.messages.control.BroadcasterSupplyMessage;
import player.tds.messages.control.InitialSearchMessage;
import player.tds.messages.control.SearchCompleteMessage;
import player.tds.messages.control.SearchRankInitMessage;
import player.tds.messages.control.SearchStartMessage;
import player.tds.messages.duplication.NodeBroadcastMessage;
import player.tds.messages.search.RemoteSearchMessage;
import player.tds.messages.search.SearchDuplicateMessage;
import player.tds.schaefers.SchaefersNode;
import statics.Constants;

public class TDSMaster extends AbstractActor {
	private int numSearchRanksConnected;
	private int numBroadcastRanksConnected;
	private ActorRef returnActor;
	private ActorRef[] searchRanks;
	private ActorRef[] broadcastRanks;
	private TDSTranspositionTable zobrist;
	private Board[] boards;
	private int turnNumber;
	private boolean startReceived;
	Cluster cluster = Cluster.get(getContext().system());

	public TDSMaster() {
		this.numSearchRanksConnected = 0;
		this.numBroadcastRanksConnected = 0;
		this.searchRanks = new ActorRef[Constants.NUM_SEARCH_RANKS];
		this.broadcastRanks = new ActorRef[Constants.NUM_BROADCAST_RANKS];
		this.zobrist = new TDSTranspositionTable(Constants.TT_CAPACITY);
		this.startReceived = false;
	}

	@Override
	public void postStop() {
		for (ActorRef a : searchRanks){
			getContext().system().stop(a);
		}
		for (ActorRef a : broadcastRanks){
			getContext().system().stop(a);
		}
		cluster.leave(cluster.selfAddress());
	}

	private void distributeBroadcasters(){
		HashMap<Integer, ArrayList<ActorRef>> broadcasterWorkers = new HashMap<Integer, ArrayList<ActorRef>>();
		for (int i = 0; i < Constants.NUM_SEARCH_RANKS; i++){
			// Supply each actor with a broadcaster
			int index = i % Constants.NUM_BROADCAST_RANKS;
			BroadcasterSupplyMessage bsm = new BroadcasterSupplyMessage(broadcastRanks[index]);
			searchRanks[i].tell(bsm, getSelf());
			ArrayList<ActorRef> al = broadcasterWorkers.get(index);
			if (al == null){
				al = new ArrayList<ActorRef>();
				broadcasterWorkers.put(index, al);
			}
			al.add(searchRanks[i]);
		}
		for (int i = 0; i < Constants.NUM_BROADCAST_RANKS; i++){
			// Distribute broadcaster references
			ArrayList<ActorRef> searchRankList = broadcasterWorkers.get(i);
			ActorRef[] associatedSearchRanks = searchRankList.toArray(new ActorRef[searchRankList.size()]);
			broadcastRanks[i].tell(new BroadcastRankInitMessage(broadcastRanks, associatedSearchRanks), getSelf());
		}
	}

	private void startIfReady(){
		if (allConnected() && startReceived){
			initWorkers();
			sendJobs();
		}
	}

	private void initWorkers(){
		int rootHash = zobrist.getHash(boards[0].getState());
		for (int i = 0; i < searchRanks.length; i++){
			ActorRef a = searchRanks[i];
			a.tell(new SearchRankInitMessage(rootHash, i, searchRanks), getSelf());
		}
		if ((numBroadcastRanksConnected > 0) && (firstSearch())){
			// Distribute broadcasters if necessary
			distributeBroadcasters();
		}
	}
	
	private boolean firstSearch(){
		return turnNumber == 0 || turnNumber == 1;
	}

	private void sendJobs(){
		if (numBroadcastRanksConnected == 0){
			// Standard TDS / Yoshizoe
			sendAllJobsToHomeProcessor();
		}
		else {
			// Schaefers
			distributeJobs();
		}
	}

	private void sendAllJobsToHomeProcessor(){
		int rootHash = zobrist.getHash(boards[0].getState());
		int homeProcessor = zobrist.getHomeProcessor(rootHash);
		ActorRef homeActor = searchRanks[homeProcessor];
		InitialSearchMessage job = new InitialSearchMessage(rootHash, boards[0], turnNumber);
		homeActor.tell(job, getSelf());
		for (int i = 1; i < Constants.NUM_PARALLEL_SEARCHES; i++){
			RemoteSearchMessage sj = new RemoteSearchMessage(boards[i], rootHash, turnNumber);
			homeActor.tell(sj, getSelf());
		}
	}

	private void distributeJobs(){
		int rootHash = zobrist.getHash(boards[0].getState());
		int homeProcessor = zobrist.getHomeProcessor(rootHash);
		ActorRef homeActor = searchRanks[homeProcessor];
		InitialSearchMessage job = new InitialSearchMessage(0, null, 0); // Only needed to start the timer
		homeActor.tell(job, getSelf());
		for (int i = 0; i < Constants.NUM_SEARCH_RANKS; i++){
			// Broadcast a copy of the root node to every search rank
			SchaefersNode root = new SchaefersNode(boards[0], rootHash);
			searchRanks[i].tell(new NodeBroadcastMessage(root, Constants.PLAYER_COLOUR), getSelf());
		}
		for (int i = 0; i < Constants.NUM_PARALLEL_SEARCHES; i++){
			// Send search jobs to all processors
			SearchDuplicateMessage sj = new SearchDuplicateMessage(boards[i], rootHash, turnNumber);
			searchRanks[i % Constants.NUM_SEARCH_RANKS].tell(sj, getSelf());
		}
	}

	private boolean allConnected(){
		return (numSearchRanksConnected == Constants.NUM_SEARCH_RANKS) &&
				(numBroadcastRanksConnected == Constants.NUM_BROADCAST_RANKS);
	}

	private void initSearch(SearchStartMessage ssm){
		returnActor = getSender();
		boards = ssm.getBoards();
		turnNumber = ssm.getTurnNumber();
		startReceived = true;
		startIfReady();
	}

	private void sendMove(TDSNode node){
		System.out.println("Playouts: " + node.getVisits());
		for (int i = 0; i < node.getExpandedMoves().size(); i++){
			Move m = node.getMoveAt(i);
			System.out.println(m + " : " + node.getUCB(i));
		}
		returnActor.tell(node.getMoveAt(node.getBestMoveIndex()), getSelf());
		stopSearch();
	}
	
	private void sendMove(SchaefersNode node){
		System.out.println("Playouts: " + node.getVisits());
		for (int i = 0; i < node.getExpandedMoves().size(); i++){
			Move m = node.getMoveAt(i);
			System.out.println(m + " : " + node.getUCB(i));
		}
		returnActor.tell(node.getMoveAt(node.getBestMoveIndex()), getSelf());
		stopSearch();
	}
	
	private void stopSearch(){
		for (ActorRef a : searchRanks){
			a.tell(new SearchCompleteMessage(), getSelf());
		}
		for (ActorRef a : broadcastRanks){
			a.tell(new SearchCompleteMessage(), getSelf());
		}
		startReceived = false;
	}

	private void registerSearchRank(){
		ActorRef worker = getSender();
		getContext().watch(worker);
		searchRanks[numSearchRanksConnected] = getSender();
		numSearchRanksConnected++;
		startIfReady();
	}

	private void registerBroadcastRank() {
		ActorRef broadcaster = getSender();
		getContext().watch(broadcaster);
		broadcastRanks[numBroadcastRanksConnected] = broadcaster;
		numBroadcastRanksConnected++;
		startIfReady();
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SearchStartMessage.class, msg -> {
					initSearch(msg);
				})
				.match(SchaefersNode.class, msg -> {
					sendMove(msg);
				})
				.match(TDSNode.class, msg -> {
					sendMove(msg);
				})
				.matchEquals(Constants.SEARCH_RANK_NAME + "_registration", msg -> {
					registerSearchRank();
				})
				.matchEquals(Constants.BROADCAST_RANK_NAME + "_registration", msg -> {
					registerBroadcastRank();
				})
				.matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
