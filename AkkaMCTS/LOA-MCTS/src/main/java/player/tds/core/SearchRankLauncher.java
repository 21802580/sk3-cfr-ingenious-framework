package player.tds.core;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import statics.Constants;
import statics.MatchConfig;

public class SearchRankLauncher {
	public static void main(String[] args){
		String seedHost = args[0];
		String seedPort = args[1];
		int playerNumber = Integer.parseInt(args[2]);
		String cfgPath = args[3];
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		MatchConfig cfg = new MatchConfig(cfgPath);
		cfg.populatePlayerConfig(playerNumber);
		Config config = ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + 0)).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + Constants.SEARCH_RANK_NAME + "]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://" + Constants.ACTOR_SYSTEM_NAME+ "@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create(Constants.ACTOR_SYSTEM_NAME, config);
		system.actorOf(Props.create(Constants.SEARCH_RANK_CLASS)
				.withDispatcher("akka.control-aware-dispatcher"), Constants.SEARCH_RANK_NAME);
	}
}
