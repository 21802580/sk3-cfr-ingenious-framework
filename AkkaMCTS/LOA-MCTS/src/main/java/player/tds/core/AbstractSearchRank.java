package player.tds.core;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.concurrent.TimeUnit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import loa.logic.Board;
import loa.logic.Move;
import player.tds.messages.control.InitialSearchMessage;
import player.tds.messages.control.SearchCompleteMessage;
import player.tds.messages.control.SearchRankInitMessage;
import player.tds.messages.search.LocalReportMessage;
import player.tds.messages.search.LocalSearchMessage;
import player.tds.messages.search.RemoteReportMessage;
import player.tds.messages.search.RemoteSearchMessage;
import scala.concurrent.duration.Duration;
import statics.Constants;
import statics.MoveTableInitialiser;

public abstract class AbstractSearchRank extends AbstractActor {
	protected ActorRef[] ioRanks;
	protected int rootHash;
	protected ActorRef master;
	protected ActorRef router;
	Cluster cluster = Cluster.get(getContext().system());
	public static TDSTranspositionTable blackTT;
	public static TDSTranspositionTable whiteTT;

	public AbstractSearchRank() {
		master = null;
		blackTT = new TDSTranspositionTable(Constants.TT_CAPACITY);
		whiteTT = new TDSTranspositionTable(Constants.TT_CAPACITY);
		MoveTableInitialiser.initialise();
	}

	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	@Override
	public void postStop() {
		cluster.leave(cluster.selfAddress());
	}
	
	public void register(Member member) {
		getContext().actorSelection(member.address() + "/user/" + Constants.MASTER_NAME).tell(
				Constants.SEARCH_RANK_NAME + "_registration", getSelf());
	}

	public void makeRouter(int rankID) {
		router = getContext().actorOf(new SmallestMailboxPool(Constants.NUM_WORKERS_PER_RANK)
				.props(Props.create(Constants.WORKER_CLASS, rootHash, rankID))
				.withDispatcher("akka.local-router-dispatcher"), "router" + getSelf().hashCode());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(ActorRef[].class, msg -> {
					ioRanks = msg;
				})
				.match(SearchRankInitMessage.class, msg -> {
					initWorker(msg);
				})
				.match(InitialSearchMessage.class, msg -> {
					processInitialSearchJob(msg);
				})
				.match(RemoteSearchMessage.class, msg -> {
					router.tell(msg, getSelf());
				})
				.match(RemoteReportMessage.class, msg -> {
					router.tell(msg, getSelf());
				})
				.match(LocalSearchMessage.class, msg -> {
					sendSearchJob(msg);
				})
				.match(LocalReportMessage.class, msg -> {
					sendReportJob(msg);
				})
				.match(SearchCompleteMessage.class, msg -> {
					searchComplete(msg);
				})
				.match(MemberUp.class, msg -> {
					register(msg.member());
				})
				.matchAny(msg -> {
					processMessage(msg);
				})
				.build();	
		}

	protected void processInitialSearchJob(InitialSearchMessage isj){
		startTimer();
		Board b = isj.getBoard();
		TDSTranspositionTable tt = getCurrentTT(Constants.PLAYER_COLOUR);
		tt.putIfAbsent(rootHash, getNewNode(b, rootHash));
		tt.releaseNode(rootHash);
		RemoteSearchMessage snm = new RemoteSearchMessage(b, rootHash, isj.getTurnNumber());
		router.tell(snm, getSelf());
	}

	public void sendSearchJob(LocalSearchMessage lsm){
		int nodeHash = lsm.getNodeHash();
		Board b = lsm.getBoard();
		int turnNumber = lsm.getTurnNumber();
		RemoteSearchMessage sj = new RemoteSearchMessage(b, nodeHash, turnNumber);
		int homeProcessor = blackTT.getHomeProcessor(nodeHash);
		ioRanks[homeProcessor].tell(sj,  getSelf());
	}

	public void sendReportJob(LocalReportMessage lrm){
		Move child = lrm.getChild();
		int parentHash = lrm.getParentHash();
		Board b = lrm.getBoard();
		int turnNumber = lrm.getTurnNumber();
		double reward = lrm.getReward();
		int homeProcessor = blackTT.getHomeProcessor(parentHash);
		RemoteReportMessage rj = new RemoteReportMessage(b, child, parentHash, reward, turnNumber);
		ioRanks[homeProcessor].tell(rj, getSelf());
	}

	protected void startTimer(){
		getContext().system().scheduler().scheduleOnce(Duration.create(Constants.TIMEOUT - 100, TimeUnit.MILLISECONDS),
			new Runnable() {
					@Override
					public void run() {
						sendFinalNode();
					}
			}, getContext().dispatcher());
	}

	public void searchComplete(SearchCompleteMessage scm){
		router.tell(new Broadcast(scm), getSelf());
	}

	public void initWorker(SearchRankInitMessage wim){
		ioRanks = wim.getSearchRanks();
		blackTT.newSearch();
		whiteTT.newSearch();
		int previousRootHash = rootHash;
		rootHash = wim.getRootHash();
		if (previousRootHash == 0){
			master = getSender();
			int rankID = wim.getRankID();
			makeRouter(rankID);
		}	
	}
	
	public static TDSTranspositionTable getCurrentTT(int currentPlayer){
		TDSTranspositionTable tt = blackTT;
		if (currentPlayer == Constants.PIECE_WHITE){
			tt = whiteTT;
		}
		return tt;
	}
	
	public abstract void sendFinalNode();
	
	public abstract TDSNode getNewNode(Board b, int hash);
	
	public abstract void processMessage(Object msg);
	
		
		private void produceDotFile(){
			Writer writer = null;
			try {
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tree.dot"), "utf-8"));
				writer.write("digraph G {\n"
						+ "    nodesep=0.3;\n"
						+ "    ranksep=0.2;\n"
						+ "    margin=0.1;\n"
						+ "    node [shape=circle];\n"
						+ "    edge [arrowsize=0.8];\n");
				int nw = 0;
				int nb = 0;
				for (int i = 0; i < Constants.TT_CAPACITY; i++){
					TDSNode bn = blackTT.getNodeAtIndex(i);
					TDSNode wn = whiteTT.getNodeAtIndex(i);
					if (bn != null){
						nb++;
						int parentHash = bn.getNodeHash();
						for (Move m : bn.getExpandedMoves()){
							int childHash = blackTT.getChildHash(parentHash, m, Constants.PIECE_BLACK);
							writer.write("0" + parentHash + " -> " + "1" + childHash
									+ " [label=\"\"];\n");		
						}	
					}
					if (wn != null){
						nw++;
						int parentHash = wn.getNodeHash();
						for (Move m : wn.getExpandedMoves()){
							int childHash = blackTT.getChildHash(parentHash, m, Constants.PIECE_WHITE);
							writer.write("1" + parentHash + " -> " + "0" + childHash
									+ " [label=\"\"];\n");		
						}	
					}
				}
				writer.write("}");
				System.out.println(nb + " " + nw);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch bloc
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {writer.close();} catch (Exception ex) {/*ignore*/}
			}        
		}

}
