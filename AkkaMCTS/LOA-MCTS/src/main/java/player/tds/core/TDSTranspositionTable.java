package player.tds.core;

import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import loa.logic.Move;
import statics.Constants;

public class TDSTranspositionTable {
	private int[][][] randoms;
	private TDSNode[] entries;
	private AtomicInteger lookupCnt;
	private int startTime;
	private ConcurrentHashMap<Integer, Lock> locks;
	private int capacity;

	public TDSTranspositionTable(int capacity) {
		lookupCnt = new AtomicInteger(0);
		this.capacity = capacity;
		startTime = 0;
		randoms = new int[8][8][3];
		entries = new TDSNode[capacity];
		Random r = new Random(12345);
		locks = new ConcurrentHashMap<Integer, Lock>();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 3; k++){
					int hash = Math.abs(r.nextInt());
					randoms[i][j][k] = hash;
				}
			}
		}
	}

	public void newSearch(){
		this.startTime = lookupCnt.incrementAndGet();
	}

	public int getHash(int[][] board){
		int h = 0;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (board[i][j] != Constants.PIECE_EMPTY){
					h = h ^ randoms[i][j][board[i][j]];
				}
			}
		}
		return h;
	}

	public int getChildHash(int hash, Move m, int player){
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		int opponent = Constants.PIECE_BLACK;
		if (player == Constants.PIECE_BLACK){
			opponent = Constants.PIECE_WHITE;
		}	
		hash ^= randoms[rf][cf][player];
		hash ^= randoms[rt][ct][player];		
		if (m.isCapture()){
			hash ^= randoms[rt][ct][opponent];
		}
		return hash;
	}

	public int getParentHash(int hash, Move m, int player){
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		int opponent = Constants.PIECE_BLACK;
		if (player == Constants.PIECE_BLACK){
			opponent = Constants.PIECE_WHITE;
		}
		hash ^= randoms[rt][ct][player];
		hash ^= randoms[rf][cf][player];
		if (m.isCapture()){
			hash ^= randoms[rt][ct][opponent];
		}
		return hash;
	}

	public PutResult putIfAbsent(int hash, TDSNode node){
		Lock l = locks.putIfAbsent(hash, new ReentrantLock());
		if (l == null){
			locks.get(hash).lock();	
		} else {
			l.lock();
		}
		int index = hash % capacity;
		index = Math.max(index, 1); // Ignore index 0
		int replacedHash = -1;
		while (entries[index] != null){
			if (hash == entries[index].getNodeHash()){
				// Node already in table
				entries[index].setLastAccess(lookupCnt.incrementAndGet());
				return new PutResult(entries[index], replacedHash);
			} else if (entries[index].getLastAccess() < startTime){
				// Replace old element
				replacedHash = entries[index].getNodeHash();
				locks.remove(replacedHash);
				break;
			} else if (index++ == capacity){
				index = 1; // Cycle back to index 1 when the end of the table is reached
			}
		}
		entries[index] = node;
		node.setLastAccess(lookupCnt.incrementAndGet());
		return new PutResult(node, replacedHash);
	}

	public TDSNode getNode(int hash){
		Lock l = locks.get(hash);
		if (l == null){
			System.out.println("COULD NOT RETRIEVE NODE " + hash);
			System.out.println("HOME PROCESSOR FOR NODE: " + getHomeProcessor(hash));
			System.out.println("TABLE CAPACITY: " + capacity);
		}
		l.lock();
		int index = hash % capacity;
		index = Math.max(index, 1); // Ignore index 0
		while (entries[index].getNodeHash() != hash){
			if (index++ == capacity){
				index = 1; // Cycle back to index 1 when the end of the table is reached
			}
		}
		entries[index].setLastAccess(lookupCnt.incrementAndGet());
		return entries[index];
	}
	
	public boolean contains(int hash){
		return locks.containsKey(hash);
	}
	
	public TDSNode getNodeAtIndex(int index){
		return entries[index];
	}
	
	public void releaseNode(int hash){
		locks.get(hash).unlock();
	}
	
	public void removeNode(int hash){
		Lock l = locks.get(hash);
		l.lock();
		int index = hash % capacity;
		index = Math.max(index, 1); // Ignore index 0
		while (entries[index].getNodeHash() != hash){
			if (index++ == capacity){
				index = 1; // Cycle back to index 1 when the end of the table is reached
			}
		}
		entries[index] = null;
		l.unlock();
		locks.remove(l);
	}
	

	public int getHomeProcessor(int hash){
		return (hash / capacity) % Constants.NUM_SEARCH_RANKS;
	}

	public int getLookupCnt(){
		return this.lookupCnt.get();
	}
	
	public class PutResult {
		private TDSNode newNode;
		private int replacedHash;
		
		public PutResult(TDSNode newNode, int replacedHash){
			this.newNode = newNode;
			this.replacedHash = replacedHash;
		}
		
		public TDSNode getNode(){
			return this.newNode;
		}
		
		public int getReplacedHash(){
			return this.replacedHash;
		}
	}
}
