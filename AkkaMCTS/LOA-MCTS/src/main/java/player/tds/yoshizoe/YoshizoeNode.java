package player.tds.yoshizoe;

import loa.logic.Board;
import player.tds.core.TDSNode;

public class YoshizoeNode extends TDSNode{

	public YoshizoeNode(Board b, int nodeHash) {
		super(b, nodeHash);
	}

	public YoshizoeNode(YoshizoeNode node) {
		super(node);
	}
}
