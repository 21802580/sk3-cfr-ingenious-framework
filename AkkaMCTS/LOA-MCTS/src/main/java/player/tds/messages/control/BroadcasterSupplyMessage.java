package player.tds.messages.control;

import akka.actor.ActorRef;
import akka.dispatch.ControlMessage;

public class BroadcasterSupplyMessage implements ControlMessage{
	private final ActorRef broadcaster;
	
	public BroadcasterSupplyMessage(ActorRef broadcaster){
		this.broadcaster = broadcaster;
	}
	
	public ActorRef getBroadcaster(){
		return this.broadcaster;
	}
}
