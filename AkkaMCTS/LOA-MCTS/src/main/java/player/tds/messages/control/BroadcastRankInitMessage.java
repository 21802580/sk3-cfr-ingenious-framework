package player.tds.messages.control;

import akka.actor.ActorRef;

public class BroadcastRankInitMessage {
	private final ActorRef[] broadcasters;
	private final ActorRef[] searchRanks;
	
	public BroadcastRankInitMessage(ActorRef[] broadcasters, ActorRef[] searchRanks){
		this.broadcasters = broadcasters;
		this.searchRanks = searchRanks;
	}

	public ActorRef[] getBroadcasters() {
		return broadcasters;
	}

	public ActorRef[] getSearchRanks() {
		return searchRanks;
	}
}
