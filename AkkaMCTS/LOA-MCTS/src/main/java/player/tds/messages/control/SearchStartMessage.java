package player.tds.messages.control;

import loa.logic.Board;

public class SearchStartMessage {
	private final Board[] boards;
	private final int turnNumber;
	
	public SearchStartMessage(Board[] boards, int turnNumber){
		this.boards = boards;
		this.turnNumber = turnNumber;
	}

	public Board[] getBoards() {
		return boards;
	}
	
	public int getTurnNumber(){
		return this.turnNumber;
	}
}
