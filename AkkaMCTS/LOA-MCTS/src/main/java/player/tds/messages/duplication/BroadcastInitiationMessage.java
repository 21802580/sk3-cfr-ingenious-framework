package player.tds.messages.duplication;

import player.tds.schaefers.SchaefersNode;
import statics.Constants;

public class BroadcastInitiationMessage {
	private final SchaefersNode node;
	private final boolean player;

	public BroadcastInitiationMessage(SchaefersNode node, int player){
		this.node = node;
		this.player = player == Constants.PIECE_BLACK ? true : false;
	}

	public SchaefersNode getNode(){
		return this.node;
	}

	public int getPlayer(){
		return player ? Constants.PIECE_BLACK : Constants.PIECE_WHITE;
	}
}
