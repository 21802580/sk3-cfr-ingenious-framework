package player.tds.messages.duplication;

import akka.dispatch.ControlMessage;
import statics.Constants;

public class RemovalInitiationMessage implements ControlMessage {
	private final int hash;
	private final boolean player;
	
	public RemovalInitiationMessage(int hash, int player){
		this.hash = hash;
		this.player = player == Constants.PIECE_BLACK ? true : false;
	}
	
	public int getHash(){
		return this.hash;
	}
	
	public int getPlayer(){
		return this.player ? Constants.PIECE_BLACK : Constants.PIECE_WHITE;
	}
}
