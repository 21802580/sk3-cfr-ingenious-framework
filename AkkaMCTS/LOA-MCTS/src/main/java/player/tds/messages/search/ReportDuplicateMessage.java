package player.tds.messages.search;

import loa.logic.Board;
import loa.logic.Move;

public class ReportDuplicateMessage {
	private final int parentHash;
	private Board board;
	private final double reward;
	private final Move child;
	private final int turnNumber;
	
	public ReportDuplicateMessage(Board board, Move child, int parentHash, double reward, int turnNumber){
		this.board = board;
		this.child = child;
		this.parentHash = parentHash;
		this.reward = reward;
		this.turnNumber = turnNumber;
	}

	public int getParentHash() {
		return parentHash;
	}

	public double getReward() {
		return reward;
	}
	
	public Board getBoard(){
		return this.board;
	}
	
	public Move getChild(){
		return this.child;
	}
	
	public int getTurnNumber(){
		return this.turnNumber;
	}
	
	public String toString(){
		String str = "REPORT MESSAGE:\n";
		str += "PARENT HASH: " + parentHash + "\n";
		str += "REWARD: " + reward + "\n";
		
		return str;
	}

}
