package player.tds.messages.control;

import akka.dispatch.ControlMessage;
import loa.logic.Board;

public class InitialSearchMessage implements ControlMessage {
	private final int nodeHash;
	private final Board board;
	private final int turnNumber;
	
	public InitialSearchMessage(int nodeHash, Board board, int turnNumber){
		this.nodeHash = nodeHash;
		this.board = board;
		this.turnNumber = turnNumber;
	}
	
	public int getNodeHash(){
		return this.nodeHash;
	}
	
	public Board getBoard(){
		return this.board;
	}
	
	public int getTurnNumber(){
		return this.turnNumber;
	}
}
