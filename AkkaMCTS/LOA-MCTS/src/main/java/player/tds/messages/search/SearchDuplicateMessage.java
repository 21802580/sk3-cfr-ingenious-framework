package player.tds.messages.search;

import loa.logic.Board;

public class SearchDuplicateMessage {
	private final int nodeHash;
	private Board board;
	private final int turnNumber;
	
	public SearchDuplicateMessage(Board board, int nodeHash, int turnNumber){
		this.nodeHash = nodeHash;
		this.board = board;
		this.turnNumber = turnNumber;
	}
	
	public int getNodeHash(){
		return this.nodeHash;
	}
	
	public Board getBoard(){
		return this.board;
	}
	
	public String toString(){
		String str = "SEARCH MESSAGE:\n";
		str += "NODE HASH: " + nodeHash + "\n";
		
		return str;
	}
	
	public int getTurnNumber(){
		return this.turnNumber;
	}
}
