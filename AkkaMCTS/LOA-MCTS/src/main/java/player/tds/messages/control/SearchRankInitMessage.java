package player.tds.messages.control;

import akka.actor.ActorRef;
import akka.dispatch.ControlMessage;

public class SearchRankInitMessage implements ControlMessage{
	private final ActorRef[] searchRanks;
	private final int rootHash;
	private final int rankID;
	
	public SearchRankInitMessage(int rootHash, int rankID, ActorRef[] searchRanks){
		this.rootHash = rootHash;
		this.rankID = rankID;
		this.searchRanks = searchRanks;
	}
	
	public int getRootHash(){
		return this.rootHash;
	}
	
	public int getRankID(){
		return this.rankID;
	}
	
	public ActorRef[] getSearchRanks(){
		return this.searchRanks;
	}
}
