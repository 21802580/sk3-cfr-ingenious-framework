package player.tds.vanilla;

import loa.logic.Board;
import player.tds.core.AbstractWorker;
import player.tds.core.TDSNode;

public class TDSWorker extends AbstractWorker{

	public TDSWorker(int rootHash, int rankID) {
		super(rootHash, rankID);
	}

	@Override
	public void processMessage(Object msg) {
		return;
	}

	@Override
	public TDSNode getNewNode(Board b, int hash) {
		return new TDSNode(b, hash);
	}

}
