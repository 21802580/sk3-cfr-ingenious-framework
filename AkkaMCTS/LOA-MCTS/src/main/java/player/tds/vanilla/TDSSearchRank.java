package player.tds.vanilla;

import loa.logic.Board;
import player.tds.core.AbstractSearchRank;
import player.tds.core.TDSNode;
import player.tds.core.TDSTranspositionTable;
import statics.Constants;

public class TDSSearchRank extends AbstractSearchRank {
	
	public TDSSearchRank() {
		super();
	}

	@Override
	public void processMessage(Object msg) {
		return;
	}

	@Override
	public TDSNode getNewNode(Board b, int hash) {
		return new TDSNode(b, hash);
	}

	@Override
	public void sendFinalNode() {
		TDSTranspositionTable tt = getCurrentTT(Constants.PLAYER_COLOUR);
		TDSNode n = tt.getNode(rootHash);
		master.tell(n, getSelf());
		tt.releaseNode(rootHash);
	}
}
