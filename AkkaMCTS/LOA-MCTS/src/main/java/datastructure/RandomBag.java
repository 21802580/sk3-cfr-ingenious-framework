package datastructure;

import java.util.AbstractSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import loa.logic.Coord;
import loa.logic.Move;

public class RandomBag<E> extends AbstractSet<E> {

    RandomSet<E> data;
    Map<E, Integer> counts;
    int numElements;
    
    public static void main(String[] args){
    	Move m1 = new Move(new Coord(0, 1), new Coord(0, 2), true);
    	Move m2 = new Move(new Coord(0, 1), new Coord(0, 2), false);
    	RandomBag<Move> rb = new RandomBag<Move>();
    	rb.add(m1);
    	rb.add(m1);
    	rb.add(m2);
    	for (Move m : rb){
    		System.out.println(m);
    	}
//    	System.out.println(rb.pollRandom(new Random()));
//    	System.out.println(rb.getCount(m1));
//    	System.out.println(rb.pollRandom(new Random()));
    	//System.out.println(rb.pollRandom(new Random()));
    }
    
    public RandomBag(RandomBag<E> rb) {
    	data = new RandomSet<E>(rb.data);
    	counts = new HashMap<E, Integer>(rb.counts);
    	numElements = rb.numElements;
    }
    
    public RandomBag() {
    	data = new RandomSet<E>();
    	counts = new HashMap<E, Integer>();
    	numElements = 0;
    }

    @Override
    public boolean add(E item) {
        if (counts.containsKey(item)) {
            counts.put(item, counts.get(item) + 1);
        } else {
            data.add(item);
            counts.put(item, 1);
        }
        numElements++;
        return true;
    }

    @Override
    public boolean remove(Object item) {
        if (!data.contains(item)) {
            return false;
        }
        E key = (E)item;
        Integer cnt = counts.get(item);
        if (cnt == 1){
        	counts.remove(key);
        	data.remove(key);
        } else {
        	counts.put(key, cnt - 1);
        }
        numElements--;
        return true;
    }

    public E get(int i) {
        return data.get(i);
    }

    public E pollRandom(Random rnd) {
        if (data.isEmpty()) {
            return null;
        }
        E key = data.peekRandom(rnd);
        remove(key);
        return key;
    }
    
    public int getCount(E key){
        if (!data.contains(key)){
        	return 0;
        }
        return counts.get(key);
    }

    @Override
    public int size() {
    	return numElements;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
        	int setPos = 0;
        	int mapPos = 0;
        	int numChecked = 0;

			@Override
			public boolean hasNext() {
				return numChecked < numElements;
			}

			@Override
			public E next() {
				E item = get(setPos);
				if (mapPos++ == counts.get(item)){
					mapPos = 0;
					setPos++;
				}
				numChecked++;
				return item;
			}
        	
		};
    }
}
