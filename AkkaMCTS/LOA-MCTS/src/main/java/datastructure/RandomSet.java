package datastructure;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import loa.logic.Coord;

public class RandomSet<E> extends AbstractSet<E> {

    List<E> data;
    Map<E, Integer> idx;
    
    public static void main(String[] args){
    	RandomSet<Coord> rs = new RandomSet<Coord>();
    	rs.add(new Coord(1, 1));
    	RandomSet<Coord> rs2 = new RandomSet<Coord>(rs);
    	System.out.println(rs.get(0) == rs2.get(0));
    }

    public RandomSet() {
    	data = new ArrayList<E>();
    	idx = new HashMap<E, Integer>();
    }

    public RandomSet(Collection<E> items) {
    	data = new ArrayList<E>();
    	idx = new HashMap<E, Integer>();
        for (E item : items) {
            idx.put(item, data.size());
            data.add(item);
        }
    }
    
    public RandomSet(RandomSet<E> rs){
    	data = new ArrayList<E>(rs.data);
    	idx = new HashMap<E, Integer>(rs.idx);
    }

    @Override
    public boolean add(E item) {
        if (idx.containsKey(item)) {
            return false;
        }
        idx.put(item, data.size());
        data.add(item);
        return true;
    }

    /**
     * Override element at position <code>id</code> with last element.
     * @param id
     */
    public E removeAt(int id) {
        if (id >= data.size()) {
            return null;
        }
        E res = data.get(id);
        idx.remove(res);
        E last = data.remove(data.size() - 1);
        // skip filling the hole if last is removed
        if (id < data.size()) {
            idx.put(last, id);
            data.set(id, last);
        }
        return res;
    }

    @Override
    public boolean remove(Object item) {
        @SuppressWarnings(value = "element-type-mismatch")
        Integer id = idx.get(item);
        if (id == null) {
            return false;
        }
        removeAt(id);
        return true;
    }
    
    @Override
    public boolean contains(Object item){
    	return idx.containsKey((E)item);
    }

    public E get(int i) {
        return data.get(i);
    }

    public E pollRandom(Random rnd) {
        if (data.isEmpty()) {
            return null;
        }
        int id = rnd.nextInt(data.size());
        return removeAt(id);
    }
    
    public E peekRandom(Random rnd){
        if (data.isEmpty()) {
            return null;
        }
        int id = rnd.nextInt(data.size());
        return get(id);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public Iterator<E> iterator() {
        return data.iterator();
    }
}