package speedtest;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import loa.logic.Board;
import player.tds.core.TDSNode;
import statics.MoveTableInitialiser;

public class SharedMemoryTest {
	private int val;
	
	public static void main(String[] args){
		SharedMemoryTest s = new SharedMemoryTest();
	}
	
	public SharedMemoryTest() {
		val = 8;
		final Config cfg = ConfigFactory.parseString("akka.local-router-dispatcher.mailbox-type = \"akka.dispatch.UnboundedControlAwareMailbox\"").
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.type = Dispatcher")).
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.executor = \"fork-join-executor\"")).
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.fork-join-executor.parallelism-min = 2")).
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.fork-join-executor.parallelism-factor = 2.0")).
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.fork-join-executor.parallelism-max = 128")).
				withFallback(ConfigFactory.parseString("akka.local-router-dispatcher.throughput = 100")).
				withFallback(ConfigFactory.defaultOverrides());
		final ActorSystem system = ActorSystem.create("test", cfg);
		final ActorRef master = system.actorOf(Props.create(Master.class, () -> new Master()), "master");
		MoveTableInitialiser.initialise();
		master.tell("go", ActorRef.noSender());
	}

	public class Master extends AbstractActor {

		ActorRef router;

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.matchAny(message -> {
						Props props = new SmallestMailboxPool(5)
								.props(Props.create(Worker.class, () -> new Worker()))
								.withDispatcher("akka.local-router-dispatcher");
						router = getContext().actorOf(props, "worker_router");
						System.out.println(router);
						router.tell(new Broadcast(new TDSNode(new Board(), 1234)), getSelf());
					}).build();
		}
	}
	
	public class Worker extends AbstractActor {

		@Override
		public Receive createReceive() {
			return receiveBuilder()
					.matchAny(message -> {
						//System.out.println(((NewRootMessage)message).getRootHash());
						//System.out.println(((TDSNode)message).getBoard());
						System.out.println(getSelf());
						System.out.println(getContext().dispatcher());
					}).build();
		}
	}
}
