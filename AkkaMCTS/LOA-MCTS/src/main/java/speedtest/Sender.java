package speedtest;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import loa.logic.Board;
import loa.logic.LineMove;
import statics.Constants;
import statics.Data;

public class Sender extends AbstractActor{
	int numMessages;
	ActorRef receiver;

	public static void main(String[] args){
		int numMessages = Integer.parseInt(args[0]);
		Config config = ConfigFactory.parseString("akka.remote.log-remote-lifecycle-events = off").
				withFallback(ConfigFactory.parseString("akka.remote.enabled-transports = [\"akka.remote.netty.tcp\"]")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=localhost")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=61234")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.maximum-frame-size = 2097152")).
				//				withFallback(ConfigFactory.parseString("akka.remote.artery.enabled=on")).
				//				withFallback(ConfigFactory.parseString("akka.remote.artery.canonical.hostname=localhost")).
				//				withFallback(ConfigFactory.parseString("akka.remote.artery.canonical.port=61234")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [sender]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://SpeedTestSystem@localhost:61234\"]")).
				withFallback(ConfigFactory.load());
		Data.powers = new int[8];
		for (int i = 0; i < 8; i++){
			Data.powers[i] = (int) Math.pow(3, i);
		}
		populateTable();
		ActorSystem system = ActorSystem.create("SpeedTestSystem", config);
		system.actorOf(Props.create(Sender.class, () -> new Sender(numMessages))
				.withDispatcher("akka.actor.control-aware-dispatcher"), "sender");
	}

	private static void populateTable(){
		Data.blackMoves = new ArrayList<HashMap<Integer, ArrayList<LineMove>>>(9);
		Data.whiteMoves = new ArrayList<HashMap<Integer, ArrayList<LineMove>>>(9);
		for (int i = 0; i <= 8; i++){
			Data.blackMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
			Data.whiteMoves.add(new HashMap<Integer, ArrayList<LineMove>>());
		}

		Scanner s = null;
		try {
			s = new Scanner(new File("moves.txt"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (s.hasNext()){
			int player = s.nextInt();
			int length = s.nextInt();
			int index = s.nextInt();
			int src = s.nextInt();
			int dest = s.nextInt();
			boolean capture = s.nextBoolean();
			if (player == Constants.PIECE_BLACK){
				HashMap<Integer, ArrayList<LineMove>> hm = Data.blackMoves.get(length);
				ArrayList<LineMove> moves = hm.get(index);
				if (moves == null){
					moves = new ArrayList<LineMove>();
					hm.put(index, moves);
				}
				moves.add(new LineMove(src, dest, capture));
			} else {
				HashMap<Integer, ArrayList<LineMove>> hm = Data.whiteMoves.get(length);
				ArrayList<LineMove> moves = hm.get(index);
				if (moves == null){
					moves = new ArrayList<LineMove>();
					hm.put(index, moves);
				}
				moves.add(new LineMove(src, dest, capture));
			}
		}
		s.close();
	}

	public Sender(int numMessages){
		this.numMessages = numMessages;
	}

	private void sendMessages(){
		Board b = new Board();
		//SearchJob sj = new SearchJob(0, b, 0);
		SmallSearchJobTest sj = new SmallSearchJobTest(0, 0);
		for (int i = 0; i < numMessages; i++){
			receiver.tell(sj, getSelf());
		}
		receiver.tell("done", getSelf());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.matchEquals("receiver_registration", msg -> {
					receiver = getSender();
					sendMessages();
				}).matchAny(msg -> {
					unhandled(msg);
				})
				.build();
	}
}
