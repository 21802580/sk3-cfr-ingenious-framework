package speedtest;

public class SmallSearchJobTest {
	private int hash;
	private int turnNumber;
	
	public SmallSearchJobTest(int hash, int turnNumber){
		this.hash = hash;
		this.turnNumber = turnNumber;
	}
	
	public int getHash(){
		return this.hash;
	}
	
	public int getTurnNumber(){
		return this.turnNumber;
	}
}
