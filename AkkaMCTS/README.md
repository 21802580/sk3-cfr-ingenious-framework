# README #

This repository contains the code written for my Masters degree, the goal of which is to use Akka to compare modern distributed Monte Carlo Tree Search (MCTS) implementations on the same domain (Lines of Action).

The repository contains the following three projects:

## examples ##

This project contains some basic "Hello World" type examples for Akka and, more specifically, Akka clustering. It also contains some packages which I used to compare the performance of OpenMPI and Akka.

## LOAProtocol ##

In order to evaluate our Lines of Action player against other strong players without the developers providing us with their code base, we provide this adapter for their code that our system can use for playing automatically against their engines.  To do this, we provide a Java interface (**LOAPlayer**) with certain basic methods used by a provided driver class (**LOADriver**) that can interface with our system from a Java binary archive. The **LOATester** class can be used to run a match between any two engines and make sure that everything works as expected. **RandomPlayer** is an example implementation of the **LOAPlayer** interface.

A more detailed description of this project can be found in [this](https://bitbucket.org/FacelessToast/msc-documents) repository.

## LOA-MCTS ##

This project houses the actual MCTS implementations used during experimentation. Currently, there are four implementations which can be found in the **loa.players** package:

**basic** - A vanilla MCTS implementation with no enhancements or parallelisation.

**rootPar** - An MCTS implementation which uses the root parallelisation technique.

**yoshizoe** - An implementation of depth-first UCT with Transposition Table driven work Scheduling (TDS). [K. Yoshizoe, A. Kishimoto, T. Kaneko, H. Yoshimoto, Y. Ishikawa, "Scalable distributed Monte Carlo tree search", in: *Proceedings of the 4th Symposium on Combinatorial Search* SOCS-11, 2011, pp. 180–187.]

**schaefers** - An implementation of the UCT-Treesplit algorithm, also with TDS. [Schaefers, L. and Platzner, M. (2015). "Distributed Monte Carlo Tree Search: A Novel Technique and its Application to Computer Go". *IEEE Trans. Comput. Intell. AI Games*, 7(4), pp.361-374.]

In order to build the project and produce the runnable JARs necessary to run matches between the players, navigate to the **LOA-MCTS** directory and type ```mvn clean install``` (assuming you have Maven installed). Once the build finishes, the JARs will be located in the "LOA-MCTS/target" directory, and you will be able to run **test.bash** to run a match between the schaefers and basic players. Note that you need **mpirun** installed to run this script.

Since these implementations are still mostly work in progress, there may be some unwanted output from the test script. For now, time limits per move, UCB constants and any other parameters required by these players are hard-coded into the script. This will be changed soon.