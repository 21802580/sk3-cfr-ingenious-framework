package loa.players.basic;

import java.io.Serializable;
import java.util.ArrayList;

import loa.utils.*;

public class BasicMCTS implements Serializable{
	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;
	
	private Board currentState;
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int maxDepth;
	private Node n;
	
	public BasicMCTS(int timeout, int colour, double ucbConstant){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.maxDepth = 0;
		this.n = null;
	}
	
	public Node getRoot(){
		return this.n;
	}
	
	public Move getMove(Board state){
		System.out.println("COLOUR: " + colour);
		int[][] s = new int[8][8];
		int[][] t = state.getState();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				s[i][j] = t[i][j];
			}
		}
		currentState = new Board(s, colour);
		n = new Node(null, null);
		long time = System.currentTimeMillis();
		int playouts = 0;
		while (System.currentTimeMillis() - time < timeout){
			Node v = treePolicy(n);
			double reward = defaultPolicy();
			backup(v, reward);
			playouts++;
		}
		System.out.println("Playouts : " + playouts);
		System.out.println("Best depth : " + maxDepth);
		Move m = bestChild(n).getMoveMade();
		return m;
	}
	
	public double defaultPolicy(){
		Board s = new Board(currentState.getState(), currentState.getCurrentPlayer());
		double reward = 0;
		boolean noLegalMove = false;
		while(!s.isTerminal()){
			Move m = s.makeRandomMove(new ArrayList<Move>());
			if ((m == null) || (m.equals(new Move(new Coord(-1, -1), new Coord(-1, -1))))){
				noLegalMove = true;
				break;
			}
		}
		if (noLegalMove){
			if (s.getCurrentPlayer() == colour){
				reward = 0;
			} else {
				reward = 1;
			}
		}
		if (colour == WHITE){
			if (s.whiteWins()){
				reward = 1;
			} else if (s.blackWins()){
				reward = 0;
			} else {
				reward = 0.5;
			}
		} else {
			if (s.whiteWins()){
				reward = 0;
			} else if (s.blackWins()){
				reward = 1;
			} else {
				reward = 0.5;
			}
		}
		return reward;
	}
	
	public Node treePolicy(Node n){
		while(!currentState.isTerminal()){
			Move m = n.getNextMove(currentState);
			//No legal move -> terminal
			if (m == null){
				break;
			}
			//Fully expanded
			if (m.equals(new Move(new Coord(-1, -1), new Coord(-1, -1)))){
				n = bestChild(n);
			} else {
				return expand(n, m);
			}
		}
		return n;
	}
	
	public Node expand(Node n, Move m){
		currentState.makeMove(m.getFrom(), m.getTo());
		Node v = new Node(n, m);
		n.addChild(v);
		return v;
	}
	
	public Node bestChild(Node n){
		double max = 0;
		Node best = null;
		ArrayList<Node> children = n.getChildren();
		for (Node x : children){
			if (x.getUCB(ucbConstant) > max){
				best = x;
				max = x.getUCB(ucbConstant);
			}
		}
		currentState.makeMove(best.getMoveMade().getFrom(), best.getMoveMade().getTo());
		return best;
	}
	
	public void backup(Node v, double reward){
		int i = 0;
		while(v != null){
			v.setVisits(v.getVisits() + 1);
			v.setReward(v.getReward() + reward);
			v = v.getParent();
			currentState.undoMove();
			i++;
		}
		if (i > maxDepth){
			maxDepth = i;
		}
	}
}
