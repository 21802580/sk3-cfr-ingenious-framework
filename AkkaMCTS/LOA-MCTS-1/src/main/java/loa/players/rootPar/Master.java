package loa.players.rootPar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import cluster.TransformationMessages;
import loa.players.basic.BasicMCTS;
import loa.utils.Board;
import loa.utils.Move;
import loa.utils.Node;

public class Master extends UntypedActor{
	private int timeout;
	private int colour;
	private double ucbConstant;
	private Board state;
	private int numWorkers;
	private int numReceived;
	//private final ActorRef workerRouter;
	private ActorRef returnActor;
	private HashMap<Move, Double> scores;
	List<ActorRef> workers = new ArrayList<ActorRef>();
	
	public Master(int timeout, int colour, double ucbConstant, int numWorkers, Board state){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.state = state;
		this.scores = new HashMap<Move, Double>();
		this.numReceived = 0;
		this.numWorkers = numWorkers;
		//this.workerRouter = this.getContext().actorOf(new RoundRobinPool(numWorkers).props(Props.create(Worker.class)), "router");
	}
	
	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof Node){
			numReceived++;
			Node root = (Node)message;
			ArrayList<Node> children = root.getChildren();
			for (Node child : children){
				Move m = child.getMoveMade();
				double score = child.getReward();
				if (scores.containsKey(m)){
					scores.put(m, scores.get(m) + score);
				} else {
					scores.put(m, score);
				}
			}
			if (numReceived == numWorkers){
				double bestScore = 0;
				Move bestMove = null;
				for (Move m : scores.keySet()){
					double score = scores.get(m);
					if (score > bestScore){
						bestScore = score;
						bestMove = m;
					}
				}
				returnActor.tell(bestMove, getSelf());
			}
		} else if (message instanceof SearchStart){
			returnActor = getSender();
			//for (int i = 0; i < numWorkers; i++){
				//workerRouter.tell(new SearchJob(new BasicMCTS(timeout, colour, ucbConstant), state), getSelf());
//				Runtime.getRuntime().exec("java WorkerMain 2551");
//				Runtime.getRuntime().exec("java WorkerMain 2552");
			//}
		} else if (message.equals(TransformationMessages.BACKEND_REGISTRATION)) {
			getContext().watch(getSender());
			workers.add(getSender());
			if (workers.size() == numWorkers){
				for (ActorRef a : workers){
					a.tell(new SearchJob(new BasicMCTS(timeout, colour, ucbConstant), state), getSelf());
				}
			}
		} else if (message instanceof Terminated) {
			Terminated terminated = (Terminated) message;
			workers.remove(terminated.getActor());
		}  else {
			unhandled(message);
		}
	}
}
