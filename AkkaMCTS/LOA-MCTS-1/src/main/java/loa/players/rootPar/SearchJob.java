package loa.players.rootPar;

import java.io.Serializable;

import loa.players.basic.BasicMCTS;
import loa.utils.Board;

public class SearchJob implements Serializable {
	private final BasicMCTS basic;
	private final Board state;
	
	public SearchJob(BasicMCTS basic, Board state){
		this.basic = basic;
		this.state = state;
	}

	public BasicMCTS getBasic() {
		return basic;
	}

	public Board getState() {
		return state;
	}
}
