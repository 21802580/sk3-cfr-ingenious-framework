package loa.players.rootPar;

import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import cluster.TransformationMessages;
import cluster.TransformationMessages.TransformationJob;
import cluster.TransformationMessages.TransformationResult;
import loa.players.basic.BasicMCTS;
import loa.utils.*;

public class Worker extends UntypedActor {
	
	Cluster cluster = Cluster.get(getContext().system());
	
	public Worker(){

	}

	//subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}

	@Override
	public void onReceive(Object message) {
		if (message instanceof SearchJob){
			SearchJob job = (SearchJob)message;
			BasicMCTS basic = job.getBasic();
			Board state = job.getState();
			basic.getMove(state);
			Node root = basic.getRoot();
			getSender().tell(root, getSelf());
		} else if (message instanceof CurrentClusterState) {
			CurrentClusterState state = (CurrentClusterState) message;
			for (Member member : state.getMembers()) {
				if (member.status().equals(MemberStatus.up())) {
					register(member);
				}
			}
		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			register(mUp.member());

		} else {
			unhandled(message);
		}
	}

	void register(Member member) {
		if (member.hasRole("master"))
			getContext().actorSelection(member.address() + "/user/master").tell(
					TransformationMessages.BACKEND_REGISTRATION, getSelf());
	}
}
