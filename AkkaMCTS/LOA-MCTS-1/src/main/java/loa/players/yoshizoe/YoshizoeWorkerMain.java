package loa.players.yoshizoe;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class YoshizoeWorkerMain {
	public static void main(String[] args) {
		// Override the configuration of the port when specified as program argument
		String seedhost = args[0];
		String seedport = args[1];
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Config config = ConfigFactory.parseString("akka.remote.log-remote-lifecycle-events = off").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.maximum-frame-size = 2097152")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [worker]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://YoshizoeSystem@" + seedhost + ":" + seedport + "\"]")).
				withFallback(ConfigFactory.load());

		ActorSystem system = ActorSystem.create("YoshizoeSystem", config);

		system.actorOf(Props.create(YoshizoeWorker.class), "yoshizoe_worker");
	}
}
