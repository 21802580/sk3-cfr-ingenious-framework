package loa.players.yoshizoe;

import java.io.Serializable;

import loa.utils.Move;

public class SearchJob implements Serializable{
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int parentHash;
	private int hash;
	private int[][] state;
	private int threshold;
	private Move lastMoveMade;
	
	public SearchJob(int timeout, int colour, double ucbConstant, int hash, int parentHash, int[][] state, int threshold, Move lastMoveMade){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.hash = hash;
		this.parentHash = parentHash;
		this.state = state;
		this.threshold = threshold;
		this.lastMoveMade = lastMoveMade;
	}

	public int getTimeout() {
		return timeout;
	}

	public int getColour() {
		return colour;
	}

	public double getUcbConstant() {
		return ucbConstant;
	}

	public int getHash() {
		return hash;
	}
	
	public int[][] getState(){
		return state;
	}
	
	public int getThreshold(){
		return threshold;
	}
	
	public Move getLastMoveMade(){
		return this.lastMoveMade;
	}
	
	public int getParentHash(){
		return parentHash;
	}
}
