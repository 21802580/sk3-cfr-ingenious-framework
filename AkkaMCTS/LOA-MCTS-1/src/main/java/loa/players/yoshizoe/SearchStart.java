package loa.players.yoshizoe;

import java.io.Serializable;

public class SearchStart implements Serializable{
	private int[][] board;
	
	public SearchStart(int[][] board){
		this.board = board;
	}
	
	public int[][] getBoard(){
		return this.board;
	}
}
