package loa.players.yoshizoe;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import cluster.TransformationMessages;
import loa.utils.Move;

public class YoshizoeWorker extends UntypedActor{
	private ActorRef[] workers;
	private Zobrist table;
	private SearchJob firstJob;
	private ActorRef master;
	private boolean running;
	Cluster cluster = Cluster.get(getContext().system());
	
	public YoshizoeWorker(){
		firstJob = null;
		master = null;
		running = true;
	}
	
	//subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof ActorRef[]){
			workers = (ActorRef[]) message;
			running = true;
			table = new Zobrist();
			firstJob = null;
		} else if (message instanceof SearchJob){
			if (running == false){
				// Move has been made. Stop the job
				return;
			}
			SearchJob sj = (SearchJob) message;
			//System.out.println("Received search job for node:");
			//printBoard(sj.getState());
			if (firstJob == null){
				firstJob = sj;
				if (sj.getParentHash() == 0){
					//System.out.println("ROOT HOME");
					master = getSender();
					final long start = System.currentTimeMillis();
					new Thread(new Runnable() {
						public void run() {
							while (true) {
								if ((System.currentTimeMillis()) - start > firstJob.getTimeout() - 10){
									master.tell(table.getNode(firstJob.getHash()), getSelf());
									for (ActorRef a : workers){
										a.tell(new SearchComplete(), getSelf());
									}
									break;
								}
								try {
									Thread.sleep(10);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}
					}).start();
				}
			}
			int hash = sj.getHash();
			YoshizoeNode node = null;
			if (table.contains(hash)){
				// The node has already been encountered
				//System.out.println("Retrieving node from hash table");
				node = table.getNode(hash);
				actionSelection(node, sj);
			} else {
				// The node must be added to the tree (new leaf node)
				//System.out.println("Adding node to hash table");
				node = new YoshizoeNode(sj.getState(), sj.getHash(), sj.getParentHash(), sj.getUcbConstant(), sj.getColour(), sj.getLastMoveMade());
				table.put(node);
				actionSelection(node, sj);
			}
		} else if (message instanceof ReportJob){
			ReportJob rj = (ReportJob)message;
			Move child = rj.getChild();
			int hash = rj.getHash();
			int winner = rj.getWinner();
			YoshizoeNode node = table.getNode(hash);
			//System.out.println("Received report job for node:");
			//printBoard(node.getState());
			int childIndex = node.getChildIndex(child);
			if (winner == rj.getMyColour()){
				node.incW(childIndex);
			}
			node.incS(childIndex);
			if (node.getParentHash() == 0){
				// We are at the root node. Start another search
				if (running){
					int home = table.getHomeProcessor(hash, workers.length);
					workers[home].tell(firstJob, getSelf());
				}
			} else {
				// Propogate job to parent
				ReportJob j = new ReportJob(node.getParentHash(), winner, rj.getMyColour(), node.getLastMoveMade());
				int home = table.getHomeProcessor(node.getParentHash(), workers.length);
				workers[home].tell(j, getSelf());
			}
		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			register(mUp.member());
		} else if (message instanceof SearchComplete){
			running = false;
		}
		else {
			unhandled(message);
		}
	}
	
	private void printBoard(int[][] state){
		String s = "";
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8 - 1; j++){
				switch (state[i][j]) {
					case 0:
						s = s + "W ";
						break;
					case 1:
						s = s + "B ";
						break;
					case 2:
						s = s + ". ";
						break;
				}
			}
			switch (state[i][8 - 1]) {
				case 0:
					s = s + "W\n";
					break;
				case 1:
					s = s + "B\n";
					break;
				case 2:
					s = s + ".\n";
					break;
			}
		}
		System.out.println(s);
	}
	
	private void actionSelection(YoshizoeNode node, SearchJob sj){
		if (running == false){
			// Move has been made. Stop the job
			return;
		}
		if (node.getT() < sj.getThreshold()){
			performPlayout(node, sj);
		} else {
			Move m = node.getBestChild();
			node.incT();
			int[][] newState = node.getNewState(m);
			//System.out.println("Sending node:");
			//printBoard(newState);
			int newHash = table.getHash(newState);
			SearchJob j = new SearchJob(sj.getTimeout(), sj.getColour(), sj.getUcbConstant(), newHash, sj.getHash(), newState, sj.getThreshold(), m);
			int home = table.getHomeProcessor(newHash, workers.length);
			workers[home].tell(j, getSelf());
		}
	}
	
	private void performPlayout(YoshizoeNode node, SearchJob sj){
		//System.out.println("Performing playout from node:");
		//printBoard(node.getState());
		int winner = node.doPlayout();
		//System.out.println("Winner: " + winner);
		node.incT();
		ReportJob rj = null;
		if (node.getParentHash() == 0){
			// We are at the root node. Start another search
			//System.out.println("Starting another search at root");
			if (running){
				int hash = node.getHash();
				int home = table.getHomeProcessor(hash, workers.length);
				workers[home].tell(firstJob, getSelf());	
			}
		} else {
			// Propagate job to parent
			//System.out.println("Sending report job for node:");
			//printBoard(node.getState());
			rj = new ReportJob(node.getParentHash(), winner, sj.getColour(), node.getLastMoveMade());
			int home = table.getHomeProcessor(node.getParentHash(), workers.length);
			workers[home].tell(rj, getSelf());
		}
	}
	
	private void register(Member member) {
		if (member.hasRole("master"))
			getContext().actorSelection(member.address() + "/user/master").tell(
					TransformationMessages.BACKEND_REGISTRATION, getSelf());
	}
}
