package loa.players.yoshizoe;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.utils.Board;
import loa.utils.Move;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

public class YoshizoePlayer {
	private Move m;
	private Config config;
//	final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
//			withFallback(ConfigFactory.parseString("akka.cluster.roles = [master]")).
//			withFallback(ConfigFactory.load());
	private ActorRef master;
	
	public YoshizoePlayer(int timeout, int colour, double ucbConstant, int numWorkers, boolean dfUCT, int threshold, String seedHost, String seedPort){
		config = ConfigFactory.parseString("akka.remote.log-remote-lifecycle-events = off").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + seedHost)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + seedPort)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.maximum-frame-size = 2097152")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [master]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://YoshizoeSystem@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("YoshizoeSystem", config);
		master = system.actorOf(Props.create(YoshizoeMaster.class, timeout, colour, ucbConstant, numWorkers, threshold), "yoshizoe_master");
	}
	
	public Move getMove(Board state){
		Timeout timeout = new Timeout(Duration.create(21474835, "seconds"));
		Future<Object> fut = Patterns.ask(master, new SearchStart(state.getState()), timeout);
		try {
			m = (Move) Await.result(fut, timeout.duration());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m;
	}
}
