package loa.players.yoshizoe;

import java.util.ArrayList;

import akka.actor.ActorRef;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import cluster.TransformationMessages;
import loa.utils.Board;
import loa.utils.Move;

public class YoshizoeMaster extends UntypedActor {
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int numWorkers;
	private int numWorkersConnected;
	private ActorRef returnActor;
	private int nparallel;
	private ActorRef[] workers;
	private Zobrist zobrist;
	private int[][] board;
	private int threshold;
	
	public YoshizoeMaster(int timeout, int colour, double ucbConstant, int numWorkers, int threshold){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.numWorkers = numWorkers;
		this.nparallel = 20 * numWorkers;
		this.numWorkersConnected = 0;
		this.workers = new ActorRef[numWorkers];
		this.threshold = threshold;
		zobrist = new Zobrist();
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof SearchStart){
			returnActor = getSender();
			SearchStart ss = (SearchStart)message;
			board = ss.getBoard();
			new Thread(new Runnable() {
				public void run() {
					// TODO Auto-generated method stub
					while (true){
						if (numWorkersConnected == numWorkers){
							int homeHash = zobrist.getHash(board);
							int homeProcessor = zobrist.getHomeProcessor(homeHash, numWorkers);
							ActorRef homeActor = workers[homeProcessor];
							for (int i = 0; i < numWorkers; i++){
								workers[i].tell(workers, getSelf());
							}
							for (int i = 0; i < nparallel; i++){
								homeActor.tell(new SearchJob(timeout, colour, ucbConstant, homeHash, 0, board, threshold, null), getSelf());
							}
							break;
						}
						try {
							Thread.sleep(10);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}	
			}).start();
		} else if (message instanceof YoshizoeNode){
			YoshizoeNode node = (YoshizoeNode) message;
			System.out.println("Playouts: " + node.getT());
			returnActor.tell(node.getBestChild(), getSelf());
		} else if (message.equals(TransformationMessages.BACKEND_REGISTRATION)) {
			getContext().watch(getSender());
			workers[numWorkersConnected] = getSender();
			numWorkersConnected++;
		} else if (message instanceof Terminated) {
			Terminated terminated = (Terminated) message;
		} else {
			unhandled(message);
		}
	}
}
