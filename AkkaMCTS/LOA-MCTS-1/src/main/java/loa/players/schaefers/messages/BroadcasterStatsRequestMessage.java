package loa.players.schaefers.messages;

import java.io.Serializable;

public class BroadcasterStatsRequestMessage implements Serializable{
	private int hash;
	private int length;
	
	public BroadcasterStatsRequestMessage(int hash, int length){
		this.hash = hash;
		this.length = length;
	}
	
	public int getHash(){
		return this.hash;
	}
	
	public int getLength(){
		return this.length;
	}
}
