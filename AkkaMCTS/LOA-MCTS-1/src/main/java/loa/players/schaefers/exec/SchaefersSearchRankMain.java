package loa.players.schaefers.exec;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import loa.players.schaefers.actors.SchaefersSearchRank;
import loa.players.yoshizoe.YoshizoeWorker;

public class SchaefersSearchRankMain {
	public static void main(String[] args) {
		// Override the configuration of the port when specified as program argument
		String seedhost = args[0];
		String seedport = args[1];
		double reductionRate = Double.parseDouble(args[2]);
		int minSForDuplication = Integer.parseInt(args[3]); 
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final Config config = ConfigFactory.parseString("akka.remote.log-remote-lifecycle-events = off").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.maximum-frame-size = 2097152")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [worker]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://SchaefersSystem@" + seedhost + ":" + seedport + "\"]")).
				withFallback(ConfigFactory.load());

		ActorSystem system = ActorSystem.create("SchaefersSystem", config);

		system.actorOf(Props.create(SchaefersSearchRank.class, reductionRate, minSForDuplication)
				.withDispatcher("akka.actor.prio-dispatcher"), "schaefers_worker");
	}
}
