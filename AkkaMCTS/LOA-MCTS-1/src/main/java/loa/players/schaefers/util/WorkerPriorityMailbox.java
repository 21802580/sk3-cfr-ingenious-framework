package loa.players.schaefers.util;
import com.typesafe.config.Config;

import akka.actor.ActorSystem;
import akka.dispatch.PriorityGenerator;
import akka.dispatch.UnboundedStablePriorityMailbox;
import loa.players.schaefers.messages.NodeBroadcastMessage;
import loa.players.yoshizoe.SearchComplete;

public class WorkerPriorityMailbox extends UnboundedStablePriorityMailbox {
	// needed for reflective instantiation
	public WorkerPriorityMailbox(ActorSystem.Settings settings, Config config) {
		// Create a new PriorityGenerator, lower prio means more important
		super(new PriorityGenerator() {
			@Override
			public int gen(Object message) {
				if (message.equals("SearchComplete")){
					return 0;
				} else if ((message instanceof NodeBroadcastMessage) || (message instanceof SchaefersNode)) {
					return 1;
				} else {
					return 2;
				}
			}
		});
	}
}
