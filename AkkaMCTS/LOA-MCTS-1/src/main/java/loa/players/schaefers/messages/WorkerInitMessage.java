package loa.players.schaefers.messages;

public class WorkerInitMessage {
	public int timeout;
	public int colour;
	public double ucbConstant;
	public int threshold;
	
	public WorkerInitMessage(int timeout, int colour, double ucbConstant, int threshold){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.threshold = threshold;
	}
}
