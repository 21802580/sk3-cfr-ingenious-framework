package loa.players.schaefers.util;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.players.schaefers.actors.SchaefersMaster;
import loa.players.yoshizoe.SearchStart;
import loa.players.yoshizoe.YoshizoeMaster;
import loa.utils.Board;
import loa.utils.Move;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

public class SchaefersPlayer {
	private ActorRef master;
	
	public SchaefersPlayer(int timeout, int colour, double ucbConstant, int threshold, int numWorkers, int numBroadcasters, String seedHost, String seedPort){
		Config config = ConfigFactory.parseString("akka.remote.log-remote-lifecycle-events = off").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + seedHost)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + seedPort)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.maximum-frame-size = 2097152")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [schaefers_master]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://SchaefersSystem@" + seedHost + ":" + seedPort + "\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("SchaefersSystem", config);
		master = system.actorOf(Props.create(SchaefersMaster.class, timeout, colour, ucbConstant, threshold, numWorkers, numBroadcasters), "schaefers_master");
	}
	
	public Move getMove(Board state){
		Timeout timeout = new Timeout(Duration.create(60 * 20, "seconds"));
		Future<Object> fut = Patterns.ask(master, new SearchStart(state.getState()), timeout);
		Move m = null;
		try {
			m = (Move) Await.result(fut, timeout.duration());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m;
	}
}
