package loa.players.schaefers.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;
import loa.utils.Coord;
import loa.utils.Move;

public class SchaefersNode implements Serializable {
	private int[][] state;
	private int hash;
	public ArrayList<Move> actions;
	private ArrayList<Move> expandedActions;
	private ArrayList<Move> unexpandedActions;
	private int t;
	private ArrayList<Integer> s;
	public ArrayList<Integer> sDelta;
	private ArrayList<Integer> w;
	public ArrayList<Integer> wDelta;
	private double c;
	public int myColour;
	public int currentPlayer;
	private int numWhite;
	private int numBlack;

	// Colour constants
	private static final int WHITE = 0;
	private static final int BLACK = 1;
	private static final int EMPTY = 2;

	public static void main(String[] args){
		int boardSize = 8;
		int[][] s = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					s[i][j] = BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					s[i][j] = WHITE;
				} else {
					s[i][j] = EMPTY;	
				}
			}
		}
		//SchaefersNode n = new SchaefersNode(s, 0, 0, Math.sqrt(2), BLACK, null);
	}

	public SchaefersNode(int[][] state, int hash, double ucbConstant, int myColour, int currentPlayer) {
		this.state = state;
		this.hash = hash;
		actions = new ArrayList<Move>();
		expandedActions = new ArrayList<Move>();
		unexpandedActions = new ArrayList<Move>();
		t = 0;
		s = new ArrayList<Integer>();
		sDelta = new ArrayList<Integer>();
		w = new ArrayList<Integer>();
		wDelta = new ArrayList<Integer>();
		c = ucbConstant;
		this.myColour = myColour;
		this.currentPlayer = currentPlayer;
		getPossibleMoves();
	}

	public int getChildIndex(Move m){
		return actions.indexOf(m);
	}

	public int[][] getState() {
		return state;
	}

	public int getHash(){
		return hash;
	}

	public int getT(){
		return t;
	}			

	public int getS(int i){
		return s.get(i);
	}

	public int getW(int i){
		return w.get(i);
	}

	public int getSDelta(int i){
		return s.get(i);
	}

	public int getWDelta(int i){
		return w.get(i);
	}

	public void incT(){
		t++;
	}

	public void incW(int i){
		w.set(i, w.get(i) + 1);
	}

	public void incS(int i){
		s.set(i, s.get(i) + 1);
	}

	public void incWDelta(int i){
		wDelta.set(i, wDelta.get(i) + 1);
	}

	public void incSDelta(int i){
		sDelta.set(i, sDelta.get(i) + 1);
	}



	public void incorporateDeltas(ArrayList<Integer> wd, ArrayList<Integer> sd){
		for (int i = 0; i < w.size(); i++){
			w.set(i, wd.get(i));
			s.set(i, sd.get(i));
		}
	}

	public void zeroDeltas(){
		for (int i = 0; i < w.size(); i++){
			wDelta.set(i, 0);
			sDelta.set(i, 0);
		}
	}

	public ArrayList<Move> getChildren(){
		return actions;
	}

	public int[][] getNewState(Move m) {
		int[][] ret = new int[8][8];
		int fromRow = m.getFrom().getRow();
		int fromCol = m.getFrom().getCol();
		int toRow = m.getTo().getRow();
		int toCol = m.getTo().getCol();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				ret[i][j] = state[i][j];
			}
		}
		ret[toRow][toCol] = ret[fromRow][fromCol];
		ret[fromRow][fromCol] = EMPTY;
		return ret;
	}

	public Move getBestChild(){
		Move bestMove = null;
		if (expandedActions.size() == actions.size()){
			// The node is fully expanded. Select the highest UCB
			double bestUCB = getUCB(0);
			bestMove = actions.get(0);
			for (int i = 1; i < actions.size(); i++){
				double ucb = getUCB(i);
				if (ucb > bestUCB){
					bestUCB = ucb;
					bestMove = actions.get(i);
				}
			}
		} else {
			// The node has unexplored children. Select a random unexpanded action
			Random r = new Random();
			bestMove = unexpandedActions.remove(r.nextInt(unexpandedActions.size()));
			expandedActions.add(bestMove);
		}
		return bestMove;
	}

	public Move getSecondBestChild(){
		double bestUCB = getUCB(0);
		Move bestMove = actions.get(0);
		Move secondBestMove = null;
		for (int i = 1; i < actions.size(); i++){
			double ucb = getUCB(i);
			if (ucb > bestUCB){
				secondBestMove = new Move(new Coord(bestMove.getFrom().getRow(), bestMove.getFrom().getCol()), 
											new Coord(bestMove.getTo().getRow(), bestMove.getTo().getCol()));
				bestUCB = ucb;
				bestMove = actions.get(i);
			}
		}
		return secondBestMove;
	}

	public double getUCB(int i){
		double wi = (double)w.get(i);
		double si = (double)s.get(i);
		return (wi / si) + (c * (Math.sqrt((Math.log((double)t)) / (si))));
	}

	public static void printBoard(int[][] state){
		String s = "";
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8 - 1; j++){
				switch (state[i][j]) {
				case 0:
					s = s + "W ";
					break;
				case 1:
					s = s + "B ";
					break;
				case 2:
					s = s + ". ";
					break;
				}
			}
			switch (state[i][8 - 1]) {
			case 0:
				s = s + "W\n";
				break;
			case 1:
				s = s + "B\n";
				break;
			case 2:
				s = s + ".\n";
				break;
			}
		}
		System.out.println(s);
	}

	public int doPlayout() {
		int[][] s = new int[8][8];
		numWhite = 0;
		numBlack = 0;
		int curPlayer = currentPlayer;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				s[i][j] = state[i][j];
				if (s[i][j] == WHITE){
					numWhite++;
				} else if (s[i][j] == BLACK){
					numBlack++;
				}
			}
		}

		while (!isTerminal(s)){
			Move m = getRandomMove(s, curPlayer);
			if (m == null){
				if (curPlayer == WHITE){
					curPlayer = BLACK;
					//System.out.println("White move: No possible moves. Skipping turn");
				} else {
					curPlayer = WHITE;
					//System.out.println("Black move: No possible moves. Skipping turn");
				}
				continue;
			}
			int fromRow = m.getFrom().getRow();
			int fromCol = m.getFrom().getCol();
			int toRow = m.getTo().getRow();
			int toCol = m.getTo().getCol();

			// Move the piece
			if (s[toRow][toCol] == BLACK){
				numBlack--;
			} else if (s[toRow][toCol] == WHITE){
				numWhite--;
			}
			s[toRow][toCol] = s[fromRow][fromCol];
			s[fromRow][fromCol] = EMPTY;

			if (curPlayer == WHITE){
				curPlayer = BLACK;
			} else {
				curPlayer = WHITE;
			}
		}

		if (whiteWins(s)){
			return WHITE;
		} else {
			return BLACK;
		}
	}

	private Move getRandomMove(int[][] b, int currentPlayer){
		// Find co-ordinates of my pieces
		ArrayList<Integer> rows = new ArrayList<Integer>();
		ArrayList<Integer> cols = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (b[i][j] == currentPlayer){
					rows.add(i);
					cols.add(j);
				}
			}
		}

		// Find a random legal move 
		Random rand = new Random();
		int numElements = rows.size();
		for (int k = 0; k < numElements; k++){
			int index = rand.nextInt(rows.size());
			int fromRow = rows.remove(index);
			int fromCol = cols.remove(index);
			for (int i = 0; i < 8; i++){
				for (int j = 0; j < 8; j++){
					if (moveLegal(b, fromRow, fromCol, i, j, currentPlayer)){
						return new Move(new Coord(fromRow, fromCol), new Coord(i, j));
					}
				}
			}	
		}
		return null;
	}

	/*
	 * Returns true of the board is in a terminal state, false otherwise
	 */
	private boolean isTerminal(int[][] b){
		return whiteWins(b) || blackWins(b);
	}

	/*
	 * Uses the connected(Coord c) method to check if the white player wins
	 */
	private boolean whiteWins(int[][] b){
		boolean win = false;
		outer: {
			for (int i = 0; i < b.length; i++){
				for (int j = 0; j < b.length; j++){
					if (b[i][j] == WHITE){
						win = connected(new Coord(i, j), b);
						break outer;
					}

				}
			}
		}	
		return win;
	}

	/*
	 * Uses the connected(Coord c) method to check if the black player wins
	 */
	private boolean blackWins(int[][] b){
		boolean win = false;
		outer: {
			for (int i = 0; i < b.length; i++){
				for (int j = 0; j < b.length; j++){
					if (b[i][j] == BLACK){
						win = connected(new Coord(i, j), b);
						break outer;
					}

				}
			}
		}	
		return win;
	}

	/*
	 * Use a breadth-first-search to check if a player's pieces are connected
	 * Returns true if all a player's pieces are connected to the piece located at c
	 */
	private boolean connected(Coord c, int[][] b){
		int colour = b[c.getRow()][c.getCol()];
		LinkedList<Coord> q = new LinkedList<Coord>();
		HashSet<Coord> checked = new HashSet<Coord>();
		q.add(c);
		while(!q.isEmpty()){
			Coord current = q.remove();
			int row = current.getRow();
			int col = current.getCol();
			if (b[row][col] == colour){
				checked.add(current);
				Coord n = new Coord(row - 1, col);
				Coord s = new Coord(row + 1, col);
				Coord e = new Coord(row, col + 1);
				Coord w = new Coord(row, col - 1);
				Coord ne = new Coord(row - 1, col + 1);
				Coord nw = new Coord(row - 1, col - 1);
				Coord se = new Coord(row + 1, col + 1);
				Coord sw = new Coord(row + 1, col - 1);
				if ((row > 0) && !checked.contains(n)){
					q.add(n);
				}
				if ((row < b.length - 1) && !checked.contains(s)){
					q.add(s);	
				}
				if ((col < b.length - 1) && !checked.contains(e)){
					q.add(e);
				}
				if ((col > 0) && !checked.contains(w)){
					q.add(w);
				}
				if ((row > 0) && (col < b.length - 1) && !checked.contains(ne)){
					q.add(ne);
				}
				if ((row > 0) && (col > 0) && !checked.contains(nw)){
					q.add(nw);
				}
				if ((col < b.length - 1) && (row < b.length - 1) && !checked.contains(se)){
					q.add(se);
				}
				if ((col > 0) && (row < b.length - 1) && !checked.contains(sw)){
					q.add(sw);
				}
			}
		}
		if (colour == WHITE){
			return checked.size() == numWhite;
		} else {
			return checked.size() == numBlack;
		}
	}

	private void getPossibleMoves(){
		// Find co-ordinates of my pieces
		ArrayList<Integer> rows = new ArrayList<Integer>();
		ArrayList<Integer> cols = new ArrayList<Integer>();
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (state[i][j] == currentPlayer){
					rows.add(i);
					cols.add(j);
				}
			}
		}

		int numElements = rows.size();
		for (int k = 0; k < numElements; k++){
			int fromRow = rows.get(k);
			int fromCol = cols.get(k);
			for (int i = 0; i < 8; i++){
				for (int j = 0; j < 8; j++){
					if (moveLegal(state, fromRow, fromCol, i, j, currentPlayer)){
						Move m = new Move(new Coord(fromRow,fromCol), new Coord(i, j));
						actions.add(m);
						unexpandedActions.add(m);
						s.add(0);
						w.add(0);
						sDelta.add(0);
						wDelta.add(0);
					}
				}
			}	
		}
	}

	/*
	 * Return true if the move is legal, false otherwise
	 */
	private boolean moveLegal(int[][] b, int fromRow, int fromCol, int toRow, int toCol, int currentPlayer){
		// No piece to move or trying to move opponent's piece
		if ((b[fromRow][fromCol] == EMPTY) || (b[fromRow][fromCol] != currentPlayer)){
			return false;
		}

		// Landing on own piece
		if (b[fromRow][fromCol] == b[toRow][toCol]) {
			return false;
		}

		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (fromRow == toRow){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < 8; i++){
				if (b[fromRow][i] != EMPTY){
					numPieces++;
					if (toCol > fromCol){
						if ((i > fromCol) && (i < toCol) && (b[fromRow][i] != currentPlayer)){
							return false;
						}
					} else if (fromCol > toCol) {
						if ((i > toCol) && (i < fromCol) && (b[fromRow][i] != currentPlayer)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toCol - fromCol)){
				return false;
			}
		} else if (fromCol == toCol){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < 8; i++){
				if (b[i][fromCol] != EMPTY){
					numPieces++;
					if (toRow > fromRow){
						if ((i > fromRow) && (i < toRow) && (b[i][fromCol] != currentPlayer)){
							return false;
						}
					} else if (fromCol > toCol) {
						if ((i > toRow) && (i < fromRow) && (b[i][fromCol] != currentPlayer)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toRow - fromRow)){
				return false;
			}
		} else {
			if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i < 8 && j < 8; i++, j++){
					if (b[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow - 1, j = fromCol - 1; i >= 0 && j >= 0; i--, j--){
					if (b[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(toRow - fromRow)){
					return false;
				}
			} else if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i >= 0 && j < 8; i--, j++){
					if (b[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow + 1, j = fromCol - 1; i < 8 && j >= 0; i++, j--){
					if (b[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (b[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(fromRow - toRow)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}
}
