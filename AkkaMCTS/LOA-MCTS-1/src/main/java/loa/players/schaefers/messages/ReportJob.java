package loa.players.schaefers.messages;

import java.util.ArrayDeque;
import loa.utils.Move;

public class ReportJob {
	private int hash;
	private ArrayDeque<Integer> ancestors;
	private ArrayDeque<Move> movesMade;
	private int winner;
	
	public ReportJob(int hash, int winner, ArrayDeque<Integer> ancestors, ArrayDeque<Move> movesMade){
		this.hash = hash;
		this.winner = winner;
		this.ancestors = ancestors;
		this.movesMade = movesMade;
	}

	public int getHash() {
		return hash;
	}

	public int getWinner() {
		return winner;
	}
	
	public ArrayDeque<Integer> getAncestors(){
		return this.ancestors;
	}
	
	public ArrayDeque<Move> getMovesMade(){
		return this.movesMade;
	}
	
	public boolean isRoot(){
		return ancestors.peek() == 0;
	}
	
	public int getParent(){
		return ancestors.pop();
	}

	public Move getChild() {
		return movesMade.pop();
	}
}
