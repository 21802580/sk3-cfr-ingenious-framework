package loa.players.schaefers.messages;

import java.io.Serializable;

public class WorkerStatsRequestMessage implements Serializable {
	private int hash;
	
	public WorkerStatsRequestMessage(int hash){
		this.hash = hash;
	}
	
	public int getHash(){
		return this.hash;
	}
}
