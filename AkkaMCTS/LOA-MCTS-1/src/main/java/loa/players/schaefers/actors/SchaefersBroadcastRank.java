package loa.players.schaefers.actors;

import java.util.ArrayList;
import java.util.HashMap;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.ClusterEvent.MemberUp;
import loa.players.schaefers.messages.BroadcasterStatsRequestMessage;
import loa.players.schaefers.messages.BroadcasterStatsResponseMessage;
import loa.players.schaefers.messages.FinalStatsMessage;
import loa.players.schaefers.messages.NodeBroadcastMessage;
import loa.players.schaefers.messages.SynchroniseMessage;
import loa.players.schaefers.messages.WorkerStatsRequestMessage;
import loa.players.schaefers.messages.WorkerStatsResponseMessage;
import loa.players.schaefers.util.SchaefersNode;

public class SchaefersBroadcastRank extends UntypedActor{
	Cluster cluster = Cluster.get(getContext().system());
	private ActorRef[] broadcasters;
	private ArrayList<ActorRef> workers;
	private HashMap<Integer, ArrayList<Integer>> w;
	private HashMap<Integer, ArrayList<Integer>> s;
	private HashMap<Integer, ActorRef> syncNodes;
	private HashMap<Integer, Integer> numWorkersResponded;
	private HashMap<Integer, Integer> numBroadcastersResponded;
	private boolean running;

	public SchaefersBroadcastRank() {
		System.out.println("This is broadcaster " + getSelf());
		w = new HashMap<Integer, ArrayList<Integer>>();
		s = new HashMap<Integer, ArrayList<Integer>>();
		numWorkersResponded = new HashMap<Integer, Integer>();
		numBroadcastersResponded = new HashMap<Integer, Integer>();
		syncNodes = new HashMap<Integer, ActorRef>();
		running = true;
	}

	//subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message.equals("Init")){
			running = true;
		} else if (!running){
			return;
		} else if (message instanceof ActorRef[]) {
			broadcasters = (ActorRef[]) message;
		} else if (message instanceof ArrayList) {
			workers = (ArrayList<ActorRef>)message;
		} else if (message instanceof NodeBroadcastMessage){
			NodeBroadcastMessage n = (NodeBroadcastMessage)message;
			System.out.println("NodeBroadcastMessage requested for " + n.getNode().getHash());
			sendNodeToBroadcasters(n.getNode());
			sendNodeToWorkers(n.getNode());
		} else if (message instanceof SchaefersNode){
			System.out.println("Node " + ((SchaefersNode)message).getHash() + " received");
			sendNodeToWorkers((SchaefersNode)message);
		} else if (message instanceof SynchroniseMessage){
			System.out.println("Synchronize requested for node " + ((SynchroniseMessage)message).getHash());
			synchroniseNode((SynchroniseMessage)message, getSender());
		} else if (message instanceof BroadcasterStatsRequestMessage){
			BroadcasterStatsRequestMessage m = (BroadcasterStatsRequestMessage)message;
			System.out.println("BroadcasterStatsRequestMessage received for node " + m.getHash());
//			ArrayList<Integer> wl = new ArrayList<Integer>();
//			ArrayList<Integer> sl = new ArrayList<Integer>();
//			for (int i = 0; i < m.getLength(); i++){
//				wl.add(0);
//				sl.add(0);
//			}
			syncNodes.put(m.getHash(), getSender());
			numWorkersResponded.put(m.getHash(), 0);
//			w.put(m.getHash(), wl);
//			s.put(m.getHash(), sl);
			getStatsFromWorkers(m.getHash());
		} else if (message instanceof WorkerStatsResponseMessage){
			System.out.println("WorkerStatsRequestMessage received for node " + ((WorkerStatsResponseMessage)message).getHash());
			assimilateWorkerStatistics((WorkerStatsResponseMessage)message);
		} else if (message instanceof BroadcasterStatsResponseMessage){
			System.out.println("BroadcasterStatsResponseMessage received for node " + ((BroadcasterStatsResponseMessage)message).getHash());
			assimilateBroadcasterStatistics((BroadcasterStatsResponseMessage)message);
		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			register(mUp.member());
		} else if (message instanceof FinalStatsMessage) {
			System.out.println("FinalStatsMessage received for node " + ((FinalStatsMessage)message).getHash());
			sendFinalStats((FinalStatsMessage)message);
		} else if (message.equals("SearchComplete")){
			running = false;
		} else {
			unhandled(message);
		}
	}

	/*
	 * Asks each of the workers associated with this broadcaster, as well as all 
	 * other broadcasters for node statistics at the given hash. This method is called
	 * when a search node finds a tree node which must be synchronised
	 */
	private void synchroniseNode(SynchroniseMessage message, ActorRef sender) {
		int hash = message.getHash();
		numWorkersResponded.put(hash, 0);
		numBroadcastersResponded.put(hash, 0);
		ArrayList<Integer> wl = message.getwDelta();
		ArrayList<Integer> sl = message.getsDelta();
		w.put(hash, wl);
		s.put(hash, sl);
		for (ActorRef a : broadcasters){
			if (!a.equals(getSelf())){
				a.tell(new BroadcasterStatsRequestMessage(hash, wl.size()), getSelf());
			}
		}
		for (ActorRef a : workers){
			if (!a.equals(sender)){
				a.tell(new WorkerStatsRequestMessage(hash), getSelf());	
			}
		}
	}

	private void broadcastFinalStatistics(FinalStatsMessage m){
		for (ActorRef a : broadcasters){
			if (!a.equals(getSelf())){
				a.tell(m, getSelf());
			}
		}
		for (ActorRef a : workers){
			a.tell(m, getSelf());
		}
		numWorkersResponded.put(m.getHash(), 0);
		numBroadcastersResponded.put(m.getHash(), 0);
	}

	/*
	 * Asks each worker for node statistics at the given hash and expects a
	 * WorkerStatsResponseMessage in return. This is called when another broadcaster
	 * requests node statistics from this broadcaster.
	 */
	private void getStatsFromWorkers(int hash){
		for (ActorRef a : workers){
			a.tell(new WorkerStatsRequestMessage(hash), getSelf());
		}
	}	

	/*
	 * Broadcasts the given node to each worker for duplication
	 */
	private void sendNodeToWorkers(SchaefersNode n) {
		for (ActorRef worker : workers){
			if (!worker.equals(getSender())){
				worker.tell(n, getSelf());
			}
		}
	}

	/*
	 * Broadcasts the given node to all other broadcasters for duplication
	 */
	private void sendNodeToBroadcasters(SchaefersNode n){
		for (int i = 0; i < broadcasters.length; i++){
			if (!broadcasters[i].equals(getSelf())){
				broadcasters[i].tell(n, getSelf());
			}
		}
	}

	/*
	 * Adds a workers statistics for the synchronized node to w and s
	 */
	private void assimilateWorkerStatistics(WorkerStatsResponseMessage message){
		numWorkersResponded.put(message.getHash(), numWorkersResponded.get(message.getHash()) + 1);
		ArrayList<Integer> wd = message.getwDelta();
		ArrayList<Integer> sd = message.getsDelta();
		int hash = message.getHash();
		ArrayList<Integer> wTot = w.get(hash);
		ArrayList<Integer> sTot = s.get(hash);
		
		// Check that the worker has statistics to share
		if ((wd != null) && (wd.size() > 0)){
			if (wTot == null){
				// This is the first worker that has responded
				w.put(hash, wd);
				s.put(hash, sd);
			} else {
				// Other workers have already responded. Add the elements of each list
				for (int i = 0; i < wd.size(); i++){
					wTot.set(i, wTot.get(i) + wd.get(i));
					sTot.set(i, sTot.get(i) + sd.get(i));
				}	
			}
		}
		
		// If syncNodes.get(hash) is null, this broadcaster initiated the syncronisation and
		// it must broadcast assimilated statistics to other workers.
		//
		// If syncNodes.get(hash) is not null, this broadcaster must send its own worker's
		// statistics back to the initial broadcaster
		if (syncNodes.get(hash) != null){
			if (numWorkersResponded.get(hash) == workers.size()){
				// All worker statistics have been assimilated. Send results to the original broadcaster
				BroadcasterStatsResponseMessage m = new BroadcasterStatsResponseMessage(hash, wTot, sTot);
				syncNodes.get(hash).tell(m, getSelf());
			}	
		} else {
			// Check if all broadcasters and workers have responded.
			// If so, send final statistics to other workers and broadcasters
			if ((numWorkersResponded.get(hash) == workers.size() - 1) && 
					(numBroadcastersResponded.get(hash) == broadcasters.length - 1)){
				FinalStatsMessage m = new FinalStatsMessage(hash, w.get(hash), s.get(hash));
				broadcastFinalStatistics(m);
			}	
		}
	}

	/*
	 * Adds a broadcasters statistics for the synchronized node to w and s
	 */
	private void assimilateBroadcasterStatistics(BroadcasterStatsResponseMessage message){
		numBroadcastersResponded.put(message.getHash(), numBroadcastersResponded.get(message.getHash()) + 1);
		ArrayList<Integer> wd = message.getwDelta();
		ArrayList<Integer> sd = message.getsDelta();
		int hash = message.getHash();
		ArrayList<Integer> wTot = w.get(hash);
		ArrayList<Integer> sTot = s.get(hash);
		
		// Check that the broadcaster has statistics to share
		if ((wd != null) && (wd.size() > 0)){
			for (int i = 0; i < wd.size(); i++){
				wTot.set(i, wTot.get(i) + wd.get(i));
				sTot.set(i, sTot.get(i) + sd.get(i));
			}	
		}
		
		// Check if all broadcasters and workers have responded.
		// If so, send final statistics to other workers and broadcasters
		if ((numWorkersResponded.get(hash) == workers.size() - 1) && 
				(numBroadcastersResponded.get(hash) == broadcasters.length - 1)){
			FinalStatsMessage m = new FinalStatsMessage(hash, w.get(hash), s.get(hash));
			broadcastFinalStatistics(m);
		}
	}

	/*
	 * Sends assimilated node statistics to all workers
	 */
	private void sendFinalStats(FinalStatsMessage message){
		for (ActorRef r : workers){
			r.tell(message, getSelf());
		}
		w.put(message.getHash(), new ArrayList<Integer>());
		s.put(message.getHash(), new ArrayList<Integer>());
		numWorkersResponded.put(message.getHash(), 0);
		syncNodes.put(message.getHash(), null);
	}

	private void register(Member member) {
		if (member.hasRole("schaefers_master"))
			getContext().actorSelection(member.address() + "/user/schaefers_master").tell(
					"BroadcasterRegistration", getSelf());
	}
}
