package loa.players.schaefers.messages;

import java.io.Serializable;
import java.util.ArrayDeque;
import loa.utils.Move;

public class SearchJob implements Serializable{
	private ArrayDeque<Integer> ancestors;
	private ArrayDeque<Move> movesMade;
	private int hash;
	private int[][] state;
	private int currentPlayer;
	
	public static void main(String[] args){
		ArrayDeque<Integer> a = new ArrayDeque<Integer>();
		for (int i = 0; i < 10; i++){
			a.push(i);
		}
		for (int i : a){
			System.out.println(i);
		}
	}
	
	public SearchJob(int hash, int[][] state, ArrayDeque<Integer> ancestors, ArrayDeque<Move> movesMade, int currentPlayer){
		this.ancestors = ancestors;
		this.hash = hash;
		this.state = state;
		this.movesMade = movesMade;
		this.currentPlayer = currentPlayer;
	}

	public int getHash(){
		return hash;
	}
	
	public boolean isRoot(){
		return ancestors.peek() == 0;
	}
	
	public ArrayDeque<Integer> getAncestors(){
		return this.ancestors;
	}
	
	public ArrayDeque<Move> getMovesMade(){
		return this.movesMade;
	}
	
	public int getParent(){
		return ancestors.peek();
	}
	
	public int[][] getState(){
		return state;
	}
	
	public Move getLastMoveMade(){
		return movesMade.peek();
	}
	
	public boolean repeating(Move move){
		int i = 0;
		int count = 0;
		for (Move m : movesMade){
			if (m.equals(move)){
				count++;
			}
			if (count == 4){
				return true;
			}
			if (i++ == 20){
				return false;
			}
		}
		return false;
	}
	
	public int getCurrentPlayer(){
		return this.currentPlayer;
	}
}
