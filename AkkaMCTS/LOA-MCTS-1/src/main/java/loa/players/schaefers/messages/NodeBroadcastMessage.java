package loa.players.schaefers.messages;

import java.io.Serializable;

import loa.players.schaefers.util.SchaefersNode;

public class NodeBroadcastMessage implements Serializable {
	private SchaefersNode node;
	
	public NodeBroadcastMessage(SchaefersNode node){
		this.node = node;
	}
	
	public SchaefersNode getNode(){
		return this.node;
	}
}
