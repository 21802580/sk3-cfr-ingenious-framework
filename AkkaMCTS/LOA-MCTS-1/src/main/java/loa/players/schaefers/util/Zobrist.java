package loa.players.schaefers.util;

import java.util.ArrayList;
import java.util.Random;

public class Zobrist {
	private int[][][] hashes;
	private SchaefersNode[] entries;
	
	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;
	
	public static void main(String[] args){
		Zobrist z1 = new Zobrist();
		Zobrist z2 = new Zobrist();
		int[][] s = new int[8][8];
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 2; k++){
					s[i][j] = (int)(Math.random() * 3);
				}
			}
		}
		
		System.out.println(z1.getHash(s));
		System.out.println(z2.getHash(s));
	}
	
	public Zobrist() {
		hashes = new int[8][8][2];
		entries = new SchaefersNode[1000000];
		Random r = new Random(12345);
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 2; k++){
					hashes[i][j][k] = Math.abs(r.nextInt());
				}
			}
		}
	}
	
	public ArrayList<SchaefersNode> getEntries(){
		ArrayList<SchaefersNode> nodes = new ArrayList<SchaefersNode>();
		for (int i = 0; i < 1000000; i++){
			if (entries[i] != null){
				nodes.add(entries[i]);
			}
		}
		return nodes;
	}
	
	public int getHash(int[][] board){
		int h = 0;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (board[i][j] != EMPTY){
					h = h ^ hashes[i][j][board[i][j]];
				}
			}
		}
		return h;
	}
	
	public boolean contains(int hash){
		int index = hash % entries.length;
		if ((entries[index] != null) && (hash == entries[index].getHash())){
			return true;
		}
		return false;
	}

	public void put(SchaefersNode node){
		int hash = node.getHash();
		int index = hash % entries.length;
		entries[index] = node;
	}
	
	public SchaefersNode getNode(int hash){
		return entries[hash % entries.length];
	}
	
	public int getHomeProcessor(int hash, int numWorkers){
		return hash % numWorkers;
	}
}
