package loa.players.schaefers.actors;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;

import akka.actor.ActorRef;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import loa.players.schaefers.messages.SearchJob;
import loa.players.schaefers.messages.WorkerInitMessage;
import loa.players.schaefers.util.SchaefersNode;
import loa.players.schaefers.util.Zobrist;
import loa.players.yoshizoe.SearchStart;
import loa.utils.Move;

public class SchaefersMaster extends UntypedActor{
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int threshold;
	private int numWorkers;
	private int numWorkersConnected;
	private int numBroadcasters;
	private int numBroadcastersConnected;
	private ActorRef returnActor;
	private int nparallel;
	private ActorRef[] workers;
	private ActorRef[] broadcasters;
	private Zobrist zobrist;
	private int[][] board;

	public SchaefersMaster(int timeout, int colour, double ucbConstant, int threshold, int numWorkers, int numBroadcasters){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.threshold = threshold;
		this.numWorkers = numWorkers;
		this.numBroadcasters = numBroadcasters;
		//this.nparallel = 20 * numWorkers;
		this.nparallel = 1;
		this.numWorkersConnected = 0;
		this.numBroadcastersConnected = 0;
		this.workers = new ActorRef[numWorkers];
		this.broadcasters = new ActorRef[numBroadcasters];
		this.zobrist = new Zobrist();
	}

	private void sendReferences(){
		HashMap<Integer, ArrayList<ActorRef>> broadcasterWorkers = new HashMap<Integer, ArrayList<ActorRef>>();
		for (int i = 0; i < numWorkers; i++){
			// Distribute actor references
			workers[i].tell(workers, getSelf());

			// Supply each actor with a broadcaster
			workers[i].tell(broadcasters[i % numBroadcasters], getSelf());
			ArrayList<ActorRef> al = broadcasterWorkers.get(i % numBroadcasters);
			if (al == null){
				broadcasterWorkers.put(i % numBroadcasters, new ArrayList<ActorRef>());
			}
			al = broadcasterWorkers.get(i % numBroadcasters);
			al.add(workers[i]);
		}
		for (int i = 0; i < numBroadcasters; i++){
			// Distribute broadcaster references
			broadcasters[i].tell(broadcasters, getSelf());
			broadcasters[i].tell(broadcasterWorkers.get(i), getSelf());
		}
	}

	private void actorConnected(){
		if ((numWorkersConnected == numWorkers) && (numBroadcastersConnected == numBroadcasters)){
			sendReferences();
			// Only send the job if the SearchStart message was received
			if (board != null){
				sendJob();
			}
		}
	}

	private void sendJob(){
		int homeHash = zobrist.getHash(board);
		int homeProcessor = zobrist.getHomeProcessor(homeHash, numWorkers);
		for (ActorRef a : workers){
			a.tell(new WorkerInitMessage(timeout, colour, ucbConstant, threshold), getSelf());
		}
		for (ActorRef a : broadcasters){
			a.tell("Init", getSelf());
		}
		ActorRef homeActor = workers[homeProcessor];
		ArrayDeque<Integer> ancestors = new ArrayDeque<Integer>();
		ArrayDeque<Move> movesMade = new ArrayDeque<Move>();
		ancestors.push(0);
		SearchJob job = new SearchJob(homeHash, board, ancestors, movesMade, colour);
		for (int i = 0; i < nparallel; i++){
			homeActor.tell(job, getSelf());
		}
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof SearchStart){
			returnActor = getSender();
			SearchStart ss = (SearchStart)message;
			board = ss.getBoard();
			if ((numWorkersConnected == numWorkers) && (numBroadcastersConnected == numBroadcasters)){
				sendJob();
			}
		} else if (message instanceof SchaefersNode){
			SchaefersNode node = (SchaefersNode) message;
			System.out.println("Playouts: " + node.getT());
			returnActor.tell(node.getBestChild(), getSelf());
			for (ActorRef a : workers){
				a.tell("SearchComplete", getSelf());	
			}
			for (ActorRef a : broadcasters){
				a.tell("SearchComplete", getSelf());
			}
		} else if (message.equals("WorkerRegistration")) {
			ActorRef worker = getSender();
			getContext().watch(worker);
			workers[numWorkersConnected] = getSender();
			numWorkersConnected++;
			actorConnected();
		} else if (message.equals("BroadcasterRegistration")) {
			ActorRef broadcaster = getSender();
			getContext().watch(broadcaster);
			broadcasters[numBroadcastersConnected] = broadcaster;
			numBroadcastersConnected++;
			actorConnected();
		} else if (message instanceof Terminated) {
			Terminated terminated = (Terminated) message;
		} else {
			unhandled(message);
		}
	}
}
