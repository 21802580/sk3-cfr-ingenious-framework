package loa.players.schaefers.actors;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import loa.players.schaefers.messages.FinalStatsMessage;
import loa.players.schaefers.messages.NodeBroadcastMessage;
import loa.players.schaefers.messages.ReportJob;
import loa.players.schaefers.messages.SearchJob;
import loa.players.schaefers.messages.SynchroniseMessage;
import loa.players.schaefers.messages.WorkerInitMessage;
import loa.players.schaefers.messages.WorkerStatsRequestMessage;
import loa.players.schaefers.messages.WorkerStatsResponseMessage;
import loa.players.schaefers.util.SchaefersNode;
import loa.players.schaefers.util.Zobrist;
import loa.utils.Move;

public class SchaefersSearchRank extends UntypedActor{
	private ActorRef[] workers;
	private ActorRef broadcaster;
	private Zobrist table;
	private int rootHash;
	private ActorRef master;
	private double reductionRate;
	private int minTForDuplication;
	private HashMap<Integer, SchaefersNode> duplicatedNodes;
	private boolean running;
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int threshold;
	private int[][] rootState;

	Cluster cluster = Cluster.get(getContext().system());

	public SchaefersSearchRank(double reductionRate, int minTForDuplication){
		this.rootHash = 0;
		this.rootState = new int[8][8];
		this.master = null;
		this.running = true;
		this.reductionRate = reductionRate;
		this.minTForDuplication = minTForDuplication;
		this.duplicatedNodes = new HashMap<Integer, SchaefersNode>();
		this.table = new Zobrist();
	}

	//subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof WorkerInitMessage){
			WorkerInitMessage i = (WorkerInitMessage)message;
			this.timeout = i.timeout;
			this.colour = i.colour;
			this.ucbConstant = i.ucbConstant;
			this.threshold = i.threshold;
			running = true;
		} else if (!running){
			return;
		} else if (message instanceof ActorRef[]){
			workers = (ActorRef[]) message;
		} else if (message instanceof ActorRef){
			broadcaster = (ActorRef)message;
		} else if (message instanceof SearchJob){
			processSearchJob((SearchJob)message);
		} else if (message instanceof ReportJob){
			processReportJob((ReportJob)message);
		} else if (message instanceof SchaefersNode){
			// Add this node to the duplicated nodes hashmap	
			SchaefersNode n = (SchaefersNode)message;
			duplicatedNodes.put(n.getHash(), n);
		} else if (message instanceof WorkerStatsRequestMessage){
			WorkerStatsRequestMessage m = (WorkerStatsRequestMessage) message;
			sendStatsResponseMessage(m.getHash());
		} else if (message instanceof FinalStatsMessage){
			updateSharedStats((FinalStatsMessage)message);
		} else if (message.equals("SearchComplete")){
			System.out.println("SearchComplete received");
			running = false;
			rootHash = 0;
			rootState = new int[8][8];
			duplicatedNodes = new HashMap<Integer, SchaefersNode>();
			table = new Zobrist();
		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			register(mUp.member());
		} else {
			unhandled(message);
		}
	}

	private void processSearchJob(SearchJob message){
		if (rootHash == 0){
			rootHash = message.getHash();
			rootState = message.getState();
			if (message.isRoot()){
				startTimer();
			}
		}
		int hash = message.getHash();
		SchaefersNode node = null;
		if (table.contains(hash)){
			// The node has already been encountered
			node = table.getNode(hash);
			actionSelection(node, message);
		} else {
			// The node must be added to the tree (new leaf node)
			node = new SchaefersNode(message.getState(), message.getHash(), ucbConstant, colour, message.getCurrentPlayer());
			table.put(node);
			actionSelection(node, message);
		}
	}

	private void processReportJob(ReportJob message){	
		int hash = message.getHash();
		int winner = message.getWinner();
		SchaefersNode node = table.getNode(hash);
		if (duplicatedNodes.containsKey(hash) && (!message.isRoot())){
			// This node, as well as its ancestors, are shared. No need to send more ReportJobs.
			propagateInDuplicateNodes(message);
			return;
		}
		Move child = message.getChild();
		System.out.println("REPORT " + message.getHash());
		int childIndex = node.getChildIndex(child);
		if (winner == colour){
			node.incW(childIndex);
		} else if (winner == 2){
			// Draw
		}
		node.incS(childIndex);
		if (message.isRoot()){
			// We are at the root node. Start another search
			if (running){
				ArrayDeque<Integer> ancestors = new ArrayDeque<Integer>();
				ArrayDeque<Move> movesMade = new ArrayDeque<Move>();
				ancestors.push(0);
				SearchJob sj = new SearchJob(rootHash, rootState, ancestors, movesMade, colour);
				processSearchJob(sj);
			}
		} else {
			// Propagate job to parent
			int parent = message.getParent();
			ReportJob rj = new ReportJob(parent, winner, message.getAncestors(), message.getMovesMade());
			int home = table.getHomeProcessor(parent, workers.length);
			workers[home].tell(rj, getSelf());
		}
	}

	private void startTimer(){
		master = getSender();
		final long start = System.currentTimeMillis();
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					if ((System.currentTimeMillis()) - start > timeout - 10){
						master.tell(duplicatedNodes.get(rootHash), getSelf());
						produceDotFile();
						break;
					}
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	private void produceDotFile(){
		Writer writer = null;
		try {
			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("tree.dot"), "utf-8"));
			writer.write("digraph G {\n"
					+ "    nodesep=0.3;\n"
					+ "    ranksep=0.2;\n"
					+ "    margin=0.1;\n"
					+ "    node [shape=circle];\n"
					+ "    edge [arrowsize=0.8];\n");
			//			for (SchaefersNode n : table.getEntries()){
			//				writer.write(n.getParentHash() + " -> " + n.getHash() + ";\n");
			//			}
			writer.write("}");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {writer.close();} catch (Exception ex) {/*ignore*/}
		}        
	}

	/*
	 * Broadcasts the given node to all other workers on the network
	 */
	private void broadcastNode(SchaefersNode node) {
		System.out.println("Broadcasting node " + node.getHash());
		duplicatedNodes.put(node.getHash(), node);
		broadcaster.tell(new NodeBroadcastMessage(node), getSelf());
	}

	/*
	 * Propagates node statistics in local memory instead of via message passing and
	 * synchronizes nodes that satisfy the inequality in section C of the Schaefers/Platzner
	 * paper
	 */
	private void propagateInDuplicateNodes(ReportJob rj){
		int parentHash = rj.getHash();
		int winner = rj.getWinner();
		Move child = null;
		while (parentHash != 0){
			SchaefersNode parent = duplicatedNodes.get(parentHash);
			System.out.println("REPORTDUP: " + parentHash);
			if (parent == null){
				ReportJob j = new ReportJob(parentHash, winner, rj.getAncestors(), rj.getMovesMade());
				int home = table.getHomeProcessor(parentHash, workers.length);
				workers[home].tell(j, getSelf());
				return;
			}
			if (parentHash == 0){
				rootHash = parent.getHash();
			} else {
				child = rj.getChild();
			}
			int childIndex = parent.getChildIndex(child);
			if (rj.getWinner() == parent.myColour){
				parent.incWDelta(childIndex);
			}
			parent.incSDelta(childIndex);
			parentHash = rj.getParent();
			if (checkForSync(parent)){
				// Broadcast node statistics to all other search ranks
				broadcaster.tell(new SynchroniseMessage(parent.getHash(), parent.wDelta, parent.sDelta) , getSelf());
				parent.zeroDeltas();
			}	
		}
		if (running){
			searchInDuplicateNodes(rootHash);
		}
	}

	/*
	 * Starts a new search at the root using duplicated nodes.
	 * When the search reaches a non-duplicated node, a new search message
	 * is sent to that node's home processor.
	 */
	private void searchInDuplicateNodes(int rootHash){
		int numIterations = 0;
		System.out.println("SEARCHDUP: " + rootHash);
		ArrayDeque<Integer> ancestors = new ArrayDeque<Integer>();
		ArrayDeque<Move> movesMade = new ArrayDeque<Move>();
		ancestors.push(0);
		int currentHash = rootHash;
		SchaefersNode currentNode = duplicatedNodes.get(currentHash);
		currentNode.incT();
		Move child = currentNode.getBestChild();
		int[][] newState = currentNode.getNewState(child);
		ancestors.push(currentHash);
		movesMade.push(child);
		currentHash = table.getHash(newState);
		while (duplicatedNodes.containsKey(currentHash)){
			currentNode = duplicatedNodes.get(currentHash);
			currentNode.incT();
			child = currentNode.getBestChild();
			if (numIterations % 4 == 0){
				if (repeating(child, movesMade)){
					child = currentNode.getSecondBestChild();
				}
			}
			newState[child.getTo().getRow()][child.getTo().getCol()] = newState[child.getFrom().getRow()][child.getFrom().getCol()];
			newState[child.getFrom().getRow()][child.getFrom().getCol()] = 2;
			ancestors.push(currentHash);
			movesMade.push(child);
			currentHash = table.getHash(newState);
			System.out.println("SELECTDUP: " + currentHash);
		}
		int currentPlayer = 0;
		if (currentNode.currentPlayer == 0){
			currentPlayer = 1;
		}
		SearchJob sj = new SearchJob(currentHash, newState, ancestors, movesMade, currentPlayer);
		int home = table.getHomeProcessor(currentHash, workers.length);
		workers[home].tell(sj, getSelf());
	}
	
	private boolean repeating(Move move, ArrayDeque<Move> movesMade){
		int i = 0;
		int count = 0;
		for (Move m : movesMade){
			if (m.equals(move)){
				count++;
			}
			if (count == 4){
				return true;
			}
			if (i++ == 20){
				return false;
			}
		}
		return false;
	}

	private boolean checkForSync(SchaefersNode node){
		double alpha = (1.0 / Math.pow(reductionRate, 2)) - 1;
		for (int i = 0; i < node.getChildren().size(); i++){
			double ndelta = node.getSDelta(i);
			double n = node.getS(i);
			if (ndelta >= alpha * (ndelta + n)){
				// Do not synchronize a node with no statistics
				if (!((ndelta == 0) && (n == 0))){
					return true;
				}
			}
		}
		return false;
	}

	private void actionSelection(SchaefersNode node, SearchJob sj){
		if (node.getT() < threshold){
			performPlayout(node, sj);
		} else {
			System.out.println("SELECT: " + node.getHash());
			Move m = node.getBestChild();
			if (sj.repeating(m)){
				m = node.getSecondBestChild();
			}
			ArrayDeque<Integer> ancestors = sj.getAncestors();
			ArrayDeque<Move> movesMade = sj.getMovesMade();
			node.incT();
			int[][] newState = node.getNewState(m);
			int newHash = table.getHash(newState);
			movesMade.push(m);
			ancestors.push(sj.getHash());
			int currentPlayer = 0;
			if (sj.getCurrentPlayer() == 0){
				currentPlayer = 1;
			}
			SearchJob j = new SearchJob(newHash, newState, ancestors, movesMade, currentPlayer);
			int home = table.getHomeProcessor(newHash, workers.length);
			workers[home].tell(j, getSelf());
		}
		// Broadcast node if it has been visited a sufficient number of times
		if ((node.getT() >= minTForDuplication) && (!duplicatedNodes.keySet().contains(node.getHash()))){
			broadcastNode(node);
		}
	}

	private void performPlayout(SchaefersNode node, SearchJob sj){
		System.out.println("PLAYOUT: " + node.getHash());
		int winner = node.doPlayout();
		node.incT();
		if (sj.isRoot()){
			// We are at the root node. Start another search
			if (running){
				ArrayDeque<Integer> ancestors = new ArrayDeque<Integer>();
				ArrayDeque<Move> movesMade = new ArrayDeque<Move>();
				ancestors.push(0);
				SearchJob j = new SearchJob(rootHash, rootState, ancestors, movesMade, colour);
				processSearchJob(j);
			}
		} else {
			// Propagate job to parent
			ArrayDeque<Integer> ancestors = sj.getAncestors();
			int parent = ancestors.pop();
			ReportJob rj = new ReportJob(parent, winner, ancestors, sj.getMovesMade());
			int home = table.getHomeProcessor(sj.getParent(), workers.length);
			workers[home].tell(rj, getSelf());
		}
	}

	private void updateSharedStats(FinalStatsMessage message){
		int hash = message.getHash();
		ArrayList<Integer> wDelta = message.getwDelta();
		ArrayList<Integer> sDelta = message.getsDelta();
		SchaefersNode n = duplicatedNodes.get(hash);
		n.incorporateDeltas(wDelta, sDelta);
	}

	private void sendStatsResponseMessage(int hash){
		SchaefersNode n = duplicatedNodes.get(hash);
		WorkerStatsResponseMessage m = null;

		// Check if this worker has statistics to share. If not, respond with null
		if (n == null){
			m = new WorkerStatsResponseMessage(hash, null, null);
		} else {
			m = new WorkerStatsResponseMessage(hash, n.wDelta, n.sDelta);
		}

		broadcaster.tell(m, getSelf());
		n.zeroDeltas();
	}

	private void register(Member member) {
		if (member.hasRole("schaefers_master"))
			getContext().actorSelection(member.address() + "/user/schaefers_master").tell(
					"WorkerRegistration", getSelf());
	}
}
