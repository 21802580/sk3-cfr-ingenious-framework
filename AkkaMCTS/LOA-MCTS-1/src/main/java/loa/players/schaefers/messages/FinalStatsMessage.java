package loa.players.schaefers.messages;

import java.io.Serializable;
import java.util.ArrayList;

public class FinalStatsMessage implements Serializable {
	private int hash;
	private ArrayList<Integer> wDelta;
	private ArrayList<Integer> sDelta;
	
	public FinalStatsMessage(int hash, ArrayList<Integer> wDelta, ArrayList<Integer> sDelta){
		this.hash = hash;
		this.wDelta = wDelta;
		this.sDelta = sDelta;
	}

	public int getHash() {
		return hash;
	}

	public ArrayList<Integer> getwDelta() {
		return wDelta;
	}

	public ArrayList<Integer> getsDelta() {
		return sDelta;
	}
}
