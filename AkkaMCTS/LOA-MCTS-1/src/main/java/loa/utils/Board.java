package loa.utils;

import java.io.Serializable;
import java.util.*;

public class Board implements Serializable{
	
	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;
	
	private int boardSize;
	private Deque<Coord> previousFrom;
	private Deque<Coord> previousTo;
	private Deque<Boolean> capture;
	private HashSet<Coord> whitePieces;
	private HashSet<Coord> blackPieces;
	private int currentPlayer;
	private int[][] state;
	
	public static void main(String[] args){
		int boardSize = 8;
		int[][] s = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					s[i][j] = BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					s[i][j] = WHITE;
				} else {
					s[i][j] = EMPTY;	
				}
			}
		}
		double time = System.currentTimeMillis();
		int n = 0;
		Board b = new Board(s, BLACK);
		while(!b.isTerminal()){
			b.makeRandomMove(new ArrayList<Move>());
			System.out.println(b);
			n++;
		}
		System.out.println(n);
		System.out.println(System.currentTimeMillis() - time);
	}
	
	public Board(int[][] state, int currentPlayer){
		this.previousFrom  = new ArrayDeque<Coord>();
		this.previousTo  = new ArrayDeque<Coord>();
		this.capture = new ArrayDeque<Boolean>();
		this.whitePieces = new HashSet<Coord>(16);
		this.blackPieces = new HashSet<Coord>(16);
		this.boardSize = state.length;
		this.currentPlayer = currentPlayer;
		this.state = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize; j++){
				if (state[i][j] == WHITE){
					whitePieces.add(new Coord(i, j));
				} else if (state[i][j] == BLACK){
					blackPieces.add(new Coord(i, j));
				}
				this.state[i][j] = state[i][j];
			}
		}
	}
	
	public String toString(){
		String s = "";
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize - 1; j++){
				switch (state[i][j]) {
					case WHITE:
						s = s + "W ";
						break;
					case BLACK:
						s = s + "B ";
						break;
					case EMPTY:
						s = s + ". ";
						break;
				}
			}
			switch (state[i][boardSize - 1]) {
				case WHITE:
					s = s + "W\n";
					break;
				case BLACK:
					s = s + "B\n";
					break;
				case EMPTY:
					s = s + ".\n";
					break;
			}
		}
		return s;
	}
	
	public int[][] getState(){
		return this.state;
	}
	
	// Returns false if move is illegal, true otherwise.
	public boolean moveLegal(Coord moveFrom, Coord moveTo){
		int rF = moveFrom.getRow();
		int cF = moveFrom.getCol();
		int rT = moveTo.getRow();
		int cT = moveTo.getCol();
		
		// No piece to move or trying to move opponent's piece
		if ((state[rF][cF] == EMPTY) || (state[rF][cF] != currentPlayer)){
			return false;
		}
		
		// Landing on own piece
		if (state[rF][cF] == state[rT][cT]) {
			return false;
		}
		
		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (rF == rT){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[rF][i] != EMPTY){
					numPieces++;
					if (cT > cF){
						if ((i > cF) && (i < cT) && (state[rF][i] != currentPlayer)){
							return false;
						}
					} else if (cF > cT) {
						if ((i > cT) && (i < cF) && (state[rF][i] != currentPlayer)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(cT - cF)){
				return false;
			}
		} else if (cF == cT){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[i][cF] != EMPTY){
					numPieces++;
					if (rT > rF){
						if ((i > rF) && (i < rT) && (state[i][cF] != currentPlayer)){
							return false;
						}
					} else if (cF > cT) {
						if ((i > rT) && (i < rF) && (state[i][cF] != currentPlayer)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(rT - rF)){
				return false;
			}
		} else {
			if ((double)(rF - rT) / (double)(cF - cT) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i < boardSize && j < boardSize; i++, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != currentPlayer)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				for (int i = rF - 1, j = cF - 1; i >= 0 && j >= 0; i--, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != currentPlayer)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rT - rF)){
					return false;
				}
			} else if ((double)(rF - rT) / (double)(cF - cT) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = rF, j = cF; i >= 0 && j < boardSize; i--, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != currentPlayer)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				for (int i = rF + 1, j = cF - 1; i < boardSize && j >= 0; i++, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (cT > cF){
							if ((j > cF) && (j < cT) && (state[i][j] != currentPlayer)){
								return false;
							}
						} else if (cF > cT) {
							if ((j > cT) && (j < cF) && (state[i][j] != currentPlayer)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(rF - rT)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}
	
	public boolean makeMove(Coord moveFrom, Coord moveTo){
		int rF = moveFrom.getRow();
		int cF = moveFrom.getCol();
		int rT = moveTo.getRow();
		int cT = moveTo.getCol();
		if (moveLegal(moveFrom, moveTo)){
			this.previousFrom.push(new Coord(rF, cF));
			this.previousTo.push(new Coord(rT, cT));
			if ((state[rT][cT] != EMPTY) && (state[rT][cT] != currentPlayer)){
				this.capture.push(true);
				if (currentPlayer == WHITE){
					blackPieces.remove(moveTo);
				} else {
					whitePieces.remove(moveTo);
				}
			} else {
				this.capture.push(false);
			}
			state[rT][cT] = currentPlayer;
			state[rF][cF] = EMPTY;
			if (currentPlayer == BLACK){
				blackPieces.remove(moveFrom);
				blackPieces.add(moveTo);
				currentPlayer = WHITE;
			} else {
				whitePieces.remove(moveFrom);
				whitePieces.add(moveTo);
				currentPlayer = BLACK;
			}
			return true;
		}
		return false;
	}

	public boolean undoMove(){
		try{
			Coord from = previousFrom.pop();
			Coord to = previousTo.pop();
			boolean cap = capture.pop();
			state[from.getRow()][from.getCol()] = state[to.getRow()][to.getCol()];
			if (cap){
				state[to.getRow()][to.getCol()] = currentPlayer;
				if (currentPlayer == WHITE){
					whitePieces.add(to);
				} else {
					blackPieces.add(to);
				}
			} else {
				state[to.getRow()][to.getCol()] = EMPTY;
			}
			if (currentPlayer == BLACK){
				whitePieces.add(from);
				whitePieces.remove(to);
				currentPlayer = WHITE;
			} else {
				blackPieces.add(from);
				blackPieces.remove(to);
				currentPlayer = BLACK;
			}
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
		
	}

	public boolean whiteWins(){
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == WHITE){
						win = connected(new Coord(i, j));
						break outer;
					}
					
				}
			}
		}	
		return win;
	}
	
	public boolean blackWins(){
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == BLACK){
						win = connected(new Coord(i, j));
						break outer;
					}
					
				}
			}
		}	
		return win;
	}
	
	public boolean connected(Coord c){
		int colour = state[c.getRow()][c.getCol()];
		LinkedList<Coord> q = new LinkedList<Coord>();
		HashSet<Coord> checked = new HashSet<Coord>();
		q.add(c);
		while(!q.isEmpty()){
			Coord current = q.remove();
			int row = current.getRow();
			int col = current.getCol();
			if (state[row][col] == colour){
				checked.add(current);
				Coord n = new Coord(row - 1, col);
				Coord s = new Coord(row + 1, col);
				Coord e = new Coord(row, col + 1);
				Coord w = new Coord(row, col - 1);
				Coord ne = new Coord(row - 1, col + 1);
				Coord nw = new Coord(row - 1, col - 1);
				Coord se = new Coord(row + 1, col + 1);
				Coord sw = new Coord(row + 1, col - 1);
				if ((row > 0) && !checked.contains(n)){
					q.add(n);
				}
				if ((row < state.length - 1) && !checked.contains(s)){
					q.add(s);	
				}
				if ((col < state.length - 1) && !checked.contains(e)){
					q.add(e);
				}
				if ((col > 0) && !checked.contains(w)){
					q.add(w);
				}
				if ((row > 0) && (col < state.length - 1) && !checked.contains(ne)){
					q.add(ne);
				}
				if ((row > 0) && (col > 0) && !checked.contains(nw)){
					q.add(nw);
				}
				if ((col < state.length - 1) && (row < state.length - 1) && !checked.contains(se)){
					q.add(se);
				}
				if ((col > 0) && (row < state.length - 1) && !checked.contains(sw)){
					q.add(sw);
				}
			}
		}
		if (colour == WHITE){
			return checked.size() == whitePieces.size();
		} else {
			return checked.size() == blackPieces.size();
		}
	}

	public boolean isTerminal(){
		return whiteWins() || blackWins();
	}

	public int getBoardSize(){
		return this.boardSize;
	}
	
	public int getCurrentPlayer(){
		return this.currentPlayer;
	}

	// Returns ((-1, -1) -> (-1, -1) if fully expanded)
	public Move makeRandomMove(ArrayList<Move> expanded){
		ArrayList<Coord> loc = new ArrayList<Coord>();
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize; j++){
				if (state[i][j] == currentPlayer){
					loc.add(new Coord(i, j));
				}
			}
		}
		
		Random rand = new Random();
		int numElements = loc.size();
		ArrayList<Move> checked = new ArrayList<Move>();
		for (int k = 0; k < numElements; k++){
			Coord from = loc.remove(rand.nextInt(loc.size()));
			for (int i = 0; i < boardSize; i++){
				for (int j = 0; j < boardSize; j++){
					Move m = new Move(from, new Coord(i, j));
					if (moveLegal(from, new Coord(i, j))){
						checked.add(m);
						if(!expanded.contains(m)){
							makeMove(from, new Coord(i, j));
							return m;
						}
					}
				}
			}	
		}
		if (checked.size() == expanded.size()){
			return new Move(new Coord(-1, -1), new Coord(-1, -1));
		}
		return null;
	}
}
