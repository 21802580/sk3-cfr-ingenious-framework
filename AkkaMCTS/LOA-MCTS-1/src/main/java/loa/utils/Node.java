package loa.utils;

import java.io.Serializable;
import java.util.ArrayList;

public class Node implements Serializable{
	private Node parent;
	private ArrayList<Node> children;
	private Move moveMade;
	private ArrayList<Move> expandedMoves;
	private boolean fullyExpanded;
	private int visits;
	private double reward;
	
	public Node(Node parent, Move moveMade){
		this.parent	= parent;
		this.moveMade = moveMade;
		this.visits = 0;
		this.reward = 0;
		this.children = new ArrayList<Node>();
		this.expandedMoves = new ArrayList<Move>();
		this.fullyExpanded = false;
	}
	
	public void addChild(Node child){
		children.add(child);
	}
	
	public double getUCB(double c){
		return ((double)reward / (double)(visits)) + (c * Math.sqrt((Math.log((double)parent.getVisits())) / ((double)(visits))));
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public ArrayList<Node> getChildren() {
		return children;
	}

	public void setChildren(ArrayList<Node> children) {
		this.children = children;
	}

	public Move getMoveMade() {
		return moveMade;
	}

	public void setMoveMade(Move moveMade) {
		this.moveMade = moveMade;
	}

	public int getVisits() {
		return visits;
	}

	public void setVisits(int visits) {
		this.visits = visits;
	}

	public double getReward() {
		return reward;
	}

	public void setReward(double reward) {
		this.reward = reward;
	}
	
	public boolean isFullyExpanded(){
		return this.fullyExpanded;
	}
	
	public Move getNextMove(Board b){
		if (this.fullyExpanded){
			return new Move(new Coord(-1, -1), new Coord(-1, -1));
		}
		Board tmp = new Board(b.getState(), b.getCurrentPlayer());
		Move m = tmp.makeRandomMove(expandedMoves);
		if (m.equals(new Move(new Coord(-1, -1), new Coord(-1, -1)))){
			this.fullyExpanded = true;
		} else {
			expandedMoves.add(m);
		}
		return m;
	}
}
