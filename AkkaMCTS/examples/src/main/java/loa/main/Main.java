package loa.main;

import java.net.InetAddress;
import java.net.UnknownHostException;

import loa.players.basic.BasicMCTS;
import loa.players.rootPar.RootParMCTS;
import loa.players.yoshizoe.TDSPlayer;
import loa.utils.Board;
import loa.utils.Move;

public class Main {
	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;
	private static int[][] state;
	
	public static void main(String[] args){
		int boardSize = 8;
		String seedhost = args[0];
		String seedport = args[1];
		int numWorkers = Integer.parseInt(args[2]);
		state = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					state[i][j] = BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					state[i][j] = WHITE;
				} else {
					state[i][j] = EMPTY;	
				}
			}
		}
		
		Board b = new Board(state, BLACK);
		BasicMCTS basic = new BasicMCTS(2000, BLACK, Math.sqrt(2.0));
		//BasicMCTS yosh = new BasicMCTS(2000, WHITE, Math.sqrt(2.0));
		TDSPlayer yosh = new TDSPlayer(2000, WHITE, Math.sqrt(2.0), numWorkers, false, 4, seedhost, seedport);
		//RootParMCTS yosh = new RootParMCTS(2000, WHITE, Math.sqrt(2.0), 2);
		Move m = null;
		while (!b.isTerminal()){
			m = basic.getMove(b);
			System.out.println(b.getCurrentPlayer());
			System.out.println(m.toString());
			b.makeMove(m.getFrom(), m.getTo());
			System.out.println(b.toString());	
			m = yosh.getMove(b);
			System.out.println(b.getCurrentPlayer());
			System.out.println(m.toString());
			b.makeMove(m.getFrom(), m.getTo());
			System.out.println(b.toString());
		}
	}
}
