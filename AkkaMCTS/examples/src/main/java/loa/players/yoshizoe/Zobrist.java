package loa.players.yoshizoe;

import java.util.Random;

import loa.utils.Move;

public class Zobrist {
	private int[][][] hashes;
	private TDSNode[] entries;
	
	public static final int WHITE = 0;
	public static final int BLACK = 1;
	public static final int EMPTY = 2;
	
	public Zobrist() {
		hashes = new int[8][8][2];
		entries = new TDSNode[1000000];
		Random r = new Random(12345);
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 2; k++){
					hashes[i][j][k] = Math.abs(r.nextInt());
				}
			}
		}
	}
	
	public int getHash(int[][] board){
		int h = 0;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				if (board[i][j] != EMPTY){
					h = h ^ hashes[i][j][board[i][j]];
				}
			}
		}
		return h;
	}
	
	public boolean contains(int hash){
		int index = hash % entries.length;
		if ((entries[index] != null) && (hash == entries[index].getHash())){
			return true;
		}
		return false;
	}

	public void put(TDSNode node){
		int hash = node.getHash();
		int index = hash % entries.length;
		entries[index] = node;
	}
	
	public TDSNode getNode(int hash){
		return entries[hash % entries.length];
	}
	
	public int getHomeProcessor(int hash, int numWorkers){
		return hash % numWorkers;
	}
}
