package loa.players.yoshizoe;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import cluster.TransformationMessages;
import loa.utils.Move;

public class TDSWorker extends UntypedActor{
	private ActorRef[] workers;
	private Zobrist table;
	private SearchJob firstJob;
	private ActorRef master;
	Cluster cluster = Cluster.get(getContext().system());
	
	public TDSWorker(){
		firstJob = null;
		master = null;
		table = new Zobrist();
	}
	
	//subscribe to cluster changes, MemberUp
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberUp.class);
	}

	//re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		if (message instanceof ActorRef[]){
			System.out.println("RECEIVING WORKER ARRAY");
			System.out.println();
			workers = (ActorRef[]) message;
		} else if (message instanceof SearchJob){
			System.out.println("RECEIVED SEARCH JOB");
			System.out.println();
			SearchJob sj = (SearchJob) message;
			if (firstJob == null){
				firstJob = sj;
				if (sj.getParentHash() == 0){
					master = getSender();
					final long start = System.currentTimeMillis();
					new Thread(new Runnable() {
						public void run() {
							while (true){
								if ((System.currentTimeMillis()) - start > firstJob.getTimeout()){
									master.tell(table.getNode(firstJob.getHash()), getSelf());
									break;
								}
							}
						}
					}).start();
				}
			}
			int hash = sj.getHash();
			TDSNode node = null;
			if (table.contains(hash)){
				// The node has already been encountered
				node = table.getNode(hash);
				actionSelection(node, sj);
			} else {
				// The node must be added to the tree (new leaf node)
				node = new TDSNode(sj.getState(), sj.getHash(), sj.getParentHash(), sj.getUcbConstant(), sj.getColour(), sj.getLastMoveMade());
				table.put(node);
				expandAndPlayout(node, sj);
			}
		} else if (message instanceof ReportJob){
			ReportJob rj = (ReportJob)message;
			Move child = rj.getChild();
			int hash = rj.getHash();
			System.out.println("RJ HASH: " + hash);
			int winner = rj.getWinner();
			TDSNode node = table.getNode(hash);
			int childIndex = node.getChildIndex(child);
			if (winner == rj.getMyColour()){
				node.incW(childIndex);
			}
			node.incS(childIndex);
			if (node.getParentHash() == 0){
				// We are at the root node. Start another search
				workers[hash].tell(firstJob, getSelf());
			} else {
				// Propogate job to parent
				ReportJob j = new ReportJob(node.getParentHash(), winner, rj.getMyColour(), node.getLastMoveMade());
				workers[node.getParentHash()].tell(j, getSelf());
			}
//		} else if (message instanceof CurrentClusterState) {
//			CurrentClusterState state = (CurrentClusterState) message;
//			for (Member member : state.getMembers()) {
//				if (member.status().equals(MemberStatus.up())) {
//					register(member);
//				}
//			}
		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			register(mUp.member());
		} else {
			unhandled(message);
		}
	}
	
	private void actionSelection(TDSNode node, SearchJob sj){
		if (node.getT() < sj.getThreshold()){
			expandAndPlayout(node, sj);
		} else {
			Move m = node.getBestChild();
			node.incT();
			int[][] newState = node.getNewState(m);
			int newHash = table.getHash(newState);
			SearchJob j = new SearchJob(sj.getTimeout(), sj.getColour(), sj.getUcbConstant(), newHash, sj.getHash(), newState, sj.getThreshold(), m);
			int home = table.getHomeProcessor(newHash, workers.length);
			workers[home].tell(j, getSelf());
		}
	}
	
	private void expandAndPlayout(TDSNode node, SearchJob sj){
		int winner = node.doPlayout();
		node.incT();
		ReportJob rj = new ReportJob(node.getParentHash(), winner, sj.getColour(), node.getLastMoveMade());
		int home = table.getHomeProcessor(node.getParentHash(), workers.length);
		workers[home].tell(rj, getSelf());
	}
	
	private void register(Member member) {
		if (member.hasRole("master"))
			getContext().actorSelection(member.address() + "/user/master").tell(
					TransformationMessages.BACKEND_REGISTRATION, getSelf());
	}
}
