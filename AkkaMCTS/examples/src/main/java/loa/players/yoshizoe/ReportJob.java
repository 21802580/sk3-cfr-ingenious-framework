package loa.players.yoshizoe;

import java.io.Serializable;

import loa.utils.Move;

public class ReportJob implements Serializable{
	private int hash;
	private int winner;
	private int myColour;
	private Move child;
	
	public ReportJob(int hash, int winner, int myColour, Move child){
		this.hash = hash;
		this.winner = winner;
		this.myColour = myColour;
		this.child = child;
	}

	public int getHash() {
		return hash;
	}

	public int getWinner() {
		return winner;
	}
	
	public int getMyColour(){
		return this.myColour;
	}

	public Move getChild() {
		return child;
	}
}
