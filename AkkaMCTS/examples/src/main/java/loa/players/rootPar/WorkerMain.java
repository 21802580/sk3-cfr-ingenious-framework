package loa.players.rootPar;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberUp;
import cluster.TransformationBackend;
import cluster.TransformationMessages;
import cluster.TransformationMessages.TransformationJob;
import cluster.TransformationMessages.TransformationResult;

public class WorkerMain {

	public static void main(String[] args) {
		// Override the configuration of the port when specified as program argument
		final String port = args.length > 0 ? args[0] : "0";
		final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [worker]")).
				withFallback(ConfigFactory.load());

		ActorSystem system = ActorSystem.create("RootParSystem", config);

		system.actorOf(Props.create(Worker.class), "worker");

	}
}
