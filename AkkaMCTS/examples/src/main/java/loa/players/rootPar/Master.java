package loa.players.rootPar;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.routing.GetRoutees;
import akka.routing.RoundRobinPool;
import cluster.TransformationMessages;
import cluster.TransformationMessages.JobFailed;
import cluster.TransformationMessages.TransformationJob;
import loa.players.basic.BasicMCTS;
import loa.utils.Board;
import loa.utils.Move;
import loa.utils.Node;

public class Master extends UntypedActor{
	private int timeout;
	private int colour;
	private double ucbConstant;
	private Board state;
	private int numWorkers;
	private int numReceived;
	private ActorRef returnActor;
	private HashMap<Move, Double> scores;
	List<ActorRef> workers = new ArrayList<ActorRef>();
	
	public Master(int timeout, int colour, double ucbConstant, int numWorkers, Board state){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.state = state;
		this.scores = new HashMap<Move, Double>();
		this.numReceived = 0;
		this.numWorkers = numWorkers;
	}
	
	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof Node){
			numReceived++;
			Node root = (Node)message;
			ArrayList<Node> children = root.getChildren();
			for (Node child : children){
				Move m = child.getMoveMade();
				double score = child.getReward();
				if (scores.containsKey(m)){
					scores.put(m, scores.get(m) + score);
				} else {
					scores.put(m, score);
				}
			}
			if (numReceived == numWorkers){
				double bestScore = 0;
				Move bestMove = null;
				for (Move m : scores.keySet()){
					double score = scores.get(m);
					if (score > bestScore){
						bestScore = score;
						bestMove = m;
					}
				}
				returnActor.tell(bestMove, getSelf());
			}
		} else if (message instanceof SearchStart){
			returnActor = getSender();
		} else if (message.equals(TransformationMessages.BACKEND_REGISTRATION)) {
			getContext().watch(getSender());
			workers.add(getSender());
			if (workers.size() == numWorkers){
				for (ActorRef a : workers){
					a.tell(new SearchJob(new BasicMCTS(timeout, colour, ucbConstant), state), getSelf());
				}
			}
		} else if (message instanceof Terminated) {
			Terminated terminated = (Terminated) message;
			workers.remove(terminated.getActor());
		}  else {
			unhandled(message);
		}
	}
}
