package loa.players.rootPar;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.OnComplete;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.utils.Board;
import loa.utils.Move;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

public class RootParMCTS {
	private int timeout;
	private int colour;
	private double ucbConstant;
	private int numWorkers;
	private Move m;

	public RootParMCTS(int timeout, int colour, double ucbConstant, int numWorkers){
		this.timeout = timeout;
		this.colour = colour;
		this.ucbConstant = ucbConstant;
		this.numWorkers = numWorkers;
	}

	public Move getMove(Board state){
		final String port = "0";
		final Config config = ConfigFactory.parseString("akka.remote.netty.tcp.port=" + port).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [master]")).
				withFallback(ConfigFactory.load());

		final ActorSystem system = ActorSystem.create("RootParSystem", config);
		ActorRef master = system.actorOf(Props.create(Master.class, timeout, colour, ucbConstant, numWorkers, state), "master");
		Timeout timeout = new Timeout(Duration.create(21474835, "seconds"));
		Future<Object> fut = Patterns.ask(master, new SearchStart(), timeout);
		try {
			m = (Move) Await.result(fut, timeout.duration());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return m;
	}
}
