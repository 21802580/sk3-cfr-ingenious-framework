package stringarr.akka.main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import stringarr.akka.actors.Master;

public class Main {
	public static void main(String[] args){
		int numWorkers = Integer.parseInt(args[0]);
		int numElements = Integer.parseInt(args[1]);
		String str = args[2];
		
		// Create an Akka system
        ActorSystem system = ActorSystem.create("StringSystem");

        // create the master
        final ActorRef master = system.actorOf(Props.create(Master.class, numWorkers, numElements), "master");
        
        // start the calculation
        master.tell(str, master);
	}
}
