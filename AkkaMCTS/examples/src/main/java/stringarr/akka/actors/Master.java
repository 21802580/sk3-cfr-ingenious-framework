package stringarr.akka.actors;

import stringarr.akka.messages.Result;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;

public class Master extends UntypedActor {
	private int numElements;
	private int numWorkers;
	private final ActorRef workerRouter;
	private int numResults;
	private long start;
	
	public Master(int numWorkers, int numElements){
		this.numElements = numElements;
		this.numWorkers = numWorkers;
		this.workerRouter = this.getContext().actorOf(new RoundRobinPool(numWorkers).props(Props.create(Worker.class)), "router");
		this.start = System.currentTimeMillis();
		this.numResults = 0;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof String) {
			String[] toSend = new String[numElements];
			for (int i = 0; i < numElements; i++){
				toSend[i] = (String)message;
			}
			for (int i = 0; i < numWorkers; i++){
                workerRouter.tell(toSend, getSelf());
            }
		} else if (message instanceof Result){
			numResults++;
			System.out.println("Ack received from worker " + getSender());
			if (numResults == numWorkers){
                System.out.println("Time : " + (System.currentTimeMillis() - start));
                getContext().stop(getSelf());
			}
		} else {
			unhandled(message);
		}
	}
}
