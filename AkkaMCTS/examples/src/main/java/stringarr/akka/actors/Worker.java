package stringarr.akka.actors;

import akka.actor.UntypedActor;
import stringarr.akka.messages.Result;

public class Worker extends UntypedActor{

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof String[]){
			System.out.println(((String[])message)[1] + " received by worker " + getSelf());
			getSender().tell(new Result(), getSelf());
		} else {
			unhandled(message);
		}
	}
}
