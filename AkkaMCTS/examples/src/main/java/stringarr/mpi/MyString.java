package stringarr.mpi;

import mpi.*;

public class MyString extends Struct {

	// This section defines the offsets of the fields.
	private int length = addInt(), value = addChar(100);

	// This method tells the super class how to create a data object.
	@Override 
	protected Data newData() {
		return new Data();
	}

	// A Data object is a reference to a buffer section.
	// It permits read (puts) and write (gets) on the buffer
	public class Data extends Struct.Data {
		public String getValue() {
			int len = getInt(length); // Gets the value length.
			StringBuilder sb = new StringBuilder(len);

			// Deserialization
			for(int i = 0; i < len; i++)
				sb.append(getChar(value, i));

			return sb.toString();
		}

		public void putValue(String d) {
			int len = d.length();

			putInt(length, len); // Puts the value length on the buffer.

			// Serialization
			for(int i = 0; i < len; i++)
				putChar(value, i, d.charAt(i));
		}
	}
}
