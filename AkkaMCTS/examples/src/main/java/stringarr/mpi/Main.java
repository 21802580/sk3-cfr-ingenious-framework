package stringarr.mpi;

import java.nio.ByteBuffer;

import mpi.*;

public class Main {
	
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);
		
		int myrank = MPI.COMM_WORLD.getRank();
		int size = MPI.COMM_WORLD.getSize();
		int numElements = Integer.parseInt(args[0]);
		String str = args[1];
		
		MyString s = new MyString();
		
		if (myrank == 0){
			long time = System.currentTimeMillis();
			ByteBuffer b = MPI.newByteBuffer(s.getExtent() * numElements);
			for (int k = 0; k < numElements; k++){
				MyString.Data d = s.getData(b, k);
				d.putValue(str);
			}
			for (int i = 1; i < size; i++){
				MPI.COMM_WORLD.send(b, numElements, s.getType(), i, 0);	
			}
			for (int i = 1; i < size; i++){
				int[] ack = new int[1];
				MPI.COMM_WORLD.recv(ack, 1, MPI.INT, MPI.ANY_SOURCE, 1);
				System.out.println("Ack " + i + " received.");
			}
			System.out.println("Time : " + (System.currentTimeMillis() - time));
		} else {
	        ByteBuffer b = MPI.newByteBuffer(s.getExtent() * numElements);
		    MPI.COMM_WORLD.recv(b, numElements, s.getType(), 0, 0);
	        int[] ack = new int[] {1};
	        MPI.COMM_WORLD.send(ack, 1, MPI.INT, 0, 1);
		}
		
		MPI.Finalize();
	}
}
