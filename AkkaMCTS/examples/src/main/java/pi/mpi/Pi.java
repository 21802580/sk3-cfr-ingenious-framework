package pi.mpi;

import mpi.*;

public class Pi {
	
	public static void main(String[] args) throws MPIException {
		MPI.Init(args);
		
		int myrank = MPI.COMM_WORLD.getRank();
		int size = MPI.COMM_WORLD.getSize();
		int numElements = Integer.parseInt(args[0]);
		int numMessages = Integer.parseInt(args[1]);
		double pi = 0;
		
		if (myrank == 0){
			long time = System.currentTimeMillis();
			for (int i = 1; i < size; i++){
				for (int j = 0; j < numMessages/(size - 1); j++){
					int[] message = new int[] {j, numElements};
					MPI.COMM_WORLD.send(message, 2, MPI.INT, i, 0);	
				}
			}
			for (int i = 1; i < size; i++){
				for (int j = 0; j < numMessages/(size - 1); j++){
					double[] result = new double[1];
					MPI.COMM_WORLD.recv(result, 1, MPI.DOUBLE, i, 1);
					pi += result[0];	
				}
			}
			System.out.println("Pi approximation: " + (pi/(size-1) + "\nCalculation time: " + (System.currentTimeMillis() - time)));
		} else {
			int[][] messages = new int[numMessages/(size-1)][2];
			for (int i = 0; i < numMessages/(size-1); i++){
				int[] message = new int[2];
				MPI.COMM_WORLD.recv(message, 2, MPI.INT, 0, 0);
				messages[i] = message;
			}
			for (int i = 0; i < numMessages/(size-1); i++){
				int start = messages[i][0];
				int numEle = messages[i][1];
				double[] result = new double[] {calculatePiFor(start, numEle)};
				MPI.COMM_WORLD.send(result, 1, MPI.DOUBLE, 0, 1);	
			}
		}
		
		MPI.Finalize();
	}
	
	private static double calculatePiFor(int start, int nrOfElements){
        double acc = 0.0;
        for (int i = start * nrOfElements; i <= ((start + 1) * nrOfElements - 1); i++){
            acc += 4.0 * (1 - (i % 2) * 2) / (2 * i + 1);
        }
        return acc;
    }
}
