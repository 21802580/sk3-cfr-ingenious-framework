package pi.akka.actors;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import pi.akka.messages.Calculate;
import pi.akka.messages.PiApproximation;
import pi.akka.messages.Result;
import pi.akka.messages.Work;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public class Master extends UntypedActor{
    private final int numMessages;
    private final int numElements;
    private final ActorRef workerRouter;
    private double pi;
    private int numResults;
    private final long start;

    public Master(final int numWorkers, int numMessages, int numElements){
        this.numMessages = numMessages;
        this.numElements = numElements;
        this.workerRouter = this.getContext().actorOf(new RoundRobinPool(numWorkers).props(Props.create(Worker.class)), "router");
        this.start = System.currentTimeMillis();
    }

    public void onReceive(Object message){
        if (message instanceof Calculate){
            for (int start = 0; start < numMessages; start++){
                workerRouter.tell(new Work(start, numElements), getSelf());
            }
        } else if (message instanceof Result){
            Result result = (Result) message;
            pi += result.getValue();
            numResults += 1;
            if (numResults == numMessages) {
                Duration duration = Duration.create(System.currentTimeMillis() - start, TimeUnit.MILLISECONDS);
                PiApproximation approximation = new PiApproximation(pi, duration);
                System.out.println(String.format("Pi approximation: %s\nCalculation time: %s", approximation.getPi(), approximation.getDuration()));
                getContext().stop(getSelf());
            }
        } else {
            unhandled(message);
        }
    }
}
