package pi.akka.main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.japi.Creator;
import pi.akka.actors.Master;
import pi.akka.messages.Calculate;

import java.util.concurrent.TimeUnit;

public class Pi {

    public static void main(String[] args){
        Pi pi = new Pi();
        pi.calculate(4, 50000, 50000);
    }

    public void calculate(final int nrOfWorkers, final int nrOfElements, final int nrOfMessages){
        // Create an Akka system
        ActorSystem system = ActorSystem.create("PiSystem");

        // create the master
        final ActorRef master = system.actorOf(Props.create(Master.class, nrOfWorkers, nrOfMessages, nrOfElements), "master");
        // start the calculation
        master.tell(new Calculate(), master);
    }
}
