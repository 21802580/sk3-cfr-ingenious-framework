package pi.akka.messages;
import scala.concurrent.duration.Duration;

public class PiApproximation {
    private final double pi;
    private final Duration duration;

    public PiApproximation(double pi, Duration duration){
        this.duration = duration;
        this.pi = pi;
    }

    public double getPi(){
        return this.pi;
    }

    public Duration getDuration() {
        return this.duration;
    }
}
