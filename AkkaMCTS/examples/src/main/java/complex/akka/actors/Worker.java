package complex.akka.actors;

import java.util.ArrayList;
import akka.actor.UntypedActor;
import complex.akka.messages.Complex;
import complex.akka.messages.Result;

public class Worker extends UntypedActor {

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof ArrayList){
			ArrayList<Complex> al = (ArrayList<Complex>)message;
			Complex c = al.get(0);
			double r = c.getReal();
			double i = c.getImag();
			System.out.println("(" + r + ", " + i + ") received by worker " + getSelf());
			getSender().tell(new Result(), getSelf());
		} else {
			unhandled(message);
		}
	}
}
