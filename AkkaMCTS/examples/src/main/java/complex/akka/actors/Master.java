package complex.akka.actors;

import java.util.ArrayList;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.routing.RoundRobinPool;
import complex.akka.messages.Complex;
import complex.akka.messages.Result;
import complex.akka.messages.StartJob;

public class Master extends UntypedActor{
	private int numElements;
	private int numWorkers;
	private final ActorRef workerRouter;
	private int numResults;
	private long start;
	
	public Master(int numWorkers, int numElements){
		this.numElements = numElements;
		this.numWorkers = numWorkers;
		this.workerRouter = this.getContext().actorOf(new RoundRobinPool(numWorkers).props(Props.create(Worker.class)), "router");
		this.start = System.currentTimeMillis();
		this.numResults = 0;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		if (message instanceof StartJob) {
			ArrayList<Complex> toSend = new ArrayList<Complex>();
			double r = 0, im = 0;
			for (int i = 0; i < numElements; i++){
				r = (Math.random() * 200) - 100;
				im = (Math.random() * 200) - 100;
				toSend.add(new Complex(r, im));
			}
			System.out.println("Sending (" + toSend.get(0).getReal() + ", " + toSend.get(0).getImag() + ") to workers.");
			for (int i = 0; i < numWorkers; i++){
                workerRouter.tell(toSend, getSelf());
            }
		} else if (message instanceof Result){
			numResults++;
			System.out.println("Ack received from worker " + getSender());
			if (numResults == numWorkers){
                System.out.println("Time : " + (System.currentTimeMillis() - start));
                getContext().stop(getSelf());
			}
		} else {
			unhandled(message);
		}
	}
}
