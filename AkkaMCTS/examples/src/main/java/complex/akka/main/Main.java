package complex.akka.main;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import complex.akka.actors.Master;
import complex.akka.messages.StartJob;

public class Main {
	public static void main(String[] args){
		int numWorkers = Integer.parseInt(args[0]);
		int numElements = Integer.parseInt(args[1]);
		
		
		// Create an Akka system
        ActorSystem system = ActorSystem.create("ComplexSystem");

        // create the master
        final ActorRef master = system.actorOf(Props.create(Master.class, numWorkers, numElements), "master");
        
        // start the calculation
        master.tell(new StartJob(), master);
	}
}
