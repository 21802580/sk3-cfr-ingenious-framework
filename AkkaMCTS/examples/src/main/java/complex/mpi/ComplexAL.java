package complex.mpi;
					
import java.util.ArrayList;
import mpi.*;

public class ComplexAL extends Struct {
	private final int length, complexList;
	
	public ComplexAL(int size) throws MPIException{
		length = addInt();
		complexList = addDouble(size);
	}

	@Override
	protected Data newData() {
		return new Data();
	}

	public class Data extends Struct.Data {
		public ArrayList<Complex> getList() throws MPIException {
			int len = getInt(length);
			ArrayList<Complex> al = new ArrayList<Complex>();
			
			for (int i = 0; i < len; i += 2){
				double real = getDouble(complexList, i);
				double imag = getDouble(complexList, i + 1);
				al.add(new Complex(real, imag));
			}
			
			return al;
		}
		
		public void putList(ArrayList<Complex> al) throws MPIException {
			putInt(length, al.size());

			for (int i = 0; i < al.size(); i += 2){
				Complex c = al.get(i);

				putDouble(complexList, i, c.getReal());
				putDouble(complexList, i + 1, c.getImag());
			}
		}
	}
}
