package complex.mpi;

import java.nio.ByteBuffer;
import java.util.ArrayList;

import mpi.MPI;
import mpi.MPIException;

public class Main {
	public static void main(String[] args) throws MPIException{
		MPI.Init(args);
		
		int myrank = MPI.COMM_WORLD.getRank();
		int size = MPI.COMM_WORLD.getSize();
		int numElements = Integer.parseInt(args[0]);
		
		ComplexAL c = new ComplexAL(numElements);
		
		if (myrank == 0){
			ArrayList<Complex> al = new ArrayList<Complex>();
			for (int i = 0; i < numElements; i++){
				double real = (Math.random() * 200) - 100;
				double imag = (Math.random() * 200) - 100;
				al.add(new Complex(real, imag));
			}
			
			//System.out.println("First complex before sending : (" + al.get(0).getReal() + "), (" + al.get(0).getImag() + ")");
			
			long time = System.currentTimeMillis();
			ByteBuffer b = MPI.newByteBuffer(c.getExtent() * numElements);
			ComplexAL.Data d = c.getData(b);
			d.putList(al);
			for (int i = 1; i < size; i++){
				MPI.COMM_WORLD.send(b, numElements, c.getType(), i, 0);	
			}
			for (int i = 1; i < size; i++){
				int[] ack = new int[1];
				MPI.COMM_WORLD.recv(ack, 1, MPI.INT, MPI.ANY_SOURCE, 1);
				System.out.println("Ack " + i + " received.");
			}
			System.out.println("Time : " + (System.currentTimeMillis() - time));
		} else {
	        ByteBuffer b = MPI.newByteBuffer(c.getExtent() * numElements);
		    MPI.COMM_WORLD.recv(b, numElements, c.getType(), 0, 0);
		    //ComplexAL.Data d = c.getData(b);
		    //ArrayList<Complex> al = d.getList();
		    //System.out.println("First complex after sending : (" + al.get(0).getReal() + "), (" + al.get(0).getImag() + ")");
		    int[] ack = new int[] {1};
	        MPI.COMM_WORLD.send(ack, 1, MPI.INT, 0, 1);
		}
		
		MPI.Finalize();
	}
}
