import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class LOADriver {
	static LOAPlayer player;

	public static void main(String[] args){
		//Uncomment the line below and instantiate your player
		player = new RandomPlayer();
		int port = Integer.parseInt(args[0]);
		ObjectOutputStream out = null;
		ObjectInputStream in = null;
		Socket socket = null;
		try {
			socket = new Socket("localhost", port);
			out = new ObjectOutputStream(socket.getOutputStream());
			in = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		boolean running = true;
		while (running){
			try {
				String[] message = ((String)in.readObject()).split(" ");
				switch (message[0]){
				case "playmove":
					player.playMove(message[1]);
					break;
				case "genmove":
					long timeout = Long.parseLong(message[1]);
					String move = player.genMove(timeout);
					out.writeObject(move);
					break;
				case "gameover":
					int terminationCode = Integer.parseInt(message[1]);
					player.gameOver(terminationCode);
					running = false;
					socket.close();
					break;
				}
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
		}
	}
}
