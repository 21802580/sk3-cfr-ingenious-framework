
public interface LOAPlayer {
	
	/*
	 * This is called when your engine is re- quested to generate a move. 
	 * The time limit (in milliseconds) for the move is contained in the “timeout”
	 * argument.
	 *  
	 * When called, this method should generate a move, and apply it to the
	 * game state in your engine. It must also return a string of the form “vwxy”
	 * where [v, w] is a tuple representing the row and column of the piece to be 
	 * moved and [x, y] is a tuple representing the row and column (both numerical and
	 * starting at zero) where the piece is to be moved to. The row and column counts 
	 * start at the top left corner of the board. Thus the top left corresponds to [0, 0]
	 * and the bottom left corresponds to [7, 0].
	 *
	 * As an example, if the piece to be moved is located at [0, 1], and must be moved
	 * to [0, 7], this method would return the string “0107”.
	 *
	 * To pass, this method must return “pass”, and to resign, this method must return
	 * “resign”.
	 * 
	 * Note that your engine will not receive playMove calls for its own moves, so you must
	 * apply the move returned here to your game state.
	 */
	public String genMove(long timeout);
	
	/*
	 * This method is called to apply a move to the current game state — usually when
	 * another engine has made a move, but also possibly to set up a specific position. 
	 * The move is contained in a String parameter and is in the form described above (and 
	 * may thus also be either “pass” or “resign”).
	 */
	public void playMove(String move);
	
	/*
	 * This method is called when the game has finished for any reason (including 
	 * resignation by your engine). The cause of the game coming to an end is stored
	 * in the terminationCode argument. This is provided for convenience in your engine (e.g.
	 * terminating threads, closing files, etc.), but simply providing an empty implementation
	 * of this method is fine. Current termination codes are:
	 * 
	 * -2 : Your player has timed out
	 * -1 : Your player has made an illegal move
	 * 0 : The opposing player has won the match
	 * 1 : Your player has won the match
	 * 2 : The opposing player has made an illegal move
	 * 3 : The opposing player has timed out
	 */
	public void gameOver(int terminationCode);
}
