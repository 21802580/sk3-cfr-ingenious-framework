import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.ProcessBuilder.Redirect;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashSet;
import java.util.LinkedList;

public class LOATester {

	// Paths to the engine jars
	private static String player1Path;
	private static String player2Path;

	// Engine streams
	private static ObjectInputStream is1;
	private static ObjectInputStream is2;
	private static ObjectOutputStream os1;
	private static ObjectOutputStream os2;

	// Game state
	private static int[][] state;
	private static final int boardSize = 8;
	private static int numWhite;
	private static int numBlack;

	// Colour constants
	private static final int WHITE = 0;
	private static final int BLACK = 1;
	private static final int EMPTY = 2;

	private static int timeout1;	// Time limit for an engine to make a move
	private static int timeout2;	// Time limit for an engine to make a move
	
	private static boolean matchLogging;

	/*
	 * Launch both engines and play a match to completion
	 */
	public static void main(String[] args) throws IOException {
		player1Path = args[0];
		timeout1 = Integer.parseInt(args[1]);
		player2Path = args[2];
		timeout2 = Integer.parseInt(args[3]);
		if (args.length > 4){
			matchLogging = true;
		} else {
			matchLogging = false;
		}
		
		// Get input and output streams from the relevant sockets
		Socket player1Socket = launchEngine(player1Path, timeout1 + 1000000, 1);
		Socket player2Socket = launchEngine(player2Path, timeout2 + 1000000, 2);
		try {
			is1 = new ObjectInputStream(player1Socket.getInputStream());
			is2 = new ObjectInputStream(player2Socket.getInputStream());
			os1 = new ObjectOutputStream(player1Socket.getOutputStream());
			os2 = new ObjectOutputStream(player2Socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Initialize the board
		state = new int[boardSize][boardSize];
		numWhite = 12;
		numBlack = 12;
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					state[i][j] = BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					state[i][j] = WHITE;
				} else {
					state[i][j] = EMPTY;	
				}
			}
		}
		playMatch();
		player1Socket.close();
		player2Socket.close();
		os1.close();
		os2.close();
		is1.close();
		is2.close();
	}

	/*
	 * Launch the engine stored at the given path and return a socket for communication
	 * with that engine
	 */
	private static Socket launchEngine(String path, int timeout, int playerNumber){
		//Open ServerSocket
		ServerSocket serverSocket = null;
		try {
			serverSocket = new ServerSocket(0);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Get ServerSocket port to pass to engine
		int port = serverSocket.getLocalPort();

		// Redirect engine's error and output streams to a separate log
		ProcessBuilder pb = new ProcessBuilder("java", "-jar", path, port + "");
		String logName = playerNumber + "-" + path.substring(path.lastIndexOf('/') + 1, path.lastIndexOf('.'));
		File log = new File(logName + ".log");
		pb.redirectErrorStream(true);
		pb.redirectOutput(Redirect.appendTo(log));

		// Launch the engine
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Process p = pb.start();
					p.waitFor();
					if (p.exitValue() != 0){
						// TODO: Decide how to handle engine errors
					}
				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();

		// Return a socket for communication with the engine
		try {
			Socket s = serverSocket.accept();
			s.setSoTimeout(timeout);
			return s;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	private static void playMatch(){
		int currentPlayer = BLACK;
		ObjectInputStream currentIS = is1;
		ObjectOutputStream currentOS = os1;
		int roundNumber = 1;

		System.out.println("=============== Match Start ===============\n");
		if (matchLogging) printBoard();

		// Loop until the match is over, alternating engines
		while (!isTerminal()){
			try {
				if (currentPlayer == BLACK){
					currentOS.writeObject("genmove " + timeout1);
				} else {
					currentOS.writeObject("genmove " + timeout2);
				}
				String move = (String)currentIS.readObject();
				if (move.equals("pass")){
					// Switch current player and print current board state
					if (currentIS.equals(is1)){
						if (matchLogging) System.out.println("[INFO]: ROUND " + roundNumber + " [BLACK MOVE]: " + getReadableMoveString(move) + "\n");
						os2.writeObject("playmove " + move);
						currentIS = is2;
						currentOS = os2;
						currentPlayer = WHITE;
					} else {
						if (matchLogging) System.out.println("[INFO]: ROUND " + roundNumber + " [WHITE MOVE]: " + getReadableMoveString(move) + "\n");
						os1.writeObject("playmove " + move);
						currentIS = is1;
						currentOS = os1;
						currentPlayer = BLACK;
					}
					if (matchLogging) printBoard();
					roundNumber++;
					continue;
				} else if (move.equals("resign")){
					if (currentPlayer == BLACK){
						try {
							os1.writeObject("gameover 0");
							os2.writeObject("gameover 1");
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.println("[INFO]: Game over! Engine " + player1Path + " [BLACK] has resigned " + getReadableMoveString(move) + 
								" T1 = " + timeout1 + " T2 = " + timeout2);
					} else {
						try {
							os1.writeObject("gameover 1");
							os2.writeObject("gameover 0");
						} catch (IOException e) {
							e.printStackTrace();
						}
						System.out.println("[INFO]: Game over! Engine " + player2Path + " [WHITE] has resigned " + getReadableMoveString(move) + 
								" T1 = " + timeout1 + " T2 = " + timeout2);
					}
					return;
				} else if (moveLegal(move, currentPlayer)){
					// Apply the move to the board state and call playMove() for the other engine
					applyMove(move);
				} else {
					// The engine has made an illegal move
					if (currentPlayer == BLACK){
						System.out.println("[INFO]: Game over! Engine " + player1Path + " [BLACK] has made an illegal move " + getReadableMoveString(move) + 
								" T1 = " + timeout1 + " T2 = " + timeout2);
					} else {
						System.out.println("[INFO]: Game over! Engine " + player1Path + " [WHITE] has made an illegal move " + getReadableMoveString(move) + 
								" T1 = " + timeout1 + " T2 = " + timeout2);
					}
					os1.writeObject("gameover -1");
					os2.writeObject("gameover 2");
					break;
				}
				
				// Switch current player and print current board state
				if (currentIS.equals(is1)){
					if (matchLogging) System.out.println("[INFO]: ROUND " + roundNumber + " [BLACK MOVE]: " + getReadableMoveString(move) + "\n");
					os2.writeObject("playmove " + move);
					currentIS = is2;
					currentOS = os2;
					currentPlayer = WHITE;
				} else {
					if (matchLogging) System.out.println("[INFO]: ROUND " + roundNumber + " [WHITE MOVE]: " + getReadableMoveString(move) + "\n");
					os1.writeObject("playmove " + move);
					currentIS = is1;
					currentOS = os1;
					currentPlayer = BLACK;
				}
				if (matchLogging) printBoard();
				roundNumber++;
			} catch (SocketTimeoutException e){
				// The engine has exceeded the timeout
				if (currentPlayer == BLACK){
					System.out.println("[INFO]: Game over! Engine " + player1Path + " [BLACK] has exceeded the time limit" + 
							" T1 = " + timeout1 + " T2 = " + timeout2);
				} else {
					System.out.println("[INFO]: Game over! Engine " + player2Path + " [WHITE] has exceeded the time limit" + 
							" T1 = " + timeout1 + " T2 = " + timeout2);
				}
				try {
					os1.writeObject("gameover -2");
					os2.writeObject("gameover 3");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				break;
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		if (blackWins()){
			// Black victory
			try {
				os1.writeObject("gameover 1");
				os2.writeObject("gameover 0");
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("[INFO]: Game over! Engine " + player1Path + " [BLACK] has won" + 
					" T1 = " + timeout1 + " T2 = " + timeout2);
		} else if (whiteWins()){
			// White victory
			try {
				os1.writeObject("gameover 0");
				os2.writeObject("gameover 1");
			} catch (IOException e) {
				e.printStackTrace();
			}
			System.out.println("[INFO]: Game over! Engine " + player2Path + " [WHITE] has won" + 
					" T1 = " + timeout1 + " T2 = " + timeout2);
		} 
	}

	/*
	 * Returns a more readable representation of a move string
	 */
	private static String getReadableMoveString(String move){
		if (move.equals("pass") || move.equals("resign")){
			return move;
		}
		
		// Co-ordinates of the piece to move
		int fromRow = Integer.parseInt(String.valueOf(move.charAt(0)));
		int fromCol = Integer.parseInt(String.valueOf(move.charAt(1)));

		// The piece's destination co-ordinates
		int toRow = Integer.parseInt(String.valueOf(move.charAt(2)));
		int toCol = Integer.parseInt(String.valueOf(move.charAt(3)));
		
		return "(" + fromRow + ", " + fromCol + ") --> (" + toRow + ", " + toCol + ")";
	}

	/*
	 * Applies the move to the board state
	 */
	private static void applyMove(String move){
		// Co-ordinates of the piece to move
		int fromRow = Integer.parseInt(String.valueOf(move.charAt(0)));
		int fromCol = Integer.parseInt(String.valueOf(move.charAt(1)));

		// The piece's destination co-ordinates
		int toRow = Integer.parseInt(String.valueOf(move.charAt(2)));
		int toCol = Integer.parseInt(String.valueOf(move.charAt(3)));

		// Move the piece
		if (state[toRow][toCol] == BLACK){
			numBlack--;
		} else if (state[toRow][toCol] == WHITE){
			numWhite--;
		}
		state[toRow][toCol] = state[fromRow][fromCol];
		state[fromRow][fromCol] = EMPTY;
	}

	/*
	 * Return true if the move is legal, false otherwise
	 */
	private static boolean moveLegal(String move, int myColour){
		// Co-ordinates of the piece to move
		int fromRow = Integer.parseInt(String.valueOf(move.charAt(0)));
		int fromCol = Integer.parseInt(String.valueOf(move.charAt(1)));

		// The piece's destination co-ordinates
		int toRow = Integer.parseInt(String.valueOf(move.charAt(2)));
		int toCol = Integer.parseInt(String.valueOf(move.charAt(3)));

		// No piece to move or trying to move opponent's piece
		if ((state[fromRow][fromCol] == EMPTY) || (state[fromRow][fromCol] != myColour)){
			return false;
		}

		// Landing on own piece
		if (state[fromRow][fromCol] == state[toRow][toCol]) {
			return false;
		}

		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (fromRow == toRow){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[fromRow][i] != EMPTY){
					numPieces++;
					if (toCol > fromCol){
						if ((i > fromCol) && (i < toCol) && (state[fromRow][i] != myColour)){
							return false;
						}
					} else if (fromCol > toCol) {
						if ((i > toCol) && (i < fromCol) && (state[fromRow][i] != myColour)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toCol - fromCol)){
				return false;
			}
		} else if (fromCol == toCol){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[i][fromCol] != EMPTY){
					numPieces++;
					if (toRow > fromRow){
						if ((i > fromRow) && (i < toRow) && (state[i][fromCol] != myColour)){
							return false;
						}
					} else if (fromRow > toRow) {
						if ((i > toRow) && (i < fromRow) && (state[i][fromCol] != myColour)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toRow - fromRow)){
				return false;
			}
		} else {
			if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i < boardSize && j < boardSize; i++, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow - 1, j = fromCol - 1; i >= 0 && j >= 0; i--, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(toRow - fromRow)){
					return false;
				}
			} else if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i >= 0 && j < boardSize; i--, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow + 1, j = fromCol - 1; i < boardSize && j >= 0; i++, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(fromRow - toRow)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}

	/*
	 * Returns true of the board is in a terminal state, false otherwise
	 */
	private static boolean isTerminal(){
		return whiteWins() || blackWins();
	}

	/*
	 * Uses the connected(Coord c) method to check if the white player wins
	 */
	private static boolean whiteWins(){
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == WHITE){
						win = connected(new Coord(i, j));
						break outer;
					}

				}
			}
		}	
		return win;
	}

	/*
	 * Uses the connected(Coord c) method to check if the black player wins
	 */
	private static boolean blackWins(){
		boolean win = false;
		outer: {
			for (int i = 0; i < state.length; i++){
				for (int j = 0; j < state.length; j++){
					if (state[i][j] == BLACK){
						win = connected(new Coord(i, j));
						break outer;
					}

				}
			}
		}	
		return win;
	}

	/*
	 * Use a breadth-first-search to check if a player's pieces are connected
	 * Returns true if all a player's pieces are connected to the piece located at c
	 */
	private static boolean connected(Coord c){
		int colour = state[c.getRow()][c.getCol()];
		LinkedList<Coord> q = new LinkedList<Coord>();
		HashSet<Coord> checked = new HashSet<Coord>();
		q.add(c);
		while(!q.isEmpty()){
			Coord current = q.remove();
			int row = current.getRow();
			int col = current.getCol();
			if (state[row][col] == colour){
				checked.add(current);
				Coord n = new Coord(row - 1, col);
				Coord s = new Coord(row + 1, col);
				Coord e = new Coord(row, col + 1);
				Coord w = new Coord(row, col - 1);
				Coord ne = new Coord(row - 1, col + 1);
				Coord nw = new Coord(row - 1, col - 1);
				Coord se = new Coord(row + 1, col + 1);
				Coord sw = new Coord(row + 1, col - 1);
				if ((row > 0) && !checked.contains(n)){
					q.add(n);
				}
				if ((row < state.length - 1) && !checked.contains(s)){
					q.add(s);	
				}
				if ((col < state.length - 1) && !checked.contains(e)){
					q.add(e);
				}
				if ((col > 0) && !checked.contains(w)){
					q.add(w);
				}
				if ((row > 0) && (col < state.length - 1) && !checked.contains(ne)){
					q.add(ne);
				}
				if ((row > 0) && (col > 0) && !checked.contains(nw)){
					q.add(nw);
				}
				if ((col < state.length - 1) && (row < state.length - 1) && !checked.contains(se)){
					q.add(se);
				}
				if ((col > 0) && (row < state.length - 1) && !checked.contains(sw)){
					q.add(sw);
				}
			}
		}
		if (colour == WHITE){
			return checked.size() == numWhite;
		} else {
			return checked.size() == numBlack;
		}
	}

	/*
	 * Prints the current board state to the standard output stream
	 */
	private static void printBoard(){
		String s = "";
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize - 1; j++){
				switch (state[i][j]) {
				case WHITE:
					s = s + "W ";
					break;
				case BLACK:
					s = s + "B ";
					break;
				case EMPTY:
					s = s + ". ";
					break;
				}
			}
			switch (state[i][boardSize - 1]) {
			case WHITE:
				s = s + "W\n";
				break;
			case BLACK:
				s = s + "B\n";
				break;
			case EMPTY:
				s = s + ".\n";
				break;
			}
		}
		System.out.println(s);
	}

	/*
	 * A convenience class for co-ordinates.
	 */
	private static class Coord {
		private int row;
		private int col;

		public Coord(int row, int col){
			this.row = row;
			this.col = col;
		}

		public int getRow() {
			return row;
		}

		public void setRow(int row) {
			this.row = row;
		}

		public int getCol() {
			return col;
		}

		public void setCol(int col) {
			this.col = col;
		}

		public String toString(){
			return "(" + row + ", " + col + ")";
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + col;
			result = prime * result + row;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Coord other = (Coord) obj;
			if (col != other.col)
				return false;
			if (row != other.row)
				return false;
			return true;
		}
	}
}
