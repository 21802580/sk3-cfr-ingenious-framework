import java.util.ArrayList;
import java.util.Random;

public class RandomPlayer implements LOAPlayer {
	private int[][] state;					// Current game state
	private int myColour;					// This engine's colour. If genMove is called before playMove, it is black. Otherwise, it is white
	private final int boardSize = 8;		// Assume that the board is 8x8
	
	// Colour constants
	private static final int WHITE = 0;
	private static final int BLACK = 1;
	private static final int EMPTY = 2;
	
	/*
	 * Initialize state array with the initial LOA board.
	 */
	public RandomPlayer(){
		myColour = EMPTY;
		state = new int[boardSize][boardSize];
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j <  boardSize; j++){
				if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)){
					state[i][j] = BLACK;
				} else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
					state[i][j] = WHITE;
				} else {
					state[i][j] = EMPTY;	
				}
			}
		}
	}
	
	/*
	 * Apply the given move to the board 
	 */
	@Override
	public void playMove(String move) {
		// Check if playMove was called first
		if (myColour == EMPTY){
			myColour = WHITE;
		}
		
		// Co-ordinates of the piece to move
		int fromRow = Integer.parseInt(String.valueOf(move.charAt(0)));
		int fromCol = Integer.parseInt(String.valueOf(move.charAt(1)));
		
		// The piece's destination co-ordinates
		int toRow = Integer.parseInt(String.valueOf(move.charAt(2)));
		int toCol = Integer.parseInt(String.valueOf(move.charAt(3)));
		
		// Move the piece
		state[toRow][toCol] = state[fromRow][fromCol];
		state[fromRow][fromCol] = EMPTY;
	}

	/*
	 * Return a random legal move.
	 * Not the most efficient implementation, but it will do as an example
	 */
	@Override
	public String genMove(long timeout) {
		// Check if genMove was called first
		if (myColour == EMPTY){
			myColour = BLACK;
		}
		
		// Find co-ordinates of my pieces
		ArrayList<Integer> rows = new ArrayList<Integer>();
		ArrayList<Integer> cols = new ArrayList<Integer>();
		for (int i = 0; i < boardSize; i++){
			for (int j = 0; j < boardSize; j++){
				if (state[i][j] == myColour){
					rows.add(i);
					cols.add(j);
				}
			}
		}
		
		// Find a random legal move 
		Random rand = new Random();
		int numElements = rows.size();
		for (int k = 0; k < numElements; k++){
			int index = rand.nextInt(rows.size());
			int fromRow = rows.remove(index);
			int fromCol = cols.remove(index);
			for (int i = 0; i < boardSize; i++){
				for (int j = 0; j < boardSize; j++){
					if (moveLegal(fromRow, fromCol, i, j)){
						String move = "" + fromRow + fromCol + i + j;
						playMove(move);
						return move;
					}
				}
			}	
		}
		return "";
	}

	@Override
	public void gameOver(int terminationCode) {
		
	}
	/*
	 * Return true if the move is legal, false otherwise
	 */
	private boolean moveLegal(int fromRow, int fromCol, int toRow, int toCol){
		// No piece to move or trying to move opponent's piece
		if ((state[fromRow][fromCol] == EMPTY) || (state[fromRow][fromCol] != myColour)){
			return false;
		}
		
		// Landing on own piece
		if (state[fromRow][fromCol] == state[toRow][toCol]) {
			return false;
		}
		
		// Check that player moves the correct number of spaces and doesn't jump over
		// enemy's pieces
		if (fromRow == toRow){
			// Horizontal
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[fromRow][i] != EMPTY){
					numPieces++;
					if (toCol > fromCol){
						if ((i > fromCol) && (i < toCol) && (state[fromRow][i] != myColour)){
							return false;
						}
					} else if (fromCol > toCol) {
						if ((i > toCol) && (i < fromCol) && (state[fromRow][i] != myColour)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toCol - fromCol)){
				return false;
			}
		} else if (fromCol == toCol){
			// Vertical
			int numPieces = 0;
			for (int i = 0; i < boardSize; i++){
				if (state[i][fromCol] != EMPTY){
					numPieces++;
					if (toRow > fromRow){
						if ((i > fromRow) && (i < toRow) && (state[i][fromCol] != myColour)){
							return false;
						}
					} else if (fromCol > toCol) {
						if ((i > toRow) && (i < fromRow) && (state[i][fromCol] != myColour)){
							return false;
						}
					}
				}
			}
			if (numPieces != Math.abs(toRow - fromRow)){
				return false;
			}
		} else {
			if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == 1){
				// Positive diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i < boardSize && j < boardSize; i++, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow - 1, j = fromCol - 1; i >= 0 && j >= 0; i--, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(toRow - fromRow)){
					return false;
				}
			} else if ((double)(fromRow - toRow) / (double)(fromCol - toCol) == -1) {
				// Negative diagonal
				int numPieces = 0;
				for (int i = fromRow, j = fromCol; i >= 0 && j < boardSize; i--, j++){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				for (int i = fromRow + 1, j = fromCol - 1; i < boardSize && j >= 0; i++, j--){
					if (state[i][j] != EMPTY){
						numPieces++;
						if (toCol > fromCol){
							if ((j > fromCol) && (j < toCol) && (state[i][j] != myColour)){
								return false;
							}
						} else if (fromCol > toCol) {
							if ((j > toCol) && (j < fromCol) && (state[i][j] != myColour)){
								return false;
							}
						}
					}
				}
				if (numPieces != Math.abs(fromRow - toRow)){
					return false;
				}
			} else {
				// Not a straight line
				return false;
			}
		}
		return true;
	}
}
