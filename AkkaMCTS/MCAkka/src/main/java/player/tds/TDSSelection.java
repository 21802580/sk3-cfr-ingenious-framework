package player.tds;

public class TDSSelection {
	private long nodeHash;
	private int selectedMoveIndex;
	private int homeProcessorIndex;
	
	public TDSSelection(long nodeHash, int selectedMoveIndex, int homeProcessorIndex) {
		this.selectedMoveIndex = selectedMoveIndex;
		this.nodeHash = nodeHash;
		this.homeProcessorIndex = homeProcessorIndex;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
	
	public int getSelectedMoveIndex() {
		return selectedMoveIndex;
	}
	
	public int getHomeProcessorIndex() {
		return homeProcessorIndex;
	}

	public String toString() {
		return "(" + nodeHash + ": " + homeProcessorIndex + ")";
	}
}
