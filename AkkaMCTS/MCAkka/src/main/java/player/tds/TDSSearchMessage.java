package player.tds;

import loa.gamelogic.Board;
import player.core.message.Message;

import java.util.List;

public class TDSSearchMessage implements Message {
	private Board board;
	private List<TDSSelection> path;
	private long currentHash;
	private int searchId;
	
	public TDSSearchMessage(Board board, List<TDSSelection> path, long currentHash, int searchId) {
		this.board = board;
		this.path = path;
		this.currentHash = currentHash;
		this.searchId = searchId;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public List<TDSSelection> getPath() {
		return path;
	}
	
	public long getCurrentHash() {
		return currentHash;
	}

	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append("=== Search " + currentHash + " ===\n");
		for (TDSSelection s : path) {
			b.append(s + "\n");
		}
		b.append(board.toString() + "\n");
		return b.toString();
	}

	public int getSearchId() {
		return searchId;
	}
}
