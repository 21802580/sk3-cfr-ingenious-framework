package player.tds;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.transposition.ConcurrentHashMapTranspositionTable;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;

import java.util.ArrayList;
import java.util.List;

public class TDSSearchRank extends AbstractSearchRank {
	private int numPlayouts;
	private boolean running;
	private ActorRef[] allSearchRanks;
	private int rankIndex;
	private ActorRef workerRouter;
	private int numRootReports;
	private int numWorkers;
	private int numPlayoutsReceived;
	
	public TDSSearchRank(PlayerParameters playerParameters, TDSSearchRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		running = true;
		numWorkers = Math.max(1, playerParameters.getNumThreads() - 1);
		SmallestMailboxPool pool = new SmallestMailboxPool(numWorkers);
		allSearchRanks = initMessage.getAllSearchRanks();
		rankIndex = initMessage.getRankIndex();
		workerRouter = context().actorOf(pool.props(Props.create(
				TDSWorker.class, playerParameters, transpositionTable, allSearchRanks, rankIndex)));
	}
	
	private void processSearchMessage(TDSSearchMessage msg){
		long hash = msg.getCurrentHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			if (homeProc == rankIndex) {
				workerRouter.tell(msg, getSelf());
			} else {
				allSearchRanks[homeProc].tell(msg, getSelf());
			}
		}
	}
	
	private void processReportMessage(TDSReportMessage msg){
		long hash = msg.getHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			if (homeProc == rankIndex) {
				workerRouter.tell(msg, getSelf());
				if (hash == rootHash){
					numRootReports++;
				}
			} else {
				allSearchRanks[homeProc].tell(msg, getSelf());
			}
		}
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	@Override
	protected void matchStarted(MatchStartMessage message) {
		if (getRootHomeIndex() == rankIndex) {
			int numParallelSearches = playerParameters.getTdsOverload() * allSearchRanks.length * numWorkers;
//            int numParallelSearches = 2;
			for (int i = 0; i < numParallelSearches; i++) {
				Board b = new Board();
				List<TDSSelection> path = new ArrayList<TDSSelection>();
				getSelf().tell(new TDSSearchMessage(b, path, rootHash, i), getSelf());
			}
		}
	}
	
	@Override
	protected void turnStart() {
		numPlayouts = 0;
		numRootReports = 0;
	}
	
	private void requestPlayouts(){
		numPlayoutsReceived = 0;
		workerRouter.tell(new Broadcast(new PlayoutRequestMessage()), getSelf());
	}
	
	private void addPlayouts(TotalPlayoutMessage msg){
		numPlayouts += msg.numSimulations();
		if (++numPlayoutsReceived == numWorkers){
			sendSearchResult();
		}
	}
	
	private void sendSearchResult(){
		if (getRootHomeIndex() != rankIndex){
			clusterManager.tell(new SearchResultMessage(null, numPlayouts, 0, 0), getSelf());
		} else {
			TreeNode node = transpositionTable.get(rootHash);
			int moveIndex = MCTS.getHighestVisitChild(node);
			Move move = node.getMoveAt(moveIndex);
//			System.out.println("CN" + rankIndex +  " FINAL MOVE " + move + " at index " + moveIndex + " from:");
//			for (int i = 0; i < node.getNumChildren(); i++) {
//				System.out.println("CN" + rankIndex + " " + i + ": " + node.getMoveAt(i) + " [" + node.getMoveScore(i) + "/" + node.getMoveSelections(i) + "]");
//			}
			clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, numRootReports), getSelf());
		}
	}
	
	@Override
	protected void turnEnd() {
		requestPlayouts();
	}
	
	@Override
	protected void rootUpdated(Move m) {
		workerRouter.tell(new Broadcast(new ApplyMoveMessage(m)), getSelf());
	}
	
	@Override
	protected TranspositionTable makeTranspositionTable() {
		return new ConcurrentHashMapTranspositionTable(playerParameters.getTtPower(), false, playerParameters.getPerTurnCapacityPower());
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof TDSSearchMessage) {
			processSearchMessage((TDSSearchMessage)msg);
		} else if (msg instanceof TDSReportMessage){
			processReportMessage((TDSReportMessage)msg);
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		} else if (msg instanceof TotalPlayoutMessage){
			addPlayouts((TotalPlayoutMessage)msg);
		}
	}
	
	private int getRootHomeIndex(){
		return Zobrist.getHomeProcessorIndex(rootHash, allSearchRanks.length, transpositionTable.getCapacity());
	}
}
