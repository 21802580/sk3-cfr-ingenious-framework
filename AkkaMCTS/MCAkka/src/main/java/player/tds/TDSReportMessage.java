package player.tds;

import player.core.message.Message;

public class TDSReportMessage implements Message {
	private long hash;
	private int moveIndex;
	private double[] reward;
	private int searchId;
	
	public TDSReportMessage(long hash, int moveIndex, double[] reward, int searchId) {
		this.hash = hash;
		this.moveIndex = moveIndex;
		this.reward = reward;
		this.searchId = searchId;
	}
	
	public long getHash() {
		return hash;
	}
	
	public double[] getReward() {
		return reward;
	}
	
	public int getMoveIndex() {
		return moveIndex;
	}

	public String toString(){
		return "=== Report " + hash + " (" + moveIndex + ")" + " ===";
	}

	public int getSearchId() {
		return searchId;
	}
}
