package player.tds;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.ApplyMoveMessage;
import player.core.message.NodeAddedMessage;
import player.core.message.TotalPlayoutMessage;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.Selection;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

public class TDSWorker extends AbstractActor {
	
	private PlayerParameters playerParameters;
	private TranspositionTable transpositionTable;
	private ActorRef[] allSearchRanks;
	private Board rootBoard;
	private long rootHash;
	private int rankIndex;
	private int numSims;
	
	public TDSWorker(PlayerParameters playerParameters, TranspositionTable transpositionTable, ActorRef[] allSearchRanks, int rankIndex){
		this.playerParameters = playerParameters;
		this.transpositionTable = transpositionTable;
		this.allSearchRanks = allSearchRanks;
		this.rankIndex = rankIndex;
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(TDSSearchMessage.class, this::processSearchMessage)
				.match(TDSReportMessage.class, this::processReportMessage)
				.match(ApplyMoveMessage.class, this::updateRoot)
				.match(PlayoutRequestMessage.class, this::sendPlayouts)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void processSearchMessage(TDSSearchMessage msg) {
//		System.out.println("S" + msg.getSearchId() + " " + msg.toString().replace("\n", "\nS" + msg.getSearchId()));
		List<TDSSelection> path = msg.getPath();
		Board board = msg.getBoard();
		long hash = msg.getCurrentHash();
		if (board.getWinner() != null){
			// Terminal state -> end descent
			simulateAndSendReports(board, path, msg.getSearchId());
			return;
		}
		TreeNode node = transpositionTable.putIfAbsent(hash, board, playerParameters);
		if (node == null) {
			// TT full -> end descent
			simulateAndSendReports(board, path, msg.getSearchId());
			return;
		} else if (node.getTotalVisits() == 0 && node.getHash() == rootHash){
			// Add one visit so that a simulation is not performed at the root
			node.addVisits(1);
		}
		Selection s = MCTS.actionSelection(node, playerParameters);
		if (s == null){
			// First visit for node -> end descent
			allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
			simulateAndSendReports(board, path, msg.getSearchId());
			return;
		}
		addSelectionAndSendSearch(node, board, path, hash, s, msg.getSearchId());
	}
	
	private void processReportMessage(TDSReportMessage msg) {
		TreeNode node = transpositionTable.get(msg.getHash());
		if (node != null){
			// Only add reward if node has not been overwritten
			double reward = MCTS.updateReward(node, msg.getMoveIndex(), msg.getReward());
//			StringBuilder b = new StringBuilder();
//			b.append("S" + msg.getSearchId() + " " + msg + " " + " [" + reward + "]: " + node.getMoveAt(msg.getMoveIndex()) + " TO:\n");
//            for (int i = 0; i < node.getNumChildren(); i++) {
//                b.append("S" + msg.getSearchId() + " " + i + ": " + node.getMoveAt(i) + " [" + node.getMoveScore(i) + "/" + node.getMoveSelections(i) + "]\n");
//            }
//            System.out.println(b);
		}
	}
	
	private void sendPlayouts(PlayoutRequestMessage msg){
		getSender().tell(new TotalPlayoutMessage(numSims), getSelf());
	}
	
	private void addSelectionAndSendSearch(TreeNode node, Board board, List<TDSSelection> path, long currentHash, Selection newSelection, int searchId){
		Move m = node.getMoveAt(newSelection.getSelectedMoveIndex());
//        StringBuilder b = new StringBuilder();
//        b.append("S" + searchId + " SELECT " + newSelection.getSelectedMoveIndex() + ": " + m + " FROM:\n");
//        for (int i = 0; i < node.getNumChildren(); i++) {
//            b.append("S" + searchId + " " + i + ": " + node.getMoveAt(i) + " [" + node.getMoveScore(i) + "/" + node.getMoveSelections(i) + "]\n");
//        }
//        System.out.println(b);
		long newHash = Zobrist.getChildHash(currentHash, m, board.getCurrentPlayer());
		board.applyMove(m);
		TDSSelection s = new TDSSelection(newSelection.getNodeHash(), newSelection.getSelectedMoveIndex(), rankIndex);
		path.add(s);
		allSearchRanks[rankIndex].tell(new TDSSearchMessage(board, path, newHash, searchId), getSelf());
	}
	
	private void simulateAndSendReports(Board board, List<TDSSelection> path, int searchId){
		numSims++;
		double[] rewards = MCTS.randomPlayout(board);
//		System.out.println("S" + searchId + " SIMULATION: [" + rewards[0] + ", " + rewards[1] + "]");
		for (TDSSelection s : path){
			// Send report messages to all processors on the path to the root
			TDSReportMessage msg = new TDSReportMessage(s.getNodeHash(), s.getSelectedMoveIndex(), rewards, searchId);
			allSearchRanks[rankIndex].tell(msg, getSelf());
		}
		// Send a search message to the home processor of the root
		Board b = new Board(rootBoard);
		List<TDSSelection> p = new ArrayList<TDSSelection>();
		TDSSearchMessage newSearch = new TDSSearchMessage(b, p, rootHash, searchId);
		allSearchRanks[rankIndex].tell(newSearch, getSelf());
	}
	
	private void updateRoot(ApplyMoveMessage msg){
		numSims = 0;
		rootHash = Zobrist.getChildHash(rootHash, msg.getMove(), rootBoard.getCurrentPlayer());
		rootBoard.applyMove(msg.getMove());
//		System.out.println("CN" + rankIndex + "NEW ROOT (" + rankIndex + ")" + rootHash);
	}
	
//	private class CallableReport implements Callable<Void> {
//		private TDSReportMessage msg;
//
//		public CallableReport(TDSReportMessage msg) {
//			this.msg = msg;
//		}
//
//		@Override
//		public Void call() throws Exception {
//			TreeNode node = transpositionTable.get(msg.getHash());
//			if (node != null){
//				// Only add reward if node has not been overwritten
//				MCTS.updateReward(node, msg.getMoveIndex(), msg.getReward());
//			}
//			return null;
//		}
//	}
//
//	private class CallableSearch implements Callable<Boolean> {
//		private TDSSearchMessage msg;
//
//		public CallableSearch(TDSSearchMessage msg) {
//			this.msg = msg;
//		}
//
//		@Override
//		public Boolean call() {
//			List<TDSSelection> path = msg.getPath();
//			Board board = msg.getBoard();
//			long hash = msg.getCurrentHash();
//			if (board.getWinner() != null){
//				// Terminal state -> end descent
//				simulateAndSendReports(board, path);
//				return Boolean.TRUE;
//			}
//			TreeNode node = transpositionTable.putIfAbsent(hash, board, playerParameters);
//			if (node == null) {
//				// TT full -> end descent
//				simulateAndSendReports(board, path);
//				return Boolean.TRUE;
//			} else if (node.getTotalVisits() == 0 && node.getHash() == rootHash){
//				// Add one visit so that a simulation is not performed at the root
//				node.addVisits(1);
//			}
//			Selection s = MCTS.actionSelection(node, playerParameters);
//			if (s == null){
//				// First visit for node -> end descent
//				allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
//				simulateAndSendReports(board, path);
//				return Boolean.TRUE;
//			}
//			addSelectionAndSendSearch(node, board, path, hash, s);
//			return Boolean.FALSE;
//		}
//	}
}
