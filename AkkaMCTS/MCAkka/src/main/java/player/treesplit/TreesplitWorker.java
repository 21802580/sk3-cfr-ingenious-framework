package player.treesplit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.ApplyMoveMessage;
import player.core.message.NodeAddedMessage;
import player.core.message.TotalPlayoutMessage;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.Selection;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;
import player.tds.TDSReportMessage;
import player.tds.TDSSearchMessage;
import player.tds.TDSSelection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

public class TreesplitWorker extends AbstractActor{
	private PlayerParameters playerParameters;
	private TranspositionTable transpositionTable;
	private ActorRef[] allSearchRanks;
	private int rankIndex;
	private Map<Long, TreeNode> duplicatedNodes;
	private Board rootBoard;
	private long rootHash;
	private int numSims;
	
	public TreesplitWorker(PlayerParameters playerParameters, TranspositionTable transpositionTable, ActorRef[] allSearchRanks, int rankIndex, Map<Long, TreeNode> duplicatedNodes){
		this.playerParameters = playerParameters;
		this.transpositionTable = transpositionTable;
		this.allSearchRanks = allSearchRanks;
		this.rankIndex = rankIndex;
		this.duplicatedNodes = duplicatedNodes;
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(TDSSearchMessage.class, this::processSearchMessage)
				.match(TDSReportMessage.class, this::processReportMessage)
				.match(ApplyMoveMessage.class, this::updateRoot)
				.match(PlayoutRequestMessage.class, this::sendPlayouts)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void sendPlayouts(PlayoutRequestMessage msg){
		getSender().tell(new TotalPlayoutMessage(numSims), getSelf());
	}
	
	private void processSearchMessage(TDSSearchMessage msg) {
		List<TDSSelection> path = msg.getPath();
		Board board = msg.getBoard();
		long hash = msg.getCurrentHash();
		if (board.getWinner() != null){
			// Terminal state -> end descent
			simulateAndSendReports(board, path);
			return;
		}
		TreeNode node = duplicatedNodes.get(hash);
		if (node == null){
			node = transpositionTable.putIfAbsent(hash, board, playerParameters);
		}
		if (node == null) {
			// TT full -> end descent
			simulateAndSendReports(board, path);
			return;
		} else if (node.getTotalVisits() == 0 && node.getHash() == rootHash){
			// Add one visit so that a simulation is not performed at the root
			node.addVisits(1);
		}
		//trySync(node, board);
		Selection s = MCTS.actionSelection(node, playerParameters);
		if (s == null){
			// First visit for node -> end descent
			allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
			simulateAndSendReports(board, path);
			return;
		}
		addSelectionAndSendSearch(node, board, path, hash, s);
	}
	
	private void processReportMessage(TDSReportMessage msg) {
		long hash = msg.getHash();
		TreeNode node = duplicatedNodes.get(hash);
		if (node == null){
			node = transpositionTable.get(hash);
		}
		if (node != null){
			// Only add reward if node has not been overwritten
			MCTS.updateReward(node, msg.getMoveIndex(), msg.getReward());
		}
	}
	
	private void addSelectionAndSendSearch(TreeNode node, Board board, List<TDSSelection> path, long currentHash, Selection newSelection){
		Move m = node.getMoveAt(newSelection.getSelectedMoveIndex());
		long newHash = Zobrist.getChildHash(currentHash, m, board.getCurrentPlayer());
		board.applyMove(m);
		TDSSelection s = new TDSSelection(newSelection.getNodeHash(), newSelection.getSelectedMoveIndex(), rankIndex);
		path.add(s);
		allSearchRanks[rankIndex].tell(new TDSSearchMessage(board, path, newHash, 0), getSelf());
	}
	
	private void simulateAndSendReports(Board board, List<TDSSelection> path){
		numSims++;
		double[] rewards = MCTS.randomPlayout(board);
		for (TDSSelection s : path){
			// Send report messages to all processors on the path to the root
			TDSReportMessage msg = new TDSReportMessage(s.getNodeHash(), s.getSelectedMoveIndex(), rewards, 0);
			allSearchRanks[rankIndex].tell(msg, getSelf());
		}
		Board b = new Board(rootBoard);
		List<TDSSelection> p = new ArrayList<TDSSelection>();
		TDSSearchMessage newSearch = new TDSSearchMessage(b, p, rootHash, 0);
		allSearchRanks[rankIndex].tell(newSearch, getSelf());
	}
	
	private void updateRoot(ApplyMoveMessage msg){
		numSims = 0;
		rootHash = Zobrist.getChildHash(rootHash, msg.getMove(), rootBoard.getCurrentPlayer());
		rootBoard.applyMove(msg.getMove());
	}
	
	private class CallableReport implements Callable<Void> {
		private TDSReportMessage msg;
		
		public CallableReport(TDSReportMessage msg) {
			this.msg = msg;
		}
		
		@Override
		public Void call() throws Exception {
			long hash = msg.getHash();
			TreeNode node = duplicatedNodes.get(hash);
			if (node == null){
				node = transpositionTable.get(hash);
			}
			if (node != null){
				// Only add reward if node has not been overwritten
				MCTS.updateReward(node, msg.getMoveIndex(), msg.getReward());
			}
			return null;
		}
	}
	
	private class CallableSearch implements Callable<Boolean> {
		private TDSSearchMessage msg;
		
		private CallableSearch(TDSSearchMessage msg) {
			this.msg = msg;
		}
		
		@Override
		public Boolean call() throws Exception {
			List<TDSSelection> path = msg.getPath();
			Board board = msg.getBoard();
			long hash = msg.getCurrentHash();
			if (board.getWinner() != null){
				// Terminal state -> end descent
				simulateAndSendReports(board, path);
				return Boolean.TRUE;
			}
			TreeNode node = duplicatedNodes.get(hash);
			if (node == null){
				node = transpositionTable.putIfAbsent(hash, board, playerParameters);
			}
			if (node == null) {
				// TT full -> end descent
				simulateAndSendReports(board, path);
				return Boolean.TRUE;
			} else if (node.getTotalVisits() == 0 && node.getHash() == rootHash){
				// Add one visit so that a simulation is not performed at the root
				node.addVisits(1);
			}
			//trySync(node, board);
			Selection s = MCTS.actionSelection(node, playerParameters);
			if (s == null){
				// First visit for node -> end descent
				allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
				simulateAndSendReports(board, path);
				return Boolean.TRUE;
			}
			addSelectionAndSendSearch(node, board, path, hash, s);
			return Boolean.FALSE;
		}
	}
	
	
}
