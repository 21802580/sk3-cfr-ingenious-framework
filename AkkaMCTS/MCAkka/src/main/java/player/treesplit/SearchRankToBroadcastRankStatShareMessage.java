package player.treesplit;

import akka.actor.ActorRef;
import loa.gamelogic.Board;
import player.core.message.Message;
import player.core.node.MoveStatistics;
import player.root.NodeStatisticsMessage;

public class SearchRankToBroadcastRankStatShareMessage implements Message{
	private NodeStatisticsMessage message;
	
	public SearchRankToBroadcastRankStatShareMessage(ActorRef searchRank, long nodeHash, Board board, MoveStatistics[] newStatistics) {
		message = new NodeStatisticsMessage(searchRank, nodeHash, board, newStatistics);
	}
	
	public NodeStatisticsMessage getMessage() {
		return message;
	}
}
