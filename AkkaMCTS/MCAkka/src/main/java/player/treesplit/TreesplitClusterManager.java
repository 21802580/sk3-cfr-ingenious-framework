package player.treesplit;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Move;
import player.core.actor.ClusterManager;
import player.core.message.ApplyMoveMessage;
import player.core.message.GenMoveMessage;
import player.core.message.SearchResultMessage;

import java.util.*;

public class TreesplitClusterManager extends ClusterManager{
	private int numPendingResults;
	private HashMap<Move, Integer> votes;
	private int receivedPlayouts;
	private int numRootReports;
	private List<ActorRef> searchRanks;
	
	public TreesplitClusterManager(PlayerParameters playerParameters) {
		super(playerParameters);
	}
	
	@Override
	public SearchResultMessage searchResultReceived(SearchResultMessage msg) {
		receivedPlayouts += msg.getTotalPlayouts();
		numRootReports += msg.getReportsAtRoot();
		Move m = msg.getSelectedMove();
		if (m == null) return null;
		if (!votes.containsKey(m)){
			votes.put(m, 1);
		} else {
			votes.put(m, votes.get(m) + 1);
		}
		if (--numPendingResults == 0){
			int mostVotes = 0;
			Move chosenMove = null;
			for (Map.Entry entry : votes.entrySet()){
				if ((int)entry.getValue() > mostVotes){
					mostVotes = (int)entry.getValue();
					chosenMove = (Move)entry.getKey();
				}
			}
			for (ActorRef a : searchRanks){
				a.tell(new ApplyMoveMessage(chosenMove), getSelf());
			}
			return new SearchResultMessage(chosenMove, receivedPlayouts, 0, numRootReports);
		} else {
			return null;
		}
	}
	
	@Override
	public void clusterMemberJoined(ActorRef member) {
		if (numRegistrationsReceived == playerParameters.getNumNodes()){
			sendInitMessages();
		}
	}
	
	//TODO: Gross.
	private void sendInitMessages(){
		int maxSearchRanksPerBroadcastRank = playerParameters.getSchaefersSearchRanksPerBroadcaster() * playerParameters.getNumThreads();
		searchRanks = new ArrayList<ActorRef>();
		List<ActorRef> broadcastRanks = new ArrayList<ActorRef>();
		broadcastRanks.add(allMembers[0]);
		int numSearchRanks = 0;
		for (int i = 1; i < allMembers.length; i++){
			ActorRef a = allMembers[i];
			if (numSearchRanks == maxSearchRanksPerBroadcastRank){
				broadcastRanks.add(a);
				numSearchRanks = 0;
			} else {
				searchRanks.add(a);
				numSearchRanks++;
			}
		}
		
		// Map search ranks to broadcast ranks
		HashMap<ActorRef, List<ActorRef>> broadcasterMap = new HashMap<ActorRef, List<ActorRef>>();
		for (ActorRef a : broadcastRanks){
			broadcasterMap.put(a, new ArrayList<ActorRef>());
		}
		int currentBroadcaster = 0;
		for (ActorRef a : searchRanks){
			ActorRef broadcaster = broadcastRanks.get(currentBroadcaster);
			List<ActorRef> broadcasterSearchRanks = broadcasterMap.get(broadcaster);
			broadcasterSearchRanks.add(a);
			currentBroadcaster = (currentBroadcaster + 1) % broadcastRanks.size();
		}
		
		ActorRef[] broadcastRankArr = new ActorRef[broadcastRanks.size()];
		broadcastRankArr = broadcastRanks.toArray( broadcastRankArr);
		for (Map.Entry e : broadcasterMap.entrySet()){
			ActorRef broadcastRank = (ActorRef)e.getKey();
			List<ActorRef> srList = (List<ActorRef>)e.getValue();
			ActorRef[] localSearchRanks = new ActorRef[srList.size()];
			localSearchRanks = srList.toArray(localSearchRanks);
			broadcastRank.tell(new TreesplitBroadcastRankInitMessage(localSearchRanks, broadcastRankArr, nodeCounter), getSelf());
		}
		
		ActorRef[] allSearchRanks = new ActorRef[searchRanks.size()];
		allSearchRanks = searchRanks.toArray(allSearchRanks);
		for (int i = 0; i < allSearchRanks.length; i++){
			ActorRef a = allSearchRanks[i];
			a.tell(new TreesplitSearchRankInitMessage(i, allSearchRanks, nodeCounter), getSelf());
		}
	}
	
	@Override
	protected void genMove(GenMoveMessage msg) {
		for (ActorRef a : searchRanks){
			a.tell(msg, getSelf());
		}
		numPendingResults = searchRanks.size();
		votes = new HashMap<Move, Integer>();
		receivedPlayouts = 0;
		numRootReports = 0;
	}
	
	@Override
	protected void allMembersInitialised() {
		for (ActorRef a : allMembers){
			a.tell(new AllMembersReadyMessage(), getSelf());
		}
	}
}
