package player.treesplit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import player.core.node.MoveStatistics;
import player.root.NodeStatisticsMessage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TreesplitBroadcastWorker extends AbstractActor {
	private PlayerParameters playerParameters;
	private ActorRef[] searchRanks;
	private ActorRef[] remoteBroadcastRanks;
	private ActorRef[] broadcastWorkers;
	private ActorRef broadcastRank;
	private HashMap<ActorRef, List<NodeStatisticsMessage>> messages;
	
	public TreesplitBroadcastWorker(PlayerParameters playerParameters,  ActorRef[] searchRanks, ActorRef[] remoteBroadcastRanks){
		this.playerParameters = playerParameters;
		this.searchRanks = searchRanks;
		this.remoteBroadcastRanks = remoteBroadcastRanks;
		initMap();
	}
	
	private void initMap(){
		messages = new HashMap<ActorRef, List<NodeStatisticsMessage>>();
		for (ActorRef sr : searchRanks){
			messages.put(sr, new ArrayList<NodeStatisticsMessage>());
		}
	}
	
	private void sendReferences(AllMembersReadyMessage msg){
		for (ActorRef a : searchRanks){
			a.tell(new BroadcasterSupplyMessage(), getSelf());
		}
	}
	
	private void addWorkers(BroadcastWorkerArrayMessage msg){
		this.broadcastWorkers = msg.getBroadcasterWorkers();
	}
	
	private void setParent(ActorRef broadcastRank){
		this.broadcastRank = broadcastRank;
	}
	
	private void forwardStatistics(NodeStatisticsMessage msg){
		for (ActorRef a : searchRanks){
			a.tell(msg, getSelf());
		}
	}
	
	private void shareNodeStatistics(StartShareMessage msg){
		for (Map.Entry e : messages.entrySet()){
			ActorRef sender = (ActorRef)e.getKey();
			List<NodeStatisticsMessage> messages = (List<NodeStatisticsMessage>)e.getValue();
			for (NodeStatisticsMessage message : messages){
				broadcastMessage(message, sender);
			}
		}
		initMap();
	}
	
	private void broadcastMessage(NodeStatisticsMessage msg, ActorRef ignore){
		for (ActorRef a : searchRanks){
			if (!a.equals(ignore)){
				a.tell(msg, getSelf());
			}
		}
		for (ActorRef a : remoteBroadcastRanks){
			if (!a.equals(broadcastRank)){
				a.tell(msg, getSelf());
			}
		}
		for (ActorRef a : broadcastWorkers){
			if (!a.equals(getSelf())) {
				a.tell(msg, getSelf());
			}
		}
	}
	
	private void addStatistics(SearchRankToBroadcastRankStatShareMessage message){
		NodeStatisticsMessage msg = message.getMessage();
		long hash = msg.getNodeHash();
		ActorRef sender = msg.getSearchRank();
		List<NodeStatisticsMessage> messagesFromRank = messages.get(sender);
		boolean hasMessage = false;
		for (NodeStatisticsMessage accumulatedMessage : messagesFromRank){
			if (accumulatedMessage.getNodeHash() == hash){
				// Accumulate stats for same node
				hasMessage = true;
				MoveStatistics[] newStats = msg.getNewStatistics();
				MoveStatistics[] accumulatedStats = accumulatedMessage.getNewStatistics();
				for (int i = 0; i < newStats.length; i++){
					accumulatedStats[i].addSelections(newStats[i].getSelections());
					accumulatedStats[i].addScore(newStats[i].getScore());
				}
			}
		}
		if (!hasMessage){
			messagesFromRank.add(msg);
		}
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(NodeStatisticsMessage.class, this::forwardStatistics)
				.match(StartShareMessage.class, this::shareNodeStatistics)
				.match(SearchRankToBroadcastRankStatShareMessage.class, this::addStatistics)
				.match(BroadcastWorkerArrayMessage.class, this::addWorkers)
				.match(AllMembersReadyMessage.class, this::sendReferences)
				.match(ActorRef.class, this::setParent)
				.matchAny(this::unhandled)
				.build();
	}

}
