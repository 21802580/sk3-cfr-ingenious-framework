package player.treesplit;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class TreesplitBroadcastRankInitMessage implements ClusterMemberInitMessage{
	private ActorRef[] searchRanks;
	private ActorRef[] broadcastRanks;
	private ActorRef nodeCounter;
	
	public TreesplitBroadcastRankInitMessage(ActorRef[] searchRanks, ActorRef[] broadcastRanks, ActorRef nodeCounter) {
		this.searchRanks = searchRanks;
		this.broadcastRanks = broadcastRanks;
		this.nodeCounter = nodeCounter;
	}
	
	public ActorRef[] getSearchRanks() {
		return searchRanks;
	}
	
	public ActorRef[] getBroadcastRanks() {
		return broadcastRanks;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
