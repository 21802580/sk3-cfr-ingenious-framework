package player.treesplit;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.*;
import player.core.node.MoveStatistics;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.transposition.ConcurrentHashMapTranspositionTable;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.NodeStatisticsMessage;
import player.root.PlayoutRequestMessage;
import player.tds.TDSReportMessage;
import player.tds.TDSSearchMessage;
import player.tds.TDSSelection;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class TreesplitSearchRank extends AbstractSearchRank {
	private int numPlayouts;
	private boolean running;
	private ActorRef[] allSearchRanks;
	private ActorRef broadcaster;
	private int rankIndex;
	private ActorRef workerRouter;
	private int numRootReports;
	private int numWorkers;
	private int numPlayoutsReceived;
	private Map<Long, TreeNode> duplicatedNodes;
	
	public TreesplitSearchRank(PlayerParameters playerParameters, TreesplitSearchRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		running = true;
		allSearchRanks = initMessage.getAllSearchRanks();
		rankIndex = initMessage.getRankIndex();
		numWorkers = Math.max(1, playerParameters.getNumThreads() - 1);
		duplicatedNodes = new ConcurrentHashMap<Long, TreeNode>();
		TreeNode rootNode = transpositionTable.makeNode(rootBoard, rootHash, playerParameters, true);
		duplicatedNodes.put(rootHash, rootNode); // Root is duplicated from the start
		SmallestMailboxPool pool = new SmallestMailboxPool(numWorkers);
		workerRouter = context().actorOf(pool.props(Props.create(TreesplitWorker.class, playerParameters, transpositionTable,
				allSearchRanks, rankIndex, duplicatedNodes)));
	}
	
	private void processSearchMessage(TDSSearchMessage msg){
		long hash = msg.getCurrentHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			TreeNode node = duplicatedNodes.get(hash);
			if (node == null){
				// Node is not duplicated
				if (homeProc == rankIndex){
					// We are at the home processor for the node -> forward job to router
					node = transpositionTable.get(hash);
					if (node != null){
						// Node in TT -> check for sync
						trySync(node, msg.getBoard());
					}
					workerRouter.tell(msg, getSelf());
				} else {
					// Forward job to home processor of the node
					allSearchRanks[homeProc].tell(msg, getSelf());
				}
			} else {
				// Node is duplicated -> check for sync and forward to worker
				trySync(node, msg.getBoard());
				workerRouter.tell(msg, getSelf());
			}
		}
	}
	
	private void trySync(TreeNode node, Board board){
		boolean shouldSync = false;
		int nodeVisits = node.getTotalVisits();
		if (nodeVisits >= playerParameters.getSchaefersNDup()){
			for (int i = 0; i < node.getNumChildren(); i++){
				int nDelta = node.getUnsyncedMoveStatistics()[i].getSelections();
				int syncMin = playerParameters.getSchaefersMinSync();
				int syncMax = playerParameters.getSchaefersMaxSync();
				double alpha = playerParameters.getSchaefersAlpha();
				double nSync = Math.min(syncMax, Math.max(syncMin, (alpha * node.getMoveSelections(i))));
				if (nDelta >= nSync) {
					shouldSync = true;
					break;
				}
			}
		}
		if (shouldSync){
			if (!duplicatedNodes.containsKey(node.getHash())){
				duplicatedNodes.put(node.getHash(), node);
			}
			MoveStatistics[] toSend = new MoveStatistics[node.getNumChildren()];
			for (int i = 0; i < node.getNumChildren(); i++){
				toSend[i] = node.getUnsyncedMoveStatistics()[i].copy();
			}
			broadcaster.tell(new SearchRankToBroadcastRankStatShareMessage(context().parent(), node.getHash(), new Board(board), toSend), getSelf());
			node.incorporateAndZeroUnsyncedStatistics();
		}
	}
	
	private void processReportMessage(TDSReportMessage msg){
		long hash = msg.getHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			if (msg.getHash() == rootHash){
				//numPlayouts++;
				if (homeProc == rankIndex){
					numRootReports++;
				}
			}
			if ((homeProc == rankIndex) || (duplicatedNodes.containsKey(hash))) {
				workerRouter.tell(msg, getSelf());
			} else {
				allSearchRanks[homeProc].tell(msg, getSelf());
			}
		}
	}
	
	private void synchroniseNodeStatistics(NodeStatisticsMessage msg){
		TreeNode n = duplicatedNodes.get(msg.getNodeHash());
		if (n == null){
			n = transpositionTable.makeNode(msg.getBoard(), msg.getNodeHash(), playerParameters, true);
			duplicatedNodes.put(msg.getNodeHash(), n);
		}
		n.incorporateRemoteStatistics(msg.getNewStatistics());
	}
	
	@Override
	protected TranspositionTable makeTranspositionTable() {
		return new ConcurrentHashMapTranspositionTable(playerParameters.getTtPower(), true, playerParameters.getPerTurnCapacityPower());
	}
	
	@Override
	protected void matchStarted(MatchStartMessage msg) {
		int numParallelSearches = playerParameters.getUctTreesplitOverload() * allSearchRanks.length * numWorkers;
		int numSearchesHere = numParallelSearches / allSearchRanks.length;
		for (int i = 0; i < numSearchesHere; i++) {
			Board b = new Board();
			List<TDSSelection> path = new ArrayList<TDSSelection>();
			getSelf().tell(new TDSSearchMessage(b, path, rootHash, 0), getSelf());
		}
	}
	
	@Override
	protected void turnStart() {
		numPlayouts = 0;
		numRootReports = 0;
	}
	
	private void requestPlayouts(){
		numPlayoutsReceived = 0;
		workerRouter.tell(new Broadcast(new PlayoutRequestMessage()), getSelf());
	}
	
	private void addPlayouts(TotalPlayoutMessage msg){
		numPlayouts += msg.numSimulations();
		if (++numPlayoutsReceived == numWorkers){
			sendSearchResult();
		}
	}
	
	private void sendSearchResult(){
		TreeNode node = duplicatedNodes.get(rootHash);
		if (node == null){
			node = transpositionTable.get(rootHash);
		}
		if (node == null){
			clusterManager.tell(new SearchResultMessage(null, numPlayouts, 0, 0), getSelf());
			return;
		}
		int moveIndex = MCTS.getHighestVisitChild(node);
		Move move = node.getMoveAt(moveIndex);
		clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, numRootReports), getSelf());
	}
	
	@Override
	protected void turnEnd() {
		requestPlayouts();
	}
	
	@Override
	protected void rootUpdated(Move m) {
		workerRouter.tell(new Broadcast(new ApplyMoveMessage(m)), getSelf());
		TreeNode rootNode = transpositionTable.makeNode(rootBoard, rootHash, playerParameters, true);
		duplicatedNodes.putIfAbsent(rootHash, rootNode); // Root is duplicated from the start
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof TDSSearchMessage) {
			processSearchMessage((TDSSearchMessage)msg);
		} else if (msg instanceof TDSReportMessage){
			processReportMessage((TDSReportMessage)msg);
		} else if (msg instanceof NodeStatisticsMessage) {
			synchroniseNodeStatistics((NodeStatisticsMessage) msg);
		} else if (msg instanceof SearchRankToBroadcastRankStatShareMessage){
			broadcaster.tell(msg, getSelf());
		} else if (msg instanceof BroadcasterSupplyMessage){
			broadcaster = getSender();
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		} else if (msg instanceof TotalPlayoutMessage){
			addPlayouts((TotalPlayoutMessage)msg);
		}
	}
	
	private int getRootHomeIndex(){
		return Zobrist.getHomeProcessorIndex(rootHash, allSearchRanks.length, transpositionTable.getCapacity());
	}
}
