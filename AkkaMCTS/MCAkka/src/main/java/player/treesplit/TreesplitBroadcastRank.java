package player.treesplit;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.Props;
import commandline.PlayerParameters;
import player.core.message.MatchStartMessage;
import player.root.NodeStatisticsMessage;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

public class TreesplitBroadcastRank extends AbstractActor{
	private ActorRef[] searchRanks;
	private ActorRef[] broadcastRanks;
	private ActorRef[] workers;
	private PlayerParameters playerParameters;
	private Cancellable sharingTimer;

	
	public TreesplitBroadcastRank(PlayerParameters playerParameters, TreesplitBroadcastRankInitMessage initMessage){
		this.playerParameters = playerParameters;
		searchRanks = initMessage.getSearchRanks();
		broadcastRanks = initMessage.getBroadcastRanks();
		initialiseWorkers();
		sendWorkerReferences();
	}
	
	private void initialiseWorkers(){
		int numWorkersRequired = (int)Math.ceil((double)searchRanks.length /
				(double)playerParameters.getSchaefersSearchRanksPerBroadcaster());

		workers = new ActorRef[numWorkersRequired];
		int currentSearchRankIndex = 0;
		for (int i = 0; i < numWorkersRequired - 1; i++){
			ActorRef[] searchRanksForThisWorker = new ActorRef[playerParameters.getSchaefersSearchRanksPerBroadcaster()];
			for (int j = 0; j < playerParameters.getSchaefersSearchRanksPerBroadcaster(); j++){
				searchRanksForThisWorker[j] = searchRanks[currentSearchRankIndex];
				currentSearchRankIndex++;
			}
			workers[i] = context().actorOf(Props.create(TreesplitBroadcastWorker.class, playerParameters, searchRanksForThisWorker, broadcastRanks));
		}
		
		int numSearchRanksForFinalWorker = searchRanks.length - currentSearchRankIndex;
		ActorRef[] searchRanksForFinalWorker = new ActorRef[numSearchRanksForFinalWorker];
		for (int i = 0; i < numSearchRanksForFinalWorker; i++){
			searchRanksForFinalWorker[i] = searchRanks[i + currentSearchRankIndex];
		}
		workers[numWorkersRequired - 1] = context().actorOf(Props.create(TreesplitBroadcastWorker.class, playerParameters, searchRanksForFinalWorker, broadcastRanks));
	}
	
	private void sendWorkerReferences() {
		for (ActorRef a : workers){
			a.tell(new BroadcastWorkerArrayMessage(workers), getSelf());
			a.tell(context().parent(), getSelf());
		}
	}
	
	private void matchStarted(MatchStartMessage msg){
		long tickMillis = (long)(playerParameters.getRootSharingInterval() * 1000);
		FiniteDuration tick = FiniteDuration.create(tickMillis, TimeUnit.MILLISECONDS);
		sharingTimer = context().system().scheduler().schedule(tick,
				tick, this::initiateBroadcast, context().system().dispatcher());
	}
	
	private void initiateBroadcast(){
		for (ActorRef a : workers){
			a.tell(new StartShareMessage(), getSelf());
		}
	}
	
	private void forwardToWorkers(NodeStatisticsMessage msg){
		for (ActorRef a : workers){
			a.tell(msg, getSelf());
		}
	}
	
	private void allMembersReady(AllMembersReadyMessage msg){
		for (ActorRef a : workers){
			a.tell(msg, getSelf());
		}
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MatchStartMessage.class, this::matchStarted)
				.match(NodeStatisticsMessage.class, this::forwardToWorkers)
				.match(AllMembersReadyMessage.class, this::allMembersReady)
				.matchAny(this::unhandled)
				.build();
	}
}
