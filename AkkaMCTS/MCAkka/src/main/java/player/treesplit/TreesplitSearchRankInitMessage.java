package player.treesplit;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class TreesplitSearchRankInitMessage implements ClusterMemberInitMessage {
	private int rankIndex;
	private ActorRef[] allSearchRanks;
	private ActorRef nodeCounter;
	
	public TreesplitSearchRankInitMessage(int rankIndex, ActorRef[] allSearchRanks, ActorRef nodeCounter) {
		this.rankIndex = rankIndex;
		this.allSearchRanks = allSearchRanks;
		this.nodeCounter = nodeCounter;
	}
	
	public int getRankIndex() {
		return rankIndex;
	}
	
	public ActorRef[] getAllSearchRanks() {
		return allSearchRanks;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
