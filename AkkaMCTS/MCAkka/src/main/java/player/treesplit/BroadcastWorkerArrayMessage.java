package player.treesplit;

import akka.actor.ActorRef;
import player.core.message.Message;

public class BroadcastWorkerArrayMessage implements Message {
	private ActorRef[] broadcasterWorkers;
	
	public BroadcastWorkerArrayMessage(ActorRef[] broadcasterWorkers) {
		this.broadcasterWorkers = broadcasterWorkers;
	}
	
	public ActorRef[] getBroadcasterWorkers() {
		return broadcasterWorkers;
	}
}
