package player.dfuct;

import commandline.PlayerParameters;
import loa.gamelogic.Colour;
import loa.gamelogic.Move;
import player.core.node.MoveStatistics;
import player.core.node.VolatileMoveStatistics;

public class DFUCTStackNode {
	
	private long nodeHash;
	private int nodeVisits;
	private int selectedMoveIndex;
	private Colour playerToMove;
	private Move selectedMove;
	private MoveStatistics selectedMoveStatistics;
	private MoveStatistics secondBestMoveStatistics;
	private double selectedHeuristicValue;
	private double secondBestHeuristicValue;
	private MoveStatistics statisticsToBeAdded;
	
	public DFUCTStackNode(long nodeHash, int nodeVisits, DFUCTSelectionResult selectionResult, Colour playerToMove, Move selectedMove) {
		this.nodeHash = nodeHash;
		this.nodeVisits = nodeVisits;
		selectedMoveIndex = selectionResult.getSelectedMoveIndex();
		selectedMoveStatistics = selectionResult.getSelectedMoveStats();
		secondBestMoveStatistics = selectionResult.getSecondBestMoveStats();
		this.playerToMove = playerToMove;
		selectedHeuristicValue = selectionResult.getSelectedHeuristicValue();
		secondBestHeuristicValue = selectionResult.getSecondBestHeuristicValue();
		this.selectedMove = selectedMove;
		statisticsToBeAdded = new VolatileMoveStatistics();
	}
	
	// Update the reward of the stack entry and return true if the best move has changed
	public boolean addReward(double[] rewards, PlayerParameters playerParameters){
		double playerReward = rewards[playerToMove.ordinal() - 1];
		int windowBeforeUpdate = getUnpruningWindow(playerParameters);
		statisticsToBeAdded.addSelections(1);
		statisticsToBeAdded.addScore(playerReward);
		int windowAfterUpdate = getUnpruningWindow(playerParameters);
		int totalNodeVisits = nodeVisits + statisticsToBeAdded.getSelections();
		int totalSelectedMoveVisits = selectedMoveStatistics.getSelections() + statisticsToBeAdded.getSelections();
		double totalSelectedMoveScore = selectedMoveStatistics.getScore() + statisticsToBeAdded.getScore();
		if (secondBestMoveStatistics == null){
			// No other legal move at this node
			return false;
		}
		if (!playerParameters.isProgressiveUnpruning()){
			windowAfterUpdate = 2;
		} else if (windowAfterUpdate > windowBeforeUpdate){
			return true;
		}
		double selectedUCB = getUCB(totalNodeVisits, totalSelectedMoveVisits, totalSelectedMoveScore, selectedHeuristicValue, playerParameters);
		double otherUCB = getUCB(totalNodeVisits, secondBestMoveStatistics.getSelections(), secondBestMoveStatistics.getScore(), secondBestHeuristicValue, playerParameters);
		return otherUCB > selectedUCB && windowAfterUpdate > 1;
	}
	
	private int getUnpruningWindow(PlayerParameters playerParameters){
		int totalNodeVisits = nodeVisits + statisticsToBeAdded.getSelections();
		return playerParameters.getInitialWindowSize() + (int) (Math.log(totalNodeVisits) / (Math.log(playerParameters.getUnpruningRate())));
	}
	
	private double getUCB(int nodeVisits, int childSelections, double childScore, double heuristicValue, PlayerParameters playerParameters){
		double C = playerParameters.getUCBConstant();
		if (childSelections == 0){
			if (playerParameters.isFPU()){
				return playerParameters.getFpu();
			} else {
				return Integer.MAX_VALUE;
			}
		}
		double baseUCB = (childScore / childSelections) + (C * Math.sqrt(Math.log(nodeVisits) / childSelections));
		double biasTerm = playerParameters.getPbWeight() * (heuristicValue / (childSelections + 1));
		return baseUCB + biasTerm;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
	
	public int getSelectedMoveIndex() {
		return selectedMoveIndex;
	}
	
	public double getAccumulatedReward(){
		return statisticsToBeAdded.getScore();
	}
	
	public int getAccumulatedVisits(){
		return statisticsToBeAdded.getSelections();
	}
	
	public Move getSelectedMove() {
		return selectedMove;
	}
	
	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append("=== Stack Node ===\n");
		b.append("hash = ").append(nodeHash).append("\n");
		b.append("player to move: ").append(playerToMove).append("\n");
		b.append("visits = ").append(nodeVisits).append("\n");
		b.append("selected move = ").append(selectedMove).append("\n");
		b.append("selected move visits = ").append(selectedMoveStatistics.getSelections()).append("\n");
		b.append("selected move score = ").append(selectedMoveStatistics.getScore()).append("\n");
		b.append("accumulated visits = ").append(statisticsToBeAdded.getSelections()).append("\n");
		b.append("accumulated score = ").append(statisticsToBeAdded.getScore()).append("\n");
		b.append("==================\n");
		return b.toString();
	}
	
}
