package player.dfuct;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Move;
import player.core.actor.ClusterManager;
import player.core.message.ApplyMoveMessage;
import player.core.message.GenMoveMessage;
import player.core.message.SearchResultMessage;

public class DFUCTClusterManager extends ClusterManager {
	private int numResultsReceived;
	private Move selectedMove;
	private int numPlayouts;
	private int numRootReports;
	
	public DFUCTClusterManager(PlayerParameters playerParameters) {
		super(playerParameters);
	}
	
	@Override
	public SearchResultMessage searchResultReceived(SearchResultMessage msg) {
		numPlayouts += msg.getTotalPlayouts();
		if (msg.getSelectedMove() != null) {
			selectedMove = msg.getSelectedMove();
			numRootReports = msg.getReportsAtRoot();
		}
		if (++numResultsReceived == allMembers.length) {
			for (ActorRef searchRank : allMembers) {
				searchRank.tell(new ApplyMoveMessage(selectedMove), getSelf());
			}
			return new SearchResultMessage(selectedMove, numPlayouts, 0, numRootReports);
		}
		return null;
	}
	
	@Override
	public void clusterMemberJoined(ActorRef member) {
		if (numRegistrationsReceived == playerParameters.getNumNodes()){
			for (int i = 0; i < allMembers.length; i++){
				ActorRef searchRank = allMembers[i];
				searchRank.tell(new DFUCTSearchRankInitMessage(allMembers, i, nodeCounter), getSelf());
			}
		}
	}
	
	@Override
	protected void genMove(GenMoveMessage msg) {
		numResultsReceived = 0;
		numPlayouts = 0;
		numRootReports = 0;
		for (int i = 0; i < allMembers.length; i++){
			allMembers[i].tell(msg, getSelf());
		}
	}
	
	@Override
	protected void allMembersInitialised() {
	
	}
}
