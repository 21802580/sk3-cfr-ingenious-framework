package player.dfuct;

import loa.gamelogic.Move;
import player.core.message.Message;

public class SearchResultSentMessage implements Message {
	private Move move;
	
	public SearchResultSentMessage(Move move) {
		this.move = move;
	}
	
	public Move getMove() {
		return move;
	}
}
