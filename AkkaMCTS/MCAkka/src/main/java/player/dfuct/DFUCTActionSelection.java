package player.dfuct;

import commandline.PlayerParameters;
import player.core.node.MoveStatistics;
import player.core.node.TreeNode;
import player.core.search.MCTS;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class DFUCTActionSelection {

	public static DFUCTSelectionResult selectActions(TreeNode node, PlayerParameters playerParameters){
		node.addVisits(1);
		if (node.getTotalVisits() == 1) return null;
		int[] twoBest = getTwoBestChildren(node, playerParameters);
		node.addMoveSelections(twoBest[0], 1);
		MoveStatistics selectedMoveStats = node.getUnsyncedMoveStatistics()[twoBest[0]].copy();
		MoveStatistics secondBestMoveStats = twoBest[1] < 0 ? null : node.getUnsyncedMoveStatistics()[twoBest[1]].copy();
		double selectedHeuristicValue = node.getMoveHeuristicValue(twoBest[0]);
		double secondBestHeuristicValue = twoBest[1] < 0 ? 0 : node.getMoveHeuristicValue(twoBest[1]);
		return new DFUCTSelectionResult(twoBest[0], selectedMoveStats, selectedHeuristicValue, secondBestMoveStats, secondBestHeuristicValue);
	}
	
	
	private static int[] getTwoBestChildren(TreeNode n, PlayerParameters playerParameters){
		int[] children = new int[2];
		int windowSize = n.getWindowSize();
		Map<Double, List<Integer>> indexMap = new HashMap<Double, List<Integer>>();
		double bestUCB = Double.MIN_VALUE;
		double secondBestUCB = Double.MIN_VALUE;
		for (int i = 0; i < windowSize && i < n.getNumChildren(); i++){
			double ucb = MCTS.getUCB(n, i, playerParameters);
			List<Integer> indices = indexMap.computeIfAbsent(ucb, k -> new ArrayList<Integer>());
			indices.add(i);
			if (ucb > bestUCB){
				secondBestUCB = bestUCB;
				bestUCB = ucb;
			} else if (ucb > secondBestUCB){
				secondBestUCB = ucb;
			}
		}
		List<Integer> bestIndices = indexMap.get(bestUCB);
		if (n.getNumChildren() == 1){
			// Only one legal move
			children[0] = bestIndices.remove(ThreadLocalRandom.current().nextInt(bestIndices.size()));
			children[1] = -1;
		} else if (windowSize == 1){
			children[0] = bestIndices.remove(ThreadLocalRandom.current().nextInt(bestIndices.size()));
			children[1] = children[0] + 1; // Next move to be unpruned
		} else if (bestIndices.size() > 1){
			children[0] = bestIndices.remove(ThreadLocalRandom.current().nextInt(bestIndices.size()));
			children[1] = bestIndices.remove(ThreadLocalRandom.current().nextInt(bestIndices.size()));
		} else {
			children[0] = bestIndices.get(0);
			List<Integer> secondBestIndices = indexMap.get(secondBestUCB);
			children[1] = secondBestIndices.get(ThreadLocalRandom.current().nextInt(secondBestIndices.size()));
		}
		return children;
	}
}
