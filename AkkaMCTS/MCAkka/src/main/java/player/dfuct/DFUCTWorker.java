package player.dfuct;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.ApplyMoveMessage;
import player.core.message.NodeAddedMessage;
import player.core.message.TotalPlayoutMessage;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;

import java.util.ArrayDeque;
import java.util.Deque;

public class DFUCTWorker extends AbstractActor {
	private PlayerParameters playerParameters;
	private TranspositionTable transpositionTable;
	private ActorRef[] allSearchRanks;
	private Board rootBoard;
	private long rootHash;
	private int rankIndex;
	private int numSims;
	
	public DFUCTWorker(PlayerParameters playerParameters, TranspositionTable transpositionTable, ActorRef[] allSearchRanks, int rankIndex) {
		this.playerParameters = playerParameters;
		this.transpositionTable = transpositionTable;
		this.allSearchRanks = allSearchRanks;
		this.rankIndex = rankIndex;
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(DFUCTSearchMessage.class, this::processSearchMessage)
				.match(DFUCTReportMessage.class, this::processReportMessage)
				.match(ApplyMoveMessage.class, this::updateRoot)
				.match(PlayoutRequestMessage.class, this::sendPlayouts)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void sendPlayouts(PlayoutRequestMessage msg) {
		getSender().tell(new TotalPlayoutMessage(numSims), getSelf());
	}
	
	
	private void processSearchMessage(DFUCTSearchMessage msg) {
		Deque<DFUCTStackNode> stack = msg.getStack();
		Board board = msg.getBoard();
		long hash = msg.getNodeHash();
		if (board.getWinner() != null) {
			// Terminal state -> end descent
			simulateAndSendReports(board, stack, hash);
			return;
		}
		TreeNode node = transpositionTable.putIfAbsent(hash, board, playerParameters);
		if (node == null) {
			// TT full -> end descent
			simulateAndSendReports(board, stack, hash);
			return;
		} else if (node.getTotalVisits() == 0 && (stack.isEmpty())) {
			// Add one visit so that a simulation is not performed at the root
			node.addVisits(1);
		}
		DFUCTSelectionResult s = DFUCTActionSelection.selectActions(node, playerParameters);
		if (s == null) {
			// First visit for node -> end descent
			allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
			simulateAndSendReports(board, stack, hash);
			return;
		}
		addSelectionAndSendSearch(node, board, stack, hash, s);
	}
	
	private void processReportMessage(DFUCTReportMessage msg) {
		TreeNode node = transpositionTable.get(msg.getHash());
		if (node != null) {
			// Only add reward if node has not been overwritten
			node.addMoveSelections(msg.getMoveIndex(), msg.getTotalSelections() - 1);
			node.addVisits(msg.getTotalSelections() - 1);
			node.addChildScore(msg.getMoveIndex(), msg.getAccumulatedReward());
		}
	}
	
	private void addSelectionAndSendSearch(TreeNode node, Board board, Deque<DFUCTStackNode> stack, long currentHash, DFUCTSelectionResult newSelection) {
		Move m = node.getMoveAt(newSelection.getSelectedMoveIndex());
		long newHash = Zobrist.getChildHash(currentHash, m, board.getCurrentPlayer());
		board.applyMove(m);
		DFUCTStackNode s = new DFUCTStackNode(currentHash, node.getTotalVisits(), newSelection, node.getPlayerToMove(), m);
		stack.push(s);
		allSearchRanks[rankIndex].tell(new DFUCTSearchMessage(stack, board, newHash), getSelf());
	}
	
	private void simulateAndSendReports(Board board, Deque<DFUCTStackNode> stack, long hash) {
		if (hash == rootHash) {
			// Don't simulate from the root
			startNewSearch();
			return;
		}
		numSims++;
		Board b = new Board(board);
		double[] rewards = MCTS.randomPlayout(b);
		int newSearchIndex = 0; // Index of the node from which selection should resume
		int currentIndex = 0;
		for (DFUCTStackNode node : stack) {
			if (node.addReward(rewards, playerParameters)) {
				newSearchIndex = currentIndex;
			}
//			if (node.getAccumulatedVisits() >= allSearchRanks.length){
//				maxVisitsAccumulated = true;
//				break;
//			}
			currentIndex++;
		}
//		if (maxVisitsAccumulated) {
//			newSearchIndex = stack.size() - 1;
//		}
		for (int i = 0; i <= newSearchIndex; i++) {
			// Send report messages to all nodes up to newSearch Index
			DFUCTStackNode node = stack.pop();
			DFUCTReportMessage msg = new DFUCTReportMessage(node.getNodeHash(), node.getSelectedMoveIndex(), node.getAccumulatedReward(), node.getAccumulatedVisits());
			allSearchRanks[rankIndex].tell(msg, getSelf());
			board.undoMove(node.getSelectedMove());
			if (stack.size() == 0) {
				// Backpropagating to the root -> Send new search job to the current root
				startNewSearch();
				break;
			} else if (i == newSearchIndex) {
				DFUCTSearchMessage sm = new DFUCTSearchMessage(stack, board, node.getNodeHash());
				allSearchRanks[rankIndex].tell(sm, getSelf());
			}
		}
	}
	
	private void startNewSearch() {
		Board newBoard = new Board(rootBoard);
		Deque<DFUCTStackNode> newStack = new ArrayDeque<DFUCTStackNode>();
		allSearchRanks[rankIndex].tell(new DFUCTSearchMessage(newStack, newBoard, rootHash), getSelf());
	}
	
	private void updateRoot(ApplyMoveMessage msg) {
		numSims = 0;
		rootHash = Zobrist.getChildHash(rootHash, msg.getMove(), rootBoard.getCurrentPlayer());
		rootBoard.applyMove(msg.getMove());
	}
}

//	private class CallableReport implements Callable<Void> {
//		private DFUCTReportMessage msg;
//
//		private CallableReport(DFUCTReportMessage msg) {
//			this.msg = msg;
//		}
//
//		@Override
//		public Void call() throws Exception {
//			TreeNode node = transpositionTable.get(msg.getHash());
//			if (node != null){
//				// Only add reward if node has not been overwritten
//				node.addMoveSelections(msg.getMoveIndex(), msg.getTotalSelections() - 1);
//				node.addVisits(msg.getTotalSelections() - 1);
//				node.addChildScore(msg.getMoveIndex(), msg.getAccumulatedReward());
//			}
//			return null;
//		}
//	}
//
//	private class CallableSearch implements Callable<Boolean> {
//		private DFUCTSearchMessage msg;
//
//		private CallableSearch(DFUCTSearchMessage msg) {
//			this.msg = msg;
//		}
//
//		@Override
//		public Boolean call() {
//
//			Deque<DFUCTStackNode> stack = msg.getStack();
//			Board board = msg.getBoard();
//			long hash = msg.getNodeHash();
//			if (board.getWinner() != null){
//				// Terminal state -> end descent
//				simulateAndSendReports(board, stack, "Terminal state");
//				return Boolean.TRUE;
//			}
//			TreeNode node = transpositionTable.putIfAbsent(hash, board, playerParameters);
//			if (node == null) {
//				// TT full -> end descent
//				simulateAndSendReports(board, stack, "Full TT");
//				return Boolean.TRUE;
//			} else if (node.getTotalVisits() == 0 && (stack.isEmpty())){
//				// Add one visit so that a simulation is not performed at the root
//				node.addVisits(1);
//			}
//			DFUCTSelectionResult s = DFUCTActionSelection.selectActions(node, playerParameters);
//			if (s == null){
//				// First visit for node -> end descent
//				allSearchRanks[rankIndex].tell(new NodeAddedMessage(node.getHash()), getSelf());
//				simulateAndSendReports(board, stack, "First node visit");
//				return Boolean.TRUE;
//			}
//			addSelectionAndSendSearch(node, board, stack, hash, s);
//			return Boolean.FALSE;
//		}
//	}
//}

//mpirun -np 1 java -XX:+UseG1GC -jar clustermatch.jar -seedhost 127.0.0.1 -seedport 61234 -name dfuct_1_1000_a -type dfuct -timeout 1000 -colour BLACK -nodes 1 -nworkers 1 _ -seedhost 127.0.0.1 -seedport 61235 -name serial_1_1000_b -type serial -timeout 1000 -colour WHITE -nodes 1 -nworkers 1 : -np 2 java -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61234 -name dfuct_1_1000_a -type dfuct -timeout 1000 -colour BLACK -nodes 1 -nworkers 1 : -np 2 java -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61235 -name serial_1_1000_b -type serial -timeout 1000 -colour WHITE -nodes 1 -nworkers 1
