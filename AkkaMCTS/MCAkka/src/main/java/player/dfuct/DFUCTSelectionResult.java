package player.dfuct;

import player.core.node.MoveStatistics;

public class DFUCTSelectionResult {
	private int selectedMoveIndex;
	private MoveStatistics selectedMoveStats;
	private double selectedHeuristicValue;
	private MoveStatistics secondBestMoveStats;
	private double secondBestHeuristicValue;
	
	public DFUCTSelectionResult(int selectedMoveIndex, MoveStatistics selectedMoveStats, double selectedHeuristicValue, MoveStatistics secondBestMoveStats, double secondBestHeuristicValue) {
		this.selectedMoveIndex = selectedMoveIndex;
		this.selectedMoveStats = selectedMoveStats;
		this.selectedHeuristicValue = selectedHeuristicValue;
		this.secondBestMoveStats = secondBestMoveStats;
		this.secondBestHeuristicValue = secondBestHeuristicValue;
	}
	
	public int getSelectedMoveIndex() {
		return selectedMoveIndex;
	}
	
	public MoveStatistics getSelectedMoveStats() {
		return selectedMoveStats;
	}
	
	public MoveStatistics getSecondBestMoveStats() {
		return secondBestMoveStats;
	}
	
	public double getSelectedHeuristicValue() {
		return selectedHeuristicValue;
	}
	
	public double getSecondBestHeuristicValue() {
		return secondBestHeuristicValue;
	}
	
}
