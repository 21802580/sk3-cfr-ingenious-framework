package player.dfuct;

import player.core.message.Message;

public class DFUCTReportMessage implements Message {
	private long hash;
	private int moveIndex;
	private double accumulatedReward;
	private int totalSelections;
	
	public DFUCTReportMessage(long hash, int moveIndex, double accumulatedReward, int totalSelections) {
		this.hash = hash;
		this.moveIndex = moveIndex;
		this.accumulatedReward = accumulatedReward;
		this.totalSelections = totalSelections;
	}
	
	public long getHash() {
		return hash;
	}
	
	public double getAccumulatedReward() {
		return accumulatedReward;
	}
	
	public int getMoveIndex() {
		return moveIndex;
	}
	
	public int getTotalSelections() {
		return totalSelections;
	}
}
