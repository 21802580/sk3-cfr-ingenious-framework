package player.dfuct;

import loa.gamelogic.Board;
import player.core.message.Message;

import java.util.Deque;

// Size = ~1.3kb

public class DFUCTSearchMessage implements Message{
	private Deque<DFUCTStackNode> stack;
	private Board board;
	private long nodeHash;
	
	public DFUCTSearchMessage(Deque<DFUCTStackNode> stack, Board board , long nodeHash) {
		this.stack = stack;
		this.board = board;
		this.nodeHash = nodeHash;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public Deque<DFUCTStackNode> getStack() {
		return stack;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
}
