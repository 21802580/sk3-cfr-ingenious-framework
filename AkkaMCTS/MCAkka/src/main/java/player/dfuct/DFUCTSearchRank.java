package player.dfuct;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.transposition.ConcurrentHashMapTranspositionTable;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;

import java.util.ArrayDeque;
import java.util.Deque;

public class DFUCTSearchRank extends AbstractSearchRank {
	private int numPlayouts;
	private int numRootReports;
	private boolean running;
	private ActorRef[] allSearchRanks;
	private int rankIndex;
	private ActorRef workerRouter;
	private int numWorkers;
	private int numPlayoutsReceived;
	
	public DFUCTSearchRank(PlayerParameters playerParameters, DFUCTSearchRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		running = true;
		numWorkers = Math.max(1, playerParameters.getNumThreads() - 1);
		SmallestMailboxPool pool = new SmallestMailboxPool(numWorkers);
		allSearchRanks = initMessage.getAllSearchRanks();
		rankIndex = initMessage.getRankIndex();
		workerRouter = context().actorOf(pool.props(Props.create(DFUCTWorker.class, playerParameters, transpositionTable, allSearchRanks, rankIndex)));
	}
	
	private void processSearchMessage(DFUCTSearchMessage msg){
		long hash = msg.getNodeHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			if (homeProc == rankIndex) {
				workerRouter.tell(msg, getSelf());
			} else {
				allSearchRanks[homeProc].tell(msg, getSelf());
			}
		}
	}
	
	private void processReportMessage(DFUCTReportMessage msg){
		long hash = msg.getHash();
		int homeProc = Zobrist.getHomeProcessorIndex(hash, allSearchRanks.length, transpositionTable.getCapacity());
		if (running) {
			if (homeProc == rankIndex) {
				workerRouter.tell(msg, getSelf());
				if (msg.getHash() == rootHash) {
					numRootReports++;
				}
			} else {
				allSearchRanks[homeProc].tell(msg, getSelf());
			}
		}
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof DFUCTSearchMessage) {
			processSearchMessage((DFUCTSearchMessage)msg);
		} else if (msg instanceof  DFUCTReportMessage){
			processReportMessage((DFUCTReportMessage)msg);
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		} else if (msg instanceof TotalPlayoutMessage){
			addPlayouts((TotalPlayoutMessage)msg);
		}
	}
	
	@Override
	protected void matchStarted(MatchStartMessage message) {
		if (getRootHomeIndex() == rankIndex) {
			int numParallelSearches = playerParameters.getDfUctOverload() * allSearchRanks.length * numWorkers;
			for (int i = 0; i < numParallelSearches; i++) {
				Board b = new Board();
				Deque<DFUCTStackNode> stack = new ArrayDeque<DFUCTStackNode>();
				getSelf().tell(new DFUCTSearchMessage(stack, b, rootHash), getSelf());
			}
		}
	}
	
	@Override
	protected void turnStart() {
		numPlayouts = 0;
		numRootReports = 0;
	}
	
	private void requestPlayouts(){
		numPlayoutsReceived = 0;
		workerRouter.tell(new Broadcast(new PlayoutRequestMessage()), getSelf());
	}
	
	private void addPlayouts(TotalPlayoutMessage msg){
		numPlayouts += msg.numSimulations();
		if (++numPlayoutsReceived == numWorkers){
			sendSearchResult();
		}
	}
	
	private void sendSearchResult(){
		if (getRootHomeIndex() != rankIndex){
			clusterManager.tell(new SearchResultMessage(null, numPlayouts, 0, 0), getSelf());
		} else {
//			System.out.println("Fetching move for rootHash: " + rootHash);
			TreeNode node = transpositionTable.get(rootHash);
			int moveIndex = MCTS.getHighestVisitChild(node);
			Move move = node.getMoveAt(moveIndex);
			clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, numRootReports), getSelf());
		}
	}
	
	@Override
	protected void turnEnd() {
		requestPlayouts();
	}
	
	@Override
	protected void rootUpdated(Move m) {
//		System.out.println("rootUpdated: " + rootHash);
		workerRouter.tell(new Broadcast(new ApplyMoveMessage(m)), getSelf());
	}
	
	@Override
	protected TranspositionTable makeTranspositionTable() {
		return new ConcurrentHashMapTranspositionTable(playerParameters.getTtPower(), false, playerParameters.getPerTurnCapacityPower());
//		return new ConcurrentReplacementTranspositionTable(playerParameters.getTtPower(), false);
	}
	
	
	private int getRootHomeIndex(){
		return Zobrist.getHomeProcessorIndex(rootHash, allSearchRanks.length, transpositionTable.getCapacity());
	}
}
