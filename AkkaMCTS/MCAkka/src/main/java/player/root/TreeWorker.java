package player.root;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.Selection;
import player.core.search.TreeDescent;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;

import java.util.List;


public class TreeWorker extends AbstractActor{
	private TranspositionTable transpositionTable;
	private int totalPlayouts;
	private Board rootState;
	private long rootHash;
	private PlayerParameters playerParameters;
	private boolean running;
	private ActorRef searchRank;
	private int numNodes;

	public TreeWorker(TranspositionTable transpositionTable, PlayerParameters playerParameters, ActorRef searchRank){
		this.transpositionTable = transpositionTable;
		this.playerParameters = playerParameters;
		totalPlayouts = 0;
		rootState = new Board();
		rootHash = Zobrist.getBoardHash(rootState);
		running = true;
		this.searchRank = searchRank;
		numNodes = 0;
	}


	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MCTSIterationMessage.class, this::mctsIteration)
				.match(TurnStartMessage.class, this::zeroPlayouts)
				.match(ApplyMoveMessage.class, this::applyMove)
				.match(PlayoutRequestMessage.class, this::sendPlayouts)
				.match(MatchOverMessage.class, this::matchOver)
				.matchAny(this::unhandled).build();
	}

	private void mctsIteration(MCTSIterationMessage message){
		Board board = new Board(rootState);
		TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
		if (descent.getAddedNode() != -1){
			searchRank.tell(new NodeAddedMessage(descent.getAddedNode()), getSelf());
		}
		double[] rewards = MCTS.randomPlayout(board);
		MCTS.backupAllRewards(descent.getPath(), rewards, transpositionTable);
		totalPlayouts++;
		if (running) getSelf().tell(message, getSelf());
	}

	private Board descendTree(List<Selection> path){
		Board board = new Board(rootState);
		long hash = rootHash;
		while (board.getWinner() == null) {
			TreeNode node = transpositionTable.putIfAbsent(hash, board, playerParameters);
			if (node == null) break; // TT full -> do simulation
			Selection s = MCTS.actionSelection(node, playerParameters);
			if (s == null) break; // First visit for node -> do simulation
			path.add(s);
			Move m = node.getMoveAt(s.getSelectedMoveIndex());
			hash = Zobrist.getChildHash(hash, m, board.getCurrentPlayer());
			board.applyMove(m);
		}
		return board;
	}

	private void backupReward(List<Selection> path, double[] rewards){
		for (Selection s : path){
			TreeNode n = transpositionTable.get(s.getNodeHash());
			MCTS.updateReward(n, s.getSelectedMoveIndex(), rewards);
		}
	}
	
	private void applyMove(ApplyMoveMessage message){
		Move m = message.getMove();
		rootHash = Zobrist.getChildHash(rootHash, m, rootState.getCurrentPlayer());
		rootState.applyMove(m);
	}

	private void zeroPlayouts(TurnStartMessage msg){
		totalPlayouts = 0;
	}

	private void sendPlayouts(PlayoutRequestMessage msg){
		getSender().tell(new TotalPlayoutMessage(totalPlayouts), getSelf());
	}
	
	private void matchOver(MatchOverMessage msg){
		running = false;
	}
}
