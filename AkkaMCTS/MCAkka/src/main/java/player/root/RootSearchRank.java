package player.root;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.MatchOverMessage;
import player.core.message.MatchStartMessage;
import player.core.message.Message;
import player.core.node.MoveStatistics;
import player.core.node.TreeNode;
import player.core.transposition.*;
import player.treesplit.StartShareMessage;
import scala.concurrent.duration.FiniteDuration;

import java.util.*;
import java.util.concurrent.TimeUnit;

public abstract class RootSearchRank extends AbstractSearchRank {
	private Cancellable sharingTimer;
	private ActorRef[] allRanks;
	private int rankIndex;
	
	public RootSearchRank(PlayerParameters playerParameters, RootSearchRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		allRanks = initMessage.getAllSearchRanks();
		rankIndex = initMessage.getRankIndex();
	}
	
	// TODO: Gross(ish).
	private void shareNodeStatistics(){
		int maxDepth = playerParameters.getRootSharingMaxPly();
		int currentDepth = 0;
		Set<Long> alreadyChecked = new HashSet<Long>();
		Queue<NodeBoardPair> nodeQueue = new LinkedList<NodeBoardPair>();
		TreeNode rootNode = transpositionTable.get(rootHash);
		if ((rootNode == null) || maxDepth < 0) return;
		nodeQueue.add(new NodeBoardPair(rootNode, new Board(rootBoard)));
		nodeQueue.add(null);
		int totalRootPlayouts = rootNode.getTotalVisits();
		while(!nodeQueue.isEmpty()){
			NodeBoardPair currentNBP = nodeQueue.poll();
			if (currentNBP == null){
				if (++currentDepth > maxDepth) break;
				nodeQueue.add(null);
				if (nodeQueue.peek() == null) break;
				else continue;
			}
			TreeNode currentNode = currentNBP.node;
			if (!alreadyChecked.add(currentNode.getHash())) {
				continue;
			}
			tryNodeBroadcast(currentNBP, totalRootPlayouts);
			for (int i = 0; i < currentNode.getNumChildren(); i++) {
				Move m = currentNode.getMoveAt(i);
				long childHash = Zobrist.getChildHash(currentNode.getHash(), m, currentNode.getPlayerToMove());
				TreeNode child = transpositionTable.get(childHash);
				if (child != null) {
					Board childBoard = new Board(currentNBP.board);
					childBoard.applyMove(m);
					nodeQueue.add(new NodeBoardPair(child, childBoard));
				}
			}
		}
	}
	
	private void tryNodeBroadcast(NodeBoardPair nbp, int rootVisits){
		TreeNode node = nbp.node;
		double nodeContribution = (double)node.getTotalVisits() / (double)rootVisits;
		if (nodeContribution >= playerParameters.getRootSharingMinimumContribution()){
			MoveStatistics[] moveStats = node.getUnsyncedMoveStatistics();
			MoveStatistics[] toSend = new MoveStatistics[moveStats.length];
			for (int i = 0; i < moveStats.length; i++){
				toSend[i] = moveStats[i].copy();
			}
			broadcastStatistics(new NodeStatisticsMessage(getSelf(), node.getHash(), nbp.board, toSend));
			node.incorporateAndZeroUnsyncedStatistics();
		}
	}
	
	private void incorporateStatistics(NodeStatisticsMessage msg){
		TreeNode n = transpositionTable.get(msg.getNodeHash());
		if (n == null){
			n = transpositionTable.putIfAbsent(msg.getNodeHash(), msg.getBoard(), playerParameters);
		}
		if (n != null){
			// Only add the node if TT is not full
			n.incorporateRemoteStatistics(msg.getNewStatistics());
		}
	}
	
	private void broadcastStatistics(NodeStatisticsMessage msg){
		for (int i = 0; i < allRanks.length; i++){
			if (i != rankIndex){
				allRanks[i].tell(msg, getSelf());
			}
		}
	}
	
	@Override
	protected TranspositionTable makeTranspositionTable() {
//		return new ConcurrentReplacementTranspositionTable(playerParameters.getTtPower(), true);
		return new ConcurrentHashMapTranspositionTable(playerParameters.getTtPower(), true, playerParameters.getPerTurnCapacityPower());
	}
	
	@Override
	protected void matchStarted(MatchStartMessage msg) {
		matchStarted();
		long tickMillis = (long)(playerParameters.getRootSharingInterval() * 1000);
		FiniteDuration tick = FiniteDuration.create(tickMillis, TimeUnit.MILLISECONDS);
		sharingTimer = context().system().scheduler().schedule(tick,
				tick, this::sendShareMessage, context().system().dispatcher());
	}
	
	private void sendShareMessage(){
		getSelf().tell(new StartShareMessage(), getSelf());
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg){
		sharingTimer.cancel();
		matchOver();
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof NodeStatisticsMessage) {
			incorporateStatistics((NodeStatisticsMessage) msg);
		} else if (msg instanceof StartShareMessage){
			shareNodeStatistics();
		} else {
			processRootImplementationSpecificMessage(msg);
		}
	}
	
	private class NodeBoardPair {
		public TreeNode node;
		public Board board;
		
		public NodeBoardPair(TreeNode node, Board board){
			this.node = node;
			this.board = board;
		}
	}
	
	protected abstract void matchStarted();
	
	protected abstract void matchOver();
	
	protected abstract void processRootImplementationSpecificMessage(Message msg);
}
