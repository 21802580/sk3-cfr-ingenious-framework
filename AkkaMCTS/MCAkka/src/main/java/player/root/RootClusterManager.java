package player.root;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Move;
import player.core.actor.ClusterManager;
import player.core.message.ApplyMoveMessage;
import player.core.message.GenMoveMessage;
import player.core.message.SearchResultMessage;

import java.util.HashMap;
import java.util.Map;

public class RootClusterManager extends ClusterManager {
	private int numPendingResults;
	private HashMap<Move, Integer> votes;
	private int receivedPlayouts;
	
	public RootClusterManager(PlayerParameters playerParameters){
		super(playerParameters);
	}
	
	@Override
	public SearchResultMessage searchResultReceived(SearchResultMessage msg) {
		receivedPlayouts += msg.getTotalPlayouts();
		Move m = msg.getSelectedMove();
		if (!votes.containsKey(m)){
			votes.put(m, 1);
		} else {
			votes.put(m, votes.get(m) + 1);
		}
		if (--numPendingResults == 0){
			int mostVotes = 0;
			Move chosenMove = null;
			for (Map.Entry entry : votes.entrySet()){
				if ((int)entry.getValue() > mostVotes){
					mostVotes = (int)entry.getValue();
					chosenMove = (Move)entry.getKey();
				}
			}
			for (int i = 0; i < allMembers.length; i++){
				allMembers[i].tell(new ApplyMoveMessage(chosenMove), getSelf());
			}
			return new SearchResultMessage(chosenMove, receivedPlayouts, 0, 0);
		} else {
			return null;
		}
	}
	
	@Override
	public void clusterMemberJoined(ActorRef member) {
		if (numRegistrationsReceived == playerParameters.getNumNodes()){
			for (int i = 0; i < allMembers.length; i++){
				ActorRef searchRank = allMembers[i];
				searchRank.tell(new RootSearchRankInitMessage(allMembers, i, nodeCounter), getSelf());
			}
		}
	}
	
	@Override
	protected void genMove(GenMoveMessage msg) {
		for (int i = 0; i < allMembers.length; i++){
			ActorRef searchRank = allMembers[i];
			searchRank.tell(msg, getSelf());
		}
		numPendingResults = allMembers.length;
		votes = new HashMap<Move, Integer>();
		receivedPlayouts = 0;
	}
	
	@Override
	protected void allMembersInitialised() {
	
	}
}
