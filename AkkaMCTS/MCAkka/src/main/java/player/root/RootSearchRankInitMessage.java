package player.root;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class RootSearchRankInitMessage implements ClusterMemberInitMessage{
	private ActorRef[] allSearchRanks;
	private int rankIndex;
	private ActorRef nodeCounter;
	
	public RootSearchRankInitMessage(ActorRef[] allSearchRanks, int rankIndex, ActorRef nodeCounter) {
		this.allSearchRanks = allSearchRanks;
		this.rankIndex = rankIndex;
		this.nodeCounter = nodeCounter;
	}
	
	public ActorRef[] getAllSearchRanks() {
		return allSearchRanks;
	}
	
	public int getRankIndex() {
		return rankIndex;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
