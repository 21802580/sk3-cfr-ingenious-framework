package player.root;

import akka.actor.ActorRef;
import akka.actor.Props;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.ApplyMoveMessage;
import player.core.message.Message;
import player.core.message.NodeAddedMessage;
import player.core.message.SearchResultMessage;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.TreeDescent;
import player.leaf.LeafSimulator;
import player.leaf.LeafTraverser;
import player.leaf.SimulationRequestMessage;
import player.leaf.SimulationResponseMessage;

public class RootLeafSearchRank extends RootSearchRank {
	private int numPlayouts;
	private boolean running;
	private ActorRef traverser;
	private ActorRef[] simulators;
	
	public RootLeafSearchRank(PlayerParameters playerParameters, RootSearchRankInitMessage msg, ActorRef clusterManager) {
		super(playerParameters, msg, clusterManager);
		int numThreads = Math.max(playerParameters.getNumThreads(), 3); // At least 1 traverser and 1 simulator
		traverser = context().actorOf(Props.create(LeafTraverser.class, playerParameters, transpositionTable, getSelf()));
		simulators = new ActorRef[numThreads - 2];
		running = true;
		for (int i = 0; i < simulators.length; i++){
			simulators[i] = context().actorOf(Props.create(LeafSimulator.class, getSelf()));
		}
	}
	
	@Override
	protected void matchStarted() {
		for (int i = 0; i < simulators.length; i++){
			Board board = new Board(rootBoard);
			TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
			if (descent.getAddedNode() != -1){
				addNodeToBatch(descent.getAddedNode());
			}
			simulators[i].tell(new SimulationRequestMessage(board, descent.getPath()), getSelf());
		}
	}
	
	private void processSimulationResponse(SimulationResponseMessage msg){
		numPlayouts++;
		if (running) traverser.forward(msg, getContext());
	}
	
	@Override
	protected void matchOver() {
		running = false;
	}
	
	@Override
	protected void turnStart() {
		numPlayouts = 0;
	}
	
	@Override
	protected void turnEnd() {
		TreeNode node = transpositionTable.get(rootHash);
		int moveIndex = MCTS.getHighestVisitChild(node);
		Move move = node.getMoveAt(moveIndex);
		clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, 0), getSelf());
	}
	
	@Override
	protected void rootUpdated(Move m) {
		traverser.tell(new ApplyMoveMessage(m), getSelf());
	}
	
	
	@Override
	protected void processRootImplementationSpecificMessage(Message msg) {
		if (msg instanceof SimulationResponseMessage){
			processSimulationResponse((SimulationResponseMessage)msg);
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		}
	}
}
