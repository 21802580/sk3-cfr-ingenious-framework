package player.root;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.BroadcastPool;
import commandline.PlayerParameters;
import loa.gamelogic.Move;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;

public class RootTreeSearchRank extends RootSearchRank {
	private ActorRef workerRouter;
	private int totalPlayouts;
	private int playoutsReceived;
	private int numWorkers;
	
	public RootTreeSearchRank(PlayerParameters playerParameters, RootSearchRankInitMessage msg, ActorRef clusterManager) {
		super(playerParameters, msg, clusterManager);
		numWorkers = Math.max(1, playerParameters.getNumThreads() - 1);
		Props props = new BroadcastPool(numWorkers).props(Props.create(TreeWorker.class, transpositionTable, playerParameters, getSelf()));
		workerRouter = context().actorOf(props);
	}
	
	@Override
	protected void processRootImplementationSpecificMessage(Message msg) {
		if (msg instanceof TotalPlayoutMessage){
			accumulatePlayouts((TotalPlayoutMessage)msg);
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		}
	}
	
	@Override
	protected void rootUpdated(Move m) {
		workerRouter.tell(new ApplyMoveMessage(m), getSelf());
	}
	
	@Override
	protected void matchStarted() {
		workerRouter.tell(new MCTSIterationMessage(), getSelf());
	}
	
	@Override
	protected void matchOver() {
		workerRouter.tell(new MatchOverMessage(), getSelf());
	}
	
	@Override
	protected void turnStart() {
		workerRouter.tell(new TurnStartMessage(), getSelf());
	}
	
	@Override
	protected void turnEnd() {
		workerRouter.tell(new PlayoutRequestMessage(), getSelf());
	}

	private void accumulatePlayouts(TotalPlayoutMessage msg){
		totalPlayouts += msg.numSimulations();
		if (++playoutsReceived == numWorkers){
			TreeNode node = transpositionTable.get(rootHash);
			int moveIndex = MCTS.getHighestVisitChild(node);
			Move move = node.getMoveAt(moveIndex);
			clusterManager.tell(new SearchResultMessage(move, totalPlayouts, 0, 0), getSelf());
			playoutsReceived = 0;
			totalPlayouts = 0;
		}
	}
}
