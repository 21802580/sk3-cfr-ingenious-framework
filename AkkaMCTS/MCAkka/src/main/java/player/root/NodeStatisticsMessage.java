package player.root;

import akka.actor.ActorRef;
import loa.gamelogic.Board;
import player.core.message.Message;
import player.core.node.MoveStatistics;

public class NodeStatisticsMessage implements Message {
	private ActorRef searchRank;
	private long nodeHash;
	private Board board;
	private MoveStatistics[] newStatistics;
	
	public NodeStatisticsMessage(ActorRef searchRank, long nodeHash, Board board, MoveStatistics[] newStatistics) {
		this.searchRank = searchRank;
		this.nodeHash = nodeHash;
		this.board = board;
		this.newStatistics = newStatistics;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
	
	public MoveStatistics[] getNewStatistics() {
		return newStatistics;
	}
	
	public Board getBoard() {
		return board;
	}
	
	public ActorRef getSearchRank() {
		return searchRank;
	}
}
