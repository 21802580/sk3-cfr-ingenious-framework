package player.serial;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class SerialSearchRankInitMessage implements ClusterMemberInitMessage{
	private ActorRef nodeCounter;
	
	public SerialSearchRankInitMessage(ActorRef nodeCounter) {
		this.nodeCounter = nodeCounter;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
