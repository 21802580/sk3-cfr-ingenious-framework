package player.serial;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.TreeDescent;
import player.core.transposition.HashMapTranspositionTable;
import player.core.transposition.SimpleReplacementTranspositionTable;
import player.core.transposition.TranspositionTable;

public class SerialSearchRank extends AbstractSearchRank {
	private int numPlayouts;
	private boolean running;
	private int numNodes;
	
	public SerialSearchRank(PlayerParameters playerParameters,  SerialSearchRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		running = true;
		numPlayouts = 0;
	}
	
	private void mctsIteration(MCTSIterationMessage message){
		Board board = new Board(rootBoard);
		TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
		if (descent.getAddedNode() != -1){
			addNodeToBatch(descent.getAddedNode());
		}
		double[] rewards = MCTS.randomPlayout(board);
		MCTS.backupAllRewards(descent.getPath(), rewards, transpositionTable);
		numPlayouts++;
		if (running) getSelf().tell(message, getSelf());
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	@Override
	protected void matchStarted(MatchStartMessage message) {
		mctsIteration(new MCTSIterationMessage());
	}
	
	@Override
	protected void turnStart() {
		numPlayouts  = 0;
	}
	
	@Override
	protected void turnEnd() {
		TreeNode node = transpositionTable.get(rootHash);
		int moveIndex = MCTS.getHighestVisitChild(node);
		Move move = node.getMoveAt(moveIndex);
//		System.out.println("Selecting move " + move + " at index " + moveIndex + " from:");
//		for (int i = 0; i < node.getNumChildren(); i++) {
//			System.out.println(i + ": " + node.getMoveAt(i) + " [" + node.getMoveScore(i) + "/" + node.getMoveSelections(i) + "]");
//		}
		clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, 0), getSelf());
	}
	
	@Override
	protected void rootUpdated(Move m) {
	
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof MCTSIterationMessage){
			mctsIteration((MCTSIterationMessage) msg);
		}
	}
	
	@Override
	protected TranspositionTable makeTranspositionTable() {
		return new HashMapTranspositionTable(playerParameters.getTtPower(), false, playerParameters.getPerTurnCapacityPower());
//		return new SimpleReplacementTranspositionTable(playerParameters.getTtPower(), false);
	}
	
}

//mpirun -np 1 java -jar clustermatch.jar -seedhost 127.0.0.1 -seedport 61234 -name tds_1_0000_a -type tds -timeout 1000 -colour BLACK -nodes 1 -nworkers 1 _ -seedhost 127.0.0.1 -seedport 61235 -name serial_1_1000_b -type serial -timeout 1000 -colour WHITE -nodes 1 -nworkers 1 : -np 2 java -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61234 -name tds_1_1000_a -type tds -timeout 1000 -colour BLACK -nodes 1 -nworkers 1 : -np 2 java  -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61235 -name serial_1_1000_b -type serial -timeout 1000 -colour WHITE -nodes 1 -nworkers 1