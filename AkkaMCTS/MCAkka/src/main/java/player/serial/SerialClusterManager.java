package player.serial;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import player.core.actor.ClusterManager;
import player.core.message.ApplyMoveMessage;
import player.core.message.GenMoveMessage;
import player.core.message.SearchResultMessage;

public class SerialClusterManager extends ClusterManager{
	public SerialClusterManager(PlayerParameters playerParameters){
		super(playerParameters);
	}
	
	@Override
	public SearchResultMessage searchResultReceived(SearchResultMessage msg) {
		getSender().tell(new ApplyMoveMessage(msg.getSelectedMove()), getSelf());
		return msg;
	}
	
	@Override
	public void clusterMemberJoined(ActorRef member) {
		member.tell(new SerialSearchRankInitMessage(nodeCounter), getSelf());
	}
	
	@Override
	protected void genMove(GenMoveMessage msg) {
		allMembers[0].tell(msg, getSelf());
	}
	
	@Override
	protected void allMembersInitialised() {
	
	}
}
