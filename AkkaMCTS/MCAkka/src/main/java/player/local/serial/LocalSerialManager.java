package player.local.serial;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import commandline.PlayerParameters;
import player.core.message.*;
import player.serial.SerialSearchRank;

public class LocalSerialManager extends AbstractActor {
	private ActorRef searcher;
	private ActorRef referee;
	
	public LocalSerialManager(PlayerParameters playerParameters){
		searcher = context().actorOf(Props.create(SerialSearchRank.class, playerParameters));
	}
	
	private void initResponse(PlayerInitRequestMessage msg){
		getSender().tell(new PlayerInitResponseMessage(), getSelf());
	}
	
	private void genMove(GenMoveMessage msg){
		referee = getSender();
		searcher.tell(msg, getSelf());
	}
	
	private void sendResult(SearchResultMessage msg){
		referee.tell(msg, getSelf());
		searcher.tell(new ApplyMoveMessage(msg.getSelectedMove()), getSelf());
	}
	
	private void matchOver(MatchOverMessage msg){
		searcher.tell(msg, getSelf());
	}
	
	private void matchStart(MatchStartMessage msg){
		searcher.tell(msg, getSelf());
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(PlayerInitRequestMessage.class, this::initResponse)
				.match(MatchStartMessage.class, this::matchStart)
				.match(GenMoveMessage.class, this::genMove)
				.match(SearchResultMessage.class, this::sendResult)
				.match(MatchOverMessage.class, this::matchOver)
				.matchAny(this::unhandled)
				.build();
	}
}
