package player.local.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.TreeDescent;
import player.core.transposition.AbstractReplacementTranspositionTable;
import player.core.transposition.SimpleReplacementTranspositionTable;
import player.core.transposition.Zobrist;
import player.leaf.LeafSimulator;
import player.leaf.LeafTraverser;
import player.leaf.SimulationRequestMessage;
import player.leaf.SimulationResponseMessage;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;

public class LocalLeafParallelSearcher extends AbstractActor {
	private int numPlayouts;
	private boolean running;
	private ActorRef traverser;
	private ActorRef[] simulators;
	private AbstractReplacementTranspositionTable transpositionTable;
	private PlayerParameters playerParameters;
	private Board rootBoard;
	private long rootHash;
	private FiniteDuration genMoveDuration;
	
	public LocalLeafParallelSearcher(PlayerParameters playerParameters){
		this(playerParameters, new SimpleReplacementTranspositionTable(playerParameters.getTtPower(), false));
	}
	
	public LocalLeafParallelSearcher(PlayerParameters playerParameters, AbstractReplacementTranspositionTable transpositionTable){
		this.playerParameters = playerParameters;
		this.transpositionTable = transpositionTable;
		genMoveDuration = FiniteDuration.create(playerParameters.getTimeout() - 50, TimeUnit.MILLISECONDS);
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
		running = true;
		traverser = context().actorOf(Props.create(LeafTraverser.class, playerParameters, transpositionTable));
		simulators = new ActorRef[playerParameters.getNumThreads() - 1];
		for (int i = 0; i < simulators.length; i++){
			simulators[i] = context().actorOf(Props.create(LeafSimulator.class, getSelf()));
		}
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MatchStartMessage.class, this::matchStarted)
				.match(MatchOverMessage.class, this::matchOver)
				.match(GenMoveMessage.class, this::genMove)
				.match(ApplyMoveMessage.class, this::updateRoot)
				.match(PlayerInitRequestMessage.class, this::replyToInit)
				.match(SimulationResponseMessage.class, this::processSimulationResponse)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void processSimulationResponse(SimulationResponseMessage msg){
		numPlayouts++;
		if (running) traverser.forward(msg, getContext());
	}
	
	private void replyToInit(PlayerInitRequestMessage msg){
		getSender().tell(new PlayerInitResponseMessage(), getSelf());
	}
	
	
	private void genMove(GenMoveMessage msg){
		numPlayouts = 0;
		ActorRef replyTo = getSender();
		Move m = msg.getOpponentMove();
		if (m != null){
			getSelf().tell(new ApplyMoveMessage(m), getSelf());
		}
		getContext().getSystem().scheduler().scheduleOnce(genMoveDuration,
				() -> sendSearchResult(replyTo), getContext().getSystem().dispatcher());
	}
	
	private void updateRoot(ApplyMoveMessage msg){
		rootHash = Zobrist.getChildHash(rootHash, msg.getMove(), rootBoard.getCurrentPlayer());
		rootBoard.applyMove(msg.getMove());
		transpositionTable.updateStartTime();
		traverser.tell(msg, getSelf());
	}
	
	private void sendSearchResult(ActorRef replyTo) {
		TreeNode node = transpositionTable.get(rootHash);
		int moveIndex = MCTS.getHighestVisitChild(node);
		Move move = node.getMoveAt(moveIndex);
		getSelf().tell(new ApplyMoveMessage(move), getSelf());
		replyTo.tell(new SearchResultMessage(move, numPlayouts, 0, 0), getSelf());
	}
	
	private void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	private void matchStarted(MatchStartMessage message) {
		for (int i = 0; i < simulators.length; i++){
			Board board = new Board(rootBoard);
			TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
			simulators[i].tell(new SimulationRequestMessage(board, descent.getPath()), getSelf());
		}
	}
	
}
