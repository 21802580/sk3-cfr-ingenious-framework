package player.local.tree;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.BroadcastPool;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.transposition.AbstractReplacementTranspositionTable;
import player.core.transposition.ConcurrentReplacementTranspositionTable;
import player.core.transposition.Zobrist;
import player.root.PlayoutRequestMessage;
import player.root.TreeWorker;
import scala.concurrent.duration.FiniteDuration;

import java.util.concurrent.TimeUnit;


public class LocalTreeParallelSearcher extends AbstractActor {
	private ActorRef workerRouter;
	private long rootHash;
	private AbstractReplacementTranspositionTable transpositionTable;
	private ActorRef returnActor;
	private Board rootBoard;
	private PlayerParameters playerParams;
	private int totalPlayouts;
	private int playoutsReceived;
	private FiniteDuration genMoveDuration;

	public LocalTreeParallelSearcher(PlayerParameters playerParameters){
		this(playerParameters, new ConcurrentReplacementTranspositionTable(playerParameters.getTtPower(), false));
	}
	
	public LocalTreeParallelSearcher(PlayerParameters playerParameters, AbstractReplacementTranspositionTable transpositionTable){
		this.playerParams = playerParameters;
		this.transpositionTable = transpositionTable;
		totalPlayouts = 0;
		playoutsReceived = 0;
		genMoveDuration = FiniteDuration.create(playerParams.getTimeout() - 50, TimeUnit.MILLISECONDS);
		Props props = new BroadcastPool(playerParams.getNumThreads()).props(Props.create(TreeWorker.class, transpositionTable, playerParams));
		workerRouter = context().actorOf(props);
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
		transpositionTable.putIfAbsent(rootHash, rootBoard, playerParams);
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MatchStartMessage.class, this::matchStart)
				.match(MatchOverMessage.class, this::matchOver)
				.match(GenMoveMessage.class, this::genMove)
				.match(PlayerInitRequestMessage.class, this::replyToInit)
				.match(TotalPlayoutMessage.class, this::accumulatePlayouts)
				.match(ApplyMoveMessage.class, this::applyMove)
				.matchAny(this::unhandled).build();
	}

	private void genMove(GenMoveMessage message){
		returnActor = getSender();
		Move m = message.getOpponentMove();
		if (m != null){
			updateRoot(m);
		}
		workerRouter.tell(message, getSelf());
		getContext().getSystem().scheduler().scheduleOnce(genMoveDuration,
				this::requestPlayouts, getContext().getSystem().dispatcher());
	}
	
	private void applyMove(ApplyMoveMessage msg){
		updateRoot(msg.getMove());
	}

	private void matchStart(MatchStartMessage msg){
		workerRouter.tell(new MCTSIterationMessage(), getSelf());
	}

	private void matchOver(MatchOverMessage msg){
		workerRouter.tell(msg, getSelf());
	}

	private void requestPlayouts(){
		workerRouter.tell(new PlayoutRequestMessage(), getSelf());
	}

	private void accumulatePlayouts(TotalPlayoutMessage msg){
		totalPlayouts += msg.numSimulations();
		if (++playoutsReceived == playerParams.getNumThreads()){
			TreeNode node = transpositionTable.get(rootHash);
			int moveIndex = MCTS.getHighestVisitChild(node);
			Move move = node.getMoveAt(moveIndex);
			returnActor.tell(new SearchResultMessage(move, totalPlayouts, 0, 0), getSelf());
			playoutsReceived = 0;
			totalPlayouts = 0;
		}
	}

	private void updateRoot(Move m){
		rootHash = Zobrist.getChildHash(rootHash, m, rootBoard.getCurrentPlayer());
		rootBoard.applyMove(m);
		workerRouter.tell(new ApplyMoveMessage(m), getSelf());
		transpositionTable.updateStartTime();
		transpositionTable.putIfAbsent(rootHash, rootBoard, playerParams);
	}
	
	private void replyToInit(PlayerInitRequestMessage msg){
		getSender().tell(new PlayerInitResponseMessage(), getSelf());
	}
}
