package player.core.message;

public class NodeCountMessage implements Message {
	private int numNodes;
	
	public NodeCountMessage(int numNodes) {
		this.numNodes = numNodes;
	}
	
	public int getNumNodes() {
		return numNodes;
	}
}
