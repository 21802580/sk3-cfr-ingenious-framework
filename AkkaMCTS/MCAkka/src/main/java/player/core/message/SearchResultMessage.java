package player.core.message;

import loa.gamelogic.Move;

public class SearchResultMessage implements Message {
	private Move selectedMove;
	private int totalPlayouts;
	private int totalNodes;
	private int reportsAtRoot;
	
	public SearchResultMessage(Move selectedMove, int totalPlayouts, int totalNodes, int reportsAtRoot) {
		this.selectedMove = selectedMove;
		this.totalPlayouts = totalPlayouts;
		this.totalNodes = totalNodes;
		this.reportsAtRoot = reportsAtRoot;
	}
	
	public int getTotalPlayouts() {
		return totalPlayouts;
	}
	
	public Move getSelectedMove() {
		return selectedMove;
	}
	
	public void setTotalNodes(int numNodes){
		this.totalNodes = numNodes;
	}
	
	public int getTotalNodes() {
		return totalNodes;
	}
	
	public int getReportsAtRoot() {
		return reportsAtRoot;
	}
}
