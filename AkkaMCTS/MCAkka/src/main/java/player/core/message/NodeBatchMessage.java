package player.core.message;

import java.util.LinkedList;

public class NodeBatchMessage implements Message {
	private final LinkedList<Long> nodesAdded; // LinkedList for faster addition and removal. Retrieval not an issue.
	
	public NodeBatchMessage(LinkedList<Long> nodesAdded){
		this.nodesAdded = nodesAdded;
	}
	
	public LinkedList<Long> getNodesAdded(){
		return nodesAdded;
	}
}
