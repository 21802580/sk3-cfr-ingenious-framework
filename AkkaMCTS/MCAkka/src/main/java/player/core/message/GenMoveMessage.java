package player.core.message;

import loa.gamelogic.Move;

public class GenMoveMessage implements Message {
	private Move opponentMove;
	
	public GenMoveMessage(Move opponentMove) {
		this.opponentMove = opponentMove;
	}
	
	public Move getOpponentMove() {
		return opponentMove;
	}
}
