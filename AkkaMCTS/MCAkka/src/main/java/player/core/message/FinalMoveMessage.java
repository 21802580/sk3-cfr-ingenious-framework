package player.core.message;

public class FinalMoveMessage implements Message {
	private SearchResultMessage searchResult;
	private int numNodes;
	
	public FinalMoveMessage(SearchResultMessage searchResult, int numNodes) {
		this.searchResult = searchResult;
		this.numNodes = numNodes;
	}
	
	public int getNumNodes() {
		return numNodes;
	}
	
	public SearchResultMessage getSearchResult() {
		return searchResult;
	}
}
