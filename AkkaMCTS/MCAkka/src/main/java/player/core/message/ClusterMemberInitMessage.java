package player.core.message;

import akka.actor.ActorRef;

public interface ClusterMemberInitMessage extends Message {
	ActorRef getNodeCounter();
}
