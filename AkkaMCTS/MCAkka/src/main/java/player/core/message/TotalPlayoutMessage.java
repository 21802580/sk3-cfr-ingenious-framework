package player.core.message;

public class TotalPlayoutMessage implements Message {
	private int numSimulations;
	
	public TotalPlayoutMessage(int numSimulations) {
		this.numSimulations = numSimulations;
	}
	
	public int numSimulations() {
		return numSimulations;
	}
}
