package player.core.message;

public class NodeAddedMessage implements Message {
	private long nodeHash;
	
	public NodeAddedMessage(long nodeHash) {
		this.nodeHash = nodeHash;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
}
