package player.core.message;

import loa.gamelogic.Move;

public class ApplyMoveMessage implements Message {
	private Move move;
	
	public ApplyMoveMessage(Move move) {
		this.move = move;
	}
	
	public Move getMove() {
		return move;
	}
}
