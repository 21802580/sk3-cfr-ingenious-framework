package player.core.node;

public class VolatileMoveStatistics implements MoveStatistics{
	
	private volatile int selections;
	private volatile double score;
	
	@Override
	public int getSelections() {
		return selections;
	}
	
	@Override
	public double getScore() {
		return score;
	}
	
	@Override
	public void addSelections(int delta) {
		selections += delta;
	}
	
	@Override
	public void addScore(double delta) {
		score += delta;
	}
	
	@Override
	public void zero() {
		selections = 0;
		score = 0;
	}
	
	@Override
	public MoveStatistics copy() {
		MoveStatistics statistics = new VolatileMoveStatistics();
		statistics.addSelections(this.selections);
		statistics.addScore(this.score);
		return statistics;
	}
}
