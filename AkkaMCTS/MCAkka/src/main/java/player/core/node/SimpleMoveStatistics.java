package player.core.node;

public class SimpleMoveStatistics implements MoveStatistics{
	private int selections;
	private double score;
	
	@Override
	public int getSelections() {
		return selections;
	}
	
	@Override
	public double getScore() {
		return score;
	}
	
	@Override
	public void addSelections(int delta) {
		selections += delta;
	}
	
	@Override
	public void addScore(double delta) {
		score += delta;
	}
	
	@Override
	public void zero() {
		selections = 0;
		score = 0;
	}
	
	@Override
	public MoveStatistics copy() {
		MoveStatistics statistics = new SimpleMoveStatistics();
		statistics.addSelections(this.selections);
		statistics.addScore(this.score);
		return statistics;
	}
}
