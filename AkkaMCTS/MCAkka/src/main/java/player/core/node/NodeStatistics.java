package player.core.node;

public interface NodeStatistics {
	
	void initialiseMoveStatistics(int index);
	
	int getTotalVisits();
	
	int getMoveSelections(int index);
	
	MoveStatistics[] getUnsyncedMoveStatistics();
	
	double getMoveScore(int index);
	
	void addVisits(int delta);
	
	void addMoveSelections(int index, int delta);
	
	void addMoveScore(int index, double delta);
	
	void incorporateRemoteStatistics(MoveStatistics[] moveStatistics);
	
	void incorporateUnsyncedStatistics();
	
	void incorporateAndZeroUnsyncedStatistics();
	
	void zeroUnsyncedStatistics();
	
}
