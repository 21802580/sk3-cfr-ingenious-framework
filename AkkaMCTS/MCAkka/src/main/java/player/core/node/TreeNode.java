package player.core.node;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Colour;
import loa.gamelogic.Move;
import loa.movegeneration.MoveGenerator;
import loa.movegeneration.MoveValuePair;

import java.util.Collections;
import java.util.List;

public class TreeNode {
	
	private Move[] moves;
	private double[] heuristicValues;
	private long nodeHash;
	private Colour playerToMove;
	private PlayerParameters playerParameters;
	private int windowSize;
	private NodeStatistics statistics;
	
	public TreeNode(Board board, long nodeHash, PlayerParameters playerParameters, boolean isVolatile, boolean isShareable){
		this.nodeHash = nodeHash;
		playerToMove = board.getCurrentPlayer();
		this.playerParameters = playerParameters;
		makeChildren(board, isVolatile, isShareable);
		windowSize = playerParameters.isProgressiveUnpruning() ? playerParameters.getInitialWindowSize() : moves.length;
	}
	
	private void makeChildren(Board board, boolean isVolatile, boolean isShareable){
		List<MoveValuePair> allMoves = MoveGenerator.generateMovesWithValues(board, playerParameters);
		if (playerParameters.isProgressiveUnpruning()) {
			Collections.sort(allMoves, Collections.reverseOrder());
		}
		moves = new Move[allMoves.size()];
		heuristicValues = new double[allMoves.size()];
		statistics = isVolatile ? new VolatileNodeStatistics(allMoves.size(), isShareable) :
				new SimpleNodeStatistics(allMoves.size(), isShareable);
		for (int i = 0; i < allMoves.size(); i++){
			MoveValuePair mvp = allMoves.get(i);
			moves[i] = mvp.getMove();
			heuristicValues[i] = mvp.getValue();
			statistics.initialiseMoveStatistics(i);
		}
	}
	
	public long getHash(){
		return nodeHash;
	}
	
	public void incorporateRemoteStatistics(MoveStatistics[] remoteStatistics){
		statistics.incorporateRemoteStatistics(remoteStatistics);
		updateWindow();
	}
	
	public void incorporateUnsyncedStatistics(){
		statistics.incorporateUnsyncedStatistics();
	}
	
	public void incorporateAndZeroUnsyncedStatistics(){
		statistics.incorporateAndZeroUnsyncedStatistics();
	}
	
	public void zeroUnsyncedStatistics(){
		statistics.zeroUnsyncedStatistics();
	}
	
	public void addVisits(int delta){
		statistics.addVisits(delta);
		updateWindow();
	}
	
	public int getTotalVisits(){
		return statistics.getTotalVisits();
	}
	
	public Move getMoveAt(int moveIndex){
		return moves[moveIndex];
	}
	
	public int getMoveSelections(int moveIndex){
		return statistics.getMoveSelections(moveIndex);
	}
	
	public double getMoveScore(int moveIndex){
		return statistics.getMoveScore(moveIndex);
	}
	
	public double getMoveHeuristicValue(int moveIndex){
		return heuristicValues[moveIndex];
	}
	
	public void addMoveSelections(int moveIndex, int numSelections){
		statistics.addMoveSelections(moveIndex, numSelections);
	}
	
	public void addChildScore(int moveIndex, double score){
		statistics.addMoveScore(moveIndex, score);
	}
	
	public int getWindowSize(){
		return windowSize;
	}
	
	public int getNumChildren() { return moves.length; }
	
	private void updateWindow(){
		if (windowSize < moves.length){
			windowSize = playerParameters.getInitialWindowSize() + (int) (Math.log(getTotalVisits()) / (Math.log(playerParameters.getUnpruningRate())));
		}
	}
	
	public MoveStatistics[] getUnsyncedMoveStatistics(){
		return statistics.getUnsyncedMoveStatistics();
	}
	
	public Colour getPlayerToMove() {
		return playerToMove;
	}
}
