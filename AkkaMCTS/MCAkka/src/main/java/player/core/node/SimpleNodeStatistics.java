package player.core.node;

public class SimpleNodeStatistics implements NodeStatistics{
	private int visits;
	private SimpleMoveStatistics[] syncedMoveStatistics;
	private SimpleMoveStatistics[] unsyncedMoveStatistics;
	
	public SimpleNodeStatistics(int numMoves, boolean shareable){
		if (shareable){
			syncedMoveStatistics = new SimpleMoveStatistics[numMoves];
		}
		unsyncedMoveStatistics = new SimpleMoveStatistics[numMoves];
	}
	
	
	@Override
	public void initialiseMoveStatistics(int index) {
		unsyncedMoveStatistics[index] = new SimpleMoveStatistics();
		if (syncedMoveStatistics != null){
			syncedMoveStatistics[index] = new SimpleMoveStatistics();
		}
	}
	
	@Override
	public int getTotalVisits() {
		return visits;
	}
	
	@Override
	public int getMoveSelections(int index) {
		if (syncedMoveStatistics == null){
			return unsyncedMoveStatistics[index].getSelections();
		}
		return syncedMoveStatistics[index].getSelections() +
				unsyncedMoveStatistics[index].getSelections();
	}
	
	@Override
	public MoveStatistics[] getUnsyncedMoveStatistics() {
		return unsyncedMoveStatistics;
	}
	
	@Override
	public double getMoveScore(int index) {
		if (syncedMoveStatistics == null){
			return unsyncedMoveStatistics[index].getScore();
		}
		return syncedMoveStatistics[index].getScore() +
				unsyncedMoveStatistics[index].getScore();
	}
	
	@Override
	public void addVisits(int delta) {
		visits += delta;
	}
	
	@Override
	public void addMoveSelections(int index, int delta) {
		unsyncedMoveStatistics[index].addSelections(delta);
	}
	
	@Override
	public void addMoveScore(int index, double delta) {
		unsyncedMoveStatistics[index].addScore(delta);
	}
	
	@Override
	public void incorporateRemoteStatistics(MoveStatistics[] moveStatistics) {
		if (syncedMoveStatistics == null){
			return;
		}
		for (int i = 0; i < moveStatistics.length; i++){
			syncedMoveStatistics[i].addSelections(moveStatistics[i].getSelections());
			syncedMoveStatistics[i].addScore(moveStatistics[i].getScore());
			visits += moveStatistics[i].getSelections();
		}
	}
	
	@Override
	public void incorporateUnsyncedStatistics() {
		if (syncedMoveStatistics == null){
			return;
		}
		for (int i = 0; i < syncedMoveStatistics.length; i++){
			syncedMoveStatistics[i].addSelections(unsyncedMoveStatistics[i].getSelections());
			syncedMoveStatistics[i].addScore(unsyncedMoveStatistics[i].getScore());
		}
	}
	
	@Override
	public void incorporateAndZeroUnsyncedStatistics() {
		if (syncedMoveStatistics == null){
			return;
		}
		for (int i = 0; i < syncedMoveStatistics.length; i++){
			syncedMoveStatistics[i].addSelections(unsyncedMoveStatistics[i].getSelections());
			syncedMoveStatistics[i].addScore(unsyncedMoveStatistics[i].getScore());
			unsyncedMoveStatistics[i].zero();
		}
	}
	
	@Override
	public void zeroUnsyncedStatistics() {
		for (int i = 0; i < syncedMoveStatistics.length; i++){
			visits -= unsyncedMoveStatistics[i].getSelections();
			unsyncedMoveStatistics[i].zero();
		}
	}
}
