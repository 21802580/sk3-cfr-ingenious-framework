package player.core.node;

public interface MoveStatistics {
	
	int getSelections();
	
	double getScore();
	
	void addSelections(int delta);
	
	void addScore(double delta);
	
	void zero();
	
	MoveStatistics copy();
}
