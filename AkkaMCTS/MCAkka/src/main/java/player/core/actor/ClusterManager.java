package player.core.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.pattern.Patterns;
import akka.util.Timeout;
import commandline.PlayerParameters;
import player.core.message.*;
import player.root.TurnStartMessage;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.concurrent.TimeUnit;

public abstract class ClusterManager extends AbstractActor {
    protected ActorRef[] allMembers;
    private ActorRef replyTo;
    private Cluster cluster;
    protected PlayerParameters playerParameters;
    protected int numRegistrationsReceived;
    private int numMembersInitialised;
    protected ActorRef nodeCounter;
    private int numExited;

    public ClusterManager(PlayerParameters playerParameters) {
        allMembers = new ActorRef[playerParameters.getNumNodes()];
        cluster = Cluster.get(context().system());
        this.playerParameters = playerParameters;
        this.numExited = 0;
    }
	
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), ClusterEvent.MemberExited.class);
	}

    @Override
    public void postStop() {
//		System.out.println(playerParameters.getColour() + " ClusterManager: postStop()");
		cluster.leave(cluster.selfAddress());
    }
    
    private void memberExited(ClusterEvent.MemberExited msg) {
    	if (++numExited == allMembers.length + 1) { // +1 For NodeCounter
//			System.out.println(playerParameters.getColour() + " ClusterManager: MemberExited(" + numExited + ")");
			context().stop(getSelf());
		}
	}

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MatchStartMessage.class, this::receiveMatchStart)
                .match(ClusterMemberRegistrationMessage.class, this::receiveActorRegistration)
                .match(PlayerInitRequestMessage.class, this::receiveInitRequest)
				.match(GenMoveMessage.class, this::receiveGenMove)
				.match(MatchOverMessage.class, this::matchOver)
				.match(SearchResultMessage.class, this::receiveSearchResult)
				.match(InitCompleteMessage.class, this::memberInitialised)
				.match(ClusterEvent.MemberExited.class, this::memberExited)
                .matchAny(this::unhandled)
                .build();
    }
    
    private void receiveMatchStart(MatchStartMessage msg){
		replyTo = getSender();
		for (ActorRef a : allMembers){
			a.tell(msg, getSelf());
		}
		nodeCounter.tell(msg, getSelf());
	}
	
	private void memberInitialised(InitCompleteMessage msg){
    	numMembersInitialised++;
    	if (numMembersInitialised == playerParameters.getNumNodes() + 1) { // NodeCounter also sends an InitCompleteMessage
			allMembersInitialised();
			if (replyTo != null){
				replyTo.tell(new PlayerInitResponseMessage(), getSelf());
			}
		}
	}
	
    private void receiveActorRegistration(ClusterMemberRegistrationMessage msg){
		ActorRef member = getSender();
		if (nodeCounter == null) {
			nodeCounter = member;
			nodeCounter.tell(new NodeCounterInitMessage(), self());
		} else {
			allMembers[numRegistrationsReceived] = member;
			numRegistrationsReceived++;
			clusterMemberJoined(member);
		}
    }
	
	private void receiveSearchResult(SearchResultMessage msg){
		SearchResultMessage finalResult = searchResultReceived(msg);
		if (finalResult != null) {
			Timeout t = new Timeout(Duration.create(10, TimeUnit.SECONDS));
			Future<Object> fut = Patterns.ask(nodeCounter, new MoveChosenMessage(), t);
			NodeCountMessage nodeCountMessage = null;
			try {
				nodeCountMessage = (NodeCountMessage) Await.result(fut, t.duration());
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}
			replyTo.tell(new FinalMoveMessage(finalResult, nodeCountMessage.getNumNodes()), getSelf());
		}
	}
	
	private void receiveInitRequest(PlayerInitRequestMessage msg){
		replyTo = getSender();
		if (numMembersInitialised == playerParameters.getNumNodes() + 1) { // NodeCounter also sends an InitCompleteMessage
			replyTo.tell(new PlayerInitResponseMessage(), getSelf());
			allMembersInitialised();
		}
	}
	
	private void matchOver(MatchOverMessage msg){
//    	System.out.println(playerParameters.getColour() + " ClusterManager: MatchOverMessage");
    	for (ActorRef a : allMembers){
    		a.tell(msg, getSelf());
		}
		nodeCounter.tell(msg, getSelf());
	}
	
	private void receiveGenMove(GenMoveMessage msg){
    	replyTo = getSender();
    	nodeCounter.tell(new TurnStartMessage(), getSelf());
    	genMove(msg);
	}
    
    public abstract SearchResultMessage searchResultReceived(SearchResultMessage msg);

    public abstract void clusterMemberJoined(ActorRef member);
    
    protected abstract void genMove(GenMoveMessage msg);
    
    protected abstract void allMembersInitialised();
    
}
