package player.core.actor;

import akka.actor.AbstractActor;
import commandline.PlayerParameters;
import player.core.message.MatchOverMessage;
import player.core.message.MoveChosenMessage;
import player.core.message.NodeBatchMessage;
import player.core.message.NodeCountMessage;
import player.root.TurnStartMessage;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class NodeCounter extends AbstractActor {
	private Set<Long> nodesForCurrentTurn;
	private PlayerParameters playerParameters;
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(NodeBatchMessage.class, this::addNodes)
				.match(MatchOverMessage.class, this::die)
				.match(TurnStartMessage.class, this::startTurn)
				.match(MoveChosenMessage.class, this::sendNodes)
				.matchAny(this::unhandled)
				.build();
	}
	
	public NodeCounter(PlayerParameters playerParameters) {
		this.playerParameters = playerParameters;
		nodesForCurrentTurn = ConcurrentHashMap.newKeySet((int)Math.pow(2, playerParameters.getPerTurnCapacityPower()));
	}
	
	private void startTurn(TurnStartMessage msg) {
		nodesForCurrentTurn =  ConcurrentHashMap.newKeySet((int)Math.pow(2, playerParameters.getPerTurnCapacityPower()));
	}
	
	private void addNodes(NodeBatchMessage msg) {
//		int i = 0;
//		for (long h : msg.getNodesAdded()) {
//			if (nodesForCurrentTurn.contains(h)) {
//				i++;
//			}
//		}
//		System.out.println("Already had " + i + " hashes");
		nodesForCurrentTurn.addAll(msg.getNodesAdded());
	}
	
	private void sendNodes(MoveChosenMessage msg) {
//		System.out.println("NodeCounter sending batch to ClusterManager");
		sender().tell(new NodeCountMessage(nodesForCurrentTurn.size()), getSelf());
	}
	
	private void die(MatchOverMessage msg) {
		context().stop(self());
	}
}
