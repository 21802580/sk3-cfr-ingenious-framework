package player.core.actor;

import akka.actor.*;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.Member;
import commandline.PlayerParameters;
import player.core.message.*;
import player.dfuct.DFUCTSearchRank;
import player.dfuct.DFUCTSearchRankInitMessage;
import player.leaf.SimulatorRank;
import player.leaf.SimulatorRankInitMessage;
import player.leaf.TraverserRank;
import player.leaf.TraverserRankInitMessage;
import player.root.RootLeafSearchRank;
import player.root.RootSearchRankInitMessage;
import player.root.RootTreeSearchRank;
import player.serial.SerialSearchRank;
import player.serial.SerialSearchRankInitMessage;
import player.tds.TDSSearchRank;
import player.tds.TDSSearchRankInitMessage;
import player.treesplit.*;

public class ClusterMember extends AbstractActor {
	private PlayerParameters playerParameters;
    private Cluster cluster;
    private ActorRef master;
    private ActorRef child;
    private ActorSystem system;
    
    public static void main(String[] args){
    	PlayerParameters playerParameters = PlayerParameters.getInstance(args);
		ActorSystem system = ActorSystemFactory.makeClusterActorSystem(playerParameters, ActorSystemFactory.MEMBER_ROLE);
		system.actorOf(Props.create(ClusterMember.class, playerParameters, system));
	}

    public ClusterMember(PlayerParameters playerParameters, ActorSystem system) {
		this.system = system;
		this.playerParameters = playerParameters;
    	cluster = Cluster.get(system);
    }

    @Override
    public void preStart() {
        cluster.subscribe(getSelf(), MemberUp.class);
    }

    @Override
    public void postStop() {
//		System.out.println(playerParameters.getColour() + " ClusterMember: postStop()");
		cluster.leave(cluster.selfAddress());
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(MemberUp.class, this::tryRegistration)
                .match(ClusterMemberInitMessage.class, this::initMember)
				.match(MatchOverMessage.class, this::matchOver)
                .matchAny(this::forwardToChild)
				.build();
    }

    private void initMember(ClusterMemberInitMessage msg){
		master = getSender();
		child = makeClusterNode(msg);
		while (child == null){}
		master.tell(new InitCompleteMessage(), getSelf());
	}

    private void tryRegistration(MemberUp msg) {
    	Member member = msg.member();
        if (member.hasRole(ActorSystemFactory.MANAGER_ROLE)) {
            String managerName = playerParameters.getColour() + ActorSystemFactory.MANAGER_ROLE;
            ActorSelection selection = getContext().actorSelection(member.address() + "/user/" + managerName);
            selection.tell(new ClusterMemberRegistrationMessage(), getSelf());
        }
    }
	
	private ActorRef makeClusterNode(ClusterMemberInitMessage msg){
		ActorRef ref = null;
		String memberName = playerParameters.getColour() + ActorSystemFactory.MEMBER_ROLE;
		if (msg instanceof TraverserRankInitMessage) {
			ref = context().actorOf(Props.create(TraverserRank.class, playerParameters, msg, master), memberName);
		} else if (msg instanceof SimulatorRankInitMessage){
			ref = context().actorOf(Props.create(SimulatorRank.class, playerParameters, msg), memberName);
		} else if (msg instanceof TDSSearchRankInitMessage){
			ref = context().actorOf(Props.create(TDSSearchRank.class, playerParameters, msg, master), memberName);
		} else if (msg instanceof DFUCTSearchRankInitMessage){
			ref = context().actorOf(Props.create(DFUCTSearchRank.class, playerParameters, msg, master), memberName);
		} else if (msg instanceof SerialSearchRankInitMessage){
			ref = context().actorOf(Props.create(SerialSearchRank.class, playerParameters, msg, master), memberName);
		} else if (msg instanceof RootSearchRankInitMessage){
			if (playerParameters.getType().equals("roottree")){
				ref = context().actorOf(Props.create(RootTreeSearchRank.class, playerParameters, msg, master), memberName);
			} else {
				ref = context().actorOf(Props.create(RootLeafSearchRank.class, playerParameters, msg, master), memberName);
			}
		} else if (msg instanceof TreesplitSearchRankInitMessage){
			ref = context().actorOf(Props.create(TreesplitSearchRank.class, playerParameters, msg, master), memberName);
		} else if (msg instanceof TreesplitBroadcastRankInitMessage){
			ref = context().actorOf(Props.create(TreesplitBroadcastRank.class, playerParameters, msg), memberName);
		} else if (msg instanceof NodeCounterInitMessage) {
			ref = context().actorOf(Props.create(NodeCounter.class, playerParameters), memberName);
		}
		return ref;
	}
	
	private void forwardToChild(Object message){
    	if (child == null){
    		unhandled(message);
		} else {
			child.forward(message, getContext());
		}
	}
	
	private void matchOver(MatchOverMessage msg){
//    	System.out.println(playerParameters.getColour() + " ClusterMember: MatchOverMessage");
		context().stop(getSelf());
	}
}

//mpirun -np 1 java -XX:+UseG1GC -jar clustermatch.jar -seedhost 127.0.0.1 -seedport 61234 -name treesplit_8_1000_a -type treesplit -timeout 1000 -colour BLACK -nodes 4 -nworkers 2 _ -seedhost 127.0.0.1 -seedport 61235 -name roottree_8_1000_b -type roottree -timeout 1000 -colour WHITE -nodes 4 -nworkers 2 : -np 5 java -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61234 -name treesplit_8_1000_a -type treesplit -timeout 1000 -colour BLACK -nodes 4 -nworkers 2 : -np 5 java -jar clustermember.jar -seedhost 127.0.0.1 -seedport 61235 -name roottree_8_1000_b -type roottree -timeout 1000 -colour WHITE -nodes 4 -nworkers 2

