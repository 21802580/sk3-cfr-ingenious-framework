package player.core.actor;

import akka.actor.ActorSystem;
import akka.dispatch.PriorityGenerator;
import akka.dispatch.UnboundedStablePriorityMailbox;
import com.typesafe.config.Config;
import player.core.message.*;
import player.root.PlayoutRequestMessage;

public class MatchControlPriorityMailbox extends UnboundedStablePriorityMailbox {
	
	public MatchControlPriorityMailbox(ActorSystem.Settings settings, Config config) {
		super(new PriorityGenerator() {
			@Override
			public int gen(Object message) {
				if (message instanceof GenMoveMessage)
					return 0;
				else if (message instanceof ApplyMoveMessage)
					return 0;
				else if (message instanceof MatchStartMessage){
					return 0;
				} else if (message instanceof MatchOverMessage){
					return 0;
				} else if (message instanceof SearchResultMessage){
					return 0;
				} else if (message instanceof TotalPlayoutMessage){
					return 0;
				} else if (message instanceof PlayoutRequestMessage){
					return 0;
				} else if (message instanceof FinalMoveMessage){
					return 0;
				} else if (message instanceof MoveChosenMessage){
					return 0;
				} else if (message instanceof NodeCountMessage){
					return 0;
				} else {
					return 1;
				}
			}
		});
	}
}
