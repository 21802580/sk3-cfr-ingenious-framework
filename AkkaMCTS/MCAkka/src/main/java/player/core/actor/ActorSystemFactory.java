package player.core.actor;

import akka.actor.ActorSystem;
import com.esotericsoftware.minlog.Log;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import commandline.PlayerParameters;
import loa.gamelogic.Colour;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class ActorSystemFactory {
	public static final String MANAGER_ROLE = "ClusterManager";
	public static final String MEMBER_ROLE = "ClusterMember";
	public static final String ACTOR_SYSTEM_SUFFIX = "System";
	
	public static ActorSystem makeLocalActorSystem(PlayerParameters params){
		Colour colour = params.getColour();
		String actorSystemName = colour + "System";
		Config config = ConfigFactory.load();
		return ActorSystem.create(actorSystemName, config);
	}
	
	public static ActorSystem makeClusterActorSystem(PlayerParameters params, String role) {
		Colour colour = params.getColour();
		String seedHost = params.getSeedHost();
		String seedPort = params.getSeedPort();
		String hostname = "";
		try {
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
			Log.error("Could not get hostname " + colour, e);
		}
		String actorSystemName = colour + ACTOR_SYSTEM_SUFFIX;
		String seedNode = "akka.tcp://" + actorSystemName + "@" + seedHost + ":" + seedPort;
//		String seedNode = "akka://" + actorSystemName + "@" + seedHost + ":" + seedPort;
		if (role.equals(MANAGER_ROLE)) hostname = seedHost;
		String localPort = role.equals(MANAGER_ROLE) ? seedPort : "0";
		
		// To use netty
		Config config = ConfigFactory.parseString("akka.actor.provider=\"cluster\"").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=" + hostname)).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=" + localPort)).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"" + seedNode + "\"]")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + role + "]")).
				withFallback(ConfigFactory.load());
		
		// To use artery
//		Config config = ConfigFactory.parseString("akka.actor.provider=\"cluster\"").
//				withFallback(ConfigFactory.parseString("akka.remote.artery.enabled=on")).
//				withFallback(ConfigFactory.parseString("akka.remote.artery.transport = aeron-udp")).
////				withFallback(ConfigFactory.parseString("akka.remote.artery.advanced.idle-cpu-level = 10")).
//				withFallback(ConfigFactory.parseString("akka.remote.artery.canonical.hostname=" + hostname)).
//				withFallback(ConfigFactory.parseString("akka.remote.artery.canonical.port=" + localPort)).
//				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"" + seedNode + "\"]")).
//				withFallback(ConfigFactory.parseString("akka.cluster.roles = [" + role + "]")).
//				withFallback(ConfigFactory.load());
		return ActorSystem.create(actorSystemName, config);
	}
}
