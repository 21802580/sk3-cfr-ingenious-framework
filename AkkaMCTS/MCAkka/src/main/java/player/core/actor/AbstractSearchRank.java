package player.core.actor;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.*;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;
import scala.concurrent.duration.FiniteDuration;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public abstract class AbstractSearchRank extends AbstractActor {
	protected PlayerParameters playerParameters;
	protected TranspositionTable transpositionTable;
	protected long rootHash;
	protected Board rootBoard;
	protected ActorRef clusterManager;
	private Set<Long> nodeHashBatch;
	private FiniteDuration genMoveDuration;
	private ActorRef nodeCounter;
	
	public AbstractSearchRank(PlayerParameters playerParameters, ActorRef clusterManager, ClusterMemberInitMessage initMessage) {
		this.playerParameters = playerParameters;
		this.clusterManager = clusterManager;
		this.nodeCounter = initMessage.getNodeCounter();
		transpositionTable = makeTranspositionTable();
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
		genMoveDuration = FiniteDuration.create(playerParameters.getTimeout() - 50, TimeUnit.MILLISECONDS);
		nodeHashBatch = new HashSet<Long>();
		transpositionTable.putIfAbsent(rootHash, rootBoard, playerParameters);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(MatchStartMessage.class, this::matchStarted)
				.match(MatchOverMessage.class, this::handleMatchOver)
				.match(GenMoveMessage.class, this::genMove)
				.match(ApplyMoveMessage.class, this::applyMove)
				.match(PlayerInitRequestMessage.class, this::replyToInit)
				.match(Message.class, this::processMessage)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void replyToInit(PlayerInitRequestMessage msg){
		getSender().tell(new PlayerInitResponseMessage(), getSelf());
	}
	
	protected void genMove(GenMoveMessage msg){
		transpositionTable.updateStartTime();
		Move m = msg.getOpponentMove();
		turnStart();
		if (m != null){
			updateRoot(m);
		}
		getContext().getSystem().scheduler().scheduleOnce(genMoveDuration,
				this::turnEnd, getContext().getSystem().dispatcher());
	}
	
	private void applyMove(ApplyMoveMessage msg){
		transpositionTable.updateStartTime();
		updateRoot(msg.getMove());
	}
	
	/*
	 * 	It's pointless to search a tree with a terminal root, and the MatchOverMessage is coming soon.
		Ignore the root update in this case to prevent weird behavior in DFUCT when searching from a terminal root.
	 */
	private void updateRoot(Move m){
		long newHash = Zobrist.getChildHash(rootHash, m, rootBoard.getCurrentPlayer());
		Board newBoard = new Board(rootBoard);
		newBoard.applyMove(m);
		if (newBoard.getWinner() == null) {
			// New root is non-terminal. Apply the update
			rootBoard = newBoard;
			rootHash = newHash;
			rootUpdated(m);
			transpositionTable.putIfAbsent(rootHash, rootBoard, playerParameters);
		}
	}

	protected void addNodeToBatch(long nodeHash){
		nodeHashBatch.add(nodeHash);
//		System.out.println(nodeHashBatch.size());
		if ((nodeHashBatch.size() >= playerParameters.getNodeBatchSize()) && (nodeCounter != null)){
			LinkedList<Long> toSend = new LinkedList<Long>(nodeHashBatch);
			nodeHashBatch = new HashSet<Long>();
			nodeCounter.tell(new NodeBatchMessage(toSend), getSelf());
		}
	}

	private void handleMatchOver(MatchOverMessage msg) {
//		transpositionTable.writeDotFile("/home/marc/" + playerParameters.getColour() + "_" + playerParameters.getType() + ".dot");
		matchOver(msg);
	}
	
	protected abstract TranspositionTable makeTranspositionTable();
	
	protected abstract void matchStarted(MatchStartMessage msg);
	
	protected abstract void turnStart();
	
	protected abstract void turnEnd();
	
	protected abstract void rootUpdated(Move m);

	protected abstract void matchOver(MatchOverMessage msg);
	
	protected abstract void processMessage(Message msg);
}
