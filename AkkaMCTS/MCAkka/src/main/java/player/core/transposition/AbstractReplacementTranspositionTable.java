package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

public abstract class AbstractReplacementTranspositionTable implements TranspositionTable {
	
	private TTEntry[] entries;
	private boolean shareableNodes;
	private int currentTurnStart;
	private int previousTurnStart;
	private int capacity;
	
	public AbstractReplacementTranspositionTable(int power, boolean shareableNodes){
		capacity = (int)Math.pow(2, power);
		currentTurnStart = 1;
		previousTurnStart = 1;
		this.shareableNodes = shareableNodes;
		entries = initEntries(capacity);
	}
	
	private LookupResult lookup(long hash, Board board, PlayerParameters playerParameters){
		//System.out.println("lookup " + hash);
		IndexProbeResult ipr = getIndex(hash);
		//System.out.println("indexProbeResult " + ipr);
		int index = ipr.foundIndex;
		if (index == 0) index = ipr.bestFitIndex;
		if (index == 0)	return new LookupResult(0, LookupResult.ReturnState.FULL);
		
		incrementLookupCount();
		//System.out.println("lookupCount " + getLookupCount());
		int lastAccess = entries[index].getLastAccess();
		//System.out.println("lastAccess " + lastAccess);
		//System.out.println("currentTurnStart " + currentTurnStart);
		while (lastAccess <= previousTurnStart){
			//System.out.println("lastAccess <= currentTurnStart");
			if (lastAccess == 0){
				lastAccess = entries[index].getLastAccess();
				//System.out.println("node busy");
				//System.out.println("getLastAccess " + lastAccess);
				continue;
			}
			if (ipr.foundIndex != 0){
				//System.out.println("Node found. Updating lastAccess");
				entries[index].CASLastAccess(lastAccess, getLookupCount());
			} else if (entries[index].CASLastAccess(lastAccess, 0)){
				//System.out.println("Node not found. Inserting");
				TreeNode node = makeNode(board, hash, playerParameters, shareableNodes);
				entries[index].setNode(node);
				entries[index].setLastAccess(getLookupCount());
			}
			lastAccess = entries[index].getLastAccess();
			//System.out.println("updated lastAccess " + lastAccess);
		}
		if (entries[index].getLastAccess() > previousTurnStart && entries[index].getHash() == hash){
			entries[index].setLastAccess(getLookupCount());
			//System.out.println("lastAccess > currentTurnStart ");
			//System.out.println("updated lastAccess " + entries[index].getLastAccess());
			return new LookupResult(index, LookupResult.ReturnState.FOUND);
		}
		return new LookupResult(0, LookupResult.ReturnState.BUSY);
	}
	
	public TreeNode get(long hash){
		IndexProbeResult result = getIndex(hash);
		if (result.foundIndex == 0){
			return null;
		}
		return entries[result.foundIndex].getNode();
	}

	public TreeNode putIfAbsent(long hash, Board board, PlayerParameters playerParameters) {
//		if ((playerParameters.getColour() == Colour.BLACK) && (getLookupCount() % 1000 == 0)) {
//			System.out.println(getLookupCount());
//		}
		//System.out.println("putIfAbsent " + hash);
		LookupResult result = lookup(hash, board, playerParameters);
		//System.out.println("lookupResult " + result + "\n");
		while (result.getState() == LookupResult.ReturnState.BUSY) {
			result = lookup(hash, board, playerParameters);
		}
		if (result.getState() == LookupResult.ReturnState.FULL){
			return null;
		}
		return entries[result.getIndex()].getNode();
	}
	
	public void updateStartTime(){
		previousTurnStart = currentTurnStart;
	 	currentTurnStart = getLookupCount();
	 	//System.out.println("updateStartTime " + currentTurnStart + "\n");
	}
	
	public IndexProbeResult getIndex(long hash){
		int index = (int)(hash & (capacity - 1));
		int bestFit = 0;
		int foundIndex = 0;
		int oldestElement = Integer.MAX_VALUE;
		for (int i = 0; i < 5; i++){
			index = Math.max(index, 1);
			TTEntry entry = entries[index];
			int lastAccess = entry.getLastAccess();
			if ((lastAccess == 1) && (oldestElement > 0)) {
				// First free index
				bestFit = index;
				oldestElement = 0;
			} else if ((lastAccess < previousTurnStart) && (lastAccess < oldestElement)){
				// Old element
				bestFit = index;
				oldestElement = lastAccess;
			}
			if ((entry.getHash() == hash) && (lastAccess > 1)) {
				// Found node
				foundIndex = index;
				break;
			}
			index = (index + 1) & (capacity - 1);
		}
		return new IndexProbeResult(bestFit, foundIndex);
	}
	
	public int getCapacity(){
		return capacity;
	}
	
	public void writeDotFile(String path){
	
	}
	
	public abstract int getLookupCount();
	
	public abstract void incrementLookupCount();
	
	public abstract TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable);
	
	public abstract TTEntry[] initEntries(int capacity);
	
	public class IndexProbeResult {
		public int bestFitIndex;
		public int foundIndex;
		
		public IndexProbeResult(int bestFitIndex, int foundIndex) {
			this.bestFitIndex = bestFitIndex;
			this.foundIndex = foundIndex;
		}
		
		public String toString() {
			return "bestFitIndex: " + bestFitIndex + " foundIndex: " + foundIndex;
		}
	}
}
