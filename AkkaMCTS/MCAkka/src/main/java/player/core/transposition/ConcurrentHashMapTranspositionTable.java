package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ConcurrentHashMapTranspositionTable extends AbstractMapTranspositionTable {
	private AtomicInteger nodesThisTurn;
	
	public ConcurrentHashMapTranspositionTable(int power, boolean sheareableNodes, int perTurnCapacity) {
		super(power, sheareableNodes, perTurnCapacity);
		nodesThisTurn = new AtomicInteger(0);
	}
	
	@Override
	protected ConcurrentHashMap<Long, TreeNode> makeTable(int capacity) {
		if (capacity == 0) {
			return new ConcurrentHashMap<Long, TreeNode>();
		}
		return new ConcurrentHashMap<Long, TreeNode>(capacity);
	}
	
	@Override
	protected void incNodesThisTurn() {
		nodesThisTurn.incrementAndGet();
	}
	
	@Override
	protected void zeroNodesThisTurn() {
		nodesThisTurn.set(0);
	}
	
	@Override
	protected int getNodesThisTurn() {
		return nodesThisTurn.get();
	}
	
	@Override
	public TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable) {
		return new TreeNode(board, hash, playerParameters,true, shareable);
	}
}
