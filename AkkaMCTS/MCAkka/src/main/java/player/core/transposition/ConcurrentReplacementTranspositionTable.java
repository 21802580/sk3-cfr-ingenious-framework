package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

import java.util.concurrent.atomic.LongAdder;

public class ConcurrentReplacementTranspositionTable extends AbstractReplacementTranspositionTable {
	private LongAdder lookupCount;
	
	public ConcurrentReplacementTranspositionTable(int power, boolean shareableNodes){
		super(power, shareableNodes);
		lookupCount = new LongAdder();
		lookupCount.increment();
	}
	
	@Override
	public int getLookupCount() {
		return lookupCount.intValue();
	}
	
	@Override
	public void incrementLookupCount() {
		lookupCount.increment();
	}
	
	@Override
	public TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable) {
		return new TreeNode(board, hash, playerParameters, true, shareable);
	}
	
	@Override
	public TTEntry[] initEntries(int capacity) {
		TTEntry[] entries = new AtomicTTEntry[capacity];
		for (int i = 0; i < entries.length; i++){
			entries[i] = new AtomicTTEntry(null, 1);
		}
		return entries;
	}
}
