package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.node.TreeNode;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

// NOTE: Number of nodes inserted is limited per turn
public abstract class AbstractMapTranspositionTable implements TranspositionTable {
	private Map<Long, TreeNode> table;
	private boolean shareableNodes;
	private int totalCapacity;
	private int perTurnCapacity;
	
	public static void main(String[] args) {
		System.out.println(Zobrist.getBoardHash(new Board()));
	}
	
	public AbstractMapTranspositionTable(int power, boolean sheareableNodes, int perTurnCapacityPower) {
		this.totalCapacity = power == 0 ? 0 : (int)Math.pow(2, power);
		this.perTurnCapacity = perTurnCapacityPower == 0 ? 0 : (int)Math.pow(2, perTurnCapacityPower);
		this.table = makeTable(totalCapacity);
		this.shareableNodes = sheareableNodes;
	}
	
	public void writeDotFile(String path) {
		FileWriter w = null;
		try {
			w = new FileWriter(path);
			w.write("digraph G {\n" +
					"    nodesep=0.3;\n" +
					"    ranksep=0.2;\n" +
					"    margin=0.1;\n" +
					"    node [shape=circle];\n" +
					"    edge [arrowsize=0.8];\n");
			int maxDepth = 4;
			int currentDepth = 0;
			Set<Long> alreadyChecked = new HashSet<Long>();
			Queue<TreeNode> nodeQueue = new LinkedList<TreeNode>();
			TreeNode rootNode = table.get(Board.startStateHash);
			nodeQueue.add(rootNode);
			nodeQueue.add(null);
			while(!nodeQueue.isEmpty()){
				TreeNode currentNode = nodeQueue.poll();
				if (currentNode == null){
					if (++currentDepth > maxDepth) break;
					nodeQueue.add(null);
					if (nodeQueue.peek() == null) break;
					else continue;
				}
				if (!alreadyChecked.add(currentNode.getHash())) {
					continue;
				}
				for (int i = 0; i < currentNode.getWindowSize(); i++) {
					Move m = currentNode.getMoveAt(i);
					long childHash = Zobrist.getChildHash(currentNode.getHash(), m, currentNode.getPlayerToMove());
					TreeNode child = table.get(childHash);
					if (child != null) {
						nodeQueue.add(child);
					}
					w.write(currentNode.getHash() + " -> " + childHash + ";\n");
				}
			}
			w.write("}");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (w != null) {
				try {
					w.close();
				} catch (IOException e) {
					// This is unrecoverable. Just report it and move on
					e.printStackTrace();
				}
			}
		}
	}
	
	@Override
	public TreeNode get(long hash) {
		return table.get(hash);
	}
	
	@Override
	public TreeNode putIfAbsent(long hash, Board board, PlayerParameters playerParameters) {
		if (table.containsKey(hash)) {
			return table.get(hash);
		} else if ((perTurnCapacity == 0) || (getNodesThisTurn() <= perTurnCapacity)){
			incNodesThisTurn();
			TreeNode n = makeNode(board, hash, playerParameters, shareableNodes);
			table.put(hash, n);
			return n;
		}
		return null;
	}
	
	@Override
	public void updateStartTime() {
		zeroNodesThisTurn();
	}
	
	@Override
	public int getCapacity() {
		return totalCapacity;
	}
	
	protected abstract Map<Long, TreeNode> makeTable(int capacity);
	
	protected abstract void incNodesThisTurn();
	
	protected abstract void zeroNodesThisTurn();
	
	protected abstract int getNodesThisTurn();
}
