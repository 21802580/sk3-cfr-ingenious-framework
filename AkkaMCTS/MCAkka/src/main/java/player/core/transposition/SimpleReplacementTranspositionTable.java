package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

public class SimpleReplacementTranspositionTable extends AbstractReplacementTranspositionTable {
	private int lookupCount;
	
	public SimpleReplacementTranspositionTable(int power, boolean shareableNodes){
		super(power, shareableNodes);
		lookupCount = 1;
	}
	
	@Override
	public int getLookupCount() {
		return lookupCount;
	}
	
	@Override
	public void incrementLookupCount() {
		lookupCount++;
	}
	
	@Override
	public TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable) {
		return new TreeNode(board, hash, playerParameters,false, shareable);
	}
	
	@Override
	public TTEntry[] initEntries(int capacity) {
		TTEntry[] entries = new SimpleTTEntry[capacity];
		for (int i = 0; i < entries.length; i++){
			entries[i] = new SimpleTTEntry(null, 1);
		}
		return entries;
	}
}
