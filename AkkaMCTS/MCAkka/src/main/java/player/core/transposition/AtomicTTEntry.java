package player.core.transposition;

import player.core.node.TreeNode;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicTTEntry implements TTEntry {
	private TreeNode node;
	private AtomicInteger lastAccess;
	
	public AtomicTTEntry(TreeNode node, int accessTime) {
		lastAccess = new AtomicInteger(accessTime);
		this.node = node;
	}
	
	public int getLastAccess(){
		return lastAccess.intValue();
	}
	
	@Override
	public boolean CASLastAccess(int expectedValue, int newValue) {
		return lastAccess.compareAndSet(expectedValue, newValue);
	}
	
	@Override
	public void setLastAccess(int value) {
		lastAccess.set(value);
	}
	
	public TreeNode getNode() {
		return node;
	}
	
	public long getHash(){
		if (node == null) return 0;
		return node.getHash();
	}
	
	public void setNode(TreeNode node){
		this.node = node;
	}
}
