package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

public interface TranspositionTable {
	TreeNode get(long hash);
	TreeNode putIfAbsent(long hash, Board board, PlayerParameters playerParameters);
	void updateStartTime();
	TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable);
	int getCapacity();
	void writeDotFile(String path);
}
