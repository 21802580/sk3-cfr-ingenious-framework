package player.core.transposition;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.node.TreeNode;

import java.util.HashMap;

public class HashMapTranspositionTable extends AbstractMapTranspositionTable {
	private int nodesThisTurn;
	
	public HashMapTranspositionTable(int power, boolean sheareableNodes, int perTurnCapacity) {
		super(power, sheareableNodes, perTurnCapacity);
		nodesThisTurn = 0;
	}
	
	@Override
	protected HashMap<Long, TreeNode> makeTable(int capacity) {
		if (capacity == 0) {
			return new HashMap<Long, TreeNode>();
		}
		return new HashMap<Long, TreeNode>(capacity);
	}
	
	@Override
	protected void incNodesThisTurn() {
		nodesThisTurn++;
	}
	
	@Override
	protected void zeroNodesThisTurn() {
		nodesThisTurn = 0;
	}
	
	@Override
	protected int getNodesThisTurn() {
		return nodesThisTurn;
	}
	
	@Override
	public TreeNode makeNode(Board board, long hash, PlayerParameters playerParameters, boolean shareable) {
		return new TreeNode(board, hash, playerParameters,false, shareable);
	}
}
