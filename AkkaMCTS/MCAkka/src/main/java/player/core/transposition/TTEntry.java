package player.core.transposition;

import player.core.node.TreeNode;

public interface TTEntry {
	
	TreeNode getNode();
	
	int getLastAccess();
	
	boolean CASLastAccess(int expectedValue, int newValue);
	
	void setLastAccess(int value);
	
	long getHash();
	
	void setNode(TreeNode node);
}
