package player.core.transposition;

import loa.gamelogic.Board;
import loa.gamelogic.Colour;
import loa.gamelogic.Coord;
import loa.gamelogic.Move;
import loa.movegeneration.MoveGenerator;

import java.util.Random;

public class Zobrist {
	private static long[][][] zobristBoard; // [row][column][colour]
	private static long blackToPlay;

	public static void main(String[] args) {
		Board b = new Board();
		long hash = getBoardHash(b);
		Move m = MoveGenerator.getRandomMove(b);
		long childHash = getChildHash(hash, m, Colour.BLACK);

	}

	static {
		zobristBoard = new long[8][8][3];
		Random r = new Random(12345);
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				for (int k = 0; k < 3; k++){
					long rand = Math.abs(r.nextLong());
					zobristBoard[i][j][k] = rand;
				}
			}
		}
		blackToPlay = Math.abs(r.nextLong());
	}
	
	public static long getBoardHash(Board board){
		long h = 0;
		for (int i = 0; i < 8; i++){
			for (int j = 0; j < 8; j++){
				Colour pieceColour = board.pieceAt(Coord.coords[i][j]);
				if (pieceColour != Colour.EMPTY){
					h = h ^ zobristBoard[i][j][pieceColour.ordinal()];
				}
			}
		}
		return board.getCurrentPlayer() == Colour.BLACK ? h ^ blackToPlay : h;
	}
	
	private static long getNewHash(long currentHash, Move move, Colour player, boolean undo){
		Coord from = move.getFrom();
		Coord to = move.getTo();
		currentHash ^= zobristBoard[from.getRow()][from.getCol()][player.ordinal()];
		currentHash ^= zobristBoard[to.getRow()][to.getCol()][player.ordinal()];
		if (move.isCapture()){
			Coord captureLocation = undo ? from : to;
			currentHash ^= zobristBoard[captureLocation.getRow()][captureLocation.getCol()][player.getOpponent().ordinal()];
		}
		return currentHash;
	}
	
	public static long getChildHash(long currentHash, Move move, Colour player){
		long newHash = getNewHash(currentHash, move, player, false);
		return newHash ^ blackToPlay;
	}
	
	public static long getParentHash(long currentHash, Move move, Colour player){
		long newHash = getNewHash(currentHash, move, player, true);
		return newHash ^ blackToPlay;
	}
	
	public static int getHomeProcessorIndex(long hash, int numProcessors, int ttCapacity){
		return (int)((hash / ttCapacity) % numProcessors);
	}
}
