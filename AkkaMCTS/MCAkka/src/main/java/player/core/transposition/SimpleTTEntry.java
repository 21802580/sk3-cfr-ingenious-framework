package player.core.transposition;

import player.core.node.TreeNode;

public class SimpleTTEntry implements  TTEntry{
	private TreeNode node;
	private int lastAccess;
	
	public SimpleTTEntry(TreeNode node, int lastAccess){
		this.node = node;
		this.lastAccess = lastAccess;
	}
	
	@Override
	public TreeNode getNode() {
		return node;
	}
	
	@Override
	public int getLastAccess() {
		return lastAccess;
	}
	
	@Override
	public boolean CASLastAccess(int expectedValue, int newValue) {
		lastAccess = newValue;
		return true;
	}
	
	@Override
	public void setLastAccess(int value) {
		lastAccess = value;
	}
	
	@Override
	public long getHash() {
		if (node == null) return 0;
		return node.getHash();
	}
	
	@Override
	public void setNode(TreeNode node) {
		this.node = node;
	}
}
