package player.core.transposition;

public class LookupResult {
	public enum ReturnState {FOUND, FULL, BUSY};
	private final int index;
	private final ReturnState state;
	
	public LookupResult(int index, ReturnState state) {
		this.index = index;
		this.state = state;
	}
	
	public ReturnState getState(){
		return state;
	}
	
	public int getIndex(){
		return index;
	}
	
	public String toString(){
		return "index: " + index + " returnState: " + state.name();
	}
}
