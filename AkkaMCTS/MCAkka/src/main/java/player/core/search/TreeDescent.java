package player.core.search;

import java.util.List;

public class TreeDescent {
	private List<Selection> path;
	private long nodeAdded;
	
	public TreeDescent(List<Selection> path, long nodeAdded) {
		this.path = path;
		this.nodeAdded = nodeAdded;
	}
	
	public List<Selection> getPath() {
		return path;
	}
	
	public long getAddedNode() {
		return nodeAdded;
	}
}
