package player.core.search;

public class Selection {
	private long nodeHash;
	private int selectedMoveIndex;
	
	public Selection(long nodeHash, int selectedMoveIndex) {
		this.selectedMoveIndex = selectedMoveIndex;
		this.nodeHash = nodeHash;
	}
	
	public long getNodeHash() {
		return nodeHash;
	}
	
	public int getSelectedMoveIndex() {
		return selectedMoveIndex;
	}

	public String toString() {
		return "(" + nodeHash + ": " + selectedMoveIndex + ")";
	}
}
