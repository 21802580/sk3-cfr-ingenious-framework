package player.core.search;

import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Colour;
import loa.gamelogic.Move;
import loa.movegeneration.MoveGenerator;
import loa.movegeneration.MoveValuePair;
import player.core.node.TreeNode;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class MCTS {
	public static final double DECISIVE_MOVE_VALUE = 10000000.0;

	public static Selection actionSelection(TreeNode node, PlayerParameters playerParameters){
		node.addVisits(1);
	    if (node.getTotalVisits() == 1) {
            return null;
        }
		int moveIndex = getBestUCBChild(node, playerParameters);
		node.addMoveSelections(moveIndex, 1);
		return new Selection(node.getHash(), moveIndex);
	}
	
	public static TreeDescent descendToLeaf(Board board, long rootHash, TranspositionTable transpositionTable, PlayerParameters playerParameters){
		List<Selection> path = new ArrayList<Selection>();
		long hash = rootHash;
		TreeNode node = null;
		while (board.getWinner() == null) { // While non-terminal
			node = transpositionTable.putIfAbsent(hash, board, playerParameters);
			if (node == null) return new TreeDescent(path, -1); // TT full -> end descent
			Selection s = actionSelection(node, playerParameters);
			if (s == null) break; // First visit for node -> end descent
			path.add(s);
			Move m = node.getMoveAt(s.getSelectedMoveIndex());
			hash = Zobrist.getChildHash(hash, m, board.getCurrentPlayer());
			board.applyMove(m);
		}
		if ((path.size() == 0) || (board.getWinner() != null)){
		    // Terminal state
			return new TreeDescent(path, -1);
		}
		return new TreeDescent(path, node.getHash());
	}
	
	private static int getBestUCBChild(TreeNode n, PlayerParameters playerParameters){
		int windowSize = n.getWindowSize();
		double bestUCB = Integer.MIN_VALUE;
		Map<Double, List<Integer>> indexMap = new HashMap<Double, List<Integer>>();
		for (int i = 0; i < windowSize && i < n.getNumChildren(); i++){
			double ucb = getUCB(n, i, playerParameters);
			List<Integer> indices = indexMap.computeIfAbsent(ucb, k -> new ArrayList<Integer>());
			indices.add(i);
			if (ucb > bestUCB){
				bestUCB = ucb;
			}
		}
		List<Integer> bestIndices = indexMap.get(bestUCB);
		return bestIndices.get(ThreadLocalRandom.current().nextInt(bestIndices.size()));
	}
	
	public static double getUCB(TreeNode n, int moveIndex, PlayerParameters playerParameters){
		double nodeVisits = (double) n.getTotalVisits();
		double childSelections = (double) n.getMoveSelections(moveIndex);
		double childScore = n.getMoveScore(moveIndex);
		double heuristicValue = n.getMoveHeuristicValue(moveIndex);
		double C = playerParameters.getUCBConstant();
		if (childSelections == 0){
			if (playerParameters.isFPU()){
				return playerParameters.getFpu();
			} else {
				return Integer.MAX_VALUE;
			}
		}
		double baseUCB = (childScore / childSelections) + (C * Math.sqrt(Math.log(nodeVisits) / childSelections));
		double biasTerm = playerParameters.getPbWeight() * (heuristicValue / (childSelections + 1));
		return baseUCB + biasTerm;
	}
	

	
	public static int getHighestVisitChild(TreeNode n){
		int windowSize = n.getWindowSize();
		int mostVisits = n.getMoveSelections(0);
		int bestMoveIndex = 0;
		for (int i = 1; i < windowSize && i < n.getNumChildren(); i++){
			int visits = n.getMoveSelections(i);
			if (visits > mostVisits){
				mostVisits = visits;
				bestMoveIndex = i;
			}
		}
		return bestMoveIndex;
	}
	
	public static double[] randomPlayout(Board board){
		int numMoves = 0;
		Colour winner = null;
		while (winner == null){
			Move move = MoveGenerator.getRandomMove(board);
			if (move == null){
				winner = board.getPendingPlayer();
			} else {
				board.applyMove(move);
				winner = board.getWinner();
				if (numMoves++ == 200){
					winner = Colour.EMPTY;
				}
			}
		}
		if (winner == Colour.BLACK){
			return new double[] {1.0, 0.0};
		} else if (winner == Colour.WHITE){
			return new double[] {0.0, 1.0};
		} else {
			return new double[] {0.5, 0.5};
		}
	}
	
	public static double[] heavyPlayout(Board board, PlayerParameters playerParameters){
		int numMoves = 0;
		Colour winner = null;
		while (winner == null){
			List<MoveValuePair> moves = MoveGenerator.generateMovesWithValues(board, playerParameters);
			Move move = null;
			double bestValue = 0.0;
			for (MoveValuePair mvp : moves) {
				if (mvp.getValue() > bestValue) move = mvp.getMove();
			}
			if (move == null){
				winner = board.getPendingPlayer();
			} else {
				board.applyMove(move);
				winner = board.getWinner();
				if (numMoves++ == 200){
					winner = Colour.EMPTY;
				}
			}
		}
		if (winner == Colour.BLACK){
			return new double[] {1.0, 0.0};
		} else if (winner == Colour.WHITE){
			return new double[] {0.0, 1.0};
		} else {
			return new double[] {0.5, 0.5};
		}
	}
	
	public static void backupAllRewards(List<Selection> path, double[] rewards, TranspositionTable tt){
		for (Selection s : path){
			TreeNode n = tt.get(s.getNodeHash());
			if (n != null){
				// Only update reward if node has not been over-written since descent
				updateReward(n, s.getSelectedMoveIndex(), rewards);
			}
		}
	}
	
	public static double updateReward(TreeNode node, int moveIndex, double[] rewards){
		double playerReward = rewards[node.getPlayerToMove().ordinal() - 1];
		node.addChildScore(moveIndex, playerReward);
		return playerReward;
	}
}
