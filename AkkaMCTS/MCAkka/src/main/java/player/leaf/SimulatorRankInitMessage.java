package player.leaf;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class SimulatorRankInitMessage implements ClusterMemberInitMessage {
	private ActorRef traverserRank;
	private ActorRef nodeCounter;
	
	public SimulatorRankInitMessage(ActorRef traverserRank, ActorRef nodeCounter) {
		this.traverserRank = traverserRank;
		this.nodeCounter = nodeCounter;
	}
	
	public ActorRef getTraverserRank() {
		return traverserRank;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
