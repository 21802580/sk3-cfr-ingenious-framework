package player.leaf;

import player.core.message.Message;
import player.core.search.Selection;

import java.util.List;

public class SimulationResponseMessage implements Message {
	private final List<Selection> path;
	private double[] rewards;
	private int numSimsPerformed;
	
	public SimulationResponseMessage(List<Selection> path, double[] rewards, int numSimsPerformed) {
		this.path = path;
		this.rewards = rewards;
		this.numSimsPerformed = numSimsPerformed;
	}
	
	public double[] getRewards() {
		return rewards;
	}
	
	public List<Selection> getPath() {
		return path;
	}
	
	public int getNumSimsPerformed() {
		return numSimsPerformed;
	}
}
