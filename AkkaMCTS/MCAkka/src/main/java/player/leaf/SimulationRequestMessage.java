package player.leaf;

import loa.gamelogic.Board;
import player.core.message.Message;
import player.core.search.Selection;

import java.util.List;

public class SimulationRequestMessage implements Message {
	private Board startState;
	private final List<Selection> path;
	
	public SimulationRequestMessage(Board startState, List<Selection> path) {
		this.startState = startState;
		this.path = path;
	}
	
	public Board getStartState() {
		return startState;
	}
	
	public List<Selection> getPath() {
		return path;
	}
}
