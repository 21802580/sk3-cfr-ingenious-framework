package player.leaf;

import akka.actor.ActorRef;
import commandline.PlayerParameters;
import player.core.actor.ClusterManager;
import player.core.message.ApplyMoveMessage;
import player.core.message.GenMoveMessage;
import player.core.message.SearchResultMessage;

public class LeafClusterManager extends ClusterManager {
	private ActorRef traverserRank;
	private ActorRef[] simulatorRanks;
	
	public LeafClusterManager(PlayerParameters playerParameters) {
		super(playerParameters);
		simulatorRanks = new ActorRef[playerParameters.getNumNodes() - 1];
	}
	
	@Override
	public SearchResultMessage searchResultReceived(SearchResultMessage msg) {
		traverserRank.tell(new ApplyMoveMessage(msg.getSelectedMove()), getSelf());
		return msg;
	}
	
	@Override
	public void clusterMemberJoined(ActorRef member) {
		if (numRegistrationsReceived == playerParameters.getNumNodes()){
			traverserRank = member;
			sendInitMessages();
		} else {
			simulatorRanks[numRegistrationsReceived - 1] = member;
		}
	}
	
	private void sendInitMessages(){
		for (ActorRef simulatorRank : simulatorRanks){
			simulatorRank.tell(new SimulatorRankInitMessage(traverserRank, nodeCounter), getSelf());
		}
		traverserRank.tell(new TraverserRankInitMessage(simulatorRanks, nodeCounter), getSelf());
	}
	
	@Override
	protected void genMove(GenMoveMessage msg) {
		traverserRank.tell(msg, getSelf());
	}
	
	@Override
	protected void allMembersInitialised() {
	
	}
}
