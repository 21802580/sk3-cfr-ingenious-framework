package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import player.core.search.MCTS;

public class LeafSimulator extends AbstractActor {
	private ActorRef replyTo;

	public LeafSimulator(ActorRef replyTo){
		this.replyTo = replyTo;
	}
	
	@Override
	public Receive createReceive(){
		return receiveBuilder()
				.match(SimulationRequestMessage.class, this::doPlayout)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void doPlayout(SimulationRequestMessage msg){
		double[] reward = MCTS.randomPlayout(msg.getStartState());
		replyTo.tell(new SimulationResponseMessage(msg.getPath(), reward, 1), getSelf());
	}
}
