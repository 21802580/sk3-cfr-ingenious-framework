package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import player.core.message.ApplyMoveMessage;
import player.core.message.NodeAddedMessage;
import player.core.search.MCTS;
import player.core.search.TreeDescent;
import player.core.transposition.TranspositionTable;
import player.core.transposition.Zobrist;

public class LeafTraverser extends AbstractActor {
	private TranspositionTable transpositionTable;
	private PlayerParameters playerParameters;
	private Board rootBoard;
	private long rootHash;
	private ActorRef traverserRank;
	
	public LeafTraverser(PlayerParameters playerParameters, TranspositionTable transpositionTable, ActorRef traverserRank){
		this.playerParameters = playerParameters;
		this.transpositionTable = transpositionTable;
		rootBoard = new Board();
		rootHash = Zobrist.getBoardHash(rootBoard);
		this.traverserRank = traverserRank;
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SimulationResponseMessage.class, this::backupRewards)
				.match(ApplyMoveMessage.class, this::updateRoot)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void updateRoot(ApplyMoveMessage msg){
		rootHash = Zobrist.getChildHash(rootHash, msg.getMove(), rootBoard.getCurrentPlayer());
		rootBoard.applyMove(msg.getMove());
	}
	
	private void backupRewards(SimulationResponseMessage message){
		MCTS.backupAllRewards(message.getPath(), message.getRewards(), transpositionTable);
		requestSimulation(getSender());
	}
	
	private void requestSimulation(ActorRef simulator){
		Board board = new Board(rootBoard);
		TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
		if (descent.getAddedNode() != -1){
			traverserRank.tell(new NodeAddedMessage(descent.getAddedNode()), getSelf());
		}
		simulator.tell(new SimulationRequestMessage(board, descent.getPath()), getSelf());
	}
}
