package player.leaf;

import akka.actor.ActorRef;
import player.core.message.ClusterMemberInitMessage;

public class TraverserRankInitMessage implements ClusterMemberInitMessage {
	private ActorRef[] simulators;
	private ActorRef nodeCounter;
	
	public TraverserRankInitMessage(ActorRef[] simulators, ActorRef nodeCounter) {
		this.simulators = simulators;
		this.nodeCounter = nodeCounter;
	}
	
	public ActorRef[] getSimulators() {
		return simulators;
	}
	
	@Override
	public ActorRef getNodeCounter() {
		return nodeCounter;
	}
}
