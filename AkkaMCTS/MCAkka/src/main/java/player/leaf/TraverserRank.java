package player.leaf;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.routing.Broadcast;
import akka.routing.SmallestMailboxPool;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.actor.AbstractSearchRank;
import player.core.message.*;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.TreeDescent;
import player.core.transposition.ConcurrentHashMapTranspositionTable;
import player.core.transposition.TranspositionTable;

public class TraverserRank extends AbstractSearchRank {
	private int numPlayouts;
	private boolean running;
	private ActorRef[] simulators;
	private ActorRef traverserRouter;
	
	public TraverserRank(PlayerParameters playerParameters, TraverserRankInitMessage initMessage, ActorRef clusterManager) {
		super(playerParameters, clusterManager, initMessage);
		running = true;
		int workerPoolSize = Math.max(1, playerParameters.getNumThreads() - 1);
		SmallestMailboxPool pool = new SmallestMailboxPool(workerPoolSize);
		traverserRouter = context().actorOf(pool.props(Props.create(LeafTraverser.class, playerParameters, transpositionTable, getSelf())));
		simulators = initMessage.getSimulators();
	}
	
	private void processSimulationResponse(SimulationResponseMessage msg){
		numPlayouts += msg.getNumSimsPerformed();
		if (running) traverserRouter.forward(msg, getContext());
	}

	@Override
	protected TranspositionTable makeTranspositionTable() {
		return new ConcurrentHashMapTranspositionTable(playerParameters.getTtPower(), false, playerParameters.getPerTurnCapacityPower());
	}
	
	@Override
	protected void matchOver(MatchOverMessage msg) {
		running = false;
	}
	
	@Override
	protected void matchStarted(MatchStartMessage message) {
		for (int i = 0; i < simulators.length; i++){
			Board board = new Board(rootBoard);
			TreeDescent descent = MCTS.descendToLeaf(board, rootHash, transpositionTable, playerParameters);
			if (descent.getAddedNode() != -1){
				addNodeToBatch(descent.getAddedNode());
			}
			simulators[i].tell(new SimulationRequestMessage(board, descent.getPath()), getSelf());
		}
	}
	
	@Override
	protected void turnStart() {
		numPlayouts = 0;
	}
	
	@Override
	protected void turnEnd() {
		TreeNode node = transpositionTable.get(rootHash);
		int moveIndex = MCTS.getHighestVisitChild(node);
		Move move = node.getMoveAt(moveIndex);
		clusterManager.tell(new SearchResultMessage(move, numPlayouts, 0, 0), getSelf());
	}
	
	@Override
	protected void rootUpdated(Move m) {
		traverserRouter.tell(new Broadcast(new ApplyMoveMessage(m)), getSelf());
	}
	
	@Override
	protected void processMessage(Message msg) {
		if (msg instanceof SimulationResponseMessage){
			processSimulationResponse((SimulationResponseMessage)msg);
		} else if (msg instanceof NodeAddedMessage){
			NodeAddedMessage m = (NodeAddedMessage) msg;
			addNodeToBatch(m.getNodeHash());
		}
	}
}
