package player.leaf;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.Props;
import commandline.PlayerParameters;
import loa.gamelogic.Board;

public class SimulatorRank extends AbstractActor {
	private ActorRef[] simulationWorkers;
	private ActorRef traverserRank;
	private int numPendingSimulations;
	private double[] totalReward;
	
	public SimulatorRank(PlayerParameters playerParameters, SimulatorRankInitMessage message){
		numPendingSimulations = 0;
		totalReward = new double[2];
		simulationWorkers = new ActorRef[playerParameters.getNumThreads()];
		traverserRank = message.getTraverserRank();
		for (int i = 0; i < simulationWorkers.length; i++){
			simulationWorkers[i] = context().actorOf(Props.create(LeafSimulator.class, getSelf()));
		}
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(SimulationRequestMessage.class, this::requestSimulations)
				.match(SimulationResponseMessage.class, this::accumulateRewards)
				.matchAny(this::unhandled)
				.build();
	}
	
	private void requestSimulations(SimulationRequestMessage msg){
		numPendingSimulations = simulationWorkers.length;
		totalReward[0] = 0.0;
		totalReward[1] = 0.0;
		for (int i = 0; i < simulationWorkers.length; i++){
			simulationWorkers[i].tell(new SimulationRequestMessage(new Board(msg.getStartState()), msg.getPath()), getSelf());
		}
	}
	
	private void accumulateRewards(SimulationResponseMessage msg){
		totalReward[0] += msg.getRewards()[0];
		totalReward[1] += msg.getRewards()[1];
		if (--numPendingSimulations == 0){
			double[] averageReward = { totalReward[0] / (double)simulationWorkers.length, totalReward[1] / (double)simulationWorkers.length };
			traverserRank.tell(new SimulationResponseMessage(msg.getPath(), averageReward, simulationWorkers.length), getSelf());
		}
	}
}
