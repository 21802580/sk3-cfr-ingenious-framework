package experimental;

import loa.gamelogic.Coord;
import loa.gamelogic.Move;

import java.util.ArrayList;
import java.util.List;

public class BitBoard {
	
	private final long whitePieces;
	private final long blackPieces;
	
	private byte[] numHorizontal;
	private byte[] numVertical;
	private byte[] numPositiveDiagonal;
	private byte[] numNegativeDiagonal;
	
	private static final byte[] DEFAULT_NUM_HOR_VER = {6, 2, 2, 2, 2, 2, 2, 6};
	private static final byte[] DEFAULT_NUM_DIAGONAL= {0, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 0};
	private static final long DEFAULT_BLACK = 0x7e0000000000007eL;
	private static final long DEFAULT_WHITE = 0x0081818181818100L;
	private static final long FULL_BOARD = 0xFFFFFFFFFFFFFFFFL;
	private static final long EMPTY_BOARD = 0x0000000000000000L;
/*
	private static class Direction {
		private
	}
	
	private enum DIRECTION {
		RIGHT(1), LEFT(-1), UP(-8), DOWN(8), UP_RIGHT(-7), UP_LEFT(-9), DOWN_RIGHT(9), DOWN_LEFT(7);
		
		private int modifier;
		
		DIRECTION(int modifier) {
			this.modifier = modifier;
		}
		
		public int getModifier() {
			return modifier;
		}
	}*/
	
	public static void main(String[] args){
			short line = 0x4200;
			
	}
	
	public BitBoard(){
		whitePieces = DEFAULT_WHITE;
		blackPieces = DEFAULT_BLACK;
		numHorizontal = DEFAULT_NUM_HOR_VER.clone();
		numVertical = DEFAULT_NUM_HOR_VER.clone();
		numPositiveDiagonal = DEFAULT_NUM_DIAGONAL.clone();
		numNegativeDiagonal = DEFAULT_NUM_DIAGONAL.clone();
		System.out.println(getMoves());
	}
	
	private List<Move> getMoves(){
		byte[] blackPositions = bitPositions(blackPieces);
		List<Move> moves = new ArrayList<Move>();
		for (byte position : blackPositions){
			moves.addAll(getMovesForPosition(position));
		}
		return moves;
	}
	
	private List<Move> getMovesForPosition(byte position){
		Coord from = getCoordFromPosition(position);
		List<Move> moves = new ArrayList<Move>();
		byte distance = numVertical[from.getCol()];
		byte destination = (byte)(position - 8);
		long emptyBoard = EMPTY_BOARD;
		return moves;
	}
	
	private static String printBitBoard(String title, long bits) {
		String s = String.format("%64s", Long.toBinaryString(bits)).replace(' ', '0');
		StringBuilder builder = new StringBuilder(title + "\n");
		int i = 1;
		for (char c : s.toCharArray()) {
			builder.append(c);
			if (i % 8 == 0) {
				builder.append("\n");
			}
			i++;
		}
		return builder + "\n";
	}
	
	private static byte[] bitPositions(long n) {
		byte[] result = new byte[Long.bitCount(n)];
		int i = 0;
		for (byte bit = 0; n != 0L; bit++) {
			if ((n & 1L) != 0) result[i++] = bit;
			n >>>= 1;
		}
		return result;
	}
	
	private Coord getCoordFromPosition(byte position){
		int row = (byte)(position / 8);
		int col = (byte)(position % 8);
		return Coord.coords[row][col];
	}
}