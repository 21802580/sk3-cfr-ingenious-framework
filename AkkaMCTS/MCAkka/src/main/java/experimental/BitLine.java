package experimental;

import loa.gamelogic.Colour;
import loa.gamelogic.Coord;
import loa.gamelogic.LineOrientation;
import loa.gamelogic.Move;
import loa.movegeneration.LineUtils;

public class BitLine {
	private static int[][][] masks; // [lineLength][pieceIndex][Colour]
	
	public static void main(String[] args){
		int[] rows = new int[8];
		rows[0] = Integer.parseInt("0001010101010100", 2);
		for (int i = 1; i < 7; i++){
			rows[i] = Integer.parseInt("1000000000000010", 2);
		}
		rows[7] = rows[0];
		int pd2 = Integer.parseInt("0000000000100001", 2);
		int nd2 = Integer.parseInt("0000000000010010", 2);
		int pd12 = Integer.parseInt("0000000000010010", 2);
		int nd12 = Integer.parseInt("0000000000100001", 2);
		System.out.println(getBitString(nd12));
		Move m = new Move(Coord.coords[0][5], Coord.coords[2][7], true);
		int newRow = applyMove(nd12, LineOrientation.NEGATIVE_DIAGONAL, 3, m, Colour.BLACK);
		System.out.println(getBitString(newRow));
		System.out.println(getBitString(masks[4][3][1]));
	}
	
	static {
		masks = new int[9][8][3]; // length 0 unused
		for (int length = 1; length <= 8; length++){
			for (int index = 0; index < length; index++){
				for (Colour c : Colour.values()){
					int piece = c.ordinal();
					int numBits = 2 * length;
					int shiftToZero = numBits - 2;
					int shiftDistance = shiftToZero - (2 * index);
					masks[length][index][piece] = piece << shiftDistance;
				}
			}
		}
	}
	
	public static int undoMove(int bitLine, LineOrientation orientation, int lineLength, Move m, Colour player){
		return updateBits(bitLine, orientation, lineLength, m, player, true);
	}
		
	public static int applyMove(int bitLine, LineOrientation orientation, int lineLength, Move m, Colour player){
		return updateBits(bitLine, orientation, lineLength, m, player, false);
	}
	
	private static int updateBits(int bitLine, LineOrientation orientation, int lineLength, Move m, Colour player, boolean undo){
		int rf = m.getFrom().getRow();
		int cf = m.getFrom().getCol();
		int rt = m.getTo().getRow();
		int ct = m.getTo().getCol();
		boolean capture = m.isCapture();
		int playerNum = player.ordinal();
		int fromIndex = LineUtils.getPieceIndexInLine(rf, cf, orientation);
		int toIndex = LineUtils.getPieceIndexInLine(rt, ct, orientation);
		int newLine = bitLine;
		int[][] lineMasks = masks[lineLength];
		newLine ^= lineMasks[toIndex][playerNum]; // xor in/out dest
		newLine ^= lineMasks[fromIndex][playerNum]; // xor in/out src
		if (capture){
			newLine ^= undo ? lineMasks[fromIndex][player.getOpponent().ordinal()] :
							lineMasks[toIndex][player.getOpponent().ordinal()];
		}
		return newLine;
	}

	public static String getBitString(int line){
		return String.format("%16s", Integer.toBinaryString(line)).replace(" ", "0");
	}
	
	public static int addPiece(int bitLine, LineOrientation orientation, int lineLength, int row, int col, Colour player){
		int playerNum = player.ordinal();
		int index = LineUtils.getPieceIndexInLine(row, col, orientation);
		return bitLine ^ masks[lineLength][index][playerNum];
	}
	
//
//	public static String getColourString(int bits, int length){
//		StringBuilder builder = new StringBuilder();
//		for (int i = 0; i < length; i++){
//			builder.append(getPieceAt(bits, length, i));
//			builder.append(" ");
//		}
//		return builder.toString();
//	}
//
//	public static Colour getPieceAt(int bits, int lineLength, int bitIndex){
//		int bitPos = 2 * bitIndex;
//		int shiftDistance = (2 * (lineLength - 1)) - bitPos;
//		int colourValue = (bits >> shiftDistance) & 3;
//		if (colourValue == Colour.BLACK.ordinal()){
//			return Colour.BLACK;
//		} else if (colourValue == Colour.WHITE.ordinal()){
//			return Colour.WHITE;
//		}
//		return Colour.EMPTY;
//	}
}
