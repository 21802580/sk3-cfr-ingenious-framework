//package experimental;
//
//import akka.actor.AbstractActor;
//import akka.actor.ActorRef;
//import akka.actor.ActorSystem;
//import akka.actor.Props;
//import loa.gamelogic.Board;
//
//public class TreeParallelSearchRank extends AbstractActor{
//	private Board rootState;
//	private ActorRef rootNode;
//	private int numParallelSearches;
//
//	public static void main(String[] args){
//		ActorSystem system = ActorSystem.create("testSystem");
//		ActorRef sr = system.actorOf(Props.create(TreeParallelSearchRank.class, 1)
//				.withDispatcher("akka.control-aware-dispatcher"), "searchRank");
//		sr.tell(new SearchStartMessage(new Board()), ActorRef.noSender());
//	}
//
//	public TreeParallelSearchRank(int numParallelSearches){
//		this.numParallelSearches = numParallelSearches;
//		//MoveMapInitialiser.initialiseMoveMap();
//	}
//
//	@Override
//	public Receive createReceive() {
//		return receiveBuilder()
//				.match(SearchStartMessage.class, this::processSearchStartMessage)
//				.match(PlayoutCompleteMessage.class, this::newPlayout)
//				.matchAny(this::unhandled)
//				.build();
//	}
//
//	private void newPlayout(PlayoutCompleteMessage msg){
//		//rootNode.tell(new MCTSIterationMessage(new Board(rootState)), getSelf());
//	}
//
//	private void processSearchStartMessage(SearchStartMessage msg){
//		rootState = msg.getBoard();
//		//rootNode = getContext().actorOf(Props.create(TreeNodeOld.class, rootState, true));
//		for (int i = 0; i < numParallelSearches; i++){
//			//rootNode.tell(new MCTSIterationMessage(new Board(rootState)), getSelf());
//		}
//	}
//}
