package experimental;

import loa.gamelogic.Move;
import player.core.message.Message;
import player.tds.TDSSelection;

import java.util.List;

public class TDSSearchMessageNew implements Message {
	private List<Move> movesMade;
	private List<TDSSelection> path;
	private long currentHash;
	
	public TDSSearchMessageNew(List<Move> movesMade, List<TDSSelection> path, long currentHash) {
		this.movesMade = movesMade;
		this.path = path;
		this.currentHash = currentHash;
	}
	
	public List<Move> getMovesMade() {
		return movesMade;
	}
	
	public List<TDSSelection> getPath() {
		return path;
	}
	
	public long getCurrentHash() {
		return currentHash;
	}
}
