package experimental;

import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import com.esotericsoftware.minlog.Log;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import player.core.message.ClusterMemberRegistrationMessage;
import player.core.message.Message;
import player.core.message.PlayerInitRequestMessage;
import player.dfuct.DFUCTSearchMessage;
import player.tds.TDSSearchMessage;

public class Receiver extends AbstractActor{
	private int numMessages;
	private int numReceived;
	private long startTime;
	Cluster cluster = Cluster.get(getContext().getSystem());
	private ActorRef sender;
	
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		Config config = ConfigFactory.parseString("akka.actor.provider=\"cluster\"").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=\"127.0.0.1\"")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=61234")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [\"receiver\"]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://testsystem@127.0.0.1:61234\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("testsystem", config);
		ActorRef receiver = system.actorOf(Props.create(Receiver.class, n), "receiver");
	}
	
	public Receiver(int numMessages){
		this.numMessages = numMessages;
	}
	
	@Override
	public AbstractActor.Receive createReceive() {
		return receiveBuilder()
				.match(ClusterMemberRegistrationMessage.class, this::sendReference)
				.match(DFUCTSearchMessage.class, this::receive)
				.match(TDSSearchMessage.class, this::receive)
				.match(TDSSearchMessageNew.class, this::receive)
				.match(PlayerInitRequestMessage.class, this::receive)
				.matchAny(this::unhandled)
				.build();
	}
	
	@Override
	public void preStart() {
		Log.info("Starting cluster node");
		cluster.subscribe(getSelf(), ClusterEvent.MemberUp.class);
	}
	
	@Override
	public void postStop() {
		Log.info("Cluster node exiting cluster");
		cluster.leave(cluster.selfAddress());
		getContext().system().terminate();
	}
	
	private void sendReference(ClusterMemberRegistrationMessage msg) {
		System.out.println("Registration request received");
		sender = getSender();
		getSender().tell(new ReceiverReferenceMessage(), getSelf());
	}
	
	private void receive(Message msg) {
		sender.tell(msg, getSelf());
		if (++numReceived == 1) {
			startTime = System.currentTimeMillis();
		}
		if (numReceived == numMessages) {
			if (msg instanceof DFUCTSearchMessage){
				System.out.println("DFUCTSearchMessage time = " + (System.currentTimeMillis() - startTime) + "ms");
			} else if (msg instanceof TDSSearchMessage){
				System.out.println("TDSSearchMessage time = " + (System.currentTimeMillis() - startTime) + "ms");
			} else if (msg instanceof TDSSearchMessageNew){
				System.out.println("TDSSearchMessageNew time = " + (System.currentTimeMillis() - startTime) + "ms");
			} else if (msg instanceof PlayerInitRequestMessage){
				System.out.println("PlayerInitRequestMessage time = " + (System.currentTimeMillis() - startTime) + "ms");
			}
			numReceived = 0;
		}
	}
}
