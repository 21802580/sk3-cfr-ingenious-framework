package experimental;

import loa.gamelogic.Board;
import loa.gamelogic.Colour;
import loa.gamelogic.LineOrientation;
import loa.gamelogic.Move;

import java.util.ArrayList;
import java.util.List;

import static loa.movegeneration.LineUtils.getLineLengthFromLinePos;
import static loa.movegeneration.LineUtils.getLinePositionFromCoord;

public class BitLineMoveGenerator {
	// table -> [colour][orientation][linePos][bitLine]
	private static List<Move>[][][][] moveTable;
	
	private int[] horizontal;
	private int[] vertical;
	private int[] positiveDiagonal;
	private int[] negativeDiagonal;
	
	static {
		populateMoveTable();
	}
	
	public static void main(String[] args){
		Board b = new Board();
		BitLineMoveGenerator gen = new BitLineMoveGenerator(b.getState());
		
	}
	
	public BitLineMoveGenerator(Colour[][] state){
		horizontal = new int[8];
		vertical = new int[8];
		positiveDiagonal = new int[15];
		negativeDiagonal = new int[15];
		populateLines(state);
	}
	
	private void populateLines(Colour[][] state) {
		for (int i = 0; i < state.length; i++){
			for (int j = 0; j < state.length; j++){
				Colour c = state[i][j];
				horizontal[i] = BitLine.addPiece(horizontal[i], LineOrientation.HORIZONTAL, 8, i, j, c);
				vertical[j] = BitLine.addPiece(vertical[j], LineOrientation.VERTICAL, 8, i, j, c);
				int positiveLinePos = getLinePositionFromCoord(i, j, LineOrientation.POSITIVE_DIAGONAL);
				int negativeLinePos = getLinePositionFromCoord(i, j, LineOrientation.NEGATIVE_DIAGONAL);
				int positiveLineLength = getLineLengthFromLinePos(positiveLinePos, LineOrientation.POSITIVE_DIAGONAL, null);
				int negativeLineLength = getLineLengthFromLinePos(negativeLinePos, LineOrientation.NEGATIVE_DIAGONAL, null);
				positiveDiagonal[positiveLinePos] = BitLine.addPiece(positiveDiagonal[positiveLinePos], LineOrientation.POSITIVE_DIAGONAL, positiveLineLength, i, j, c);
				negativeDiagonal[negativeLinePos] = BitLine.addPiece(negativeDiagonal[negativeLinePos], LineOrientation.NEGATIVE_DIAGONAL, negativeLineLength, i, j, c);
			}
		}
	}
	
	private static void populateMoveTable(){
		moveTable = new ArrayList[2][4][15][];
	}
	
	private static int getTableIndexForPlayer(Colour player){
		return player.ordinal() - 1;
	}
}
