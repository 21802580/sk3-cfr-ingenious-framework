//package player.mcts;
//
//import akka.actor.*;
//import experimental.SearchStartMessage;
//import loa.gamelogic.Board;
//
//import javax.swing.tree.TreeNodeOld;
//import java.io.Writer;
//
//public class ActorTree extends AbstractActor {
//	private int numParallelSearches;
//	private Board rootBoard;
//	private ActorRef rootNode;
//	private long startTime;
//	private int numPlayouts;
//	private ActorSystem system;
//	Writer writer = null;
//
//	public static void main(String[] args){
////		Config config = ConfigFactory.parseString("akka.actor.default-mailbox.mailbox-type = \"de.aktey.akka.visualmailbox.VisualMailboxType\"").
////                withFallback(ConfigFactory.load());
//		ActorSystem system = ActorSystem.create("testSystem");
//		ActorRef sr = system.actorOf(Props.create(ActorTree.class, 1), "searchRank");
//		sr.tell(new SearchStartMessage(new Board()), ActorRef.noSender());
//	}
//
//	public ActorTree(int numParallelSearches){
//		this.numParallelSearches = numParallelSearches;
//		//MoveMapInitialiser.initialiseMoveMap();
////		try {
////			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("/home/marc/tree.dot"), "utf-8"));
////			writer.write("digraph G {\n"
////					+ "    nodesep=0.3;\n"
////					+ "    ranksep=0.2;\n"
////					+ "    margin=0.1;\n"
////					+ "    node [shape=circle];\n"
////					+ "    edge [arrowsize=0.8];\n");
////		} catch (UnsupportedEncodingException e) {
////			e.printStackTrace();
////		} catch (FileNotFoundException e) {
////			e.printStackTrace();
////		} catch (IOException e) {
////			e.printStackTrace();
////		}
//
//	}
//
//	@Override
//	public Receive createReceive() {
//		return receiveBuilder()
//				.match(SearchStartMessage.class, this::processSearchStartMessage)
//				.match(PlayoutCompleteMessage.class, this::newPlayout)
////				.match(NewNodeMessage.class, this::addNodeToDotFile)
//				.matchAny(this::unhandled)
//				.build();
//	}
//
//	private void newPlayout(PlayoutCompleteMessage msg){
//		numPlayouts++;
//		if (System.currentTimeMillis() - startTime > 1000){
//			rootNode.tell(PoisonPill.getInstance(), getSelf());
//			System.out.println(numPlayouts);
////			try {
////				writer.write("}");
////				writer.close();
////			} catch (IOException e) {
////				e.printStackTrace();
////			}
//			return;
//		}
//		//rootNode.tell(new MCTSIterationMessage(new Board(rootBoard)), getSelf());
//	}
//
//	private void processSearchStartMessage(SearchStartMessage msg){
//		startTime = System.currentTimeMillis();
//		numPlayouts = 0;
//		rootBoard = msg.getBoard();
//		rootNode = getContext().actorOf(Props.create(TreeNodeOld.class, rootBoard, true));
//		for (int i = 0; i < numParallelSearches; i++){
//			//rootNode.tell(new MCTSIterationMessage(new Board(rootBoard)), getSelf());
//		}
//	}
//
////	private void addNodeToDotFile(NewNodeMessage msg) {
////		String parentHash = "\"" + msg.getParentHash() + "\"";
////		String childHash = "\"" + msg.getChildHash() + "\"";
////		try {
////			writer.write(parentHash + "->" + childHash + ";\n");
////		} catch (UnsupportedEncodingException e) {
////			// TODO Auto-generated catch bloc
////			e.printStackTrace();
////		} catch (FileNotFoundException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		} catch (IOException e) {
////			// TODO Auto-generated catch block
////			e.printStackTrace();
////		}
////	}
//}
