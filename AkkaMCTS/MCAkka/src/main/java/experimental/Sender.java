package experimental;

import akka.actor.*;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent;
import akka.cluster.Member;
import com.esotericsoftware.minlog.Log;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import commandline.PlayerParameters;
import loa.gamelogic.Board;
import loa.gamelogic.Move;
import player.core.message.ClusterMemberRegistrationMessage;
import player.core.message.Message;
import player.core.message.PlayerInitRequestMessage;
import player.core.node.TreeNode;
import player.core.search.MCTS;
import player.core.search.Selection;
import player.core.transposition.Zobrist;
import player.dfuct.DFUCTActionSelection;
import player.dfuct.DFUCTSearchMessage;
import player.dfuct.DFUCTSelectionResult;
import player.dfuct.DFUCTStackNode;
import player.tds.TDSSearchMessage;
import player.tds.TDSSelection;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;

public class Sender extends AbstractActor {
	private int numMessages;
	Cluster cluster = Cluster.get(getContext().getSystem());
	private int numReceived;
	private ActorRef receiver;
	private Message[] messages;
	private int messageIndex;
	
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		Config config = ConfigFactory.parseString("akka.actor.provider=\"cluster\"").
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.hostname=\"127.0.0.1\"")).
				withFallback(ConfigFactory.parseString("akka.remote.netty.tcp.port=0")).
				withFallback(ConfigFactory.parseString("akka.cluster.roles = [\"sender\"]")).
				withFallback(ConfigFactory.parseString("akka.cluster.seed-nodes = [\"akka.tcp://testsystem@127.0.0.1:61234\"]")).
				withFallback(ConfigFactory.load());
		ActorSystem system = ActorSystem.create("testsystem", config);
		ActorRef sender = system.actorOf(Props.create(Sender.class, n), "sender");
	}
	
	public Sender(int numMessages){
		this.numMessages = numMessages;
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(ClusterEvent.MemberUp.class, this::tryRegistration)
				.match(ReceiverReferenceMessage.class, this::sendMessages)
				.match(DFUCTSearchMessage.class, this::receive)
				.match(TDSSearchMessage.class, this::receive)
				.match(TDSSearchMessageNew.class, this::receive)
				.match(PlayerInitRequestMessage.class, this::receive)
				.matchAny(this::unhandled)
				.build();
	}
	
	@Override
	public void preStart() {
		Log.info("Starting cluster node");
		cluster.subscribe(getSelf(), ClusterEvent.MemberUp.class);
	}
	
	@Override
	public void postStop() {
		Log.info("Cluster node exiting cluster");
		cluster.leave(cluster.selfAddress());

	}
	
	private void tryRegistration(ClusterEvent.MemberUp msg) {
		Member member = msg.member();
		Log.info("Member up: " + member.address());
		if (member.hasRole("receiver")) {
			Log.info("Requesting receiver reference");
			ActorSelection selection = getContext().actorSelection(member.address() + "/user/receiver");
			selection.tell(new ClusterMemberRegistrationMessage(), getSelf());
		}
	}
	
	private void sendMessages(ReceiverReferenceMessage msg){
		messageIndex = 0;
		messages = new Message[4];
		receiver = getSender();
		Log.info("Receiver ref received: " + receiver.path());
		Log.info("Sending messages");
		DFUCTSearchMessage dfuct = getDFUCTSearchMessage();
		TDSSearchMessage tds = getTDSSearchMessage();
		TDSSearchMessageNew tdsn = getTDSSearchMessageNew();
		PlayerInitRequestMessage empty = new PlayerInitRequestMessage();
		messages[0] = dfuct;
		messages[1] = tds;
		messages[2] = tdsn;
		messages[3] = empty;
		receiver.tell(dfuct, getSelf());
	}
	
	private DFUCTSearchMessage getDFUCTSearchMessage(){
		Deque<DFUCTStackNode> stack = new ArrayDeque<DFUCTStackNode>();
		Board b = new Board();
		long hash = Zobrist.getBoardHash(b);
		TreeNode node = new TreeNode(b, hash, new PlayerParameters(), true, false);
		node.addVisits(6);
		for (int i = 0; i < 15; i++){
			DFUCTSelectionResult res = DFUCTActionSelection.selectActions(node, new PlayerParameters());
			Move m = node.getMoveAt(res.getSelectedMoveIndex());
			DFUCTStackNode stackNode = new DFUCTStackNode(hash, 0, res, b.getCurrentPlayer(), m);
			stack.push(stackNode);
			hash = Zobrist.getChildHash(hash, m, b.getCurrentPlayer());
			b.applyMove(m);
			node = new TreeNode(b, hash, new PlayerParameters(), true, false);
			node.addVisits(6);
		}
		return new DFUCTSearchMessage(stack, b, hash);
	}
	
	private TDSSearchMessage getTDSSearchMessage(){
		List<TDSSelection> path = new ArrayList<TDSSelection>();
		Board b = new Board();
		long hash = Zobrist.getBoardHash(b);
		TreeNode node = new TreeNode(b, hash, new PlayerParameters(), true, false);
		node.addVisits(6);
		for (int i = 0; i < 15; i++){
			Selection s = MCTS.actionSelection(node, new PlayerParameters());
			Move m = node.getMoveAt(s.getSelectedMoveIndex());
			TDSSelection t = new TDSSelection(hash, s.getSelectedMoveIndex(), 1);
			path.add(t);
			hash = Zobrist.getChildHash(hash, m, b.getCurrentPlayer());
			b.applyMove(m);
			node = new TreeNode(b, hash, new PlayerParameters(), true, false);
			node.addVisits(6);
		}
		return new TDSSearchMessage(b, path, hash, 0);
	}
	
	private TDSSearchMessageNew getTDSSearchMessageNew(){
		List<TDSSelection> path = new ArrayList<TDSSelection>();
		List<Move> movesMade = new ArrayList<Move>();
		Board b = new Board();
		long hash = Zobrist.getBoardHash(b);
		TreeNode node = new TreeNode(b, hash, new PlayerParameters(), true, false);
		node.addVisits(6);
		for (int i = 0; i < 15; i++){
			Selection s = MCTS.actionSelection(node, new PlayerParameters());
			Move m = node.getMoveAt(s.getSelectedMoveIndex());
			TDSSelection t = new TDSSelection(hash, s.getSelectedMoveIndex(), 1);
			path.add(t);
			movesMade.add(m);
			hash = Zobrist.getChildHash(hash, m, b.getCurrentPlayer());
			b.applyMove(m);
			node = new TreeNode(b, hash, new PlayerParameters(), true, false);
			node.addVisits(6);
		}
		return new TDSSearchMessageNew(movesMade, path, hash);
	}
	
	private void receive(Message msg){
		if (++numReceived == numMessages){
			messageIndex++;
			numReceived = 0;
		}
		if (messageIndex < messages.length) receiver.tell(messages[messageIndex], getSelf());
	}
}
