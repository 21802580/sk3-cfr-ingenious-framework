package experimental;

import loa.gamelogic.Board;
import player.core.message.Message;

public class SearchStartMessage implements Message {
	private final Board board;
	
	
	public SearchStartMessage(Board board) {
		this.board = board;
	}
	
	public Board getBoard() {
		return board;
	}
}
