package match;

public enum WinReason {
	COMPLETED("completed"),
	OPPONENT_TIMEOUT("timeout"),
	OPPONENT_ILLEGAL("illegal");
	
	private final String stringRep;
	
	WinReason(String stringRep) {
		this.stringRep = stringRep;
	}
	
	@Override
	public String toString() {
		return stringRep;
	}
}
