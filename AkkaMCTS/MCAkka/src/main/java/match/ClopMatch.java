package match;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import commandline.PlayerParameters;
import player.core.actor.ActorSystemFactory;
import player.local.leaf.LocalLeafParallelSearcher;
import player.local.serial.LocalSerialManager;
import player.local.tree.LocalTreeParallelSearcher;

public class ClopMatch {
	
	public static void main (String[] args){
		PlayerParameters[] playerParams = PlayerParameters.splitArguments(args);
		PlayerParameters blackParams = playerParams[0];
		PlayerParameters whiteParams = playerParams[1];
		ActorSystem blackSystem = ActorSystemFactory.makeLocalActorSystem(blackParams);
		ActorSystem whiteSystem = ActorSystemFactory.makeLocalActorSystem(whiteParams);
		ActorRef black = makePlayer(blackParams, blackSystem);
		ActorRef white = makePlayer(whiteParams, whiteSystem);
		MatchResult result = Referee.playMatch(black, white, blackParams.getTimeout(), whiteParams.getTimeout());
		printResult(result, blackParams.getTimeout(), whiteParams.getTimeout());
		blackSystem.terminate();
		whiteSystem.terminate();
	}
	
	public static void printResult(MatchResult result, int blackTime, int whiteTime){
		System.out.println(result.getWinner());
	}
	
	private static ActorRef makePlayer(PlayerParameters playerParams, ActorSystem system){
		ActorRef player = null;
		switch(playerParams.getType()){
			case "leaf":
				player = system.actorOf(Props.create(LocalLeafParallelSearcher.class, playerParams));
				break;
			case "tree":
				player = system.actorOf(Props.create(LocalTreeParallelSearcher.class, playerParams));
				break;
			case "serial":
				player = system.actorOf(Props.create(LocalSerialManager.class, playerParams));
				break;
		}
		return player;
	}
	
}
