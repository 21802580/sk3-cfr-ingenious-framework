package match;

import akka.actor.ActorRef;
import akka.pattern.Patterns;
import akka.util.Timeout;
import loa.gamelogic.Board;
import loa.gamelogic.Colour;
import loa.gamelogic.Move;
import player.core.message.*;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class Referee {
	private static List<Integer> totalPlayoutsBlack;
	private static List<Integer> totalPlayoutsWhite;
	private static List<Integer> totalNodesBlack;
	private static List<Integer> totalNodesWhite;
	private static List<Integer> reportsAtRootBlack;
	private static List<Integer> reportsAtRootWhite;
	
	public static MatchResult playMatch(ActorRef black, ActorRef white, int blackTimeout, int whiteTimeout){
		startPlayers(black, white);
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Board board = new Board();
		ActorRef currentRef = black;
		Colour currentColour = Colour.BLACK;
		int currentTimeout = blackTimeout;
		List<Integer> currentPlayoutList = totalPlayoutsBlack;
		List<Integer> currentNodesList = totalNodesBlack;
		List<Integer> currentReportsList = reportsAtRootBlack;
		Move lastMove = null;
		while(board.getWinner() == null){
			Move m = requestMove(currentRef, currentTimeout, lastMove, currentPlayoutList, currentNodesList, currentReportsList);
			if (m == null){
				return endMatch(black, white, currentColour.getOpponent(), WinReason.OPPONENT_TIMEOUT);
			} else if (!board.moveLegal(m)){
				return endMatch(black, white, currentColour.getOpponent(), WinReason.OPPONENT_ILLEGAL);
			} else {
				board.applyMove(m);
				System.out.println("MOVE MADE: " + m + "\n" + board);////
				lastMove = m;
				if (currentColour == Colour.BLACK){
					currentColour = Colour.WHITE;
					currentRef = white;
					currentTimeout = whiteTimeout;
					currentPlayoutList = totalPlayoutsWhite;
					currentNodesList = totalNodesWhite;
					currentReportsList = reportsAtRootWhite;
				} else {
					currentColour = Colour.BLACK;
					currentRef = black;
					currentTimeout = blackTimeout;
					currentPlayoutList = totalPlayoutsBlack;
					currentNodesList = totalNodesBlack;
					currentReportsList = reportsAtRootBlack;
				}
			}
		}
		return endMatch(black, white, board.getWinner(), WinReason.COMPLETED);
	}
	
	private static void startPlayers(ActorRef black, ActorRef white){
		initPlayer(black, Colour.BLACK);
		initPlayer(white, Colour.WHITE);
		black.tell(new MatchStartMessage(), ActorRef.noSender());
		white.tell(new MatchStartMessage(), ActorRef.noSender());
		totalPlayoutsBlack = new ArrayList<Integer>();
		totalPlayoutsWhite = new ArrayList<Integer>();
		totalNodesBlack = new ArrayList<Integer>();
		totalNodesWhite = new ArrayList<Integer>();
		reportsAtRootBlack = new ArrayList<Integer>();
		reportsAtRootWhite = new ArrayList<Integer>();
	}
	
	private static void initPlayer(ActorRef player, Colour colour){
		System.out.println("Init player " + colour);////
		Timeout t = new Timeout(Duration.create(30, TimeUnit.SECONDS));
		Future<Object> fut = Patterns.ask(player, new PlayerInitRequestMessage(), t);
		try {
			Await.result(fut, t.duration());
		} catch (Exception e) {
//			System.out.println("Player " + colour + " failed to initialize");
			System.exit(0);
		}
	}
	
	private static Move requestMove(ActorRef player, int timeout, Move lastMove, List<Integer> currentPlayoutList, List<Integer> currentNodesList, List<Integer> currentReportsList) {
		Timeout t = new Timeout(Duration.create(10, TimeUnit.MINUTES));
        Future<Object> fut = Patterns.ask(player, new GenMoveMessage(lastMove), t);
        FinalMoveMessage result;
        try {
            result = (FinalMoveMessage) Await.result(fut, t.duration());
        } catch (Exception e) {
            return null;
        }
        currentPlayoutList.add(result.getSearchResult().getTotalPlayouts());
//        System.out.println(result.getSearchResult().getTotalPlayouts());
        currentNodesList.add(result.getNumNodes());
        currentReportsList.add(result.getSearchResult().getReportsAtRoot());
//        System.out.println("Playouts: " + result.getSearchResult().getTotalPlayouts());
//        System.out.println("Nodes: " + result.getNumNodes());
        return result.getSearchResult().getSelectedMove();
	}
	
	private static void stopPlayer(ActorRef player){
//		System.out.println("stopPlayer");
		player.tell(new MatchOverMessage(), ActorRef.noSender());
	}
	
    private static MatchResult endMatch(ActorRef black, ActorRef white, Colour winner, WinReason winReason) {
//		System.out.println("Referee: End match");
		stopPlayer(black);
		stopPlayer(white);
		return new MatchResult(winner, winReason, totalPlayoutsBlack, totalPlayoutsWhite, totalNodesBlack, totalNodesWhite, reportsAtRootBlack, reportsAtRootWhite);
    }
}
