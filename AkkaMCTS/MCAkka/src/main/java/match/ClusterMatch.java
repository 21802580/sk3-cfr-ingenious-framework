package match;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import commandline.PlayerParameters;
import player.core.actor.ActorSystemFactory;
import player.dfuct.DFUCTClusterManager;
import player.leaf.LeafClusterManager;
import player.root.RootClusterManager;
import player.serial.SerialClusterManager;
import player.tds.TDSClusterManager;
import player.treesplit.TreesplitClusterManager;

import java.util.List;

public class ClusterMatch {
	
	public static void main (String[] args){
		PlayerParameters[] playerParams = PlayerParameters.splitArguments(args);
		PlayerParameters blackParams = playerParams[0];
		PlayerParameters whiteParams = playerParams[1];
		ActorSystem blackSystem = ActorSystemFactory.makeClusterActorSystem(blackParams, "ClusterManager");
		ActorSystem whiteSystem = ActorSystemFactory.makeClusterActorSystem(whiteParams, "ClusterManager");
		ActorRef black = makeClusterManager(blackParams, blackSystem);
		ActorRef white = makeClusterManager(whiteParams, whiteSystem);
		MatchResult result = Referee.playMatch(black, white, blackParams.getTimeout(), whiteParams.getTimeout());
		printResult(result, blackParams.getName(), whiteParams.getName(), blackParams.getTimeout(), whiteParams.getTimeout());
	}
	
	public static void printResult(MatchResult result, String bName, String wName, int blackTime, int whiteTime){
		StringBuilder b = new StringBuilder();
		List<Integer> blackTotalPlayouts = result.getTotalPlayoutsBlack();
		List<Integer> whiteTotalPlayouts = result.getTotalPlayoutsWhite();
		List<Integer> nodesAddedBlack = result.getNodesAddedBlack();
		List<Integer> nodesAddedWhite = result.getNodesAddedWhite();
		List<Integer> reportsBlack = result.getReportsAtRootBlack();
		List<Integer> reportsWhite = result.getReportsAtRootWhite();
		int numMoves = Math.min(blackTotalPlayouts.size(), whiteTotalPlayouts.size());
		b.append(bName);
		b.append(" ");
		b.append(wName);
		b.append("\n");
		b.append(result.getWinner());
		b.append("\n");
		b.append(result.getWinReason());
		b.append("\n");
		b.append(numMoves);
		b.append("\n");
		for (int i = 0; i < numMoves; i++){
			double ppsb = (double)blackTotalPlayouts.get(i) / ((double)blackTime / 1000.0);
			double ppsw = (double)whiteTotalPlayouts.get(i) / ((double)whiteTime / 1000.0);
			b.append(ppsb);
			b.append(" ");
			b.append(nodesAddedBlack.get(i));
			b.append(" ");
			b.append(reportsBlack.get(i));
			b.append(" ");
			b.append(ppsw);
			b.append(" ");
			b.append(nodesAddedWhite.get(i));
			b.append(" ");
			b.append(reportsWhite.get(i));
			b.append("\n");
		}
		System.out.println(b);
	}
	
	
	private static ActorRef makeClusterManager(PlayerParameters playerParams, ActorSystem system){
		ActorRef player = null;
		String managerName = playerParams.getColour() + ActorSystemFactory.MANAGER_ROLE;
		switch(playerParams.getType()){
			case "serial":
				player = system.actorOf(Props.create(SerialClusterManager.class, playerParams), managerName);
				break;
			case "leaf":
				player = system.actorOf(Props.create(LeafClusterManager.class, playerParams), managerName);
				break;
			case "tds":
				player = system.actorOf(Props.create(TDSClusterManager.class, playerParams), managerName);
				break;
			case "dfuct":
				player = system.actorOf(Props.create(DFUCTClusterManager.class, playerParams), managerName);
				break;
			case "treesplit":
				player = system.actorOf(Props.create(TreesplitClusterManager.class, playerParams), managerName);
				break;
			case "roottree":
				player = system.actorOf(Props.create(RootClusterManager.class, playerParams), managerName);
				break;
			case "rootleaf":
				player = system.actorOf(Props.create(RootClusterManager.class, playerParams), managerName);
				break;
		}
		return player;
	}
}
