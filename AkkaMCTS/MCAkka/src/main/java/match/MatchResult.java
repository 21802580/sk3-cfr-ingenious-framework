package match;

import loa.gamelogic.Colour;

import java.util.List;

public class MatchResult {
	private Colour winner;
	private WinReason winReason;
	private List<Integer> totalPlayoutsBlack;
	private List<Integer> totalPlayoutsWhite;
	private List<Integer> nodesAddedBlack;
	private List<Integer> nodesAddedWhite;
	private List<Integer> reportsAtRootBlack;
	private List<Integer> reportsAtRootWhite;
	
	public MatchResult(Colour winner, WinReason winReason, List<Integer> totalPlayoutsBlack, List<Integer> totalPlayoutsWhite, List<Integer> nodesAddedBlack, List<Integer> nodesAddedWhite, List<Integer> reportsAtRootBlack, List<Integer> reportsAtRootWhite) {
		this.winner = winner;
		this.winReason = winReason;
		this.totalPlayoutsBlack = totalPlayoutsBlack;
		this.totalPlayoutsWhite = totalPlayoutsWhite;
		this.nodesAddedBlack = nodesAddedBlack;
		this.nodesAddedWhite = nodesAddedWhite;
		this.reportsAtRootBlack = reportsAtRootBlack;
		this.reportsAtRootWhite = reportsAtRootWhite;
	}
	
	public Colour getWinner() {
		return winner;
	}
	
	public WinReason getWinReason() {
		return winReason;
	}
	
	public List<Integer> getTotalPlayoutsBlack() {
		return totalPlayoutsBlack;
	}
	
	public List<Integer> getTotalPlayoutsWhite() {
		return totalPlayoutsWhite;
	}
	
	public List<Integer> getNodesAddedBlack() {
		return nodesAddedBlack;
	}
	
	public List<Integer> getNodesAddedWhite() {
		return nodesAddedWhite;
	}
	
	public List<Integer> getReportsAtRootBlack() {
		return reportsAtRootBlack;
	}
	
	public List<Integer> getReportsAtRootWhite() {
		return reportsAtRootWhite;
	}
}
