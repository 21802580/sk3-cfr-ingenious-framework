package loa.quad;

import loa.gamelogic.Colour;
import loa.gamelogic.Move;

import java.io.Serializable;

public class QuadTable implements Serializable{
    private Quad[][] whiteQuads;
    private Quad[][] blackQuads;

    private int[] whiteTypes;
    private int[] blackTypes;

    public QuadTable() {
        whiteQuads = new Quad[9][9];
        blackQuads = new Quad[9][9];
        whiteTypes = new int[6];
        blackTypes = new int[6];
        populateBlack();
        populateWhite();
    }

    public QuadTable(QuadTable q) {
        whiteQuads = new Quad[9][9];
        blackQuads = new Quad[9][9];
        whiteTypes = new int[6];
        blackTypes = new int[6];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                whiteQuads[i][j] = q.whiteQuads[i][j];
                blackQuads[i][j] = q.blackQuads[i][j];
            }
            if (i < 6) {
                blackTypes[i] = q.blackTypes[i];
                whiteTypes[i] = q.whiteTypes[i];
            }
        }
    }
    
    private void updateQuad(int row, int col, Colour colour){
    	Quad[][] quads = getQuads(colour);
    	int[] types = getQuadTypes(colour);
		int currentPosition = 3;
		for (int r = row; r < row + 2; r++) {
			for (int c = col; c < col + 2; c++) {
				types[quads[r][c].getCategory()]--;
				quads[r][c] = quads[r][c].addOrRemoveAt(currentPosition);
				types[quads[r][c].getCategory()]++;
				currentPosition--;
			}
		}
	}
	
	private Quad[][] getQuads(Colour colour){
		return colour == Colour.BLACK ? blackQuads : whiteQuads;
	}
	
	private int[] getQuadTypes(Colour colour){
		return colour == Colour.BLACK ? blackTypes : whiteTypes;
	}

    public void applyOrUndoMove(Move m, Colour colour) {
        int srcRow = m.getFrom().getRow();
        int srcCol = m.getFrom().getCol();
        int destRow = m.getTo().getRow();
        int destCol = m.getTo().getCol();
        boolean capture = m.isCapture();
        updateQuad(srcRow, srcCol, colour);
		updateQuad(destRow, destCol, colour);
		if (capture) updateQuad(destRow, destCol, colour.getOpponent());
    }

    public double getEuler(Colour colour) {
        if (colour == Colour.BLACK) {
			return ((double) blackTypes[1] - (double) blackTypes[3] - (2 * (double) blackTypes[5])) / 4.0;
        } else {
            return ((double) whiteTypes[1] - (double) whiteTypes[3] - (2 * (double) whiteTypes[5])) / 4.0;
        }
    }

    private void populateBlack() {
        blackQuads[0][0] = Quad.EMPTY_0;
        blackQuads[0][1] = Quad.BOTTOM_RIGHT_1;
        blackQuads[1][0] = Quad.EMPTY_0;
        blackQuads[1][1] = Quad.TOP_RIGHT_1;
        for (int i = 2; i < 7; i++) {
            blackQuads[0][i] = Quad.BOTTOM_2;
            blackQuads[1][i] = Quad.TOP_2;
        }
        blackQuads[0][7] = Quad.BOTTOM_LEFT_1;
        blackQuads[0][8] = Quad.EMPTY_0;
        blackQuads[1][7] = Quad.TOP_LEFT_1;
        blackQuads[1][8] = Quad.EMPTY_0;

        for (int i = 2; i < 7; i++) {
            for (int j = 0; j < 9; j++) {
                blackQuads[i][j] = Quad.EMPTY_0;
            }
        }

        blackQuads[7][0] = Quad.EMPTY_0;
        blackQuads[7][1] = Quad.BOTTOM_RIGHT_1;
        blackQuads[8][0] = Quad.EMPTY_0;
        blackQuads[8][1] = Quad.TOP_RIGHT_1;
        for (int i = 2; i < 7; i++) {
            blackQuads[7][i] = Quad.BOTTOM_2;
            blackQuads[8][i] = Quad.TOP_2;
        }
        blackQuads[7][7] = Quad.BOTTOM_LEFT_1;
        blackQuads[7][8] = Quad.EMPTY_0;
        blackQuads[8][7] = Quad.TOP_LEFT_1;
        blackQuads[8][8] = Quad.EMPTY_0;

        blackTypes[0] = 53;
        blackTypes[1] = 8;
        blackTypes[2] = 20;
        blackTypes[3] = 0;
        blackTypes[4] = 0;
        blackTypes[5] = 0;
    }

    private void populateWhite() {
        whiteQuads[0][0] = Quad.EMPTY_0;
        whiteQuads[1][0] = Quad.BOTTOM_RIGHT_1;
        whiteQuads[0][1] = Quad.EMPTY_0;
        whiteQuads[1][1] = Quad.BOTTOM_LEFT_1;
        for (int i = 2; i < 7; i++) {
            whiteQuads[i][0] = Quad.RIGHT_2;
            whiteQuads[i][1] = Quad.LEFT_2;
        }
        whiteQuads[7][0] = Quad.TOP_RIGHT_1;
        whiteQuads[7][1] = Quad.TOP_LEFT_1;
        whiteQuads[8][0] = Quad.EMPTY_0;
        whiteQuads[8][1] = Quad.EMPTY_0;

        for (int i = 0; i < 9; i++) {
            for (int j = 2; j < 7; j++) {
                whiteQuads[i][j] = Quad.EMPTY_0;
            }
        }

        whiteQuads[0][7] = Quad.EMPTY_0;
        whiteQuads[1][7] = Quad.BOTTOM_RIGHT_1;
        whiteQuads[0][8] = Quad.EMPTY_0;
        whiteQuads[1][8] = Quad.BOTTOM_LEFT_1;
        for (int i = 2; i < 7; i++) {
            whiteQuads[i][7] = Quad.RIGHT_2;
            whiteQuads[i][8] = Quad.LEFT_2;
        }
        whiteQuads[7][7] = Quad.TOP_RIGHT_1;
        whiteQuads[7][8] = Quad.TOP_LEFT_1;
        whiteQuads[8][7] = Quad.EMPTY_0;
        whiteQuads[8][8] = Quad.EMPTY_0;

        whiteTypes[0] = 53;
        whiteTypes[1] = 8;
        whiteTypes[2] = 20;
        whiteTypes[3] = 0;
        whiteTypes[4] = 0;

    }
}
