package loa.quad;

import java.io.Serializable;

public enum Quad implements Serializable {
	
	EMPTY_0(0),
	BOTTOM_RIGHT_1(1),
	BOTTOM_LEFT_1(1),
	BOTTOM_2(2),
	TOP_RIGHT_1(1),
	RIGHT_2(2),
	DIAG_POS(5),
	TOP_LEFT_3(3),
	TOP_LEFT_1(1),
	DIAG_NEG(5),
	LEFT_2(2),
	TOP_RIGHT_3(3),
	TOP_2(2),
	BOTTOM_LEFT_3(3),
	BOTTOM_RIGHT_3(3),
	FULL_4(4);

    private int category;

    Quad(int category) {
        this.category = category;
    }

    public int getCategory() {
        return category;
    }
	
	public Quad addOrRemoveAt(int piecePos){
    	int currentBits = ordinal();
		int newBits = currentBits ^ getMask(piecePos);
		return bitTypes[newBits];
	}
	
	private int getMask(int piecePos){
    	int mask = 0;
 		switch (piecePos){
			case 0:
				mask = 8;
				break;
			case 1:
				mask = 4;
				break;
			case 2:
				mask = 2;
				break;
			case 3:
				mask = 1;
				break;
		}
		return mask;
	}
	
	private static Quad[] bitTypes = {
			EMPTY_0,
			BOTTOM_RIGHT_1,
			BOTTOM_LEFT_1,
			BOTTOM_2,
			TOP_RIGHT_1,
			RIGHT_2,
			DIAG_POS,
			TOP_LEFT_3,
			TOP_LEFT_1,
			DIAG_NEG,
			LEFT_2,
			TOP_RIGHT_3,
			TOP_2,
			BOTTOM_LEFT_3,
			BOTTOM_RIGHT_3,
			FULL_4
	};
}
