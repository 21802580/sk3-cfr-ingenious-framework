package loa.quad;

@Deprecated
public class QuadUtils {

    // Untidy but necessary to minimise object creation and memory footprint
    public static Quad addPiece(Quad q, int pos) {
        Quad type = null;
        switch (q) {
            case EMPTY_0:
                switch (pos) {
                    case 0:
                        type = Quad.TOP_LEFT_1;
                        break;
                    case 1:
                        type = Quad.TOP_RIGHT_1;
                        break;
                    case 2:
                        type = Quad.BOTTOM_LEFT_1;
                        break;
                    case 3:
                        type = Quad.BOTTOM_RIGHT_1;
                        break;
                }
                break;
            case TOP_LEFT_1:
                switch (pos) {
                    case 1:
                        type = Quad.TOP_2;
                        break;
                    case 2:
                        type = Quad.LEFT_2;
                        break;
                    case 3:
                        type = Quad.DIAG_NEG;
                        break;
                }
                break;
            case TOP_RIGHT_1:
                switch (pos) {
                    case 0:
                        type = Quad.TOP_2;
                        break;
                    case 2:
                        type = Quad.DIAG_POS;
                        break;
                    case 3:
                        type = Quad.RIGHT_2;
                        break;
                }
                break;
            case BOTTOM_LEFT_1:
                switch (pos) {
                    case 0:
                        type = Quad.LEFT_2;
                        break;
                    case 1:
                        type = Quad.DIAG_POS;
                        break;
                    case 3:
                        type = Quad.BOTTOM_2;
                        break;
                }
                break;
            case BOTTOM_RIGHT_1:
                switch (pos) {
                    case 0:
                        type = Quad.DIAG_NEG;
                        break;
                    case 1:
                        type = Quad.RIGHT_2;
                        break;
                    case 2:
                        type = Quad.BOTTOM_2;
                        break;
                }
                break;
            case TOP_2:
                switch (pos) {
                    case 2:
                        type = Quad.BOTTOM_RIGHT_3;
                        break;
                    case 3:
                        type = Quad.BOTTOM_LEFT_3;
                        break;
                }
                break;
            case BOTTOM_2:
                switch (pos) {
                    case 0:
                        type = Quad.TOP_RIGHT_3;
                        break;
                    case 1:
                        type = Quad.TOP_LEFT_3;
                        break;
                }
                break;
            case LEFT_2:
                switch (pos) {
                    case 1:
                        type = Quad.BOTTOM_RIGHT_3;
                        break;
                    case 3:
                        type = Quad.TOP_RIGHT_3;
                        break;
                }
                break;
            case RIGHT_2:
                switch (pos) {
                    case 0:
                        type = Quad.BOTTOM_LEFT_3;
                        break;
                    case 2:
                        type = Quad.TOP_LEFT_3;
                        break;
                }
                break;
            case DIAG_NEG:
                switch (pos) {
                    case 1:
                        type = Quad.BOTTOM_LEFT_3;
                        break;
                    case 2:
                        type = Quad.TOP_RIGHT_3;
                        break;
                }
                break;
            case DIAG_POS:
                switch (pos) {
                    case 0:
                        type = Quad.BOTTOM_RIGHT_3;
                        break;
                    case 3:
                        type = Quad.TOP_LEFT_3;
                        break;
                }
                break;
            default:
                if (q.getCategory() == 3) {
                    return Quad.FULL_4;
                }

                break;
        }
        return type;
    }

    public static Quad removePiece(Quad q, int pos) {
        Quad type = null;
        switch (q) {
            case TOP_2:
                switch (pos) {
                    case 0:
                        type = Quad.TOP_RIGHT_1;
                        break;
                    case 1:
                        type = Quad.TOP_LEFT_1;
                        break;
                }
                break;
            case BOTTOM_2:
                switch (pos) {
                    case 2:
                        type = Quad.BOTTOM_RIGHT_1;
                        break;
                    case 3:
                        type = Quad.BOTTOM_LEFT_1;
                        break;
                }
                break;
            case LEFT_2:
                switch (pos) {
                    case 0:
                        type = Quad.BOTTOM_LEFT_1;
                        break;
                    case 2:
                        type = Quad.TOP_LEFT_1;
                        break;
                }
                break;
            case RIGHT_2:
                switch (pos) {
                    case 1:
                        type = Quad.BOTTOM_RIGHT_1;
                        break;
                    case 3:
                        type = Quad.TOP_RIGHT_1;
                        break;
                }
                break;
            case DIAG_NEG:
                switch (pos) {
                    case 0:
                        type = Quad.BOTTOM_RIGHT_1;
                        break;
                    case 3:
                        type = Quad.TOP_LEFT_1;
                        break;
                }
                break;
            case DIAG_POS:
                switch (pos) {
                    case 1:
                        type = Quad.BOTTOM_LEFT_1;
                        break;
                    case 2:
                        type = Quad.TOP_RIGHT_1;
                        break;
                }
                break;
            case TOP_LEFT_3:
                switch (pos) {
                    case 1:
                        type = Quad.BOTTOM_2;
                        break;
                    case 2:
                        type = Quad.RIGHT_2;
                        break;
                    case 3:
                        type = Quad.DIAG_POS;
                        break;
                }
                break;
            case TOP_RIGHT_3:
                switch (pos) {
                    case 0:
                        type = Quad.BOTTOM_2;
                        break;
                    case 2:
                        type = Quad.DIAG_NEG;
                        break;
                    case 3:
                        type = Quad.LEFT_2;
                        break;
                }
                break;
            case BOTTOM_LEFT_3:
                switch (pos) {
                    case 0:
                        type = Quad.RIGHT_2;
                        break;
                    case 1:
                        type = Quad.DIAG_NEG;
                        break;
                    case 3:
                        type = Quad.TOP_2;
                        break;
                }
                break;
            case BOTTOM_RIGHT_3:
                switch (pos) {
                    case 0:
                        type = Quad.DIAG_POS;
                        break;
                    case 1:
                        type = Quad.LEFT_2;
                        break;
                    case 2:
                        type = Quad.TOP_2;
                        break;
                }
                break;
            case FULL_4:
                switch (pos) {
                    case 0:
                        type = Quad.TOP_LEFT_3;
                        break;
                    case 1:
                        type = Quad.TOP_RIGHT_3;
                        break;
                    case 2:
                        type = Quad.BOTTOM_LEFT_3;
                        break;
                    case 3:
                        type = Quad.BOTTOM_RIGHT_3;
                        break;
                }
                break;
            default:
                if (q.getCategory() == 1) {
                    return Quad.EMPTY_0;
                }  // TODO: Throw exception

                break;
        }
        return type;
    }
}