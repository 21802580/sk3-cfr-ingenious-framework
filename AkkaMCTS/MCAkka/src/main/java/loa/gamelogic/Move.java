package loa.gamelogic;


import java.io.Serializable;

/**
 * TODO: Remove direction class
 */
public class Move implements Serializable {
    private final Coord from;
    private final Coord to;
    private final boolean capture;

    public Move(Coord from, Coord to, boolean capture) {
        this.from = from;
        this.to = to;
        this.capture = capture;
    }

    public String toString() {
        return from + " -> " + to;
    }

    public Coord getFrom() {
        return from;
    }

    public Coord getTo() {
        return to;
    }

    public boolean isCapture() {
        return capture;
    }

    private Direction getDirection() {
        int rf = from.getRow();
        int cf = from.getCol();
        int rt = to.getRow();
        int ct = to.getCol();
        Direction direction = Direction.UP;
        if ((cf == ct) && (rf < rt)) {
            direction = Direction.DOWN;
        } else if ((cf < ct) && (rf == rt)) {
            direction = Direction.RIGHT;
        } else if ((cf > ct) && (rf == rt)) {
            direction = Direction.LEFT;
        } else if ((cf < ct) && (rf > rt)) {
            direction = Direction.UP_RIGHT;
        } else if ((cf < ct) && (rf < rt)) {
            direction = Direction.DOWN_RIGHT;
        } else if ((cf > ct) && (rf < rt)) {
            direction = Direction.DOWN_LEFT;
        } else if ((cf > ct) && (rf > rt)) {
            direction = Direction.UP_LEFT;
        }
        return direction;
    }
    
    public LineOrientation getOrientation(){
        Direction direction = getDirection();
        switch(direction){
            case RIGHT:
            case LEFT:
                return LineOrientation.HORIZONTAL;
            case UP:
            case DOWN:
                return LineOrientation.VERTICAL;
            case UP_RIGHT:
            case DOWN_LEFT:
                return LineOrientation.POSITIVE_DIAGONAL;
            default:
                return LineOrientation.NEGATIVE_DIAGONAL;
        }
    }
    
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (capture ? 1231 : 1237);
        result = prime * result + ((from == null) ? 0 : from.hashCode());
        result = prime * result + ((to == null) ? 0 : to.hashCode());
        return result;
    }
    
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Move other = (Move) obj;
        if (capture != other.capture)
            return false;
        if (from == null) {
            if (other.from != null)
                return false;
        } else if (!from.equals(other.from))
            return false;
        if (to == null) {
            return other.to == null;
        } else return to.equals(other.to);
    }
}
