package loa.gamelogic;

import java.io.Serializable;

public enum Colour implements Serializable {
    EMPTY("."),
    BLACK("B"),
    WHITE("W");

    private final String stringRep;

    Colour(String stringRep) {
        this.stringRep = stringRep;
    }

    @Override
    public String toString() {
        return stringRep;
    }

    public Colour getOpponent() {
        switch (this) {
            case WHITE:
                return Colour.BLACK;
            case BLACK:
                return Colour.WHITE;
            default:
                return Colour.EMPTY;
        }
    }
}
