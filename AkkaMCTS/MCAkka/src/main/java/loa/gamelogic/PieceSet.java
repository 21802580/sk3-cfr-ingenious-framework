package loa.gamelogic;

import java.io.Serializable;
import java.util.*;

public class PieceSet implements Iterable<Piece>, Serializable {

    private List<Piece> list;
    private Map<Piece, Integer> indexMap;

    public PieceSet() {
        list = new ArrayList<Piece>();
        indexMap = new HashMap<Piece, Integer>();
    }
    
	public boolean add(Piece e) {
		if (contains(e)) return false;
		indexMap.put(e, list.size());
		list.add(e);
		return true;
	}
	
	public boolean remove(Piece o) {
		Integer indexBoxed = indexMap.remove(o);
		if (indexBoxed == null) return false;
		int index = indexBoxed;
		int last = list.size() - 1;
		Piece element = list.remove(last);
		if (index != last) {
			indexMap.put(element, index);
			list.set(index, element);
		}
		return true;
	}
	
	public Piece removeRandom(Random random) {
		Piece element = list.get(random.nextInt(list.size()));
		remove(element);
		return element;
	}

	
	public boolean contains(Piece o) {
		return indexMap.containsKey(o);
	}
	
	@Override
	public Iterator<Piece> iterator() {
		return list.iterator();
	}
	
	public int size() {
		return list.size();
	}
	
	public boolean isEmpty(){
    	return size() == 0;
	}
	
	public void addAll(Collection<Piece> pieces){
    	for (Piece p : pieces){
    		add(p);
		}
	}
	
	public Piece getRandom(Random random){
 		return isEmpty() ? null : list.get(random.nextInt(list.size()));
	}
	
	public String toString(){
    	if (isEmpty()){
    		return "[]";
		}
    	StringBuilder b = new StringBuilder();
		b.append("[");
    	for (int i = 0; i < list.size() - 1; i++){
    		Piece p = list.get(i);
    		b.append(p);
    		b.append(", ");
		}
		b.append(list.get(list.size() - 1));
		b.append("]");
    	return b.toString();
	}
}