package loa.gamelogic;

import java.io.Serializable;

public class Piece implements Serializable {
	private int row;
	private int col;
	
	public Piece(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public Piece(Piece p) {
		this.row = p.row;
		this.col = p.col;
	}
	
	public void setPos(int newRow, int newCol){
		row = newRow;
		col = newCol;
	}
	
	public int getRow() {
		return row;
	}
	
	public int getCol() {
		return col;
	}
	
	public String toString(){
		return "(" + row + ", " + col + ")";
	}
}
