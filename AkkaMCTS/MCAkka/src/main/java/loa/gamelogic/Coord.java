package loa.gamelogic;


import java.io.Serializable;

public class Coord implements Serializable {
    // Static co-ordinate array
    public static Coord[][] coords = {
            {
                    new Coord(0, 0), new Coord(0, 1), new Coord(0, 2), new Coord(0, 3),
                    new Coord(0, 4), new Coord(0, 5), new Coord(0, 6), new Coord(0, 7)
            },
            {
                    new Coord(1, 0), new Coord(1, 1), new Coord(1, 2), new Coord(1, 3),
                    new Coord(1, 4), new Coord(1, 5), new Coord(1, 6), new Coord(1, 7)
            },
            {
                    new Coord(2, 0), new Coord(2, 1), new Coord(2, 2), new Coord(2, 3),
                    new Coord(2, 4), new Coord(2, 5), new Coord(2, 6), new Coord(2, 7)
            },
            {
                    new Coord(3, 0), new Coord(3, 1), new Coord(3, 2), new Coord(3, 3),
                    new Coord(3, 4), new Coord(3, 5), new Coord(3, 6), new Coord(3, 7)
            },
            {
                    new Coord(4, 0), new Coord(4, 1), new Coord(4, 2), new Coord(4, 3),
                    new Coord(4, 4), new Coord(4, 5), new Coord(4, 6), new Coord(4, 7)
            },
            {
                    new Coord(5, 0), new Coord(5, 1), new Coord(5, 2), new Coord(5, 3),
                    new Coord(5, 4), new Coord(5, 5), new Coord(5, 6), new Coord(5, 7)
            },
            {
                    new Coord(6, 0), new Coord(6, 1), new Coord(6, 2), new Coord(6, 3),
                    new Coord(6, 4), new Coord(6, 5), new Coord(6, 6), new Coord(6, 7)
            },
            {
                    new Coord(7, 0), new Coord(7, 1), new Coord(7, 2), new Coord(7, 3),
                    new Coord(7, 4), new Coord(7, 5), new Coord(7, 6), new Coord(7, 7)
            }
    };
    private final int row;
    private final int col;

    private Coord(int row, int col) {
        this.row = row;
        this.col = col;
    }
	
	public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Coord other = (Coord) obj;
		return col == other.col && row == other.row;
	}

    public String toString() {
        return "(" + row + ", " + col + ")";
    }

}
