package loa.movegeneration;

import loa.gamelogic.Colour;
import loa.gamelogic.Coord;
import loa.gamelogic.LineOrientation;
import loa.gamelogic.Move;

import java.io.Serializable;

import static loa.movegeneration.LineUtils.*;

public class LineConfiguration implements Serializable {
	
	// Hashes for each horizontal, vertical and diagonal line
	private int[] horizontal;
	private int[] vertical;
	private int[] positiveDiagonal;
	private int[] negativeDiagonal;
	
	// 3^i for i = [0, 8] to be used for constant time line hash calculations
	public static int[] powers = {1, 3, 9, 27, 81, 243, 729, 2187, 6561};
	
	public LineConfiguration(Colour[][] state){
		horizontal = new int[8];
		vertical = new int[8];
		positiveDiagonal = new int[15];
		negativeDiagonal = new int[15];
		populateLines(state);
	}
	
	public LineConfiguration(LineConfiguration configuration) {
		horizontal = new int[8];
		vertical = new int[8];
		positiveDiagonal = new int[15];
		negativeDiagonal = new int[15];
		for (int i = 0; i < 15; i++) {
			if (i < 8) {
				horizontal[i] = configuration.horizontal[i];
				vertical[i] = configuration.vertical[i];
			}
			positiveDiagonal[i] = configuration.positiveDiagonal[i];
			negativeDiagonal[i] = configuration.negativeDiagonal[i];
		}
	}
	
	private void populateLines(Colour[][] state) {
		for (int i = 0; i < state.length; i++){
			for (int j = 0; j < state.length; j++){
				Colour c = state[i][j];
				int base = c.ordinal();
				horizontal[i] += base * powers[j];
				vertical[j] += base * powers[i];
				int positiveHashIndex = getLinePositionFromCoord(i, j, LineOrientation.POSITIVE_DIAGONAL);
				int negativeHashIndex = getLinePositionFromCoord(i, j, LineOrientation.NEGATIVE_DIAGONAL);
				int positiveLineIndex = getPieceIndexInLine(i, j, LineOrientation.POSITIVE_DIAGONAL);
				int negativeLineIndex = getPieceIndexInLine(i, j, LineOrientation.NEGATIVE_DIAGONAL);
				positiveDiagonal[positiveHashIndex] += base * powers[positiveLineIndex];
				negativeDiagonal[negativeHashIndex] += base * powers[negativeLineIndex];
			}
		}
	}
	
	private void setLineHash(LineOrientation orientation, int index, int hash){
		switch(orientation){
			case HORIZONTAL:
				horizontal[index] = hash;
				break;
			case VERTICAL:
				vertical[index] = hash;
				break;
			case POSITIVE_DIAGONAL:
				positiveDiagonal[index] = hash;
				break;
			case NEGATIVE_DIAGONAL:
				negativeDiagonal[index] = hash;
				break;
		}
	}
	
	public int getLineHash(LineOrientation orientation, int index){
		int hash = 0;
		switch(orientation){
			case HORIZONTAL:
				hash = horizontal[index];
				break;
			case VERTICAL:
				hash = vertical[index];
				break;
			case POSITIVE_DIAGONAL:
				hash = positiveDiagonal[index];
				break;
			case NEGATIVE_DIAGONAL:
				hash = negativeDiagonal[index];
				break;
		}
		return hash;
	}
	
	private void removePiece(int row, int col,  Colour player){
		int base = player.ordinal();
		for (LineOrientation orientation : LineOrientation.getArray()){
			int hashIndex = getLinePositionFromCoord(row, col, orientation);
			int lineIndex = getPieceIndexInLine(row, col, orientation);
			int oldHash = getLineHash(orientation, hashIndex);
			int newHash = oldHash - (base * powers[lineIndex]);
			setLineHash(orientation, hashIndex, newHash);
		}
	}
	
	private void addPiece(int row, int col, Colour player){
		int base = player.ordinal();
		for (LineOrientation orientation : LineOrientation.getArray()){
			int hashIndex = getLinePositionFromCoord(row, col, orientation);
			int lineIndex = getPieceIndexInLine(row, col, orientation);
			int oldHash = getLineHash(orientation, hashIndex);
			int newHash = oldHash + (base * powers[lineIndex]);
			setLineHash(orientation, hashIndex, newHash);
		}
	}
	
	public void applyMove(Move m, Colour player) {
		Coord src = m.getFrom();
		Coord dest = m.getTo();
		int rf = src.getRow();
		int cf = src.getCol();
		int rt = dest.getRow();
		int ct = dest.getCol();
		boolean capture = m.isCapture();
		removePiece(rf, cf, player);
		addPiece(rt, ct, player);
		if (capture) {
			Colour opponent = player.getOpponent();
			removePiece(rt, ct, opponent);
		}
	}
	
	public void undoMove(Move m, Colour player) {
		Coord src = m.getFrom();
		Coord dest = m.getTo();
		int rf = src.getRow();
		int cf = src.getCol();
		int rt = dest.getRow();
		int ct = dest.getCol();
		boolean capture = m.isCapture();
		removePiece(rt, ct, player);
		addPiece(rf, cf, player);
		if (capture) {
			Colour opponent = player.getOpponent();
			addPiece(rt, ct, opponent);
		}
	}
	
}
