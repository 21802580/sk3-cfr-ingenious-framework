package loa.movegeneration;

public class LineMove {
    private int startIndex;
    private int endIndex;
    private boolean capture;

    public LineMove(int startIndex, int endIndex, boolean capture) {
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.capture = capture;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public boolean isCapture() {
        return capture;
    }
    
    public String toString() {
        return startIndex + " -> " + endIndex;
    }
}
