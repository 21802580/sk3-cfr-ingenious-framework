package loa.movegeneration;

import loa.gamelogic.Colour;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static loa.movegeneration.LineUtils.lineMoveIsLegal;

@Deprecated
public class MoveMapSilly {
    /*
     * Horizontal and vertical moves in LOA are not dependent on the position of the
     * line along which it is made, i.e. The same moves are allowed on a line with the
     * configuration [B . W . . B B .] no matter where it is situated on the board, including
     * the diagonals of length 8.
     *
     * This also holds for shorted diagonals of equal length, i.e. A diagonal of length 4 with
     * the configuration [B . W .] will have the same legal moves regardless of where it is
     * situated on the board.
     *
     * The data structures below hold all legal moves for lines of every length for each player.
     * They are populated once at runtime and are used to retrieve legal moves for a line on
     * the board in constant time.
     *
     * blackMoves.get(lineLength).get(lineHash) returns a list of all legal LineMoves (one
     * dimensional moves e.g. 3 -> 6) for black, with the given line length and hash. The
     * hash is a unique identifier for the line configuration and is constantly maintained
     * as moves are made (see MoveGenerator).
     */
    private static ArrayList<HashMap<Integer, List<LineMove>>> blackMoves;
    private static ArrayList<HashMap<Integer, List<LineMove>>> whiteMoves;
	private static Colour[] colours = {Colour.BLACK, Colour.WHITE, Colour.EMPTY};



	static {
		blackMoves = new ArrayList<HashMap<Integer, List<LineMove>>>();
		whiteMoves = new ArrayList<HashMap<Integer, List<LineMove>>>();
		for (int i = 0; i <= 8; i++){
			blackMoves.add(new HashMap<Integer, List<LineMove>>());
			whiteMoves.add(new HashMap<Integer, List<LineMove>>());
		}
		for (int i = 2; i <= 8; i++){
			permuteAndPopulate(i);
		}
	}

	private static void permuteAndPopulate(int lineLength){
		Colour[] line = new Colour[lineLength];
		permuteRecursively(line, lineLength - 1);
	}

	private static void permuteRecursively(Colour[] line, int currentPos) {
		if (currentPos == -1) {
			addMoves(line);
			return;
		}
		for (int i = 0; i < 3; ++i) {
			line[currentPos] = colours[i];
			permuteRecursively(line, currentPos - 1);
		}
	}

	private static void addMoves(Colour[] line){
		int lineLength = line.length;
		int index = 0;
		int numPieces = 0;
		for (int i = 0; i < lineLength; i++){
			index += line[i].ordinal() * LineConfiguration.powers[i];
			if (line[i] != Colour.EMPTY){
				numPieces++;
			}
		}
		for (int i = 0; i < lineLength; i++){
			int dest1 = i + numPieces;
			int dest2 = i - numPieces;
			if (line[i] != Colour.EMPTY){
				if (lineMoveIsLegal(line, i, dest1, line[i])){
					boolean capture = line[dest1] != Colour.EMPTY;
					addLineMove(line, i, dest1, capture, index);
				}
				if (lineMoveIsLegal(line, i, dest2, line[i])){
					boolean capture = line[dest2] != Colour.EMPTY;
					addLineMove(line, i, dest2, capture, index);
				}
			}
		}
	}

    public static void addLineMove(Colour[] line, int srcIndex, int destIndex, boolean capture, int hash){
        Colour colour = line[srcIndex];
        HashMap<Integer, List<LineMove>> map = getMap(colour, line.length);
		List<LineMove> moves = map.computeIfAbsent(hash, k -> new ArrayList<LineMove>());
        moves.add(new LineMove(srcIndex, destIndex, capture));
    }

    private static HashMap<Integer, List<LineMove>> getMap(Colour colour, int lineLength){
        if (colour == Colour.BLACK){
            return blackMoves.get(lineLength);
        }
        return whiteMoves.get(lineLength);
    }

    public static List<LineMove> getLineMoves(Colour colour, int lineLength, int hash){
        return getMap(colour, lineLength).get(hash);
    }
}
