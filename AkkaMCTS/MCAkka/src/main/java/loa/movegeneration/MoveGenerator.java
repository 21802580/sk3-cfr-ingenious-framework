package loa.movegeneration;

import commandline.PlayerParameters;
import loa.domainknowlege.Heuristic;
import loa.domainknowlege.MoveCategory;
import loa.gamelogic.*;
import player.core.search.MCTS;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static loa.movegeneration.LineUtils.getLinePositionFromCoord;

public class MoveGenerator {
	
	// Gross but it works
    public static List<MoveValuePair> generateMovesWithValues(Board board, PlayerParameters playerParameters) {
    	if (board.getWinner() != null){
    		// Board is terminal -> return empty list
			return new ArrayList<MoveValuePair>();
		}
    	Colour player = board.getCurrentPlayer();
    	PieceSet pieces = board.getPiecesFor(player);
		LineConfiguration lines = board.getLineConfiguration();
		List<MoveValuePair> allMoves = new ArrayList<MoveValuePair>();
		HashMap<LineOrientation, HashSet<Integer>> checkedInices = new HashMap<LineOrientation, HashSet<Integer>>();
		for (LineOrientation orientation : LineOrientation.getArray()) {
			checkedInices.put(orientation, new HashSet<Integer>());
		}
		for (Piece p : pieces) {
			int row = p.getRow();
			int col = p.getCol();
			for (LineOrientation orientation : LineOrientation.getArray()) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				int hash = lines.getLineHash(orientation, hashIndex);
				HashSet<Integer> checkedIndicesForOrientation = checkedInices.get(orientation);
				if (!checkedIndicesForOrientation.contains(hashIndex)) {
					checkedIndicesForOrientation.add(hashIndex);
					List<Move> moves = MoveMap.getMovesForLine(player, orientation, hashIndex, hash, board);
					if (moves == null) continue;
					for (Move move : moves){
						double value = 0.0;
						if (playerParameters.getHeuristicType() == PlayerParameters.HeuristicType.FUNCTION) {
							value = Heuristic.getHeuristicValue(board, move);
						} else if (playerParameters.getHeuristicType() == PlayerParameters.HeuristicType.MOVE_CATEGORY) {
							value = MoveCategory.getTransitionProbability(move);
						}
						if (playerParameters.isDecisive() && Heuristic.moveIsDecisive(board, move)) {
							allMoves = new ArrayList<MoveValuePair>();
							allMoves.add(new MoveValuePair(move, MCTS.DECISIVE_MOVE_VALUE));
							return allMoves;
						}
						allMoves.add(new MoveValuePair(move, value));
					}
				}
			}
		}
		return allMoves;
	}
	
	public static Move getRandomMove(Board board){
		Colour player = board.getCurrentPlayer();
		PieceSet pieces = board.getPiecesFor(player);
		Random random = ThreadLocalRandom.current();
		LineOrientation[] orientationOrder = LineOrientation.getRandomOrder(random);
		Move move = null;
		for (int i = 0; i < 5; i++){
			Piece piece = pieces.getRandom(random);
			int row = piece.getRow();
			int col = piece.getCol();
			for (LineOrientation orientation : orientationOrder) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				move = getRandomMoveForLine(board, orientation, hashIndex, random);
				if (move != null) break;
			}
		}
		return move == null ? getRandomMoveExhaustive(board, pieces, random, orientationOrder) : move;
	}
	
	private static Move getRandomMoveExhaustive(Board board, PieceSet pieces, Random random, LineOrientation[] orientationOrder){
		HashSet<Piece> checkedPieces = new HashSet<Piece>();
		HashMap<LineOrientation, HashSet<Integer>> checkedInices = new HashMap<LineOrientation, HashSet<Integer>>();
		for (LineOrientation orientation : LineOrientation.getArray()){
			checkedInices.put(orientation, new HashSet<Integer>());
		}
		Move move = null;
		while((!pieces.isEmpty()) && (move == null)){
			Piece piece = pieces.removeRandom(random);
			checkedPieces.add(piece);
			int row = piece.getRow();
			int col = piece.getCol();
			for (LineOrientation orientation : orientationOrder) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				if (!checkedInices.get(orientation).contains(hashIndex)) {
					checkedInices.get(orientation).add(hashIndex);
					move = getRandomMoveForLine(board, orientation, hashIndex, random);
					if (move != null) break;
				}
			}
		}
		pieces.addAll(checkedPieces);
		return move;
	}
	
	public static boolean hasMove(Board board, Move m){
    	Colour colour = board.getCurrentPlayer();
    	LineOrientation orientation = m.getOrientation();
    	LineConfiguration lines = board.getLineConfiguration();
		int linepos = getLinePositionFromCoord(m.getFrom().getRow(), m.getFrom().getCol(), orientation);
		int hash = lines.getLineHash(orientation, linepos);
 		List<Move> legalLineMoves = MoveMap.getMovesForLine(colour, orientation, linepos, hash, board);
 		if (legalLineMoves == null) return false;
 		return legalLineMoves.contains(m);
	}
	
	private static Move getRandomMoveForLine(Board board, LineOrientation orientation, int hashIndex, Random random){
		LineConfiguration lines = board.getLineConfiguration();
		Colour player = board.getCurrentPlayer();
		int hash = lines.getLineHash(orientation, hashIndex);
		List<Move> moves = MoveMap.getMovesForLine(player, orientation, hashIndex, hash, board);
		if (moves == null) return null;
		return moves.get(random.nextInt(moves.size()));
	}
	
}
