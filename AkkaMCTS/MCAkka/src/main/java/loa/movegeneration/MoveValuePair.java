package loa.movegeneration;

import loa.gamelogic.Move;

import java.io.Serializable;

public class MoveValuePair implements Comparable, Serializable {
	private final Move move;
	private final double value;
	
	public MoveValuePair(Move move, double value){
		this.move = move;
		this.value = value;
	}
	
	public Move getMove() {
		return move;
	}
	
	public double getValue() {
		return value;
	}
	
	public String toString(){
		return move + " " + value;
	}

	@Override
	public int compareTo(Object o) {
		MoveValuePair other = (MoveValuePair)o;
		if (this.getValue() > other.getValue()){
			return 1;
		} else if (this.getValue() < other.getValue()){
			return -1;
		} else {
			return 0;
		}
	}
}
