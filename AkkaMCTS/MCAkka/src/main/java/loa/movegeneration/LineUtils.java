package loa.movegeneration;

import loa.gamelogic.Colour;
import loa.gamelogic.Coord;
import loa.gamelogic.LineOrientation;

public class LineUtils {
	
	public static int getLinePositionFromCoord(int row, int col, LineOrientation orientation){
		int index = 0;
		switch(orientation){
			case HORIZONTAL:
				index = row;
				break;
			case VERTICAL:
				index = col;
				break;
			case POSITIVE_DIAGONAL:
				index = row + col;
				break;
			case NEGATIVE_DIAGONAL:
				index = col - row + 7;
				break;
		}
		return index;
	}
	
	public static int getPieceIndexInLine(int row, int col, LineOrientation orientation){
		int index = 0;
		switch(orientation){
			case HORIZONTAL:
				index = col;
				break;
			case VERTICAL:
				index = row;
				break;
			case POSITIVE_DIAGONAL:
				index = (row + col <= 7) ? col : 7 - row;
				break;
			case NEGATIVE_DIAGONAL:
				index = (row > col) ? 7 - row : 7 - col;
				break;
		}
		return index;
	}
	
	public static Coord getCoordFromLineIndex(Coord baseCoord, int lineIndex, LineOrientation orientation){
		Coord boardCoord = null;
		int row = baseCoord.getRow();
		int col = baseCoord.getCol();
		switch(orientation){
			case HORIZONTAL:
				boardCoord = Coord.coords[row][lineIndex];
				break;
			case VERTICAL:
				boardCoord = Coord.coords[lineIndex][col];
				break;
			case POSITIVE_DIAGONAL:
				boardCoord = Coord.coords[row - lineIndex][col + lineIndex];
				break;
			case NEGATIVE_DIAGONAL:
				boardCoord = Coord.coords[row - lineIndex][col - lineIndex];
				break;
		}
		return boardCoord;
	}
	
	public static Coord getBaseCoordFromHashIndex(int hashIndex, LineOrientation orientation){
		Coord c = null;
		int row, col;
		switch(orientation){
			case HORIZONTAL:
				c = Coord.coords[hashIndex][0];
				break;
			case VERTICAL:
				c = Coord.coords[0][hashIndex];
				break;
			case POSITIVE_DIAGONAL:
				row = Math.min(hashIndex, 7);
				col = Math.max(0, hashIndex - 7);
				c = Coord.coords[row][col];
				break;
			case NEGATIVE_DIAGONAL:
				row = Math.min(7, 7 - (hashIndex - 7));
				col = Math.min(7, hashIndex);
				c = Coord.coords[row][col];
				break;
		}
		return c;
	}
	
	public static int getLineLengthFromLinePos(int hashIndex, LineOrientation orientation, Coord baseCoord){
		int lineLength = 0;
		if (baseCoord == null) baseCoord = getBaseCoordFromHashIndex(hashIndex, orientation);
		int startRow = baseCoord.getRow();
		int startCol = baseCoord.getCol();
		switch(orientation){
			case HORIZONTAL:
			case VERTICAL:
				lineLength = 8;
				break;
			case POSITIVE_DIAGONAL:
				lineLength = Math.min(startRow + 1, 7 - startCol + 1);
				break;
			case NEGATIVE_DIAGONAL:
				lineLength =  Math.min(startRow + 1, startCol + 1);
				break;
		}
		return lineLength;
	}
	
	public static boolean lineMoveIsLegal(Colour[] line, int src, int dest, Colour player) {
		boolean legal = true;
		if (src == dest) {
			legal = false;
		} else if (src < 0 || src > line.length - 1 || dest < 0 || dest > line.length - 1) {
			legal = false;
		} else if (line[src] != player) {
			legal = false;
		} else if (line[dest] == player) {
			legal = false;
		} else if (src < dest){
			legal = checkSkipLegality(line, player, src, dest);
		} else {
			legal = checkSkipLegality(line, player, dest, src);
		}
		return legal;
	}
	
	private static boolean checkSkipLegality(Colour[] line, Colour player, int src, int dest){
		for (int i = src + 1; i < dest; i++){
			if (line[i] != player && line[i] != Colour.EMPTY){
				return false;
			}
		}
		return true;
	}
}
