package loa.movegeneration;

import loa.gamelogic.Colour;

import java.util.ArrayList;
import java.util.List;

import static loa.movegeneration.LineUtils.lineMoveIsLegal;

@Deprecated
public class MoveMapArr {
	private static List<LineMove>[][][] moveTable; // [colour - 1][length][line]
	private static Colour[] colours = {Colour.BLACK, Colour.WHITE, Colour.EMPTY};
	
	static {
		moveTable = new List[2][9][];
		for (int colour = 0; colour < 2; colour++){
			for (int length = 1; length < 9; length++){
				moveTable[colour][length] = new List[LineConfiguration.powers[length]];
			}
		}
		for (int i = 2; i <= 8; i++){
			permuteAndPopulate(i);
		}
	}
	
	private static void permuteAndPopulate(int lineLength){
		Colour[] line = new Colour[lineLength];
		permuteRecursively(line, lineLength - 1);
	}
	
	private static void permuteRecursively(Colour[] line, int currentPos) {
		if (currentPos == -1) {
			addMoves(line);
			return;
		}
		for (int i = 0; i < 3; ++i) {
			line[currentPos] = colours[i];
			permuteRecursively(line, currentPos - 1);
		}
	}
	
	private static void addMoves(Colour[] line){
		int lineLength = line.length;
		int index = 0;
		int numPieces = 0;
		for (int i = 0; i < lineLength; i++){
			index += line[i].ordinal() * LineConfiguration.powers[i];
			if (line[i] != Colour.EMPTY){
				numPieces++;
			}
		}
		for (int i = 0; i < lineLength; i++){
			int dest1 = i + numPieces;
			int dest2 = i - numPieces;
			if (line[i] != Colour.EMPTY){
				if (lineMoveIsLegal(line, i, dest1, line[i])){
					boolean capture = line[dest1] != Colour.EMPTY;
					addLineMove(line, i, dest1, capture, index);
				}
				if (lineMoveIsLegal(line, i, dest2, line[i])){
					boolean capture = line[dest2] != Colour.EMPTY;
					addLineMove(line, i, dest2, capture, index);
				}
			}
		}
	}
	
	public static void addLineMove(Colour[] line, int srcIndex, int destIndex, boolean capture, int hash){
		Colour colour = line[srcIndex];
		List<LineMove> movesForLine = moveTable[colour.ordinal() - 1][line.length][hash];
		if (movesForLine == null){
			movesForLine = new ArrayList<LineMove>();
			moveTable[colour.ordinal() - 1][line.length][hash] = movesForLine;
		}
		movesForLine.add(new LineMove(srcIndex, destIndex, capture));
	}
	
	public static List<LineMove> getLineMoves(Colour colour, int lineLength, int hash){
		return moveTable[colour.ordinal() - 1][lineLength][hash];
	}
}
