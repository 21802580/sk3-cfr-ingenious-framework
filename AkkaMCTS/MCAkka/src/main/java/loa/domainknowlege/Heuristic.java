package loa.domainknowlege;

import loa.gamelogic.*;

public class Heuristic {
	private static int[] sumOfMinimalDistances = {0, 0, 1, 2, 3, 4, 5, 6, 7, 8, 10, 12, 14};
	
	public static double getHeuristicValue(Board board, Move m) {
		double hb = 0;
		double hw = 0;
		board.applyMove(m);
		Coord bCOM = getCentreOfMass(board, Colour.BLACK);
		Coord wCOM = getCentreOfMass(board, Colour.WHITE);
		PieceSet blackPieces = board.getPiecesFor(Colour.BLACK);
		PieceSet whitePieces = board.getPiecesFor(Colour.WHITE);
		for (Piece c : blackPieces) {
			hb += getDistance(c, bCOM);
		}
		for (Piece c : whitePieces) {
			hw += getDistance(c, wCOM);
		}
		double sodB = hb - sumOfMinimalDistances[blackPieces.size()];
		double sodW = hw - sumOfMinimalDistances[whitePieces.size()];
		board.undoMove(m);
		return board.getCurrentPlayer() == Colour.BLACK ? (1.0 / sodB) - (1.0 / sodW)
				: (1.0 / sodW) - (1.0 / sodB);
	}
	
	private static double getDistance(Piece c1, Coord c2) {
		double x1 = (double) c1.getRow();
		double x2 = (double) c2.getRow();
		double y1 = (double) c1.getCol();
		double y2 = (double) c2.getCol();
		return Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
	}
	
	private static Coord getCentreOfMass(Board board, Colour colour) {
		double cx = 0;
		double cy = 0;
		PieceSet pieces = board.getPiecesFor(colour);
		for (Piece c : pieces) {
			cx += c.getRow();
			cy += c.getCol();
		}
		cx /= (double) pieces.size();
		cy /= (double) pieces.size();
		int row = (int) Math.round(cx);
		int col = (int) Math.round(cy);
		Coord cc = Coord.coords[row][col];
		double minDist = 20;
		Coord com = Coord.coords[0][0];
		for (Piece c : pieces) {
			double dist = getDistance(c, cc);
			if (dist < minDist) {
				minDist = dist;
				com = Coord.coords[c.getRow()][c.getCol()];
			}
		}
		return com;
	}
	
	public static boolean moveIsDecisive(Board board, Move m) {
		board.applyMove(m);
		boolean decisive = false;
		Colour winner = board.getWinner();
		if (winner == board.getPendingPlayer()) {
			decisive = true;
		}
		board.undoMove(m);
		return decisive;
	}
}
