package loa.domainknowlege;

import loa.gamelogic.Coord;
import loa.gamelogic.Move;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class MoveCategory {
	public static void main(String[] args) {
		Map<Region, String> r = new LinkedHashMap<Region, String>();
		r.put(Region.CORNER, "A");
		r.put(Region.OUTER, "B");
		r.put(Region.INNER, "C");
		r.put(Region.CENTRAL, "D");
		for (Map.Entry<Region, String> entryFrom : r.entrySet()) {
			for (Map.Entry<Region, String> entryTo : r.entrySet()) {
				Region from = entryFrom.getKey();
				Region to = entryTo.getKey();
				int catWithCap = getMoveCategory(from, to, true);
				int catNoCap = getMoveCategory(from, to, false);
				double capProb = transitionProbabilities[catWithCap];
				double noCapProb = transitionProbabilities[catNoCap];
				System.out.println(entryFrom.getValue() + " & " + entryTo.getValue() + " & " + capProb + " & " + noCapProb + " \\\\");
			}
		}
	}

    public enum Region {
        CORNER, OUTER, INNER, CENTRAL
    }

    private static double[] transitionProbabilities = {0,
            0.0011037527593818985, 5.959475566150178E-4, 0.005173557373424718, 0.0, 0.003477734936115521,
            0.005080891998250984, 0.016334602151275104, 0.05461165048543689, 0.014574665095360638,
            0.023055879344387086, 0.0452784566585201, 0.22332015810276679, 0.033891850723533894,
            0.042998764576490794, 0.06854419940130735, 0.17721518987341772, 0.0, 0.0, 0.0, 0.0,
            0.017964071856287425, 0.014180868267197413, 0.02452485040151304, 0.19753086419753085,
            0.1274542429284526, 0.14287142071860476, 0.2470548255550521, 0.3076923076923077, 0.0,
            0.21635711958292603, 0.2664879356568365, 0.38333333333333336};


    public static double getTransitionProbability(Move move) {
		Region fromRegion = MoveCategory.getRegion(move.getFrom());
		Region toRegion = MoveCategory.getRegion(move.getTo());
        return transitionProbabilities[getMoveCategory(fromRegion, toRegion, move.isCapture())];
    }

    private static int getMoveCategory(Region fromRegion, Region toRegion, boolean cap) {
		int mc = 0;
		if (fromRegion == Region.CENTRAL && toRegion == Region.CORNER) {
			mc = 1;
		} else if (fromRegion == Region.INNER && toRegion == Region.CORNER) {
			mc = 2;
		} else if (fromRegion == Region.OUTER && toRegion == Region.CORNER) {
			mc = 3;
		} else if (fromRegion == Region.CORNER && toRegion == Region.CORNER) {
			mc = 4;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.OUTER) {
			mc = 5;
		} else if (fromRegion == Region.INNER && toRegion == Region.OUTER) {
			mc = 6;
		} else if (fromRegion == Region.OUTER && toRegion == Region.OUTER) {
			mc = 7;
		} else if (fromRegion == Region.CORNER && toRegion == Region.OUTER) {
			mc = 8;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.INNER) {
			mc = 9;
		} else if (fromRegion == Region.INNER && toRegion == Region.INNER) {
			mc = 10;
		} else if (fromRegion == Region.OUTER && toRegion == Region.INNER) {
			mc = 11;
		} else if (fromRegion == Region.CORNER && toRegion == Region.INNER) {
			mc = 12;
		} else if (fromRegion == Region.CENTRAL && toRegion == Region.CENTRAL) {
			mc = 13;
		} else if (fromRegion == Region.INNER && toRegion == Region.CENTRAL) {
			mc = 14;
		} else if (fromRegion == Region.OUTER && toRegion == Region.CENTRAL) {
			mc = 15;
		} else if (fromRegion == Region.CORNER && toRegion == Region.CENTRAL) {
			mc = 16;
		}
		if (cap) {
			mc += 16;
		}
		return mc;
	}

    private static Region getRegion(Coord c) {
        int row = c.getRow();
        int col = c.getCol();
        if ((row == 0 && col == 0) || (row == 0 && col == 7) || (row == 7 && col == 0) || (row == 7 && col == 7)) {
            return Region.CORNER;
        } else if (row == 7 || row == 0 || col == 7 || col == 0) {
            return Region.OUTER;
        } else if ((row == 3 && col == 3) || (row == 3 && col == 4) || (row == 4 && col == 3) || (row == 4 && col == 4)) {
            return Region.CENTRAL;
        } else {
            return Region.INNER;
        }
    }
}
