package commandline;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import loa.gamelogic.Colour;

import java.util.Arrays;

public class PlayerParameters {
	
	/********************
	 * Helper functions *
	 ********************/
	
	public static PlayerParameters getInstance(String[] argumentArray) {
		PlayerParameters playerArgs = new PlayerParameters();
		JCommander.newBuilder()
				.addObject(playerArgs)
				.build()
				.parse(argumentArray);
		return playerArgs;
	}
	
	public static PlayerParameters[] splitArguments(String[] allArguments){
		int splitIndex = 0;
        String currentArg = allArguments[0];
        while (!currentArg.equals("_")) {
            currentArg = allArguments[++splitIndex];
        }
		String[] blackArr = Arrays.copyOfRange(allArguments, 0, splitIndex);
		String[] whiteArr = Arrays.copyOfRange(allArguments, splitIndex + 1, allArguments.length);
        PlayerParameters blackArgs = getInstance(blackArr);
        PlayerParameters whiteArgs = getInstance(whiteArr);
        return new PlayerParameters[] {blackArgs, whiteArgs};
	}
	
	/*******************************************************
	 * General player arguments for cluster setup and MCTS *
	 *******************************************************/
	
	@Parameter(names = "-loglevel", description = "Log level for all classes.\n"
			+ "NONE disables all logging.\n"
			+ "ERROR is for critical errors. The application may no longer work correctly.\n"
			+ "WARN is for important warnings. The application will continue to work correctly.\n"
			+ "INFO is for informative messages. Typically used for deployment.\n"
			+ "DEBUG is for debug messages. This level is useful during development.\n"
			+ "TRACE is for trace messages. A lot of information is logged, so this level is usually only needed when debugging a problem.")
	private String logLevel = "OFF";
	
	@Parameter(names = "-name", description = "Engine name printed after match")
	private String name;
	
	/**
	 * Possible options:
	 * leaf
	 * roottree
	 * rootleaf
	 * tds
	 * dfuct
	 * treesplit
	 */
	@Parameter(names = "-type", description = "String representing the player type.", required = true)
	private String type;
	
	@Parameter(names = "-timeout", description = "Allotted time per move in milliseconds.", required = true)
	private int timeout;
	
	/*
	 * "b" or "w"
	 */
	@Parameter(names = "-colour", description = "Player colour." , required = true)
	private Colour colour;
	
	@Parameter(names = "-ucb", description = "UCB constant.")
	private double ucbConstant = 1.07478;
	
	@Parameter(names = "-seedhost", description = "Host name of the machine running the player driver.")
	private String seedHost;
	
	@Parameter(names = "-seedport", description = "Port for the seed node of the player's ActorSystem.")
	private String seedPort;
	
	@Parameter(names = "-nodes", description = "Number of cluster members used by the player.")
	private int numNodes = 0;
	
	@Parameter(names = "-nworkers", description = "Number of threads per compute node where applicable.")
	private int numThreads = 12;
	
	@Parameter(names = "-ttpower", description = "Power for transposition table size on each CN")
	private int ttPower = 24;
//	private int ttPower = 15;
	
	@Parameter(names = "-perturncappower", description = "Number of insertions allowed per turn for HashMap-based transposition table implementations")
	private int perTurnCapacityPower = 13;
//	private int perTurnCapacityPower = 0;
	
	@Parameter(names = "-fpu", description = "First play urgency value")
	private double fpu = 0;
	
	@Parameter(names = "-decisive", description = "Determines whether decisive moves are considered in the search")
	private boolean decisive = true;
//	private boolean decisive = false;
	
	@Parameter(names = "-puwindow", description = "Initial window size for progressive unpruning. Values less than 0 imply no unpruning..:\n")
	private int initialWindowSize = 1;
//	private int initialWindowSize = 0;
	
	@Parameter(names = "-purate", description = "Decay rate for unpruning. Must be a float greater than 0. Values less than 0 imply no unpruning.")
	private double unpruningRate = 2.53649;
//	private double unpruningRate = 0;
	
	@Parameter(names = "-htype", description = "The type of heuristics to be used for ordering and progressive bias. Possible types are " +
			"\"NONE\", \"MOVE_CATEGORY\" and \"FUNCTION\" (not case sensitive)")
	private HeuristicType heuristicType = HeuristicType.MOVE_CATEGORY;
//	private HeuristicType heuristicType = HeuristicType.NONE;
	
	@Parameter(names = "-pbweight", description = "The progressive bias term of the UCT formula is multiplied by this factor")
	private double pbWeight = 12.3619;
//	private double pbWeight = 0;
	
	@Parameter(names = "-batchsize", description = "Batch size for sending explored node hashes to ClusterManager.")
	private int nodeBatchSize = 100;

	/***********************************************************
	 *  Number of parallel searches for TDS-based engines
	 * Schaefers -> Spar = num workers per rank * num ranks * 5 in experiments
	 * Yoshizoe -> Spar = total processors * 20
	 ***********************************************************/

	@Parameter(names = "-tdsoverload", description = "Number of parallel searches for TDS engine (multiplied by total number of worker cores in the cluster)")
	private int tdsOverload = 1;
	
	@Parameter(names = "-dfuctoverload", description = "Number of parallel searches for df-UCT engine (multiplied by total number of worker cores in the cluster)")
	private int dfUctOverload = 1;
	
	@Parameter(names = "-treesplitoverload", description = "Number of parallel searches for UCT-Treesplit engine (multiplied by total number of worker cores in the cluster)")
	private int uctTreesplitOverload = 5;

	/***********************************************************
	 * UCT-Treesplit constants for sharing and syncronisation  *
	 ***********************************************************/
	
		@Parameter(names = "-tsdup", description = "Ndup constant in schaefers paper")
	private int schaefersNDup = 8192;
	
	@Parameter(names = "-tsminsync", description = "Min sync constant in schaefers paper")
	private int schaefersMinSync = 8;
	
	@Parameter(names = "-tsmaxsync", description = "Max sync constant in schaefers paper")
	private int schaefersMaxSync = 16;
	
	@Parameter(names = "-tsalpha", description = "Alpha constant in schaefers paper")
	private double schaefersAlpha = 0.02;
	
	@Parameter(names = "-tsbroadratio", description = "Number of search ranks per broadcast rank worker")
	private int schaefersSearchRanksPerBroadcaster = 4;
	
	
	/*******************************************************
	 * Root constants for sharing and syncronisation       *
	 *******************************************************/
	
	@Parameter(names = "-rootmaxply", description = "Maximum ply for sharing in root parallelisation")
	private int rootSharingMaxPly = 3;
	
	@Parameter(names = "-rootmincont", description = "Minimum contribution to total playouts for sharing in root parallelisation")
	private double rootSharingMinimumContribution = 0.05;
	
	@Parameter(names = "-rootinterval", description = "Sharing interval in root parallelisation")
	private double rootSharingInterval = 0.33;
	
	
	/***************************
	 * Public getter functions *
	 ***************************/
	
	public String getLogLevel() {
		return logLevel;
	}
	
	public String getType() {
		return type;
	}
	
	public int getTimeout() {
		return timeout;
	}
	
	public int getNumNodes() {
		return numNodes;
	}
	
	public int getInitialWindowSize() {
		return initialWindowSize;
	}
	
	public double getPbWeight() {
		return pbWeight;
	}
	
	public int getSchaefersNDup() {
		return schaefersNDup;
	}
	
	public int getSchaefersMinSync() {
		return schaefersMinSync;
	}
	
	public int getSchaefersMaxSync() {
		return schaefersMaxSync;
	}
	
	public double getSchaefersAlpha() {
		return schaefersAlpha;
	}
	
	public int getRootSharingMaxPly() {
		return rootSharingMaxPly;
	}
	
	public double getRootSharingMinimumContribution() {
		return rootSharingMinimumContribution;
	}
	
	public double getRootSharingInterval() {
		return rootSharingInterval;
	}
	
	public Colour getColour() {
		return colour;
	}
	
	public double getUCBConstant() {
		return ucbConstant;
	}
	
	public String getSeedHost() {
		return seedHost;
	}
	
	public String getSeedPort() {
		return seedPort;
	}
	
	public double getFpu() {
		return fpu;
	}
	
	public boolean isDecisive() {
		return decisive;
	}
	
	public HeuristicType getHeuristicType() {
		return heuristicType;
	}
	
	public boolean isProgressiveUnpruning(){
		return initialWindowSize > 0 && unpruningRate > 0;
	}
	
	public boolean isFPU(){
		return fpu != 0 && !isProgressiveUnpruning();
	}
	
	public double getUnpruningRate() {
		return unpruningRate;
	}
	
	public int getNumThreads() {
		return numThreads;
	}
	
	public int getTtPower() {
		return ttPower;
	}
	
	public int getTdsOverload() {
		return tdsOverload;
	}
	
	public int getSchaefersSearchRanksPerBroadcaster() {
		return schaefersSearchRanksPerBroadcaster;
	}
	
	public String getName() {
		return name;
	}
	
	public int getNodeBatchSize() {
		return nodeBatchSize;
	}
	
	public int getUctTreesplitOverload() {
		return uctTreesplitOverload;
	}
	
	public int getDfUctOverload() {
		return dfUctOverload;
	}
	
	public int getPerTurnCapacityPower() {
		return perTurnCapacityPower;
	}
	
	public enum HeuristicType {
		MOVE_CATEGORY, FUNCTION, NONE
	}
}
