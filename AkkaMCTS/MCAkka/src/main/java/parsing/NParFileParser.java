package parsing;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NParFileParser {
    private static Map<String, AgentData> agentData;
    private static final String[] types = {"tds", "dfuct", "treesplit"};
    private static final String[] npars = {"1", "5", "10", "20", "50"};
    private static final double[] offsets = { -2, -1, 0, 1, 2, 3 };
    private static final int minCNs = 2;
    private static final int maxCNs = 128;

    public static void main(String[] args) throws IOException {
        initAgents();
        String inDir = "/home/marc/masters/msc-code/experiments/raw/npar";
        String outDir = "/home/marc/masters/msc-code/experiments/parsed/npar";
        for (String type : types) {
            String path = inDir + "/matches_" + type + ".out";
            File file = new File(path);
            BufferedReader in = new BufferedReader(new FileReader(file));
            parseFile(in);
        }
//		writeMergedData(outDir, 1);
		for (String s : types) {
			writeWinRates(outDir, s);
		}
    }

    private static void initAgents() {
        agentData = new HashMap<String, AgentData>();
        for (String t : types) {
            for (String n : npars) {
                String playerType = t + "-" + n;
                agentData.put(playerType, new AgentData(playerType));
            }
        }
    }

    private static void parseFile(BufferedReader in) throws IOException {
        String playerString = null;
        while ((playerString = in.readLine()) != null) {
            if (!playerString.contains("_a")) { // Filthy hack to skip over MPI output
                continue;
            }
            String[] players = playerString.split(" ");
            int playerIndex = getPlayerIndex(players);
            String playerType = getPlayerType(players[playerIndex]) + "-" + getPlayerNpar(players[playerIndex]);
            int playerCNs = getPlayerCNs(players[playerIndex]);
            AgentData data = agentData.get(playerType);
            String playerColour = playerIndex == 0 ? "B" : "W";
            String winner = in.readLine();
            if (playerColour.equals(winner)) {
                data.addWin(playerCNs);
            } else {
                data.addLoss(playerCNs);
            }
            String reason = in.readLine();
            if (!reason.equals("completed")) {
                System.out.println(playerType + "_" + playerCNs + " " + reason);
            }
            int numLines = Integer.parseInt(in.readLine());
            for (int i = 0; i < numLines - 1; i++) {
                data.addTurn(playerCNs, i, in.readLine(), playerColour);
            }
            in.readLine();
            if (numLines > 0) {
                in.readLine();
            }
        }
    }

    private static int getPlayerIndex(String[] players) {
        String[] black = players[0].split("_");
        if (black[3].equals("a")) {
            return 0;
        } else {
            return 1;
        }
    }

    private static String getPlayerType(String player) {
        String[] info = player.split("_");
        return info[0];
    }

    private static int getPlayerCNs(String player) {
        String[] info = player.split("_");
        return Integer.parseInt(info[1]);
    }

    private static int getPlayerNpar(String player) {
        String[] info = player.split("_");
        return Integer.parseInt(info[2]);
    }

    private static void writeMergedData(String outDir, int turnNumber) {
        writeRawData(outDir, turnNumber);
    }

    private static void writeRawData(String outDir, int turnNumber) {
        for (String s : types) {
            String ppsPath = outDir + "/pps/npar-" + s + "-" + turnNumber + ".out";
            String nnPath = outDir + "/numnodes/npar-" + s + "-" + turnNumber + ".out";
            if (!new File(ppsPath).delete()) {
                System.out.println("Failed to delete " + ppsPath);
            }
            if (!new File(nnPath).delete()) {
                System.out.println("Failed to delete " + nnPath);
            }
            for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
                for (String n : npars) {
                    String type = s + "-" + n;
                    String pps, numNodes;
                    System.out.println(type + "_" + cns);
                    if ((cns == 1) && s.equals("treesplit")) {
                        // No 1CN data for treesplit and leaf
                        pps = ".";
                        numNodes = ".";
                    } else {
                        pps = Double.toString(agentData.get(type).turnData.get(cns).get(turnNumber).getPPSStats()[0]);
                        numNodes = Double.toString(agentData.get(type).turnData.get(cns).get(turnNumber).getNumNodesStats()[0]);
                    }
                    if (type.equals("treesplit_50")) {
                        writeString(ppsPath, cns + " " + pps + "\n");
                        writeString(nnPath, cns + " " + numNodes + "\n");
                    } else {
                        writeString(ppsPath, cns + " " + pps + " ");
                        writeString(nnPath, cns + " " + numNodes + " ");
                    }
                }
            }
        }
    }

    private static void writeWinRates(String outDir, String type) {
    	String wrPath = outDir + "/" + "wr-" + type + ".out";
		if (!new File(wrPath).delete()) {
			System.out.println("Failed to delete " + wrPath);
		}
        for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
            for (int i = 0; i < npars.length; i++) {
                String c = npars[i];
                String wr = "";
                String errLow = "";
                String errHigh = "";
                String x = "";
                String pt = type + "-" + c;
                if ((cns == 1) && (type.equals("treesplit"))) {
                    // No 1CN data for treesplit and leaf
                    x = ".";
                    wr = ".";
                    errLow = ".";
                    errHigh = ".";
                } else {
                    x = Integer.toString(cns);
                    List<Double> wins = agentData.get(pt).wins.get(cns);
                    double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins);
                    wr = Double.toString(wrWithErrors[0] * 100);
                    errLow = Double.toString(wrWithErrors[1] * 100);
                    errHigh = Double.toString(wrWithErrors[2] * 100);
                }
                if (!x.equals(".")) {
                    // Offset along x-axis for more readable graph
                    x = Double.toString(Integer.parseInt(x) + (0.05  * offsets[i] * Integer.parseInt(x)));
                }
                if (c.equals("50")) {
                    writeString(wrPath, x + " " + wr + " " + errLow + " " + errHigh + "\n");
                } else {
                    writeString(wrPath, x + " " + wr + " " + errLow + " " + errHigh + " ");
                }
            }
        }
    }


    private static void writeString(String path, String str) {
        FileWriter out = null;
        try {
            out = new FileWriter(path, true);
            out.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
