package parsing;

import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MatchFileParser {
    private static Map<String, AgentData> agentData;
    private static final String[] types = {"leaf", "rootleaf", "roottree", "tds", "dfuct", "treesplit"};
    private static final double[] offsets = { -2, -1, 0, 1, 2, 3 };
    //	private static final String[] types = {"treesplit"};
    private static final int minCNs = 1;
    private static final int maxCNs = 128;

    public static void main(String[] args) throws IOException {
        initAgents();
        String inDir = "/home/marc/masters/msc-code/experiments/raw";
        String outDir = "/home/marc/masters/msc-code/experiments/parsed";
        for (String type : types) {
            String path = inDir + "/matches-" + type + ".out";
//			String path = inDir + "/" + type + "/matches_" + type + ".out";
//			String path = inDir + "/wr-test/matches_vanilla_npar20_" + type + ".out";
//			String path = inDir + "/wr-test/matches_" + types[0] + ".out";
//			String path = inDir + "/wr-test/matches_merged_vanilla.out";
            File file = new File(path);
            BufferedReader in = new BufferedReader(new FileReader(file));
            parseFile(in);
        }
//		for (AgentData a : agentData.values()){
//			a.writeAgentData(outDir);
//		}
        writeMergedData(outDir, 1);
//		writeMergedData(outDir, 5);
//		writeMergedData(outDir, 10);

		writeMergedWinRates(outDir);
//		writeSingleWinrate(outDir);
    }

    private static void initAgents() {
        agentData = new HashMap<String, AgentData>();
        for (String t : types) {
            agentData.put(t, new AgentData(t));
        }
    }

    private static void parseFile(BufferedReader in) throws IOException {
        String playerString = null;
        while ((playerString = in.readLine()) != null) {
            if (!playerString.contains("_a")) { // Filthy hack to skip over MPI output
                continue;
            }
            String[] players = playerString.split(" ");
            int playerIndex = getPlayerIndex(players);
            String playerType = getPlayerType(players[playerIndex]);
//			if (!playerType.equals(types[0])) continue;
            int playerCNs = getPlayerCNs(players[playerIndex]);
//			int playerCNs = getPlayerTimeout(players[playerIndex]) / 1000;
            AgentData data = agentData.get(playerType);
            String playerColour = playerIndex == 0 ? "B" : "W";
            String winner = in.readLine();
            if (playerColour.equals(winner)) {
                data.addWin(playerCNs);
            } else {
                data.addLoss(playerCNs);
            }
            String reason = in.readLine();
            if (!reason.equals("completed")) {
                System.out.println(playerType + "_" + playerCNs + " " + reason);
            }
            int numLines = Integer.parseInt(in.readLine());
            for (int i = 0; i < numLines - 1; i++) {
                data.addTurn(playerCNs, i, in.readLine(), playerColour);
            }
            in.readLine();
            if (numLines > 0) {
                in.readLine();
            }
        }
    }

    private static int getPlayerIndex(String[] players) {
        String[] black = players[0].split("_");
        if (black[3].equals("a")) {
            return 0;
        } else {
            return 1;
        }
    }

    private static String getPlayerType(String player) {
        String[] info = player.split("_");
        return info[0];
    }

    private static int getPlayerCNs(String player) {
        String[] info = player.split("_");
        return Integer.parseInt(info[1]);
    }

    private static int getPlayerTimeout(String player) {
        String[] info = player.split("_");
        return Integer.parseInt(info[2]);
    }


    private static void writeMergedData(String outDir, int turnNumber) {
        writeRawData(outDir, turnNumber);
    }

    private static void writeSeparateWinRates(String outDir) {
        for (String s : types) {
			String wrPath = outDir + "/wr/" + s + "-raw.out";
			if (!new File(wrPath).delete()) {
				System.out.println("Failed to delete " + wrPath);
			}
            for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
                String wr = "";
                String errLow = "";
                String errHigh = "";
                String x = "";
                if ((cns == 1) && (s.equals("treesplit") || s.equals("leaf"))) {
                    // No 1CN data for treesplit and leaf
                    x = ".";
                    wr = ".";
                    errLow = ".";
                    errHigh = ".";
                } else if (cns == 1 && s.equals("roottree")) {
                    // Reference opponent. Set to 50%
                    x = "1";
                    wr = "50";
                    errLow = "50";
                    errHigh = "50";
                } else {
                    x = Integer.toString(cns);
                    List<Double> wins = agentData.get(s).wins.get(cns);
                    double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins);
                    wr = Double.toString(wrWithErrors[0] * 100);
                    errLow = Double.toString(wrWithErrors[1] * 100);
                    errHigh = Double.toString(wrWithErrors[2] * 100);
                }
                writeString(wrPath, x + " " + wr + " " + errLow + " " + errHigh + "\n");
            }
        }
    }

    private static void writeMergedWinRates(String outDir) {
        String wrPath = outDir + "/wr/merged-raw.out";
        if (!new File(wrPath).delete()) {
            System.out.println("Failed to delete " + wrPath);
        }
        for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
			for (int i = 0; i < types.length; i++) {
				String s = types[i];
                String wr = "";
                String errLow = "";
                String errHigh = "";
                String x = "";
                if ((cns == 1) && (s.equals("treesplit") || s.equals("leaf"))) {
                    // No 1CN data for treesplit and leaf
                    x = ".";
                    wr = ".";
                    errLow = ".";
                    errHigh = ".";
                } else if (cns == 1 && s.equals("roottree")) {
                    // Reference opponent. Set to 50%
                    x = "1";
                    wr = "50";
                    errLow = "50";
                    errHigh = "50";
                } else {
                    x = Integer.toString(cns);
                    List<Double> wins = agentData.get(s).wins.get(cns);
                    double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins);
                    wr = Double.toString(wrWithErrors[0] * 100);
                    errLow = Double.toString(wrWithErrors[1] * 100);
                    errHigh = Double.toString(wrWithErrors[2] * 100);
                }
				if (!x.equals(".")) {
					// Offset along x-axis for more readable graph
					x = Double.toString(Integer.parseInt(x) + (0.05  * offsets[i] * Integer.parseInt(x)));
				}
                if (s.equals("treesplit")) {
                    writeString(wrPath, x + " " + wr + " " + errLow + " " + errHigh + "\n");
                } else {
                    writeString(wrPath, x + " " + wr + " " + errLow + " " + errHigh + " ");
                }
            }
        }
    }

    private static void writeSingleWinrate(String outDir) {
        String type = types[0];
        String wrPath = outDir + "/wr/ + " + type + "-wr-raw.out";
        if (!new File(wrPath).delete()) {
            System.out.println("Failed to delete " + wrPath);
        }
        for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
            String x = Integer.toString(cns);
            List<Double> wins = agentData.get(type).wins.get(cns);
            double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins);
            String wr = Double.toString(wrWithErrors[0] * 100);
            int sum = 0;
            for (double num : wins) {
                sum += num;
            }
            System.out.println(cns + ": " + wr);
            writeString(wrPath, x + " " + wr + "\n");
        }
    }

    private static void writeRawData(String outDir, int turnNumber) {
        String ppsPath = outDir + "/pps/merged-" + turnNumber + "-raw.out";
        String nnPath = outDir + "/numnodes/merged-" + turnNumber + "-raw.out";
        if (!new File(ppsPath).delete()) {
            System.out.println("Failed to delete " + ppsPath);
        }
        if (!new File(nnPath).delete()) {
            System.out.println("Failed to delete " + nnPath);
        }
        for (int cns = minCNs; cns <= maxCNs; cns *= 2) {
            for (String s : types) {
                String pps, numNodes = "";
                String x = "";
                if ((cns == 1) && (s.equals("treesplit") || s.equals("leaf"))) {
                    // No 1CN data for treesplit and leaf
                    x = ".";
                    pps = ".";
                } else {
                    System.out.println(s + "_" + cns);
                    x = Integer.toString(cns);
                    pps = Double.toString(agentData.get(s).turnData.get(cns).get(turnNumber).getPPSStats()[0]);
                    numNodes = Double.toString(agentData.get(s).turnData.get(cns).get(turnNumber).getNumNodesStats()[0]);
                }
                if (s.equals("treesplit")) {
                    writeString(ppsPath, x + " " + pps + "\n");
                    writeString(nnPath, x + " " + numNodes + "\n");
                } else {
                    writeString(ppsPath, x + " " + pps + " ");
                    writeString(nnPath, x + " " + numNodes + " ");
                }
            }
        }
    }

//	private static void writeRelativeData(String outDir, int turnNumber) {
//		String ppsPath = outDir + "/pps/merged-" + turnNumber + "-rel.out";
//		String nnPath = outDir + "/numnodes/merged-" + turnNumber + "-rel.out";
//		if (!new File(ppsPath).delete()) {
//			System.out.println("Failed to delete " + ppsPath);
//		}
//		if (!new File(nnPath).delete()) {
//			System.out.println("Failed to delete " + nnPath);
//		}
//		Map<String, Double> basePps = new HashMap<String, Double>();
//		Map<String, Double> baseNumNodes = new HashMap<String, Double>();
//		for (String s : types) {
//			double pps = agentData.get(s).turnData.get(base).get(turnNumber).getPPSStats()[0];
//			basePps.put(s, pps);
//			double numNodes = agentData.get(s).turnData.get(base).get(turnNumber).getNumNodesStats()[0];
//			baseNumNodes.put(s, numNodes);
//			if (s.equals("treesplit")) {
//				writeString(ppsPath, base + " " + 1 + "\n");
//				writeString(nnPath, base + " " + 1 + "\n");
//			} else {
//				writeString(ppsPath, base + " " + 1 + " ");
//				writeString(nnPath, base + " " + 1 + " ");
//			}
//		}
//		for (int cns = base * 2; cns <= 128; cns *= 2) {
//			for (String s : types) {
//				double pps = agentData.get(s).turnData.get(cns).get(turnNumber).getPPSStats()[0] / basePps.get(s);
//				double numNodes = agentData.get(s).turnData.get(cns).get(turnNumber).getNumNodesStats()[0] / baseNumNodes.get(s);
//				if (s.equals("treesplit")) {
//					writeString(ppsPath, cns + " " + pps + "\n");
//					writeString(nnPath, cns + " " + numNodes + "\n");
//				} else {
//					writeString(ppsPath, cns + " " + pps + " ");
//					writeString(nnPath, cns + " " + numNodes + " ");
//				}
//			}
//		}
//	}

    private static void writeString(String path, String str) {
        FileWriter out = null;
        try {
            out = new FileWriter(path, true);
            out.write(str);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
