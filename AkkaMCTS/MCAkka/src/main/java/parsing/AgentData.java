package parsing;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AgentData {
	private String agentType;
	public Map<Integer, List<TurnData>> turnData;
	public Map<Integer, List<Double>> wins;
	
	public AgentData(String agentType){
		this.agentType = agentType;
		turnData = new HashMap<Integer, List<TurnData>>();
		wins = new HashMap<Integer, List<Double>>();
		for (int i = 1; i <= 128; i *= 2) {
			turnData.put(i, new ArrayList<TurnData>());
			wins.put(i, new ArrayList<Double>());
		}
	}
	
	public void addWin(int numCns){
		wins.get(numCns).add(1.0);
	}
	
	public void addLoss(int numCns){
		wins.get(numCns).add(0.0);
	}
	
	public void addTurn(int numCns, int turnNumber, String line, String colour){
		String[] stats = line.split(" ");
		double blackPPS = Double.parseDouble(stats[0]);
		double blackNumNodes = Double.parseDouble(stats[1]);
		double blackRootReports = Double.parseDouble(stats[2]);
		double whitePPS = Double.parseDouble(stats[3]);
		double whiteNumNodes = Double.parseDouble(stats[4]);
		double whiteRootReports = Double.parseDouble(stats[5]);
		List<TurnData> dataForCns = turnData.get(numCns);
		TurnData dataForTurn = null;
		if (turnNumber >= dataForCns.size()){
			dataForTurn = new TurnData();
			dataForCns.add(turnNumber, dataForTurn);
		} else {
			dataForTurn = dataForCns.get(turnNumber);
		}
		if (colour.equals("B")){
			dataForTurn.addData(blackPPS, blackNumNodes, blackRootReports);
		} else {
			dataForTurn.addData(whitePPS, whiteNumNodes, whiteRootReports);
		}
	}
	
	// TODO: Winrate must be relative ELO
	public void writeAgentData(String outDir){
		writeRawData(outDir);
		writeRelativeData(outDir);
	}
	
	private void writeRawData(String outDir) {
		String ppsPath = outDir + "/pps/" + agentType + "-raw.out";
		String nnPath = outDir + "/numnodes/" + agentType + "-raw.out";
		String wrPath = outDir + "/wr/" + agentType + "-raw.out";
		if (!new File(ppsPath).delete()) {
			System.out.println("Failed to delete " + ppsPath);
		}
		if (!new File(nnPath).delete()) {
			System.out.println("Failed to delete " + nnPath);
		}
		if (!new File(wrPath).delete()) {
			System.out.println("Failed to delete " + wrPath);
		}
		double baseWr = TurnData.getMeanAnd95ConfidenceBound(wins.get(2))[0];
		double basePps1 = turnData.get(2).get(1).getPPSStats()[0];
		double basePps5 = turnData.get(2).get(5).getPPSStats()[0];
		double basePps10 = turnData.get(2).get(10).getPPSStats()[0];
		double baseNn1 = turnData.get(2).get(1).getNumNodesStats()[0];
		double baseNn5 = turnData.get(2).get(5).getNumNodesStats()[0];
		double baseNn10 = turnData.get(2).get(10).getNumNodesStats()[0];
		// TODO: Change base to 1 after experiments
		writeLine(wrPath, String.format("2 %f 0 0\n", baseWr));
		writeLine(ppsPath, String.format("2 %f 2 %f 2 %f\n", basePps1, basePps5, basePps10));
		writeLine(nnPath, String.format("2 %f 2 %f 2 %f\n", baseNn1, baseNn5, baseNn10));
		for (int i = 4; i <= 128; i *= 2) {
			double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins.get(i));
			double wr = wrWithErrors[0];
			double wrLow = wrWithErrors[1];
			double wrHigh = wrWithErrors[2];
			double pps1 = turnData.get(i).get(1).getPPSStats()[0];
			double pps5 = turnData.get(i).get(5).getPPSStats()[0];
			double pps10 = turnData.get(i).get(10).getPPSStats()[0];
			double nn1 = turnData.get(i).get(1).getNumNodesStats()[0];
			double nn5 = turnData.get(i).get(5).getNumNodesStats()[0];
			double nn10 = turnData.get(i).get(10).getNumNodesStats()[0];
			writeLine(wrPath, String.format("%d %3f %3f %sf\n", i, wr, wrLow, wrHigh));
			writeLine(ppsPath, String.format("%d %3f %d %3f %d %sf\n", i, pps1, i, pps5, i, pps10));
			writeLine(nnPath, String.format("%d %3f %d %3f %d %sf\n", i, nn1, i, nn5, i, nn10));
		}
	}
	
	private void writeRelativeData(String outDir) {
		String ppsPath = outDir + "/pps/" + agentType + "-rel.out";
		String nnPath = outDir + "/numnodes/" + agentType + "-rel.out";
		String wrPath = outDir + "/wr/" + agentType + "-rel.out";
		if (!new File(ppsPath).delete()) {
			System.out.println("Failed to delete " + ppsPath);
		}
		if (!new File(nnPath).delete()) {
			System.out.println("Failed to delete " + nnPath);
		}
		if (!new File(wrPath).delete()) {
			System.out.println("Failed to delete " + wrPath);
		}
		double baseWr = TurnData.getMeanAnd95ConfidenceBound(wins.get(2))[0];
		double basePps1 = turnData.get(2).get(1).getPPSStats()[0];
		double basePps5 = turnData.get(2).get(5).getPPSStats()[0];
		double basePps10 = turnData.get(2).get(10).getPPSStats()[0];
		double baseNn1 = turnData.get(2).get(1).getNumNodesStats()[0];
		double baseNn5 = turnData.get(2).get(5).getNumNodesStats()[0];
		double baseNn10 = turnData.get(2).get(10).getNumNodesStats()[0];
		// TODO: Change base to 1 after experiments
		writeLine(wrPath, String.format("2 %d 0 0\n", 1));
		writeLine(ppsPath, String.format("2 %d 2 %d 2 %d\n", 1, 1, 1));
		writeLine(nnPath, String.format("2 %d 2 %d 2 %d\n", 1, 1, 1));
		for (int i = 4; i <= 128; i *= 2) {
			double[] wrWithErrors = TurnData.getMeanAnd95ConfidenceBound(wins.get(i));
			double wr = wrWithErrors[0] / baseWr;
			double wrLow = wrWithErrors[1];
			double wrHigh = wrWithErrors[2];
			double pps1 = turnData.get(i).get(1).getPPSStats()[0] / basePps1;
			double pps5 = turnData.get(i).get(5).getPPSStats()[0] / basePps5;
			double pps10 = turnData.get(i).get(10).getPPSStats()[0] / basePps10;
			double nn1 = turnData.get(i).get(1).getNumNodesStats()[0] / baseNn1;
			double nn5 = turnData.get(i).get(5).getNumNodesStats()[0] / baseNn5;
			double nn10 = turnData.get(i).get(10).getNumNodesStats()[0] / baseNn10;
			writeLine(wrPath, String.format("%d %3f %3f %sf\n", i, wr, wrLow, wrHigh));
			writeLine(ppsPath, String.format("%d %3f %d %3f %d %sf\n", i, pps1, i, pps5, i, pps10));
			writeLine(nnPath, String.format("%d %3f %d %3f %d %sf\n", i, nn1, i, nn5, i, nn10));
		}
	}
	
	private void writeLine(String path, String line) {
		FileWriter out = null;
		try {
			out = new FileWriter(path, true);
			out.write(line);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
