package parsing;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtils;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.LogAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYErrorRenderer;
import org.jfree.data.xy.XYIntervalSeries;
import org.jfree.data.xy.XYIntervalSeriesCollection;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.text.NumberFormat;

@Deprecated
public class LineGraphPlot extends JFrame {
	private boolean withErrors;
	private XYIntervalSeriesCollection dataset;
	private XYErrorRenderer renderer;
	private String fileName;
	private String graphTitle;
	private String frameTitle;
	private LogAxis xAxis;
	private LogAxis yAxis;
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new LineGraphPlot("test", "test", "test", "test", "test", true)
						.setVisible(true);
			}
		});
	}
	
	public LineGraphPlot(String fileName, String frameTitle, String graphTitle, String xLabel, String yLabel, boolean withErrors) {   // the constructor will contain the panel of a certain size and the close operations
		super(frameTitle); // calls the super class constructor
		dataset = new XYIntervalSeriesCollection();
		renderer = new XYErrorRenderer();
		renderer.setDefaultLinesVisible(true);
		
		this.withErrors = withErrors;
		if (withErrors) {
			renderer.setDrawXError(false);
			renderer.setDrawYError(true);
		} else {
			renderer.setDrawXError(false);
			renderer.setDrawYError(false);
		}
		this.fileName = fileName;
		this.frameTitle = frameTitle;
		this.graphTitle = graphTitle;
		xAxis = new LogAxis(xLabel);
		yAxis = new LogAxis(yLabel);
//		xAxis.setSmallestValue(0);
		xAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
		xAxis.setNumberFormatOverride(NumberFormat.getIntegerInstance());
		yAxis.setStandardTickUnits(NumberAxis.createStandardTickUnits());
		yAxis.setNumberFormatOverride(NumberFormat.getIntegerInstance());
//		yAxis.setRange(0, 128);
//		yAxis.setTickUnit(new NumberTickUnit(100000));
		xAxis.setBase(2);
		yAxis.setBase(2);
	}
	
	public void addLine(String name, double[] data, double[] dataHigh, double[] dataLow) {
		XYIntervalSeries series = new XYIntervalSeries(name);
		if (withErrors) {
			series.add(2, 2, 2, 2, 2, 2);
		} else {
			series.add(2, 2, 2, 2, 0, 0);
		}
		int idx = 1;
		for (int x = 4; x <= 128 && data.length > idx; x *= 2) {
			if (withErrors) {
				// TODO: Calculate error on logged values
//				series.add(x, x, x, data[idx] / data[0], dataLow[idx] / dataLow[0], dataHigh[idx] / dataHigh[0]);
			} else {
				System.out.println(data[idx] / data[0]);
				series.add(x, x, x, data[idx] / data[0], 0, 0);
			}
			idx++;
		}
		dataset.addSeries(series);
	}
	
	public void drawGraph() {
		XYPlot plot = new XYPlot(dataset, xAxis, yAxis, renderer);
		JFreeChart chart = new JFreeChart(graphTitle, JFreeChart.DEFAULT_TITLE_FONT, plot, false);
		customizeChart(chart);
		JFrame frame = new JFrame(frameTitle);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setContentPane(new ChartPanel(chart));
		frame.pack();
		frame.setVisible(true);
		
		File imageFile = new File(fileName + ".png");
		int width = 640;
		int height = 480;
		try {
			ChartUtils.saveChartAsPNG(imageFile, chart, width, height);
		} catch (IOException ex) {
			System.err.println(ex);
		}
		
	}
	
	private void customizeChart(JFreeChart chart) {   // here we make some customization
		XYPlot plot = chart.getXYPlot();
		
//		// sets paint color for each series
//		renderer.setSeriesPaint(0, Color.BLACK);
//		renderer.setSeriesPaint(1, Color.GREEN);
//		renderer.setSeriesPaint(2, Color.YELLOW);
//
//		// sets thickness for series (using strokes)
//		renderer.setSeriesStroke(0, new BasicStroke(4.0f));
//		renderer.setSeriesStroke(1, new BasicStroke(3.0f));
//		renderer.setSeriesStroke(2, new BasicStroke(2.0f));
//
//		// sets paint color for plot outlines
//		plot.setOutlinePaint(Color.WHITE);
//		plot.setOutlineStroke(new BasicStroke(2.0f));
//
//		// sets renderer for lines
//		plot.setRenderer(renderer);
//
//		// sets plot background
//		plot.setBackgroundPaint(Color.WHITE);
//
//		// sets paint color for the grid lines
//		plot.setRangeGridlinesVisible(false);
//		plot.setRangeGridlinePaint(Color.BLACK);
//
//		plot.setDomainGridlinesVisible(true);
//		plot.setDomainGridlinePaint(Color.BLACK);
	}
}