package parsing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class TuningParser {
	static HashMap<String, MatchupResult> matches = new HashMap<String, MatchupResult>();
	static HashMap<String, Integer> agentWins = new HashMap<String, Integer>();
	static int success;
	static int failed;
	
	public static void main(String[] args) throws IOException {
		int numJobs = 6;
		String[] jobNumbers = {"176472", "176646", "176653", "176978", "176982", "177000"};
		for (int i = 1; i <= numJobs; i++){
			processJob(i, jobNumbers[i - 1]);
		}
		printResults();
	}
	
	private static void processJob(int run, String job) throws IOException {
		for (int i = 1; i <= 1000 && success < 500; i++){
			File file = new File("/home/marc/masters/tuning/run" + run + "/tuning.e" + job + "." + i);
			if (!file.exists()) continue;
			BufferedReader br = new BufferedReader(new FileReader(file));
			if (br.readLine() != null){
				failed++;
				br.close();
				continue;
			}
			file = new File("/home/marc/masters/tuning/run" + run + "/tuning.o" + job + "." + i);
			br = new BufferedReader(new FileReader(file));
			if (checkMatches(br)){
				br.close();
				br = new BufferedReader(new FileReader(file));
				processFile(br);
				success++;
			} else {
				failed++;
			}
			br.close();
		}
	}
	
	private static boolean checkMatches(BufferedReader br) throws IOException {
		for (int k = 0; k < 12; k++) {
			String matchup;
			if ((matchup = br.readLine()) == null){
				return false;
			}
			String winner = br.readLine();
			String reason = br.readLine();
			int numTurns = Integer.parseInt(br.readLine());
			if (reason.equals("illegal")) {
				return false;
			} else if (reason.equals("timeout")) {
				return false;
			}
			for (int j = 0; j < numTurns; j++) {
				br.readLine();
			}
			br.readLine();
		}
		return true;
	}
	
	private static void processFile(BufferedReader br) throws IOException {
		for (int k = 0; k < 12; k++) {
			String matchup = br.readLine();
			String winner = br.readLine();
			String reason = br.readLine();
			int numTurns = Integer.parseInt(br.readLine());
			if (!matches.containsKey(matchup)) {
				matches.put(matchup, new MatchupResult());
			}
			String[] players = matchup.split(" ");
			if (!agentWins.containsKey(players[0])){
				agentWins.put(players[0], 0);
			}
			if (!agentWins.containsKey(players[1])){
				agentWins.put(players[1], 0);
			}
			matches.get(matchup).addMatch();
			if (winner.equals("B")) {
				matches.get(matchup).addBlackWin();
				agentWins.put(players[0], agentWins.get(players[0]) + 1);
			} else {
				matches.get(matchup).addWhiteWin();
				agentWins.put(players[1], agentWins.get(players[1]) + 1);
			}
			for (int j = 0; j < numTurns; j++) {
				br.readLine();
			}
			br.readLine();
		}
	}
	
	private static void printResults(){
		System.out.println("Number of failed jobs = " + failed);
		System.out.println("Number of completed jobs = " + success + ". Results: ");
		for (Map.Entry e : matches.entrySet()){
			System.out.println(e.getKey() + ": " + e.getValue());
		}
		System.out.println();
		System.out.println("Total wins:");
		for (Map.Entry e : agentWins.entrySet()){
			System.out.println(e.getKey() + ": " + e.getValue());
		}
	}
	
}
