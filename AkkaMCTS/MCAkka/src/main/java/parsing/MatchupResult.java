package parsing;

public class MatchupResult {
	private int numPlayed;
	private int blackWins;
	private int whiteWins;
	
	public MatchupResult() {
		numPlayed = 0;
		blackWins = 0;
		whiteWins = 0;
	}
	
	public void addMatch(){
		numPlayed++;
	}
	
	public void addBlackWin(){
		blackWins++;
	}
	
	public void addWhiteWin(){
		whiteWins++;
	}
	
	public String toString(){
		return blackWins + " | " + whiteWins;
	}
}
