package parsing;

import java.util.ArrayList;
import java.util.List;

public class TurnData {
	private List<Double> pps;
	private List<Double> uniqueNodes;
	private List<Double> rootReports;

	public TurnData(){
		pps = new ArrayList<Double>();
		uniqueNodes = new ArrayList<Double>();
		rootReports = new ArrayList<Double>();
	}
	
	public static double[] getMeanAnd95ConfidenceBound(List<Double> data){
		double sum = 0.0;
		for (double num : data){
			sum += num;
		}
		double mean = sum / data.size();
		double squaredDifferenceSum = 0.0;
		for (double num : data){
			squaredDifferenceSum += (num - mean) * (num - mean);
		}
		double variance = squaredDifferenceSum / data.size();
		double standardDeviation = Math.sqrt(variance);
		double confidenceLevel = 1.96;
		double range = confidenceLevel * standardDeviation / Math.sqrt(data.size());
		return new double[]{mean, mean - range, mean + range};
	}
	
	public int getNumMatchesWithThisTurn(){
		return pps.size();
	}

	public double[] getPPSStats(){
		return getMeanAnd95ConfidenceBound(pps);
	}
	
	public double[] getNumNodesStats(){
		return getMeanAnd95ConfidenceBound(uniqueNodes);
	}
	
	public double[] getRootReportStats(){
		return getMeanAnd95ConfidenceBound(rootReports);
	}
	
	public void addData(double pps, double uniqueNodes, double rootReports){
		this.pps.add(pps);
		this.uniqueNodes.add(uniqueNodes);
		this.rootReports.add(rootReports);
	}
}
