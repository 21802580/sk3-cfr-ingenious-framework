package logging;

import com.esotericsoftware.minlog.Log;
import com.esotericsoftware.minlog.Log.Logger;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * A logger implementation for the {@link Log} class. In addition to printing to the console, this logger
 * also outputs to a log file on the file system.
 */
public class FileLogger extends Logger implements Closeable {
    private FileWriter fw;
    private boolean printClean;
    private String finalFileName;

    public FileLogger(String directory, String type, boolean printClean) {
        super();
        this.printClean = printClean;
        new File(directory).mkdir();
        String baseFileName = directory + File.separator + type;
        int fileCount = 1;
        while ((new File(baseFileName + "-" + fileCount + ".log").isFile())) {
            fileCount++;
        }
        finalFileName = baseFileName + "-" + fileCount + ".log";
        try {
            fw = new FileWriter(finalFileName);
        } catch (IOException e) {
            System.err.println("Logger: could not create log file with name " + finalFileName + ". Will only log to standard out.");
        }
    }

    /*
     * Set the log level according to the provided String
     */
    public static void setLogLevel(String levelString) {
        switch (levelString.toLowerCase()) {
            case "none":
                Log.NONE();
                break;
            case "error":
                Log.ERROR();
                break;
            case "warn":
                Log.WARN();
                break;
            case "info":
                Log.INFO();
                break;
            case "debug":
                Log.DEBUG();
                break;
            case "trace":
                Log.TRACE();
                break;
        }
    }

    public void log(int level, String category, String message, Throwable ex) {
        if (printClean) {
            print(message);
        } else {
            super.log(level, category, message, ex);
        }
    }

    @Override
    protected void print(String message) {
        super.print(message);
        if (fw != null) {
            try {
                fw.write(message + "\n");
                fw.flush();
            } catch (IOException e) {
                System.err.println("Logger: could not log to file.");
            }
        }
    }

    @Override
    public void close() throws IOException {
        Log.info("Closing logger for " + finalFileName);
        fw.close();
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        fw.close();
    }

}
