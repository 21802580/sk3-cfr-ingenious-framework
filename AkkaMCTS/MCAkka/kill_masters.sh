#!/bin/bash

ps aux | grep "masters" | awk '{print $2}' | xargs kill
