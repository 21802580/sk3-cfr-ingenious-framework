#!/bin/bash

portb="$(shuf -i 5000-60000 -n 1)"
portw="$(shuf -i 5000-60000 -n 1)"

while [ $portb = $portw ]
do
	portw="$(shuf -i 5000-60000 -n 1)"
done

#for local test:
#./runTest.sh 127.0.0.1 bp leaf 2000 wp leaf 2000 1 1

#Type options:
#leaf
#roottree
#rootleaf
#tds
#dfuct
#treesplit

seedhost=$1
nameb=$2
typeb=$3
timeb=$4
namew=$5
typew=$6
timew=$7
numb=$8
numw=$9

argsb="-seedhost $seedhost -seedport $portb -name $nameb -type $typeb -timeout $timeb -colour BLACK -nodes $numb -nworkers 1"
argsw="-seedhost $seedhost -seedport $portw -name $namew -type $typew -timeout $timew -colour WHITE -nodes $numw -nworkers 1"
matchargs="$argsb _ $argsw"

#need to launch one extra cluster per colour, hence 4 calls below (According to Marc)

java -jar target/clustermatch.jar $argsb _ $argsw &

java -jar target/clustermember.jar $argsb &
java -jar target/clustermember.jar $argsw &
java -jar target/clustermember.jar $argsb &
java -jar target/clustermember.jar $argsw
