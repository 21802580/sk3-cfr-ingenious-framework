#!/bin/bash

matchscript="/mnt/lustre/users/mchristoph/scaling/run_match.sh"
run_num=$1
agents=("leaf" "rootleaf" "roottree" "tds" "dfuct" "treesplit")
#agents=("leaf")
opponents=("leaf" "rootleaf" "roottree" "tds" "dfuct" "treesplit" "serial1000" "serial8000")
numnodes=(2 4 8)
selects=(
"select=1:ncpus=4:mpiprocs=1:mem=8GB+6:ncpus=24:mpiprocs=2:mem=64GB"
"select=1:ncpus=4:mpiprocs=1:mem=8GB+7:ncpus=24:mpiprocs=2:mem=64GB"
"select=1:ncpus=4:mpiprocs=1:mem=8GB+9:ncpus=24:mpiprocs=2:mem=64GB"
)
queue="normal"
matchargs=()
matchscript="/mnt/lustre/users/mchristoph/job_chaining/scripts/run_match.sh"

for agent in ${agents[*]}
do
	nodeidx=0
	for nodes in ${numnodes[*]}
	do
		for opponent in ${opponents[*]}
		do
			numagent=$nodes
			numopponent=8
			agentname="${agent}_${nodes}_1000_a"
			opponentname=""
			opponenttype=$opponent
			opponenttime="1000"
			if [ $opponent = "serial1000" ]
			then
				opponentname="serial_1_1000_b"
				opponenttype="serial"
				numopponent=1
		
			elif [ $opponent = "serial8000" ]
			then
				opponentname="serial_1_8000_b"
				opponenttype="serial"
				opponenttime="8000"
				numopponent=1
			else	
				opponentname="${opponent}_8_1000_b"
			fi
			select="${selects[${nodeidx}]}"
			jobname="${agentname}_${opponentname}_${run_num}"
			vargsb="testtype=$agent,typeb=$agent,nameb=$agentname,timeb=1000,numb=$numagent,typew=$opponenttype,namew=$opponentname,timew=$opponenttime,numw=$numopponent"
			vargsw="testtype=$agent,typeb=$opponenttype,nameb=$opponentname,timeb=$opponenttime,numb=$numopponent,typew=$agent,namew=$agentname,timew=1000,numw=$numagent"
			subbargsb="-l $select -P CSCI1175 -M 16960769@sun.ac.za -l walltime=00:05:00 -N ${jobname}_B -q ${queue} -v ${vargsb}"
			subbargsw="-l $select -P CSCI1175 -M 16960769@sun.ac.za -l walltime=00:05:00 -N ${jobname}_W -q ${queue} -v ${vargsw}"
			matches+=("$subbargsb")
			matches+=("$subbargsw")
		done
	nodeidx=$(($nodeidx + 1))
	done
done

for ((i = 0; i < ${#matches[*]}; i++))
do
	match=${matches[$i]}
	#echo ${match}
	while [ $(qselect -u mchristoph -q normal | wc -l) -gt 19 ]; do
		sleep 1s
	done
        if [ $i -eq 140 ]
        then
                job=$(qsub ${match} -m abe ${matchscript})
        else
                job=$(qsub ${match} ${matchscript})
        fi

done
