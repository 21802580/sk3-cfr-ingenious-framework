#!/bin/bash

job=$1
trunc=$(qstat -x -f -Fdsv ${job} | awk -F'Exit_status=' '{print $2}')
echo ${trunc:0:1}

