#!/bin/bash

function join_by { local IFS="$1"; shift; echo "$*"; }
seedcom="java -XX:+UseG1GC -Xms8g -Xmx8g -jar"
membercom="java -XX:+UseG1GC -Xms16g -Xmx16g -jar"

cd /mnt/lustre/users/mchristoph/job_chaining/log/


module load chpc/java/1.8.0_73
module load chpc/openmpi/2.0.0/gcc-6.2.0_java-1.8.0_73

readarray -t nodes < ${PBS_NODEFILE}

#testype=$2
#nameb=$3
#typeb=$4
#timeb=$5
#namew=$6
#typew=$7
#timew=$8

totalb=$((numb+1))
totalw=$((numw+1))

# Parse node file
seedhost="${nodes[0]}"
nodestrb=$(join_by , "${nodes[@]:1:$totalb}")
nodestrw=$(join_by , "${nodes[@]:1+$totalb:$totalw}") # Add 1 node to each agent for the counter rank

portb="$(shuf -i 5000-60000 -n 1)"
portw="$(shuf -i 5000-60000 -n 1)"
while [ $portb = $portw ]
do
	portw="$(shuf -i 5000-60000 -n 1)"
done

argsb="-seedhost $seedhost -seedport $portb -name $nameb -type $typeb -timeout $timeb -colour BLACK -nodes $numb -nworkers 12"
argsw="-seedhost $seedhost -seedport $portw -name $namew -type $typew -timeout $timew -colour WHITE -nodes $numw -nworkers 12"
matchargs="$argsb _ $argsw"
matchjarpath="/mnt/lustre/users/mchristoph/job_chaining/clustermatch.jar"
memberjarpath="/mnt/lustre/users/mchristoph/job_chaining/clustermember.jar"
outfile="/mnt/lustre/users/mchristoph/job_chaining/${testtype}/matches_${testtype}.out"

#echo "mpirun -np 1 --host $seedhost $seedcom $matchjarpath $matchargs : -np $totalb --host $nodestrb $membercom $memberjarpath $argsb : -np $totalw --host $nodestrw $membercom $memberjarpath $argsw >> $outfile"
mpirun -np 1 --host $seedhost $seedcom $matchjarpath $matchargs : -np $totalb --host $nodestrb $membercom $memberjarpath $argsb : -np $totalw --host $nodestrw $membercom $memberjarpath $argsw >> $outfile
