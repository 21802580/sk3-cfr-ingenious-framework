#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle jarFat -p $root/../../

# Create lobby
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "$root/mdp.json" -game "MDPReferee" -lobby "mylobby" -numMatches 10 &

wait

# Create client
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "p1" -engine "za.ac.sun.cs.ingenious.games.mdp.engines.MDPPolicyIterationEngine" -game "MDPReferee" -config "$root/mdp.json" &

wait

cd $root

echo "Done"