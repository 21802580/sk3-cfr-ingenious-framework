#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Switch to python dir to run from there.
cd $root/../../src/main/python/

# Build JAR
gradle jarFat -p $root/../../

# Create lobby
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "$root/snake.json" -game "SnakeReferee" -lobby "mylobby" -numMatches 300 -port 61234 &

wait

# Create client
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "p1" -port 61234 -engine "za.ac.sun.cs.ingenious.games.snake.engines.SnakeExternalDQNEngine" -game "SnakeReferee" -config "$root/snake.json" &

# Run python
$root/../../src/main/python/venv/bin/python -m src.main

wait

cd $root

echo "Done"