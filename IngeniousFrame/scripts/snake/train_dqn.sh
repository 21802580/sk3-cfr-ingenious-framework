#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Ensure we have a directory to store the tensorboard runs.
mkdir $root/../../src/main/python/runs

# Switch to python dir to run from there.
cd $root/../../src/main/python/

# Build JAR
gradle jarFat -p $root/../../

# Run Python app
$root/../../src/main/python/venv/bin/python -m src.main &

# Run tensorboard and open in browser
$root/../../src/main/python/venv/bin/tensorboard --logdir $root/../../src/main/python/runs --port 8080 &
xdg-open http://localhost:8080 &

# Run training sandbox
java -cp $root/../../build/libs/IngeniousFrame-all-0.0.4.jar za.ac.sun.cs.ingenious.search.rl.TrainingSandbox snakeDQN 100000

wait

cd $root

echo "Done"