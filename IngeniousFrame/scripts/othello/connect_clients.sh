#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
-username "bob" \
-engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloRandomEngine" \
-game "OthelloReferee" -hostname localhost -port 61234 &

java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client \
-username "alice" \
-engine "za.ac.sun.cs.ingenious.games.othello.engines.OthelloRandomEngine" \
-game "OthelloReferee" -hostname 127.0.0.1 -port 61234
