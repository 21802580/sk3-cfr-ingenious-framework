#!/bin/bash
# Here we start two clients
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player1" -config "Go.json" &
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player2" -config "Go.json"

# Example script for running tests
#for i in `seq 1 $1`
#do
#  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "Go.json" -game "GoReferee" -lobby "mylobby" -port $4
#  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$2_player1" -config "EnhancementChoices/EnhancementChoice$2.json" -engine "za.ac.sun.cs.ingenious.games.go.engines.TreeEngine.GoMCTSTreeGenericEngine" -threadCount $5 -game "GoReferee" -hostname localhost -port $4 &
#  java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$3_player2" -config "EnhancementChoices/EnhancementChoice$3.json" -engine "za.ac.sun.cs.ingenious.games.go.engines.TreeEngine.GoMCTSTreeGenericEngine" -threadCount $6 -game "GoReferee" -hostname 127.0.0.1 -port $4
#  echo "-" -n
#done

#cat ResultsGo.txt > Experiments/"$2$3_$5_$6threads_3seconds".txt
#rm ResultsGo.txt
