#!/bin/bash

# creates a new tictactoe game with the default configuration
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "LOA.json" -game "loa" -lobby "mylobby" -players 2 -numMatches 1
