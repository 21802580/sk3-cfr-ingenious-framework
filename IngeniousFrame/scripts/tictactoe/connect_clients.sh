#!/bin/bash
# Here we start two clients
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client

java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player1" -config "TicTacToe.json" &
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player2" -config "TicTacToe.json"