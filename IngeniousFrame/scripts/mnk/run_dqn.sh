#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Switch to python dir to run from there.
cd $root/../../src/main/python/

# Build JAR
gradle jarFat -p $root/../../

# Create lobby
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "$root/mnk.json" -game "MNKReferee" -lobby "mylobby" -numMatches 3 &

wait

# Create clients
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "p1" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKExternalDQNEngine" -game "MNKReferee" -config "$root/mnk.json" &

# Run python
$root/../../src/main/python/venv/bin/python -m src.main

wait

cd $root

echo "Done"