#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
# Also note that hostname can be an IP address as in the case of the second client

#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "Phantom444.json" -game "mnk" -lobby "mylobby" -players 2
#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "bob" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine" -game "mnk" -threadCount 1 -hostname localhost -port 61234 &
#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "alice" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKMOISMCTSEngine" -game "mnk" -threadCount 1 -hostname 127.0.0.1 -port 61234 &

#java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "gameboard.json" -game "mnk" -lobby "mylobby" -port $5
java -jar ../../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player1_$1" -config "config/$1.json" -engine "za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures.MNKSSSEngine" -threadCount $2 -game "mnk" -hostname localhost -port $5 &
java -jar ../../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player2_$3" -config "config/$3.json" -engine "za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures.MNKSSSEngine" -threadCount $4 -game "mnk" -hostname 127.0.0.1 -port $5

#java -jar ../../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "$3_player1" -config "../EnhancementChoices/EnhancementChoice$3.json" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.TreeEngine.MNKMCTSTreeGenericEngine" -threadCount $4 -game "mnk" -hostname localhost -port $5

