#!/bin/bash
# Here we start two clients bob and alice
# Note that you can exclude the hostname and port if you're connecting to localhost.
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player1" -config "mnk.json" &
java -jar ../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "player2" -config "mnk.json"