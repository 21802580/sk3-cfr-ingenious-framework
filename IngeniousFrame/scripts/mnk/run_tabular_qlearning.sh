#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle jarFat -p $root/../../

# Create lobby
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar create -config "$root/mnk.json" -game "MNKReferee" -lobby "mylobby" -numMatches 3 &

wait

# Create clients
java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "p1" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKTabularQLearningEngine" -game "MNKReferee" -config "$root/mnk.json" &

wait

cd $root

echo "Done"