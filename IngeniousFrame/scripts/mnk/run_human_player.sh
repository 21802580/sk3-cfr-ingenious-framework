#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

java -jar $root/../../build/libs/IngeniousFrame-all-0.0.4.jar client -username "p2" -engine "za.ac.sun.cs.ingenious.games.mnk.engines.MNKHumanPlayerEngine" -game "MNKReferee" -config "$root/mnk.json"