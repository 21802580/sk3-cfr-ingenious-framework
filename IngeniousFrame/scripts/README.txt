Make sure you run: gradle jarFat in the root of the repository before using these scripts.

Order of running scripts:
1) ./start_server.sh
2) ./create_game.sh
3) ./connect_clients.sh

The configuration for the game is given in the <game>.json file and all parameters should preferably be altered here.
For running tests it might be more convenient to specify the parameters on the command line by adding tags
such as "-threadCount 4", these additional command line parameters take precedence over the .json file parameters.
This also allows you to set different parameters for each player (i.e. different thread numbers etc.).

The framework has three levels of game configuration setup, each with their own level of precedence.
1. Configuration file (.json) - highest level of precedence.
2. Command-line configuration tag definitions - second highest level of precedence
3. Default configuration values - lowest level of precedence.

When the same field of configuration (example hostname) is defined on multiple levels of the precedence
options, the higher precedence option is used for the configuration value.

Default values for config file options:
port: 61234
hostname: localhost
enhancement: "EnhancementChoices/EnhancementChoiceVanilla.json"
threadCount: 1
turnLength: 1000
lobby: null
game: null
engine: null

The default configuration values for the server are set in: za/ac/sun/cs/ingenious/core/commandline/gameserver/GameServerArguments.java
The default configuration values for the game creator are set in: za/ac/sun/cs/ingenious/core/commandline/gamecreator/GameCreatorArguments.java
The default configuration values for the client are set in: za/ac/sun/cs/ingenious/core/commandline/client/ClientArguments.java
