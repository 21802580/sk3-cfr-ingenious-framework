#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle jarFat -p $root/../../

# Run training sandbox
java -cp $root/../../build/libs/IngeniousFrame-all-0.0.4.jar za.ac.sun.cs.ingenious.search.rl.TrainingSandbox gridWorldMonteCarloOffPolicy ${1-5000} $root/${2-cliff.gridworld}

wait

cd $root

echo "Done"