#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle build jarFat -p "$root/../../"

# Create lobby
java -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" create -config "$root/gridWorld.json" -game "GridWorldReferee" -lobby "mylobby" -numMatches 300 &

wait

# Create client
java -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldExternalDQNEngine" -game "GridWorldReferee" -config "$root/gridWorld.json" &


# Switch to python dir to run from there.
cd "$root/../../src/main/python/"

# Run python
"$root/../../src/main/python/venv/bin/python" -m src.main

wait

cd "$root"

echo "Done"
