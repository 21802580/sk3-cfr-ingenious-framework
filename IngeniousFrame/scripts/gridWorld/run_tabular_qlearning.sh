#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Build JAR
gradle jarFat -p "$root/../../"

# Create lobby
java -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" create -config "$root/gridWorld.json" -game "GridWorldReferee" -lobby "mylobby" -numMatches 300 &

wait

# Create client
java -jar "$root/../../build/libs/IngeniousFrame-all-0.0.4.jar" client -username "p1" -engine "za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldTabularQLearningEngine" -game "GridWorldReferee" -config "$root/gridWorld.json" &

wait

cd "$root"

echo "Done"
