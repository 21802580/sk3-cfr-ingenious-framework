#!/bin/bash
trap "kill 0" SIGINT

root=$(pwd)

# Switch to python dir to run from there.
cd $root/../../src/main/python/

# Build JAR
gradle jarFat -p $root/../../

# Run Python app
$root/../../src/main/python/venv/bin/python -m src.main &

# Run training sandbox
java -cp $root/../../build/libs/IngeniousFrame-all-0.0.4.jar za.ac.sun.cs.ingenious.search.rl.TrainingSandbox gridWorldLFAQLearning ${1-100000} $root/${2-cliff.gridworld}

wait

cd $root

echo "Done"