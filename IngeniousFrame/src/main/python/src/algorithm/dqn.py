import numpy as np
import matplotlib.pyplot as plt
import numpy as np
from src.core.util.experience_replay.replay_buffer import ReplayBuffer
from src.core.util.transition.transition import Transition
from src.core.util.transition.transition_batch import TransitionBatch
from src.policy.dqn.dqn_network import DQNNetwork


class DQN(object):
    """
    Main DQN algorithm that trains a network by sampling from the replay memory. Handles received transitions and
    performs a forward pass for action values when they are requested.
    """

    def __init__(self, debug=False):
        self.initialized = False
        self.buffer_memory = None
        self.sample_size = None
        self.sample_threshold = None
        self.sample_freq = None
        self.gamma = None
        self.double_dqn = None

        self.replay_buffer = None
        self.net = None

        self.step_count = 0
        self.debug = debug

    def init(self,
             player_id: str,
             network: str,
             num_channels: int,
             height: int,
             width: int,
             action_count: int,
             buffer_memory: int,
             sample_size: int,
             gamma: float,
             sample_threshold: int,
             sample_freq: int,
             double_dqn: bool,
             prioritized: bool,
             buffer_alpha: float,
             learning_rate: float,
             target_update_freq: int,
             ):
        self.net = DQNNetwork(
            player_id,
            network,
            num_channels,
            height, width,
            action_count,
            learning_rate,
            target_update_freq
        )

        print("\nInitializing DQN with params:")
        print(f"\tsample_size:\t\t{sample_size}")
        print(f"\tgamma:\t\t\t\t{gamma}")
        print(f"\tsample_threshold:\t{sample_threshold}")
        print(f"\tsample_freq:\t\t{sample_freq}")
        print(f"\tdouble_dqn:\t\t\t{double_dqn}")

        self.buffer_memory = buffer_memory
        self.sample_size = sample_size
        self.gamma = gamma
        self.sample_threshold = sample_threshold
        self.sample_freq = sample_freq
        self.double_dqn = double_dqn
        self.replay_buffer = ReplayBuffer(self.buffer_memory, prioritized, buffer_alpha)
        self.initialized = True

    def train(self, transition: Transition):
        if not self.initialized:
            print("DQN: An attempt to was made train before initialization.")
            return

        # Add transition to the experience replay buffer.
        self.replay_buffer.put(transition)
        self.step_count += 1

        if self.debug:
            self.plot_transition(transition, self.step_count)

        # print(f"Added transition to replay buffer: {str(transition)}")

        # Sample from the replay buffer.
        [sampled_indices, sampled_transitions] = self.replay_buffer.sample(self.sample_size)

        # If there are not enough transitions yet in the buffer, skip training for now.
        if self.replay_buffer.count < self.sample_threshold:
            return

        # Only sample every sample_freq steps.
        if self.step_count % self.sample_freq != 0:
            return

        # Create a transition batch from the sampled transitions. This essentially zips together the state, action,
        # reward and next_state components into their own respective "parallel" lists.
        transition_batch = TransitionBatch(sampled_transitions)

        # Get a list of expected values for Q(s,a) for each transition.
        value_batch = list(map(self.transition_value, sampled_transitions))

        # Train the network for the batch of states with corresponding actions and values.
        loss = self.net.train(transition_batch.state_batch, transition_batch.action_batch, value_batch)
        self.replay_buffer.prioritize(sampled_indices, loss)

    def get_action_values_dict(self, state):
        """
        Returns a dictionary where the keys are the action numbers and the values are the action values for the provided
        state.
        :param state:
        :return:
        """

        if not self.initialized:
            print("DQN: An attempt to was made to retrieve action values before initialization.")
            return

        action_values = self.net.get_action_values(state.unsqueeze(0))

        return {value: index for value, index in enumerate(action_values)}

    def get_target_action_values_dict(self, state):
        """
        Returns a dictionary where the keys are the action numbers and the values are the action values for the provided
        state, using the target network.
        :param state:
        :return:
        """

        if not self.initialized:
            print("DQN: An attempt to was made to retrieve action values before initialization.")
            return

        target_action_values = self.net.get_target_action_values(state.unsqueeze(0))

        return {value: index for value, index in enumerate(target_action_values)}

    def get_greedy_action_value(self, state):
        """
        Returns the value of the action with the highest expected value for the given state.
        :param state:
        :return:
        """
        action_values = self.net.get_action_values(state.unsqueeze(0))
        return np.max(action_values)

    def get_greedy_action(self, state):
        """
        Returns the value of the action with the highest expected value for the given state.
        :param state:
        :return:
        """

        if not self.initialized:
            print("DQN: An attempt to was made to retrieve a greedy action choice before initialization.")
            return

        action_values = self.net.get_action_values(state.unsqueeze(0))
        return np.argmax(action_values)

    def get_target_greedy_action_value(self, state):
        """
        Returns the value of the action with the highest expected value for the given state, using the target network.
        :param state:
        :return:
        """

        if not self.initialized:
            print("DQN: An attempt to was made to retrieve a greedy action choice before initialization.")
            return

        action_values = self.net.get_target_action_values(state.unsqueeze(0))
        return np.max(action_values)

    def transition_value(self, transition):
        """
        Calculates the expected value for the state-action pair in `transition`, given the reward received and the
        expected value approximated by the neural network.
        :param transition:
        :return:
        """

        if transition.next_state is None:
            return transition.reward
        elif self.double_dqn is False:
            # Vanilla DQN Bellman equation.
            greedy_action_value = self.get_target_greedy_action_value(transition.next_state)
            return transition.reward + self.gamma * greedy_action_value
        elif self.double_dqn is True:
            # Double DQN modified Bellman equation.

            # Get the max action using the main network.
            greedy_action = self.get_greedy_action(transition.next_state)

            # Using the greedy action of the main network, retrieve its action value in the target network.
            target_greedy_action_values_dict = self.get_target_action_values_dict(transition.next_state)
            target_greedy_action_value = target_greedy_action_values_dict[greedy_action]
            return transition.reward + self.gamma * target_greedy_action_value

    def plot_transition(self, transition: Transition, step: int):
        if step % 400 != 0:
            return

        plt.imshow(transition.state[0].cpu().numpy(), cmap="gray")
        plt.title(f'{step}_state_layer_1')
        plt.savefig('state_0.png')
        plt.imshow(transition.state[1].cpu().numpy(), cmap="gray")
        plt.title(f'{step}_state_layer_2')
        plt.savefig('state_1.png')
        if transition.next_state is not None:
            plt.imshow(transition.next_state[0].cpu().numpy(), cmap="gray")
            plt.title(f'{step}_next_state_layer_1')
            plt.savefig('next_state_0.png')
            plt.imshow(transition.next_state[1].cpu().numpy(), cmap="gray")
            plt.title(f'{step}_next_state_layer_2')
            plt.savefig('next_state_1.png')

    def backup_network(self, filename):
        if not self.initialized:
            print("DQN: An attempt to was made to backup the network before initialization.")
            return

        self.net.backup_network(filename)


dqn = DQN()

if __name__ == '__main__':
    state_0 = [[[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_1 = [[[0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_2 = [[[0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_3 = [[[0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_4 = [[[0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_5 = [[[0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_6 = [[[0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_7 = [[[0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_8 = [[[0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]
    state_9 = [[[0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]]

    dqn = DQN()
    # dqn.init_feature_counts(20, 4)

    print(f"Action number (S1): {str(dqn.get_action_values_dict(state_1))}")
    print(f"Action value (S1): {str(dqn.get_greedy_action_value(state_1))}")

    dqn.train(Transition(state_0, 0, 1, state_1))
    dqn.train(Transition(state_1, 0, 1, state_2))
    dqn.train(Transition(state_2, 0, 1, state_3))
    dqn.train(Transition(state_3, 1, -1, state_2))
    dqn.train(Transition(state_2, 1, -1, state_1))
    dqn.train(Transition(state_1, 1, -1, state_0))
