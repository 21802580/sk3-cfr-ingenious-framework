from enum import IntEnum


class MessageType(IntEnum):
    # Exit request
    EXIT = 1

    # General acknowledgement
    ACK = 2

    # Message requesting state-actions with values.
    REQ_TABLE_STATE_ACTIONS = 3

    # Message containing state-actions with the corresponding values.
    TABLE_STATE_ACTIONS = 4

    # Message requesting value for specific state-action pair.
    REQ_TABLE_STATE_ACTION_VALUE = 5

    # Message containing the value of a state-action pair.
    TABLE_STATE_ACTION_VALUE = 6

    # Message specifying a new value for a state-action pair.
    SET_TABLE_STATE_ACTION_VALUE = 7

    # Message requesting values for multiple feature lists using linear function approximation.
    REQ_LINEAR_FEATURE_VALUES = 8

    # Message containing the size of feature sets that will be used.
    INIT_LINEAR_FEATURE_APPROXIMATION = 9

    # Message containing a list of values, with each value corresponding to a feature list sent in REQ_FEATURE_VALUES.
    LINEAR_FEATURE_VALUES = 10

    # Message containing a single set of features and a sampled value.
    TRAIN_LINEAR_FEATURES = 11

    # Message requesting action values for provided state using neural net.
    REQ_NEURAL_NET_ACTION_VALUES = 12

    # Message containing the size of input and output features that will be used.
    INIT_NEURAL_NET = 13

    # Message containing a list of action values, with each value corresponding to a action for the state sent in
    # REQ_NEURAL_NET_GREEDY_ACTION.
    NEURAL_NET_ACTION_VALUES = 14

    # Message containing a transition for DQN.
    NEURAL_NET_PUT_TRANSITION = 15

    # Message requesting that the DQN NN be backed up.
    NEURAL_NET_CREATE_BACKUP = 16
