from src.core.network.message.type.message_type import MessageType


class IngeniousMessage(object):
    """
    Base class for messages received from and sent to the Ingenious Framework.
    """

    def __init__(self, message_dict: dict = None, type: MessageType = None, payload=None):
        if message_dict is not None:
            self.type = message_dict['type']

            if 'payload' in message_dict:
                self.payload = message_dict['payload']
        else:
            self.type = type
            self.payload = payload

    def to_dict(self):
        return self.__dict__
