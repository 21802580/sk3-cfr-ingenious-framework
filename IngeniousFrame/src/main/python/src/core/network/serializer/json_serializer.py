import json

from src.core.network.message.ingenious_message import IngeniousMessage
from src.core.network.serializer.custom_json_encoder import CustomJsonEncoder
from src.core.network.serializer.serializer import Serializer


class JsonSerializer(Serializer):
    """
    Class used for encoding and decoding messages with the framework. Uses `CustomJsonEncoder`.
    """

    def decode_message(self, message_raw: str) -> IngeniousMessage:
        message_dict = json.loads(message_raw)
        return IngeniousMessage(message_dict=message_dict)

    def encode_message(self, message: IngeniousMessage) -> str:
        return json.dumps(message, cls=CustomJsonEncoder)
