from abc import ABC, abstractmethod

from src.core.network.message.ingenious_message import IngeniousMessage


class Serializer(ABC):
    """
    Abstract class that defines the API for a serializer. This should allow non JSON serialization in the future.
    """

    @abstractmethod
    def decode_message(self, message_raw: str):
        pass

    @abstractmethod
    def encode_message(self, message: IngeniousMessage) -> str:
        pass
