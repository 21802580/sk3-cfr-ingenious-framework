import random
import sys
from functools import reduce

from src.core.util.transition.transition import Transition


class ReplayBuffer(object):
    """
    Stores S, A, R, S' transitions in a buffer and provides a manner in which to sample transitions from this buffer.
    """

    def __init__(self, buffer_size: int, prioritized: bool, alpha: float):
        print("Initializing experience replay with params:")
        print(f"\tbuffer_size:\t{buffer_size}")
        print(f"\tprioritized:\t{prioritized}")
        print(f"\talpha:\t{alpha}")

        self.buffer = []
        self.max_buffer_size = buffer_size
        self.next_index = 0
        self.count = 0

        self.prioritized = prioritized
        self.alpha = alpha

    def put(self, transition: Transition):
        """
        Adds a transition to the buffer. If the buffer is full, the oldest transition in the buffer is overwritten.
        :param transition:
        :return:
        """
        if len(self.buffer) < self.max_buffer_size:
            # Lazy initialization of buffer.
            self.buffer.append(transition)
            self.count += 1
        else:
            # Buffer has been completely initialized, so existing entries are overwritten.
            self.buffer[self.next_index] = transition

        self.increment_index()

    def increment_index(self):
        """
        Increments the index to indicate the next position of the buffer to be written to.
        :return:
        """
        self.next_index = (self.next_index + 1) % self.max_buffer_size

    def sample(self, max_transition_count: int):
        if self.prioritized:
            return self.sample_prioritized(max_transition_count)
        else:
            return self.sample_uniform(max_transition_count)

    def sample_uniform(self, max_transition_count: int):
        """
        Randomly sample up to `max_transition_count` transitions uniformly from the buffer. If the buffer does not
        contain as many transitions as specified then all transition in the buffer are sampled.
        :param max_transition_count: Maximum number of transitions to sampled.
        :return: List of sampled transitions.
        """
        sample_size = min(max_transition_count, len(self.buffer))
        indices = random.sample(range(len(self.buffer)), sample_size)

        return [indices, [self.buffer[i] for i in indices]]

    def sample_prioritized(self, max_transition_count: int):
        """
        Sample up to `max_transition_count` transitions using a distribution that prioritizes transitions that have
        resulted in more loss, from the buffer. If the buffer does not contain as many transitions as specified then all
        transitions in the buffer are sampled.
        :param max_transition_count: Maximum number of transitions to sampled.
        :return: List of sampled transitions.
        """
        sample_size = min(max_transition_count, len(self.buffer))
        indices = random.choices(range(len(self.buffer)),
                                 k=sample_size,
                                 weights=[transition.priority ** self.alpha for transition in self.buffer])

        return [indices, [self.buffer[i] for i in indices]]

    def __str__(self) -> str:
        return f"[ {', '.join(str(x) for x in self.buffer)} ]"

    def prioritize(self, sampled_indices, loss):
        for index in sampled_indices:
            self.buffer[index].priority = loss
