from typing import List

import numpy as np
import torch
from torch import tensor

from src.core.util.constants import Constants


class StateMatrix(object):
    """
    Class used for arbitrary state that can be fed into neural net e.g. state for 4 image matrices.
    """

    def __init__(self, data: List[List[List[int]]]):
        if data is None:
            self.matrix = None
        else:
            self.matrix = torch.tensor(data, dtype=torch.float, device=Constants.device)
