class TwoDimensionalBoard:
    """
    Class that largely mimics the TurnBased2DBoard class in the Ingenious Framework. The purpose of this class is to
    have a means to store arbitrary boards and be able to ensure different states are stored separately in a dict
    structure such as in q_table.
    """
    def __init__(self, raw_state: dict):
        self.board = tuple(map(tuple, raw_state['board']))
        self.height = raw_state['height']
        self.width = raw_state['width']
        self.next_move_player_id = raw_state['nextMovePlayerID']
        self.num_players = raw_state['numPlayers']

    def __hash__(self) -> int:
        return hash((self.board, self.height, self.width, self.next_move_player_id, self.num_players))

    def __eq__(self, other):
        if not isinstance(other, TwoDimensionalBoard):
            return False

        return \
            self.board == other.board \
            and self.height == other.height \
            and self.width == other.width \
            and self.next_move_player_id == other.next_move_player_id \
            and self.num_players == other.num_players
