from torch import nn


class GridWorldNN(nn.Module):
    def __init__(self, num_channels: int, height: int, width: int, num_features_out: int):
        super(GridWorldNN, self).__init__()

        kernel_sizes = [3, 2]
        stride = [1, 1]
        channels = [16, 32]

        self.conv1 = nn.Conv2d(num_channels, channels[0], kernel_size=kernel_sizes[0], stride=stride[0])
        self.conv2 = nn.Conv2d(channels[0], channels[1], kernel_size=kernel_sizes[1], stride=stride[1])
        self.relu = nn.ReLU()

        linear_input_size = self.calc_features_in(
            height,
            width,
            channels[1],
            2,
            kernel_sizes,
            stride
        )

        self.fc1 = nn.Linear(linear_input_size, 256)
        self.fc2 = nn.Linear(256, num_features_out)

    def calc_features_in(self, height: int, width: int, channels: int, num_conv_layers: int, kernel_sizes: [int],
                         stride: [int]):
        conv_width = width
        conv_height = height

        for i in range(num_conv_layers):
            conv_width = ((conv_width - (kernel_sizes[i] - 1)) - 1) // stride[i] + 1
            conv_height = ((conv_height - (kernel_sizes[i] - 1)) - 1) // stride[i] + 1

        return conv_width * conv_height * channels

    def forward(self, x):
        x = self.conv1(x)
        x = self.relu(x)

        x = self.conv2(x)
        x = self.relu(x)

        x = x.view(x.size(0), -1)
        x = self.fc1(x)
        x = self.relu(x)
        x = self.fc2(x)

        return x
