from torch import nn


class MnkNN(nn.Module):
    def __init__(self, num_channels: int, height: int, width: int, num_features_out: int):
        super(MnkNN, self).__init__()

        features_in = num_channels * height * width

        self.linear1 = nn.Linear(features_in, 27)
        self.linear2 = nn.Linear(27, 27)
        self.linear3 = nn.Linear(27, 27)
        self.linear4 = nn.Linear(27, num_features_out)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = x.view(x.size()[0], -1)
        x = self.linear1(x)
        x = self.relu(x)

        x = self.linear2(x)
        x = self.relu(x)

        x = self.linear3(x)
        x = self.relu(x)

        x = self.linear4(x)
        return x
