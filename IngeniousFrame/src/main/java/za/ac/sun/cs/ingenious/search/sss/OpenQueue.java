package za.ac.sun.cs.ingenious.search.sss;
import za.ac.sun.cs.ingenious.core.util.nodes.trees.BinaryTreeNode;
import za.ac.sun.cs.ingenious.core.util.nodes.trees.TreeNode;

/**
 * A priority queue that stores descriptors.
 * Descriptors are sorted by decreasing order of their merit values.
 * The {@code poll} operation always returns the descriptor with the heighest merit score.
 * 
 * <p> Internally this class uses a bucket priority queue implemented as a binary search tree (BST).
 * This implementation is designed to be optimised around the {@code purgeAllDescendentsOf} operation.
 * 
 * @author Nicholas Robinson
 */
class OpenQueue {
    
    Bucket root, largest;

    /**
     * Put a descriptor in the open queue.
     * Places the descriptor in the bucket corresponding to its merit score, or create the bucket if it does not yet exist
     * @param descriptor the descriptor to be put in open.
     */
    void put(Descriptor descriptor) {
        if (root == null) {
            root = new Bucket(descriptor.merit, null);
            root.push(descriptor);
            largest = root;
        }
        else if (descriptor.merit == largest.merit) {
            largest.push(descriptor);
        }
        else {
            if (largest.isEmpty()) {
                discardLargestBucket();
                put(descriptor);
                return;
            }
            else if (descriptor.merit > largest.merit) {
                largest.setRight(new Bucket(descriptor.merit, largest));
                largest = largest.getRight();
                largest.push(descriptor);
            }
            else {
                Bucket cursor = root;
                while (cursor.merit != descriptor.merit) {
                    if (descriptor.merit > cursor.merit) {
                        if (cursor.getRight() == null) {
                            cursor.setRight(new Bucket(descriptor.merit, cursor));
                        }
                        cursor = cursor.getRight();
                    }
                    else {
                        if (cursor.getLeft() == null) {
                            cursor.setLeft(new Bucket(descriptor.merit, cursor));
                        }
                        cursor = cursor.getLeft();
                    }
                }
                cursor.push(descriptor);
            }
        }

        descriptor.node.descriptor = descriptor;
        descriptor.node.active = true;
    }

    /**
     * Remove a given descriptor from its bucket,
     * the bucket will be removed from the BST if it becomes empty after this operation.
     * 
     * @param descriptor
     */
    void remove(Descriptor descriptor) {
        descriptor.next.setPrevious(descriptor.previous);
        if (descriptor.previous != null) {
            descriptor.previous.next = descriptor.next;
        }

        descriptor.node.descriptor = null;
    }

    /**
     * Return the descriptor in Open with the largest merit score
     * 
     * @return the descriptor with the largest merit score
     */
    Descriptor poll() {
        return largest.pop();
    }

    /**
     * Remove from Open all descriptors that correspond to nodes that are descendents of the given node
     * 
     * @param node node that will have its decendents purged
     */
    void purgeAllDescendentsOf(SSS.SearchNode<?> node) {
        
        for (int c = 0; c <= node.latestChild; c++) {
            SSS.SearchNode<?> child = node.getChild(c);
            if (child.active) {
                purgeDescendents(child);
            }
        } 
    }

    /**
     * Recursively search the descendents of the given node in the search tree, 
     * calling the remove function on each descriptor that correspond to a node that is a descendent of the given node
     * 
     * @param node node that will have its decendents purged
     */
    private void purgeDescendents(SSS.SearchNode<?> node) {
        
        for (int c = 0; c <= node.latestChild; c++) {
            SSS.SearchNode<?> child = node.getChild(c);
            if (child.active) {
                purgeDescendents(child);
            }
        }

        if (node.descriptor != null) {
            remove(node.descriptor);
        }
        node.active = false;
    }

    /**
     * remove a given bucket from the BST
     * @param Bucket the bucket to be removed
     */
    private void discardBucket(Bucket bucket) {
        Bucket nextOfKin = null;

        if (bucket.getLeft() == null) {
            nextOfKin = bucket.getRight();
        }
        else if (bucket.getRight() == null) {
            nextOfKin = bucket.getLeft();
        }
        else {
            nextOfKin = bucket.getLeft();

            // append right subtree to rightmost leaf of left subtree
            Bucket tail = nextOfKin;
            while (tail.getRight() != null) {
                tail = tail.getRight();
            }
            tail.setRight(bucket.getRight());
            bucket.getParent().setParent(tail);


            // rotate left once
            Bucket r = nextOfKin.getRight();
            nextOfKin.setRight(r.getLeft());
            if (r.getLeft() != null) {
                r.getLeft().setParent(nextOfKin);
            }

            r.setLeft(nextOfKin);
            nextOfKin.setParent(r);

            nextOfKin = r;
        }

        if (nextOfKin != null) {
            nextOfKin.setParent(bucket.getParent());
        }

        if (bucket == root) {
            root = nextOfKin;
        }
        else if (bucket.getParent().getLeft() == bucket) {
            bucket.getParent().setLeft(nextOfKin);
        }
        else {
            bucket.getParent().setRight(nextOfKin);
        }
    }

    /**
     * Remove from the BST the bucket referenced by {@code largest} and update {@code largest}
     */
    private void discardLargestBucket() {
        if (largest == root) {
            if (largest.getLeft() == null) {
                root = null;
                largest = null;
            }
            else {
                root = largest.getLeft();
                root.setParent(null);
                largest = root;

                while (largest.getRight() != null) {
                    largest = largest.getRight();
                }
            }
        }
        else {
            largest.getParent().setRight(largest.getLeft());

            if (largest.getLeft() != null) {
                largest.getLeft().setParent(largest.getParent());
                largest = largest.getLeft();

                while (largest.getRight() != null) {
                    largest = largest.getRight();
                }
            }
            else {
                largest = largest.getParent();
            }
        }
    }

    /**
     * A linked list of descriptors that acts as a stack, contains all descriptors of the same merit score.
     * Buckets also act as the nodes in the BST.
     */
    class Bucket extends BinaryTreeNode<Bucket> implements BucketStackRelation, TreeNode.Bidirectionality<Bucket> {
        Descriptor top;

        int merit;
        Bucket parent; 

        Bucket(int merit, Bucket parent) {
            this.merit = merit;
            this.parent = parent;
        }

        boolean isEmpty() {
            return top == null;
        }

        /**
         * Append Descriptor to top of stack
         * @param descriptor Descriptor to be added
         */
        void push(Descriptor descriptor) {
            descriptor.next = this;
            descriptor.previous = top;

            if (top != null) {
                top.next = descriptor;
            }

            top = descriptor;
        }

        /**
         * Extract Descriptor from top of stack
         * @return Descriptor that was at the top of the stack
         */
        Descriptor pop() {
            Descriptor p = top;
            remove(p);
            return p;
        }

        /**
         * Set reference to descriptor at top of stack.
         * Acts as automatic detection for when this bucket becomes empty.
         */
        @Override
        public void setPrevious(Descriptor previous) {
            top = previous;

            if (previous == null && this != largest) { // bucket wont be removed if it is the largest
                discardBucket(this);
            }
        }

        @Override
        public Bucket clone() {
            throw new UnsupportedOperationException();
        }

		@Override
		public Bucket getParent() {
			return parent;
		}

		@Override
		public void setParent(Bucket parent) {
			this.parent = parent;
		}
    }

    /**
     * Descriptors in buckets have references to their "previous" and "next" neighbours.
     * The Bucket acts as the first node in the linked list, to illustrate:
     * 
     * <p> [Bucket] <--> (Descriptor) <--> (Descriptor) <--> (Descriptor) <--> ...
     * 
     * <p> Both descriptors and buckets implement this interface. When a node is unlinked from the list, 
     * its next and previous neighbours need to reference each other.
     * The {@code setPrevious} method overridden by the bucket acts as a trigger to detect whether it is empty.
     */
    interface BucketStackRelation {
        void setPrevious(Descriptor previous);
    }
}