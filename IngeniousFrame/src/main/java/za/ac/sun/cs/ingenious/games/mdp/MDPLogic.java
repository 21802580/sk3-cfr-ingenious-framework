package za.ac.sun.cs.ingenious.games.mdp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents the dynamics of an MDP. An MDPReferee will contain an instance of this class holding the true dynamics of
 * the environment. The MDPEngine will have the option of maintaining an instance of this class that may help with
 * background planning based on the observed behaviour of the game.
 *
 * @author Steffen Jacobs
 */
public class MDPLogic<T> implements GameLogic<MDPState<T>> {
    public static final int DEFAULT_PLAYER_TO_ACT = 0;
    private Random rng;

    private T initialGameState;
    private Set<MDPState<T>> terminalStates;
    private Set<MDPState<T>> states;
    private Map<MDPState<T>, Set<Action>> validActions;
    private Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p;

    public MDPLogic(
            T initialGameState,
            Set<MDPState<T>> terminalStates,
            Set<MDPState<T>> states,
            Map<MDPState<T>, Set<Action>> validActions,
            Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p) {
        rng = new Random();
        this.initialGameState = initialGameState;
        this.terminalStates = terminalStates;
        this.states = states;
        this.validActions = validActions;
        this.p = p;
    }

    public MDPLogic<T> withRandom(Random random) {
        rng = random;
        return this;
    }

    public static MDPLogic<Long> fromSetting(MDPSetting mdpSetting) {
        return new MDPLogic<>(
                createInitialGameStateFromSetting(mdpSetting),
                createTerminalStatesFromSetting(mdpSetting),
                createStatesFromSetting(mdpSetting),
                createValidActionsFromSetting(mdpSetting),
                createPFromSetting(mdpSetting)
        );
    }

    /**
     * Creates the logic of an MDP from an existing game in the framework, using that game's logic. The current
     * implementation involves a brute force DFS search of the state space and assumes that the environment is
     * non-stochastic.
     *
     * @param initialGameState Starting state of the game being mapped.
     * @param gameLogic Logic of game being mapped.
     * @param <T> Game state parameterized type.
     * @return MDPLogic object that contains the full behaviour of the MDP.
     */
    public static <T extends GameState> MDPLogic<T> fromGameLogic(T initialGameState, GameLogic<T> gameLogic) {
        Set<MDPState<T>> gameStates = new HashSet<>();
        Set<MDPState<T>> terminalStates = new HashSet<>();
        Map<MDPState<T>, Set<Action>> validActions = new HashMap<>();
        Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p = new HashMap<>();

        MDPState<T> initialMdpState = new MDPState<>(initialGameState);
        exploreGameState(gameStates, terminalStates, validActions, p, initialMdpState, gameLogic);

        return new MDPLogic<T>(
                initialGameState,
                terminalStates,
                gameStates,
                validActions,
                p
        );
    }

    private static <T extends GameState> void exploreGameState(
            Set<MDPState<T>> gameStates,
            Set<MDPState<T>> terminalStates,
            Map<MDPState<T>, Set<Action>> validActions,
            Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p,
            MDPState<T> mdpState,
            GameLogic<T> gameLogic) {
        // If state has already been explored, return.
        if (gameStates.contains(mdpState))
            return;

        // Add game state.
        gameStates.add(mdpState);

        // Perform lazy initialization for this state if necessary.
        if (!validActions.containsKey(mdpState))
            validActions.put(mdpState, new HashSet<>());

        // If state is terminal, add as a terminal state and return;
        if (gameLogic.isTerminal(mdpState.getGameState())) {
            terminalStates.add(mdpState);
            return;
        }

        List<Action> actions = gameLogic.generateActions(mdpState.getGameState(), DEFAULT_PLAYER_TO_ACT);

        // Perform lazy initialization for each of the states actions.
        p.put(mdpState, new HashMap<>());
        validActions.put(mdpState, new HashSet<>(actions));
        for (Action action : actions) {
            if (!p.get(mdpState).containsKey(action)) {
                p.get(mdpState).put(action, new HashMap<>());
            }
        }

        for (Action action : actions) {
            T newState = (T)mdpState.getGameState().deepCopy();
            boolean actionSuccess = gameLogic.makeMove(newState, action);

            if (actionSuccess) {
                MDPState<T> newMdpState = new MDPState<>(mdpState.getInitialGameState(), newState);
                p.get(mdpState).get(action).put(newMdpState, 1.0);
                exploreGameState(gameStates, terminalStates, validActions, p, newMdpState, gameLogic);
            } else {
                Log.warn("MDPLogic.exploreGameState: Action was not applied successfully. This could indicate that GameLogic.generateActions is not in line with GameLogic.makeMove.");
            }
        }
    }

    /**
     * Creates the initial state from setting.
     *
     * @param mdpSetting Settings defining the MDP we are creating.
     */
    private static Long createInitialGameStateFromSetting(MDPSetting mdpSetting) {
        return mdpSetting.getInitialState();
    }

    /**
     * Creates the set of states.
     *
     * @param mdpSetting Settings defining the MDP we are creating.
     */
    private static Set<MDPState<Long>> createStatesFromSetting(MDPSetting mdpSetting) {
        Set<MDPState<Long>> states = new HashSet<>();

        for (long i = 0; i < mdpSetting.getStates(); i++) {
            states.add(new MDPState<>(i));
        }

        return states;
    }

    /**
     * Creates the set of terminal states.
     *
     * @param mdpSetting Settings defining the MDP we are creating.
     */
    private static Set<MDPState<Long>> createTerminalStatesFromSetting(MDPSetting mdpSetting) {
        Set<MDPState<Long>> terminalStates = new HashSet<>();

        for (long terminalState : mdpSetting.getTerminalStates()) {
            terminalStates.add(new MDPState<>(terminalState));
        }

        return terminalStates;
    }

    /**
     * Creates Map containing the set of allowed actions for each state.
     *
     * @param mdpSetting Settings defining the MDP we are creating.
     */
    private static Map<MDPState<Long>, Set<Action>> createValidActionsFromSetting(MDPSetting mdpSetting) {
        Map<MDPState<Long>, Set<Action>> validActions = new HashMap<>();

        long i = 0;
        for (Collection<Integer> actionValuesForState : mdpSetting.getValidActions()) {
            Set<Action> actionsSet = actionValuesForState.stream()
                    .map(x -> new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, x))
                    .collect(Collectors.toSet());

            validActions.put(new MDPState<>(i++), actionsSet);
        }

        return validActions;
    }

    /**
     * Builds a lookup for P(S, A, S') for the provided MDP settings. Currently this essentially this converts from the
     * three dimensional list representation to a nested key-value pair representation.
     *
     * @param mdpSetting Settings defining the MDP we are creating.
     */
    private static Map<MDPState<Long>, Map<Action, Map<MDPState<Long>, Double>>> createPFromSetting(MDPSetting mdpSetting) {
        Map<MDPState<Long>, Map<Action, Map<MDPState<Long>, Double>>> p = new HashMap<>();

        // Use the dynamics, where dynamics = P(S, A, S') is the probability of S' given state-action pair (S, A).
        Collection<Collection<Collection<Double>>> dynamics = mdpSetting.getDynamics();

        long i = 0;

        // For each state, look at P(S = i, A, S').
        for (Collection<Collection<Double>> dynamicsActionNewState : dynamics) {
            Map<Action, Map<MDPState<Long>, Double>> dynamicsActionNewStateMap = new HashMap<>();

            int j = 0;
            // For each action, look at P(S = i, A = j, S')
            for (Collection<Double> dynamicsNewState : dynamicsActionNewState) {
                Map<MDPState<Long>, Double> dynamicsNewStateMap = new HashMap<>();

                long k = 0;
                /*
                 * For each resulting state, add the probability P(S = i, A = j, S' = k) to the map corresponding to the
                 * preceding state, action, new state being i, j, k.
                 */
                for (double probabilityOfState : dynamicsNewState) {
                    dynamicsNewStateMap.put(new MDPState<>(k++), probabilityOfState);
                }

                dynamicsActionNewStateMap.put(new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, j++), dynamicsNewStateMap);
            }

            p.put(new MDPState<>(i++), dynamicsActionNewStateMap);
        }

        return p;
    }

    public void setInitialGameState(T initialGameState) {
        this.initialGameState = initialGameState;
    }

    public Set<MDPState<T>> getTerminalStates() {
        return terminalStates;
    }

    public void setTerminalStates(Set<MDPState<T>> terminalStates) {
        this.terminalStates = terminalStates;
    }

    public Set<MDPState<T>> getStates() {
        return states;
    }

    public void setStates(Set<MDPState<T>> states) {
        this.states = states;
    }

    public Map<MDPState<T>, Set<Action>> getValidActions() {
        return validActions;
    }

    public void setValidActions(Map<MDPState<T>, Set<Action>> validActions) {
        this.validActions = validActions;
    }

    public Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> getP() {
        return p;
    }

    public void setP(Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p) {
        this.p = p;
    }

    @Override
    public boolean validMove(MDPState<T> fromState, Move move) {
        if (move instanceof Action) {
            Set<Action> validActions = this.validActions.get(fromState);

            return validActions != null && validActions.contains(move);
        }

        return false;
    }

    public T getInitialGameState() {
        return initialGameState;
    }

    @Override
    public boolean makeMove(MDPState<T> fromState, Move move) {
        if (move instanceof DiscreteAction) {
            int stateNumber = -1;
            Action action = (Action) move;
            Map<MDPState<T>, Double> distribution = p.get(fromState).get(action);

            MDPState<T> newState = null;

            try {
                newState = ClassUtils.chooseFromDistribution(distribution, rng);
            } catch (IncorrectlyNormalizedDistributionException e) {
                e.printStackTrace();
            }

            if (newState != null) {
                fromState.nextGameState(newState.getGameState());
                return true;
            } else {
                Log.warn("MDPLogic.makeMove: No new state was chosen for state-action pair. MDP is likely misconfigured.");
            }
        }

        return false;
    }

    @Override
    public void undoMove(MDPState<T> fromState, Move move) {
        Log.warn("MDPLogic.undoMove: Invoked but no implementation for this exists.");
    }

    @Override
    public List<Action> generateActions(MDPState<T> fromState, int forPlayerID) {
        Set<Action> actions = validActions.get(fromState);

        if (actions != null) {
            return new ArrayList<>(actions);
        }

        return null;
    }

    @Override
    public boolean isTerminal(MDPState<T> state) {
        return terminalStates.contains(state);
    }

    @Override
    public Set<Integer> getCurrentPlayersToAct(MDPState<T> fromState) {
        Set<Integer> playersToAct = new HashSet<>();
        playersToAct.add(DEFAULT_PLAYER_TO_ACT);

        return playersToAct;
    }
}
