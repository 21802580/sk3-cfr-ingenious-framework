package za.ac.sun.cs.ingenious.games.mnk.engines.TreeEngine;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationContextual;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgr;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationMast;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.MctsCMCNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationContextual.newBackpropagationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgr.newBackpropagationLgr;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationMast.newBackpropagationMast;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave.newBackpropagationRave;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationSimple.newBackpropagationSimple;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionPW.newTreeSelectionProgressiveWidening;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionRave.newTreeSelectionRave;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctFPU.newTreeSelectionFPU;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctTunedFPU.newTreeSelectionFPUTuned;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationContextual.newSimulationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrfFull.newSimulationLgrFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastFullTree.newSimulationMastFullTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastTreeOnly.newSimulationMastTreeOnly;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

public class MNKMCTSTreeGenericEngine extends MNKMctsEngine {

    MctsTree<MNKState, MctsNodeTreeParallel<MNKState>> mcts;
    ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>>> backpropagationEnhancements = new ArrayList<>();
    Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses = new Hashtable<>();
    String playerEnhancementConfig;

    int TURN_LENGTH; // in milliseconds
    int THREAD_COUNT;

//    MctsNodeTreeParallel<MNKState> root;
//    MctsNodeTreeParallel<MNKState> previousSubtreeRoot;

    /**
     * @param toServer An established connection to the GameServer
     */
    public MNKMCTSTreeGenericEngine(EngineToServerConnection toServer, String enhancementConfig, int threadCount, int turnLength) throws IOException, MissingSettingException, IncorrectSettingTypeException {
        super(toServer);
        THREAD_COUNT = threadCount;
        TURN_LENGTH = turnLength;

        int length = enhancementConfig.length();
        playerEnhancementConfig = enhancementConfig.substring(36,length-5);

        MatchSetting m = new MatchSetting(enhancementConfig);

        String SelectionEnhancement = m.getSettingAsString("Selection");
        String ExpansionEnhancement = m.getSettingAsString("Expansion");
        String SimulationEnhancement = m.getSettingAsString("Simulation");
        String BackpropagationEnhancements = m.getSettingAsString("Backpropagation");
        String[] BackpropagationEnhancement = BackpropagationEnhancements.split(",");

        // Selection strategy object
        TreeSelection<MctsNodeTreeParallel<MNKState>> selection = getSelectionClass(SelectionEnhancement);

        // Final Selection strategy object
        TreeSelectionFinal<MctsNodeTreeParallel<MNKState>> finalSelection = newFinalSelectionUct(logic);

        // Expansion strategy object
        ExpansionThreadSafe<MctsNodeTreeParallel<MNKState>,MctsNodeTreeParallel<MNKState>> expansion = getExpansionClass(ExpansionEnhancement);

        // Simulation strategy object
        boolean recordMoves = false;
        if (SelectionEnhancement.equals("Rave")) {
            recordMoves = true;
        }
        SimulationThreadSafe<MNKState> simulation = getSimulationClass(SimulationEnhancement, recordMoves);

        // Backpropagation strategy objects
        int size = BackpropagationEnhancement.length;
        for (int i = 0; i < size; i++) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagationEnhancement = getBackpropagationClass(BackpropagationEnhancement[i]);
            if (backpropagationEnhancement != null) {
                backpropagationEnhancements.add(backpropagationEnhancement);
            }
        }

        this.mcts = new MctsTree<>(selection, expansion, simulation, backpropagationEnhancements, finalSelection, logic, THREAD_COUNT, playerID);
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {

    }

    @Override
    public String engineName() {
        return "MNKMCTSTreeGenericEngine";
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        MNKMctsFinalEvaluator eval = new MNKMctsFinalEvaluator();
        Log.info("Game has terminated");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            try(FileWriter fw = new FileWriter("ResultsMNK.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                if (i == playerID) {
                    String str = playerEnhancementConfig + ": " + score[i] + "_threads:" + THREAD_COUNT;
                    out.println(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.info("Final state:");
        currentState.printPretty();
        toServer.closeConnection();
        System.exit(0);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementExtensionClasses = new Hashtable<>();
        for (MctsNodeExtensionParallelInterface newExtension: enhancementExtensionClasses.values()) {
            try {
                newEnhancementExtensionClasses.put(newExtension.getID(), newExtension.getClass().newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

//        if (root == null) {
        MctsNodeTreeParallel root = new MctsNodeTreeParallel<>(currentState, null, null, new ArrayList<>(), logic, newEnhancementExtensionClasses, playerID);
//        } else {
//            root = previousSubtreeRoot;
//        }

        MctsNodeTreeParallel finalChoice = mcts.doSearch(root, TURN_LENGTH, null);
//        previousSubtreeRoot = finalChoice;
        Action action = finalChoice.getPrevAction();
        if (action == null) {
            return new PlayActionMessage(new ForfeitAction((byte) playerID));
        }

        return new PlayActionMessage(action);
    }

    public TreeSelection<MctsNodeTreeParallel<MNKState>> getSelectionClass(String selectionClass) {
        if (selectionClass.equals("Uct")) {
            return newTreeSelectionUct(logic, 0.7, playerID);
        } else if (selectionClass.equals("UctFPU")) {
            return newTreeSelectionFPU(logic, 1000, 100, playerID);
        } else if (selectionClass.equals("UctTunedFPU")) {
            return newTreeSelectionFPUTuned(logic,1000, 100, playerID);
        } else if (selectionClass.equals("Rave")) {
            return newTreeSelectionRave(logic, 100, 50, playerID);
        } else if (selectionClass.equals("PW")) {
            MctsPWNodeExtensionParallel parallelPWNodeExtension = new MctsPWNodeExtensionParallel();
            enhancementExtensionClasses.put("PW", parallelPWNodeExtension);
            return newTreeSelectionProgressiveWidening(logic);
        } else {
            System.out.println("\nInvalid selections strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    public ExpansionThreadSafe<MctsNodeTreeParallel<MNKState>,MctsNodeTreeParallel<MNKState>> getExpansionClass(String expansionClass) {
        if (expansionClass.equals("Single")) {
            return newExpansionSingle(logic);
        } else {
            System.out.println("\nInvalid expansion strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    public SimulationThreadSafe<MNKState> getSimulationClass(String simulationClass, boolean recordMoves) {
        if (simulationClass.equals("Random")) {
            return newSimulationRandom(logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(), recordMoves);
        } else if (simulationClass.equals("MastTreeOnly")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            MastTable visitedMovesTable =((BackpropagationMast<MctsNodeTreeParallel<MNKState>>) backpropagationMast).getQMastTable(67.7468);
            return newSimulationMastTreeOnly(logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(), visitedMovesTable, 67.7468, recordMoves);
        } else if (simulationClass.equals("MastFullTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            MastTable visitedMovesTable =((BackpropagationMast<MctsNodeTreeParallel<MNKState>>) backpropagationMast).getQMastTable(67.7468);
            return newSimulationMastFullTree(logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(), visitedMovesTable, 67.7468, recordMoves);
        } else if (simulationClass.equals("Contextual")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagationContextual = newBackpropagationContextual();
            CMCTable visitedMovesTable =((BackpropagationContextual<MctsNodeTreeParallel<MNKState>>) backpropagationContextual).getCMCTable();
            return newSimulationContextual(logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(), visitedMovesTable, 0.323451, 0.271295, recordMoves);
        } else if (simulationClass.equals("Lgr")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> backpropagation = newBackpropagationLgr();
            LGRTable visitedMovesTable =((BackpropagationLgr<MctsNodeTreeParallel<MNKState>>) backpropagation).getLGRTable();
            return newSimulationLgrFull(logic, new MNKMctsFinalEvaluator(), new PerfectInformationActionSensor<MNKState>(), visitedMovesTable, recordMoves);
        } else {
            System.out.println("\nInvalid simulation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    public BackpropagationThreadSafe<MctsNodeTreeParallel<MNKState>> getBackpropagationClass(String backpropagationClass) {
        if (backpropagationClass.equals("Average")) {
            return newBackpropagationAverage();
        } else if (backpropagationClass.equals("Simple")) {
            return newBackpropagationSimple(playerID);
        } else if (backpropagationClass.equals("Mast")) {
            return null;
        } else if (backpropagationClass.equals("Rave")) {
            MctsRaveNodeExtensionParallel parallelRaveNodeExtension = new MctsRaveNodeExtensionParallel();
            enhancementExtensionClasses.put("Rave", parallelRaveNodeExtension);
            return newBackpropagationRave();
        } else if (backpropagationClass.equals("Contextual")) {
            MctsCMCNodeExtensionParallel parallelCMCNodeExtension = new MctsCMCNodeExtensionParallel();
            enhancementExtensionClasses.put("Contextual", parallelCMCNodeExtension);
            return null;
        } else {
            System.out.println("\nInvalid backpropagation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }
}
