package za.ac.sun.cs.ingenious.core;

import java.util.Observable;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.referee.GeneralReferee;

/**
 * Most general representation of a referee. Does not make any assumptions about how a
 * game is played. For that, see {@link GeneralReferee}.
 * 
 * A referee is the entity that plays the game with the players (=engines). It is run by
 * the GameServer and supports receiving {@link PlayActionMessage}s from, as well as
 * sending {@link PlayedMoveMessage}s to its connected clients. It also supports sending
 * {@link InitGameMessage} and {@link GameTerminatedMessage} at the start and end of a
 * game.
 */
public abstract class Referee extends Observable implements Runnable {

	protected final MatchSetting matchSettings;
	protected PlayerRepresentation[] players;

	/**
	 * Creates a new referee.
     * 
     * @param match Match settings for this instance of the game.
     * @param players Array of participants in the game - may not be null.
     * @throws IllegalArgumentException if players is null
	 */
	protected Referee(MatchSetting match, PlayerRepresentation[] players){
		this.matchSettings = match;
        if (players != null) {
    		this.players = players;
        } else {
            throw new IllegalArgumentException("players argument may not be null");
        }
	}

	protected void sendInitGameMessage(InitGameMessage a, int targetID ){
		players[targetID].initGame(a);
	}

	protected void sendGameTerminatedMessage(GameTerminatedMessage a, int targetID ){
		players[targetID].terminateGame(a);
	}

	protected void sendMatchResetMessage(MatchResetMessage a, int targetID) {
		players[targetID].resetMatch(a);
	}

	public MatchSetting getMatchSetting(){
		return this.matchSettings;
	}

	public PlayerRepresentation[] getEngineConns() {
		return players;
	}
	
    /**
     * @param id ID of some engine that disconnected or experiences severe communication problems. Index into the players array.
     */
    public abstract void engineDisconnected(int id);

	/**
	 * Implement game control logic here.
	 */
	@Override
	public abstract void run();

}
