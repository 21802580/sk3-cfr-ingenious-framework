package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An Action indicating a player did nothing that turn.
 *
 * @author Rudolf Stander
 */
public class IdleAction implements Action {

	/* For serialization */
	private static final long serialVersionUID = 1L;
	/* The default player ID */
	private final int DEFAULT_ID = -1;
	/* The player who was idle, with 0 representing black and
	 * 1 representing white. */
	private int player;

	/**
	 * Constructs a new IdleAction object.
	 *
	 * @param player	the player who made the move.
	 */
	public IdleAction(int player) {
		this.player = player;
	}

	/**
	 * Constructs a new IdleAction object with a default playerID.
	 */
	public IdleAction() {
		this.player = DEFAULT_ID;
	}

	/**
	 * Get the player who made the move.
	 *
	 * @return	the player who placed the disk with 0 representing black and
	 *			1 representing white
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	/* Copied from Bomberman IdleAction.
	 *
	 * This is NOT how equals should work. This is more like a type check.
	 *
	 * FIXME: Rename this or give a valid reason/description for calling this
	 * "equals".
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		return this.getClass() == obj.getClass();
	}

	/* Copied from Bomberman IdleAction.
	 *
	 * NOT how hashcode should look...
	 *
	 * FIXME: Create an actual hashcode.
	 */
	@Override
	public int hashCode() {
		return Integer.MAX_VALUE;
	}

	public String toString() {
		return "Action: IdleAction, Player: "+player;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
