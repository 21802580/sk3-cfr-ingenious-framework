package za.ac.sun.cs.ingenious.core.exception;

/**
 * Created by Chris Coetzee on 2016/07/26.
 */
public class InvalidJarException extends Exception {
	private static final long serialVersionUID = 1L;

    public InvalidJarException(String msg) {
        super(msg);
    }
}
