package za.ac.sun.cs.ingenious.core.util.reward;

import za.ac.sun.cs.ingenious.core.Reward;

/**
 * A single double value that serves as a reward for reinforcement learning
 * players.
 *
 * @author Elan van Biljon
 */
public class ScalarReward implements Reward {
	private final double reward;

	public ScalarReward(double r) {
		reward = r;
	}

	public double getReward() {
		return reward;
	}

	public String toString() {
		return "" + reward;
	}

	@Override
	public Reward deepCopy() {
		return Reward.cloner.deepClone(this);
	}
}
