package za.ac.sun.cs.ingenious.core.commandline.main;

import java.util.Arrays;

/**
 * Main entry point of the framework. The client, gamecreator and gameserver
 * classes can be started by supplying "server", "client" or "create" as the
 * first commandline argument, followed by the arguments to the respective
 * classes.
 * 
 * Created by Chris Coetzee on 2016/07/28.
 */
public class Driver {

    public static void main(String args[]) {

        MainArguments arguments = new MainArguments();
        arguments.processArguments(args);

        String[] remainingArgs = Arrays.copyOfRange(args, 1, args.length);

        switch (arguments.getCommand()) {

            case "server": {
                za.ac.sun.cs.ingenious.core.commandline.gameserver.Driver.main(remainingArgs);
                break;
            }
            case "client": {
                za.ac.sun.cs.ingenious.core.commandline.client.Driver.main(remainingArgs);
                break;
            }
            case "create": {
                za.ac.sun.cs.ingenious.core.commandline.gamecreator.Driver.main(remainingArgs);
                break;
            }
        }
    }
}
