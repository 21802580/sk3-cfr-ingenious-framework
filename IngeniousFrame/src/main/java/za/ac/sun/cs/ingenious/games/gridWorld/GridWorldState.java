package za.ac.sun.cs.ingenious.games.gridWorld;

import com.esotericsoftware.minlog.Log;
import com.google.common.collect.ImmutableMap;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldLevelParser;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Class representing the game state for the GridWorld game.
 *
 * @author Steffen Jacobs
 */
public class GridWorldState extends TurnBased2DBoard {
    public static final int DEFAULT_FIRST_PLAYER = 0;
    public static final int DEFAULT_NUM_PLAYERS = 1;

    public static final int CELL_UNREACHABLE = -2;
    public static final int CELL_REACHABLE = -1;

    /**
     * Default constructor in order to allow instantiating GridWorld's state, logic and eval from single level file
     * read later.
     */
    public GridWorldState() {
        super(1, 1, DEFAULT_FIRST_PLAYER, DEFAULT_NUM_PLAYERS);
    }

    public GridWorldState(int height, int width) {
        super(height, width, DEFAULT_FIRST_PLAYER, DEFAULT_NUM_PLAYERS);
    }

    public GridWorldState(int height, int width, int[][] board) {
        super(height, width, DEFAULT_FIRST_PLAYER, DEFAULT_NUM_PLAYERS);

        this.board = board;
    }

    public GridWorldState(GridWorldLevelParser parser) {
        super(parser.getHeight(), parser.getWidth(), DEFAULT_FIRST_PLAYER, DEFAULT_NUM_PLAYERS);

        this.board = parser.getLevel();
    }

    public GridWorldState(GridWorldState toCopy) {
        super(toCopy);
    }

    /**
     * Searches the board to find the coordinates of the cell occupied by the specified player ID.
     *
     * @param playerID Player ID associated with the cell searched for.
     * @return Coordinates of the cell associated playerID.
     */
    public Coord findPlayerCoords(int playerID) {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (board[y][x] == playerID)
                    /*
                     * Note the switch here between x and y. This is because our board matrix is indexed as
                     * [rows][columns], or alternatively [y][x]. The Coord constructor orders these two components as
                     * (x, y).
                     */
                    return new Coord(x, y);
            }
        }

        Log.warn("GridWorldState.getPlayerCoords: Coordinate not found for player " + playerID + ".");
        return null;
    }


    /**
     * Moves a piece from coordinate from to coordinate to by replacing the destination cell with the piece and
     * setting the source cell empty. The assumption is therefore made that pieces can only come from cells that were
     * previously empty.
     *
     * @param from Source position
     * @param to Destination position
     */
    public void update(Coord from, Coord to) {
        if (from.equals(to)) return;

        if (board[from.getY()][from.getX()] == CELL_REACHABLE) {
            Log.warn("GridWorldState.update: Moving an empty cell from " + from + " to " + to + ".");
        }

        // Move the player's "piece".
        board[to.getY()][to.getX()] = board[from.getY()][from.getX()];
        board[from.getY()][from.getX()] = CELL_REACHABLE;
    }

    @Override
    public GridWorldState deepCopy() {
        return new GridWorldState(this);
    }

    @Override
    public String toString() {
        return toString(null);
    }

    public String toString(GridWorldLogic logic) {
        StringBuilder sb = new StringBuilder();

        sb.append(width).append("x").append(height).append("\n");

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (board[i][j] >= 0) {
                    // Cell occupied by player piece
                    sb.append(board[i][j]);
                } else if (logic != null && logic.getTerminalPositions().contains(new Coord(j, i))) {
                    sb.append('T');
                } else if (logic != null && logic.getPortalCoordinates().contains(new Coord(j, i))) {
                    sb.append('P');
                } else if (board[i][j] == CELL_UNREACHABLE) {
                    // Unreachable cell
                    sb.append(GridWorldLevelParser.LEVEL_TOKEN_UNREACHABLE);
                } else if (board[i][j] == CELL_REACHABLE) {
                    // Empty cell
                    sb.append(GridWorldLevelParser.LEVEL_TOKEN_EMPTY);
                }
                sb.append(GridWorldLevelParser.LEVEL_TOKEN_SPACER);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        GridWorldState that = (GridWorldState) o;
        return height == that.height &&
                width == that.width &&
                Arrays.deepEquals(board, that.board);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(super.hashCode(), height, width);
        result = 31 * result + Arrays.deepHashCode(board);
        return result;
    }
}
