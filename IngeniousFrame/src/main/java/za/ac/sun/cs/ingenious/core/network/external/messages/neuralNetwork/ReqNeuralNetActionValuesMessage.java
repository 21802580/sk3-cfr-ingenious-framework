package za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalStateMatrix;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to request action values for a state using the DQN neural network.
 *
 * @author Steffen Jacobs
 */
public class ReqNeuralNetActionValuesMessage extends ExternalMessage {
    private ExternalStateMatrix payload;

    public ReqNeuralNetActionValuesMessage(double[][][] state) {
        super(ExternalMessageType.REQ_NEURAL_NET_ACTION_VALUES);

        payload = new ExternalStateMatrix(state);
    }

    public ReqNeuralNetActionValuesMessage(double[][] state) {
        super(ExternalMessageType.REQ_NEURAL_NET_ACTION_VALUES);

        payload = new ExternalStateMatrix(state);
    }

    public ReqNeuralNetActionValuesMessage(double[] state) {
        super(ExternalMessageType.REQ_NEURAL_NET_ACTION_VALUES);

        payload = new ExternalStateMatrix(state);
    }
}
