package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldFeatureExtractor;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.qlearning.LinearFunctionApproxQLearning;

import java.io.IOException;
import java.util.List;

/**
 * Linear function approximation engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldLinearFunctionApproxQLearningEngine extends GridWorldEngine {
    private GridWorldState previousState = null;
    private Action previousAction = null;
    private LinearFunctionApproxQLearning alg;

    public GridWorldLinearFunctionApproxQLearningEngine(EngineToServerConnection toServer) throws IOException, ClassNotFoundException {
        super(toServer);
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        if (alg == null) {
            try {
                alg = new LinearFunctionApproxQLearning(logic, new GridWorldFeatureExtractor());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldExternalTabularQLearningEngine";
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(state, a.getMove());
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = null;
        List<Action> availableActions = logic.generateActions(state, playerID);

        choice = alg.chooseAction(state, availableActions);

        previousAction = choice.deepCopy();
        previousState = state.deepCopy();
        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward reward = (ScalarReward) a.getReward();

        alg.update(previousState, previousAction, reward, state);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.endEpisode();

        if (alg.getTotalEpisodes() % 10 == 0)
            Log.info(GridWorldPolicyStringBuilder.buildUsingExternalFunctionApprox(state, alg.getKnownActions().toArray(new Action[0]), alg.getFunctionApproximation(), alg.getFeatureExtractor(), logic));
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        alg.printMetrics();

        Log.info(GridWorldPolicyStringBuilder.buildUsingExternalFunctionApprox(state, alg.getKnownActions().toArray(new Action[0]), alg.getFunctionApproximation(), alg.getFeatureExtractor(), logic));

        alg.close();
    }

    public LinearFunctionApproxQLearning getAlg() {
        return alg;
    }
}
