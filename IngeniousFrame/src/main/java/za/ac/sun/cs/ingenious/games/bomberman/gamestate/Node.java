package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import java.awt.Point;

/**
 * Representation of a single square on the Bomberman board
 */
public class Node {
	private Bomb bomb;
	private Player player;
	private boolean wall;
	private boolean cradle;
	private boolean exploding;
	private boolean bombUpgrade;
	private boolean blastUpgrade;
    private final Point loc;
	
	public Node(Point loc, boolean cradle, boolean wall) {
        this.loc = new Point(loc);
		this.cradle = cradle;
		this.wall = wall;
	}
	
	public void setBomb(Bomb b) {
		this.bomb = b;
	}
	
	public void setPlayer(Player p){
		 player = p;
	}
	
	public void removeCradle(){
		cradle = false;
	}
	
	public boolean isPassable(){
		return !hasBomb() && !hasPlayer() && !wall && !cradle;
	}

	public boolean hasBomb(){
		return bomb != null;
	}
	
	public boolean hasPlayer(){
		return player != null;
	}
	
	public Player getPlayer() {
		return player;
	}
	
	public void removeBomb() {
		bomb = null;		
	}

	public void removePlayer() {
		player = null;
	}

	public Bomb getBomb() {
		return bomb;
	}
	
	public boolean isCradle() {
		return cradle;
	}
	
	public boolean isWall() {
		return wall;
	}
	
	public void markExploding(boolean exp){
        if (!wall) {
    		exploding = exp;
        }
	}
	
	public boolean isExploding() {
		return exploding;
	}

	public boolean hasBlastUpgrade() {
		return blastUpgrade;
	}
	
	public boolean hasBombUpgrade() {
		return bombUpgrade;
	}
	
	public void setBlastUpgrade(boolean blastUpgrade) {
		this.blastUpgrade = blastUpgrade;
	}
	
	public void setBombUpgrade(boolean bombUpgrade) {
		this.bombUpgrade = bombUpgrade;
	}

    /**
     * Returns the location of the node in the map.
     *
     * @return Location as a point storing (x,y) coordinates.  Note that these
     * are not the same as the indices into the underlying Node[][] used for the
     * board representation.
     */
    public Point getLoc() {
        return new Point(loc);
    }

	public char getChar() {
		if (wall)
			return '#';
		if (exploding)
			return '*';
		if (cradle)
			return '+';
		if (hasBomb() && hasPlayer()){
			return 'p';
		}
		if (hasPlayer())
			return 'P';
		if (hasBomb())
			return (char)(48+bomb.getTimer());
        // TODO: issue if cell has both upgrade types - not possible for current implementation, but could lead to errors later - see issue 169
		if (hasBlastUpgrade()){
			return '!';
		}
		if (hasBombUpgrade()){
			return '&';
		}
		return ' ';
	}

    public boolean similar(Node n) {
        if (n == null) return false;
        if (loc == null) {
            if (n.loc != null) return false;
        } else {
            if (!loc.equals(n.loc)) return false;
        }
        if (wall != n.wall || cradle != n.cradle || exploding != n.exploding || bombUpgrade != n.bombUpgrade || blastUpgrade != n.blastUpgrade) return false;
        if (bomb == null) {
            if (n.bomb != null) return false;
        } else {
            if (!bomb.similar(n.bomb)) return false;
        }
        if (player == null) {
            if (n.player != null) return false;
        } else {
            if (!player.similar(n.player)) return false;
        }
        return true;
    }
}
