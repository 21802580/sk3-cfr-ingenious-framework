package za.ac.sun.cs.ingenious.games.tictactoe;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

/**
 * The final evaluator determines the score at the end of the game. Every game must
 * implement the GameFinalEvaluator interface. Here, we also implement the GameEvaluator
 * interface, so that we can use Minimax search.
 */
public class TTTFinalEvaluator implements GameEvaluator<TurnBasedSquareBoard>{

	/**
	 * @param forState Some terminal state
	 * @return The ID of the player who won in that state, or -1 if it is a draw.
	 */
	public int getWinner(TurnBasedSquareBoard forState) {
		for(int i = 0; i < 9; i= i+3){
			if(forState.board[i+0] != 0 &&  forState.board[i+0] == forState.board[i+1] && forState.board[i+1] == forState.board[i+2]) {
				return forState.board[i+0]-1;
			}
		}
		for(int i = 0; i < 3; i++){
			if(forState.board[i+0] != 0 && forState.board[i+0] == forState.board[i+3] && forState.board[i+3] == forState.board[i+6]) {
				return forState.board[i+0]-1;
			}
		}
		if(forState.board[0] != 0 && forState.board[0] == forState.board[4] && forState.board[4] == forState.board[8]) {
			return forState.board[0]-1;
		}
		if(forState.board[2] != 0 && forState.board[2] == forState.board[4] && forState.board[4] == forState.board[6]) {
			return forState.board[2]-1;
		}

		return -1;
	}

	/**
	 * We normalize the players's scores here, so 0 is a complete loss, 1 a perfect win
	 * and 0.5 a draw.
	 */
	@Override
	public double[] getScore(TurnBasedSquareBoard forState) {
		double[] scores = new double[2];
		int winner = getWinner(forState);
		if(winner == -1){
			scores[0] = 0.5;
			scores[1] = 0.5;
		}else{
			scores[winner] = 1;
		}
		return scores;
	}

}
