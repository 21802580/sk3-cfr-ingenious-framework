package za.ac.sun.cs.ingenious.search.rl;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.GameSpeed;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.*;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKReferee;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKExternalDQNEngine;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKMCTSEngine;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKRandomEngine;
import za.ac.sun.cs.ingenious.games.mnk.engines.MNKTabularQLearningEngine;
import za.ac.sun.cs.ingenious.games.snake.SnakeEngine;
import za.ac.sun.cs.ingenious.games.snake.SnakeReferee;
import za.ac.sun.cs.ingenious.games.snake.engines.SnakeExternalDQNEngine;

import java.io.IOException;

/**
 * Class with a main method used to train reinforcement learning agents. While using the client-server flow does work
 * for RL agents, it is very slow. This provides a place to train agents a bit more quickly.
 *
 * The structure of this class is very informal, and can be made much more modular in the future, if needed.
 *
 * @author Steffen Jacobs
 */
public class TrainingSandbox {
    public static void main(String[] args) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        if (args.length < 2) {
            System.out.println("Match count and scenario must be provided as args.");
            return;
        }

        String scenario = args[0];
        int matchCount = Integer.parseInt(args[1]);
        String optionalArg = args.length > 2 ? args[2] : null;

        System.out.println("Running `" + scenario + "` for " + matchCount + " matches.");

        switch (scenario) {
            case "snakeDQN":
                snakeDQN(matchCount);
                break;

            case "gridWorldExternalTable":
                gridWorldExternalTable(matchCount, optionalArg);
                break;
            case "gridWorldExternalFunctionApproximation":
                gridWorldExternalFunctionApproximation(matchCount, optionalArg);
                break;
            case "gridWorldDQN":
                gridWorldDQN(matchCount, optionalArg);
                break;
            case "gridWorldPolicyIteration":
                gridWorldPolicyIteration(matchCount, optionalArg);
                break;
            case "gridWorldValueIteration":
                gridWorldValueIteration(matchCount, optionalArg);
                break;
            case "gridWorldMonteCarloOnPolicy":
                gridWorldMonteCarloOnPolicy(matchCount, optionalArg);
                break;
            case "gridWorldMonteCarloOffPolicy":
                gridWorldMonteCarloOffPolicy(matchCount, optionalArg);
                break;
            case "gridWorldSarsa":
                gridWorldSarsa(matchCount, optionalArg);
                break;
            case "gridWorldTabularQLearning":
                gridWorldTabularQLearning(matchCount, optionalArg);
                break;
            case "gridWorldLFAQLearning":
                gridWorldLFAQLearning(matchCount, optionalArg);
                break;

            case "mnkTabularQLearning":
                mnkTabularQLearning(matchCount);
                break;
            case "mnkDQN":
                mnkDQN(matchCount);
                break;
            case "mnkDQNSelfPlay":
                mnkDQNSelfPlay(matchCount);
                break;
            default:
                System.out.println("Specified scenario not valid.");
                break;
        }

        System.exit(0);
    }

    private static void snakeDQN(int matchCount) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        SnakeExternalDQNEngine engine = new SnakeExternalDQNEngine(
                new DummyEngineToServerConnection(0)
        );

        engine.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);

        SnakeReferee referee = new SnakeReferee(matchSetting, players);
        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
//        rlEngine.getAlg().printQTable();
    }

    private static void gridWorldPolicyIteration(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldPolicyIterationEngine engine = new GridWorldPolicyIterationEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldValueIteration(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldValueIterationEngine engine = new GridWorldValueIterationEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldMonteCarloOnPolicy(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldMonteCarloOnPolicyEngine engine = new GridWorldMonteCarloOnPolicyEngine(
                new DummyEngineToServerConnection(0)
        );

        engine.getAlg().withEpsilon(0.3);

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldMonteCarloOffPolicy(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldMonteCarloOffPolicyEngine engine = new GridWorldMonteCarloOffPolicyEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldSarsa(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldTabularSarsaEngine engine = new GridWorldTabularSarsaEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldTabularQLearning(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldTabularQLearningEngine engine = new GridWorldTabularQLearningEngine(
                new DummyEngineToServerConnection(0)
        );

        engine.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(engine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldLFAQLearning(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldLinearFunctionApproxQLearningEngine rlEngine = new GridWorldLinearFunctionApproxQLearningEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(rlEngine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.showUI();
        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void gridWorldDQN(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldExternalDQNEngine rlEngine = new GridWorldExternalDQNEngine(
                new DummyEngineToServerConnection(0)
        );

        rlEngine.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(rlEngine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.showUI();
        referee.setGameSpeed(GameSpeed.REALTIME);
        referee.run();
    }

    private static void mnkTabularQLearning(int matchCount) throws IOException, ClassNotFoundException, MissingSettingException, IncorrectSettingTypeException {
        int playerID = 0;

        MNKTabularQLearningEngine player0 = new MNKTabularQLearningEngine(
                new DummyEngineToServerConnection(playerID++)
        );

        player0.setTraining();

        MNKTabularQLearningEngine player1 = new MNKTabularQLearningEngine(
                new DummyEngineToServerConnection(playerID++)
        );

        player1.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(player0),
                new LocalPlayer(player1),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("mnk_height", 3.0);
        matchSetting.setSetting("mnk_width", 3.0);
        matchSetting.setSetting("mnk_k", 3.0);
        matchSetting.setSetting("perfectInformation", true);

        // Instantiate and run the referee.
        MNKReferee referee = new MNKReferee(matchSetting, players);

        referee.run();
    }

    private static void mnkDQN(int matchCount) throws IOException, ClassNotFoundException, MissingSettingException, IncorrectSettingTypeException {
        int playerID = 0;

        MNKExternalDQNEngine player0 = new MNKExternalDQNEngine(
                new DummyEngineToServerConnection(playerID++)
        );

        player0.setTraining();

        MNKRandomEngine player1 = new MNKRandomEngine (
                new DummyEngineToServerConnection(playerID++)
        );

//        player1.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(player0),
                new LocalPlayer(player1),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("mnk_height", 3.0);
        matchSetting.setSetting("mnk_width", 3.0);
        matchSetting.setSetting("mnk_k", 3.0);
        matchSetting.setSetting("perfectInformation", true);

        // Instantiate and run the referee.
        MNKReferee referee = new MNKReferee(matchSetting, players);

        referee.run();
    }

    private static void mnkDQNSelfPlay(int matchCount) throws IOException, ClassNotFoundException, MissingSettingException, IncorrectSettingTypeException {
        int playerID = 0;

        MNKExternalDQNEngine player0 = new MNKExternalDQNEngine(
                new DummyEngineToServerConnection(playerID++)
        );

        player0.setTraining();

        MNKExternalDQNEngine player1 = new MNKExternalDQNEngine (
                new DummyEngineToServerConnection(playerID++)
        );

        player1.setTraining();

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(player0),
                new LocalPlayer(player1),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("mnk_height", 3.0);
        matchSetting.setSetting("mnk_width", 3.0);
        matchSetting.setSetting("mnk_k", 3.0);
        matchSetting.setSetting("perfectInformation", true);

        // Instantiate and run the referee.
        MNKReferee referee = new MNKReferee(matchSetting, players);

        referee.run();
    }

    private static void gridWorldExternalTable(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {

        GridWorldExternalTabularQLearningEngine rlEngine = new GridWorldExternalTabularQLearningEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(rlEngine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.run();

//        rlEngine.getAlg().printQTable();
    }

    private static void gridWorldExternalFunctionApproximation(int matchCount, String level) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldLinearFunctionApproxQLearningEngine rlEngine = new GridWorldLinearFunctionApproxQLearningEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(rlEngine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", level);

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.run();

//        rlEngine.getAlg().printQTable();
    }

    private static void gridWorldExternalDQN(int matchCount) throws MissingSettingException, IncorrectSettingTypeException, IOException, ClassNotFoundException {
        GridWorldExternalDQNEngine rlEngine = new GridWorldExternalDQNEngine(
                new DummyEngineToServerConnection(0)
        );

        PlayerRepresentation[] players = new PlayerRepresentation[] {
                new LocalPlayer(rlEngine),
        };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumPlayers(players.length);
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelFile", "./scripts/gridWorld/large.gridworld");

        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);

        referee.run();
    }
}
