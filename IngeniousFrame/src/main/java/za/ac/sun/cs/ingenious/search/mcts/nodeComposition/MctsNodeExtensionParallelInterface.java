package za.ac.sun.cs.ingenious.search.mcts.nodeComposition;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.List;

public interface MctsNodeExtensionParallelInterface {

    String getID();

    void setUp(MctsNodeComposition node);

    <S extends GameState> void addChild(MctsNodeTreeParallel<S> child);

    <S extends GameState> void addChildren(List<MctsNodeTreeParallel<S>> newChildren);

    <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void logPossibleMoves(N node);

}