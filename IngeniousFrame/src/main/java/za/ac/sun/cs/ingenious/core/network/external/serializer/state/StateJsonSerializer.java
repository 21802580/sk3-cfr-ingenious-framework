package za.ac.sun.cs.ingenious.core.network.external.serializer.state;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Generic serializer for extensions of GameState.
 *
 * @author Steffen Jacobs
 */
public class StateJsonSerializer {
    public static String serialize(GameState state) {
        Gson gson = new Gson();

        JsonObject obj = new JsonObject();
        obj.addProperty("type", state.getClass().getSimpleName());
        obj.addProperty("data", gson.toJson(state));

        return obj.toString();
    }
}