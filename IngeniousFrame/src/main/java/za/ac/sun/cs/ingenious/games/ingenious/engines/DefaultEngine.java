package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousBoard;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousEngine;

public class DefaultEngine extends IngeniousEngine {

	private IngeniousBoard gameBoard;

	public DefaultEngine(String host, int port, MatchSetting match, int position)
			throws UnknownHostException, IOException {
		super(new Socket(host, port), match, position);
		gameBoard = new IngeniousBoard(11,6);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void receiveInitGameMessage(InitGameMessage a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void receivePlayedMoveMessage(PlayedMoveMessage a) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		// TODO Auto-generated method stub
		return null;
	}

//	public void run() {
//		Log.info("default engine started running");
//		InputStreamReader ISR = new InputStreamReader(System.in);
//		BufferedReader BR = new BufferedReader(ISR);
//		int counter = 0;
//
//		while (true) {
//			try {
//				String[] msg = messageHandler.receiveMessage();
//				
//
//				if (msg[0].equals(TCPProtocol.ID)) {
//					messageHandler.reply("" + this.playerId);
//				} else if (msg[0].equals(TCPProtocol.NAME)) {
//					messageHandler.reply("Default_Engine");
//				} else if (msg[0].equals(TCPProtocol.GENMOVE)) {
//					for (int i = 1; i < msg.length; i++) {
//						Log.info(msg[i]);
//					}
//					Log.info("WHAT IS YOUR REPLY?");
//					String[] reply = messageHandler.splitMessage(BR.readLine());
//
//					Tile tile = new Tile(Integer.parseInt(reply[1]),
//							Integer.parseInt(reply[2]));
//					tile.setRotation(Integer.parseInt(reply[3]), 6);
//					Coord coord = new Coord(
//							Integer.parseInt(reply[4]),
//							Integer.parseInt(reply[5]));
//					IngeniousMove move = new IngeniousMove(tile, coord);
//					gameBoard.makeMove(move);
//					Log.info(gameBoard);
//					messageHandler.reply(reply[1], reply[2], reply[3],
//							reply[4], reply[5]);
//				} else if (msg[0].equals(TCPProtocol.PLAYMOVE)) {
//					Log.info("playmove received");
//					String[] moveReply = msg;
//					
//					Tile tile = new Tile(Integer.parseInt(moveReply[1]),
//							Integer.parseInt(moveReply[2]));
//					tile.setRotation(Integer.parseInt(moveReply[3]), 6);
//					Coord coords = new Coord(
//							Integer.parseInt(moveReply[4]),
//							Integer.parseInt(moveReply[5]));
//					IngeniousMove move = new IngeniousMove(tile, coords);
//					gameBoard.makeMove(move);
//					Log.info(gameBoard);
//
//				} else {
//					Log.info("WHAT IS YOUR REPLY?");
//					messageHandler.replyBrute(BR.readLine());
//				}
//
//			} catch (Exception e) {
//				messageHandler.errorReply("" + e.toString());
//			}
//		}
//	}
}
