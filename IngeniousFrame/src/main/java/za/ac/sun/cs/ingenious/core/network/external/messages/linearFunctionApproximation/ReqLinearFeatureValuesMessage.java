package za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalFeatures;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to request action values for a set of linear features from a state.
 *
 * @author Steffen Jacobs
 */
public class ReqLinearFeatureValuesMessage extends ExternalMessage {
    private ExternalFeatures payload;

    public ReqLinearFeatureValuesMessage(double[][] featureSets) {
        super(ExternalMessageType.REQ_LINEAR_FEATURE_VALUES);

        this.payload = new ExternalFeatures(featureSets);
    }

    public ExternalFeatures getPayload() {
        return payload;
    }

    public void setPayload(ExternalFeatures payload) {
        this.payload = payload;
    }
}
