package za.ac.sun.cs.ingenious.core.util.nodes.trees;

/**
 * An extendable abstract class that provides base functionality for binary tree nodes.  
 * Provides default implementations of all methods defined by the {@link TreeNode} and {@link TreeNode.Binary} interfaces.
 * Instead of an array of children this class defines two public fields: {@code left} and {@code right}, 
 * which define the first and second child of the node respectively.
 * 
 * @author Nicholas Robinson
 */
public abstract class BinaryTreeNode<SELF extends BinaryTreeNode<SELF>> implements TreeNode<SELF>, TreeNode.Binary<SELF> {

    /**
     * first child 
     */
    protected SELF left;

    /**
     * second child
     */
    protected SELF right;

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF getLeft() {
        return left;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF getRight() {
        return right;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLeft(SELF left) {
        this.left = left;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setRight(SELF right) {
        this.right = right;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int childCount() {
        return (getLeft() != null ? 1:0) + (getRight() != null ? 1:0);
    }

    /**
     * @return 2
     */
    @Override
    public int childCapacity() {
        return 2;
    }

    /**
     * will only accept index values of 0 (for left child) and 1 (for right child).
     * 
     * {@inheritDoc}
     */
    @Override
    public SELF getChild(int index) {
        switch (index) {
            case 0: return left;
            case 1: return right;
            default: throw new IndexOutOfBoundsException(index);
        }
    }

    /**
     * {@inheritDoc}
     * 
     * If {@code left} is null then {@code child} will be referenced by {@code left},
     * otherwise if {@code right} is null then {@code child} will be referenced by {@code right}.
     * If both {@code left} and {@code right} already reference children, then {@code child} will not be appended.
     * 
     */
    @Override
    public boolean appendChild(SELF child) {
        if (left == null) {
            left = child;
            return true;
        }
        else if (right == null) {
            right = child;
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF removeChild(int index) {
        SELF c;
        switch (index) {
            case 0:
                c = left;
                left = null;
                return c;
            case 1:
                c = right;
                right = null;
                return c;
            default: throw new IndexOutOfBoundsException(index);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF removeChild(SELF child) {
        SELF c = null;
        if (left == child) {
            c = left;
            left = null;
        }
        else if (right == child) {
            c = right;
            right = null;
        }
        return c;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF setChild(int index, SELF replacement) {
        SELF c;
        switch (index) {
            case 0:
                c = left;
                left = replacement;
                return c;
            case 1:
                c = right;
                right = replacement;
                return c;
            default:
                throw new IndexOutOfBoundsException(index);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SELF replaceChild(SELF child, SELF replacement) {
        SELF c = null;
        if (left == child) {
            c = left;
            left = replacement;
        }
        else if (right == child) {
            c = right;
            right = replacement;
        }
        return c;
    }

    /**
     * {@inheritDoc}
     */
    public abstract SELF clone();
}