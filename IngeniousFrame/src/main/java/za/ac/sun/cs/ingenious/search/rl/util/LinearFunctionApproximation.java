package za.ac.sun.cs.ingenious.search.rl.util;

/**
 * Interface that defines a layer between an algorithm and the maintained linear function weights, whether it be an
 * internal implementation in the Ingenious Framework, or external by communicating with the Python program.
 *
 * @author Steffen Jacobs
 */
public interface LinearFunctionApproximation {
    boolean initialized = false;

    /**
     * Initialize the weights for the specified feature set size.
     * @param size Size of the feature sets used.
     */
    void init(int size);

    /**
     * Performs any initialization if necessary, such as invoking init() if necessary.
     * @param featureSet A feature set, used to aid in any initialization (for instance, using its size).
     */
    void ensureInitialized(double[] featureSet);

    /**
     * Given multiple feature sets (from multiple states), compute the resulting predicted values for each feature set.
     *
     * Note that indices of the returned value array correspond to those of the provided featureSets. While such parallel
     * arrays are bad practice, this is a trade-off in order to simplify implementations that make use of network
     * communication (e.g. an implementation that communicates with the Python app).
     *
     * @param featureSets One or more feature sets to determine the value of.
     * @return Array of values corresponding to each feature set.
     */
    public double[] get(double[][] featureSets);

    /**
     * Given the provided feature set and sampled estimatedValue, train the weights by calculating the loss in order to perform
     * a gradient descent step.
     *
     * @param featureSet Feature set corresponding to the sampled estimatedValue.
     * @param estimatedValue Sampled estimatedValue.
     */
    public void put(double[] featureSet, double estimatedValue);
}
