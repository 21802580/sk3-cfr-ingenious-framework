package za.ac.sun.cs.ingenious.games.loa.knowledge;

public enum HeuristicType {
    MOVE_CATEGORY, FUNCTION, NONE
}
