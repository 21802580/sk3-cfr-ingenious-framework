package za.ac.sun.cs.ingenious.games.puzzles.slidingpuzzle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


import za.ac.sun.cs.ingenious.core.util.hashing.BitStringGenerator;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.persistent.misc.PCopyOnWriteArray;
import za.ac.sun.cs.ingenious.search.astar.AStar;

/**
 * A module that unites sliding puzzle logic, state and the A* algorithm in order to solve sliding puzzle problems. 
 * Boards are represented with PVectors of Integers.
 * Moves are represented by the cardinal directions of {@link CompassDirection}.
 * 
 * <p> See {@link SlidingPuzzleAStarSolver#demo demo()} for a usage example.
 */
public class SlidingPuzzleAStarSolver extends SlidingPuzzleLogic {

    long ZHSEED1 = 123456789L, ZHSEED2 = 987654321L;
    int ZHMINHAMDIST = 4;

    private long[][] zobristHashes1 = new long[size][size];
    private long[][] zobristHashes2 = new long[size][size];
    {
        long[] l1 = BitStringGenerator.generateL(size * size - size, ZHMINHAMDIST, ZHSEED1);
        long[] l2 = BitStringGenerator.generateL(size * size - size, ZHMINHAMDIST, ZHSEED2);

        int l = 0;
        for (int a = 0; a < size; a++) {
            for (int b = 0; b < size; b++) {
                if (a == b) {
                    zobristHashes1[a][b] = 0;
                    zobristHashes2[a][b] = 0;
                }
                else {
                    zobristHashes1[a][b] = l1[l];
                    zobristHashes2[a][b] = l2[l];
                    l++;
                }
            }
        }
    }

    private Integer[] tiles = new Integer[size];
    {
        for (int i = 0; i < tiles.length; i++) {
            tiles[i] = i;
        }
    }

    public SlidingPuzzleAStarSolver(int width, int height) {
        super(width, height);
    }

    /**
     * Function useful for manually generating board states. Generates a sliding puzzle state from a 2D-array
     * @param board 2D-array of Integers
     * @return A sliding puzzle state that represents the given board
     */
    public SlidingPuzzleState generateState(Integer[][] board) {   
        Integer[] flattenedBoard = new Integer[size];

        int i = 0;
        for (int y = 0; y < height; y++) for (int x = 0; x < width; x++) {
            flattenedBoard[i++] = tiles[board[x][y]];
        }

        return generateState(flattenedBoard);
    }

    /**
     * Function useful for manually generating board states. Generates a sliding puzzle state from an array
     * @param board array of Integers
     * @return A sliding puzzle state that represents the given board
     */
    public SlidingPuzzleState generateState(Integer[] board) {
        SlidingPuzzleState state = new SlidingPuzzleState();
        
        for (int i = 0; i < size; i++) {
            if (board[i] == 0) state.emptyTileLocation = i;

            state.zobristHash1 ^= zobristHashes1[board[i]][i];

            state.zobristHash2 ^= zobristHashes2[board[i]][i];

            state.heuristicCost += manhattanDistance(board[i], i);
        }

        state.board = new PCopyOnWriteArray<Integer>(board);

        return state;
    }

    /**
     * Generates a sliding puzzle state that represents a solved state
     * @return solved state
     */
    public SlidingPuzzleState generateSolvedState() {
        Integer[] board = new Integer[width * height];
        long zobristHash1 = 0, zobristHash2 = 0;

        for (int i = 0; i < board.length; i++) {
            board[i] = i;
            zobristHash1 ^= zobristHashes1[i][i];
            zobristHash2 ^= zobristHashes2[i][i];
        }

        SlidingPuzzleState state = new SlidingPuzzleState();
        state.board = new PCopyOnWriteArray<Integer>(board);
        state.zobristHash1 = zobristHash1;
        state.zobristHash2 = zobristHash2;
        
        return state;
    }

    /**
     * Generates the optimal set of moves to solve a given sliding puzzle problem in the fewest number of moves.
     * @param state problem to be sovled.
     * @return a sequence of moves that if performed on the state will solve it.
     */
    public List<CompassDirection> determineOptimalMoveSequence(SlidingPuzzleState state) {
        SearchNode result = solve(state);
        return result == null ? null : extractMoveSequence(result);
    }

    /**
     * Generate a new state (a new object instance) by applying a move to a given state.
     * @param fromState that state which a move is applied to.
     * @param move the move that is applied.
     * @return a new state.
     */
    public SlidingPuzzleState applyMove(SlidingPuzzleState fromState, CompassDirection move) {
        return applyMove(fromState, new SlidingPuzzleState(), move);
    }

    /**
     * Apply a move to a state. The contents of the new state will be applied to {@code newState}
     * 
     * @param fromState that state which a move is applied to.
     * @param newState the object that will represent the new state.
     * @param move the move that is applied.
     * @return {@code newState}
     */
    public SlidingPuzzleState applyMove(SlidingPuzzleState fromState, SlidingPuzzleState newState, CompassDirection move) {
        
        int newEmptyTileLocation = moveLocation(fromState.emptyTileLocation, move);

        Integer tile = fromState.board.get(newEmptyTileLocation);
        
        PVector<Integer> newBoard = fromState.board.multiSet(fromState.emptyTileLocation, newEmptyTileLocation) 
            .to(tile, tiles[0]);

        long newZobristHash1 = fromState.zobristHash1
            ^ zobristHashes1[0][fromState.emptyTileLocation]
            ^ zobristHashes1[0][newEmptyTileLocation]
            ^ zobristHashes1[tile][newEmptyTileLocation]
            ^ zobristHashes1[tile][fromState.emptyTileLocation];

        long newZobristHash2 = fromState.zobristHash2
            ^ zobristHashes2[0][fromState.emptyTileLocation]
            ^ zobristHashes2[0][newEmptyTileLocation]
            ^ zobristHashes2[tile][newEmptyTileLocation]
            ^ zobristHashes2[tile][fromState.emptyTileLocation];

        double newHeuristicCost = fromState.heuristicCost 
            - manhattanDistance(tile, newEmptyTileLocation)
            + manhattanDistance(tile, fromState.emptyTileLocation);

        newState.emptyTileLocation = newEmptyTileLocation;
        newState.board = newBoard;
        newState.zobristHash1 = newZobristHash1;
        newState.zobristHash2 = newZobristHash2;
        newState.heuristicCost = newHeuristicCost;

        return newState;
    }

    /**
     * Shuffle a board randomly by applying a set number of moves to it.
     * @param fromState the state taht moves are applied to.
     * @param newState the object that will contain the final state generated by this function.
     * @param t the number of moves to apply.
     * @return a shuffled sliding puzzle state
     */
    public SlidingPuzzleState shuffle(SlidingPuzzleState fromState, SlidingPuzzleState newState, int t) {
        return shuffle(fromState, newState, t, new Random());
    }

    /**
     * Shuffle a board randomly by applying a set number of moves to it.
     * @param fromState the state taht moves are applied to.
     * @param newState the object that will contain the final state generated by this function.
     * @param t the number of moves to apply.
     * @param random the random number generater to generate random moves
     * @return a shuffled sliding puzzle state
     */
    public SlidingPuzzleState shuffle(SlidingPuzzleState fromState, SlidingPuzzleState newState, int t, Random random) {
        SlidingPuzzleState state = fromState;
        CompassDirection[] moves = CompassDirection.cardinalDirections().toArray(new CompassDirection[4]);
        while (t-- > 0) {
            int m;
            while (!legalMove(state.emptyTileLocation, moves[m = random.nextInt(4)])) {}
            state = applyMove(state, newState, moves[m]);
        }
        return state;
    }

    /**
     * Calls the {@link AStar} search algorithm with SearchNode containing the initial state.
     * @param state state to solve
     * @return The searchnode that was returned by A*.
     */
    private SearchNode solve(SlidingPuzzleState state) {
        SlidingPuzzleState solvedState = generateSolvedState();

        SearchNode result = new AStar<SearchNode>(
            (n) -> n.getPosition().equals(solvedState),
            (n) -> generateSuccessors(n),
            (a, b) -> 1,
            (n) -> ((SlidingPuzzleState)n.getPosition()).heuristicCost
        ) {
            {
                //createOpenMap = TreeMap::new;
                //createClosedMap = TreeMap::new;
            }
        }.search(new SearchNode(state));

        return result;
    }

    /**
     * Extracts a move sequence from the lineage of a SearchNode
     * @param node SearchNode generated by the search
     * @return Move sequence
     */
    private List<CompassDirection> extractMoveSequence(SearchNode node) {
        List<CompassDirection> moves = new ArrayList<CompassDirection>();

        while (node != null && node.move != null) {
            moves.add(node.move);
            node = node.previous;
        }

        Collections.reverse(moves);
        return moves;
    }

    /**
     * For a given node, generate all the successors of that node, by considering and applying the moves that are legal to the
     * state of the given node.
     * @param node parent node that we will generate children from.
     * @return An Iterable element of the successors of the given node.
     */
    private Iterable<SearchNode> generateSuccessors(SearchNode node) {
        List<SearchNode> successors = new ArrayList<SearchNode>(4);

        for (CompassDirection move : CompassDirection.cardinalDirections()) {
            if (legalMove(node.getPosition().emptyTileLocation, move)) {
                successors.add(new SearchNode(applyMove(node.getPosition(), move), node, move));  
            }
        }
        return successors;       
    }

    public static class SearchNode extends AStar.SearchNode<SlidingPuzzleState> {

        
        public static long NODE_E; // number of expansions
        {
            NODE_E++;
        }
        

        /* the parent of this node, from which this node was generated */
        public SearchNode previous;

        /* the move that was applied to reach the current state */
        public CompassDirection move;

        public SearchNode(SlidingPuzzleState position) {
            super(position);
        }

        public SearchNode(SlidingPuzzleState position, SearchNode previous, CompassDirection move) {
            super(position);
            this.previous = previous;
            this.move = move;
        }
    }

    /**
     * Visually displays a sequence of moves applied to a state by printing to console (System.out) at fixed intervals. Useful for debugging.
     * 
     * @param fromState the starting state.
     * @param moves the move sequence to apply to the state.
     * @param sleepInterval the wait period between frames.
     * @param overwrite if set to true, for each frame will erase the previously printed frame and write over it
     * @throws InterruptedException
     */
    public void displayMoveSequence(SlidingPuzzleState fromState, List<CompassDirection> moves, long sleepInterval, boolean overwrite) throws InterruptedException {
        
        SlidingPuzzleState state = fromState.clone();
        System.out.println(stringifyBoard(state.board));
        System.out.println("move: " + 0);

        int m = 0;
        for (CompassDirection move : moves) {
            Thread.sleep(sleepInterval);

            if (overwrite) System.out.print(String.format("\033[%dA", height + 2));

            state = applyMove(state, state, move);

            System.out.println(stringifyBoard(state.board));
            System.out.println("move: " + ++m);
        }
    }

    /**
     * Sample usage
     */
    public static void demo() {

        SlidingPuzzleAStarSolver solver = new SlidingPuzzleAStarSolver(6, 6);

        var solvedState = solver.generateSolvedState();

        SlidingPuzzleState state = solver.shuffle(solvedState, new SlidingPuzzleState(), 100, new Random(123));

        System.out.println("Initial State");
        System.out.println(solver.stringifyBoard(state.board));

        System.gc();

        System.out.println("\n\nSTARTING SEARCH");

        long ST = System.nanoTime();

        var solution = solver.determineOptimalMoveSequence(state);
        
        long m = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1048576;
        System.out.println("memory: " + m);

        System.out.println(solution == null ? "no solution" : "found solution");

        System.out.println("time: " + ((System.nanoTime() - ST) / 1000000) + " ms");
        
        System.out.println("optimal number of moves: " + solution.size());

        try {
			solver.displayMoveSequence(state, solution, 1000, true);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    }
}
