package za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to initialize linear function approximation in the Python application.
 *
 * @author Steffen Jacobs
 */
public class InitLinearFeatureApproximationMessage extends ExternalMessage {
    private int payload;

    public InitLinearFeatureApproximationMessage(int featureSetSize) {
        super(ExternalMessageType.INIT_LINEAR_FEATURE_APPROXIMATION);

        this.payload = featureSetSize;
    }

    public int getPayload() {
        return payload;
    }

    public void setPayload(int payload) {
        this.payload = payload;
    }
}
