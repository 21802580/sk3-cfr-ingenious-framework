package za.ac.sun.cs.ingenious.games.mdp;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Class containing the final evaluator for the MDP game. An MDP has no concept of game score at the end of the game,
 * only whether or not an agent has reached a terminal state. This evaluator therefore regards winning as reaching a
 * terminal state that results in a positive reward.
 *
 * @author Steffen Jacobs
 */
public class MDPFinalEvaluator<T> implements GameFinalEvaluator<MDPState<T>> {
    public static final int WIN_SCORE = 1;
    public static final int LOSE_SCORE = -1;

    private Set<MDPState<T>> winningStates;

    public MDPFinalEvaluator(Set<MDPState<T>> winningStates) {
        this.winningStates = winningStates;
    }

    public static MDPFinalEvaluator<Long> fromSetting(MDPSetting mdpSetting) {
        return new MDPFinalEvaluator<>(createWinningStatesFromSetting(mdpSetting));
    }

    /**
     * Initialize reward lookups for (S, S') pairs in a nested map using the provided MDP settings file.
     *
     * @param mdpSetting Settings defining the MDP being created.
     */
    private static Set<MDPState<Long>> createWinningStatesFromSetting(MDPSetting mdpSetting) {
        Set<MDPState<Long>> winningStates = new HashSet<>();

        Collection<Integer> terminalStates = mdpSetting.getTerminalStates();

        // Rewards collection representing R(S, S').
        Collection<Collection<Double>> rewards = mdpSetting.getReward();
        long i = 0;

        // For each state, look at R(S = i, S' in terminal).
        for (Collection<Double> rewardForNewState : rewards) {
            Double[] rewardForNewStateArray = rewardForNewState.toArray(new Double[0]);

            // For each new state, look at R(S = i, S' = terminalStateNumber).
            for (int terminalStateNumber : terminalStates) {
                // If the reward for this terminal state is positive, assume it counts as a winning state.
                if (rewardForNewStateArray[terminalStateNumber] > 0) {
                    winningStates.add(new MDPState<>((long) terminalStateNumber));
                }
            }
        }

        return winningStates;
    }

    @Override
    public double[] getScore(MDPState<T> forState) {
        double score = LOSE_SCORE;

        if (winningStates.contains(forState)) score = WIN_SCORE;

        return new double[] { score };
    }
}
