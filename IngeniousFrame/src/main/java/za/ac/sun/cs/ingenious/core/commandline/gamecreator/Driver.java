package za.ac.sun.cs.ingenious.core.commandline.gamecreator;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.lobby.LobbyHandler;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.logging.Logger;

/**
 * Class for creating a lobby on some server. Hostname and port of the server
 * and the name of the lobby must be supplied as commandline parameters as well
 * as the name of the referee of the game to create a lobby for. After the lobby
 * was created (or creating the lobby failed), the program terminates with no
 * further action.
 *
 * Created by Chris Coetzee on 2016/07/27.
 */
public class Driver {

    public static void main(String[] args) {
        GameCreatorArguments gameArgs = new GameCreatorArguments();
        gameArgs.processArguments(args);

        Log.setLogger(new Logger(Constants.LogDirectory, "core.commandline.gamecreator.Driver"));
        Log.info("Gamecreator commandline script started");

        //Create lobbyHandler for hosting engine.
        String username = "LobbyHoster_" + Math.random() * 1000 + "_" + System.currentTimeMillis();
        LobbyHandler host = new LobbyHandler();

        if (!host.connect(gameArgs.getServerHostname(), gameArgs.getServerPort(), username)) {
            Log.info("Could not establish connection to server, terminating gamecreator script.");
            System.exit(-1);
        }

        MatchSetting ms = null;
        try {
            ms = new MatchSetting(gameArgs.getGameconfigPath());
        } catch (IOException e) {
            Log.error("Error during parsing of match settings");
            throw new RuntimeException(e);
        }

        ms.setLobbyName(gameArgs.getLobbyName());
        if (gameArgs.getPlayerCount() != MatchSetting.UNSET_POSITIVE_SCALAR) {
        	ms.setNumPlayers(gameArgs.getPlayerCount());
        }

        if (ms.getNumPlayers() == MatchSetting.UNSET_POSITIVE_SCALAR) {
            Log.error("Number of player not set in configuration file or with "
                              + GameCreatorArguments.FLAG_PLAYERS + " flag");
            System.exit(-1);
        }

        ms.setNumMatches(gameArgs.getNumMatches());
        Log.info("" + gameArgs.getNumMatches() + " matches will be played.");

        ms.setGameName(gameArgs.getGameName());

        Log.info(ms.getGameName());

        /* load the match settings */
        Log.info("Creating Lobby");
        if (host.createGame(ms)) {
			Log.info("Lobby successfully created");
		} else {
			Log.info("Error when creating lobby");
		}

        Log.info("Gamecreator script shutting down");
        host.closeConnection();
    }
}
