package za.ac.sun.cs.ingenious.core.network.game;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.LobbyHost;

/**
 * This class handles all sending and receiving of messages with one client.
 * It is assumed that a handshake with the connecting player has already taken place when an
 * object of this class is constructed
 *
 * @author Stephan Tietz, Marc
 */
public class ServerToEngineConnection extends SocketWrapper implements PlayerRepresentation {
	private LobbyHost lobbyHost;
	private String playerName;
	private int id;

	/**
	 * Creates a new server to engine connection.
	 *
	 * @param playerId the ID under which the player is registered in the lobby
	 * @param lobbyHost the lobbyHost that manages the game of this connection
	 * @param client the socket leading to the client
	 * @param is input stream for receiving messages
	 * @param os output stream for sending messages
	 */
	public ServerToEngineConnection(int playerId, LobbyHost lobbyHost, Socket client, ObjectInputStream is, ObjectOutputStream os, String playerName){
		super(client, is, os);
		this.lobbyHost = lobbyHost;
		this.id = playerId;
		this.playerName = playerName;
	}

	/**
	 * If an exception arises, the {@code ServerToEngineConnection} will notify the lobbyHost that an engine disconnected.
	 */
	@Override
	protected void handleIOException(IOException e){
		super.handleIOException(e);
		lobbyHost.engineDisconnected(id);
	}

	/**
	 * Sends this message to the corresponding client.
	 * @param move the message to be sent
	 */
	@Override
	public void playMove(PlayedMoveMessage move) {
		sendMessage(move);
	}

	/**
	 * Sends a {@link GenActionMessage} message to the client and blocks until the client
	 * responds with a {@link PlayActionMessage}.
	 *
	 * @return the {@link PlayActionMessage} sent by the client
	 */
	@Override
	public PlayActionMessage genAction(GenActionMessage a) {
		sendMessage(a);
		PlayActionMessage moveReply = (PlayActionMessage)receiveMessage();
		return moveReply;
	}

	@Override
	public int getID() {
		return id;
	}

	public String getPlayerName() {
		return playerName;
	}

	@Override
	public String toString() {
		return playerName;
	}

	@Override
	public void initGame(InitGameMessage a) {
		sendMessage(a);
	}

	@Override
	public void terminateGame(GameTerminatedMessage a) {
		sendMessage(a);
	}

	@Override
	public void resetMatch(MatchResetMessage a) {
		sendMessage(a);
	}

	@Override
	public void reward(RewardMessage a) {
		sendMessage(a);
	}
}
