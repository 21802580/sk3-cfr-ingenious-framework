package za.ac.sun.cs.ingenious.games.mnk.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;

import java.util.List;
import java.util.Scanner;

/**
 * Human player for the MNK game, prompting the user to input chosen actions.
 *
 * @author Steffen Jacobs
 */
public class MNKHumanPlayerEngine extends MNKEngine {
    private Scanner scanner;

    public MNKHumanPlayerEngine(EngineToServerConnection toServer) {
        super(toServer);
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {

    }

    @Override
    public String engineName() {
        return "MNKHumanPlayerEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        scanner = new Scanner(System.in);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        List<Action> availableActions = logic.generateActions(board, playerID);

        XYAction[] availableDirections = availableActions.stream()
                .map(x -> ((XYAction)x))
                .toArray(XYAction[]::new);

        StringBuilder promptStringBuilder = new StringBuilder();

        board.printPretty();

        promptStringBuilder.append("\nAvailable actions:\n");

        for (int i = 0; i < availableDirections.length; i++) {
            promptStringBuilder.append(i).append(" ").append(availableDirections[i]).append("\n");
        }

        promptStringBuilder.append("Choose an action: ");

        System.out.println(promptStringBuilder);

        try {
            int directionChoice = scanner.nextInt();

            while (directionChoice < 0 || directionChoice >= availableDirections.length) {
                System.out.println("Invalid action.");
                directionChoice = scanner.nextInt();
            }

            return new PlayActionMessage(availableDirections[directionChoice]);
        } catch (Exception e) {
            Log.error("An error occurred while reading user input.");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        scanner.close();
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        Log.info("Match ended. New match begins.");
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        Log.info("RECEIVED REWARD: " + a.getReward());
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        super.receivePlayedMoveMessage(a);
    }
}
