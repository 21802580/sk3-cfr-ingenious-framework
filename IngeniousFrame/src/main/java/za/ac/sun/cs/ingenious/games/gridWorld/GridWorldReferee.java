package za.ac.sun.cs.ingenious.games.gridWorld;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.GameSpeed;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.ui.GridWorldFrame;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldLevelParser;

import java.awt.event.WindowEvent;
import java.util.Map;

/**
 * Referee for GridWorld that handles referee initialization, UI and communicating a player's position after
 * stochastic mechanics have been applied.
 *
 * @author Steffen Jacobs
 */
public class GridWorldReferee extends FullyObservableMovesReferee<GridWorldState, GridWorldLogic, GridWorldFinalEvaluator> {
    private GridWorldRewardEvaluator rewardEval;
    private GameSpeed gameSpeed = GameSpeed.SLOW;
    private GridWorldFrame gridWorldFrame;
    private boolean enabledUI;

    public GridWorldReferee(MatchSetting match, PlayerRepresentation[] players) throws MissingSettingException, IncorrectSettingTypeException {
        super(match, players, new GridWorldState(), null, null);

        GridWorldLevelParser parser = new GridWorldLevelParser(match);

        currentState = new GridWorldState(parser);
        initialState = currentState.deepCopy();
        logic = new GridWorldLogic(parser);
        rewardEval = new GridWorldRewardEvaluator(parser.getRewards(), parser.getStepReward());
        eval = new GridWorldFinalEvaluator(parser.getRewards());

        gridWorldFrame = new GridWorldFrame(currentState);
        gridWorldFrame.getPanel().updateState(currentState, logic);

        showUI();
        enabledUI = true;
    }

    public void showUI() {
        gridWorldFrame.setVisible(true);
    }

    public void disposeUI() {
        gridWorldFrame.dispose();
        this.gameSpeed = GameSpeed.REALTIME;
        enabledUI = false;
    }

    public void setGameSpeed(GameSpeed gameSpeed) {
        this.gameSpeed = gameSpeed;
    }

    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        return new MatchSettingsInitGameMessage(matchSettings);
    }

    @Override
    protected void reactToValidAction(int player, PlayActionMessage m) {
        Action action = m.getAction();
        if (action instanceof CompassDirectionAction) {
            // Get resulting position from the player's choice of direction.
            Coord playerPosition = currentState.findPlayerCoords(player);
            Coord finalPosition = logic.getAfterStateDestination(currentState, playerPosition);

            if (playerPosition.equals(finalPosition)) return;

            Action newAction = new XYAction(finalPosition.getX(), finalPosition.getY(), player);

            distributeAcceptedAction(new PlayActionMessage(newAction));
            logic.makeMove(currentState, newAction);
        }
    }

    @Override
    protected void afterRound(Map<Integer, PlayActionMessage> messages) {
        super.afterRound(messages);

        if (enabledUI)
            gridWorldFrame.getPanel().updateState(currentState, logic);

        try {
            Thread.sleep(gameSpeed.getValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void reset() {
        super.reset();

        if (enabledUI)
            gridWorldFrame.getPanel().updateState(currentState, logic);
    }

    @Override
    protected double[] calculatePlayerRewards() {
        return rewardEval.getReward(currentState);
    }
}
