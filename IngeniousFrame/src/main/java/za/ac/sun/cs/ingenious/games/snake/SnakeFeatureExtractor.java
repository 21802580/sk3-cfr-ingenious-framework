package za.ac.sun.cs.ingenious.games.snake;

import com.esotericsoftware.minlog.Log;
import net.coobird.thumbnailator.Thumbnails;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;
import za.ac.sun.cs.ingenious.games.snake.ui.SnakeFrame;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.LinkedList;

/**
 * Implementation of a feature extractor for Snake that pulls images of the game state and applies any pre-processing.
 *
 * @author Steffen Jacobs
 */
public class SnakeFeatureExtractor implements StateFeatureExtractor<SnakeState, SnakeLogic> {
    private static final int NUMBER_OF_CHANNELS = 2;

    public SnakeFeatureExtractor() {
    }

    @Override
    public double[][][] buildTensor(SnakeState state, SnakeLogic logic) {
        LinkedList<BufferedImage> images = state.getImageBuffer();

        double[][][] tensor = new double[NUMBER_OF_CHANNELS][][];

        try {
            for (int i = 0; i < NUMBER_OF_CHANNELS; i++) {
                if (i >= images.size()) break;
                tensor[i] = convertImage(images.get(i));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tensor;
    }

    private double[][] convertImage(BufferedImage bufferedImage) throws IOException {
        BufferedImage convertedBufferedImage = Thumbnails.of(bufferedImage)
                .imageType(BufferedImage.TYPE_BYTE_GRAY)
                .forceSize(84, 84)
                .asBufferedImage();

        Raster raster = convertedBufferedImage.getRaster();

        double[][] rawImage = new double[convertedBufferedImage.getHeight()][convertedBufferedImage.getWidth()];

        for (int i = 0; i < rawImage.length; i++) {
            for (int j = 0; j < rawImage[0].length; j++) {
                rawImage[i][j] = Math.floor(raster.getPixel(j, i, (double[]) null)[0] / 255.0 * 1000) / 1000;
            }
        }

        return rawImage;
    }

    private void logConvertedFile(BufferedImage image) {
        File outputfile = new File("snake_scaled.jpg");
        try {
            ImageIO.write(image, "jpg", outputfile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
