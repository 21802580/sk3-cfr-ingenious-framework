package za.ac.sun.cs.ingenious.games.cardGames.core.actions;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;

/**
 * The Class CardMove.
 *
 * The abstract class PlayCardMove is a just superclass for different card moves.
 *
 * @param <Location> The generic location enum.
 *
 * @author Joshua Wiebe
 */
public abstract class CardAction<Location extends Enum<Location>> implements Action{

	/** The Constant serialVersionUID. */
	protected static final long serialVersionUID = 1L;

	/** player needed to verify if it's player's turn. */
	protected int player;

	/** The card to move. */
	protected Card card;

	/** The old location. */
	protected Location oldLoc;

	/** The new location. */
	protected Location newLoc;

	/**
	 * Instantiates a new card move.
	 *
	 * @param player the player
	 * @param card the card
	 * @param oldLoc the old location
	 * @param newLoc the new location
	 */
	public CardAction(int player, Card card, Location oldLoc, Location newLoc){
		this.card = card;
		this.oldLoc = oldLoc;
		this.newLoc = newLoc;
		this.player = player;
	}

	/**
	 * Gets the player.
	 *
	 * @return the player
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	/**
	 * Gets the card.
	 *
	 * @return the card
	 */
	public Card getCard() {
		return card;
	}

	/**
	 * Gets the old location.
	 *
	 * @return the old location
	 */
	public Location getOldLoc() {
		return oldLoc;
	}

	/**
	 * Gets the new location.
	 *
	 * @return new location
	 */
	public Location getNewLoc() {
		return newLoc;
	}

	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other){
		if(other == null){return false;}
		if(other == this){return true;}
		if(!(other instanceof CardAction)){return false;}

		CardAction<Location> otherPCM = (CardAction<Location>) other;
		if(this.card.equals(otherPCM.getCard()) && this.player == otherPCM.getPlayerID() &&
				this.oldLoc.equals(otherPCM.getOldLoc()) && this.newLoc.equals(otherPCM.getNewLoc())){
			return true;
		}
		return false;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString(){
		return ("Move " + this.card.toString() + " from " + this.oldLoc + " to " + this.newLoc + ".");
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
