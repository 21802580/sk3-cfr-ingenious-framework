package za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.persistent.misc.PCopyOnWriteArray;
import za.ac.sun.cs.ingenious.core.util.persistent.tries.PBitOrderedTrie;

/**
 * An engine for the MNK game, using the SSS* search with functional data structures.
 * Can only work with perfect information games.
 * 
 * @author Nicholas Robinson
 */
public class MNKSSSEngine extends Engine {

    ExecutorService executor;
    MNKSSSStrategy strategy;
    int threadCount;

    PVector<Integer> board;
    moveGeneratorFunction moveGenerator;

    MatchSetting engineSettings;

    public MNKSSSEngine(EngineToServerConnection toServer) {
        super(toServer);
    }

    public MNKSSSEngine(EngineToServerConnection toServer, String config, int threadCount) throws IOException, MissingSettingException, IncorrectSettingTypeException {
        super(toServer);

        engineSettings =  new MatchSetting(config);
        this.threadCount = threadCount;
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MNKSSSEngine";
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        executor.shutdown();
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        MatchSetting gameSettings = ((MatchSettingsInitGameMessage) a).getSettings();
        try {
            int m = gameSettings.getSettingAsInt("mnk_width");
            int n = gameSettings.getSettingAsInt("mnk_height");
            int k = gameSettings.getSettingAsInt("mnk_k");

            switch (engineSettings.getSettingAsString("game_state_data_structure").toLowerCase()) {
                case "array":
                    board = new PCopyOnWriteArray<Integer>(m * n);
                    break;
                case "pbitorderedtrie":
                    board = new PBitOrderedTrie<Integer>();
                    break;
            }

            int depthLimit = engineSettings.getSettingAsInt("depth_limit");
            int partitionSize = engineSettings.getSettingAsInt("partition_size");

            executor = Executors.newFixedThreadPool(threadCount);

            switch (engineSettings.getSettingAsString("strategy").toLowerCase()) {
                case "standard":
                    strategy = new MNKSSSStandardStrategy(m, n, k);
                    moveGenerator = () -> ((MNKSSSStandardStrategy)strategy).generateMove(playerId2playerToken(playerID), board, depthLimit, executor, threadCount, partitionSize);
                    break;
                case "beamsearch":
                    strategy = new MNKSSSBeamSearchStrategy(m, n, k);
                    int expansionLimit = engineSettings.getSettingAsInt("expansion_limit");
                    moveGenerator = () -> ((MNKSSSBeamSearchStrategy)strategy).generateMove(playerId2playerToken(playerID), board, depthLimit, expansionLimit, executor, threadCount, partitionSize);
                    break;
            }

        } catch (MissingSettingException | IncorrectSettingTypeException e) {
            Log.error("Could not read settings from received MatchSetting object, message: " + e.getMessage());
        }
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        
        Move moveAction = a.getMove();
        if (moveAction instanceof XYAction) {
            XYAction xya = (XYAction) moveAction;

            int move = xyAction2Move((XYAction)a.getMove());
            Integer token = playerId2playerToken(xya.getPlayerID());

            board = board.set(move, token);

        }
        else {
            Log.info("Received unknown move type");
        }
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        int m = moveGenerator.generate();

        Log.info("Player " + playerId2playerToken(playerID) + " plays " + m);
        Log.info(strategy.stringifyBoard(board.set(m, 0)));

        return new PlayActionMessage(move2XYAction(m, playerID));
    }

    @FunctionalInterface
    interface moveGeneratorFunction {
        public int generate();
    }

    Integer playerId2playerToken(int id) {
        return id + 1;
    }

    XYAction move2XYAction(int move, int playerID) {
        int x = move % strategy.n;
        int y = move / strategy.m;
        return new XYAction(x, y, playerID);
    }

    int xyAction2Move(XYAction action) {
        return strategy.m * action.getY() + action.getX();
    }

}
