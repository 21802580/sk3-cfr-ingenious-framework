package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldFinalEvaluator;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldRewardEvaluator;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.dp.ValueIteration;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Value Iteration engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldValueIterationEngine extends GridWorldEngine {
    /**
     * Value iteration algorithm used to solve the MDP.
     */
    private ValueIteration<GridWorldState, MDPLogic<GridWorldState>, GridWorldRewardEvaluator> alg;

    private MDPState<GridWorldState> mdpState;

    public GridWorldValueIterationEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new ValueIteration<>();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldValueIterationEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        MDPLogic<GridWorldState> mdpLogic = MDPLogic.fromGameLogic(state.deepCopy(), logic);

        // Since this can't be set at time of algorithm initialization, doing it here. These are important.
        alg.setStates(mdpLogic.getStates());
        alg.setLogic(mdpLogic);
        alg.setEvaluator(new GridWorldRewardEvaluator(parser.getRewards(), parser.getStepReward()));
        alg.initializeValueTable();
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(state, a.getMove());

    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        alg.solve();
        Log.info("Solved! Choosing action...");

        Action choice = alg.chooseActionForState(state);
        Log.info("Action chosen: " + choice);

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        Log.info("Solved policy:");

        Map<GameState, Action> policy = new HashMap<>();

        for (MDPState<GridWorldState> state : alg.getStates()) {
            policy.put(state.getGameState(), alg.chooseActionForState(state.getGameState()));
        }

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicy(state, policy));

        Map<MDPState<GridWorldState>, Double> valueFunction = alg.getValueTable();

        // Quick and dirty map from Map<MDPState, Double> to Map<GameState, Double>.
        Map<GameState, Double> mappedValueFunction = valueFunction.keySet()
                .stream()
                .collect(Collectors.toMap(MDPState<GridWorldState>::getGameState, valueFunction::get));

        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicy(state, mappedValueFunction));
    }

    public ValueIteration<GridWorldState, MDPLogic<GridWorldState>, GridWorldRewardEvaluator> getAlg() {
        return alg;
    }
}
