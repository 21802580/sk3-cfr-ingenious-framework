package za.ac.sun.cs.ingenious.games.tictactoe;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;

/**
 * The referee acts as the "game master". It handles communication with the clients and
 * updates the server's game state. In the case of TicTacToe, we can use one of the helper
 * classes provided by the framework, as we do not need extra functionality.
 */
public class TTTReferee extends FullyObservableMovesReferee<TurnBasedSquareBoard, TTTLogic, TTTFinalEvaluator> {

	/**
	 * The required constructor for the RefereeFactory
	 * @param match contains the match settings from the MatchSettingsFile
	 * @param players an array of player representations (either local or TCP)
	 */
	public TTTReferee(MatchSetting match, PlayerRepresentation[] players) throws MissingSettingException, IncorrectSettingTypeException {
		// The superclass can be used for many different games and just needs to have the
		// initial game state, the game logic and the final evaluator. Then it operates on
		// the methods defined in those interfaces.
		super(match, players, new TurnBasedSquareBoard(3,0,2), new TTTLogic(), new TTTFinalEvaluator());
	}

}
