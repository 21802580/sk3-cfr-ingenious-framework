package za.ac.sun.cs.ingenious.core.network.external.messages;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Wraps a serialized `GameState` to be sent to the Python application.
 *
 * @author Steffen Jacobs
 */
public class ExternalState {
    private String type;
    private GameState data;

    public ExternalState(GameState data) {
        this.type = data.getClass().getSuperclass().getSimpleName();
        this.data = data;
    }

    public ExternalState(GameState data, String type) {
        this.type = type;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GameState getData() {
        return data;
    }

    public void setData(GameState data) {
        this.data = data;
    }
}
