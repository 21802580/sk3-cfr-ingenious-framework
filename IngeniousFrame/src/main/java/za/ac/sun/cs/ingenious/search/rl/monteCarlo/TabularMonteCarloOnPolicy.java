package za.ac.sun.cs.ingenious.search.rl.monteCarlo;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Reward;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.util.EpisodeStep;
import za.ac.sun.cs.ingenious.search.rl.util.ReinforcementLearningInstrumentation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Class containing an implementation of Monte Carlo that uses solves for an optimal policy. When actions are chosen,
 * there is a non-zero probability (epsilon) that a random action will be chosen in order to ensure that the agent
 * continues to explore.
 *
 * @param <S>
 * @param <L>
 * @author Steffen Jacobs
 */
public class TabularMonteCarloOnPolicy<S extends GameState, L extends GameLogic<S>> extends ReinforcementLearningInstrumentation {
    private static final double DEFAULT_EPSILON = 0.5;
    private static final double DEFAULT_GAMMA = 1.0;
    private static final double INITIAL_RETURN = 0.0;
    private static final double INITIAL_Q_VALUE = 0.0;

    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_WIDTH = 5;
    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_HEIGHT = 5;

    /**
     * Game logic in order to retrieve list of available actions for any given state.
     */
    L logic;

    /**
     * ID of the player using this MC instance.
     */
    int playerID;

    /**
     * Random number generator.
     */
    private Random rng;

    /**
     * Probability of taking a random action.
     */
    private double epsilon;

    /**
     * Discount factor.
     */
    private double gamma;

    /**
     * Policy being improved on.
     */
    private Map<GameState, Map<Action, Double>> policy;

    /**
     * Table containing averaged returns for state-action pairs.
     */
    private Map<GameState, Map<Action, Double>> qTable;

    /**
     * List of returns for each state-action pair.
     */
    private Map<GameState, Map<Action, Collection<Double>>> returns;

    /**
     * List of episode steps in the current episode.
     */
    private List<EpisodeStep> steps;

    public TabularMonteCarloOnPolicy() {
        rng = new Random();
        epsilon = DEFAULT_EPSILON;
        gamma = DEFAULT_GAMMA;

        policy = new HashMap<>();
        qTable = new HashMap<>();
        returns = new HashMap<>();
        steps = new ArrayList<>();
    }

    public TabularMonteCarloOnPolicy(String identifier, double epsilon, double gamma) {
        super(identifier);

        rng = new Random();
        this.epsilon = epsilon;
        this.gamma = gamma;

        policy = new HashMap<>();
        qTable = new HashMap<>();
        returns = new HashMap<>();
        steps = new ArrayList<>();
    }

    public TabularMonteCarloOnPolicy<S, L> withEpsilon(double epsilon) {
        this.epsilon = epsilon;
        return this;
    }

    public TabularMonteCarloOnPolicy<S, L> withGamma(double gamma) {
        this.gamma = gamma;
        return this;
    }

    public TabularMonteCarloOnPolicy<S, L> withRandom(Random random) {
        this.rng = random;
        return this;
    }

    public TabularMonteCarloOnPolicy<S, L> withLogic(L logic) {
        this.setLogic(logic);
        return this;
    }

    public TabularMonteCarloOnPolicy<S, L> withPolicy(Map<GameState, Map<Action, Double>> policy) {
        this.policy = policy;
        return this;
    }

    public void setLogic(L logic) {
        this.logic = logic;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public void setSteps(List<EpisodeStep> steps) {
        this.steps = steps;
    }

    public void setReturns(Map<GameState, Map<Action, Collection<Double>>> returns) {
        this.returns = returns;
    }

    public List<EpisodeStep> getSteps() {
        return steps;
    }

    public Map<GameState, Map<Action, Double>> getPolicy() {
        return policy;
    }

    public Map<GameState, Map<Action, Double>> getQTable() {
        return qTable;
    }

    public void beginNewEpisode() {
        steps = new ArrayList<>();
    }

    /**
     * Chooses an action for the given state according to the policy. Adds an episode step to the list, containing the
     * state and chosen action.
     *
     * @param state State from which an action must be chosen.
     * @return An action choice.
     */
    public Action chooseActionFromPolicy(S state) {
        incrementStep();

        GameState clonedState = state.deepCopy();
        performLazyPolicyInitialization(clonedState);

        Action choice = null;
        try {
            choice = ClassUtils.chooseFromDistribution(policy.get(clonedState), rng);
        } catch (IncorrectlyNormalizedDistributionException e) {
            e.printStackTrace();
        }

        steps.add(new EpisodeStep(clonedState, choice));

        return choice;
    }

    /**
     * Chooses an action for the given state according to the policy, ignoring exploration.
     *
     * @param state State from which an action must be chosen.
     * @return An action choice.
     */
    public Action chooseBestActionFromPolicy(S state) {
        GameState clonedState = state.deepCopy();
        performLazyPolicyInitialization(clonedState);

        Action best = null;
        double bestProb = 0.0;

        Set<Action> actions = policy.get(clonedState).keySet();
        for (Action a : actions) {
            if (policy.get(clonedState).get(a) > bestProb) {
                best = a;
                bestProb = policy.get(clonedState).get(a);
            }
        }

        return best;
    }

    /**
     * Sets the reward for the most recent episode step. This is necessary since there is a delay between performing
     * the action and receiving the corresponding reward.
     *
     * @param reward Reward to set.
     */
    public void setMostRecentReward(Reward reward) {
        addReward(((ScalarReward) reward).getReward());
        steps.get(steps.size() - 1).setReward(reward);
    }

    /**
     * Performs lazy initialization of the policy for the given state. All action probabilities for the state are
     * initialized with equal probability.
     *
     * @param state State for which the policy is initialized.
     */
    public void performLazyPolicyInitialization(GameState state) {
        List<Action> availableActions = logic.generateActions((S)state, playerID);

        if (!policy.containsKey(state))
            policy.put(state, new HashMap<>());
        if (policy.get(state).keySet().size() == 0) {
            for (Action a : availableActions) {
                policy.get(state).put(a, 1.0 / availableActions.size());
            }
        }
    }

    public void update() {
        totalEpisodes++;
        double g = 0;

        for (int t = steps.size() - 1; t >= 0; t--) {
            EpisodeStep currentStep = steps.get(t);
            GameState state = currentStep.getState();
            Action action = currentStep.getAction();
            ScalarReward reward = (ScalarReward) currentStep.getReward();

            // Some lazy initialization of maps.
            performLazyInitialization(state, action, reward);

            // Update g with new reward.
            g = gamma * g + reward.getReward();

            // If this state-action pair occurs in a previous time step, it will be handled in a later iteration.
            if (stateActionOccursInPreviousStep(state, action, t)) continue;

            returns.get(state).get(action).add(g);

            // Update Q Table
            qTable.get(state).put(action, averageReturns(state, action));

            Action greedyAction = greedyActionByQ(state);
            List<Action> availableActionsForState = logic.generateActions((S) state, playerID);
            for (Action a : availableActionsForState) {
                if (a.equals(greedyAction)) {
                    policy.get(state).put(a, 1 - epsilon + (epsilon / availableActionsForState.size()));
                } else {
                    policy.get(state).put(a, epsilon / availableActionsForState.size());
                }
            }
        }

//        printPolicy();
//        printPolicyGrid(MDP_GRID_WIDTH, MDP_GRID_HEIGHT);
    }

    /**
     * Performs lazy initialization of maps for the specified state, action and reward.
     *
     * @param state State.
     * @param action Action.
     * @param reward Reward.
     */
    private void performLazyInitialization(GameState state, Action action, Reward reward) {
        if (!returns.containsKey(state))
            returns.put(state, new HashMap<>());

        if (!returns.get(state).containsKey(action))
            returns.get(state).put(action, new ArrayList<>());

        if (!qTable.containsKey(state))
            qTable.put(state, new HashMap<>());

        if (!qTable.get(state).containsKey(action))
            qTable.get(state).put(action, INITIAL_Q_VALUE);

        if (!policy.containsKey(state))
            policy.put(state, new HashMap<>());
    }

    /**
     * Calculates the average return for the specified state-action pair.
     *
     * @param state State.
     * @param action Action.
     * @return Average return.
     */
    public double averageReturns(GameState state, Action action) {
        double totalReturn = 0;
        for (double r : returns.get(state).get(action)) {
            totalReturn += r;
        }

        return totalReturn / returns.get(state).get(action).size();
    }

    /**
     * Checks if the specified state-action pair appears in a time step before currentT.
     *
     * @param state State in state-action pair.
     * @param action Action in state-action pair.
     * @param currentT Current time step.
     * @return True if state-action pair occurs in a previous time step, false otherwise.
     */
    public boolean stateActionOccursInPreviousStep(GameState state, Action action, int currentT) {
        for (int t = currentT - 1; t >= 0; t--) {
            EpisodeStep currentStep = steps.get(t);
            GameState currentStepState = currentStep.getState();
            Action currentStepAction = currentStep.getAction();

            if (currentStepState.equals(state) && currentStepAction.equals(action))
                return true;
        }

        return false;
    }

    /**
     * Retrieves an available action for the specified state by choosing the action with the highest Q value. Ties are
     * broken randomly.
     *
     * @param state State dictating the available actions.
     * @return An action with the highest Q value.
     */
    private Action greedyActionByQ(GameState state) {
        double bestQ = -Double.MAX_VALUE;
        List<Action> bestActions = new ArrayList<>();
        Map<Action, Double> qs = qTable.get(state);

        // Iterate over actions and get the actions with the best value.
        for (Action a : logic.generateActions((S) state, playerID)) {
            if (!qs.containsKey(a)) qs.put(a, INITIAL_Q_VALUE);
            double q = qs.get(a);

            if (q > bestQ) {
                bestQ = q;
                bestActions = new ArrayList<>();
                bestActions.add(a);
            } else if (q == bestQ) {
                bestActions.add(a);
            }
        }

        // If one action has the best value, it is chosen.
        if (bestActions.size() == 1) {
            return bestActions.get(0);
        }

        // If multiple actions are tied, choose randomly between them.
        return bestActions.get(rng.nextInt(bestActions.size()));
    }

    private void printPolicy() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        Set<GameState> states = policy.keySet();
        for (GameState state : states) {
            sb.append(state).append("\n");
            Set<Action> actions = policy.get(state).keySet();

            for (Action action : actions) {
                sb.append("\t").append(action).append(" : ").append(policy.get(state).get(action)).append("\n");
            }
        }

        sb.append("\n");
        Log.info(sb);
    }

    /**
     * This method is rather hardcoded, but for the moment is used to print gridworld MDPs.
     *
     * @param w
     * @param h
     */
    public void printPolicyGrid(int w, int h) {
        Log.info("Q Table: \n");
        Set<GameState> keySet = policy.keySet();

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Map<Action, Double> qs = policy.get(state);

                if (qs == null) {
                    sb.append("0.00\t");
                    continue;
                }

                Set<Action> actionSet = qs.keySet();
                if (actionSet.size() == 0) {
                    sb.append("0.00\t");
                    continue;
                }

                Action maxA = chooseBestActionFromPolicy((S)state);
                sb.append(df.format(policy.get(state).get(maxA))).append("\t");
            }
            sb.append("\n\n");
        }

        sb.append("-------------------------\n\n");

        k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Map<Action, Double> qs = policy.get(state);

                if (qs == null) {
                    sb.append("• ");
                    continue;
                }

                Set<Action> actionSet = qs.keySet();
                if (actionSet.size() == 0) {
                    sb.append("• ");
                    continue;
                }

                Action maxA = chooseBestActionFromPolicy((S)state);

                switch (((DiscreteAction) maxA).getActionNumber()) {
                    case 0:
                        sb.append("↑ ");
                        break;
                    case 1:
                        sb.append("→ ");
                        break;
                    case 2:
                        sb.append("↓ ");
                        break;
                    case 3:
                        sb.append("← ");
                        break;
                }
            }
            sb.append("\n");
        }

        sb.append("\n\n");

        Log.info(sb);
        Log.info("==========================");
    }
}
