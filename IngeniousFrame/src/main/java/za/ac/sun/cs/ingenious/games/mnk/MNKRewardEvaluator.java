package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;

/**
 * Reward evaluator for MNK that derives rewards from.
 *
 * @author Steffen Jacobs
 */
public class MNKRewardEvaluator implements GameRewardEvaluator<MNKState> {
    private final double DRAW_REWARD = 0.0;
    private final double STEP_REWARD = 0.0;
    private final double WIN_REWARD = 1.0;
    private final double LOSE_REWARD = -1.0;

    MNKFinalEvaluator eval;

    public MNKRewardEvaluator(MNKFinalEvaluator evaluator) {
        eval = evaluator;
    }

    @Override
    public double[] getReward(MNKState state) {
        double[] rewards = new double[state.getNumPlayers()];

        int winnerOrGameStatus = eval.getWinner(state);

        if (winnerOrGameStatus == MNKFinalEvaluator.DRAW_RESULT) {
            for (int i = 0; i < rewards.length; i++) {
                rewards[i] = DRAW_REWARD;
            }
        } else if (winnerOrGameStatus == MNKFinalEvaluator.NON_TERMINAL_RESULT) {
            for (int i = 0; i < rewards.length; i++) {
                rewards[i] = STEP_REWARD;
            }
        } else {
            for (int i = 0; i < rewards.length; i++) {
                if (winnerOrGameStatus == i) {
                    // Win reward
                    rewards[i] = WIN_REWARD;
                } else {
                    // Lose reward
                    rewards[i] = LOSE_REWARD;
                }
            }
        }

        return rewards;
    }
}
