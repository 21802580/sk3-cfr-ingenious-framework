package za.ac.sun.cs.ingenious.games.mnk;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;

/**
 * Note: in the perfect information setting, all actions take place on a single board, which has index 0.
 */
public class MNKState extends TurnBasedGameState {

	private int k;
	private boolean perfectInformation;

	public List<TurnBased2DBoard> playerBoards;
	public List<List<Move>> moveHistories;

	public MNKState(int height, int width, int k, boolean perfectInformation, int numPlayers) {
		super(0, numPlayers);
		this.k = k;
		this.perfectInformation = perfectInformation;
		playerBoards = new ArrayList<TurnBased2DBoard>();
		moveHistories = new ArrayList<List<Move>>();
		for (int i = 0; i < numPlayers; i++) {
			if (!(perfectInformation && i!=0)) { // In the perfect information setting, add only one board
				playerBoards.add(new TurnBased2DBoard(height, width, 0, numPlayers));
			}
			moveHistories.add(new ArrayList<Move>());
		}
	}

	/**
	 * Copy constructor. Duplicates the given state.
	 */
	public MNKState(MNKState toCopy) {
		super(toCopy.nextMovePlayerID, toCopy.numPlayers);
		this.k = toCopy.getK();
		this.perfectInformation = toCopy.isPerfectInformation();
		this.playerBoards = new ArrayList<TurnBased2DBoard>();
		this.moveHistories = new ArrayList<List<Move>>();
		for (TurnBased2DBoard b : toCopy.playerBoards) {
			this.playerBoards.add((TurnBased2DBoard) b.deepCopy());
		}
		for (List<Move> l : toCopy.moveHistories) {
			moveHistories.add(new ArrayList<Move>());
			for (Move m : l) {
				moveHistories.get(moveHistories.size()-1).add(m);
			}
		}
	}

	@Override
	public MNKState deepCopy() {
		return new MNKState(this);
	}

	@Override
	public void printPretty() {
		Log.info(this.toString());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for (int i = 0; i < numPlayers; i++) {
			sb.append("Player ").append(i).append("'s move history as I see it:").append("\n");
			sb.append(moveHistories.get(i)).append("\n");
		}
		if (perfectInformation) {
			sb.append("Game board:").append("\n");
			sb.append(playerBoards.get(0).toString()).append("\n");
		} else {
			for (int i = 0; i < playerBoards.size(); i++) {
				sb.append("Player ").append(i).append("'s board:").append("\n");
				sb.append(playerBoards.get(i).toString()).append("\n");
			}
		}

		return sb.toString();
	}

	public int getK(){
		return this.k;
	}

	public boolean isPerfectInformation() {
		return perfectInformation;
	}

	public int getWidth() {
		return this.playerBoards.get(0).getWidth();
	}

	public int getHeight() {
		return this.playerBoards.get(0).getHeight();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + k;
		result = prime * result + (perfectInformation ? 1231 : 1237);
		result = prime * result + ((playerBoards == null) ? 0 : playerBoards.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		MNKState other = (MNKState) obj;
		if (k != other.k)
			return false;
		if (perfectInformation != other.perfectInformation)
			return false;
		if (playerBoards == null) {
			if (other.playerBoards != null)
				return false;
		} else if (!playerBoards.equals(other.playerBoards))
			return false;
		return true;
	}
	
}
