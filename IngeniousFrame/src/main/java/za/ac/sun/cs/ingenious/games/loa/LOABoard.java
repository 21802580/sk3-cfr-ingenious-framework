package za.ac.sun.cs.ingenious.games.loa;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.games.loa.movegeneration.LineConfiguration;
import za.ac.sun.cs.ingenious.games.loa.movegeneration.MoveGenerator;
import za.ac.sun.cs.ingenious.games.loa.quad.QuadTable;
import za.ac.sun.cs.ingenious.games.loa.util.*;

import java.util.HashSet;
import java.util.LinkedList;


/**
 * TODO: Remove Colour[][] state. Add colour property to Piece (then capturing involves changing the colour property)
 */
// Object size ~= 4.39062Kb
public class LOABoard extends TurnBasedGameState {
    public static final long startStateHash = 8504895456016848759L;
    private Colour[][] state;
    private Colour currentPlayer;
    private Piece[][] pieces;
    private PieceSet whitePieces;
    private PieceSet blackPieces;
    private QuadTable quadTable;
    private LineConfiguration lineConfiguration;

    public LOABoard() {
        super(Colour.BLACK.toInt(), 2);
        currentPlayer = Colour.BLACK;
        whitePieces = new PieceSet();
        blackPieces = new PieceSet();
        quadTable = new QuadTable();
        initialiseBoard();
        lineConfiguration = new LineConfiguration(state);
    }

    public LOABoard(LOABoard b) {
        super(b.currentPlayer.toInt(), 2);
        state = new Colour[8][8];
        pieces = new Piece[8][8];
        whitePieces = new PieceSet();
        blackPieces = new PieceSet();
        for (int i = 0; i < state.length; i++) {
            for (int j = 0; j < state.length; j++){
                state[i][j] = b.state[i][j];
                if (state[i][j] == Colour.EMPTY){
                    pieces[i][j] = null;
                } else if (state[i][j] == Colour.BLACK){
                    pieces[i][j] = new Piece(b.pieces[i][j]);
                    blackPieces.add(pieces[i][j]);
                } else {
                    pieces[i][j] = new Piece(b.pieces[i][j]);
                    whitePieces.add(pieces[i][j]);
                }
            }
        }
        currentPlayer = b.currentPlayer;
        quadTable = new QuadTable(b.quadTable);
        lineConfiguration = new LineConfiguration(state);
    }

    public Colour[][] getState(){
        return state;
    }

    public LineConfiguration getLineConfiguration(){
        return lineConfiguration;
    }

    public Colour getCurrentPlayer() {
        return currentPlayer;
    }

    public Colour pieceAt(LOACoord c) {
        return state[c.getRow()][c.getCol()];
    }

    public QuadTable getQuadTable() {
        return quadTable;
    }

    public PieceSet getPiecesFor(Colour colour){
        if (colour == Colour.BLACK){
            return blackPieces;
        }
        return whitePieces;
    }

    public Colour getPendingPlayer(){
        if (currentPlayer == Colour.BLACK){
            return Colour.WHITE;
        }
        return Colour.BLACK;
    }

    public void applyMove(LOAMove m) {
        int rf = m.getFrom().getRow();
        int cf = m.getFrom().getCol();
        int rt = m.getTo().getRow();
        int ct = m.getTo().getCol();
        boolean capture = m.isCapture();
        quadTable.applyOrUndoMove(m, state[rf][cf]);
        lineConfiguration.applyMove(m, state[rf][cf]);
        state[rt][ct] = currentPlayer;
        state[rf][cf] = Colour.EMPTY;
        Piece srcPiece = pieces[rf][cf];
        Piece destPiece = pieces[rt][ct];
        srcPiece.setPos(rt, ct);
        pieces[rt][ct] = srcPiece;
        pieces[rf][cf] = null;
        if (capture){
            Colour opponent = getPendingPlayer();
            PieceSet opponentPieces = getPiecesFor(opponent);
            opponentPieces.remove(destPiece);
        }
        currentPlayer = getPendingPlayer();
    }

    public LOAMove undoMove(LOAMove m) {
        int rf = m.getFrom().getRow();
        int cf = m.getFrom().getCol();
        int rt = m.getTo().getRow();
        int ct = m.getTo().getCol();
        boolean capture = m.isCapture();
        Colour undoingPlayer = state[rt][ct];
        Colour otherPlayer = undoingPlayer.getOpponent();
        quadTable.applyOrUndoMove(m, undoingPlayer);
        lineConfiguration.undoMove(m, undoingPlayer);
        state[rf][cf] = undoingPlayer;
        state[rt][ct] = Colour.EMPTY;
        Piece movingPiece = pieces[rt][ct];
        movingPiece.setPos(rf, cf);
        pieces[rf][cf] = movingPiece;
        pieces[rt][ct] = null;
        if (capture){
            PieceSet capturedPieces = getPiecesFor(otherPlayer);
            state[rt][ct] = otherPlayer;
            Piece newPiece = new Piece(rt, ct);
            capturedPieces.add(newPiece);
            pieces[rt][ct] = newPiece;
        }
        currentPlayer = undoingPlayer;
        return m;
    }

    // Initialise the board with the starting LOA position
    private void initialiseBoard() {
        int boardSize = 8;
        state = new Colour[boardSize][boardSize];
        pieces = new Piece[8][8];
        for (int i = 0; i < boardSize; i++) {
            for (int j = 0; j < boardSize; j++) {
                if (((i == 0) || (i == boardSize - 1)) && (j != 0) && (j != boardSize - 1)) {
                    state[i][j] = Colour.BLACK;
                    Piece p = new Piece(i, j);
                    pieces[i][j] = p;
                    blackPieces.add(p);
                } else if (((j == 0) || (j == boardSize - 1)) && (i != 0) && (i != boardSize - 1)) {
                    state[i][j] = Colour.WHITE;
                    Piece p = new Piece(i, j);
                    pieces[i][j] = p;
                    whitePieces.add(p);
                } else {
                    state[i][j] = Colour.EMPTY;
                }
            }
        }
    }

    public boolean moveLegal(LOAMove m) {
        return MoveGenerator.hasMove(this, m);
    }

    // Returns the winning player if one exists, the empty player for a draw and null if the board is in a non-terminal state
    public Colour getWinner() {
        if (whitePieces.size() == 1 && blackPieces.size() == 1) {
            // Simultaneous connection. Moving player wins (Winands thesis)
            return getPendingPlayer();
        } else if (whitePieces.size() == 1) {
            return Colour.WHITE;
        } else if (blackPieces.size() == 1) {
            return Colour.BLACK;
        } else if (quadTable.getEuler(Colour.BLACK) > 1.0 && quadTable.getEuler(Colour.WHITE) > 1.0) {
            return null;
        } else {
            boolean bwins = connected(Colour.BLACK);
            boolean wwins = connected(Colour.WHITE);
            if (bwins && wwins) {
                return getPendingPlayer();
            } else if (bwins) {
                return Colour.BLACK;
            } else if (wwins) {
                return Colour.WHITE;
            } else {
                return null;
            }
        }
    }

    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 7; j++) {
                switch (state[i][j]) {
                    case WHITE:
                        s.append("W ");
                        break;
                    case BLACK:
                        s.append("B ");
                        break;
                    case EMPTY:
                        s.append(". ");
                        break;
                }
            }
            switch (state[i][7]) {
                case WHITE:
                    s.append("W\n");
                    break;
                case BLACK:
                    s.append("B\n");
                    break;
                case EMPTY:
                    s.append(".\n");
                    break;
            }
        }
        return s.toString();
    }

    public boolean connected(Colour colour) {
        Piece c = getPiecesFor(colour).iterator().next();
        LinkedList<LOACoord> q = new LinkedList<LOACoord>();
        HashSet<LOACoord> checked = new HashSet<LOACoord>();
        q.add(LOACoord.coords[c.getRow()][c.getCol()]);
        while (!q.isEmpty()) {
            LOACoord current = q.remove();
            int row = current.getRow();
            int col = current.getCol();
            if (state[row][col] == colour) {
                checked.add(current);
                LOACoord n = row > 0 ? LOACoord.coords[row - 1][col] : null;
                LOACoord s = row < state.length - 1 ? LOACoord.coords[row + 1][col] : null;
                LOACoord e = col < state.length - 1 ? LOACoord.coords[row][col + 1] : null;
                LOACoord w = col > 0 ? LOACoord.coords[row][col - 1] : null;
                LOACoord ne = (row > 0) && (col < state.length - 1) ? LOACoord.coords[row - 1][col + 1] : null;
                LOACoord nw = (row > 0) && (col > 0) ? LOACoord.coords[row - 1][col - 1] : null;
                LOACoord se = (col < state.length - 1) && (row < state.length - 1) ? LOACoord.coords[row + 1][col + 1] : null;
                LOACoord sw = (col > 0) && (row < state.length - 1) ? LOACoord.coords[row + 1][col - 1] : null;
                if (n != null && !checked.contains(n)) q.add(n);
                if (s != null && !checked.contains(s)) q.add(s);
                if (e != null && !checked.contains(e)) q.add(e);
                if (w != null && !checked.contains(w)) q.add(w);
                if (ne != null && !checked.contains(ne)) q.add(ne);
                if (nw != null && !checked.contains(nw)) q.add(nw);
                if (se != null && !checked.contains(se)) q.add(se);
                if (sw != null && !checked.contains(sw)) q.add(sw);
            }
        }
        if (colour == Colour.WHITE) {
            return checked.size() == whitePieces.size();
        } else {
            return checked.size() == blackPieces.size();
        }
    }

    @Override
    public void printPretty() {
        Log.info("\n" + this);
    }
}
