package za.ac.sun.cs.ingenious.games.snake;

import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;

/**
 * Reward evaluator for Snake.
 *
 * @author Steffen Jacobs
 */
public class SnakeRewardEvaluator implements GameRewardEvaluator<SnakeState> {
    public static final double FOOD_MULTIPLIER = 10.0;
    public static final double WIN_REWARD = 10.0;
    public static final double LOSE_REWARD = -1.0;
    public static final double STEP_REWARD = -0.0001;

    private SnakeLogic logic;

    public SnakeRewardEvaluator() {
        logic = new SnakeLogic();
    }

    public SnakeRewardEvaluator(SnakeLogic logic) {
        this.logic = logic;
    }

    @Override
    public double[] getReward(SnakeState forState) {

        if (!logic.isTerminal(forState)) {
            // For non-terminal states, we only check if the score has incremented.
            int scoreDiff = forState.getScore() - forState.getPreviousScore();
            return new double[]{(scoreDiff * FOOD_MULTIPLIER) + (STEP_REWARD)};
        } else {
            // For terminal states we need to check if the game has been won or lost.

            if (forState.hasMaximumBodyLength()) {
                return new double[]{WIN_REWARD};
            } else {
                // Assume that if body length isn't max, the game was lost due to collision.
                return new double[]{LOSE_REWARD};
            }
        }
    }
}
