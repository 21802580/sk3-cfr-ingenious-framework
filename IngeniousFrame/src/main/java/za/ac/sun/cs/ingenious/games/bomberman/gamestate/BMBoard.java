package za.ac.sun.cs.ingenious.games.bomberman.gamestate;

import com.esotericsoftware.minlog.Log;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;

/**
 * Game state representation from Bomberman.
 */
public class BMBoard extends GameState {

    public static final int DEFAULT_NUM_PLAYERS = 4; // Default number of players
    public static final int DEFAULT_BOARD_SIZE = 21; // Default board size (number of rows and columns)
    private static final int minSupportedPlayers = 2;
    private static final int maxSupportedPlayers = 4;
	private Player[] players;
	private ArrayList<Bomb> bombs = new ArrayList<>();
	private Node[][] nodes;
	private int round;
	private int[] scores;
    private boolean perfectInformation = false;
    private boolean alternating = false;
    public int alternatingMovingPlayer = 0;
	private BMLogic logic = BMLogic.defaultBMLogic;
    // Cached for efficiency:  TODO - replace java.awt.Point by an immutable alternative to make HashSet usage more sound - see issue 201
    private Set<Point> visibleBombUpgrades = new HashSet<>();
    private Set<Point> visibleBlastUpgrades = new HashSet<>();
	
    // TODO - make the cradle and upgrade factors configurable in match settings - see issue 155
	/**
     * Probability for each non-wall grid square of containing a cradle (before allocation of upgrades in
     * the perfect information case).
	 */
	private final static double cradleFactor = 0.6;
	
	/**
	 * Creates a new board with specified size, number of players and perfect information flag.
     *
	 * @param size
	 */
	public BMBoard(int size, int players, boolean perfectInformation, boolean alternating) {
		this(createGameBoard(size, players), players, perfectInformation, alternating);
	}
	
	/**
	 * Initializes a new board from the given char array for use at start of game.
     *
	 * @param board Should only contain cradles, walls, spaces, bomb and blast upgrades, and players at initial locations (NOT planting bombs or standing on upgrades).
     * @param nPlayers Number of players - two, three, or four
     * @param perfectInformation true if upgrades should be generated at the start of the game.  Engines should set this to false when recreating a board sent at the start of the game.
	 */
	public BMBoard(char[][] board, int nPlayers, boolean perfectInformation, boolean alternating) {	
		super(nPlayers);
        this.perfectInformation = perfectInformation;
        this.alternating = alternating;
		scores = new int[nPlayers];
		players = new Player[nPlayers];
		for (int i = 0; i < players.length; i++) {
			players[i] = new Player(i);
		}
		nodes = setInitialMapContents(board);
        if (perfectInformation) placeUpgrades(logic.bombUpgradeFactor, logic.blastUpgradeFactor); // TODO: this would be better handled by the logic!? See issue 198
	}
	
	/**
	 * Creates a char array representing an initial board with players in each corner.
     *
	 * @param size
     *
	 * @return The char array created.
	 */
	public static char[][] createGameBoard(int size, int nPlayers){
		if(nPlayers < minSupportedPlayers || nPlayers > maxSupportedPlayers){
			Log.error("numPlayers must be between "+ minSupportedPlayers + " and " + maxSupportedPlayers + ", was " + nPlayers);
			throw new IllegalArgumentException("numPlayers must be between "+ minSupportedPlayers + " and " + maxSupportedPlayers + ", was " + nPlayers);
		}
		char[][] board = new char[size][size];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if ((i % 2 == 0 && j % 2 == 0) || i == 0 || j == 0 || i == size-1
						|| j == size-1) {
					board[i][j] = '#';
				} else {
					board[i][j] = new Random().nextDouble() < cradleFactor ? '+' : ' ';
				}
			}
		}
		
        board[1][1] = ('A');
        board[2][1] = (' ');
        board[1][2] = (' ');
        board[size-2][size-2] = ('B');
        board[size-3][size-2] = (' ');
        board[size-2][size-3] = (' ');
        if (nPlayers >= 3) {
			board[size-2][1] = ('C');
			board[size-3][1] = (' ');
			board[size-2][2] = (' ');
        }
		if (nPlayers >= 4){
            board[1][size-2] = ('D');
            board[2][size-2] = (' ');
            board[1][size-3] = (' ');
        }
        // TODO: extend to support more players on the map - see issue 156
		return board;
	}
	
	/**
	 * @param pos
	 * @param dir
     *
	 * @return The neighbouring position of this point in the specified direction.
	 */
    // TODO - refactor into an appropriate method in the core.  See issue 169
	public static Point getPos(Point pos, CompassDirection dir) {
		switch (dir) {
		case N:
			return new Point(pos.x, pos.y - 1);
		case E:
			return new Point(pos.x + 1, pos.y);
		case S:
			return new Point(pos.x, pos.y + 1);
		case W:
			return new Point(pos.x - 1, pos.y);
		}
		return null;
	}	
	
	/**
	 * @param position
	 * @return Whether a (live) player is currently at the specified position.
	 */
	public boolean isPlayer(Point position) {
        return get(position).hasPlayer();
	}

    /**
     * @return the player with the specified player ID.  Returns null for an invalid player ID.
     */
	public Player getPlayer(int i) {
        if (i < 0 || i >= players.length) return null;
		return players[i];
	}
	
    public boolean isAlternating() {
		return alternating;
	}

	/**
     * Returns an array containing the players.
     */
	public Player[] getPlayers() {
		return players;
	}

    /**
     * Returns the player at the specified position if there is one, otherwise null.
     */
	public Player getPlayerAt(Point pos) {
        return get(pos).getPlayer();
	}

    /**
     * Returns the bombs currently on the board.
     */
	public List<Bomb> getBombs() {
		return bombs;
	}
	
    /**
     * Returns the player at the specified position if there is one, otherwise null.
     */
	public Bomb getBombAt(Point pos) {
        return get(pos).getBomb();
	}

    /**
     * Returns the round number
     */
	public int getRound() {
		return round;
	}
	
    /**
     * Increase the round number by one.
     */
	public void incrementRound() {
		round++;
	}

    /**
     * Returns the score of the player with the specified ID
     */
	public int score(int id){
		return getScores()[id];
	}
	
    /**
     * Returns an array of the scores for each player, indexed by the player IDs.
     */
	public int[] getScores() {
		return scores;
	}
	
    /** Returns whether there is a bomb at the specified position */
	public boolean isBomb(Point position) {
		return nodes[position.y][position.x].hasBomb();
	}
	
    /** Returns whether there is a permanent wall at the specified position */
	public boolean isWall(Point position) {
		return nodes[position.y][position.x].isWall();
	}
	
    /** Returns whether there is a cradle at the specified position */
	public boolean isCradle(Point position) {
		return nodes[position.y][position.x].isCradle();
	}

    /** Add a newly discovered bomb upgrade to discovered bomb upgrades */
    protected void addVisibleBombUpgrade(Node n) {
        n.setBombUpgrade(true);
        visibleBombUpgrades.add(n.getLoc());
    }

    /** Add a newly discovered blast upgrade to discovered blast upgrades */
    protected void addVisibleBlastUpgrade(Node n) {
        n.setBlastUpgrade(true);
        visibleBlastUpgrades.add(n.getLoc());
    }

    /** Remove a collected bomb upgrade from visible bomb upgrades */
    protected void removeVisibleBombUpgrade(Node n) {
        n.setBombUpgrade(false);
        visibleBombUpgrades.remove(n.getLoc());
    }

    /** Remove a collected blast upgrade from visible blast upgrades */
    protected void removeVisibleBlastUpgrade(Node n) {
        n.setBlastUpgrade(false);
        visibleBlastUpgrades.remove(n.getLoc());
    }

    /** Returns discovered bomb upgrades */
	public Iterable<Point> getVisibleBombUpgrades() {
		return visibleBombUpgrades;
	}

    /** Returns discovered blast upgrades */
	public Iterable<Point> getVisibleBlastUpgrades() {
		return visibleBlastUpgrades;
	}

	/**
	 * @param pos
	 * @return Whether the provided position is passable, i.e. whether a player should be able to move onto it.
	 */
	public boolean isPassable(Point pos) {
		return nodes[pos.y][pos.x].isPassable();
	}

	@Override
	public void printPretty() {
		char[][] board = asCharArray();
		for (char[] c : board){
			Log.info(new String(c));
		}
	}

	/**
	 * When using this method some information is lost, such as: hidden upgrades, bomb timers if player are still standing on the bomb, "bomb owners", etc.
	 * @return A two-dimensional char array corresponding to the current state as far as possible.
	 */
	public char[][] asCharArray(){
		char[][] board = new char[nodes.length][nodes[0].length];
		for (int i = 0; i < nodes.length; i++) {
			for (int j = 0; j < nodes[0].length; j++) {
				board[i][j] = nodes[i][j].getChar();
			}
		}
		for (int i = 0; i < players.length; i++) {
			if(players[i].isAlive()){
				Point pos = players[i].getPosition();
				if (nodes[pos.y][pos.x].hasBomb()){
					board[pos.y][pos.x] = (char)('a'+i);
				}else{
					board[pos.y][pos.x] = (char)('A'+i);
				}
			}
		}
		return board;
	}

    /** Checks to see if this board and the provided board correspond to the same Bomberman game state. */
    public boolean similar(BMBoard b) {
        printPretty();
        if (b != null) {
            b.printPretty();
        } else {
            Log.error("null target");
        }
        if (b == null) return false;
        if (round != b.round) return false;
        // Check player to act is the same (if not alternating, is always the same)
        if (alternating) {
        	if (!b.alternating) return false;
        	if (alternatingMovingPlayer != b.alternatingMovingPlayer) return false;
        }
        // Check scores are same
        if (scores == null) {
            if (b.scores != null) return false;
        } else {
            if (!Arrays.equals(scores, b.scores)) return false;
        }
        // Check visible bomb upgrades
        if (visibleBombUpgrades == null) {
            if (b.visibleBombUpgrades != null) return false;
        } else {
            if (!visibleBombUpgrades.equals(b.visibleBombUpgrades)) return false;
        }
        // Check visible blast upgrades
        if (visibleBlastUpgrades == null) {
            if (b.visibleBlastUpgrades != null) return false;
        } else {
            if (!visibleBlastUpgrades.equals(b.visibleBlastUpgrades)) return false;
        }
        // Check players are the same
        if (players == null) {
            if (b.players != null) return false;
        } else {
            if (!players.equals(b.players)) {
                if (b.players == null) return false;
                if (players.length != b.players.length) return false;
                for (int i = 0; i < players.length; i++) {
                    if (!players[i].similar(b.players[i])) return false;
                }
            }
        }
        // Check bombs are the same
        if (bombs == null) {
            if (b.bombs != null) return false;
        } else {
            if (!bombs.equals(b.bombs)) {
                if (b.bombs == null) return false;
                if (bombs.size() != b.bombs.size()) return false;
                for (int i = 0; i < bombs.size(); i++) {
                    if (!bombs.get(i).similar(b.bombs.get(i))) return false;
                }
            }
        }
        // Check nodes are the same
        if (nodes == null) {
            if (b.nodes != null) return false;
        } else {
            if (!nodes.equals(b.nodes)) {
                if (b.nodes == null) return false;
                if (nodes.length != b.nodes.length) return false;
                // Check each array in the 2D nodes array is the same
                for (int i = 0; i < nodes.length; i++) {
                    if (!nodes[i].equals(b.nodes[i])) {
                        if (b.nodes[i] == null) return false;
                        if (nodes[i].length != b.nodes[i].length) return false;
                        for (int j = 0; j < nodes[i].length; j++) {
                            if (!nodes[i][j].similar(b.nodes[i][j])) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

	/**
	 * Places a bomb on the board.
     *
	 * @param pos Location for bomb
	 * @param timer Remaining time of bomb
	 * @param radius Blast radius for bomb
     * @param id Player who owns the bomb
	 */
	protected void placeBomb(Point pos, short timer, short radius, int id){
		Bomb b = new Bomb(pos,timer, radius, id);
		bombs.add(b);
		nodes[pos.y][pos.x].setBomb(b);
        if (id >= 0 && id < players.length) {
            players[id].decNofBombs();
        } else {
            Log.warn("Placing bomb with illegal owner");
        }
	}
	
	protected Node[][] getNodes() {
		return nodes;
	}
	
    /**
     * Returns the node in the board corresponding to the specified coordinates.
     */
	protected Node get(Point p){
		return nodes[p.y][p.x];
	}
	
    /**
     * Adjust the score and note that the state has changed for observers
     *
     * @param player Player whose score should change
     * @param delta Change in the score
     */
	protected void adjustScore(Player player, int delta) {
        adjustScore(player, delta, true);
	}

    /**
     * Adjust the score
     *
     * @param player Player whose score should change
     * @param delta Change in the score
     * @param setChanged true if the state must be marked as changed for observers, false otherwise.
     */
	protected void adjustScore(Player player, int delta, boolean setChanged) {
		scores[player.getId()] += delta;
        setChanged();        
	}

	/**
	 * Places random upgrades below cradles.  For perfect information, the cradles are removed.
	 */
	private void placeUpgrades(double probBomb, double probBlast) {
		for (Node[] line : nodes) {
			for (Node n: line) {
				if(n.isCradle()) placeUpgrade(n, probBomb, probBlast);
			}
		}
	}

	/**
	 * Determines whether to place an upgrade on the provided node.
     * Used at initialization for "perfect information" setting, or after exploding bombs
     * in general.  Note that placing an upgrade on a cradle will cause the cradle to be
     * removed.
     *
     * @return Whether an upgrade was placed.
	 */
	protected boolean placeUpgrade(Node n, double probBomb, double probBlast) {
        double p = Math.random();
        if (p < probBomb) {
            n.removeCradle();
            addVisibleBombUpgrade(n);
            return true;
        }
        if (p < probBomb + probBlast) {
            n.removeCradle();
            addVisibleBlastUpgrade(n);
            return true;
        }
        return false;
	}

	/**
	 * Initializes cradle, wall, and space nodes from a char representation.
     *
	 * @param board
     *
	 * @return Board corresponding to provided char array as a Node array. 
	 */
	private Node[][] setInitialMapContents(char[][] board) {
		for (int i = 0; i < players.length; i++){
			players[i].setAlive(false);
		}
		nodes = new Node[board.length][board[0].length];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				if(board[i][j] == '#') {
					nodes[i][j] = new Node(new Point(j, i), false, true);
				} else if (board[i][j] == '+') {
					nodes[i][j] = new Node(new Point(j, i), true, false);
				} else {
					nodes[i][j] = new Node(new Point(j, i), false, false);
                    if (board[i][j] == 'A' || board[i][j] == 'B' || board[i][j] == 'C' || board[i][j] == 'D') {
                        // TODO: Code specific to no more than four players - see issue 156
                        int id = board[i][j]-'A';
                        placePlayer(id, new Point(j, i));
                        players[id].setAlive(true);
                    } else if (board[i][j] == '!' ){
                        addVisibleBlastUpgrade(nodes[i][j]);
                    } else if (board[i][j] == '&' ){
                        addVisibleBombUpgrade(nodes[i][j]);
                    } else if (board[i][j] != ' ') {
                        throw new IllegalArgumentException("Invalid character on board at indices (" + i + "," + j + "): "+ board[i][j]);
                    }
                }
			}
		}
		return nodes;
	}

	/**
	 * Places the the player with the corresponding id the the specified node.
     *
	 * @param id Player ID
	 * @param pos the point to place the player at
	 */
	private void placePlayer(int id, Point pos){
        nodes[pos.y][pos.x].setPlayer(players[id]);
        players[id].setPosition(pos);
	}
}
