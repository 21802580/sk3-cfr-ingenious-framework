package za.ac.sun.cs.ingenious.games.loa.movegeneration;

import za.ac.sun.cs.ingenious.games.loa.LOABoard;
import za.ac.sun.cs.ingenious.games.loa.util.Colour;
import za.ac.sun.cs.ingenious.games.loa.util.LOACoord;
import za.ac.sun.cs.ingenious.games.loa.util.LOAMove;
import za.ac.sun.cs.ingenious.games.loa.util.LineOrientation;

import java.util.ArrayList;
import java.util.List;

import static za.ac.sun.cs.ingenious.games.loa.movegeneration.LineUtils.getBaseCoordFromHashIndex;
import static za.ac.sun.cs.ingenious.games.loa.movegeneration.LineUtils.lineMoveIsLegal;

public class MoveMap {
	private static List<LOAMove>[][][][] moveTable; // [colour - 1][orientation][linePosition][lineHash]
	private static Colour[] colours = {Colour.BLACK, Colour.WHITE, Colour.EMPTY};
	
	static {
		moveTable = new List[2][4][][];
		for (int colour = 0; colour < 2; colour++){
			moveTable[colour][LineOrientation.HORIZONTAL.ordinal()] = new List[8][LineConfiguration.powers[8]];
			moveTable[colour][LineOrientation.VERTICAL.ordinal()] = new List[8][LineConfiguration.powers[8]];
			moveTable[colour][LineOrientation.POSITIVE_DIAGONAL.ordinal()] = new List[15][];
			moveTable[colour][LineOrientation.NEGATIVE_DIAGONAL.ordinal()] = new List[15][];
			for (int i = 0; i < 15; i++){
				int pdLength = LineUtils.getLineLengthFromLinePos(i, LineOrientation.POSITIVE_DIAGONAL, null);
				int ndLength = LineUtils.getLineLengthFromLinePos(i, LineOrientation.NEGATIVE_DIAGONAL, null);
				moveTable[colour][LineOrientation.POSITIVE_DIAGONAL.ordinal()][i] = new List[LineConfiguration.powers[pdLength]];
				moveTable[colour][LineOrientation.NEGATIVE_DIAGONAL.ordinal()][i] = new List[LineConfiguration.powers[ndLength]];
			}
		}
		
		for (int i = 2; i <= 8; i++){
			permuteAndPopulate(i);
		}
	}
	
	private static void permuteAndPopulate(int lineLength){
		Colour[] line = new Colour[lineLength];
		permuteRecursively(line, lineLength - 1);
	}
	
	private static void permuteRecursively(Colour[] line, int currentPos) {
		if (currentPos == -1) {
			addMovesForLine(line);
			return;
		}
		for (int i = 0; i < 3; ++i) {
			line[currentPos] = colours[i];
			permuteRecursively(line, currentPos - 1);
		}
	}
	
	private static void addMovesForLine(Colour[] line){
		int lineLength = line.length;
		int index = 0;
		int numPieces = 0;
		for (int i = 0; i < lineLength; i++){
			index += line[i].ordinal() * LineConfiguration.powers[i];
			if (line[i] != Colour.EMPTY){
				numPieces++;
			}
		}
		for (int i = 0; i < lineLength; i++){
			int dest1 = i + numPieces;
			int dest2 = i - numPieces;
			if (line[i] != Colour.EMPTY){
				if (lineMoveIsLegal(line, i, dest1, line[i])){
					boolean capture = line[dest1] != Colour.EMPTY;
					addMovesForLine(line, i, dest1, capture, index);
				}
				if (lineMoveIsLegal(line, i, dest2, line[i])){
					boolean capture = line[dest2] != Colour.EMPTY;
					addMovesForLine(line, i, dest2, capture, index);
				}
			}
		}
	}
	
	private static void addMovesForLine(Colour[] line, int srcIndex, int destIndex, boolean capture, int hash){
		if (line.length == 8){
			addOrthogonal(line, srcIndex, destIndex, capture, hash);
		}
		addDiagonal(line, srcIndex, destIndex, capture, hash);
	}
	
	private static void addOrthogonal(Colour[] line, int srcIndex, int destIndex, boolean capture, int hash){
		Colour colour = line[srcIndex];
		for (int i = 0; i < 8; i++){
			List<LOAMove> horizontalMoves = getMoveList(colour, LineOrientation.HORIZONTAL, i, hash);
			List<LOAMove> verticalMoves = getMoveList(colour, LineOrientation.VERTICAL, i, hash);
			LOACoord horizontalSrc = LOACoord.coords[i][srcIndex];
			LOACoord horizontalDest = LOACoord.coords[i][destIndex];
			LOACoord verticalSrc = LOACoord.coords[srcIndex][i];
			LOACoord verticalDest = LOACoord.coords[destIndex][i];
			horizontalMoves.add(new LOAMove(horizontalSrc, horizontalDest, capture));
			verticalMoves.add(new LOAMove(verticalSrc, verticalDest, capture));
		}
	}
	
	// Gross but it works
	private static void addDiagonal(Colour[] line, int srcIndex, int destIndex, boolean capture, int hash){
		Colour colour = line[srcIndex];
		int length = line.length;
		int pos1 = length - 1; // Positions of positive/negative are the same for each line length
		int pos2 = 14 - pos1;
		List<LOAMove> pdMoves1 = getMoveList(colour, LineOrientation.POSITIVE_DIAGONAL, pos1, hash);
		List<LOAMove> pdMoves2 = getMoveList(colour, LineOrientation.POSITIVE_DIAGONAL, pos2, hash);
		List<LOAMove> ndMoves1 = getMoveList(colour, LineOrientation.NEGATIVE_DIAGONAL, pos1, hash);
		List<LOAMove> ndMoves2 = getMoveList(colour, LineOrientation.NEGATIVE_DIAGONAL, pos2, hash);
		LOACoord pdSrc1 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos1, LineOrientation.POSITIVE_DIAGONAL), srcIndex, LineOrientation.POSITIVE_DIAGONAL);
		LOACoord pdSrc2 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos2, LineOrientation.POSITIVE_DIAGONAL), srcIndex, LineOrientation.POSITIVE_DIAGONAL);
		LOACoord pdDest1 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos1, LineOrientation.POSITIVE_DIAGONAL), destIndex, LineOrientation.POSITIVE_DIAGONAL);
		LOACoord pdDest2 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos2, LineOrientation.POSITIVE_DIAGONAL), destIndex, LineOrientation.POSITIVE_DIAGONAL);
		LOACoord ndSrc1 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos1, LineOrientation.NEGATIVE_DIAGONAL), srcIndex, LineOrientation.NEGATIVE_DIAGONAL);
		LOACoord ndSrc2 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos2, LineOrientation.NEGATIVE_DIAGONAL), srcIndex, LineOrientation.NEGATIVE_DIAGONAL);
		LOACoord ndDest1 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos1, LineOrientation.NEGATIVE_DIAGONAL), destIndex, LineOrientation.NEGATIVE_DIAGONAL);
		LOACoord ndDest2 = LineUtils.getCoordFromLineIndex(getBaseCoordFromHashIndex(pos2, LineOrientation.NEGATIVE_DIAGONAL), destIndex, LineOrientation.NEGATIVE_DIAGONAL);
		pdMoves1.add(new LOAMove(pdSrc1, pdDest1, capture));
		ndMoves1.add(new LOAMove(ndSrc1, ndDest1, capture));
		if (pos1 != pos2){
			pdMoves2.add(new LOAMove(pdSrc2, pdDest2, capture));
			ndMoves2.add(new LOAMove(ndSrc2, ndDest2, capture));
		}
	}
	
	private static List<LOAMove> getMoveList(Colour colour, LineOrientation orientation, int linePos, int hash){
		List<LOAMove> moveList =  moveTable[colour.ordinal() - 1][orientation.ordinal()][linePos][hash];
		if (moveList == null){
			moveList = new ArrayList<LOAMove>();
			moveTable[colour.ordinal() - 1][orientation.ordinal()][linePos][hash] = moveList;
		}
		return moveList;
	}
	
	public static List<LOAMove> getMovesForLine(Colour colour, LineOrientation orientation, int linePos, int hash, LOABoard board){
		return moveTable[colour.ordinal() - 1][orientation.ordinal()][linePos][hash];
	}
}
