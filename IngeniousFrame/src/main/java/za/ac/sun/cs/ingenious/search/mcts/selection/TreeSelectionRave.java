package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;

import java.util.HashMap;
import java.util.Hashtable;

/**
 * A standard implementation of the TreeEngine selection strategy {@link TreeSelection},
 * that calculates UCT values to make a decision of which child node to select.
 * This strategy is specifically for TreeEngine selection and makes use of read locks
 * when viewing node information that is used in the calculations.
 *
 * @author Karen Laubscher
 *
 * @param <S>  The game state type.
 * @param <N>  The type of the mcts node.
 */
public class TreeSelectionRave<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, ?>> implements TreeSelection<N> {

	private GameLogic<S> logic;
	private static double c;
	private static double V;
	public int player;


	/**
	 * Constructor to create a TreeEngine UCT selection object with a specified value
	 * for the constant C used in the UCT value calculation.
	 *
	 * @param logic  The game logic
	 * @param c      The constant value for C (in the UCT calculation).
	 */
	private TreeSelectionRave(GameLogic<S> logic, double c, double V, int player) {
		this.logic = logic;
		this.c = c;
		this.V = V;
		this.player = player;
	}

	/**
	 * The method that decide which child node to traverse to next, based on
	 * calculating the UCT value for each child and then selecting the child
	 * with the highest UCT value. Since the TreeEngine sturture is shared in TreeEngine
	 * parallelisation, nodes are (read) locked when information for
	 * the calculations are viewed.
	 *
	 * @param node  The current node whose children nodes are considered for the
	 *				next node to traverse to.
	 *
	 * @return      The selected child node with the highest UCT value.
	 */
	public N select(N node) {
		//Terminal state - no further selection takes place, therefore return node as selected node
		if (logic.isTerminal(node.getState())) {
			return node;
		}

		// Get the current node's visit count
		double currentVisitCount;
		currentVisitCount = node.getVisitCount();

		N highestUctChild = null;
		double tempVal;
		double highestUct = Double.NEGATIVE_INFINITY;


		Hashtable<String, MctsNodeExtensionParallelInterface> enhancements = node.getEnhancementClasses();

		node.readLockEnhancementClassesArrayList();
		MctsRaveNodeExtensionParallel raveEnhancementExtensionParallel = (MctsRaveNodeExtensionParallel)enhancements.get("Rave");
		node.readUnlockEnhancementClassesArrayList();

		if (raveEnhancementExtensionParallel == null) {
			System.out.println("No rave enhancement to select from in Rave selection class");
		}

		Hashtable<Action, MctsNodeTreeParallelInterface<?, ?, ?>> actionToNodeMapping = raveEnhancementExtensionParallel.getActionToNodeMapping();

		raveEnhancementExtensionParallel.setReadLockChild();
		for (HashMap.Entry<Action, MctsNodeTreeParallelInterface<?, ?, ?>> entry : actionToNodeMapping.entrySet()) {
			Action action = entry.getKey();
			N value = (N)entry.getValue();
			N child = (N)actionToNodeMapping.get(action);


			double raveVisitsCount = raveEnhancementExtensionParallel.getRaveVisits(action);
			double raveWinsCount = raveEnhancementExtensionParallel.getRaveWins(action) - child.getVirtualLoss();
			double monteCarloVisitCount = child.getVisitCount();
			double monteCarloWinCount = child.getValue();

			double n = ((V - currentVisitCount) / V);
			double alpha = Math.max(0.0, n);

			double childAverage =  (1-(monteCarloWinCount/monteCarloVisitCount)) + c*Math.sqrt(2*Math.log(currentVisitCount)/monteCarloVisitCount);
			double childRaveAverage =  (raveWinsCount/raveVisitsCount);

			tempVal = (1 - alpha)*childAverage + alpha*(childRaveAverage);

			if (Double.compare(tempVal, highestUct) > 0) {
				highestUctChild = value;
				highestUct = tempVal;
			}
		}
		raveEnhancementExtensionParallel.unsetReadLockChild();

		if (highestUctChild == null || node.getUnexploredChildren().size() != 0) {
			return null;
		}
		highestUctChild.applyVirtualLoss(-10);
		return highestUctChild;
	}

	/**
	 * A static factory method, which creates a TreeEngine UCT selection object that
	 * uses the specified value for the constant C in the UCT value calculation.
	 *
	 * This factory method is also a generic method, which uses Java type
	 * inferencing to encapsulate the complex generic type capturing required
	 * by the TreeEngine UCT selection constructor.
	 *
	 * @param logic   The game logic
	 * @param c       The constant value for C (in the UCT calculation).
	 */
	public static <SS extends GameState, NN extends MctsNodeTreeParallelInterface<SS, NN, ?>> TreeSelection<NN> newTreeSelectionRave(GameLogic<SS> logic, double c, double V, int player) {
		return new TreeSelectionRave<SS, NN>(logic, c, V, player);
	}

}
