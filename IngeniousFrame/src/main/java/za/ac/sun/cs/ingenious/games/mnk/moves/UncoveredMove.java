package za.ac.sun.cs.ingenious.games.mnk.moves;

import za.ac.sun.cs.ingenious.core.Move;

public class UncoveredMove implements Move {

	private static final long serialVersionUID = 1L;

	private final int playerID;

	/**
	 * @param playerID The ID of the player executing the move
	 */
	public UncoveredMove(int playerID) {
		this.playerID = playerID;
	}

	/**
	 * @return The ID of the player executing the move
	 */
	@Override
	public int getPlayerID() {
		return playerID;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(obj instanceof UncoveredMove)) {
			return false;
		} else {
			return (this.playerID == ((UncoveredMove) obj).playerID);
		}
	}

	@Override
	public int hashCode() {
		return playerID;
	}

	@Override
	public String toString() {
		return "UncoveredMove, playerID: " + playerID;
	}

}
