package za.ac.sun.cs.ingenious.games.cardGames.core.actions;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;

/**
 * The Class PlayCardMove.
 *
 * A DrawCardMove is sent when a player wants to (or is forced to) draw a card.
 *
 * @param <Location> The generic location enum
 *
 * @author Joshua Wiebe
 */
public class DrawCardAction<Location extends Enum<Location>> extends CardAction<Location>{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new play card move.
	 *
	 * @param player Current player
	 * @param card The card which changes location
	 * @param oldLoc the old location
	 * @param newLoc the new location
	 */
	public DrawCardAction(int player, Card card, Location oldLoc, Location newLoc) {
		super(player, card, oldLoc, newLoc);
	}


	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.actions.CardAction#toString()
	 *
	 * Override toString() of CardMove to get a suitable output.
	 *
	 * @return Returns an appropriate toString
	 */
	@Override
	public String toString(){
		return ("Draw a card from draw pile: " + super.card.toString());
	}
}
