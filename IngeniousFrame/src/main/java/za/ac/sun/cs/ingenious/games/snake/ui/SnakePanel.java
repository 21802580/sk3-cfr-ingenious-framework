package za.ac.sun.cs.ingenious.games.snake.ui;

import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.games.snake.SnakeLogic;
import za.ac.sun.cs.ingenious.games.snake.SnakeState;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class SnakePanel extends JPanel {
    public static final int EDGE_INSET = 30;
    public final static int BODY_WIDTH = SnakeLogic.BODY_SIZE;
    public final static int FOOD_WIDTH = SnakeLogic.FOOD_SIZE;
    public static final int X_OFFSET = EDGE_INSET;
    public static final int Y_OFFSET = EDGE_INSET;
    public static final boolean DEBUG = false;

    private SnakeState state;

    public SnakePanel() {
    }

    public void updateUI(SnakeState state) {
        this.state = state;
        repaint();
    }

    public void putScreenshotToState(SnakeState updateState) {
        // Attach a screenshot of the newly rendered UI to the state.
        updateState.addImage(getImage());
    }

    public void replaceScreenshotInState(SnakeState updateState) {
        /*
         * Replace the most recent screenshot with the newly rendered UI to the state. Typically used for an
         * after-effect resulting from a move e.g. eating food, then rendering the synchronized state received from the
         * referee afterwards.
         */
        updateState.replaceImage(getImage(), 0);
    }

    public void replaceAllScreenshotsInState(SnakeState updateState) {
        for (int i = 0; i < updateState.getImageBuffer().size(); i++) {
            updateState.replaceImage(getImage(), i);
        }
    }

    /**
     * Takes a screenshot of the game and returns the resulting buffered image.
     * @return BufferedImage representation of the game.
     */
    public BufferedImage getImage() {
        BufferedImage bufferedImage = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

        Graphics g = bufferedImage.createGraphics();
        this.print(g);
        g.dispose();

        return bufferedImage;
    }

    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);

        Graphics2D graphics2d = (Graphics2D) graphics;
        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // Background
        graphics2d.setColor(Color.BLACK);
        graphics2d.fillRect(0, 0, getWidth(), getHeight());

        // Movement area background
        graphics2d.setColor(Color.LIGHT_GRAY);
        graphics2d.fillRect(EDGE_INSET, EDGE_INSET, SnakeState.WIDTH, SnakeState.HEIGHT);

        if (DEBUG) {
            graphics2d.setColor(Color.BLACK);
            graphics2d.drawRect(EDGE_INSET, EDGE_INSET, SnakeState.WIDTH, SnakeState.HEIGHT);
        }

        // Score
        graphics2d.setColor(Color.LIGHT_GRAY);
        graphics2d.setFont(graphics2d.getFont().deriveFont(24.0f));
        graphics2d.drawString(state.getScore() + "", getWidth() / 2 - 5, 24);

        Coord foodCoord = state.getFood();

        if (foodCoord != null) {

            int t1 = FOOD_WIDTH / 3;

            graphics2d.setColor(Color.BLACK);
            graphics2d.fillRect(foodCoord.getX() + X_OFFSET + t1, foodCoord.getY() + Y_OFFSET, t1, t1);
            graphics2d.fillRect(foodCoord.getX() + X_OFFSET, foodCoord.getY() + Y_OFFSET + t1, t1, t1);
            graphics2d.fillRect(foodCoord.getX() + X_OFFSET + t1 * 2, foodCoord.getY() + Y_OFFSET + t1, t1, t1);
            graphics2d.fillRect(foodCoord.getX() + X_OFFSET + t1, foodCoord.getY() + Y_OFFSET + t1 * 2, t1, t1);


            if (DEBUG) {
                graphics2d.setColor(Color.BLACK);
                graphics2d.drawRect(foodCoord.getX() + X_OFFSET, foodCoord.getY() + Y_OFFSET, FOOD_WIDTH, FOOD_WIDTH);
            }
        }

        // Create as a new list to prevent concurrent modification.
        LinkedList<Coord> bodyCoords = new LinkedList<>(state.getBody());
        for (Coord bodyCoord : bodyCoords) {
            if (bodyCoord.equals(bodyCoords.getFirst())) {
                graphics2d.setColor(Color.GREEN.darker().darker());
            } else {
                graphics2d.setColor(Color.GREEN.darker());
            }

            graphics2d.fillRect(bodyCoord.getX() + X_OFFSET, bodyCoord.getY() + Y_OFFSET, BODY_WIDTH, BODY_WIDTH);

            if (DEBUG) {
                graphics2d.setColor(Color.BLACK);
                graphics2d.drawRect(bodyCoord.getX() + X_OFFSET, bodyCoord.getY() + Y_OFFSET, BODY_WIDTH, BODY_WIDTH);
            }
        }

    }
}
