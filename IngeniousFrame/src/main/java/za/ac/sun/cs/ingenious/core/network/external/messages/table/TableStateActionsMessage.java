package za.ac.sun.cs.ingenious.core.network.external.messages.table;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

import java.util.Map;

/**
 * Used to receive an action value from the table in the Python application.
 *
 * @author Steffen Jacobs
 */
public class TableStateActionsMessage extends ExternalMessage {
    private Map<Integer, Double> payload;

    public TableStateActionsMessage(Map<Integer, Double> payload) {
        super(ExternalMessageType.TABLE_STATE_ACTIONS);

        this.payload = payload;
    }

    public Map<Integer, Double> getPayload() {
        return payload;
    }

    public void setPayload(Map<Integer, Double> payload) {
        this.payload = payload;
    }
}
