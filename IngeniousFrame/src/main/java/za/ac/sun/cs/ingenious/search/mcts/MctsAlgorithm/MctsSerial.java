package za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm;

import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;

// needed for default constructor:
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import static za.ac.sun.cs.ingenious.search.mcts.selection.SelectionUct.newSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;

// needed for enhancements:
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage;
import za.ac.sun.cs.ingenious.search.mcts.enhancements.Enhancements;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.selection.SelectionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.selection.SelectionUct;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationTuple;

/**
 * Implements a general-purpose serial version of the MCTS algorithm.
 * Due to the shared memory nature of this algorithm, the MCTS node type 
 * that can be used for this algorithm should store the child and parent of the 
 * node as references to the node structure.
 * 
 * @author Karen Laubscher
 * 
 * @param <S>  The game state type
 * @param <N>  The MCTS node type
 */
public class MctsSerial<S extends GameState, N extends MctsNodeCompositionInterface<S, N, N>> {

	private static boolean PRE_SELECTION_ENHANCEMENT;
	private static boolean POST_SELECTION_ENHANCEMENT;
	private static boolean PRE_EXPANSION_ENHANCEMENT;
	private static boolean POST_EXPANSION_ENHANCEMENT;
	private static boolean PRE_SIMULATION_ENHANCEMENT;
	private static boolean POST_SIMULATION_ENHANCEMENT;
	private static boolean PRE_BACKPROP_ENHANCEMENT;
	private static boolean POST_BACKPROP_ENHANCEMENT;
	
	Enhancements<S, N, N> enhancements = null;

	private SelectionThreadSafe<N, N> selection;
	private ExpansionThreadSafe<N, N> expansion;
	private SimulationThreadSafe<S> simulation;
	private BackpropagationThreadSafe<N> backpropagation;
	private SelectionThreadSafe<N, N> finalSelection;
	private GameLogic<S> logic;
	
	/** 
	 * Default constructor, which uses SelectionUct, ExpansionSingle,
	 * SimulationRandom and BackpropagationAverage. The final selection strategy 
	 * is also the same as the selection strategy.
	 *
	 * @param logic	The game logic, used to check for teminal gamestates.
	 * @param mctsEvaluator	
	 * 				Evaluator that includes values used for mcts simulation results.
	 * @param obs	Object to process actions made by the players during playouts. 
	 *			    Actions are observed from the point of view of the environment player.
	 * @param playerId
	 *				The ID of the player, used during backpropagation.
	 */
	public MctsSerial(GameLogic<S> logic, MctsGameFinalEvaluator<S> mctsEvaluator, ActionSensor<S> obs, int playerId) {
		
		this.selection = newSelectionUct(logic);
		this.expansion = newExpansionSingle(logic);
		this.simulation = newSimulationRandom(logic, mctsEvaluator, obs, false);
		this.backpropagation = newBackpropagationAverage();
		this.finalSelection = this.selection;
		this.logic = logic;
		enhancementSetup(null);
	}
	
	/** 
	 * Default constructor, also allowing the use of enhancements and uses 
	 * SelectionUct, ExpansionSingle, SimulationRandom and BackpropagationAverage.
	 * The final selection strategy is also the same as the selection strategy. 
	 *
	 * @param logic	The game logic, used to check for teminal gamestates.
	 * @param mctsEvaluator	
	 * 				Evaluator that includes values used for mcts simulation results.
	 * @param obs	Object to process actions made by the players during playouts. 
	 *			    Actions are observed from the point of view of the environment player.
	 * @param playerId
	 *				The ID of the player, used during backpropagation.
	 * @param enhancements
	 *				An enhancements object containing the chosen algorithm 
	 *				enhancements combination desired to be used with the mcts.
	 */
	 public MctsSerial(GameLogic<S> logic, MctsGameFinalEvaluator<S> mctsEvaluator, ActionSensor<S> obs, int playerId, Enhancements<S, N, N> enhancements) {
		
		this.selection = newSelectionUct(logic);
		this.expansion = newExpansionSingle(logic);
		this.simulation = newSimulationRandom(logic, mctsEvaluator, obs, false);
		this.backpropagation = newBackpropagationAverage();
		this.finalSelection = this.selection;
		this.logic = logic;
		this.enhancements = enhancements;
		enhancementSetup(enhancements);
	}
	
	/**
	 * Constructor that takes in the 4 basic strategies (where final selection 
	 * is the same as the general selection strategy).
	 *
	 * @param selection  The selection strategy. A common strategy to use is 
	 *					 {@link SelectionUct}, which calculates UCT values.
	 * @param expansion	 The expansion strategy. A common strategy to use is 
	 *  				 {@link ExpansionSingle}, which adds one unexplored node
	 * @param simulation The simulation strategy. A basic play out policy is to 
	 *					 use {@link SimulationRandom}, which do random playout.
	 * @param backpropagation
	 *                   The backpropagation strategy. A common strategy to use 
	 *					 is {@link BackpropagationAverage}, just adding the result
	 * @param logic 	 The game logic, used to check for terminal gamestates.
	 * @param enhancements
	 *				An enhancements object containing the chosen algorithm 
	 *				enhancements combination desired to be used with the mcts.
	 */
	public MctsSerial(SelectionThreadSafe<N, N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, BackpropagationThreadSafe<N> backpropagation, GameLogic<S> logic, Enhancements<S, N, N> enhancements) {
		
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagation = backpropagation;
		this.finalSelection = selection;
		this.logic = logic;
		this.enhancements = enhancements;
		enhancementSetup(enhancements);
	}
	
	/**
	 * Constructor that takes in the 4 basic strategies as well as a final 
	 * selection strategy, that may differ from the selection strategy used when
	 * traversing the TreeEngine during the MCTS algorithm.
	 *
	 * @param selection  The selection strategy. A common strategy to use is 
	 *					 {@link SelectionUct}, which calculates UCT values.
	 * @param expansion	 The expansion strategy. A common strategy to use is 
	 *  				 {@link ExpansionSingle}, which adds one unexplored node
	 * @param simulation The simulation strategy. A basic play out policy is to 
	 *					 use {@link SimulationRandom}, which do random playout.
	 * @param backpropagation
	 *                   The backpropagation strategy. A common strategy to use 
	 *					 is {@link BackpropagationAverage}, just adding the result
	 * @param finalSelection
	 *				     The selection strategy used to make the final selection
	 *					 to determine the best move to be returned.
	 * @param logic 	 The game logic, used to check for terminal gamestates.
	 * @param enhancements
	 *				An enhancements object containing the chosen algorithm 
	 *				enhancements combination desired to be used with the mcts.
	 */
	public MctsSerial(SelectionThreadSafe<N, N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, BackpropagationThreadSafe<N> backpropagation, SelectionThreadSafe<N, N> finalSelection, GameLogic<S> logic, Enhancements<S, N, N> enhancements) {
		
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagation = backpropagation;
		this.finalSelection = finalSelection;
		this.logic = logic;
		this.enhancements = enhancements;
		enhancementSetup(enhancements);
	}
	
	/**
	 * This method generates an action that represents the best move choice 
	 * found. The method is the template operation that drives the serial 
	 * MCTS algorithm.
	 * 
	 * @param root        The root node from which the search should start.
	 * @param turnlength  Time in ms within which the move needs to be decided.
	 *
	 * @return The best move action that was found by the algorithm during the
	 *  	   time of the turn length.
	 */
	public Action doSearch(N root, long turnlength) { // TODO: turnlength, change to clock?
		
		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnlength; // TODO: optimise (minus final move selecin time)
		
		while (System.currentTimeMillis() < endTime) {
			//root.incVisitCount(); // TODO: move to backprop and remove
			if (PRE_SELECTION_ENHANCEMENT) enhancements.applyPreSelection(root);
			N currentNode = selection.select(root);
			if (POST_SELECTION_ENHANCEMENT) enhancements.applyPostSelection(root, currentNode);
			
			
			// Traverse TreeEngine (selection phase):
			while (currentNode.isUnexploredEmpty() && !logic.isTerminal(currentNode.getState())) {
				N previousNode = currentNode;
				if (PRE_SELECTION_ENHANCEMENT) enhancements.applyPreSelection(previousNode);
				currentNode = selection.select(previousNode);
				if (POST_SELECTION_ENHANCEMENT) enhancements.applyPostSelection(previousNode, currentNode);
			}
			
			// Add a node (expansion phase):
			if (PRE_EXPANSION_ENHANCEMENT) enhancements.applyPreExpansion(currentNode);
			N addedNode = expansion.expand(currentNode); 
			if (POST_EXPANSION_ENHANCEMENT) enhancements.applyPostExpansion(currentNode, addedNode);
			//if (addedNode != null) addedNode.incVisitCount(); // TODO: move to backprop and remove
			
			// Play a simulation (simulation phase):
			if (PRE_SIMULATION_ENHANCEMENT) enhancements.applyPreSimulation(addedNode.getState());
			SimulationTuple resultsTuple = simulation.simulate(addedNode.getState());
			double[] results = resultsTuple.getScores();
			if (POST_SIMULATION_ENHANCEMENT) enhancements.applyPostSimulation(results);
			
			// Propagate results up TreeEngine (BackpropagationThreadSafe phase):
			currentNode = addedNode;
			while (currentNode != root) {
				if (PRE_BACKPROP_ENHANCEMENT) enhancements.applyPreBackpropagation(currentNode, results);
				backpropagation.propagate(currentNode, results);
				if (POST_BACKPROP_ENHANCEMENT) enhancements.applyPostBackpropagation(currentNode);
				currentNode = currentNode.getParent();
			}
		}
		
		N finalChoice = finalSelection.select(root);
		Action bestMove = finalChoice.getPrevAction();
		
		return bestMove;
	}
	
	private void enhancementSetup(Enhancements<S, N, N> enhancements) {
		if (enhancements == null) {
			PRE_SELECTION_ENHANCEMENT = false;
			POST_SELECTION_ENHANCEMENT = false;
			PRE_EXPANSION_ENHANCEMENT = false;
			POST_EXPANSION_ENHANCEMENT = false;
			PRE_SIMULATION_ENHANCEMENT = false;
			POST_SIMULATION_ENHANCEMENT = false;
			PRE_BACKPROP_ENHANCEMENT = false;
			POST_BACKPROP_ENHANCEMENT = false;

		} else {
			PRE_SELECTION_ENHANCEMENT = enhancements.getPreSelectionBool();
			POST_SELECTION_ENHANCEMENT = enhancements.getPostSelectionBool();
			PRE_EXPANSION_ENHANCEMENT = enhancements.getPreExpansionBool();
			POST_EXPANSION_ENHANCEMENT = enhancements.getPostExpansionBool();
			PRE_SIMULATION_ENHANCEMENT = enhancements.getPreSimulationBool();
			POST_SIMULATION_ENHANCEMENT = enhancements.getPostSimulationBool();
			PRE_BACKPROP_ENHANCEMENT = enhancements.getPreBackpropagationBool();
			POST_BACKPROP_ENHANCEMENT = enhancements.getPostBackpropagationBool();
		}

	}
	
}
