package za.ac.sun.cs.ingenious.search.mcts.nodeComposition;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

/** 
 * Additional methods that are required if a node is to be used for root
 * parallelistion MCTS version, particularly the merge function.
 *
 * @author Karen Laubscher 
 */
public interface MctsRootNode<N extends MctsNodeCompositionInterface<?, ?, ?>> {
	
	/**
	 * The method that merges another node into the instance node. (Used in 
	 * root parallelisation to merge the root nodes)
	 *
	 * @param other  The other node that needs to be merged into this node
	 */
	public void merge(N other);
		
}
