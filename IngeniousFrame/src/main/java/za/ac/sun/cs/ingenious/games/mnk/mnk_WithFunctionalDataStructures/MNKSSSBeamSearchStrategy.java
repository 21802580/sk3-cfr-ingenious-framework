package za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ExecutorService;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.search.sss.SSS;

/**
 * A beam search depth-limited strategy for finding moves in the MNK game using the SSS* search algorithm.
 * A beam search limits the branching factor of the search tree by only considering the L most promising moves at each node.
 * 
 * <p> This class acts as a module that extends {@link MNKSSSStrategy}. Provides nodes that extend SSS.SearchNode,
 * and calls the SSS.compute method with these nodes to determine the best player action.
 * 
 * @author Nicholas Robinson
 */
public class MNKSSSBeamSearchStrategy extends MNKSSSStrategy {

    public MNKSSSBeamSearchStrategy(int m, int n, int k) {
        super(m, n, k);
    }

    /**
     * determine the best move from the given list of moves, using the SSS* algorithm.
     * 
     * @param player the player to move.
     * @param board the board state on which the player will act on
     * @param moves an array of the moves to be considered
     * @param bound SSS* bound
     * @param depthLimit maximum search tree depth
     * @param beamWidth the maximum number of children of each node in the search tree
     * @param numberOfAvailableMoves number of empty positions on the game board
     * @return the results of the search
     */
    public SearchResult determineBestMove(int player, PVector<Integer> board, int[] moves, int bound, int depthLimit, int beamWidth, int numberOfAvailableMoves) {
        if (moves.length == 0) {
            return null;
        }

        Environment environment = new Environment(player, depthLimit, beamWidth, numberOfAvailableMoves);

        List<Environment.SearchNode> searches = new ArrayList<Environment.SearchNode>(moves.length);
        for (int m : moves) {
            searches.add(environment.new SearchNode(board.set(m, player(player)), null, quantifyChangeInOpportunities(board, m, player), 1));
        }

        SSS.SearchResult<Environment.SearchNode> result = SSS.compute(searches);

        return new SearchResult(moves[searches.indexOf(result.getRoot())], result.getScore(), result.getIterations());
    }

    /**
     * Return the best possible player action as determined by the SSS* algorithm using the beam search strategy.
     * Evenly distributes the possible moves from a given board state into a set of tasks that are run by an executor service.
     * 
     * @param player the player to move.
     * @param board the board state on which the player will act on.
     * @param depthLimit maximum search tree depth.
     * @param beamWidth the maximum number of children of each node in the search tree.
     * @param executor executor service.
     * @param threadCount number of threads in the executor service.
     * @param partitionSize move batching size.
     * @return the position of the best move as determined by the search.
     */
    public int generateMove(int player, PVector<Integer> board, int depthLimit, int beamWidth, ExecutorService executor, int threadCount, int partitionSize) {
        return generateMove(player, board, executor, threadCount, partitionSize,
            (m, b, a) -> determineBestMove(player, board, m, b, depthLimit, beamWidth, a));
    }


    class Environment {

        int limit;
        Integer player;
        Integer opponent;
        int maxDepth;
        int initialNumberOfAvailableMoves;

        Environment(int player, int maxDepth, int limit, int initialNumberOfAvailableMoves) {
            this.limit = limit;
            this.maxDepth = maxDepth;
            this.initialNumberOfAvailableMoves = initialNumberOfAvailableMoves;

            if (player == 1) {
                this.player = player1;
                this.opponent = player2;
            }
            else if (player == 2) {
                this.player = player2;
                this.opponent = player1;
            }

        }

        int numberOfAvailableMoves(int depth) {
            return initialNumberOfAvailableMoves - depth;
        }

        Integer player(int depth) {
            return (depth & 1) == 0 ? player : opponent;
        }

        class SearchNode extends SSS.SearchNode<SearchNode> {

            PVector<Integer> board;
            int score;
            int depth;

            SearchNode parent;
            SearchNode[] children;

            public SearchNode(PVector<Integer> board, SearchNode parent, int score, int depth) {
                this.board = board;
                this.score = score;
                this.depth = depth;
                this.parent = parent;
            }

            void expand() {
                TopKMeritSet meritSet = new TopKMeritSet(Math.min(limit, numberOfAvailableMoves(depth)));
                
                int m = 0;
                for (Integer token : board.range(0, board_size - 1)) {
                    if (token == null) {

                        meritSet.offer(quantifyChangeInOpportunities(board, m, player(depth)), m);
                    }
                    m++;
                }

                children = new SearchNode[meritSet.size()];

                int i = 0;
                for (TopKMeritSet.Item item : meritSet)  {
                    PVector<Integer> b = board.set(item.move, player(depth));
                    int s; 
                    if (item.merit == Integer.MAX_VALUE) {
                        s = VICTORYSCORE + numberOfAvailableMoves(depth);
                        if (isMinimiser()) s = -s;
                    }
                    else {
                        s = score + (isMaximiser() ?  item.merit : -item.merit);
                    }

                    children[i] = new SearchNode(b, this, s, depth + 1);
                    i++;
                }
            }

            @Override
            public int childCount() {
                if (children == null) expand();
                return children.length;
            }

            @Override
            public SearchNode getChild(int index) {
                if (children == null) expand();
                return children[index];
            }

            @Override
            public SearchNode getParent() {
                return parent;
            }

            @Override
            public boolean isMinimiser() {
                return player(depth) != player;
            }

            @Override
            public boolean isMaximiser() {
                return player(depth) == player;
            }

            @Override
            public boolean isTerminal() {
                return depth >= maxDepth || gameHasWinner(score) || numberOfAvailableMoves(depth) == 0;
            }

            @Override
            public boolean isRoot() {
                return parent == null;
            }

            @Override
            public int evaluate() {
                return score;
            }

            @Override
            public void free() {
                board = null;
                children = null;
            }
        }
    }
}

class TopKMeritSet implements Iterable<TopKMeritSet.Item> {
    int k;
    SortedSet<Item> sortedSet = new TreeSet<Item>();

    static class Item implements Comparable<Item> {
        int move, merit;

        Item(int move, int merit) {
            this.move = move;
            this.merit = merit;
        }

        @Override
        public int compareTo(Item b) {
            return merit - b.merit;
        }
    }

    @SuppressWarnings("unchecked")
    public TopKMeritSet(int k) {
        this.k = k;
    }

    public boolean offer(int merit, int move) {
        if (sortedSet.size() < k) {
            sortedSet.add(new Item(move, merit));
            return true;
        }
        else if (merit > sortedSet.first().merit) {
            sortedSet.remove(sortedSet.first());
            sortedSet.add(new Item(move, merit));
        }
        return false;
    }

    public int size() {
        return sortedSet.size();
    }

	@Override
	public Iterator<Item> iterator() {
		return sortedSet.iterator();
	}
}