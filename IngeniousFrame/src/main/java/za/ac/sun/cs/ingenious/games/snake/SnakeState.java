package za.ac.sun.cs.ingenious.games.snake;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.Random;

import static za.ac.sun.cs.ingenious.games.snake.SnakeLogic.BODY_SIZE;
import static za.ac.sun.cs.ingenious.games.snake.SnakeLogic.FOOD_SIZE;
import static za.ac.sun.cs.ingenious.games.snake.SnakeLogic.STEP_SIZE;

/**
 * Snake state that supports being reset and also keeps track of the last few state images.
 *
 * @author Steffen Jacobs
 */
public class SnakeState extends GameState {
    public static final int DEFAULT_PLAYER_COUNT = 1;
    public static final int DEFAULT_FIRST_PLAYER = 0;
    public static final int HEIGHT = 180;
    public static final int WIDTH = 180;
    public static final Coord INITIAL_POSITION = new Coord((WIDTH / BODY_SIZE / 2) * BODY_SIZE, (HEIGHT / BODY_SIZE / 2) * BODY_SIZE);
    public static final CompassDirection INITIAL_DIRECTION = CompassDirection.E;
    public static final int INITIAL_LENGTH = 2;
    public static final int MAX_IMAGES = 4;

    private LinkedList<BufferedImage> imageBuffer;
    private Random rng;
    private LinkedList<Coord> body;
    private Coord food;
    private CompassDirection currentDirection;
    private int score;
    private int previousScore;
    private int snakeAge;

    public SnakeState() {
        super(DEFAULT_PLAYER_COUNT);

        imageBuffer = new LinkedList<>();

        rng = new Random();

        currentDirection = INITIAL_DIRECTION;

        body = new LinkedList<>();
        for (int i = 0; i < INITIAL_LENGTH; i++) {
            body.add(INITIAL_POSITION.add(currentDirection, -STEP_SIZE * i));
        }

        score = 0;
        previousScore = 0;

        spawnFood();
    }

    public SnakeState(SnakeState existing) {
        super(DEFAULT_PLAYER_COUNT);

        rng = existing.getRng();
        body = new LinkedList<>(existing.getBody());
        food = existing.getFood();
        currentDirection = existing.getCurrentDirection();
        score = existing.getScore();
        previousScore = existing.getPreviousScore();
        imageBuffer = new LinkedList<>(existing.imageBuffer);
        snakeAge = existing.getSnakeAge();
    }

    public void reset() {
        imageBuffer = new LinkedList<>();

        currentDirection = INITIAL_DIRECTION;

        body = new LinkedList<>();
        for (int i = 0; i < INITIAL_LENGTH; i++) {
            body.add(INITIAL_POSITION.add(currentDirection, -STEP_SIZE * i));
        }

        score = 0;
        previousScore = 0;
        snakeAge = 0;

        spawnFood();
    }

    public void setFood(Coord food) {
        this.food = food;
    }

    public void setCurrentDirection(CompassDirection currentDirection) {
        this.currentDirection = currentDirection;
    }

    /**
     * Performs a single update cycle to the state, given the current body and direction of movement.
     */
    public void tick() {
        // Add a new head, to move into the new position.
        Coord nextHead = body.getFirst().add(currentDirection, STEP_SIZE);
        body.addFirst(nextHead);

        // Remove the tail if food isn't eaten in this step.
        if (!nextHead.equals(food))
            body.removeLast();

        previousScore = score;
        snakeAge++;
    }

    /**
     * Repeatedly attempts to spawn food that does not share position with the body.
     */
    public void spawnFood() {
        do {
            int foodX = rng.nextInt(WIDTH / FOOD_SIZE);
            int foodY = rng.nextInt(HEIGHT / FOOD_SIZE);
            food = new Coord(foodX * FOOD_SIZE, foodY * FOOD_SIZE);
        } while (body.contains(food) || (food.getY() >= HEIGHT) || (food.getX() >= WIDTH));
    }

    public LinkedList<Coord> getBody() {
        return body;
    }

    public Coord getFood() {
        return food;
    }

    public CompassDirection getCurrentDirection() {
        return currentDirection;
    }

    public int getScore() {
        return score;
    }

    public int getPreviousScore() {
        return previousScore;
    }

    public Random getRng() {
        return rng;
    }

    public void incrementFoodEaten() {
        this.score++;
    }

    public LinkedList<BufferedImage> getImageBuffer() {
        return imageBuffer;
    }

    public int getSnakeAge() {
        return snakeAge;
    }

    public boolean hasMaximumBodyLength() {
        int rows = SnakeState.HEIGHT / SnakeLogic.BODY_SIZE;
        int columns = SnakeState.WIDTH / SnakeLogic.BODY_SIZE;

        return body.size() == rows * columns;
    }

    @Override
    public void printPretty() {
    }

    public SnakeState deepCopy() {
        return new SnakeState(this);
    }

    public void addImage(BufferedImage bufferedImage) {
        // Add image to the image history.
        imageBuffer.addFirst(bufferedImage);

        // Remove an image if image history size exceeds the number of frames needed.
        if (imageBuffer.size() > MAX_IMAGES) {
            imageBuffer.removeLast();
        }
    }

    public void replaceImage(BufferedImage bufferedImage, int index) {
        // Replace most recent image in the image history.
        imageBuffer.set(index, bufferedImage);
    }
}
