package za.ac.sun.cs.ingenious.core.util.nodes.trees;

/**
 * A collection of useful utilities for processing objects that implement the TreeNode interface
 * 
 * @author Nicholas Robinson
 */
public class TreeUtils {

    /**
     * Function obtain a string representation of a tree, format is similar to a file structure tree.
     * Performs a pre-order traversal on the given tree starting from a specified root. Prints one node per line.
     * Calls the {@link #Object.toString() toString} method on nodes to get their String representation, therefore the user
     * will have to override the toString method of the node object in order to gain useful information.
     * 
     * <p> string output is formatted as follows: 
     * <p> ". ".repeat(depth) + "{" + k + "}: " + node.toString()
     * <p> A dot is printed for each unit of depth of the node, and k indicates that the node is the k'th child of its parent
     * 
     * 
     * @param root root node of tree
     * @param maxDepth maximum depth to traverse
     * @param showNullChildren if true then links to null children will be displayed
     * @return formatted String representing tree
     */
    public static String stringifyTree(TreeNode<?> root, int maxDepth, boolean showNullChildren) {
        StringBuilder sb = new StringBuilder();
        new Object(){
            {
                build(root, 0, 0);
            }

            void build(TreeNode<?> node, int k, int depth)  {
                sb.append(". ".repeat(depth));
                sb.append("{" + k + "}: ");
                sb.append(node);
                sb.append('\n');

                if (node != null && depth < maxDepth) for (int i = 0; i < node.childCapacity(); i++) {
                    TreeNode<?> child = node.getChild(i);
                    if (child != null || showNullChildren) build(node.getChild(i), i, depth + 1);
                }
            }
        };
        return sb.toString();
    }

    /**
     * calls {@link #stringifyTree(TreeNode, int, boolean)} with the following default values on its parameters: 
     * <P> {@code maxDepth} = +infinity, {@code showNullChildren} = false
     * @param root root node of tree
     * @return formatted String representing tree
     */
    public static String stringifyTree(TreeNode<?> root) {
        return stringifyTree(root, Integer.MAX_VALUE, false);
    }

    /**
     * Counts the number of nodes in the given tree.
     * @param root root node of tree.
     * @return number of nodes in the tree.
     */
    public static int treeSize(TreeNode<?> root)
    {
        if (root == null) return 0;

        int count = 1;
        for (int i = 0; i < root.childCapacity(); i++) {
            count += treeSize(root.getChild(i));
        }
        return count;
    }
}
