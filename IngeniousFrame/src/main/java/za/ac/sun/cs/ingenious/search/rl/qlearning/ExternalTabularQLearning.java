package za.ac.sun.cs.ingenious.search.rl.qlearning;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.search.rl.util.ExternalTable;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Class containing an implementation of off-policy Q-learning.
 *
 * @author Steffen Jacobs
 */
public class ExternalTabularQLearning extends QLearning implements Serializable {
    /**
     * Table containing estimated expected reward Q(S, A).
     */
    private ExternalTable externalQTable;

    public ExternalTabularQLearning() throws IOException {
        super("ExternalTabularQLearning");

        alpha = DEFAULT_ALPHA;
        epsilon = DEFAULT_EPSILON;
        gamma = DEFAULT_GAMMA;
        externalQTable = new ExternalTable();
        rng = new Random();
    }

    public ExternalTable getExternalQTable() {
        return externalQTable;
    }

    /**
     * Updates the Q table entry Q(state, action), using the reward received by moving from state to newState.
     *
     * @param state Previous state.
     * @param action Action taken at previous state.
     * @param reward Reward received by transitioning to newState.
     * @param newState Resulting state after taking action at state.
     */
    @Override
    public void update(GameState state, Action action, ScalarReward reward, GameState newState) {
        super.update(state, action, reward, newState);

        Map<Action, Double> qs = externalQTable.get(state);
        Map<Action, Double> qsDash = externalQTable.get(newState);

        double maxQForNewState = -Double.MAX_VALUE;
        for (Action a : qsDash.keySet()) {
            if (!qsDash.containsKey(a)) qsDash.put(a, DEFAULT_Q_VALUE);
            double q = qsDash.get(a);
            if (q > maxQForNewState) maxQForNewState = q;
        }

        // Ensure that if table entries have been initialized for Q(S',maxA), the default value is used.
        if (qsDash.keySet().size() == 0) {
            maxQForNewState = DEFAULT_Q_VALUE;
        }

        if (!qs.containsKey(action)) qs.put(action, DEFAULT_Q_VALUE);
        double newValue = qs.get(action) + alpha * (reward.getReward() + gamma * maxQForNewState - qs.get(action));

        if (enableTraining)
            externalQTable.put(state, action, newValue);
    }

    /**
     * Greedily chooses the best action from the available actions with the greatest value in the Q table corresponding
     * to Q(state, *).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return The greedy action choice.
     */
    public Action greedyActionForState(GameState state, List<Action> availableActions) {
        double bestQ = -Double.MAX_VALUE;
        List<Action> bestActions = new ArrayList<>();

        Map<Action, Double> qs = externalQTable.get(state);

        // Iterate over actions and get the actions with the best value.
        for (Action a : availableActions) {
            if (!qs.containsKey(a)) qs.put(a, DEFAULT_Q_VALUE);

            double q = qs.get(a);

            if (q > bestQ) {
                bestQ = q;
                bestActions = new ArrayList<>();
                bestActions.add(a);
            } else if (q == bestQ) {
                bestActions.add(a);
            }
        }

        // If one action has the best value, it is chosen.
        if (bestActions.size() == 1) {
            return bestActions.get(0);
        }

        // If multiple actions are tied, choose randomly between them.
        return bestActions.get(rng.nextInt(bestActions.size()));
    }
}
