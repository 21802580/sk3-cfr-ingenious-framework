package za.ac.sun.cs.ingenious.games.cardGames.core.actions;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * The Class HiddenDrawCardMove.
 *
 * This move is sent if an other player (not the one this move is sent to)
 * drew a card from the draw pile.
 *
 * @author Joshua Wiebe
 */
public class HiddenDrawCardAction implements Action{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/** The player who drew from draw pile. */
	private int player;

	/**
	 * Instantiates a new hidden draw card move.
	 *
	 * @param player The player who drew from draw pile
	 */
	public HiddenDrawCardAction(int player) {
		this.player = player;
	}

	/**
	 * Gets the player.
	 *
	 * @return The player who had to draw from the draw pile.
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
