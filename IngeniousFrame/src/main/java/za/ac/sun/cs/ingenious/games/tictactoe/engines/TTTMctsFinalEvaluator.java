package za.ac.sun.cs.ingenious.games.tictactoe.engines;

import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.go.GoFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.search.minimax.GameEvaluator;

/**
 * The final evaluator determines the score at the end of the game. Every game must
 * implement the GameFinalEvaluator interface. Here, we also implement the GameEvaluator
 * interface, so that we can use Minimax search.
 */
public class TTTMctsFinalEvaluator extends TTTFinalEvaluator implements MctsGameFinalEvaluator<TurnBasedSquareBoard> {
	private static final double WIN_VALUE  = 1;
	private static final double LOSE_VALUE = -1;
	private static final double DRAW_VALUE = 0;

	@Override
	public double[] getMctsScore(TurnBasedSquareBoard forState) {
		double[] scores = new double[2];
		scores[0] = LOSE_VALUE;
		scores[1] = LOSE_VALUE;
		if(getWinner(forState)==-1){
			scores[0] = DRAW_VALUE;
			scores[1] = DRAW_VALUE;
		}else{
			scores[getWinner(forState)] = WIN_VALUE;
		}
		return scores;
	}

	/**
	 * Getter for the value of a win (For the MCTS result)
	 *
	 * @return 	The win value used in MCTS.
	 */
	public double getWinValue() {
		return (double) WIN_VALUE;
	}

	/**
	 * Getter for the value of a draw (For the MCTS result).
	 *
	 * @return 	The draw value used in MCTS.
	 */
	public double getDrawValue() {
		return (double) DRAW_VALUE;
	}

	/**
	 * Getter for the value of a loss (For the MCTS result).
	 *
	 * @return 	The loss value used in MCTS.
	 */
	public double getLossValue() {
		return (double) LOSE_VALUE;
	}

	/**
	 * @param forState Some terminal state
	 * @return The ID of the player who won in that state, or -1 if it is a draw.
	 */
	public int getWinner(TurnBasedSquareBoard forState) {
		for(int i = 0; i < 9; i= i+3){
			if(forState.board[i+0] != 0 &&  forState.board[i+0] == forState.board[i+1] && forState.board[i+1] == forState.board[i+2]) {
				return forState.board[i+0]-1;
			}
		}
		for(int i = 0; i < 3; i++){
			if(forState.board[i+0] != 0 && forState.board[i+0] == forState.board[i+3] && forState.board[i+3] == forState.board[i+6]) {
				return forState.board[i+0]-1;
			}
		}
		if(forState.board[0] != 0 && forState.board[0] == forState.board[4] && forState.board[4] == forState.board[8]) {
			return forState.board[0]-1;
		}
		if(forState.board[2] != 0 && forState.board[2] == forState.board[4] && forState.board[4] == forState.board[6]) {
			return forState.board[2]-1;
		}
		return -1;
	}
}
