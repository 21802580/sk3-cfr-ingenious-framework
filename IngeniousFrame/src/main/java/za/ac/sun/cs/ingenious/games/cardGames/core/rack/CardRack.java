package za.ac.sun.cs.ingenious.games.cardGames.core.rack;

import com.esotericsoftware.minlog.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The Class CardRack.
 * 
 * Just a wrapper for an ArrayList of cards.
 * 
 * Create a new rack. Then add/remove cards to it. 
 * 
 * It can for instance be used to create an initial rack and send it to a specific player.
 *
 * @param <F1> Generic type of first feature (implements CardFeature).
 * @param <F2> Generic type of second feature (implements CardFeature).
 * 
 * @author Joshua Wiebe
 */
public class CardRack<F1 extends CardFeature, F2 extends CardFeature> implements Serializable, Iterable<Card<F1, F2>>{
	
	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** ArrayList of cards. */
	private ArrayList<Card<F1, F2>> cards;
	
	/**
	 * Instantiates a new card rack (ArrayList).
	 */
	public CardRack(){
		cards = new ArrayList<>();
	}
	
	/**
	 * Adds the card.
	 *
	 * @param New card
	 */
	public void addCard(Card<F1, F2> newCard){
		cards.add(newCard);
	}
	
	/**
	 * Removes a card.
	 *
	 * @param  Card to remove.
	 */
	public void removeCard(Card<F1, F2> toRemove){
		cards.remove(toRemove);
	}
	
	/**
	 * Gets the card.
	 *
	 * @param index of the required card.
	 * @return Card at given index.
	 */
	public Card<F1, F2> getCard(int index){
		return cards.get(index);
	}
	
	/**
	 * Size.
	 *
	 * @return size of the rack.
	 */
	public int size(){
		return cards.size();
	}
	
	/**
	 * Check if rack contains a specific card.
	 *
	 * @param cardToCheck card to check.
	 * @return true, card is contained.
	 */
	public boolean contains(Card<F1, F2> cardToCheck){
		for(Card<F1, F2> card : cards){
			if(card.equals(cardToCheck)){
				return true;
			}
		}
		return false;
	}

	/**
	 * @see java.lang.Iterable#iterator()
	 * 
	 * @return Returns the iterator of the underlying ArrayList
	 */
	@Override
	public Iterator<Card<F1, F2>> iterator() {
		return cards.iterator();
	}
	
	/**
	 * Prints all cards in the rack.
	 */
	public void printCards(){
		for(Card<F1, F2> card : cards){
			Log.info(card.toString());
		}
	}
}
