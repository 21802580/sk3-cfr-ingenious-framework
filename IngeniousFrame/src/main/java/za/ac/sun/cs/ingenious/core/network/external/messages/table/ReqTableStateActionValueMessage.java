package za.ac.sun.cs.ingenious.core.network.external.messages.table;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalState;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to request action values for a provided state from the table in the Python application.
 * @author Steffen Jacobs
 */
public class ReqTableStateActionValueMessage extends ExternalMessage {
    Map<String, Object> payload;

    public ReqTableStateActionValueMessage(GameState state, int action) {
        super(ExternalMessageType.REQ_TABLE_STATE_ACTION_VALUE);

        payload = new HashMap<>();
        payload.put("state", new ExternalState(state));
        payload.put("action", action);
    }
}
