package za.ac.sun.cs.ingenious.core.util.tools;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * A utility for evenly splitting an array into a 2D-array.
 * Example use: Distributing tasks amoung threads.
 * 
 * @author Nicholas Robinson
 */
public class ArrayPartitioner {

    private ArrayPartitioner() {}

    public static byte[][] partition(byte[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        byte[][] partitions = new byte[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static short[][] partition(short[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        short[][] partitions = new short[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static int[][] partition(int[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        int[][] partitions = new int[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static long[][] partition(long[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        long[][] partitions = new long[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static float[][] partition(float[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        float[][] partitions = new float[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static double[][] partition(double[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        double[][] partitions = new double[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static boolean[][] partition(boolean[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        boolean[][] partitions = new boolean[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static char[][] partition(char[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        char[][] partitions = new char[numberOfPartitions][];
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            partitions[p] = Arrays.copyOfRange(array, a, a += Math.min(maxPartitionSize, array.length - a));
        }

        return partitions;
    }

    public static <T> T[][] partition(T[] array, int minNumberOfPartitions, int maxPartitionSize) {

        int numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        while (numberOfPartitions < minNumberOfPartitions && maxPartitionSize > 1) {
            maxPartitionSize--;
            numberOfPartitions = (int) Math.ceil((double)array.length / maxPartitionSize);
        }

        @SuppressWarnings("unchecked")
        T[][] partitions = (T[][]) Array.newInstance(array.getClass().getComponentType(), numberOfPartitions, maxPartitionSize);
     
        for (int a = 0, p = 0; p < partitions.length; p++) {
            if (array.length - a >= maxPartitionSize) {
                System.arraycopy(array, a, partitions[p], 0, maxPartitionSize);
                a += maxPartitionSize;
            }
            else {
                partitions[p] = Arrays.copyOfRange(array, a, a += array.length - a);
            }
        }

        return partitions;
    }
}