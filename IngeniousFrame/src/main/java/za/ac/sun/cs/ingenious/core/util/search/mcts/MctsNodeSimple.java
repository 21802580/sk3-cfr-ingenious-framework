package za.ac.sun.cs.ingenious.core.util.search.mcts;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.search.TreeNode;
import za.ac.sun.cs.ingenious.core.util.search.TreeNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsRootNode;
import java.util.List;
import java.util.ArrayList;

public class MctsNodeSimple<S extends GameState> implements MctsNodeCompositionInterface<S, MctsNodeSimple<S>, MctsNodeSimple<S>>, MctsRootNode<MctsNodeSimple<S>> {

	TreeNode<S, MctsNodeSimple<S>, MctsNodeSimple<S>> treeNode;
	int visitCount;
	List<Action> unexploredChildren = null;
	Action prevAction;
	private int playerID;

	public MctsNodeSimple(S state, Action prevAction, MctsNodeSimple<S> parent, List<MctsNodeSimple<S>> children, GameLogic<S> logic, int playerID) {
		this.playerID = playerID;
		treeNode = new TreeNodeComposition<S, MctsNodeSimple<S>, MctsNodeSimple<S>>(state, parent, children);
		this.prevAction = prevAction;
		this.visitCount = 0;
		for (int i : logic.getCurrentPlayersToAct(state)) {
			if (unexploredChildren == null) {
				unexploredChildren = logic.generateActions(state, i);
			} else {
				unexploredChildren.addAll(logic.generateActions(state, i));
			}
		}
	}

	public int getPlayerID() {
		return playerID;
	}

	// SearchNode interface:
	public S getState() {
		return treeNode.getState();
	}

	public double getValue() {
		return treeNode.getValue();
	}

	public void addValue(double add) {
		treeNode.addValue(add);
	}

	// TODO: change?
	public String toString() {
		String parentString = (this.getParent() == null) ? "null" : this.getParent().getState().toString();

		return ("TreeNodeNode:\n\tState = " + getState().toString() + "\nprevAction = " + getPrevAction() + "\n\tvalue = " + getValue() +
				"\n\tParent (state) = " + parentString + "\n\tChildren size = " + getChildren().size() +
				"\n\tvisitCount = " + getVisitCount() + "\n\tUnexploredChildren = " + getUnexploredChildren());
	}

	// TreeNode interface:
	public MctsNodeSimple<S> getParent() {
		return treeNode.getParent();
	}

	public void setParent(MctsNodeSimple<S> parent) {
		treeNode.setParent(parent);
	}

	public List<MctsNodeSimple<S>> getChildren() {
		return treeNode.getChildren();
	}

	public void setChildren(List<MctsNodeSimple<S>> children) {
		treeNode.setChildren(children);
		for (MctsNodeSimple<S> child : children) {
			this.getUnexploredChildren().remove(child.getPrevAction());
		}
	}

	// FIXME: when a child is added, need to check that child is not already a
	// child, if it is, merge info, if it's not add and remove prevAction from
	// unexploredactions...
	public void addChild(MctsNodeSimple<S> child) {
		treeNode.addChild(child);
		this.getUnexploredChildren().remove(child.getPrevAction());
	}

	public void addChildren(List<MctsNodeSimple<S>> newChildren) {
		treeNode.addChildren(newChildren);
	}

	// MctsNodeCompositionInterface interface:
	public double getVisitCount() {
		return this.visitCount;
	}

	public void incVisitCount() {
		visitCount++;
	}

	public void decVisitCount() {
		visitCount--;
	}

	public void incChildVisitCount(MctsNodeSimple<S> child) {
		child.incVisitCount();
	}

	public double getChildVisitCount(MctsNodeSimple<S> child) {
		return child.getVisitCount();
	}

	public double getChildValue(MctsNodeSimple<S> child) {
		return child.getValue();
	}

	public List<Action> getUnexploredChildren() {
		return this.unexploredChildren;
	}

	public boolean isUnexploredEmpty() {
		return unexploredChildren.isEmpty();
	}

	public MctsNodeSimple<S> getAnUnexploredChild(GameLogic<S> logic) {

		if (isUnexploredEmpty()) {
			return null;
		}

		// getState() returns defensive copy, so this is allowed/not alias:
		S childState = getState();
		Action unexploredAction = unexploredChildren.remove((int) (Math.random() * unexploredChildren.size()));
		logic.makeMove(childState, unexploredAction);

		int alteratePlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeSimple<S>(childState, unexploredAction, this, new ArrayList<MctsNodeSimple<S>>(), logic, alteratePlayer);
	}

	public MctsNodeSimple<S> getAnUnexploredChild(GameLogic<S> logic, int index) {

		if (isUnexploredEmpty()) {
			return null;
		}

		// getState() returns defensive copy, so this is allowed/not alias:
		S childState = getState();
		Action unexploredAction = unexploredChildren.remove(index);
		logic.makeMove(childState, unexploredAction);

		int alteratePlayer = alternatePlayer(this.getPlayerID());
		return new MctsNodeSimple<S>(childState, unexploredAction, this, new ArrayList<MctsNodeSimple<S>>(), logic, alteratePlayer);
	}

	public int alternatePlayer(int player) {
		if (player == 0) {
			return 1;
		} else if (player == 1) {
			return 0;
		} else {
			System.out.println("Shouldn't get here, MctsNodeTreeParallel invalid player");
			return -1;
		}
	}

	public Action getPrevAction() {
		return this.prevAction;
	}

	public MctsNodeSimple<S> getSelfAsChildType() {
		return this;
	}

	public void merge(MctsNodeSimple<S> other) {
		mergeHelper(other, false);
	}

	private boolean mergeHelper(MctsNodeSimple<S> other, boolean isChild) {

		if (!this.getState().equals(other.getState())) {
			return false;
		}

		this.addValue(other.getValue());
		for (int i = 0; i < other.getVisitCount(); i++) {
			this.incVisitCount();
		}

		if (isChild) return true;

		List<MctsNodeSimple<S>> thisKiddies = this.getChildren();
		List<MctsNodeSimple<S>> otherKiddies = other.getChildren();
		if (thisKiddies.isEmpty()) {
			this.setChildren(otherKiddies);
			return true;
		}

		for (MctsNodeSimple<S> otherKid : otherKiddies) {
			boolean kidMerged = false;
			for (MctsNodeSimple<S> thisKid : thisKiddies) {
				if (thisKid.getState().equals(otherKid.getState())) {
					thisKid.mergeHelper(otherKid, true);
					kidMerged = true;
					break;
				}
			}
			if (!kidMerged) this.addChild(otherKid);
		}

		return true;
	}
}

// TODO: Comments!
