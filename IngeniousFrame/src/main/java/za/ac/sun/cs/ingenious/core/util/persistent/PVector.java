
package za.ac.sun.cs.ingenious.core.util.persistent;

/**
 * The PVector interface — Persistent Vector — is an interface for fully persistent functional data structures.
 * This interface mimics the functionality of a dynamic array, and is loosely modeled after the List interface in Java Collections.
 * 
 * <p> A functional data structure is a structure that is immutable, 
 * i.e. an instance of it cannot be modified after it has been created.
 * The mutator methods in this interface will instead return a new 
 * instance of the structure that appears similar to the original structure, except with the mutations applied to it.
 * The previous structures that a new structure is derived from will remain unchanged.
 * 
 * <p> As a general rule all items stored in a PVector should be effectively immutable
 * 
 * <p> All classes that implement this interface should be thread safe.
 */
public interface PVector<T> {
    
    /**
     * Returns the item at the specified index position in this PVector
     * 
     * @param index index of the item to return
     * @return the item at the specified position in this PVector
     */
    public T get(int index);

    /**
     * Returns a PVector with the item at the specified position of this PVector replaced with
     * the specified item. The original PVector is unchanged by this action.
     * 
     * @param index index of the item to replace
     * @param value item to be stored at the specified position
     * @return A PVector holding the specified item at the specified position
     */
    public PVector<T> set(int index, T item);

    /**
     * Returns a PVector with multiple items at the specified positions of this PVector replaced with the specified items. 
     * The original PVector is unchanged by this action.
     * 
     * <p> Should the user desire to obtain a PVector with multiple variations in its contents then, depending on implementation,
     * it may be cheaper to call this method instead of calling the {@link #set(int, T)} method for each desired modification.
     * 
     * <p> There must be a one-to-one mapping between {@code indices} and {@code items}.
     * 
     * <p> If there are duplicate indices, then the latest corresponding item should be assigned to that index.
     * 
     * @param indices indices of the items to replaced
     * @param items items to be stored at the specified positions
     * @return A PVector holding the specified items at the specified positions
     */
    public default PVector<T> multiSet(int[] indices, T[] items) { 
        // default implementation simply calls the set operation multiple times
        PVector<T> pv = this;
        for (int i = 0; i < indices.length; i++) {
            pv = pv.set(indices[i], items[i]);
        }
        return pv;
    };

    /**
     * A varient of the {@link #multiSet(int[], T[])} method where the user may use varargs for both parameters instead of raw arrays
     * 
     * <p> Returns a PVector with multiple items at the specified positions of this PVector replaced with the specified items. 
     * The original PVector is unchanged by this action.
     * 
     * <p> Should the user desire to obtain a PVector with multiple variations in its contents then, depending on implementation,
     * it may be cheaper to call this method instead of calling the {@link #set(int, T)} method for each desired modification.
     * 
     * <p> There must be a one-to-one mapping between {@code indices} and {@code items}. 
     * 
     * @param indices indices of the items to replaced
     * @return an object containing a single function {@link MultiSetter#to(T...)} which is called to complete the method call
     */
    public default MultiSetter<T> multiSet(int... indices)
    {
        return new MultiSetter<T>(this, indices);
    }

    /**
     * an object containing a single function {@link MultiSetter#to(T...) .to(T...)} which is called to complete 
     * the {@link #multiSet(int..)} with varargs method call
     */
    public static final class MultiSetter<T>
    {
        private PVector<T> encloser;
        private int[] indices;

        private MultiSetter(PVector<T> encloser, int[] indices)
        {
            this.encloser = encloser;
            this.indices = indices;
        }

        /**
         * Completes the {@link #multiSet(int..)} call.
         * 
         * <p> Returns a PVector with multiple items at the specified positions of this PVector replaced with the specified items. 
         * The original PVector is unchanged by this action.
         * 
         * <p> Should the user desire to obtain a PVector with multiple variations in its contents then, depending on implementation,
         * it may be cheaper to call this method instead of calling the {@link PVector#set(int, T) set(int, T)} method for each desired modification.
         * 
         * <p> There must be a one-to-one mapping between {@code indices} and {@code items}. 
         * 
         * @param items items to be stored at the specified positions
         * @return A PVector holding the specified items at the specified positions
         */
        @SafeVarargs
        public final PVector<T> to(T... items)
        {
            return encloser.multiSet(indices, items);
        }
    }

    /**
     * Returns an Iterable element to iterate over the PVector starting and ending at the specified indices.
     * 
     * @param a start index (inclusive)
     * @param b end index (exclusive)
     * @return An Iterable element over the specified indices
     */
    public PVectorIterable<T> range(int a, int b);

    /**
     * Returns an Iterable element to iterate over all the contents of the PVector.
     * @return An Iterable element over the contents of this PVector.
     */
    public PVectorIterable<T> items();
}