package za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.tools.ArrayPartitioner;

/**
 * An abstract module that unites MNK logic with the SSS* search algorithm to produce an agent for generating moves.
 * 
 * Nicholas Robinson
 */
public abstract class MNKSSSStrategy extends MNKLogic {

    // final static definitions for player tokens, so that we do not create a new Integer object for each move.
    final static Integer player1 = 1;
    final static Integer player2 = 2;

    final static int VICTORYSCORE = 2000000000;

    public MNKSSSStrategy(int m, int n, int k) {
        super(m, n, k);
    }

    /**
     * int -> Integer
     */
    public Integer player(int player) {
        switch (player) {
            case 1: return player1;
            case 2: return player2;
            default: return null;
        }
    }

    boolean gameHasWinner(int score) {
        return score >= 0 ? score > (VICTORYSCORE / 2) : score < -(VICTORYSCORE / 2);
    }
    

    /**
     * Functional interface to be configured for calling the SSS* search function.
     */
    @FunctionalInterface
    interface MoveSearcherFunction {
        SearchResult search(int[] moves, int bound, int numberOfAvailableMoves);
    }

    /**
     * Common set of actions to find the best player action for the given board state.
     * Evenly distributes a given board state into a set of tasks to be called by an executor service.
     * Tasks call the given searchFunction on a set of moves. 
     * This utilizes the SSS* search bound parameter to perform additional pruning.
     * 
     * @param player the player who's turn it is to move.
     * @param board the board state.
     * @param executor executor service.
     * @param threadCount the number of threads used in the executor service.
     * @param partitionSize the number of moves to batch together in an SSS* call.
     * @param searchFunction a lambda function to call the SSS* search for a given set of moves, and a bound.
     * @return the board position of the best possible move that was found, or -1 if no such move was found.
     */
    int generateMove(int player, PVector<Integer> board, ExecutorService executor, int threadCount, int partitionSize, MoveSearcherFunction searchFunction) {
        AtomicInteger bound = new AtomicInteger(Integer.MIN_VALUE);
        int[] availableMoves = availableMoves(board);
        int[][] partitions = ArrayPartitioner.partition(availableMoves, threadCount, partitionSize);
        
        List<Callable<SearchResult>> tasks = new ArrayList<Callable<SearchResult>>(partitions.length);

        for (int[] moves : partitions) tasks.add(new Callable<SearchResult>(){
                @Override
                public SearchResult call() throws Exception {
                    SearchResult result = searchFunction.search(moves, bound.get(), availableMoves.length);

                    if (result != null) {
                        bound.getAndUpdate((b) -> result.score > b ? result.score : b);
                    }
                    
                    return result;
                }
        });
        
        try {
            List<Future<SearchResult>> futureResults = executor.invokeAll(tasks);
            SearchResult best = null;

            for (Future<SearchResult> fr : futureResults) {
                SearchResult result = fr.get();
                if (result != null && (best == null || result.score > best.score)) best = result;
            }

            return best.move;

        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        return -1;
    }

    /**
     * Results of a search, returned by the MoveSearcherFunction.
     */
    public class SearchResult {

        /**
         * The position of the best found move.
         */
        private final int move;

        /**
         * The score of the move.
         */
        private final int score;
        
        /**
         * The number of iterations performed during the search.
         */
        private final int iterations;

        public int getMove() {
            return move;
        }

        public int getScore() {
            return score;
        }

        public int getIterations() {
            return iterations;
        }

        SearchResult(int move, int score, int iterations) {
            this.move = move;
            this.score = score;
            this.iterations = iterations;
        }

        @Override
        public String toString() {
            return "{ move: " + move + " score: " + score + " iterations: " + iterations + " }";
        }
    }
}