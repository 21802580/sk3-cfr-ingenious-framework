package za.ac.sun.cs.ingenious.core.util.persistent;

/**
 * An Iterable for obtaining PVectorIterators
 * 
 * @author Nicholas Robinson
 */
public interface PVectorIterable<T> extends Iterable<T>
{
    public PVectorIterator<T> iterator();
}