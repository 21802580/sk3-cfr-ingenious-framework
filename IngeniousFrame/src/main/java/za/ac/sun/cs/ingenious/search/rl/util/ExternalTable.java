package za.ac.sun.cs.ingenious.search.rl.util;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.external.ExternalComms;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExitMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.table.ReqTableStateActionValueMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.table.ReqTableStateActionsMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.table.SetTableStateActionValueMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.table.TableStateActionValueMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.table.TableStateActionsMessage;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Class that abstracts communication detail with the Python application away from the external tabular Q-learning
 * implementation.
 *
 * @author Steffen Jacobs
 */
public class ExternalTable {
    private final ExternalComms externalComms;
    private final Map<Action, Integer> actionMap;
    private final Map<Integer, Action> reverseActionMap;
    private int nextActionNumber;
    private final HashSet<GameState> knownStates;

    public ExternalTable() throws IOException {
        // TODO (#289): Would work nicely if the external Python app could somehow be launched from Java.

        externalComms = new ExternalComms();
        actionMap = new HashMap<>();
        reverseActionMap = new HashMap<>();
        nextActionNumber = 1;
        knownStates = new HashSet<>();
    }

    private void ensureActionIsMapped(Action action) {
        if (!actionMap.containsKey(action)) {
            actionMap.put(action, nextActionNumber);
            reverseActionMap.put(nextActionNumber++, action);
        }
    }

    public Map<Action, Double> get(GameState state) {
        Gson gson = new Gson();
        String message = gson.toJson(new ReqTableStateActionsMessage(state));
        externalComms.send(message);

        String map = externalComms.receive();
        TableStateActionsMessage response = gson.fromJson(map, TableStateActionsMessage.class);

        Map<Action, Double> actionValues = new HashMap<>();
        for (int key : response.getPayload().keySet()) {
            actionValues.put(reverseActionMap.get(key), response.getPayload().get(key));
        }

        return actionValues;
    }

    public Double get(GameState state, Action action) {
        ensureActionIsMapped(action);

        Gson gson = new Gson();
        String message = gson.toJson(new ReqTableStateActionValueMessage(state, actionMap.get(action)));

        Log.info("Sending: " + message);
        externalComms.send(message);

        String map = externalComms.receive();
        TableStateActionValueMessage response = gson.fromJson(map, TableStateActionValueMessage.class);
        return response.getPayload();
    }

    public void put(GameState state, Action action, double value) {
        ensureActionIsMapped(action);

        knownStates.add(state);

        Gson gson = new Gson();
        String message = gson.toJson(new SetTableStateActionValueMessage(state, actionMap.get(action), value));
        externalComms.send(message);
        externalComms.receive();
    }

    public void close() {
        Gson gson = new Gson();
        String message = gson.toJson(new ExitMessage());
        Log.info("Exit message: " + message);
        externalComms.send(message);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        externalComms.close();
    }

    public Map<Action, Integer> getActionMap() {
        return actionMap;
    }

    public Map<Integer, Action> getReverseActionMap() {
        return reverseActionMap;
    }

    public HashSet<GameState> getKnownStates() {
        return knownStates;
    }
}
