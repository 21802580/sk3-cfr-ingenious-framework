package za.ac.sun.cs.ingenious.core.network.external;

import com.esotericsoftware.minlog.Log;

import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.BindException;
import java.net.Socket;
import java.net.ServerSocket;
import java.io.IOException;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * This object sends and receives String encodings over a specified socket. The
 * primary use case for this object is to facilitate communication between an
 * Engine and an external trainer.
 *
 * TODO - Issue #259: make this generic (allow it to send non-String objects over the socket
 * - one will probably need to use gRPC to do this).
 *
 * @author Elan van Biljon
 */
public class ExternalComms {
	private static final int INITIAL_PORT = 9999;
	protected Runnable send;
	protected Runnable receive;
	protected Socket client;
	protected ServerSocket server;
	protected ExecutorService executor;
	protected PrintWriter outWriter;
	protected BufferedReader socketIn;

	/**
	 * As much of this code is asynchronous, we need buffers to store
	 * information before it is sent and as it is received.
	 */
	protected String inMessage = "";
	protected String outMessage = "";

	/**
	 * Creates the server socket by attempting to connect via the default port. The port used is repeatedly incremented
	 * followed by reattempting to connect should the port be in use.
	 */
	public ExternalComms() {
		int port = INITIAL_PORT;

		while (server == null) {
			try {
				Log.info("ExternalComms: Attempting to host on port " + port + ".");
				server = new ServerSocket(port);
				Log.info("ExternalComms: Host successful on port " + port + ".");
			} catch (BindException e) {
				Log.info("ExternalComms: Port " + port + " already in use.");
				port++;
				continue;
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}

		// a blocking call that will wait for a client to connect
		connectClient();

		try {
			outWriter = new PrintWriter(client.getOutputStream(), true);
			socketIn = new BufferedReader(new InputStreamReader(client.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		executor = Executors.newFixedThreadPool(5);

		receive = new Runnable() {
			@Override
			public void run() {
				receiveBlock();
			}
		};

		send = new Runnable() {
			@Override
			public void run() {
				outWriter.print(outMessage + "\r\n");
				outWriter.flush();
			}
		};

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				close();
			}
		});
	}

	public void close() {
		try {
			outWriter.close();
			socketIn.close();
			client.close();
			server.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a message with length prefix.
	 *
	 * @param message Message to be sent.
	 */
	public void send(String message) {
		outMessage = message.length() + "::" + message;
		executor.execute(send);
	}

	public void receiveBlock() {
		try {
			inMessage = socketIn.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String receive() {
		receiveBlock();
		return inMessage;
	}

	public void receiveAsync() {
		executor.execute(receive);
	}

	public void connectClient() {
		try {
			client = server.accept();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
