package za.ac.sun.cs.ingenious.search.rl.sarsa;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.util.ReinforcementLearningInstrumentation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Class containing an implementation of on-policy SARSA.
 *
 * @author Steffen Jacobs
 */
public class TabularSarsa extends ReinforcementLearningInstrumentation {
    private static final double DEFAULT_ALPHA = 0.6;
    private static final double DEFAULT_EPSILON = 0.1;
    private static final double DEFAULT_GAMMA = 1.0;
    private static final double DEFAULT_Q_VALUE = 0;

    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_WIDTH = 5;
    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_HEIGHT = 5;

    /**
     * Random number generator.
     */
    private Random rng;

    /**
     * Step size
     */
    private double alpha;

    /**
     * Probability of taking a random action.
     */
    private double epsilon;

    /**
     * Discount factor.
     */
    private double gamma;

    /**
     * Table containing estimated expected reward Q(S, A).
     */
    private Map<GameState, Map<Action, Double>> qTable;

    public TabularSarsa() {
        alpha = DEFAULT_ALPHA;
        epsilon = DEFAULT_EPSILON;
        gamma = DEFAULT_GAMMA;
        qTable = new HashMap<>();
        rng = new Random();
    }

    public TabularSarsa(String identifier, double alpha, double epsilon, double gamma) {
        super(identifier);

        this.alpha = alpha;
        this.epsilon = epsilon;
        this.gamma = gamma;

        qTable = new HashMap<>();
        rng = new Random();
    }

    public TabularSarsa withAlpha(double alpha) {
        this.alpha = alpha;
        return this;
    }

    public TabularSarsa withEpsilon(double epsilon) {
        this.epsilon = epsilon;
        return this;
    }

    public TabularSarsa withGamma(double gamma) {
        this.gamma = gamma;
        return this;
    }

    public TabularSarsa withRandom(Random random) {
        this.rng = random;
        return this;
    }

    public TabularSarsa withQTable(Map<GameState, Map<Action, Double>> qTable) {
        this.qTable = qTable;
        return this;
    }

    public Map<GameState, Map<Action, Double>> getQTable() {
        return qTable;
    }

    /**
     * Updates the Q table entry Q(state, action), using the reward received by moving from state to newState and taking
     * an action from the new state. The new state / new action pair are used to update the Q table (i.e. unlike
     * Q-Learning, this bootstraps).
     *
     * @param state Previous state.
     * @param action Action taken at previous state.
     * @param reward Reward received by transitioning to newState.
     * @param newState Resulting state after taking action at state.
     * @param newStateActions List of available actions to choose from the new state.
     */
    public void update(GameState state, Action action, ScalarReward reward, GameState newState, List<Action> newStateActions) {
        addReward(reward.getReward());

        if (!qTable.containsKey(state)) qTable.put(state, new HashMap<>());
        if (!qTable.containsKey(newState)) qTable.put(newState, new HashMap<>());

        Map<Action, Double> qs = qTable.get(state);
        Map<Action, Double> qsDash = qTable.get(newState);

        Action aDash = chooseAction(newState, newStateActions, false);

        if (!qs.containsKey(action)) qs.put(action, DEFAULT_Q_VALUE);
        if (!qsDash.containsKey(aDash)) qsDash.put(aDash, DEFAULT_Q_VALUE);
        double newValue = qs.get(action) + alpha * (reward.getReward() + gamma * qsDash.get(aDash) - qs.get(action));

        qs.put(action, newValue);

//        printQTableGrid(MDP_GRID_WIDTH, MDP_GRID_HEIGHT);
    }

    /**
     * Choose an action from the list of available actions corresponding to the game state. Chooses a random action
     * with probability epsilon, otherwise greedily chooses the action with the best value in the Q table corresponding
     * to Q(S,*).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return A chosen action.
     */
    public Action chooseAction(GameState state, List<Action> availableActions, boolean incrementsStep) {
        if (incrementsStep) incrementStep();

        Action choice = null;

        if (rng.nextDouble() < this.epsilon) {
            choice = randomAction(availableActions);
        } else {
            choice = greedyActionForState(state, availableActions);
        }

//        Log.info("TabularSarsa Choice for state={" + state + "}: " + choice);
        return choice;
    }

    public Action chooseAction(GameState state, List<Action> availableActions) {
        return chooseAction(state, availableActions, true);
    }

    /**
     * Greedily chooses the best action from the available actions with the greatest value in the Q table corresponding
     * to Q(state, *).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return The greedy action choice.
     */
    public Action greedyActionForState(GameState state, List<Action> availableActions) {
        // If this table row does not exist, initialize it.
        if (!qTable.containsKey(state)) qTable.put(state, new HashMap<>());

        double bestQ = -Double.MAX_VALUE;
        List<Action> bestActions = new ArrayList<>();

        Map<Action, Double> qs = qTable.get(state);

        // Iterate over actions and get the actions with the best value.
        for (Action a : availableActions) {
            if (!qs.containsKey(a)) qs.put(a, DEFAULT_Q_VALUE);
            double q = qs.get(a);

            if (q > bestQ) {
                bestQ = q;
                bestActions = new ArrayList<>();
                bestActions.add(a);
            } else if (q == bestQ) {
                bestActions.add(a);
            }
        }

        // If one action has the best value, it is chosen.
        if (bestActions.size() == 1) {
            return bestActions.get(0);
        }

        // If multiple actions are tied, choose randomly between them.
        return bestActions.get(rng.nextInt(bestActions.size()));
    }

    /**
     * Chooses an action from the list of available actions randomly.
     *
     * @param availableActions List of actions available.
     * @return A random action.
     */
    public Action randomAction(List<Action> availableActions) {
        return availableActions.get(rng.nextInt(availableActions.size()));
    }

    public void printQTable() {
        Log.info("Q Table: \n");
        Set<GameState> keySet = qTable.keySet();

        Log.info("==========================");
        for (GameState state : keySet) {
            Set<Action> actionSet = qTable.get(state).keySet();

            if (actionSet.size() == 0) continue;

            Log.info(state + "\t");

            for (Action action : actionSet) {
                Log.info("\t" + action + " " + qTable.get(state).get(action) + "\t");
            }
        }
        Log.info("==========================");
    }

    /**
     * This method is rather hardcoded, but for the moment is used to print gridworld MDPs.
     *
     * @param w
     * @param h
     */
    public void printQTableGrid(int w, int h) {
        Log.info("Q Table: \n");
        Set<GameState> keySet = qTable.keySet();

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Map<Action, Double> qs = qTable.get(state);

                if (qs == null) {
                    sb.append("0.00\t");
                    continue;
                }

                Set<Action> actionSet = qs.keySet();
                if (actionSet.size() == 0) {
                    sb.append("0.00\t");
                    continue;
                }

                Action maxA = greedyActionForState(state, new ArrayList<>(actionSet));
                sb.append(df.format(qTable.get(state).get(maxA))).append("\t");
            }
            sb.append("\n\n");
        }

        sb.append("-------------------------\n\n");

        k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Map<Action, Double> qs = qTable.get(state);

                if (qs == null) {
                    sb.append("• ");
                    continue;
                }

                Set<Action> actionSet = qs.keySet();
                if (actionSet.size() == 0) {
                    sb.append("• ");
                    continue;
                }

                Action maxA = greedyActionForState(state, new ArrayList<>(actionSet));

                switch (((DiscreteAction) maxA).getActionNumber()) {
                    case 0:
                        sb.append("↑ ");
                        break;
                    case 1:
                        sb.append("→ ");
                        break;
                    case 2:
                        sb.append("↓ ");
                        break;
                    case 3:
                        sb.append("← ");
                        break;
                }
            }
            sb.append("\n");
        }

        sb.append("\n\n");

        Log.info(sb);
        Log.info("==========================");
    }
}
