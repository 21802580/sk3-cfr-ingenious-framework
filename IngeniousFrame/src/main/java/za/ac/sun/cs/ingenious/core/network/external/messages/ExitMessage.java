package za.ac.sun.cs.ingenious.core.network.external.messages;

import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to indicate to the Python application when the socket will be closed.
 *
 * @author Steffen Jacobs
 */
public class ExitMessage extends ExternalMessage {
    public ExitMessage() {
        super(ExternalMessageType.EXIT);
    }
}
