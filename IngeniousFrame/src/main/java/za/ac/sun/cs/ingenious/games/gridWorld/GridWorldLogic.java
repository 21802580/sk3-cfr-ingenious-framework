package za.ac.sun.cs.ingenious.games.gridWorld;

import com.esotericsoftware.minlog.Log;
import com.google.common.collect.ImmutableMap;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldLevelParser;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import static za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState.CELL_REACHABLE;
import static za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState.CELL_UNREACHABLE;

/**
 * Class containing the turn-based game logic for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldLogic implements TurnBasedGameLogic<GridWorldState> {
    public static final int MOVEMENT_UNIT = 1;

    public static final double STOCHASTIC_WIND_DECREMENT_PROBABILITY = 0.25;
    public static final double STOCHASTIC_WIND_INCREMENT_PROBABILITY = 0.25;
    public static final int STOCHASTIC_WIND_DELTA = 1;

    public enum NavigationType {
        CARDINAL,
        ORDINAL,
        PRINCIPAL,
    }

    private final NavigationType navigationType;

    private final Set<Coord> terminalPositions;

    private Random rng;

    private Set<Coord> portalCoordinates;

    private Map<Integer, GridWorldLevelParser.WindDefinition> columnWindIntensity;
    private Map<Integer, GridWorldLevelParser.WindDefinition> rowWindIntensity;
    private Set<Coord> terminalObjectives;
    private Set<Coord> terminalPenalties;

    public GridWorldLogic(GridWorldLevelParser parser) {
        navigationType = NavigationType.values()[parser.getNavigationType()];
        terminalPositions = parser.getTerminalCoordinates();

        rng = new Random();
        portalCoordinates = parser.getPortalCoordinates();
        columnWindIntensity = parser.getColumnWindIntensity();
        rowWindIntensity = parser.getRowWindIntensity();

        terminalPenalties = new HashSet<>();
        terminalObjectives = new HashSet<>();
        for (Coord rewardCoord : parser.getRewards().keySet()) {
            if (parser.getRewards().get(rewardCoord) < 0 && terminalPositions.contains(rewardCoord)) {
                terminalPenalties.add(rewardCoord);
            } else if (parser.getRewards().get(rewardCoord) > 0 && terminalPositions.contains(rewardCoord)) {
                terminalObjectives.add(rewardCoord);
            }
        }
    }

    public Set<Coord> getTerminalPositions() {
        return terminalPositions;
    }

    public Set<Coord> getPortalCoordinates() {
        return portalCoordinates;
    }

    public Map<Integer, GridWorldLevelParser.WindDefinition> getColumnWindIntensity() {
        return columnWindIntensity;
    }

    public Map<Integer, GridWorldLevelParser.WindDefinition> getRowWindIntensity() {
        return rowWindIntensity;
    }

    public Set<Coord> getTerminalObjectives() {
        return terminalObjectives;
    }

    public Set<Coord> getTerminalPenalties() {
        return terminalPenalties;
    }

    @Override
    public boolean validMove(GridWorldState fromState, Move move) {
        if (move == null) {
            return false;
        }

        if (move instanceof CompassDirectionAction) {
            CompassDirectionAction action = (CompassDirectionAction) move;

            if (navigationType == NavigationType.CARDINAL && action.getDir().isOrdinal())
                return false;

            if (navigationType == NavigationType.ORDINAL && action.getDir().isCardinal())
                return false;

            if (!action.getDir().isPrincipal())
                return false;

            Coord playerCoords = fromState.findPlayerCoords(move.getPlayerID());
            Coord destination = playerCoords.add(action.getDir(), MOVEMENT_UNIT);

            boolean result = canMoveToCell(fromState, destination.getX(), destination.getY());
            return result;
        } else if (move instanceof XYAction) {
            XYAction action = (XYAction) move;
            return canMoveToCell(fromState, action.getX(), action.getY());
        }

        return false;
    }

    /**
     * Determines if the specified coordinates are a valid movement destination in the specified state.
     *
     * @param state The state.
     * @param x The X coordinate.
     * @param y The Y coordinate.
     * @return True if valid destination, false if not.
     */
    private boolean canMoveToCell(GridWorldState state, int x, int y) {
        if (x < 0 || y < 0 || x >= state.getWidth() || y >= state.getHeight()) return false;

        return state.board[y][x] == CELL_REACHABLE;
    }

    @Override
    public boolean makeMove(GridWorldState fromState, Move move) {
        if (!validMove(fromState, move)) return false;

        if (move instanceof CompassDirectionAction) { // Initial move made by an agent.
            CompassDirectionAction action = (CompassDirectionAction) move;

            Coord playerCoords = fromState.findPlayerCoords(move.getPlayerID());
            Coord initialDestination = playerCoords.add(action.getDir(), MOVEMENT_UNIT);

            updateState(fromState, playerCoords, initialDestination);
            return true;
        } else if (move instanceof XYAction) { // The resulting position after stochastic mechanics come into play.
            XYAction action = (XYAction) move;

            Coord playerCoords = fromState.findPlayerCoords(move.getPlayerID());
            Coord finalDestination = new Coord(action.getX(), action.getY());

            updateState(fromState, playerCoords, finalDestination);
            return true;
        }

        return false;
    }

    /**
     * Moves a piece from coordinate `from` to coordinate `to` by replacing the destination cell with the piece and
     * setting the source cell empty. The assumption is therefore made that pieces can only come from cells that were
     * previously empty.
     *
     * @param from Coordinates of the source cell.
     * @param to Coordinates of the destination cell.
     */
    public void updateState(GridWorldState state, Coord from, Coord to) {
        if (to.equals(from)) return;

        if (state.board[from.getY()][from.getX()] == CELL_REACHABLE) {
            Log.warn("GridWorldState.update: Moving an empty cell from " + from + " to " + to + ".");
        }

        // Move the player's "piece".
        state.board[to.getY()][to.getX()] = state.board[from.getY()][from.getX()];
        state.board[from.getY()][from.getX()] = CELL_REACHABLE;
    }

    /**
     * Using the provided GridWorld state and piece position, determine the final coordinates of a piece after wind and
     * teleportation are taken into account. In practice, this should allow a referee to determine the result of
     * stochastic GridWorld mechanics, and distribute the result to all engines as an XYAction.
     * @param state GridWorld state after a piece was moved.
     * @param piecePosition The destination coordinates of a piece after it took a CompassDirectionAction.
     */
    public Coord getAfterStateDestination(GridWorldState state, Coord piecePosition) {
        int finalX = piecePosition.getX();
        int finalY = piecePosition.getY();

        // Calculate wind shift.
        finalY += getColumnWindShiftByIndex(columnWindIntensity, state, piecePosition);
        finalX += getRowWindShiftByIndex(rowWindIntensity, state, piecePosition);
        Coord finalDestination = new Coord(finalX, finalY);

        // Teleport the piece if it is on a portal.
        if (portalCoordinates.contains(finalDestination)) {
            finalDestination = getTeleportDestination(finalDestination);
        }

        return finalDestination;
    }

    private int getColumnWindShiftByIndex(Map<Integer, GridWorldLevelParser.WindDefinition> columnWind, GridWorldState state, Coord position) {
        // Check if columnWind is applicable for this column for the given x.
        if (columnWind.containsKey(position.getX())) {
            int windShift = 0;
            int signMultiplier = columnWind.get(position.getX()).getIntensity() >= 0 ? 1 : -1;

            // Increase columnWind shift step by step, checking for collision for each intensity level.
            for (int i = 0; i <= columnWind.get(position.getX()).getIntensity(); i++) {
                if (state.board[position.getY() + (i * signMultiplier)][position.getX()] == CELL_UNREACHABLE) {
                    break;
                }
                windShift = i;
            }

            if (columnWind.get(position.getX()).hasStochasticBehaviour()) {
                Map<Integer, Double> distribution = ImmutableMap.of(
                        windShift - STOCHASTIC_WIND_DELTA, STOCHASTIC_WIND_DECREMENT_PROBABILITY,
                        windShift + STOCHASTIC_WIND_DELTA, STOCHASTIC_WIND_INCREMENT_PROBABILITY,
                        windShift, 1 - STOCHASTIC_WIND_INCREMENT_PROBABILITY - STOCHASTIC_WIND_DECREMENT_PROBABILITY
                );

                int stochasticShift = 0;

                try {
                    stochasticShift = ClassUtils.chooseFromDistribution(distribution, rng);
                } catch (IncorrectlyNormalizedDistributionException e) {
                    e.printStackTrace();
                }

                // Wind is only adjusted stochastically if there is no collision.
                if (state.board[position.getY() + (stochasticShift * signMultiplier)][position.getX()] != CELL_UNREACHABLE) {
                    windShift = stochasticShift;
                }
            }

            return windShift;
        }

        return 0;
    }

    private int getRowWindShiftByIndex(Map<Integer, GridWorldLevelParser.WindDefinition> rowWind, GridWorldState state, Coord position) {
        // Check if rowWind is applicable for this column for the given x.
        if (rowWind.containsKey(position.getY())) {
            int windShift = 0;
            int signMultiplier = rowWind.get(position.getY()).getIntensity() >= 0 ? 1 : -1;

            // Increase rowWind shift step by step, checking for collision for each intensity level.
            for (int i = 0; i <= rowWind.get(position.getY()).getIntensity(); i++) {
                if (state.board[position.getY()][position.getX() + (i * signMultiplier)] == CELL_UNREACHABLE) {
                    break;
                }
                windShift = i;
            }

            if (rowWind.get(position.getY()).hasStochasticBehaviour()) {
                Map<Integer, Double> distribution = ImmutableMap.of(
                        windShift - STOCHASTIC_WIND_DELTA, STOCHASTIC_WIND_DECREMENT_PROBABILITY,
                        windShift + STOCHASTIC_WIND_DELTA, STOCHASTIC_WIND_INCREMENT_PROBABILITY,
                        windShift, 1 - STOCHASTIC_WIND_INCREMENT_PROBABILITY - STOCHASTIC_WIND_DECREMENT_PROBABILITY
                );

                int stochasticShift = 0;
                try {
                    stochasticShift = ClassUtils.chooseFromDistribution(distribution, rng);
                } catch (IncorrectlyNormalizedDistributionException e) {
                    e.printStackTrace();
                }

                // Wind is only adjusted stochastically if there is no collision.
                if (state.board[position.getY()][position.getX() + (stochasticShift * signMultiplier)] != CELL_UNREACHABLE) {
                    windShift = stochasticShift;
                }
            }

            return windShift;
        }

        return 0;
    }

    /**
     * Returns coordinates of a randomly chosen destination portal to teleport to.
     *
     * @param currentPosition Current position from which the piece is teleported.
     * @return A new Coord position as the destination.
     */
    private Coord getTeleportDestination(Coord currentPosition) {
        if (portalCoordinates.size() > 1) {
            Coord[] destinationOptions = portalCoordinates.stream()
                    .filter(x -> !x.equals(currentPosition))
                    .toArray(Coord[]::new);

            return destinationOptions[rng.nextInt(destinationOptions.length)];
        }

        return currentPosition;
    }

    @Override
    public void undoMove(GridWorldState fromState, Move move) {
        throw new UnsupportedOperationException();
    }

    @Override
    public List<Action> generateActions(GridWorldState fromState, int forPlayerID) {
        List<Action> actions = new ArrayList<>();

        Coord playerCoord = fromState.findPlayerCoords(forPlayerID);

        for (CompassDirection direction : CompassDirection.values()) {
            if ((navigationType == NavigationType.CARDINAL) && direction.isOrdinal())
                continue;

            if ((navigationType == NavigationType.ORDINAL) && direction.isCardinal())
                continue;

            if (!direction.isPrincipal())
                continue;

            Coord destination = playerCoord.add(direction, MOVEMENT_UNIT);
            if (destination == null) continue;

            if (canMoveToCell(fromState, destination.getX(), destination.getY())) {
                actions.add(new CompassDirectionAction(forPlayerID, direction));
            }
        }

        return actions;
    }

    @Override
    public boolean isTerminal(GridWorldState state) {
        for (Coord terminalPosition : terminalPositions) {
            /*
             * Check if cell is of type unreachable. If this is the case then the cell is a player piece type, implying
             * that a player piece is situated in one of the terminal positions.
             */
            if (state.board[terminalPosition.getY()][terminalPosition.getX()] != CELL_REACHABLE)
                return true;
        }

        return false;
    }

    @Override
    public Set<Integer> getCurrentPlayersToAct(GridWorldState fromState) {
        Set<Integer> playersToAct = new HashSet<>();
        playersToAct.add(GridWorldState.DEFAULT_FIRST_PLAYER);

        return playersToAct;
    }
}
