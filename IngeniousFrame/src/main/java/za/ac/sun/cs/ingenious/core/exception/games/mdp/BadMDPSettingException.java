package za.ac.sun.cs.ingenious.core.exception.games.mdp;

/**
 * Thrown if a validation error occurs during MDP JSON setting validation.
 *
 * @author Steffen Jacobs
 */
public class BadMDPSettingException extends Exception {
    public BadMDPSettingException(String message) {
        super(message);
    }
}
