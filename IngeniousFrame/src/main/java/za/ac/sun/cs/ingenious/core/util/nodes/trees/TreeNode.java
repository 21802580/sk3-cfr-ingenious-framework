package za.ac.sun.cs.ingenious.core.util.nodes.trees;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * A general purpose interface for self-typed tree-node objects within a tree-like structure. The intention is that objects implementing this interface
 * can be processed by various tree utilities. Note that this interface only provides methods for node linkage relations, 
 * it does not cater for the data stored within the node.
 * 
 * @author Nicholas Robinson
 */
public interface TreeNode<SELF extends TreeNode<SELF>> extends Cloneable {

    /**
     * Returns an Iterable element of this nodes associated child nodes, can be used in conjunction with a for-all-loop.
     * 
     * @return Iterable element of this nodes children
     */
    public default Iterable<SELF> children() {
        
        /**
         * A simple default iterator that depends on the getChild() and childCapacity() methods
         */
        class DefaultIterator implements Iterable<SELF>, Iterator<SELF> {
            int i = -1;
            {
                advance();
            }

            void advance() {
                i++;
                if (i < childCapacity() && getChild(i) == null) advance();
            }

			@Override
			public boolean hasNext() {
				return i < childCapacity();
			}

			@Override
			public SELF next() {
                if (i >= childCapacity()) throw new NoSuchElementException();
                SELF n = getChild(i);
                advance();
                return n;
			}

			@Override
			public Iterator<SELF> iterator() {
				return this;
			}
        }

        return new DefaultIterator();
    }

    /**
     * Returns the number of child nodes stored within this nodes internal list of child nodes.
     * 
     * @return the number of children nodes associated with this node
     */
    public int childCount();

    /**
     * Returns this nodes child capacity.
     * It is intended that this method be used when iterating by index, especially where index association matters and children are nullable.
     * 
     * <p> Note: by default, unless overriden, will return the same value as the {@link #childCount() childcount} method.
     * 
     * @return the max number of child nodes that this node may potentially associate with
     */
    public default int childCapacity() {
        return childCount();
    } 

    /**
     * Returns the child node at the specified position of this nodes internal list of child nodes.
     * 
     * @param index index of the child node to be returned
     * @return the child node at the specified position
     * @throws IndexOutOfBoundsException
     */
    public SELF getChild(int index);

    /**
     * Appends the specified child node to the end of this nodes internal list of child nodes.
     *
     * @param child child node to be appended
     * @return true if this nodes internal list of child nodes has changed as a result of this call
     */
    public boolean appendChild(SELF child);

    /**
     * Removes the child node at the specified position within this nodes internal list of child nodes.
     * May either shift subsequent elements to the left, or set the element at the specified position to null,
     * depending on implementation.
     * 
     * @param index index of child node to be removed
     * @return the child that was removed from the list
     * @throws IndexOutOfBoundsException
     */
    public SELF removeChild(int index);

    /**
     * Removes a single instance of the specified child node from this
     * nodes internal list of child nodes, if the child is present.
     * May either shift subsequent elements to the left, or set the element at the specified position to null,
     * depending on implementation.
     * 
     * @param child child node to be removed, if present
     * @return the child that was removed from the list, or null if no eligible child was found
     */
    public SELF removeChild(SELF child);

    /**
     * Replaces the child node at the specified position within this nodes internal list of child nodes with the specified node.
     * 
     * @param index index of the child node to be replaced
     * @param replacement node to be stored at the specified position
     * @return the element that was previously stored at the specified position
     * @throws IndexOutOfBoundsException
     */
    public SELF setChild(int index, SELF replacement);

    /**
     * Replaces the a single instance of the specified child node from this nodes internal list of child nodes, if the child is present.
     * 
     * @param child child node to be replaced, if present
     * @param replacement node to replace {@code child}
     * @return the child that was replaced, or null if no eligible child was found 
     */
    public SELF replaceChild(SELF child, SELF replacement);

    /**
     * Returns a shallow copy of this tree node instance
     * 
     * @return a clone of this
     */
    public SELF clone();

    /**
     * Interface to expand the functionality of TreeNodes, assumes that they possess references to their parent nodes.
     * Provides the {@link #getParent() getParent} and {@link #setParent() setParent} methods.
     */
    public interface Bidirectionality<SELF extends TreeNode<SELF>> {

        /**
         * @return the parent of this node
         */
        public SELF getParent();

        /**
         * @param parent the parent node to associate this node with
         */
        public void setParent(SELF parent);
    }
    
    /**
     * Interface to expand the functionality of TreeNodes, assumes that the node belongs to a binary tree,
     * i.e. the node has at most 2 children. This interface distinguishes the nodes first child as it left child,
     * and its second child as its right child.
     */
    public interface Binary<SELF extends TreeNode<SELF>> {

        /**
         * @return the first child of this node
         */
        public SELF getLeft();

        /**
         * @return the second child of this node
         */
        public SELF getRight();

        /**
         * @param left node to be associated with as this nodes first child
         */
        public void setLeft(SELF left);

        /**
         * @param right node to be associated with as this nodes second child
         */
        public void setRight(SELF right);
    }
}