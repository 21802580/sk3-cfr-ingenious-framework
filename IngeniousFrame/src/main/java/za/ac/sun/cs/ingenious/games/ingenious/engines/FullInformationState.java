package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;
import za.ac.sun.cs.ingenious.games.ingenious.Tile;
import za.ac.sun.cs.ingenious.games.ingenious.search.minimax.SearchState;

public class FullInformationState extends SearchState {

	private ArrayList<Tile> searchBag;
	private int bagIndex =0;
	/*
	 * Alpha and beta values can be ignored for normal logginsearch.minimax
	 */
	double alpha = Double.MIN_VALUE +1;
	double beta = Double.MAX_VALUE;
	public FullInformationState(IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board, IngeniousScoreKeeper scores,
			ArrayList<IngeniousRack> racks,ArrayList<Tile> bag,int depth) {
		super(board, scores, racks,depth);
		setSearchBag(new ArrayList<Tile>());
		for(IngeniousRack r : racks){
			setBagIndex(getBagIndex() + r.size());
		}
		for(Tile t : bag){
			getSearchBag().add(t.copy());
		}
		
	}
	public ArrayList<Tile> getSearchBag() {
		return searchBag;
	}
	public void setSearchBag(ArrayList<Tile> searchBag) {
		this.searchBag = searchBag;
	}
	public int getBagIndex() {
		return bagIndex;
	}
	public void setBagIndex(int bagIndex) {
		this.bagIndex = bagIndex;
	}

}
