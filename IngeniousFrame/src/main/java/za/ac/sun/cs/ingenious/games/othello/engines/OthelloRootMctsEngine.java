package za.ac.sun.cs.ingenious.games.othello.engines;


// TODO: check which is needed in the end/clean up!!

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsNodeSimple;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloBoard;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsRoot;
import za.ac.sun.cs.ingenious.search.mcts.selection.SelectionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

import java.util.ArrayList;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.SelectionUct.newSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

// specific othello

/**
 * An engine for the Othello (Reversi) board game which plays random moves. 
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public class OthelloRootMctsEngine extends OthelloEngine {

	MctsRoot<OthelloBoard, MctsNodeSimple<OthelloBoard>> mcts;
	int threadCount;

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection	the connection the engine uses to receive and send
	 *							information from and to the server
	 */
	public OthelloRootMctsEngine(EngineToServerConnection serverConnection)
	{
		super(serverConnection);
		
		threadCount = 4;
		
		SelectionThreadSafe<MctsNodeSimple<OthelloBoard>, MctsNodeSimple<OthelloBoard>> selection = newSelectionUct(logic);
			
		ExpansionThreadSafe<MctsNodeSimple<OthelloBoard>, MctsNodeSimple<OthelloBoard>> expansion = newExpansionSingle(logic);

		boolean recordMoves = false;
		if (selection.equals("Rave")) {
			recordMoves = true;
		}
		SimulationThreadSafe<OthelloBoard> simulation =
			newSimulationRandom(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), recordMoves);
		
		BackpropagationThreadSafe<MctsNodeSimple<OthelloBoard>> backprop = newBackpropagationAverage();

		this.mcts = new MctsRoot<>(selection, expansion, simulation, backprop, logic, threadCount, null);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	/**
	 * Returns the Engine's name
	 *
	 * @return	the engine's name
	 */
	public String engineName()
	{
		return "OthelloRootMctsEngine";
	}

	/**
	 * Requests a move from the player and determines if the move is valid.
	 * If the move is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out or
	 * the maximum wrong moves is reached.
	 *
	 * @param message	the generate move request sent from the Referee
	 *
	 * @return			a valid move, or null if the clock ran out, the
	 *					player's tries ran out, or the player has forfeit
	 */
	public PlayActionMessage receiveGenActionMessage(GenActionMessage message) {
		if (output) {
			board.printPretty();
		}

		MctsNodeSimple<OthelloBoard> root = new MctsNodeSimple<OthelloBoard>(board, null, null, new ArrayList<MctsNodeSimple<OthelloBoard>>(), logic, playerID);
		
		Action action = mcts.doSearch(root, Constants.TURN_LENGTH); // turnlength? Shouldn't this be from the clock

		if (action == null) {
			return new PlayActionMessage(new ForfeitAction((byte) playerID));
		}

		return new PlayActionMessage(action);
	}
}

// TODO: comments!! CORRECT!!! and add!!! Current comments are INCORRECT!!!
// TODO: play with sending classes and having factory methods perhaps? make it more user friendly (new policies made things complicated)
