package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.sarsa.TabularSarsa;

import java.util.List;
import java.util.Map;

/**
 * Tabular SARSA engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldTabularSarsaEngine extends GridWorldEngine {
    private final double ALPHA = 0.1;
    private final double EPSILON = 0.2;
    private final double GAMMA = 1.0;

    private GridWorldState previousState = null;
    private Action previousAction = null;
    private TabularSarsa alg;

    public GridWorldTabularSarsaEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new TabularSarsa("GridWorldTabularSarsaEngine_" + playerID, ALPHA, EPSILON, GAMMA);
        alg.displayChart();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldTabularSarsaEngine";
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        Move move = a.getMove();
        int player = move.getPlayerID();
        if (player == this.playerID) {
            previousState = state.deepCopy();
        }

        logic.makeMove(state, a.getMove());
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = null;
        List<Action> availableActions = logic.generateActions(state, playerID);

        choice = alg.chooseAction(state, availableActions);

        previousAction = choice;
        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward reward = (ScalarReward) a.getReward();

        List<Action> stateActions = logic.generateActions(state, playerID);

        alg.update(previousState, previousAction, reward, state, stateActions);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.endEpisode();
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTable(state, alg.getQTable()));
        alg.printMetrics();

        Map<GameState, Map<Action, Double>> qTable = alg.getQTable();
        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicyTable(state, qTable));
        alg.close();
    }

    public TabularSarsa getAlg() {
        return alg;
    }
}
