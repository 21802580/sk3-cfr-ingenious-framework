package za.ac.sun.cs.ingenious.games.loa.util;


import java.io.Serializable;

public class LOACoord implements Serializable {
    // Static co-ordinate array
    public static LOACoord[][] coords = {
            {
                    new LOACoord(0, 0), new LOACoord(0, 1), new LOACoord(0, 2), new LOACoord(0, 3),
                    new LOACoord(0, 4), new LOACoord(0, 5), new LOACoord(0, 6), new LOACoord(0, 7)
            },
            {
                    new LOACoord(1, 0), new LOACoord(1, 1), new LOACoord(1, 2), new LOACoord(1, 3),
                    new LOACoord(1, 4), new LOACoord(1, 5), new LOACoord(1, 6), new LOACoord(1, 7)
            },
            {
                    new LOACoord(2, 0), new LOACoord(2, 1), new LOACoord(2, 2), new LOACoord(2, 3),
                    new LOACoord(2, 4), new LOACoord(2, 5), new LOACoord(2, 6), new LOACoord(2, 7)
            },
            {
                    new LOACoord(3, 0), new LOACoord(3, 1), new LOACoord(3, 2), new LOACoord(3, 3),
                    new LOACoord(3, 4), new LOACoord(3, 5), new LOACoord(3, 6), new LOACoord(3, 7)
            },
            {
                    new LOACoord(4, 0), new LOACoord(4, 1), new LOACoord(4, 2), new LOACoord(4, 3),
                    new LOACoord(4, 4), new LOACoord(4, 5), new LOACoord(4, 6), new LOACoord(4, 7)
            },
            {
                    new LOACoord(5, 0), new LOACoord(5, 1), new LOACoord(5, 2), new LOACoord(5, 3),
                    new LOACoord(5, 4), new LOACoord(5, 5), new LOACoord(5, 6), new LOACoord(5, 7)
            },
            {
                    new LOACoord(6, 0), new LOACoord(6, 1), new LOACoord(6, 2), new LOACoord(6, 3),
                    new LOACoord(6, 4), new LOACoord(6, 5), new LOACoord(6, 6), new LOACoord(6, 7)
            },
            {
                    new LOACoord(7, 0), new LOACoord(7, 1), new LOACoord(7, 2), new LOACoord(7, 3),
                    new LOACoord(7, 4), new LOACoord(7, 5), new LOACoord(7, 6), new LOACoord(7, 7)
            }
    };
    private final int row;
    private final int col;

    private LOACoord(int row, int col) {
        this.row = row;
        this.col = col;
    }
	
	public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + col;
        result = prime * result + row;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LOACoord other = (LOACoord) obj;
		return col == other.col && row == other.row;
	}

    public String toString() {
        return "(" + row + ", " + col + ")";
    }

}
