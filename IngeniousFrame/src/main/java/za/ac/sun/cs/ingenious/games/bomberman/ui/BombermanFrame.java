package za.ac.sun.cs.ingenious.games.bomberman.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.TitledBorder;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.games.bomberman.BMReferee;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.Drawable;
import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.DrawableCanvas;
import za.ac.sun.cs.ingenious.games.bomberman.ui.drawableengine.DrawableStore;

public class BombermanFrame extends JFrame implements Observer {

	private static final long serialVersionUID = 1L;
	private LinkedList<Drawable> drawables;
	private DrawableStore drawableStore;
	private DrawableCanvas canvas;
	private JLabel roundLabel;
	private JPanel scorePanel;
	private JLabel[] scoreLabels;
	private JPanel logPanel;
	private String log = "";
	
	public BombermanFrame(BMReferee controller) {
		this.setPreferredSize(new Dimension(800, 600));
		this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		controller.getBoard().addObserver(this);
		drawables = new LinkedList<>();
		drawableStore = new DrawableStore();
		canvas = new DrawableCanvas(drawableStore, false);
		canvas.setBorder(BorderFactory.createEtchedBorder());
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(canvas, BorderLayout.CENTER);
		
		JPanel sidePanel = new JPanel();
		sidePanel.setPreferredSize(new Dimension(400, 100));
		sidePanel.setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		//c.ipadx = 10;
		//c.ipady = 10;
		c.insets = new Insets(10, 10, 10, 10);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.anchor = GridBagConstraints.NORTH;
		c.weighty = 0.1;
		c.weightx = 1;
		
		roundLabel = new JLabel("Game starts ...");
		roundLabel.setFont(new Font("ARIAL", Font.PLAIN, 20));
		roundLabel.setBorder(BorderFactory.createTitledBorder(null, "Round Counter", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("ARIAL", Font.PLAIN, 20)));
		//this.getContentPane().add(roundLabel, BorderLayout.NORTH);
		sidePanel.add(roundLabel,c);
		c.gridy = 1;
		c.weighty = 0.1;
		sidePanel.add(createScorePanel(controller),c);
		c.gridy = 2;
		c.weighty = 0.8;
		JScrollPane logScrollPanel = createLogPanel();
		sidePanel.add(logScrollPanel,c);
		this.getContentPane().add(sidePanel, BorderLayout.EAST);
		this.pack();
		this.setVisible(true);
		controller.addObserver(this);
	}
	
	private JPanel createScorePanel(BMReferee bm){
		scorePanel = new JPanel(new GridBagLayout());
		scorePanel.setBorder(BorderFactory.createTitledBorder(null, "Score Board", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("ARIAL", Font.PLAIN, 20)));
		PlayerRepresentation[] engineConns = bm.getEngineConns();
		scoreLabels = new JLabel[engineConns.length];
		GridBagConstraints c = new GridBagConstraints();
		c.ipadx = 10;
		c.ipadx = 5;
		c.weightx = 1;
		c.weighty = 0.1;
		c.fill = GridBagConstraints.HORIZONTAL;
		for (int i = 0; i < engineConns.length; i++){
				c.gridy=i;
				c.gridx = 0;
				JLabel nameLabel = new JLabel(""+(char)('A'+i));
				nameLabel.setFont(new Font("Arial", Font.PLAIN, 30));
				scorePanel.add(nameLabel,c);
				c.gridx = 1;
				JLabel typeLabel = new JLabel(engineConns[i]!=null?engineConns[i].toString():"not connected");
				typeLabel.setFont(new Font("Arial", Font.PLAIN, 20));
				scorePanel.add(typeLabel,c);
				c.gridx = 2;
				scoreLabels[i] = new JLabel("0 Points");
				scoreLabels[i].setFont(new Font("Arial", Font.PLAIN, 20));
				scorePanel.add(scoreLabels[i],c);
			
		}
		return scorePanel;
	}
	
	private JScrollPane createLogPanel(){
		logPanel = new JPanel();
		logPanel.setLayout(new BoxLayout(logPanel,BoxLayout.Y_AXIS));
		logPanel.setBorder(BorderFactory.createTitledBorder(null, "Log", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION, new Font("ARIAL", Font.PLAIN, 20)));
		JScrollPane scrollPane = new JScrollPane(logPanel);
		scrollPane.setMinimumSize(new Dimension(100, 400));
		scrollPane.setPreferredSize(new Dimension(100, 400));
		return scrollPane;
	}
	
	private void addToLogPanel(String str){
		JLabel logArea = new JLabel(str);
		logArea.setFont(new Font("ARIAL", Font.PLAIN, 20));
		logPanel.add(logArea);
	}
	
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof BMReferee && arg instanceof char[][]) {
			BMReferee bm = (BMReferee) o;
			roundLabel.setText("Round: "+bm.getBoard().getRound());
			for(int i = 0; i < scoreLabels.length; i++){
				scoreLabels[i].setText(bm.getBoard().getScores()[i] + " Points");
			}
			for(Drawable d : drawables){
				drawableStore.removeDrawable(d, 0);
			}
			char[][] map = (char[][])arg;
			for(int y = 0; y < map.length; y++){
				for(int x = 0; x < map[0].length; x++){
					TileDrawable d = new TileDrawable(map[y][x], x, y);
					drawables.add(d);
					drawableStore.addDrawable(d, 0);
				}
			}
			canvas.repaint();
			scorePanel.updateUI();
			scorePanel.repaint();
		} else if (o instanceof BMBoard && arg instanceof String) {
			addToLogPanel((String)arg);
			logPanel.repaint();
		}
	}
}
