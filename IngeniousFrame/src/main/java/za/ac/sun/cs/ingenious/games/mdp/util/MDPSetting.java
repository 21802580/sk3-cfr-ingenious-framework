package za.ac.sun.cs.ingenious.games.mdp.util;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import za.ac.sun.cs.ingenious.core.exception.games.mdp.BadMDPSettingException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collection;

/**
 * Class used to represent an MDP configuration loaded from a JSON file.
 *
 * @author Steffen Jacobs
 */
public class MDPSetting {
    private static final int MINIMUM_STATE_COUNT = 1;
    private static final int MINIMUM_ACTION_COUNT = 0;

    private long initialState;
    private long states;
    private long actions;
    private Collection<Integer> terminalStates;
    private Collection<Collection<Double>> reward;
    private Collection<Collection<Integer>> validActions;
    private Collection<Collection<Collection<Double>>> dynamics;

    /**
     * Comprehensive constructor used for unit testing MDP logic.
     *
     * @param initialState Initial state of the MDP.
     * @param states Number of states.
     * @param actions Number of actions.
     * @param terminalStates List indicating which states are terminal.
     * @param reward Reward corresponding to (S, S').
     * @param validActions Valid actions given (S).
     * @param dynamics Distribution of resulting states for (S, A).
     */
    public MDPSetting(
            int initialState,
            int states,
            int actions,
            Collection<Integer> terminalStates,
            Collection<Collection<Double>> reward,
            Collection<Collection<Integer>> validActions,
            Collection<Collection<Collection<Double>>> dynamics
    ) {
        this.initialState = initialState;
        this.states = states;
        this.actions = actions;
        this.terminalStates = terminalStates;
        this.reward = reward;
        this.validActions = validActions;
        this.dynamics = dynamics;
    }

    public static MDPSetting fromFile(String configFile) {
        MDPSetting mdpSetting = null;

        try {
            byte[] encoded = Files.readAllBytes(Paths.get(configFile));
            String jsonObject = new String(encoded, StandardCharsets.UTF_8);

            mdpSetting = new Gson().fromJson(jsonObject, MDPSetting.class);
            mdpSetting.ensureValid();
        } catch (IOException e) {
            Log.error("MDP File IOException: " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        } catch (BadMDPSettingException e) {
            Log.error("MDP File BadMDPSettingException: " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }

        return mdpSetting;
    }

    public long getStates() {
        return states;
    }

    public long getActions() {
        return actions;
    }

    public long getInitialState() {
        return initialState;
    }

    public Collection<Integer> getTerminalStates() {
        return terminalStates;
    }

    public Collection<Collection<Integer>> getValidActions() {
        return validActions;
    }

    public Collection<Collection<Double>> getReward() {
        return reward;
    }

    public Collection<Collection<Collection<Double>>> getDynamics() {
        return dynamics;
    }

    /**
     * Ensures that this model of MDP settings is valid, throwing an exception if this is not the case.
     *
     * @throws BadMDPSettingException If an invalid MDP setting is present.
     */
    public void ensureValid() throws BadMDPSettingException {
        if (states < MINIMUM_STATE_COUNT)
            throw new BadMDPSettingException("Invalid number of states.");

        if (actions < MINIMUM_ACTION_COUNT)
            throw new BadMDPSettingException("Invalid number of actions.");

        if (reward.size() != states)
            throw new BadMDPSettingException("Reward not specified for each `from` state.");

        if (reward.stream().anyMatch(a -> a.size() != states))
            throw new BadMDPSettingException("Reward not specified for each `to` state");

        if (dynamics.size() != states)
            throw new BadMDPSettingException("Dynamics not specified for each state.");

        if (dynamics.stream().anyMatch(a -> a.size() != actions))
            throw new BadMDPSettingException("Dynamics not specified for each action.");

        if (dynamics.stream().anyMatch(a -> a.stream().anyMatch(s -> s.size() != states)))
            throw new BadMDPSettingException("Dynamics not specified for each new state.");

        if (dynamics.stream().anyMatch(a -> a.stream().anyMatch(s -> s.stream().anyMatch(p -> p < 0.0 || p > 1.0))))
            throw new BadMDPSettingException("Dynamics contains an invalid probability.");
    }
}
