package za.ac.sun.cs.ingenious.games.puzzles.slidingpuzzle;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;

/**
 * Sliding puzzle state. Represents a position in the search space. Contains the location of the empty space 
 * and a PVector to represent the board. Also Contains methods for comparing states 
 * in order to determine if they are equivalent. Makes use of zobrist hashes for quick comparisons between states.
 */
public class SlidingPuzzleState implements Comparable<SlidingPuzzleState> {

    public PVector<Integer> board;
    public int emptyTileLocation;
    public long zobristHash1;
    public long zobristHash2;
    public double heuristicCost;

    public SlidingPuzzleState() {}

    public SlidingPuzzleState(SlidingPuzzleState s) {
        board = s.board;
        emptyTileLocation = s.emptyTileLocation;
        zobristHash1 = s.zobristHash1;
        zobristHash2 = s.zobristHash2;
        heuristicCost = s.heuristicCost;
    }

    // for use in HashMap
    @Override
    public int hashCode() {
        return (int)zobristHash1;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SlidingPuzzleState == false) return false;
        SlidingPuzzleState other = (SlidingPuzzleState)obj;
        
        if (other.zobristHash1 != zobristHash1) return false;
        if (other.zobristHash2 != zobristHash2) return false;
        if (other.emptyTileLocation != emptyTileLocation) return false;
        if (other.heuristicCost != heuristicCost) return false;

        return true;
    }

    @Override
    public String toString() {
        return "empty tile location: " + emptyTileLocation + " zobrist hash1: " + zobristHash1 + " hash2: " + zobristHash2 + " heuristic cost: " + heuristicCost;
    }

    public SlidingPuzzleState clone() {
        return new SlidingPuzzleState(this);
    }

    // for use in TreeMap
	@Override
	public int compareTo(SlidingPuzzleState o) {
		if (zobristHash1 == o.zobristHash1){
            if (zobristHash2 == o.zobristHash2) return 0;
            return zobristHash2 > o.zobristHash2 ? 1 : -1;
        }
        return zobristHash1 > o.zobristHash1 ? 1 : -1;
	}
}
