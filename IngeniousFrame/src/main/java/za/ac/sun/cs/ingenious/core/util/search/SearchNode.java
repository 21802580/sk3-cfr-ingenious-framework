package za.ac.sun.cs.ingenious.core.util.search;

import za.ac.sun.cs.ingenious.core.GameState;

import java.util.List;

public interface SearchNode<S extends GameState> {

	public S getState();
	
	public double getValue();
	
	public void addValue(double add);
	
	public String toString();

//	public int getDepth();
	
}

// TODO: comments!!
