package za.ac.sun.cs.ingenious.search.rl.util;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import za.ac.sun.cs.ingenious.core.network.external.ExternalComms;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExitMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation.LinearFeatureValuesMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation.InitLinearFeatureApproximationMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation.ReqLinearFeatureValuesMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation.TrainLinearFeaturesMessage;

import java.io.IOException;

/**
 * Class that abstracts communication detail with the Python application away from the linear function approximation
 * Q-learning algorithm.
 *
 * @author Steffen Jacobs
 */
public class ExternalLinearFunctionApproximation implements LinearFunctionApproximation {
    private boolean initialized;
    private final ExternalComms externalComms;
//    private final Process pythonProcess;

    public ExternalLinearFunctionApproximation() throws IOException {
        // TODO (#289): Would work nicely if the external Python app could somehow be launched from Java.

        externalComms = new ExternalComms();
        initialized = false;
    }

    public void init(int size) {
        Gson gson = new Gson();
        String message = gson.toJson(new InitLinearFeatureApproximationMessage(size));

        externalComms.send(message);
        externalComms.receive();
        initialized = true;
    }

    public void ensureInitialized(double[] featureSet) {
        if (initialized) return;

        init(featureSet.length);
    }

    public double[] get(double[][] featureSets) {
        if (featureSets.length < 1) return new double[0];

        ensureInitialized(featureSets[0]);

        Gson gson = new Gson();
        String message = gson.toJson(new ReqLinearFeatureValuesMessage(featureSets));
        externalComms.send(message);

        String map = externalComms.receive();
        LinearFeatureValuesMessage response = gson.fromJson(map, LinearFeatureValuesMessage.class);

        return response.getPayload();
    }

    public void put(double[] featureSet, double estimatedValue) {
        ensureInitialized(featureSet);

        Gson gson = new Gson();
        String message = gson.toJson(new TrainLinearFeaturesMessage(featureSet, estimatedValue));

        externalComms.send(message);
        externalComms.receive();
    }

    public void close() {
        Gson gson = new Gson();
        String message = gson.toJson(new ExitMessage());
        Log.info("Exit message: " + message);
        externalComms.send(message);

        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        externalComms.close();
    }
}
