package za.ac.sun.cs.ingenious.core.util.hashing;

import java.util.Random;

/**
 * Utility for generating a set of binary hash strings with a minimum hamming distance between the hashes.
 * 
 * @author Nicholas Robinson
 */
public class BitStringGenerator {

    /**
     * The hamming distance between two ints.
     * @param a value to be compared
     * @param b value to be compared
     * @return hamming distance between {@code a} and {@code b}
     */
    public static int hammingDistance(int a, int b) {
        int distance = 0;
        for (int i = 0, bit = 1; i < 32; i++, bit <<= 1) {
            if ((a & bit) != (b & bit)) distance++;
        }
        return distance;
    }

    /**
     * The hamming distance between two longs.
     * @param a value to be compared
     * @param b value to be compared
     * @return hamming distance between {@code a} and {@code b}
     */
    public static int hammingDistance(long a, long b) {
        int distance = 0;
        for (long i = 0, bit = 1; i < 64; i++, bit <<= 1) {
            if ((a & bit) != (b & bit)) distance++;
        }
        return distance;
    }

    /**
     * Generate a set of hashes, as ints.
     * @param count the number of hashes to generate
     * @param minHammingDistance the minimum hamming distance between the hashes
     * @return an array of hashes
     */
    public static int[] generateI(int count, int minHammingDistance) {
        return generateIntegers(count, minHammingDistance, new Random());
    }

    /**
     * Generate a set of hashes, as ints.
     * @param count the number of hashes to generate
     * @param minHammingDistance the minimum hamming distance between the hashes
     * @param seed seed used to generate random numbers
     * @return an array of hashes
     */
    public static int[] generateI(int count, int minHammingDistance, long seed) {
        return generateIntegers(count, minHammingDistance, new Random(seed));
    }

    /**
     * Generate a set of hashes, as longs.
     * @param count the number of hashes to generate
     * @param minHammingDistance the minimum hamming distance between the hashes
     * @return an array of hashes
     */
    public static long[] generateL(int count, int minHammingDistance) {
        return generateLongs(count, minHammingDistance, new Random());
    }

    /**
     * Generate a set of hashes, as longs.
     * @param count the number of hashes to generate
     * @param minHammingDistance the minimum hamming distance between the hashes
     * @param seed seed used to generate random numbers
     * @return an array of hashes
     */
    public static long[] generateL(int count, int minHammingDistance, long seed) {
        return generateLongs(count, minHammingDistance, new Random(seed));
    }

    /**
     * Do any of the hamming distance between the specified value and the given values of an array violate the
     * minimum hamming distance?
     */
    private static boolean hasHammingClash(int value, int[] strings, int startIndex, int endIndex, int minHammingDistance) {
        for (int i = startIndex; i < endIndex; i++) {
            if (hammingDistance(value, strings[i]) < minHammingDistance) {
                return true;
            }
        }
        return false;
    }

    /**
     * Do any of the hamming distance between the specified value and the given values of an array violate the
     * minimum hamming distance?
     */
    private static boolean hasHammingClash(long value, long[] strings, int startIndex, int endIndex, int minHammingDistance) {
        for (int i = startIndex; i < endIndex; i++) {
            if (hammingDistance(value, strings[i]) < minHammingDistance) {
                return true;
            }
        }
        return false;
    }

    private static int[] generateIntegers(int count, int minHammingDistance, Random rng) {
        int[] strings = new int[count];

        for (int i = 0; i < strings.length; i++) {
            do {
                strings[i] = rng.nextInt();
            } while (hasHammingClash(strings[i], strings, 0, i, minHammingDistance));
        }
        return strings;
    }

    private static long[] generateLongs(int count, int minHammingDistance, Random rng) {
        long[] strings = new long[count];

        for (int i = 0; i < strings.length; i++) {
            do {
                strings[i] = rng.nextInt();
            } while (hasHammingClash(strings[i], strings, 0, i, minHammingDistance));
        }
        return strings;
    }
}
