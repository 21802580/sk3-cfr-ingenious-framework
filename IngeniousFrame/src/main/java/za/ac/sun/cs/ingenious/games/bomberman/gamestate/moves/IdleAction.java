package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.Action;

public class IdleAction implements Action {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString(){
		return "idle";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass())) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public int hashCode() {
		return Integer.MAX_VALUE;
	}

	@Override
	public int getPlayerID() {
		return -1;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
