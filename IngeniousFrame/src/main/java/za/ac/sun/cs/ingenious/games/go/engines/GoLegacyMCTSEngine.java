package za.ac.sun.cs.ingenious.games.go.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.*;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.go.GoEngine;
import za.ac.sun.cs.ingenious.games.go.GoFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.*;
import za.ac.sun.cs.ingenious.search.mcts.legacy.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GoLegacyMCTSEngine extends GoEngine {

    private static final int TURN_TIME = 500;

    /**
     * @param toServer An established connection to the GameServer
     */
    public GoLegacyMCTSEngine(EngineToServerConnection toServer) throws MissingSettingException, IncorrectSettingTypeException, IOException {
        super(toServer);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        MCTSNode<TurnBasedSquareBoard> root = new MCTSNode<TurnBasedSquareBoard>(currentState, logic, null, null);
        return new PlayActionMessage(MCTS.generateAction(root, new RandomPolicy<TurnBasedSquareBoard>(
                        logic, new GoFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), false),
                new MCTSDescender<TurnBasedSquareBoard>(logic, new UCT<>(), new PerfectInformationActionSensor<>()),
                new SimpleUpdater<>(), TURN_TIME));
    }
}
