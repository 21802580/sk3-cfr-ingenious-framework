package za.ac.sun.cs.ingenious.games.loa.movegeneration;

import za.ac.sun.cs.ingenious.games.loa.LOABoard;
import za.ac.sun.cs.ingenious.games.loa.knowledge.Heuristic;
import za.ac.sun.cs.ingenious.games.loa.knowledge.HeuristicType;
import za.ac.sun.cs.ingenious.games.loa.knowledge.MoveCategory;
import za.ac.sun.cs.ingenious.games.loa.util.*;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

import static za.ac.sun.cs.ingenious.games.loa.movegeneration.LineUtils.getLinePositionFromCoord;

public class MoveGenerator {

	// Gross but it works
	public static List<MoveValuePair> generateMovesForPlayer(LOABoard board, HeuristicType heuristicType, boolean decisive, Colour player) {
		if (board.getWinner() != null){
			// Board is terminal -> return empty list
			return new ArrayList<MoveValuePair>();
		}
		PieceSet pieces = board.getPiecesFor(player);
		LineConfiguration lines = board.getLineConfiguration();
		List<MoveValuePair> allMoves = new ArrayList<MoveValuePair>();
		HashMap<LineOrientation, HashSet<Integer>> checkedInices = new HashMap<LineOrientation, HashSet<Integer>>();
		for (LineOrientation orientation : LineOrientation.getArray()) {
			checkedInices.put(orientation, new HashSet<Integer>());
		}
		for (Piece p : pieces) {
			int row = p.getRow();
			int col = p.getCol();
			for (LineOrientation orientation : LineOrientation.getArray()) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				int hash = lines.getLineHash(orientation, hashIndex);
				HashSet<Integer> checkedIndicesForOrientation = checkedInices.get(orientation);
				if (!checkedIndicesForOrientation.contains(hashIndex)) {
					checkedIndicesForOrientation.add(hashIndex);
					List<LOAMove> moves = MoveMap.getMovesForLine(player, orientation, hashIndex, hash, board);
					if (moves == null) continue;
					for (LOAMove move : moves){
						double value = 0.0;
						if (heuristicType == HeuristicType.FUNCTION) {
							value = Heuristic.getHeuristicValue(board, move);
						} else if (heuristicType == HeuristicType.MOVE_CATEGORY) {
							value = MoveCategory.getTransitionProbability(move);
						}
						if (decisive && Heuristic.moveIsDecisive(board, move)) {
							allMoves = new ArrayList<MoveValuePair>();
							allMoves.add(new MoveValuePair(move, Integer.MAX_VALUE));
							return allMoves;
						}
						allMoves.add(new MoveValuePair(move, value));
					}
				}
			}
		}
		return allMoves;
	}

    public static List<MoveValuePair> generateMovesWithValues(LOABoard board, HeuristicType heuristicType, boolean decisive) {
    	Colour player = board.getCurrentPlayer();
    	return generateMovesForPlayer(board, heuristicType, decisive, player);
	}
	
	public static LOAMove getRandomMove(LOABoard board){
		Colour player = board.getCurrentPlayer();
		PieceSet pieces = board.getPiecesFor(player);
		Random random = ThreadLocalRandom.current();
		LineOrientation[] orientationOrder = LineOrientation.getRandomOrder(random);
		LOAMove move = null;
		for (int i = 0; i < 5; i++){
			Piece piece = pieces.getRandom(random);
			int row = piece.getRow();
			int col = piece.getCol();
			for (LineOrientation orientation : orientationOrder) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				move = getRandomMoveForLine(board, orientation, hashIndex, random);
				if (move != null) break;
			}
		}
		return move == null ? getRandomMoveExhaustive(board, pieces, random, orientationOrder) : move;
	}
	
	private static LOAMove getRandomMoveExhaustive(LOABoard board, PieceSet pieces, Random random, LineOrientation[] orientationOrder){
		HashSet<Piece> checkedPieces = new HashSet<Piece>();
		HashMap<LineOrientation, HashSet<Integer>> checkedInices = new HashMap<LineOrientation, HashSet<Integer>>();
		for (LineOrientation orientation : LineOrientation.getArray()){
			checkedInices.put(orientation, new HashSet<Integer>());
		}
		LOAMove move = null;
		while((!pieces.isEmpty()) && (move == null)){
			Piece piece = pieces.removeRandom(random);
			checkedPieces.add(piece);
			int row = piece.getRow();
			int col = piece.getCol();
			for (LineOrientation orientation : orientationOrder) {
				int hashIndex = getLinePositionFromCoord(row, col, orientation);
				if (!checkedInices.get(orientation).contains(hashIndex)) {
					checkedInices.get(orientation).add(hashIndex);
					move = getRandomMoveForLine(board, orientation, hashIndex, random);
					if (move != null) break;
				}
			}
		}
		pieces.addAll(checkedPieces);
		return move;
	}
	
	public static boolean hasMove(LOABoard board, LOAMove m){
    	Colour colour = board.getCurrentPlayer();
    	LineOrientation orientation = m.getOrientation();
    	LineConfiguration lines = board.getLineConfiguration();
		int linepos = getLinePositionFromCoord(m.getFrom().getRow(), m.getFrom().getCol(), orientation);
		int hash = lines.getLineHash(orientation, linepos);
 		List<LOAMove> legalLineMoves = MoveMap.getMovesForLine(colour, orientation, linepos, hash, board);
 		if (legalLineMoves == null) return false;
 		return legalLineMoves.contains(m);
	}
	
	private static LOAMove getRandomMoveForLine(LOABoard board, LineOrientation orientation, int hashIndex, Random random){
		LineConfiguration lines = board.getLineConfiguration();
		Colour player = board.getCurrentPlayer();
		int hash = lines.getLineHash(orientation, hashIndex);
		List<LOAMove> moves = MoveMap.getMovesForLine(player, orientation, hashIndex, hash, board);
		if (moves == null) return null;
		return moves.get(random.nextInt(moves.size()));
	}
	
}
