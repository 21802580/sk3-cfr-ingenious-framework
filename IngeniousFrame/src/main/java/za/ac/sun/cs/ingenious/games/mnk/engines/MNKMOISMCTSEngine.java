package za.ac.sun.cs.ingenious.games.mnk.engines;

import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.ismcts.MOISMCTSDescender;
import za.ac.sun.cs.ingenious.search.ismcts.MOISMCTSNode;
import za.ac.sun.cs.ingenious.search.ismcts.MOISMCTSUpdater;
import za.ac.sun.cs.ingenious.search.ismcts.SubsetUCT;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;

public class MNKMOISMCTSEngine extends MNKEngine {

	public MNKMOISMCTSEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		return "MNKMOISMCTSEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		MOISMCTSNode<MNKState> root = MOISMCTSNode.createRootNode(playerID, board, logic, logic, logic);
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<MNKState>(logic, eval, logic, false),
				new MOISMCTSDescender<MNKState>(logic, new SubsetUCT<MNKState>(), logic, logic),
				new MOISMCTSUpdater<MNKState>(), Constants.TURN_LENGTH));
	}

}
