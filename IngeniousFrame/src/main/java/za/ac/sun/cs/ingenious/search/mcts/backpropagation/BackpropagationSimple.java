package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

public class BackpropagationSimple<N extends MctsNodeTreeParallelInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {

	public int player;

	public BackpropagationSimple(int player) {
		this.player = player;
	}

	/**
	 * Update the fields relating to this backpropagation strategy for the current node.
	 * @param node
	 * @param results the win/loss or draw outcome for the simulation relating to the current playout.
	 */
	public void propagate(N node, double[] results) {

		if (node.getVisitCount() != 0) {
            node.restoreVirtualLoss(-10);
        }

		node.incVisitCount();
		double Results = results[node.getPlayerID()];
//		double Results = results[player];
		boolean win = false;
		boolean draw = false;

		if (Results == 1) {
			win = true;
		} else if (Results == 0) {
			draw = true;
		}

		if (win) {
			node.addValue(1);
		} else if (draw) {
			node.addValue(0.5);
		}
	}
	
	public static <NN extends MctsNodeTreeParallelInterface<?, ?, ?>> BackpropagationSimple<NN> newBackpropagationSimple(int player) {
		return new BackpropagationSimple(player);
	}

}