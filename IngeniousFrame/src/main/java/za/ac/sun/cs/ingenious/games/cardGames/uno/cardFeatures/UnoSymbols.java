package za.ac.sun.cs.ingenious.games.cardGames.uno.cardFeatures;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature;

/**
 * The enum for Uno symbols.
 * 
 * @author Joshua Wiebe
 */
public enum UnoSymbols implements CardFeature{
	
	/** Uno symbol. */
	ZERO(0),
	ONE(1),
	TWO(2),
	THREE(3),
	FOUR(4),
	FIVE(5),
	SIX(6),
	SEVEN(7),
	EIGHT(8),
	NINE(9);

	/** 
	 * Values for hierarchical ordering. 
	 * Even though Uno has no hierarchy. (Just for sorting purposes)
	 */
	int value;
	
	/**
	 * Constructor for symbols.
	 *
	 * @param Value of symbol.
	 */
	UnoSymbols(int value){
		this.value = value;
	}
	
	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#getValue()
	 */
	@Override
	public int getValue() {
		return this.value;
	}

	/**
	 * @see za.ac.sun.cs.ingenious.games.cardGames.core.card.CardFeature#setValue(int)
	 */
	@Override
	public void setValue(int value) {
		this.value = value;
	}
}
