package za.ac.sun.cs.ingenious.search.rl.qlearning;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.search.rl.util.ExternalNeuralNetwork;
import za.ac.sun.cs.ingenious.search.rl.util.Transition;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Implementation of DQN where the main aspects (such as replay memory and neural networks) of the algorithm reside in
 * the Python application. This class handles communicating transitions to the Python application as well as choosing
 * exploration actions. Greedy actions are chosen by first requesting the action values from the Python application.
 *
 * @author Steffen Jacobs
 */
public class ExternalDQN extends QLearning {
    private static final String DEFAULT_NETWORK = "StandardCNN";
    private static final int DEFAULT_BUFFER_MEMORY = 50000;
    private static final int DEFAULT_SAMPLE_SIZE = 32;
    private static final int DEFAULT_SAMPLE_THRESHOLD = 35000;
    private static final int DEFAULT_SAMPLE_FREQUENCY = 4;
    private static final int DEFAULT_TARGET_NETWORK_UPDATE_FREQUENCY = 1000;
    private static final boolean DEFAULT_DOUBLE_LEARNING = true;
    private static final boolean DEFAULT_PRIORITIZED_SAMPLING = true;
    private static final double DEFAULT_BUFFER_ALPHA = 0.5;
    private static final double DEFAULT_LEARNING_RATE = 0.01;

    enum ActionSelection {
        ALL,
        AVAILABLE
    }

    private GameLogic<GameState> logic;
    private ExternalNeuralNetwork neuralNetwork;
    private StateFeatureExtractor featureExtractor;

    /**
     * Indicates whether the greedy action chosen should be the greedy action across all actions in the game, or if it
     * should be the greedy action across the available legal actions for the given state.
     */
    private ActionSelection greedyActionChoice;

    public ExternalDQN(
            String identifier,
            GameLogic logic,
            StateFeatureExtractor featureExtractor,
            int channels,
            int height,
            int width,
            List<Action> availableActions
    ) throws IOException {
        super(identifier);

        this.logic = logic;
        neuralNetwork = new ExternalNeuralNetwork();
        this.featureExtractor = featureExtractor;
        this.greedyActionChoice = ActionSelection.AVAILABLE;

        neuralNetwork.init(
                identifier,
                DEFAULT_NETWORK,
                channels,
                height,
                width,
                availableActions,
                DEFAULT_BUFFER_MEMORY,
                DEFAULT_SAMPLE_SIZE,
                DEFAULT_SAMPLE_THRESHOLD,
                DEFAULT_SAMPLE_FREQUENCY,
                DEFAULT_TARGET_NETWORK_UPDATE_FREQUENCY,
                DEFAULT_DOUBLE_LEARNING,
                DEFAULT_PRIORITIZED_SAMPLING,
                DEFAULT_BUFFER_ALPHA,
                DEFAULT_LEARNING_RATE,
                DEFAULT_GAMMA
        );
    }

    public ExternalDQN(
            String identifier,
            boolean enableTraining,
            GameLogic logic,
            StateFeatureExtractor featureExtractor,
            String network,
            int channels,
            int height,
            int width,
            List<Action> availableActions,
            double alpha,
            double epsilon,
            double epsilonStep,
            double epsilonMin,
            double gamma,
            int bufferMemory,
            int sampleSize,
            int sampleThreshold,
            int sampleFrequency,
            int targetNetworkUpdateFrequency,
            boolean doubleLearning,
            boolean prioritizedSampling,
            double bufferAlpha
    ) throws IOException {
        super(
                identifier,
                enableTraining,
                alpha,
                epsilon,
                epsilonStep,
                epsilonMin,
                gamma
        );

        this.logic = logic;
        neuralNetwork = new ExternalNeuralNetwork();
        this.featureExtractor = featureExtractor;
        this.greedyActionChoice = ActionSelection.AVAILABLE;

        neuralNetwork.init(
                identifier,
                network,
                channels,
                height,
                width,
                availableActions,
                bufferMemory,
                sampleSize,
                sampleThreshold,
                sampleFrequency,
                targetNetworkUpdateFrequency,
                doubleLearning,
                prioritizedSampling,
                bufferAlpha,
                alpha,
                gamma
        );
    }

    @Override
    public void update(GameState state, Action action, ScalarReward reward, GameState newState) {
        super.update(state, action, reward, newState);

        double[][][] newStateChannels = null;

        // Only include next state if it is not terminal. If it is terminal, leave as null.
        if (newState != null && !logic.isTerminal(newState)) {
            newStateChannels = featureExtractor.buildTensor(newState, logic);
        }

        Transition transition = new Transition(
                featureExtractor.buildTensor(state, logic),
                action,
                reward.getReward(),
                newStateChannels
        );

        if (enableTraining)
            neuralNetwork.put(transition);
    }

    @Override
    public Action greedyActionForState(GameState state, List<Action> availableActions) {
        Map<Action, Double> actionValues = neuralNetwork.getActionValues(featureExtractor.buildTensor(state, logic));
        Collection<Action> actionPool = null;

        if (greedyActionChoice == ActionSelection.ALL) {
            actionPool = actionValues.keySet();
        } else if (greedyActionChoice == ActionSelection.AVAILABLE) {
            actionPool = availableActions;
        }

        double maxValue = -Double.MAX_VALUE;
        Action maxAction = null;

        int maxValueFreq = 1;
        for (Action action : actionPool) {
            double actionValue = actionValues.get(action);

            if (actionValue > maxValue) {
                maxValue = actionValue;
                maxAction = action;
                maxValueFreq = 1;
            } else if (actionValue == maxValue) {
                maxValueFreq++;

                if (rng.nextDouble() < 1.0 / maxValueFreq) {
                    maxValue = actionValue;
                    maxAction = action;
                }
            }
        }

        return maxAction;
    }

    @Override
    public void notifyScore(double score) {
        if (score > maxScore) {
            neuralNetwork.createBackup("ExternalDQNBackup_" + score);
        }

        super.notifyScore(score);
    }

    public ExternalNeuralNetwork getNeuralNetwork() {
        return neuralNetwork;
    }

    public GameLogic<GameState> getLogic() {
        return logic;
    }

    public StateFeatureExtractor getFeatureExtractor() {
        return featureExtractor;
    }
}
