package za.ac.sun.cs.ingenious.games.mnk.engines;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFeatureExtractor;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.rl.qlearning.ExternalDQN;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MNKExternalDQNEngine extends MNKEngine {
    private final String NETWORK = "MnkNN";
    private final double ALPHA = 0.001;
    private final double EPSILON = 1.0;
    private final double EPSILON_STEP = 1.0 / 500000;
    private final double EPSILON_MIN = 0.1;
    private final double GAMMA = 0.97;
    private final int BUFFER_MEMORY = 50000;
    private final int SAMPLE_SIZE = 32;
    private final int SAMPLE_THRESHOLD = 50000;
    private final int SAMPLE_FREQUENCY = 4;
    private final int TARGET_NETWORK_UPDATE_FREQUENCY = 10000 / 4;
    private final boolean DOUBLE_LEARNING = true;
    private final boolean PRIORITIZED_SAMPLING = true;
    private final double BUFFER_ALPHA = 0.5;

    private static final int REWARD_MULTIPLIER = 2;
    private static final int REWARD_SHIFT = REWARD_MULTIPLIER / 2;

    private MNKState previousBoard = null;
    private Action previousAction = null;
    private ScalarReward previousReward = null;
    private ExternalDQN alg;
    private boolean enableTraining;

    public MNKExternalDQNEngine(EngineToServerConnection toServer) throws IOException, ClassNotFoundException {
        super(toServer);

        enableTraining = false;
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MNKExternalDQNEngine";
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        // Learn from previous move if match hasn't ended. This value should typically be 0.
        if (previousBoard != null) {
            alg.update(previousBoard, previousAction, previousReward, board);
        }

        // Record board, then choose an action.
        previousBoard = board.deepCopy();

        Action choice = null;
        List<Action> availableActions = logic.generateActions(board, playerID);
        choice = alg.chooseAction(board, availableActions);

        previousAction = choice;

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        // Learn from previous move if match has ended. This value should either be -1 or 1 for loss or win respectively.
        alg.update(previousBoard, previousAction, previousReward, null);
        alg.endEpisode();

        previousBoard = null;
        previousAction = null;
        previousReward = null;

        alg.printMetrics();
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward rewardReceived = (ScalarReward) a.getReward();

        previousReward = new ScalarReward(rewardReceived.getReward());
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        if (alg == null) {
            List<Action> actionsAvailable = new ArrayList<>();

            for (int i = 0; i < board.getHeight(); i++) {
                for (int j = 0; j < board.getWidth(); j++) {
                    actionsAvailable.add(new XYAction(j, i, playerID));
                }
            }

            try {
                double[][][] sampleChannelSet = new MNKFeatureExtractor(playerID).buildTensor(board, logic);
                alg = new ExternalDQN(
                        "MNKExternalDQNEngine_" + playerID,
                        enableTraining,
                        logic,
                        new MNKFeatureExtractor(playerID),
                        NETWORK,
                        sampleChannelSet.length,
                        sampleChannelSet[0].length,
                        sampleChannelSet[0][0].length,
                        actionsAvailable,
                        ALPHA,
                        EPSILON,
                        EPSILON_STEP,
                        EPSILON_MIN,
                        GAMMA,
                        BUFFER_MEMORY,
                        SAMPLE_SIZE,
                        SAMPLE_THRESHOLD,
                        SAMPLE_FREQUENCY,
                        TARGET_NETWORK_UPDATE_FREQUENCY,
                        DOUBLE_LEARNING,
                        PRIORITIZED_SAMPLING,
                        BUFFER_ALPHA
                );

                alg.displayChart();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        alg.printMetrics();
        alg.close();
    }

    public ExternalDQN getAlg() {
        return alg;
    }

    public void setTraining() {
        this.enableTraining = true;
    }
}
