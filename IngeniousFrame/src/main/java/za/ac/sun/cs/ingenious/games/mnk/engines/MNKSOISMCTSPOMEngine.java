package za.ac.sun.cs.ingenious.games.mnk.engines;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.mnk.MNKEngine;
import za.ac.sun.cs.ingenious.games.mnk.MNKFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mnk.MNKState;
import za.ac.sun.cs.ingenious.search.ismcts.ISMCTSNode;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSPOMDescender;
import za.ac.sun.cs.ingenious.search.ismcts.SubsetUCT;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;

public class MNKSOISMCTSPOMEngine extends MNKEngine {

	public MNKSOISMCTSPOMEngine(EngineToServerConnection toServer) {
		super(toServer);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	@Override
	public String engineName() {
		return "MNKSOISMCTSPOMEngine";
	}

	@Override
	public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
		ISMCTSNode<MNKState> root = new ISMCTSNode<MNKState>(board, logic, null, null,
				new ArrayList<Move>(logic.generateActions(board, board.nextMovePlayerID)));
		return new PlayActionMessage(MCTS.generateAction(root,
				new RandomPolicy<MNKState>(logic, new MNKFinalEvaluator(), logic, false),
				new SOISMCTSPOMDescender<MNKState>(logic, new SubsetUCT<>(), logic, playerID, logic),
				new SimpleUpdater<ISMCTSNode<MNKState>>(),
				Constants.TURN_LENGTH));
	}

}
