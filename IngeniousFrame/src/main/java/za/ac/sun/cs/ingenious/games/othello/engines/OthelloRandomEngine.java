package za.ac.sun.cs.ingenious.games.othello.engines;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.move.IdleAction;

import java.util.List;

/**
 * An engine for the Othello (Reversi) board game which plays random moves. 
 * The engine represents a player and receives game updates and requests
 * from the Referee (server).
 */
public class OthelloRandomEngine extends OthelloEngine {

	/**
	 * Constructs a new engine.
	 *
	 * @param serverConnection	the connection the engine uses to receive and send
	 *							information from and to the server
	 */
	public OthelloRandomEngine(EngineToServerConnection serverConnection)
	{
		super(serverConnection);
	}

	@Override
	public void setZobrist(ZobristHashing zobristHashing) {

	}

	/**
	 * Returns the Engine's name
	 *
	 * @return	the engine's name
	 */
	public String engineName()
	{
		return "Othello Random Engine";
	}

	/**
	 * Requests a move from the player and determines if the move is valid.
	 * If the move is valid, it is sent to the Referee. Otherwise the player
	 * is asked to generate a new move until the player's clock runs out or
	 * the maximum wrong moves is reached.
	 *
	 * @param message	the generate move request sent from the Referee
	 *
	 * @return			a valid move, or null if the clock ran out, the
	 *					player's tries ran out, or the player has forfeit
	 */
	public PlayActionMessage receiveGenActionMessage(GenActionMessage message) {
		if (output) {
			board.printPretty();
		}

		Action action = null;
		List<Action> actions = logic.generateActions(board, playerID);

		if (actions == null || actions.size() == 0) {
			return new PlayActionMessage(new IdleAction((byte) playerID));
		}

		int random = (int) (Math.random() * actions.size());

		try {
			action = actions.get(random);
		} catch (Exception e) {
			assert false;
		}

		if (action == null) {
			return new PlayActionMessage(new ForfeitAction((byte) playerID));
		}

		return new PlayActionMessage(action);
	}
}
