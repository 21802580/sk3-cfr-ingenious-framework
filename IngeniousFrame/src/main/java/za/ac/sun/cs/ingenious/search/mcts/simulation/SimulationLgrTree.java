package za.ac.sun.cs.ingenious.search.mcts.simulation;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SimulationLgrTree<S extends GameState> implements SimulationThreadSafe<S> {

    GameLogic<S> logic;
    // Evaluator in general usually evaluates to -1 for loss, 0 for draw, 1 for win (zero sum games).
    MctsGameFinalEvaluator<S> evaluator;
    ActionSensor<S> obs;
    ArrayList<Action> moves = new ArrayList<>();
    boolean recordMoves;

    private LGRTable visitedMovesTable;

    /**
     * Constructor.
     *
     * @param logic     The game logic used during the simulation.
     * @param evaluator The game evaluator used to determine the results of the simulation.
     * @param obs       Object to process actions made by the players during playouts.
     *                  Actions are observed from the point of view of the environment player.
     * @param visitedMovesTable This is a hashtable to store the last good reply for each move played
     */
    public SimulationLgrTree(GameLogic<S> logic, MctsGameFinalEvaluator<S> evaluator, ActionSensor<S> obs, LGRTable visitedMovesTable, boolean recordMoves) {
        this.logic = logic;
        this.evaluator = evaluator;
        this.obs = obs;
        this.visitedMovesTable = visitedMovesTable;
        this.recordMoves = recordMoves;
    }

    /**
     * The method that performs random simulation playout.
     *
     * @param state The game state from which to start the simulation playout.
     * @return The result scores
     */
    public SimulationTuple simulate(S state) {
        Action[] previousActions = new Action[2];
        previousActions[0] = null;
        previousActions[1] = null;

        ArrayList<Action> moveList = new ArrayList<>();

        while (!logic.isTerminal(state)) {
            int playerToPlay = -1;
            for (int playerId : logic.getCurrentPlayersToAct(state)) {
                playerToPlay = playerId;
            }
            Action nextAction;
            if (playerToPlay == 0) {
                nextAction = getNextAction(playerToPlay, previousActions[1], state);
                previousActions[0] = nextAction;
            } else {
                nextAction = getNextAction(playerToPlay, previousActions[0], state);
                previousActions[1] = nextAction;
            }
            if (recordMoves) {
                moves.add(nextAction);
            }
            logic.makeMove(state, obs.fromPointOfView(nextAction, state, playerToPlay));
            moveList.add(nextAction);
        }
        double[] scores = evaluator.getMctsScore(state);
        SimulationTuple returnValues = new SimulationTuple(moves, scores);
        return returnValues;
    }

    /**
     * @param previousAction
     * @param state
     * @return the next action as chosen by the Lgr algorithm
     */
    public Action getNextAction(int player, Action previousAction, S state) {
        Random randomGen = new Random();
        Action currentLgr = null;
        if (previousAction != null) {
            currentLgr = visitedMovesTable.getMoveCombinationScores().get(previousAction);
        }
        // stochastic distribution to prevent looping
        if (currentLgr != null && randomGen.nextInt(100) <= 90 && logic.validMove(state, currentLgr)) {
            Action action = currentLgr;
            return action;
        } else {
            List<Action> possibleActions;
            possibleActions = logic.generateActions(state, player);

            if (!possibleActions.isEmpty()) {
                int index = randomGen.nextInt(possibleActions.size());
                Action randomAction = possibleActions.get(index);
                return randomAction;
            }
        }
        return null;
    }

    /**
     * @return the list of moves made during the simulation
     */
    public ArrayList<Action> getMoves() {
        return moves;
    }

    /**
     * Game logic object getter.
     *
     * @return The game logic
     */
    public GameLogic<S> getLogic() {
        return logic;
    }

    public static <SS extends GameState> SimulationThreadSafe<SS> newSimulationLgrTree(GameLogic<SS> logic, MctsGameFinalEvaluator<SS> evaluator, ActionSensor<SS> obs, LGRTable visitedMovesTable, boolean recordMoves) {
        return new SimulationLgrTree(logic, evaluator, obs, visitedMovesTable, recordMoves);
    }

}
