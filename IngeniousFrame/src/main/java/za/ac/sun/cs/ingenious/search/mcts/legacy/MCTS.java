package za.ac.sun.cs.ingenious.search.mcts.legacy;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Arrays;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSPOMDescender;

/**
 * Implements MCTS for any game supporting the required interfaces. This can also be used for other
 * MCTS variants, for example ISMCTS using the appropriate descender / updater classes (for example,
 * for SOISMCTS-POM use {@link SOISMCTSPOMDescender}).
 */
public final class MCTS {

	private MCTS() {}	

	/**
	 * This method will generate an action by building up a game TreeEngine according to the rules
	 * in the supplied TreeDescender and TreeUpdater.
	 * 
	 * @param root The root node, holding the current game state and the current player.
	 * @param policy The policy for the playout. Most simple is to use {@link RandomPolicy},
	 *            which will just do random playouts.
	 * @param descender The descender that decides which node to expand next.
	 * @param updater The updater that backpropagates the results of playouts through the game TreeEngine.
	 * @param turnLength Time in ms to finish generating this move.
	 * @return returns the best action found after the specified amount of time.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> Action generateAction(N root,
			PlayoutPolicy<S> policy, TreeDescender<S,N> descender , TreeUpdater<N> updater, long turnLength) {

//		Log.set(1);

		long timeInit = System.currentTimeMillis();
		long endTime = timeInit + turnLength;
		
		int numberOfPlayouts = 0;
		while(System.currentTimeMillis() < endTime){
			numberOfPlayouts++;
			N bestNode = descender.descend(root, numberOfPlayouts);
			N expandedChild = descender.expand(bestNode);
			if(expandedChild != null) {
				bestNode = expandedChild;
			}
			updater.backupValue(bestNode, policy.playout(descender.getStateForPlayout(bestNode)).getPayoffs());
		}
		
		Move bestPlayMove = descender.bestPlayMove(root);
		Action bestAction = (bestPlayMove instanceof Action) ? (Action) bestPlayMove : null;
		printTreeOverview(root);
		printPossibleMoves(root);
		printTree(root);
		if (bestAction != null) {
			Log.debug("MCTS", "Best action found: " + bestAction.toString());
		} else {
			Log.debug("MCTS", "No best action found.");
		}
		Log.debug("MCTS", "Total number of playouts: " + numberOfPlayouts);

		return bestAction;
	}
	
	/**
	 * Helper method to nicely print the first level of the game TreeEngine.
	 * @param root The root of the TreeEngine. All moves leading out from this TreeEngine will be printed.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> void printPossibleMoves(N root){
		if (!Log.DEBUG) {
			return;
		}
		Log.debug("MCTS", "===================");
		Log.debug("MCTS", "Possible moves:");
		for(N node : root.getExpandedChildren()){
			Log.debug("MCTS", node.getMove().toString() + " winrate: " + Arrays.toString(node.getReward()) + "/"
					+ node.getVisits());
		}
		Log.debug("MCTS", "===================");
	}
	

	/**
	 * Print the TreeEngine up to a depth of 3. This generates excessive printout.
	 * @param root The root of the TreeEngine.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> void printTree(N root){
		printTree(root, 3);
	}

	/**
	 * Print the TreeEngine up to the specified depth. This generates excessive printout.
	 * @param root The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine printout.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> void printTree(N root, int maxDepth){
		if (!Log.TRACE) {
			return;
		}
		
		Log.trace("MCTS", "===================");
		Log.trace("MCTS", "Current search TreeEngine:");
		
		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty()){
			N n = list.remove(0);
			if (n.getDepth()>=maxDepth) {
				break;
			}
			Log.trace("MCTS", n.toString());
			list.addAll(n.getExpandedChildren());
		}
		Log.trace("MCTS", "===================");
	}
	
	
	/**
	 * Prints an overview over the amount of nodes in the search TreeEngine to a depth of 10.
	 * @param root The root of the TreeEngine.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> void printTreeOverview(N root){
		printTreeOverview(root, 10);
	}

	/**
	 * Prints an overview over the amount of nodes in the search TreeEngine to the specified depth.
	 * @param root The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine overview.
	 */
	public static <S extends TurnBasedGameState, N extends SearchNode<S,N>> void printTreeOverview(N root, int maxDepth){
		if (!Log.DEBUG) {
			return;
		}
		
		Log.debug("MCTS", "===================");
		Log.debug("MCTS", "Current search TreeEngine overview:");
		
		int[] count = new int[maxDepth];
		
		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty() ){
			N n = list.remove(0);

			if(n.getDepth() >= maxDepth){
				break;
			}

			count[n.getDepth()]++;
			list.addAll(n.getExpandedChildren());
		}
		
		for (int i = 0; i < count.length; i++) {
			Log.debug("MCTS", "depth " + i + ": " + count[i] + " nodes");
		}
		
		Log.debug("MCTS", "===================");
	}
}
