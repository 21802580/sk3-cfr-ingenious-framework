package za.ac.sun.cs.ingenious.core.exception.probability;

/**
 * Custom exception thrown when invalid distributions are provided as arguments.
 *
 * @author Steffen Jacobs
 */
public class IncorrectlyNormalizedDistributionException extends Exception {
    public IncorrectlyNormalizedDistributionException(String message)  {
        super(message);
    }
}
