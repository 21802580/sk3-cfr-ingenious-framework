package za.ac.sun.cs.ingenious.games.ingenious;

import za.ac.sun.cs.ingenious.core.util.misc.Coord;

/**
 * Tile object represents a game tile.
 * The rotation is based on the bottom hex being the pivot.
 * The two ints represent the colour of the individual hexes.
 * 
 * @author steven
 */


public class Tile {
	private final int Top;
	private final int Bottom;
	private int rotation; // an int between 0 - 5 that which in a antiClockwise fashion indicates the roation of the tile.
	private static final Coord TOP_LEFT =new Coord(0,-1);
	private static final Coord LEFT =new Coord(-1,0);
	private static final Coord BOTTOM_LEFT =new Coord(-1,1);
	private static final Coord BOTTOM_RIGHT =new Coord(0,1);
	private static final Coord RIGHT =new Coord(1,0);
	private static final Coord TOP_RIGHT =new Coord(1,-1);
				
	public static final Coord [] coordinateSystem = {TOP_LEFT,LEFT,BOTTOM_LEFT,BOTTOM_RIGHT,RIGHT,TOP_RIGHT};
		
	
	public Tile(int first, int second){
		this.Top = first;
		this.Bottom = second;
		this.rotation = 0;
	}

	public int getTopColour() {
		return Top;
	}
	
	/**
	 * Copy returns a tile with the same values.
	 * The rotation is not set.
	 * @return Tile
	 */
	public Tile copy(){
		return new Tile(Top,Bottom);
	}
	
	public boolean equals(Object o){
		if (o instanceof Tile){
			Tile tile = (Tile) o;
			if((tile.Top == this.Top) &&(tile.Bottom == this.Bottom) ){
				return true;
			} else if((tile.Top == this.Bottom ) &&(tile.Bottom == this.Top )){
				return true;
			}
		}
		return false;
	}

	public Coord getTopCoord(Coord bottomPosition){
		return bottomPosition.add(coordinateSystem[this.rotation]);
	}
	
	public int getBottomColour() {
		return Bottom;
	}
	
	public int getRotation() {
		return rotation;
	}
	
	public void setRotation(int rotation, int NumberOfColours){
		this.rotation = rotation%NumberOfColours;
	}
	
	public void incrementRotation( int NumberOfColours){
		this.rotation = this.rotation++;
		this.rotation = this.rotation%NumberOfColours;
	}

	public String toString(){
		return "("+this.Top+"-"+this.Bottom+") rotation => "+this.rotation;
	}		
}