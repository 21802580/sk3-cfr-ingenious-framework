package za.ac.sun.cs.ingenious.games.cardGames.core.card;

/**
 * The Interface CardFeature.
 * 
 * All card features need a value in order to make hierarchical distinctions (Comparable).
 * 
 * If a game doesn't have a hierarchy regarding it's cards just give it any value. 
 * It will only have an impact when printing the game state since a TreeMap is used (which relies on the Comparable interface) for internal representation.
 *
 * For most card types using an enum (see uno.cardFeatures) is very convenient.
 *
 * Please note: when comparing two cards the first feature is more dominant than the second feature.
 * 
 * @author Joshua Wiebe
 */
public interface CardFeature{
	
	/**
	 * Gets the value.
	 *
	 * @return value
	 */
	public int getValue();
	
	/**
	 * Sets the value.
	 *
	 * @param the new value
	 */
	public void setValue(int value);
}
