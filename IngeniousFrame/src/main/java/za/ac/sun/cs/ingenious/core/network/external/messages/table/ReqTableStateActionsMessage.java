package za.ac.sun.cs.ingenious.core.network.external.messages.table;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalState;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to request an action value for a provided state and action using the table in the Python app.
 *
 * @author Steffen Jacobs
 */
public class ReqTableStateActionsMessage extends ExternalMessage {
    private ExternalState payload;

    public ReqTableStateActionsMessage(GameState state) {
        super(ExternalMessageType.REQ_TABLE_STATE_ACTIONS);

        this.payload = new ExternalState(state);
    }
}
