package za.ac.sun.cs.ingenious.search.rl.qlearning;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.search.rl.util.ReinforcementLearningInstrumentation;

import java.io.Serializable;
import java.util.List;
import java.util.Random;

/**
 * Base class for Q-learning variants. Contains logic such as epsilon greedy action choice as well as instrumentation
 * invocations.
 *
 * @author Steffen Jacobs
 */
public abstract class QLearning extends ReinforcementLearningInstrumentation implements Serializable {
    protected static final double DEFAULT_ALPHA = 0.1;
    protected static final double DEFAULT_EPSILON = 1.0;
    protected static final double DEFAULT_EPSILON_NO_TRAINING = 0.0;
    private static final double DEFAULT_EPSILON_STEP = 1.0 / 1000000;
    private static final double DEFAULT_EPSILON_MIN = 0.01;
    protected static final double DEFAULT_GAMMA = 0.97;
    protected static final double DEFAULT_Q_VALUE = 0;

    protected double epsilonNoTraining = DEFAULT_EPSILON_NO_TRAINING;

    /**
     * Specifies whether or not the agent is being trained.
     */
    protected boolean enableTraining;

    /**
     * Random number generator.
     */
    protected Random rng;

    /**
     * Step size
     */
    protected double alpha;

    /**
     * Probability of taking a random action.
     */
    protected double epsilon;

    /**
     * Amount that epsilon decays every step.
     */
    protected double epsilonStep;

    /**
     * Value of epsilon where decay stops.
     */
    protected double epsilonMin;

    /**
     * Discount factor.
     */
    protected double gamma;

    public QLearning() {}

    public QLearning(String identifier) {
        super(identifier);

        alpha = DEFAULT_ALPHA;
        epsilon = DEFAULT_EPSILON;
        epsilonStep = DEFAULT_EPSILON_STEP;
        epsilonMin = DEFAULT_EPSILON_MIN;
        gamma = DEFAULT_GAMMA;
        enableTraining = true;
        rng = new Random();
    }

    public QLearning(
            String identifier,
            boolean enableTraining,
            double alpha,
            double epsilon,
            double epsilonStep,
            double epsilonMin,
            double gamma
    ) {
        super(identifier);

        this.enableTraining = enableTraining;
        this.alpha = alpha;
        this.epsilon = epsilon;
        this.epsilonStep = epsilonStep;
        this.epsilonMin = epsilonMin;
        this.gamma = gamma;
        rng = new Random();
    }

    public QLearning withRandom(Random random) {
        this.rng = random;
        return this;
    }

    public QLearning withEpsilon(double epsilon) {
        this.epsilon = epsilon;
        this.epsilonMin = epsilon;
        this.epsilonStep = 0;
        return this;
    }

    public QLearning withEpsilon(double epsilon, double epsilonStep, double epsilonMin) {
        this.epsilon = epsilon;
        this.epsilonStep = epsilonStep;
        this.epsilonMin = epsilonMin;
        return this;
    }

    public QLearning withAlpha(double alpha) {
        this.alpha = alpha;
        return this;
    }

    public void setEpsilonNoTraining(double epsilonNoTraining) {
        this.epsilonNoTraining = epsilonNoTraining;
    }

    /**
     * Updates policy using the reward received by moving from `state` to `newState` by taking `action`.
     *
     * @param state Previous state.
     * @param action Action taken at previous state.
     * @param reward Reward received by transitioning to newState.
     * @param newState Resulting state after taking action at state.
     */
    public void update(GameState state, Action action, ScalarReward reward, GameState newState) {
        addReward(reward.getReward());
    }

    /**
     * Choose an action from the list of available actions corresponding to the game state. Chooses a random action
     * with probability epsilon, otherwise greedily chooses the action with the best value in the policy corresponding
     * to Q(S,*).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return A chosen action.
     */
    public Action chooseAction(GameState state, List<Action> availableActions) {
        incrementStep();

        Action choice = null;

        // Override epsilon when agent is not training.
        double epsilonUsed = enableTraining ? this.epsilon : epsilonNoTraining;

        if (rng.nextDouble() < epsilonUsed) {
            choice = randomAction(availableActions);
        } else {
            choice = greedyActionForState(state, availableActions);
        }

        if (epsilon > epsilonMin)
            epsilon -= epsilonStep;

        return choice;
    }

    /**
     * Greedily chooses the best action from the available actions with the greatest value in the policy corresponding
     * to Q(state, *).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return The greedy action choice.
     */
    public abstract Action greedyActionForState(GameState state, List<Action> availableActions);

    /**
     * Chooses an action from the list of available actions randomly.
     *
     * @param availableActions List of actions available.
     * @return A random action.
     */
    public Action randomAction(List<Action> availableActions) {
        return availableActions.get(rng.nextInt(availableActions.size()));
    }
}
