package za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm;
import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationTuple;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * Implements a general-purpose TreeEngine parallelism version of the MCTS algorithm.
 * This implementation uses Threads and is for multicore systems with shared 
 * memory. Due to the shared memory nature of this algorithm, the MCTS node type 
 * that can be used for this algorithm should store the child and parent of the 
 * node as references to the node structure.
 * 
 * @author Karen Laubscher
 * 
 * @param <S>  The game state type
 * @param <N>  The MCTS node type
 */
public class MctsTree<S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> {
	private TreeSelection<N> selection;
	private ExpansionThreadSafe<N, N> expansion;
	private SimulationThreadSafe<S> simulation;
	private ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements;
	private TreeSelectionFinal<N> finalSelection;
	private GameLogic<S> logic;
	private int threadCount;
	int[] playoutCounts;
    private final CyclicBarrier barrier;
    private int playerId;
    private long totalPlayoutsOverGame = 0;
    private int numberTurns = 0;

	/**
	 * Constructor that takes in the 4 basic strategies as well as a final
	 * selection strategy, that may differ from the selection strategy used when
	 * traversing the TreeEngine during the MCTS algorithm.
	 *
	 * @param selection  The selection strategy. A common strategy to use is
	 *					 {@link TreeSelection}, which calculates UCT values.
	 * @param expansion	 The expansion strategy. A common strategy to use is
	 *  				 {@link ExpansionSingle}, which adds one unexplored node
	 * @param simulation The simulation strategy. A basic play out policy is to
	 *					 use {@link SimulationRandom}, which do random playout.
	 * @param backpropagationEnhancements
	 *                   The backpropagation strategy. A common strategy to use
	 *					 is {@link BackpropagationAverage}, just adding the result
	 * @param finalSelection
	 *				     The selection strategy used to make the final selection
	 *					 to determine the best move to be returned.
	 * @param logic 	 The game logic, used to check for ternimal gamestates.
	 * @param threadCount
	 *					 The number of TreeEngine threads that should be created.
	 */
	public MctsTree(TreeSelection<N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements, TreeSelectionFinal<N> finalSelection, GameLogic<S> logic, int threadCount, int playerId) {
		this.selection = selection;
		this.expansion = expansion;
		this.simulation = simulation;
		this.backpropagationEnhancements = backpropagationEnhancements;
		this.finalSelection = finalSelection;
		this.logic = logic;
		this.threadCount = threadCount;
		this.barrier = new CyclicBarrier(threadCount+1);
		this.playerId = playerId;
		playoutCounts = new int[this.threadCount];
	}
	
	
	/**
	 * This method generates an action that represents the best move choice 
	 * found. The method forms part of the template operation that drives the 
	 * TreeEngine parallelisation MCTS algorithm, in particular, it is the main thread
	 * operation, that creates the TreeEngine threads, and at the end, performs the
	 * final selection from the shared root node.
	 * 
	 * @param root        The root node from which the search should start.
	 * @param turnlength  Time in ms within which the move needs to be decided.
	 *
	 * @return The best move action that was found by the algorithm during the
	 *  	   time of the turn length.
	 */

	public N doSearch(N root, long turnlength, ZobristHashing zobrist) { // TODO: turnlength, change to clock?
		Log.set(2);
		long timeInit = System.currentTimeMillis();

		long endTime = timeInit + turnlength; // TODO: optimise (- final move selecin time)
		List<TreeThread> threads = new ArrayList<TreeThread>(threadCount);
	    for (int i = 0; i < threadCount; i++) {
            TreeThread th = new TreeThread(root, endTime, selection, expansion, simulation, backpropagationEnhancements, logic);
			threads.add(th);
			th.start();
		}

		try {
       		barrier.await();
      	} catch (InterruptedException ex) {
       		Log.error("simulationThread", "Thread " + Thread.currentThread().getId() + " was interrupted");
			return null;
	  	} catch (BrokenBarrierException ex) {
       		Log.error("simulationThread", "Thread " + Thread.currentThread().getId() + " broke out from barrier prematurely");
			return null;
       	}

       	N finalChoice = finalSelection.select(root, zobrist);
		Action bestMove = finalChoice.getPrevAction();

		printTreeOverview(root, 10);
		printPossibleMoves(root, playerId);
		printTree(root, 3);

		if (bestMove != null) {
			Log.debug("MctsTree", "Best action found: " + bestMove.toString());
		} else {
			Log.debug("MctsTree", "No best action found.");
		}

		int totalPlayoutCount = 0;
		for (int i = 0; i < threads.size(); i++) {
			Thread th = threads.get(i);
			int currentThreadPlayoutCount = ((TreeThread) th).getPlayoutCount();
			totalPlayoutCount += currentThreadPlayoutCount;
			Log.debug("Number of playouts over thread with id " + th.getId() + " is: " + currentThreadPlayoutCount);
		}
		Log.debug("Total number of playouts over all threads: " + totalPlayoutCount);
		totalPlayoutsOverGame += + totalPlayoutCount;
		numberTurns += 1;
		Log.debug("numTurns: " + numberTurns);
		Log.debug("Average number of playouts per turn to this point in the game: " + (totalPlayoutsOverGame/numberTurns));

		Log.debug("Best Move: " + bestMove);
		return finalChoice;
	}

	/**
	 * Print the TreeEngine up to the specified depth. This generates excessive printout.
	 * @param root The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine printout.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printTree(N root, int maxDepth){
		if (!Log.TRACE) {
			return;
		}

		Log.trace("MCTS", "===================");
		Log.trace("MCTS", "Current search TreeEngine:");

		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty()){
			N n = list.remove(0);
			if (n.getDepth()>=maxDepth) {
				break;
			}
			Log.trace("MCTS", n.toString());
			Log.trace("Depth: " + n.getDepth());


			// print board
			Log.trace("printing board state");
			n.getState().printPretty();

			list.addAll(n.getChildren());
		}
		Log.trace("MCTS", "===================");
	}

	/**
	 * Helper method to nicely print the first level of the game TreeEngine.
	 * @param root The root of the TreeEngine. All moves leading out from this TreeEngine will be printed.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printPossibleMoves(N root, int playerId){
		if (!Log.DEBUG) {
			return;
		}
		Log.debug("MCTS", "===================");
		Log.debug("MCTS", "Possible moves:");
		root.logPossibleMoves();
		Log.debug("MCTS", "===================");
	}

	/**
	 * Prints an overview over the amount of nodes in the search TreeEngine to the specified depth.
	 * @param root The root of the TreeEngine.
	 * @param maxDepth Depth at which to cut off the TreeEngine overview.
	 */
	public static <S extends GameState, N extends MctsNodeTreeParallelInterface<S, N, N>> void printTreeOverview(N root, int maxDepth){
		if (!Log.DEBUG) {
			return;
		}

		Log.debug("MCTS", "===================");
		Log.debug("MCTS", "Current search TreeEngine overview:");

		int[] count = new int[maxDepth];

		ArrayList<N> list = new ArrayList<>();
		list.add(root);
		while(!list.isEmpty() ){
			N n = list.remove(0);

			if(n.getDepth() >= maxDepth){
				break;
			}

			count[n.getDepth()]++;
			list.addAll(n.getChildren());
		}

		for (int i = 0; i < count.length; i++) {
			Log.debug("MCTS", "depth " + i + ": " + count[i] + " nodes");
		}

		Log.debug("MCTS", "===================");
	}


	/**
	 * The TreeEngine thread that performs MCTS on the shared TreeEngine, This forms part of
	 * the template operation that drives the TreeEngine parallelisation.
	 */
	private class TreeThread extends Thread {
		// root is shared
		private final N myRoot;
		private final long myEndTime;
		private TreeSelection<N> mySelection;
		private ExpansionThreadSafe<N, N> myExpansion;
		private SimulationThreadSafe<S> mySimulation;
		ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements;
		private GameLogic<S> myLogic;
		int numberOfPlayouts = 0;

	    public TreeThread(N root, long endTime, TreeSelection<N> selection, ExpansionThreadSafe<N, N> expansion, SimulationThreadSafe<S> simulation, ArrayList<BackpropagationThreadSafe<N>> backpropagationEnhancements, GameLogic<S> logic) {
	    	this.myRoot = root;
	    	this.myEndTime = endTime;
	    	this.mySelection = selection;
	    	this.myExpansion = expansion;
	    	this.mySimulation = simulation;
	    	this.backpropagationEnhancements = backpropagationEnhancements;
	    	this.myLogic = logic;
	    }

		/**
		 * @return number of playouts made by this thread
		 */
		public int getPlayoutCount() {
	    	return numberOfPlayouts;
		}

		@Override
	    public void run() {
	       	double[] results;
	       	long turnTime = 0;
			while (System.currentTimeMillis() < this.myEndTime) {
				long turnStartTime = System.currentTimeMillis();
				numberOfPlayouts++;
				myRoot.writeLock();
                N currentNode = myRoot;
				currentNode.writeLock();
				myRoot.writeUnlock();
				// Selection
				while (!this.myLogic.isTerminal(currentNode.getState()) && currentNode.isUnexploredEmpty()) {
					N previousNode = currentNode;
					currentNode = this.mySelection.select(previousNode);
					if (currentNode == null) {
						currentNode = previousNode;
						break;
					}
					currentNode.writeLock();
					previousNode.writeUnlock();
				}

				//Expansion
				N addedNode = this.myExpansion.expand(currentNode);
				currentNode.writeUnlock();

                // Simulation
                SimulationTuple resultsTuple = this.mySimulation.simulate(addedNode.getState());
                results = resultsTuple.getScores();
				ArrayList<Action> moves = resultsTuple.getMoves();

                // Backpropagation
                currentNode = addedNode;
				// update values from simulation to backprop object if rave
				if (currentNode != myRoot) {
					int size = backpropagationEnhancements.size();
					for (int i = 0; i < size; i++) {
						if (backpropagationEnhancements.get(i).getClass().equals(BackpropagationRave.newBackpropagationRave().getClass())) {
							BackpropagationRave raveBackprop = (BackpropagationRave)backpropagationEnhancements.get(i);
							raveBackprop.updateMovesFromSimulation(moves);
						}
					}
				}
                while (currentNode != myRoot) { // Pseudo: while (currentNode element of TreeEngine)
                	int size = backpropagationEnhancements.size();
                	for (int i = 0; i < size; i++) {
                		backpropagationEnhancements.get(i).propagate(currentNode, results);
					}
                    currentNode = currentNode.getParent();
                }
                myRoot.incVisitCount();
				long turnEndTime = System.currentTimeMillis();
				turnTime = turnTime + (turnEndTime-turnStartTime);

            }
			try {
           		barrier.await();
         	} catch (InterruptedException ex) {
           		Log.error("TreeThread", "Thread " + Thread.currentThread().getId() + " was interrupted");
           		return;
         	} catch (BrokenBarrierException ex) {
           		Log.error("TreeThread", "Thread " + Thread.currentThread().getId() + " broke out from barrier prematurely");
           		return;
         	}
	    }
	}
}
