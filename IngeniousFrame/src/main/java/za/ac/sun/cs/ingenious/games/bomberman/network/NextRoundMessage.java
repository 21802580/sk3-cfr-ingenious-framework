package za.ac.sun.cs.ingenious.games.bomberman.network;

import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * Message to indicate the current round is complete and the round counter in
 * the game state should be incremented.
 */
public class NextRoundMessage extends PlayedMoveMessage {

	private static final long serialVersionUID = 1L;
	
	/**
	 * This is only an indicator message. No action is contained. When this
	 * message is received the round counter should be updated. See BMLogic's
	 * nextRound method.
	 */
	public NextRoundMessage() {
		super((Move) null);
	}

}
