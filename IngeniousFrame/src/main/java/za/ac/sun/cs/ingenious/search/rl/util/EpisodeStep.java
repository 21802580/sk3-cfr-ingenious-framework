package za.ac.sun.cs.ingenious.search.rl.util;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Reward;

import java.util.Objects;

/**
 * Represents a step in an episode, containing the state, action and resulting reward.
 *
 * @author Steffen Jacobs
 */
public class EpisodeStep {
    private GameState state;
    private Action action;
    private Reward reward;

    public EpisodeStep(GameState state, Action action) {
        this.state = state;
        this.action = action;
    }

    public void setReward(Reward reward) {
        this.reward = reward;
    }

    public GameState getState() {
        return state;
    }

    public Action getAction() {
        return action;
    }

    public Reward getReward() {
        return reward;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EpisodeStep that = (EpisodeStep) o;
        return Objects.equals(state, that.state) &&
                Objects.equals(action, that.action) &&
                Objects.equals(reward, that.reward);
    }

    @Override
    public int hashCode() {
        return Objects.hash(state, action, reward);
    }
}
