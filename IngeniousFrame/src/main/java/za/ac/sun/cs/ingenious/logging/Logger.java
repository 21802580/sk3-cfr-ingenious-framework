package za.ac.sun.cs.ingenious.logging;

import com.esotericsoftware.minlog.Log;

import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

import za.ac.sun.cs.ingenious.core.commandline.gameserver.Driver;

/**
 * A logger implementation for the {@link Log} class. In addition to printing to the console, this logger
 * also outputs to a log file on the file system.
 * In order to use this, the logger must be registered with the {@link Log} class first using
 * the {@link Log#setLogger(com.esotericsoftware.minlog.Log.Logger)} method. For a usage example,
 * see {@link Driver#main(String[])}.
 */
public class Logger extends com.esotericsoftware.minlog.Log.Logger implements Closeable {

	private FileWriter fw;

	/**
	 * Creates a logger that logs to standard out and also a log file in the given directory. The name
	 * of the log file is constructed as:
	 * dd-MMM-yyyy-HH:mm-className.COUNT.log
	 * COUNT is any number beginning from 1 so that the resulting log file name does not exist already.
	 * 
	 * @param directory Directory to put the log file in.
	 * @param className Used for the file name of the logfile.
	 */
	public Logger(String directory, String className) {
		super();
		long yourmilliseconds = System.currentTimeMillis();
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy-HH:mm");    
		Date resultdate = new Date(yourmilliseconds);
		String dateString = sdf.format(resultdate);
		
		new File(directory).mkdir();
		String fileName = directory + File.separator + dateString +"-"+ className;
		
		int fileCount = 1;
		while ((new File(fileName + "." + fileCount + ".log").isFile()))
			fileCount++;
		fileName += "." + fileCount + ".log";
		
		try {
			fw = new FileWriter(fileName);
		} catch (IOException e) {
			System.err.println("Logger: could not create log file. Will only log to standard out.");
		}
	}
	
	@Override
	protected void print(String message) {
		super.print(message);
		if (fw!=null) {
			try {
				fw.write(message + "\n");
				fw.flush();
			} catch (IOException e) {
				System.err.println("Logger: could not log to file.");
			}
		}
	}

	@Override
	public void close() throws IOException {
		fw.close();
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
		fw.close();
	}

}
