package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;

import java.util.Objects;

/**
 * Action that consists of the action number corresponding to a set of actions.
 *
 * @author Steffen Jacobs
 */
public class DiscreteAction implements Action {
    private int playerID;
    private int actionNumber;

    public DiscreteAction(int playerID, int action) {
        this.playerID = playerID;
        this.actionNumber = action;
    }

    public int getActionNumber() {
        return actionNumber;
    }

    @Override
    public int getPlayerID() {
        return playerID;
    }

    @Override
    public Action deepCopy() {
        return new DiscreteAction(playerID, actionNumber);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DiscreteAction that = (DiscreteAction) o;
        return actionNumber == that.actionNumber
                && playerID == that.playerID;
    }

    @Override
    public int hashCode() {
        return Objects.hash(playerID, actionNumber);
    }

    @Override
    public String toString() {
        return "" + actionNumber;
    }
}
