package za.ac.sun.cs.ingenious.games.ingenious;

import java.io.Serializable;
/**
 * Variants are intra-player rule differences, a single match
 * can feature players with many different variants. Variants are
 * applied to a player by the referee.
 * 
 * @author steven
 */
public class Variant implements Serializable {
	
	/**
	 *  Variant Version
	 */
	private static final long serialVersionUID = -460148649382259989L;

	public String getName(){
		return name;
	}
	public boolean isBonusPlayAllowed() {
		return bonusPlayAllowed;
	}

	public boolean isSwapAllowed() {
		return swapAllowed;
	}

	public boolean isOutrightWinAllowed() {
		return outrightWinAllowed;
	}

	public boolean isOpponentsRackSizeVisible() {
		return opponentsRackSizeVisible;
	}

	public int getOpRackVisible() {
		return opRackVisible;
	}

	public int getBagLookAhead() {
		return bagLookAhead;
	}

	public int getMaximumColourScore() {
		return maximumColourScore;
	}

	public int getWinningScore() {
		return winningScore;
	}

	public int getRackSize() {
		return rackSize;
	}

	private String name;
	private boolean bonusPlayAllowed;
	private boolean swapAllowed;
	private boolean outrightWinAllowed;
	private boolean opponentsRackSizeVisible;
	private int opRackVisible;
	private final int bagLookAhead;
	private final int maximumColourScore;
	private final int winningScore;
	private final int rackSize;

	public static class VariantBuilder {
		/*
		 * Rules that can be specified for a variant.
		 */
		boolean bonusPlayAllowed;
		boolean swapAllowed;
		boolean outrightWinAllowed;
		boolean opponentsRackSizeVisible;
		int opRackVisible;
		int bagLookAhead;
		int maximumColourScore;
		int winningScore;
		int rackSize;
		String name;
		
		public VariantBuilder(String name , int bagLookAhead,	int maximumColourScore, int winningScore, int rackSize) {
			this.name = name;
			this.bagLookAhead = bagLookAhead;
			this.maximumColourScore = maximumColourScore;
			this.winningScore = winningScore;
			this.rackSize = rackSize;
		}
	
		public void setBonusPlay(boolean bonus){
			this.bonusPlayAllowed = bonus;
		}
		
		public void setSwap(boolean swap){
			this.swapAllowed = swap;
		}
		
		public void setOutrightWin(boolean win){
			this.outrightWinAllowed = win;
		}
	
		public void setOppRackSizeVisible(boolean visible){
			this.opponentsRackSizeVisible = visible;
		}
		public void setVisibleOppRackSize(int visible){
			this.opRackVisible = visible;
		}

		
		public void setMaxColour(int max){
			this.maximumColourScore = max;
		}

		public Variant createVariant() {
			return new Variant(this);
		}
	}
	
	private Variant(VariantBuilder b) {
		this.bagLookAhead = b.bagLookAhead;
		this.maximumColourScore = b.maximumColourScore;
		this.winningScore = b.winningScore;
		this.rackSize = b.rackSize;
		this.name = b.name;
	}
}