package za.ac.sun.cs.ingenious.core.commandline.client;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.commandline.CommandlineUtils;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.lobby.LobbyHandler;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.LobbyOverviewReply;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.logging.Logger;

/**
 * Class for executing a client. Hostname and port of the server, the game and
 * lobby to join, the engine to play with and a username are passed via the
 * commandline. Usernames must be unique, the server refuses a connection if a
 * user with the same name has already been connected. Users have the option to
 * choose between different lobbies, if there are multiple available. Once a
 * connection to a lobby has been established, the specified engine is
 * constructed and starts listening to messages from the server.
 *
 * All methods in this class are allowed to terminate the system via a
 * System.exit.
 *
 * Created by Chris Coetzee on 2016/07/18.
 */
public class Driver {

    public static Engine loadEngine(String classname, String enhancementPath, int threadCount, int turnLength, EngineToServerConnection conn) {
        Class<Engine> EngineClass = null;

        Engine instance = null;

        try {
            Class<?> o = Driver.class.getClassLoader().loadClass(classname);
            if (!Engine.class.isAssignableFrom(o)) {
                throw new IllegalArgumentException(String.format("%s does not extend %s",
                                                                 o.getCanonicalName(),
                                                                 Engine.class.getCanonicalName()));
            }
            EngineClass = (Class<Engine>) o;
        } catch (ClassNotFoundException e) {
            Log.error(String.format("Unknown Engine class: %s", classname));
            e.printStackTrace();
            System.exit(4);
        } catch (IllegalArgumentException e) {
            Log.error(String.format("Class does not extend engine: %s", classname));
            e.printStackTrace();
            System.exit(5);
        }

        try {
            if (EngineClass.getDeclaredConstructors()[0].getParameterCount() == 1) {
                instance = EngineClass.getDeclaredConstructor(EngineToServerConnection.class)
                        .newInstance(conn);
            } else {
                instance = EngineClass.getDeclaredConstructor(EngineToServerConnection.class, String.class, int.class, int.class)
                        .newInstance(conn, enhancementPath, threadCount, turnLength);
            }


        } catch (Exception e) {
            e.printStackTrace();
            System.exit(6);
        }
        return instance;

    }

    public static void filterLobbies(ClientArguments parsedArgs, List<String> lobbyNames,
            List<String> gameNames) {

        /* filter out all the loggingames that are not of this gametype */
        List<String> lobbyNamesUpdated = new ArrayList<>();
        List<String> gameNamesUpdated = new ArrayList<>();
        /* TODO refactor, see issue #120 */

        /* Atrocious game filtering code */
        for (int i = 0; i < lobbyNames.size(); i++) {
            String lobbyName = lobbyNames.get(i);
            String gametype = gameNames.get(i);

            if (parsedArgs.getGameType() == null && parsedArgs.getLobbyName() == null) {
                    /* no filter */
                lobbyNamesUpdated.add(lobbyName);
                gameNamesUpdated.add(gametype);
            } else if (parsedArgs.getGameType() != null && parsedArgs.getLobbyName() != null) {

                    /* dual filter */
                if (parsedArgs.getGameType().equals(gametype) && parsedArgs.getLobbyName().equals(
                        lobbyName)) {
                    lobbyNamesUpdated.add(lobbyName);
                    gameNamesUpdated.add(gametype);
                    break;
                }
            } else if (parsedArgs.getGameType() != null) {
                    /* single filter on game type */
                if (parsedArgs.getGameType().equals(gametype)) {
                    lobbyNamesUpdated.add(lobbyName);
                    gameNamesUpdated.add(gametype);
                }
            } else {
                    /* single filter on lobbyname */
                if (parsedArgs.getLobbyName().equals(lobbyName)) {
                    lobbyNamesUpdated.add(lobbyName);
                    gameNamesUpdated.add(gametype);
                    break;
                }
            }
        }

        /* use the updated entries */
        lobbyNames.clear();
        gameNames.clear();

        for (int i = 0; i < lobbyNamesUpdated.size(); i++) {
            lobbyNames.add(lobbyNamesUpdated.get(i));
            gameNames.add(gameNamesUpdated.get(i));
        }
    }

    public static String getChosenLobby(List<String> lobbyNames, List<String> gameTypes) {

        /* are there even any loggingames ?*/
        if (lobbyNames.size() == 0) {
            Log.error("There are no games of the given type at this time");
            return null;
        }

        /* Create options menu */
        if (lobbyNames.size() > 1) {
            List<String> messages = new ArrayList<>();

            for (int i = 0; i < lobbyNames.size(); i++) {
                messages.add(String.format("%s [game: %s]", lobbyNames.get(i), gameTypes.get(i)));
            }
            return CommandlineUtils.readChoice("Please choose a lobby", lobbyNames, messages);
        } else {
            return lobbyNames.get(0);
        }
    }

    public static void main(String args[]) {
        /* Parse the commandline arguments */
        final ClientArguments parsedArgs = new ClientArguments();
        parsedArgs.processArguments(args);

        Log.setLogger(new Logger(Constants.LogDirectory, "core.commandline.client.Driver"));
        Log.info("Client commandline script started");

        /* Start a client */
        LobbyHandler lh = new LobbyHandler();
        if (!lh.connect(parsedArgs.getServerHostname(),
                parsedArgs.getServerPort(), parsedArgs.getUsername())) {
			Log.info("Could not establish connection to server, terminating client script.");
			System.exit(-1);
		}

        /* fetch all the available loggingames */
        LobbyOverviewReply lobby = lh.refreshListOfLobbies();

        List<String> lobbyNames = new ArrayList<>(Arrays.asList(lobby.getLobbyNames()));
        List<String> gameTypes = new ArrayList<>(Arrays.asList(lobby.getGameNames()));

        filterLobbies(parsedArgs, lobbyNames, gameTypes);
        String lobbyName = getChosenLobby(lobbyNames, gameTypes);
        if (lobbyName == null) {
        	Log.info("Could not join lobby, client script shutting down");
        	lh.closeConnection();
        	System.exit(3);
        }

        /* establish the game connection */
        EngineToServerConnection conn = lh.joinLobby(lobbyName);
        if (conn == null) {
        	Log.info("Could not join lobby, client script shutting down");
        	lh.closeConnection();
        	System.exit(-1);
        }

        /* Load the engine  and check it's type */
        Engine engine = loadEngine(parsedArgs.getEngineClassname(), parsedArgs.getEnhancementConfigPath(),
                parsedArgs.getThreadCount(), parsedArgs.getTurnLength(), conn);

        /* Connect the client */
        conn.connectEngine(engine);
    }

}
