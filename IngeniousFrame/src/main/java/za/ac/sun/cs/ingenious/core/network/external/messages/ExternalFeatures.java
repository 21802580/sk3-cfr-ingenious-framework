package za.ac.sun.cs.ingenious.core.network.external.messages;

/**
 * Wraps a set of features to be sent to the Python application.
 *
 * @author Steffen Jacobs
 */
public class ExternalFeatures {
    private final static String TYPE_STR = "LinearFeatures";
    private String type;
    private double[][] data;

    public ExternalFeatures(double[][] featureSets) {
        this.type = TYPE_STR;
        this.data = featureSets;
    }

    public ExternalFeatures(double[] featureSet) {
        this.type = TYPE_STR;
        this.data = new double[][]{featureSet};
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double[][] getData() {
        return data;
    }

    public void setData(double[][] data) {
        this.data = data;
    }
}
