package za.ac.sun.cs.ingenious.games.mdp.engines;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.mdp.MDPEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPRewardEvaluator;
import za.ac.sun.cs.ingenious.search.rl.dp.PolicyIteration;

/**
 * Policy iteration engine for MDP.
 *
 * @author Steffen Jacobs
 */
public class MDPPolicyIterationEngine extends MDPEngine {
    /**
     * Value iteration algorithm used to solve the MDP.
     */
    private PolicyIteration<Long, MDPLogic<Long>, MDPRewardEvaluator<Long>> alg;

    public MDPPolicyIterationEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new PolicyIteration<>();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MDPPolicyIterationEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        // Since this can't be set at time of algorithm initialization, doing it here. These are important.
        alg.setStates(logic.getStates());
        alg.setLogic(logic);
        alg.setEvaluator(rewardEval);

        alg.initializeValueTable();
        alg.initializePolicy();
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        alg.solve();

        Action choice = alg.chooseActionForState(state);

        return new PlayActionMessage(choice);
    }
}
