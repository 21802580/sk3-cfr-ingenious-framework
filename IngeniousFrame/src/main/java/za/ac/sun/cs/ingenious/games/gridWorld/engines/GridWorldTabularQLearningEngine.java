package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.qlearning.TabularQLearning;

import java.util.List;
import java.util.Random;

/**
 * Tabular Q-learning engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldTabularQLearningEngine extends GridWorldEngine {
    private double alpha = 0.01;
    private double epsilon = 1.0;
    private double epsilonStep = 1.0 / 3000;
    private double epsilonMin = 0.1;
    private double gamma = 1.0;
    private boolean doubleLearning = true;
    private Random rng = new Random();
    private boolean loadFromPrevious = true;

    private GridWorldState previousState = null;
    private Action previousAction = null;
    private TabularQLearning alg;
    private String algorithmFileName = null;
    private boolean enableTraining;

    public GridWorldTabularQLearningEngine(EngineToServerConnection toServer) {
        super(toServer);

        this.enableTraining = false;
        algorithmFileName = "GridWorldTabularQLearning_Player" + playerID + ".alg";
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldTabularQLearningEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        if (alg == null) {
            try {
                if (!loadFromPrevious) throw new Exception();

                alg = TabularQLearning.fromFile(algorithmFileName);
            } catch (Exception e) {
                Log.info("Trained agent was not loaded from file. Creating new.");
                alg = new TabularQLearning(
                        "GridWorldTabularQLearningEngine_" + playerID,
                        enableTraining,
                        alpha,
                        epsilon,
                        epsilonStep,
                        epsilonMin,
                        gamma,
                        doubleLearning
                );
                alg.withRandom(rng);
                alg.setRecentStepCount(1000);
            }

            alg.displayChart();
        }
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(state, a.getMove());
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = null;
        List<Action> availableActions = logic.generateActions(state, playerID);

        choice = alg.chooseAction(state, availableActions);

        previousAction = choice.deepCopy();
        previousState = state.deepCopy();
        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        ScalarReward reward = (ScalarReward) a.getReward();

        alg.update(previousState, previousAction, reward, state);
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.printMetrics();
        alg.endEpisode();

        if (alg.isUsingDoubleLearning()) {
            Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTables(state, alg.getQTable(), alg.getQTable2()));
        } else {
            Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTable(state, alg.getQTable()));
        }
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        alg.save(algorithmFileName);

        if (alg.isUsingDoubleLearning()) {
            Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTables(state, alg.getQTable(), alg.getQTable2()));
        } else {
            Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTable(state, alg.getQTable()));
        }
        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicyTable(state, alg.getQTable()));
        alg.close();
    }

    public void setTraining() {
        this.enableTraining = true;
    }

    public TabularQLearning getAlg() {
        return alg;
    }

    public void setAlpha(double alpha) {
        this.alpha = alpha;
    }

    public void setEpsilon(double epsilon) {
        this.epsilon = epsilon;
    }

    public void setEpsilonStep(double epsilonStep) {
        this.epsilonStep = epsilonStep;
    }

    public void setEpsilonMin(double epsilonMin) {
        this.epsilonMin = epsilonMin;
    }

    public void setGamma(double gamma) {
        this.gamma = gamma;
    }

    public void setDoubleLearning(boolean doubleLearning) {
        this.doubleLearning = doubleLearning;
    }

    public void setLoadFromPrevious(boolean loadFromPrevious) {
        this.loadFromPrevious = loadFromPrevious;
    }

    public void setRng(Random rng) {
        this.rng = rng;
    }
}
