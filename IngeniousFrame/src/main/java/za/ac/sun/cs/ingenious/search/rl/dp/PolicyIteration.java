package za.ac.sun.cs.ingenious.search.rl.dp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;


/**
 * Implementation of the Value Iteration dynamic programming algorithm. This algorithm requires full knowledge of all
 * states as well as the full dynamics of the environment in order to solve the MDP. Currently only supports the MDP
 * game because of this. In order to apply this algorithm to other games, they will first need to be converted to an MDP
 * game. Another assumption made is that class parameter T, in MDPState, extends GameState (as opposed to evaluating
 * some arbitrary game representation, such as a list of states).
 *
 * @param <T> Type wrapped by MDPState in the MDP.
 * @param <L> Logic type.
 * @param <E> Evaluator type.
 *
 * @author Steffen Jacobs
 */
public class PolicyIteration<T, L extends MDPLogic<T>, E extends GameRewardEvaluator> {
    private static final double DEFAULT_THETA = 0.1;
    private static final double DEFAULT_STATE_VALUE = 0.0;
    private static final double DEFAULT_GAMMA = 0.99;

    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_WIDTH = 5;
    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_HEIGHT = 5;

    private int playerID;

    /**
     * Random number generator used for arbitrary policy initialization.
     */
    private Random rng;

    /**
     * Threshold specifying the accuracy of solved policy.
     */
    private double theta;

    /**
     * Discount factor.
     */
    private double gamma;

    /**
     * Game logic.
     */
    private L logic;

    /**
     * Evaluator for rewards.
     */
    private E evaluator;

    /**
     * Collection of non-terminal states.
     */
    private Collection<MDPState<T>> states;

    /**
     * Table containing the value corresponding to each state.
     */
    private Map<MDPState<T>, Double> valueTable;

    /**
     * Deterministic policy containing the action that is taken for each state.
     */
    private Map<MDPState<T>, Action> policy;

    public PolicyIteration() {
        theta = DEFAULT_THETA;
        gamma = DEFAULT_GAMMA;
        rng = new Random();
    }

    public PolicyIteration<T, L, E> withRandom(Random rng) {
        this.rng = rng;
        return this;
    }

    public void initializeValueTable() {
        valueTable = new HashMap<>();
        for (MDPState<T> state : this.states) {
            valueTable.put(state, DEFAULT_STATE_VALUE);
        }
    }

    public void initializePolicy() {
        policy = new HashMap<>();

        for (MDPState<T> state : states) {
            List<Action> actions = logic.generateActions(state, playerID);

            if (actions.size() > 0)
                policy.put(state, actions.get(rng.nextInt(actions.size())));
        }
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public void setStates(Collection<MDPState<T>> states) {
        this.states = states;
    }

    public void setLogic(L logic) {
        this.logic = logic;
    }

    public void setEvaluator(E evaluator) {
        this.evaluator = evaluator;
    }

    public Map<MDPState<T>, Action> getPolicy() {
        return policy;
    }

    public Map<MDPState<T>, Double> getValueTable() {
        return valueTable;
    }

    public void solve() {
        if (states == null || valueTable == null || policy == null) {
            Log.error("Set of states, value table or policy were not set for PolicyIteration algorithm. Ensure that these are set after algorithm initialization.");
        }

        boolean policyStable = false;

        while (!policyStable) {
            policyEvaluation();
            policyStable = policyImprovement();
        }
    }

    public void policyEvaluation() {
        double delta = Double.MAX_VALUE;
        Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p = logic.getP();

        while (delta >= theta) {
            delta = 0.0;
            for (MDPState<T> state : states) {
                if (logic.isTerminal(state)) continue;

                double stateValue = valueTable.get(state);

                double newValue = 0.0;
                Action policyActionForState = policy.get(state);

                for (MDPState<T> newState : p.get(state).get(policyActionForState).keySet()) {
                    MDPState<T> clonedNewState = (MDPState<T>)newState.deepCopy();
                    clonedNewState.setPreviousGameState(state.getGameState());

                    GameState retrievedNewState = null;
                    if (clonedNewState.getGameState() instanceof GameState) {
                        retrievedNewState = (GameState)clonedNewState.getGameState();
                    } else {
                        retrievedNewState = clonedNewState;
                    }

                    double rewardForNewState = evaluator.getReward(retrievedNewState)[playerID];
                    double probabilityOfNewState = p.get(state).get(policyActionForState).get(newState);
                    double valueOfNewState = valueTable.get(newState);

                    newValue += probabilityOfNewState * (rewardForNewState + gamma * valueOfNewState);
                }
                valueTable.put(state, newValue);

                double newDelta = Math.abs(stateValue - newValue);
                if (newDelta > delta)
                    delta = newDelta;
            }
        }
    }

    public boolean policyImprovement() {
        Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p = logic.getP();
        boolean policyStable = true;

        for (MDPState<T> state : states) {
            if (logic.isTerminal(state)) continue;

            Action oldAction = policy.get(state);

            List<Action> availableActions = logic.generateActions(state, playerID);
            double maxActionValue = -Double.MAX_VALUE;
            Action maxAction = null;

            for (Action action : availableActions) {
                double actionValue = 0;
                for (MDPState<T> newState : p.get(state).get(action).keySet()) {
                    MDPState<T> clonedNewState = (MDPState<T>)newState.deepCopy();
                    clonedNewState.setPreviousGameState(state.getGameState());

                    GameState retrievedNewState = null;
                    if (clonedNewState.getGameState() instanceof GameState) {
                        retrievedNewState = (GameState)clonedNewState.getGameState();
                    } else {
                        retrievedNewState = clonedNewState;
                    }

                    actionValue += p.get(state).get(action).get(newState)
                            * (evaluator.getReward(retrievedNewState)[playerID] + gamma * valueTable.get(newState));
                }

                if (actionValue > maxActionValue) {
                    maxActionValue = actionValue;
                    maxAction = action;
                }
            }
            policy.put(state, maxAction);

            if (!oldAction.equals(maxAction))
                policyStable = false;
        }

        return policyStable;
    }

    public Action chooseActionForState(MDPState<T> state) {
        return policy.get(state);
    }

    /**
     * Provides a visualization of the state values, applicable only to MDPs for games in a grid e.g. GridWorld.
     * @param h Grid height
     * @param w Grid width
     */
    public void printStateGrid(int h, int w) {
        Log.info("Value Table: \n");

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                double value = valueTable.get(state);
                sb.append(df.format(value)).append("\t");
            }
            sb.append("\n\n");
        }

//        Log.info(sb);
//        printPolicyGrid(MDP_GRID_WIDTH, MDP_GRID_HEIGHT);
    }

    /**
     * This method is rather hardcoded, but for the moment is used to print gridworld MDPs.
     *
     * @param w
     * @param h
     */
    public void printPolicyGrid(int w, int h) {
        Log.info("Q Table: \n");
        Set<MDPState<T>> keySet = policy.keySet();

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Action a = policy.get(state);

                if (a == null) {
                    sb.append("• ");
                    continue;
                }

                switch (((DiscreteAction) a).getActionNumber()) {
                    case 0:
                        sb.append("\u2191 ");
                        break;
                    case 1:
                        sb.append("\u2192 ");
                        break;
                    case 2:
                        sb.append("\u2193 ");
                        break;
                    case 3:
                        sb.append("\u2194 ");
                        break;
                }
            }
            sb.append("\n");
        }

        sb.append("\n\n");

        Log.info(sb);
        Log.info("==========================");
    }
}
