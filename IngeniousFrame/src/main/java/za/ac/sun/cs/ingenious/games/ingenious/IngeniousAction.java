package za.ac.sun.cs.ingenious.games.ingenious;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

public class IngeniousAction implements Action {

	private final Tile tile;
	private final Coord position;

	public IngeniousAction(Tile tile, Coord position){
		this.tile = tile;
		this.position = position;
	}

	public Tile getTile(){
		return this.tile;
	}
	public Coord getPosition(){
		return this.position;
	}
	@Override
	public String toString(){
		String ret = "Tile : "+tile.toString()+"\n";
		ret += "Coordinate : "+ position.toString();
		return ret;
	}

	@Override
	public int getPlayerID() {
		// TODO Auto-generated method stub
		return -1;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}