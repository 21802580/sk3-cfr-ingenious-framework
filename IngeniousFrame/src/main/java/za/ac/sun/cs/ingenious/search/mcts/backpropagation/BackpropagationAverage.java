package za.ac.sun.cs.ingenious.search.mcts.backpropagation;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

public class BackpropagationAverage<N extends MctsNodeCompositionInterface<?, ?, ?>> implements BackpropagationThreadSafe<N> {
	
	public BackpropagationAverage() {
	}

	/**
	 * Update the fields relating to this backpropagation strategy for the current node.
	 * @param node
	 * @param results the win/loss or draw outcome for the simulation relating to the current playout.
	 */
	public void propagate(N node, double[] results) {
		double oldValue = node.getValue();
		double oldVisitCount = node.getVisitCount();
		double tempTotal = (oldValue * oldVisitCount) + results[node.getPlayerID()];
		node.incVisitCount();
		node.addValue(tempTotal/node.getVisitCount() - oldValue);
	}
	
	public static <NN extends MctsNodeCompositionInterface<?, ?, ?>> BackpropagationAverage<NN> newBackpropagationAverage() {
		return new BackpropagationAverage();
	}
	
}
