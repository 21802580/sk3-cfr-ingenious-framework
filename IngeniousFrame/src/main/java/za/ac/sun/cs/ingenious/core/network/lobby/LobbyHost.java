package za.ac.sun.cs.ingenious.core.network.lobby;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.RefereeFactory;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.ServerToEngineConnection;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.JoinedLobbyMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.messages.SendNameMessage;

/**
 * Handles all actions specific to a lobby on the server side. Can accept new joining
 * players and will start the game once enough players have joined.
 */
public class LobbyHost {

    protected final    MatchSetting               matchSettings;
    protected volatile int                        numberOfEngines;
    private            int                        maxPlayers;
    protected          ServerToEngineConnection[] engineConns;

    private Referee referee; // null if game is not yet started

    private LobbyManager lobbyManager;

    public LobbyHost(MatchSetting match, LobbyManager lobbyManager) {
        this.matchSettings = match;
        this.numberOfEngines = 0;
        this.lobbyManager = lobbyManager;
        this.maxPlayers = match.getNumPlayers();
        this.engineConns = new ServerToEngineConnection[matchSettings.getNumPlayers()];
        this.referee = null; // Game is not yet started
    }

    /**
     * Adds the given player to the lobby. Performs a handshake wherein the new player is assigned an ID
     * Once the last player has joined the game is started immediately.
     * @return ID of the newly joined player or -1 if there was an error.
     */
    public synchronized int acceptJoiningPlayers(Socket client, ObjectInputStream is,
            ObjectOutputStream os) throws IOException, ClassNotFoundException {
        int id = getFreeID();
        Log.info("Player " + id + " joined lobby, initiating handshake");

        // Perform Handshake with newly connected player
        String playerName = "NO_NAME_RECEIVED";
		os.writeObject(new JoinedLobbyMessage(id));
		playerName = ((SendNameMessage)is.readObject()).getPlayerName();
        Log.info("Player name for id " + id + ": " +playerName);

        ServerToEngineConnection engine = new ServerToEngineConnection(id, this, client, is, os, playerName);

        if (engineConns[numberOfEngines] == null) {
            engineConns[numberOfEngines] = engine;
            numberOfEngines++;
            Log.info("The number of engines has just been increased to : " + numberOfEngines);
        } else {
        	Log.error("Cannot accept new joining player, need last player to disconnect first");
        	return -1;
        }
        if (numberOfEngines == matchSettings.getNumPlayers()) {
            startGame();
        }
        return id;
    }

    private int getFreeID() {
        for (int i = 0; i < engineConns.length; i++) {
            if (engineConns[i] == null) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Starts the game by unregistering this lobby, creating a referee object suitable for this game
     * using the {@link RefereeFactory} and then starting the referee in a new thread.
     * A reference to the newly started referee is kept in the lobby manager (so that the
     * {@link GameServer} can keep track of all the threads it started).
     */
    protected void startGame() {
    	if (referee != null) {
    		Log.error("LobbyHost", "The lobby " + matchSettings.getLobbyName() + " for game " + matchSettings.getGameName() + " has already started the game once. Can not start the game again.");
    		return;
    	}

        Log.info("Starting game");
        if (lobbyManager != null) {
            lobbyManager.unregisterLobby(matchSettings.getLobbyName());
            for (ServerToEngineConnection engine : engineConns) {
            	lobbyManager.removeClientHandler(engine.getPlayerName());
            }
        }
        referee = RefereeFactory.getInstance().getReferee(matchSettings, engineConns);

        Thread newThread = new Thread(referee);
        newThread.start();
        lobbyManager.registerStartedReferee(newThread);
    }

    public MatchSetting getMatchSetting() {
        return this.matchSettings;
    }

    /**
     * Called whenever an engine disconnects.
     */
    public void engineDisconnected(int id) {
        Log.info("Player " + id + " disconnected.");
        numberOfEngines--;

        if (referee == null) { // Game not yet started
            engineConns[id] = null;
            if (numberOfEngines == 0) {
                lobbyManager.unregisterLobby(matchSettings.getLobbyName());
                Log.info("Lobby " + matchSettings.getLobbyName()
                                           + " unregistered due to no more players");
            }
        } else {
        	referee.engineDisconnected(id);
        }
    }

    public int getNumberOfEngines() {
        return numberOfEngines;
    }

    public int getMaxPlayers() {
        return matchSettings.getNumPlayers();
    }

}
