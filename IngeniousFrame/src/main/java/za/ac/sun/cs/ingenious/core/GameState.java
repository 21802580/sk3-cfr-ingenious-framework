package za.ac.sun.cs.ingenious.core;

import com.rits.cloning.Cloner;

import java.util.Observable;
import java.io.Serializable;

import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;

/**
 * Represents one state of some game. Must be extended with specific information for each
 * game. Note that for both perfect and imperfect information games, the GameState
 * contains <b>all</b> information that constitutes a state of the game. In games of
 * imperfect information, a GameState object may represent an information set that a
 * player of the game sees. But it must also support representing the complete state of
 * the game as seen e.g. by the referee.
 *
 * @author Michael Krause
 */
public abstract class GameState extends Observable implements Serializable {

	protected static final Cloner cloner=new Cloner();

	protected int numPlayers;

	public GameState(int numPlayers) {
		this.numPlayers = numPlayers;
	}

	public int getNumPlayers() {
		return numPlayers;
	}

	/**
	 * Print a representation of this state. This is used e.g. to display the
	 * GameState after a player made their move.
	 */
    // TODO - should take a logging level as a parameter - see issue 147
	public abstract void printPretty();

	/**
	 * This can provide a deep copy of !any! GameState using the Cloning library.
	 * Note that the cloning library is rather slow. Thus, this method can be overridden with manual
	 * deep copying functions for specific game states. For example, see {@link TurnBased2DBoard#deepCopy()}.
	 * @return A deep copy of this GameState
	 */
	public GameState deepCopy() {
		return cloner.deepClone(this);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numPlayers;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GameState other = (GameState) obj;
		if (numPlayers != other.numPlayers)
			return false;
		return true;
	}

}
