package za.ac.sun.cs.ingenious.games.snake;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.GameSpeed;
import za.ac.sun.cs.ingenious.core.util.referee.FullyObservableMovesReferee;
import za.ac.sun.cs.ingenious.games.snake.moves.SnakeSyncAction;

import java.util.Map;

/**
 * Referee for Snake that also handles keeping the agent's game synchronized with randomized food spawns.
 *
 * @author Steffen Jacobs
 */
public class SnakeReferee extends FullyObservableMovesReferee<SnakeState, SnakeLogic, SnakeFinalEvaluator> {
    private SnakeRewardEvaluator rewardEval;
    private Coord previousFoodCoord;
    private GameSpeed gameSpeed = GameSpeed.MEDIUM;

    public SnakeReferee(MatchSetting matchSetting, PlayerRepresentation[] players) {
        super(matchSetting, players, new SnakeState(), new SnakeLogic(), new SnakeFinalEvaluator());

        previousFoodCoord = null;
        rewardEval = new SnakeRewardEvaluator(logic);
    }

    @Override
    protected InitGameMessage createInitGameMessage(PlayerRepresentation player) {
        return new MatchSettingsInitGameMessage(matchSettings);
    }

    @Override
    protected void beforeRound() {
        super.beforeRound();

        Coord currentFoodCoord = currentState.getFood();

        /*
         * If the food location has not yet been synced yet for this round, communicate this to the agent. The reason
         * this must be done is because food locations are not deterministic, so the referee must communicate where the
         * food has spawned at the start of the round.
         */
        if (previousFoodCoord == null) {
            for (int i = 0; i < players.length; i++) {
                players[i].playMove(new PlayedMoveMessage(new SnakeSyncAction(i, currentFoodCoord)));
            }
        }

        previousFoodCoord = currentFoodCoord;
    }

    @Override
    protected void afterRound(Map<Integer, PlayActionMessage> messages) {
        super.afterRound(messages);

        try {
            Thread.sleep(gameSpeed.getValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void reactToValidAction(int player, PlayActionMessage m) {
        super.reactToValidAction(player, m);

        Coord currentFoodCoord = currentState.getFood();

        /*
         * If the location of the food has changed as a result of a recent action (i.e. has been eaten), communicate
         * this to the agent. The reason this must be done is because food locations are not deterministic, so there is
         * no way of updating the state for the agent without the referee communicating where the food has spawned.
         */
        if (!currentFoodCoord.equals(previousFoodCoord)) {
            for (int i = 0; i < players.length; i++) {
                players[i].playMove(new PlayedMoveMessage(new SnakeSyncAction(i, currentFoodCoord)));
            }
        }

        previousFoodCoord = currentFoodCoord;
    }

    @Override
    protected void reset() {
        super.reset();

        previousFoodCoord = null;
        currentState.spawnFood();
    }

    @Override
    protected double[] calculatePlayerRewards() {
        return rewardEval.getReward(currentState);
    }

    public void setGameSpeed(GameSpeed gameSpeed) {
        this.gameSpeed = gameSpeed;
    }
}
