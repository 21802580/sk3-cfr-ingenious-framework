package za.ac.sun.cs.ingenious.games.bomberman.network;

import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;

/**
 * Action for initializing an engine for a bomberman match.
 */
public class BMInitGameMessage extends InitGameMessage {
	private static final long serialVersionUID = 1L;
	private char playerID;
	private int engineTurn;
	private int nPlayers;
	private char[][] board;
	
	public BMInitGameMessage(char playerID, int engineTurn, BMBoard board) {
		this.playerID = playerID;
		this.engineTurn = engineTurn;
		this.board = board.asCharArray();
        nPlayers = board.getPlayers().length;
	}
	
	public int getEngineTurn() {
		return engineTurn;
	}
	
	public char getPlayerID() {
		return playerID;
	}

    public int getNPlayers() {
        return nPlayers;
    }
	
	public char[][] getBoard() {
		return board;
	}
	
}
