package za.ac.sun.cs.ingenious.core.network;

/**
 * Class containing some default settings and indicator strings used by the network package.
 */
public class TCPProtocol {
	public static final int PORT = 61234;
	public static final String SERVER = "localhost";
	
	//Lobby Connection
	public static final String SUCCESS = "Success";
	public static final String FAILURE = "Failure";
	
	// The following constants are still used in some parts of the framework. Sending strings
	// directly is deprecated and should be replaced by communication wrapped in {@link
	// Message} objects.
	@Deprecated
	public static final String ID = "id";
	@Deprecated
	public static final String NAME = "name";
	@Deprecated
	public static final String MATCHSETTING = "MatchSetting";
	
}
