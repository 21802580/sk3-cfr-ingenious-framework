package za.ac.sun.cs.ingenious.games.gridWorld;

import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

import java.util.Map;
import java.util.Set;

/**
 * Class containing the final evaluator for GridWorld. GridWorld has no concept of game score at the end of the game,
 * only whether or not an agent has accomplished its task. This evaluator therefore regards winning as the task being
 * accomplished, and losing as otherwise.
 *
 * @author Steffen Jacobs
 */
public class GridWorldFinalEvaluator implements GameFinalEvaluator<GridWorldState> {
    public static final int WIN_SCORE = 1;
    public static final int LOSE_SCORE = -1;

    private Map<Coord, Double> rewards;

    public GridWorldFinalEvaluator(Map<Coord, Double> rewards) {
        this.rewards = rewards;
    }

    @Override
    public double[] getScore(GridWorldState forState) {
        double[] score = new double[forState.getNumPlayers()];

        for (int i = 0; i < forState.getNumPlayers(); i++) {
            score[i] = LOSE_SCORE;
            Coord playerCoords = forState.findPlayerCoords(i);

            /*
             * Assume that if the terminal position has a positive reward, the agent has accomplished a task.
             */
            if (rewards.containsKey(playerCoords) && rewards.get(playerCoords) > 0) {
                score[i] = WIN_SCORE;
            }
        }

        return score;
    }
}
