package za.ac.sun.cs.ingenious.search.mcts.selection;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

/**
 * The selection strategy that determines how the TreeEngine is traversed from the
 * root node down. The strategy should be mindful about the balance between 
 * exploration and exploitation. A standard implementation of this strategy is 
 * {@link TreeSelectionUct}.
 *
 * @author Karen Laubscher
 * 
 * @param <N> The mcts node type
 */
public interface SelectionThreadSafe<C, N extends MctsNodeCompositionInterface<?, C, ?>> {

	/**
	 * The method that decide which child node to traverse to next.
	 * 
	 * @param node  The current node whose children nodes are considered for the
	 *				next node to traverse to.
	 * 
	 * @return      The child node that is selected.
	 */
	public C select(N node);

}
