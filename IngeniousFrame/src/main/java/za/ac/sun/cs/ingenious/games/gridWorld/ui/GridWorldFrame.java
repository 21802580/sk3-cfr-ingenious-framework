package za.ac.sun.cs.ingenious.games.gridWorld.ui;

import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;

import javax.swing.JFrame;
import javax.swing.WindowConstants;
import java.awt.Dimension;

/**
 * JFrame for GridWorld that wraps `GridWorldPanel`.
 *
 * @author Steffen Jacobs
 */
public class GridWorldFrame extends JFrame {
    private final GridWorldPanel panel;

    public GridWorldFrame(GridWorldState sampleState) {
        Dimension dimension = new Dimension(
                sampleState.board[0].length * GridWorldPanel.BLOCK_WIDTH,
                sampleState.board.length * GridWorldPanel.BLOCK_WIDTH
        );

        panel = new GridWorldPanel();
        panel.setPreferredSize(dimension);

        setSize(dimension);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setContentPane(panel);
        pack();
    }

    public GridWorldPanel getPanel() {
        return panel;
    }
}