package za.ac.sun.cs.ingenious.games.gridWorld;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.message.MatchSettingsInitGameMessage;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldLevelParser;

/**
 * Base engine for GridWorld, handling game instance initialization and moves received from the referee.
 *
 * @author Steffen Jacobs
 */
public abstract class GridWorldEngine extends Engine {
    protected GridWorldState state;
    protected GridWorldLogic logic;
    protected GridWorldFinalEvaluator eval;
    protected GridWorldLevelParser parser;

    public GridWorldEngine(EngineToServerConnection toServer) {
        super(toServer);
        state = null;
        logic = null;
        eval = null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        Log.info("Final scores for my terminal position:");
        double[] score = eval.getScore(state);
        for (int i = 0; i < score.length; i++) {
            Log.info("Player " + i + ": " + score[i]);
        }
        Log.info("Final state as seen by player " + this.playerID + ":");
        state.printPretty();

        toServer.closeConnection();
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        MatchSetting ms = ((MatchSettingsInitGameMessage) a).getSettings();

        try {
            parser = new GridWorldLevelParser(ms);

            logic = new GridWorldLogic(parser);
            state = new GridWorldState(parser);
            eval = new GridWorldFinalEvaluator(parser.getRewards());
        } catch (MissingSettingException | IncorrectSettingTypeException e) {
            Log.error("GridWorld init failed with message: " + e.getMessage() + ".");
            e.printStackTrace();
        }
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(state, a.getMove());
    }
}
