package za.ac.sun.cs.ingenious.core;

import za.ac.sun.cs.ingenious.core.network.game.ServerToEngineConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;

/**
 * Interface representing a player connected to a server.
 * The concrete implementation of this is {@link ServerToEngineConnection}
 */
public interface PlayerRepresentation {

	/**
	 * @return The connected player's ID
	 */
	public int getID();

	/**
	 * Start the game by sending the given message to the player
	 */
	public void initGame(InitGameMessage a);

	/**
	 * Inform the player that the given move was played
	 */
	public void playMove(PlayedMoveMessage a);

	/**
	 * Request an action from the player represented by this interface. When overwriting
	 * this method one must ensure that the clock restriction (as defined in the
	 * GenActionMessage) are enabled!
	 *
	 * @param a GenActionMessage containing a clock.
	 * @return The player's action embedded in a PlayActionMessage.
	 */
	public PlayActionMessage genAction(GenActionMessage a);

	/**
	 * End the game by sending the given message to the player
	 */
	public void terminateGame(GameTerminatedMessage a);

	/**
	 * Reset the match by sending the given message to the player.
	 */
	public void resetMatch(MatchResetMessage a);

	public default void reward(RewardMessage a) {};
}
