package za.ac.sun.cs.ingenious.core;

import com.rits.cloning.Cloner;

import java.io.Serializable;

/**
 * Any object that implements this interface serves as a reward that can be
 * given to an engine that implements reinforcement learning.
 *
 * @author Elan van Biljon
 */
public interface Reward extends Serializable {
	public static final Cloner cloner = new Cloner();

	public Reward deepCopy();
}
