package za.ac.sun.cs.ingenious.games.loa;

import com.esotericsoftware.minlog.Log;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.Referee;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

public class LOAController extends Referee implements Runnable{
	private LOABoard gameBoard;
	private LOALogic logic;
	boolean gameOver;

	public LOAController(MatchSetting match, PlayerRepresentation[] players) {
		super(match, players);
		this.gameBoard = new LOABoard();
		this.logic = new LOALogic();
		this.gameOver = false;
	}
	
	/**
	 * Distributes this move to all players
	 */
	protected void distributeAcceptedMove( PlayedMoveMessage move) {
		for (int i = 0; i < players.length; i++) {
				players[i].playMove(move);
		}
	}

	@Override
	public void run() {
		Log.info("CONTROLLER CONNECTION CONCLUDED");
		Log.info("CONTROLLER SETUP CONCLUDED");

		while (!gameOver) {
			for (PlayerRepresentation engineTurn : players) {
				int playerId = engineTurn.getID();
				Log.info("CURRENT PLAYER NUMBER : " + playerId);

				
				PlayActionMessage data = engineTurn.genAction(new GenActionMessage());
				LOAAction generatedMove = data == null ? null : (LOAAction) data.getAction();

				if (generatedMove == null) {
					Log.info("generate move pass.");
					break;
				}

				if (this.logic.makeMove(gameBoard,generatedMove)) {
					distributeAcceptedMove(new PlayedMoveMessage(data));
					Log.info(gameBoard);
				} else {
					Log.info("Invalid move");
					gameOver = true;
					break;
				}
				
				Log.info(gameBoard);
				
				if (this.logic.isTerminal(gameBoard)){
					Log.info("GAME OVER");
					gameOver = true;
					break;
				}
			}
		}
	}
	
	@Override
	public void engineDisconnected(int id) {
		Log.info("Player " + id  + " disconnected");
		gameOver = true;
	}
	
	public LOAAction fromArray(String[] moveReply){
		Coord from = new Coord(Integer.parseInt(moveReply[1]), Integer.parseInt(moveReply[2]));
		Coord to = new Coord(Integer.parseInt(moveReply[3]), Integer.parseInt(moveReply[4]));
		return new LOAAction(from, to);
	}
}
