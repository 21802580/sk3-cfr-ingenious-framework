package za.ac.sun.cs.ingenious.search.mcts.legacy;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * This Updater is suitable for two player zero sum games with alternating moves.
 * Instead of two score values for the players, only the score of the player that
 * the playout was started for is passed to backupValue. A negamax updating
 * algorithm is then used to update the rewards in each SearchNode of the search TreeEngine.
 * This should be used together with {@link SingleValueUCT}.
 * @author steve
 */
public class NegamaxUpdater<N extends SearchNode<? extends GameState,N>> implements TreeUpdater<N> {

	@Override
	public void backupValue(N node, double[] normalizedResults) {
		N iterator = node;
		while (iterator != null) {
			iterator.addReward(normalizedResults);
			iterator.increaseVisits();
			normalizedResults[0] = -normalizedResults[0];
			iterator = iterator.getParent();
		}
	}

}
