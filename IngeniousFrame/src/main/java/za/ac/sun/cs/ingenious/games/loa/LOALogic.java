package za.ac.sun.cs.ingenious.games.loa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.games.loa.knowledge.HeuristicType;
import za.ac.sun.cs.ingenious.games.loa.movegeneration.MoveGenerator;
import za.ac.sun.cs.ingenious.games.loa.movegeneration.MoveValuePair;
import za.ac.sun.cs.ingenious.games.loa.util.Colour;
import za.ac.sun.cs.ingenious.games.loa.util.LOACoord;
import za.ac.sun.cs.ingenious.games.loa.util.LOAMove;

public class LOALogic implements TurnBasedGameLogic<LOABoard> {

	@Override
	public boolean validMove(LOABoard fromState, Move rawMove) {
		LOAAction action = (LOAAction) rawMove;
		LOACoord from = LOACoord.coords[action.getFrom().getY()][action.getFrom().getX()];
		LOACoord to = LOACoord.coords[action.getTo().getY()][action.getTo().getX()];
		boolean capture = fromState.pieceAt(to).equals(fromState.getPendingPlayer());
		LOAMove move = new LOAMove(from, to, capture);
		return fromState.moveLegal(move);
	}

	@Override
	public boolean makeMove(LOABoard fromState, Move rawMove) {
		LOAAction action = (LOAAction) rawMove;
		LOACoord from = LOACoord.coords[action.getFrom().getY()][action.getFrom().getX()];
		LOACoord to = LOACoord.coords[action.getTo().getY()][action.getTo().getX()];
		boolean capture = fromState.pieceAt(to).equals(fromState.getPendingPlayer());
		LOAMove move = new LOAMove(from, to, capture);
		if (validMove(fromState, rawMove)) {
			fromState.applyMove(move);
			return true;
		}
		return false;
	}

	@Override
	public void undoMove(LOABoard fromState, Move move) {
		LOAAction action = (LOAAction) move;
		LOACoord from = LOACoord.coords[action.getFrom().getY()][action.getFrom().getX()];
		LOACoord to = LOACoord.coords[action.getTo().getY()][action.getTo().getX()];
		boolean capture = fromState.pieceAt(to).equals(fromState.getPendingPlayer());
		LOAMove loaMove = new LOAMove(from, to, capture);
		fromState.undoMove(loaMove);
	}

	@Override
	public List<Action> generateActions(LOABoard fromState, int forPlayerID) {
		Colour player = Colour.fromInt(forPlayerID);
		// TODO: Pass HeuristicType/decisiveMoves from config to MoveGenerator
		List<MoveValuePair> moveValuePairs = MoveGenerator.generateMovesForPlayer(fromState, HeuristicType.NONE, false, player);
		List<Action> actions = new ArrayList<Action>(moveValuePairs.size());
		for (MoveValuePair mvp : moveValuePairs) {
			LOAMove move = mvp.getMove();
			Coord from = new Coord(move.getFrom().getCol(), move.getFrom().getRow());
			Coord to = new Coord(move.getTo().getCol(), move.getTo().getRow());
			actions.add(new LOAAction(from, to));
		}
		return actions;
	}

	@Override
	public boolean isTerminal(LOABoard state) {
		return state.getWinner() != null;
	}
}
