package za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalFeatures;
import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

import java.util.HashMap;
import java.util.Map;

/**
 * Used to initialize the DQN neural network in the Python application.
 *
 * @author Steffen Jacobs
 */
public class InitNeuralNetMessage extends ExternalMessage {
    private Map<String, Object> payload;

    public InitNeuralNetMessage(
            String playerIdentifier,
            String network,
            int channels,
            int height,
            int width,
            int sizeOut,
            int bufferMemory,
            int sampleSize,
            int sampleThreshold,
            int sampleFrequency,
            int targetNetworkUpdateFrequency,
            boolean doubleLearning,
            boolean prioritizedSampling,
            double bufferAlpha,
            double learningRate,
            double gamma
    ) {
        super(ExternalMessageType.INIT_NEURAL_NET);

        payload = new HashMap<>();
        payload.put("player_id", playerIdentifier);
        payload.put("network", network);
        payload.put("num_channels", channels);
        payload.put("height", height);
        payload.put("width", width);
        payload.put("action_count", sizeOut);
        payload.put("buffer_memory", bufferMemory);
        payload.put("sample_size", sampleSize);
        payload.put("gamma", gamma);
        payload.put("sample_threshold", sampleThreshold);
        payload.put("sample_freq", sampleFrequency);
        payload.put("double_dqn", doubleLearning);
        payload.put("prioritized", prioritizedSampling);
        payload.put("buffer_alpha", bufferAlpha);
        payload.put("learning_rate", learningRate);
        payload.put("target_update_freq", targetNetworkUpdateFrequency);
    }
}
