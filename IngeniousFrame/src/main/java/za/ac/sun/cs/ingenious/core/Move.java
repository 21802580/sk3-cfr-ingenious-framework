package za.ac.sun.cs.ingenious.core;

import java.io.Serializable;

import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;

/**
 * Represents one move in a game. Moves are equivalence classes of actions representing
 * how an action is observed by a player. The equivalence relations on actions may be
 * different per player, i.e: different players may observe different moves for the same
 * action.
 *
 * For example, consider the game of phantom TicTacToe. The first player that acts may
 * place their mark on any of the nine free spots and fully observes what they are doing.
 * Therefore, the first player's nine possible actions also correspond to nine different
 * moves that they can play and observe. The second player, however, does not see the
 * first player's action and therefore only observes the same move, regardless on where
 * the first player put their mark.
 *
 * After a player has send in their action using a {@link PlayActionMessage}, the server
 * sends {@link PlayedMoveMessage}s to all players to inform them about the result that
 * this action had. In perfect information games, the server will typically send the same
 * action that the player sent in. In imperfect information games, the server will
 * construct move-objects corresponding to what move the players observed.
 *
 * Game developers can create classes for moves that may occur in their game by
 * implementing this interface.
 */
public interface Move extends Serializable {
	/**
	 * @return ID of the player who made this move. -1 for actions executed by the environment.
	 */
	public int getPlayerID();
}
