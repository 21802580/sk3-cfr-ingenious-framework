package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeComposition;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallelInterface;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class MctsRaveNodeExtensionComposition<S extends GameState, C, P> implements MctsRaveNodeExtensionCompositionInterface {

    public String id = "Rave";

    private ArrayList<Action> possibleActions = new ArrayList<>();
    private Hashtable<Action, Integer> RaveActionVisitCount = new Hashtable<Action, Integer>();
    private Hashtable<Action, Integer> RaveActionWinCount = new Hashtable<Action, Integer>();
    private Hashtable<Action, MctsNodeTreeParallelInterface<?, ?, ?>> actionToNodeMapping = new Hashtable<>();

    /**
     * empty constructor
     */
    public MctsRaveNodeExtensionComposition() {
    }

    /**
     * set up to initialise the relevant lists with all possible actions relating to children. This
     * is used because reward values of the children are stored at the parent node.
     * @param node
     */
    public void setUp(MctsNodeComposition node) {
        List<Action> unexploredChildren = node.getUnexploredChildren();

        for (Action actionToAdd: unexploredChildren) {
            possibleActions.add(actionToAdd);
        }

        // Add each possible action from this game state to a hashtable with entries corresponding to the Rave wins/game plays for the TreeEngine under this node
        for (Action actionToAdd: unexploredChildren) {
            RaveActionVisitCount.put(actionToAdd, 0);
            RaveActionWinCount.put(actionToAdd, 0);
        }

    }

    /**
     * @return the id of the next player to play for the node object for which this extension relates
     */
    public String getID() {
        return id;
    }

    /**
     * stores the node relating to each action for the moves on path used during backpropagation to update
     * rave fields
     * @param action
     * @param node
     */
    public void storeActionToNodeMapping(Action action, MctsNodeTreeParallelInterface<?, ?, ?> node) {
        actionToNodeMapping.put(action, node);
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave implementation
     * @param child
     */
    public void addChild(C child) {
        MctsNodeTreeParallel childToRecord = (MctsNodeTreeParallel)child;
        storeActionToNodeMapping(childToRecord.getPrevAction(), childToRecord);
    }

    /**
     * Update the action to node mapping for use in the selection phase of the rave implementation
     * @param newChildren
     */
    public void addChildren(List<C> newChildren) {
        int size = newChildren.size();
        for (int i = 0; i < size; i++) {
            MctsNodeTreeParallel child = (MctsNodeTreeParallel)newChildren.get(i);
            storeActionToNodeMapping(child.getPrevAction(), child);
        }
    }

    /**
     * @return action to node mapping for use in the selection phase of the rave implementation
     */
    public Hashtable<Action, MctsNodeTreeParallelInterface<?, ?, ?>> getActionToNodeMapping() {
        return actionToNodeMapping;
    }

    /**
     * @param action
     * @return the number of winning playouts made through the child node relating to the action given as a parameter
     */
    public int getRaveWins(Action action) {
        return RaveActionWinCount.get(action);
    }

    /**
     * @param action
     * @return the number of playouts made through the child node relating to the action given as a parameter
     */
    public int getRaveVisits(Action action) {
        return RaveActionVisitCount.get(action);
    }

    /**
     * updates the rave win and visit count fields for children of the current node's
     * @param win
     * @param movesOnPath
     */
    public void updateRaveValues(boolean win, ArrayList<Action> movesOnPath) {
        for (int i = 0; i < possibleActions.size(); i++) {
            Action action = possibleActions.get(i);
            if (movesOnPath.contains(action)) {
                // increment rave visits
                int oldCount = RaveActionVisitCount.get(action);
                RaveActionVisitCount.replace(action, oldCount + 1);
                // increment rave win count
                if (win) {
                    int oldWinsCount = RaveActionWinCount.get(action);
                    RaveActionWinCount.replace(action, oldWinsCount + 1);
                }
            }
        }
    }

}
