package za.ac.sun.cs.ingenious.search.rl.qlearning;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.search.rl.util.ExternalLinearFunctionApproximation;
import za.ac.sun.cs.ingenious.search.rl.util.LinearFunctionApproximation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Implementation of Q-learning that uses linear function approximation. The linear function approximation can reside
 * either in Python using ExternalLinearFunctionApproximation, or in the framework as
 * InternalLinearFunctionApproximation. This class handles communicating action values to the Python application as well
 * as choosing exploration actions. Greedy actions are chosen by first requesting the action values from the Python
 * application.
 *
 * @author Steffen Jacobs
 */
public class LinearFunctionApproxQLearning extends QLearning {
    private GameLogic<GameState> logic;
    private LinearFunctionApproximation functionApproximation;
    private StateFeatureExtractor featureExtractor;
    private Set<Action> knownActions;

    public LinearFunctionApproxQLearning(GameLogic logic, StateFeatureExtractor featureExtractor) throws IOException {
        super("LinearFunctionApproxQLearning");
        
        this.logic = logic;
        functionApproximation = new ExternalLinearFunctionApproximation();
        this.featureExtractor = featureExtractor;

        knownActions = new HashSet<>();
    }

    public LinearFunctionApproximation getFunctionApproximation() {
        return functionApproximation;
    }

    public StateFeatureExtractor getFeatureExtractor() {
        return featureExtractor;
    }

    public Set<Action> getKnownActions() {
        return knownActions;
    }

    @Override
    public void update(GameState state, Action action, ScalarReward reward, GameState newState) {
        super.update(state, action, reward, newState);
//        Log.info("Reward: " + reward.getReward());

        Action[] actions = knownActions.toArray(new Action[0]);

        double[] actionValues = new double[actions.length];

        for (int i = 0; i < actions.length; i++) {
            double[] featureSet = featureExtractor.buildFeatures(newState, actions[i], logic);
            actionValues[i] = functionApproximation.get(new double[][] {featureSet})[0];
        }

        double maxValueForNewState = -Double.MAX_VALUE;

        if (!logic.isTerminal(newState)) {
            for (int j = 0; j < actions.length; j++) {
                if (actionValues[j] > maxValueForNewState) maxValueForNewState = actionValues[j];
            }
        } else {
            maxValueForNewState = 0;
        }


        double newValue = reward.getReward() + gamma * maxValueForNewState;
        double[] featureSetToUpdate = featureExtractor.buildFeatures(state, action, logic);

        if (enableTraining)
            functionApproximation.put(featureSetToUpdate, newValue);
    }

    @Override
    public Action greedyActionForState(GameState state, List<Action> availableActions) {
        Action[] actions = availableActions.toArray(new Action[0]);
        double[][] stateFeatureSets = new double[actions.length][];

        double[] stateValues = new double[actions.length];
        for (int i = 0; i < actions.length; i++) {
            double[] featureSet = featureExtractor.buildFeatures(state, actions[i], logic);
            stateValues[i] = functionApproximation.get(new double[][]{featureSet})[0];
        }

        List<Action> bestActions = new ArrayList<>();

        double maxValue = -Double.MAX_VALUE;
        int maxIndex = 0;
        for (int j = 0; j < actions.length; j++) {
            if (stateValues[j] > maxValue) {
                maxValue = stateValues[j];
                bestActions = new ArrayList<>();
                bestActions.add(actions[j]);
            } else if (stateValues[j] == maxValue) {
                bestActions.add(actions[j]);
            }
        }

        if (bestActions.size() == 1) {
            return bestActions.get(0);
        }

//        Log.info("Choose greedy action:");
//        for (int j = 0; j < actions.length; j++) {
//            Log.info(actions[j] + ": " + stateValues[j]);
//        }

        return bestActions.get(rng.nextInt(bestActions.size()));
    }

    @Override
    public Action chooseAction(GameState state, List<Action> availableActions) {
        knownActions.addAll(availableActions);
        return super.chooseAction(state, availableActions);
    }
}
