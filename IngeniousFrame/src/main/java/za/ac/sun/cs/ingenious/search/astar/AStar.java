package za.ac.sun.cs.ingenious.search.astar;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToDoubleBiFunction;
import java.util.function.ToDoubleFunction;

/**
 * A generic implementation of the classic A* search algorithm.
 * 
 * <p> This implementation utilises functional templating in order to be applicable to specific search problems.
 * There are four lambda functions that the user will have to provide, namely: {@code isGoal}, {@code getSucessors}, 
 * {@code cost}, and {@code heuristic}.
 * 
 * <p> The algorithm interacts directly with instances of an object called {@link SearchNode}.
 * These nodes hold information used by the search, but can be extended to hold additional information that may 
 * be required by the user provided lambda functions. The search returns the SearchNode that corresponds to the
 * goal state (as identified by {@code isGoal}), thus it is recommended that SearchNode be extended to hold a reference
 * to its predecessor, so that the optimal path can be extracted from the lineage of the returned node.
 * 
 * <p> The algorithm interacts with the open and closed sets through interfaces so that their underlying
 * implementations can be easily overriden. The algorithm creates instances of these sets through protected {@link Supplier}
 * lambdas. They can be overriden within the constructor or with an initialization block.
 * 
 * @param N Node that extends {@link SearchNode}
 * 
 * @author Nicholas Robinson
 */
public class AStar<N extends AStar.SearchNode<?>> {
    
    public static class SearchNode<P> {

        /**
         * A representation of state, i.e. the position within the search space.
         */
        P position;

        /**
         * cost to reach the current node, from the start node.
         */
        double g;

        /**
         * the sum of the g value and the heuristically estimated cost to reach the goal node from the current node.
         */
        double f;

        public SearchNode(P position) {
            this.position = position;
        }

        /**
         * copy constructor
         */
        SearchNode(SearchNode<P> s) {
            position = s.position;
            g = s.g;
            f = s.f;
        }

        /**
         * @return Object that represents state, i.e. the position in the search space.
         */
        public final P getPosition() {
            return position;
        }

        /**
         * @return cost to reach the current node from the start node.
         */
        public final double getG() {
            return g;
        }

        /**
         * @return estimated cost to reach the goal node from this current node.
         */
        public final double getH() {
            return f - g;
        }

        /**
         * @return sum of the {@code g} and {@code h} values.
         */
        public final double getF() {
            return f;
        }        
    }

    /* ------------------ LAMBDAS ------------------------------- */

    /**
     * Determine whether the given node satisfies the goal state.
     */
    public Predicate<N> isGoal;

    /**
     * Provide the successor nodes to this state.
     */
    public Function<N, Iterable<N>> getSuccessors;

    /**
     * Provide the edge weight cost between a given node and its successor.
     * (first argument = parent, second argument = child)
     */
    public ToDoubleBiFunction<N, N> cost;

    /**
     * Provide an estimated cost to reach the goal state from this node.
     */
    public ToDoubleFunction<N> heuristic;

    /* ---------- OPEN SET AND CLOSED SET INITIALIZATION --------- */

    /**
     * create the Open set.
     */
    protected Supplier<OpenSet<N>> createOpenSet = MappedOpenSet::new;

    /**
     * create the Closed set.
     */
    protected Supplier<ClosedSet> createClosedSet = MappedClosedSet::new;

    /**
     * Create an instances of the AStar class using the user defined lambda functions.
     * 
     * @param isGoal Determine whether the given node satisfies the goal state.
     * @param getSuccessors Provide the set of successor nodes to this state.
     * @param cost Provide the edge weight cost between a given node and its successor.
     * (first argument = parent, second argument = child)
     * @param heuristic Provide an estimated cost to reach the goal state from this node.
     */
    public AStar(Predicate<N> isGoal, Function<N, Iterable<N>> getSuccessors, ToDoubleBiFunction<N, N> cost, ToDoubleFunction<N> heuristic) {
        this.isGoal = isGoal;
        this.getSuccessors = getSuccessors;
        this.cost = cost;
        this.heuristic = heuristic;
    }

    /**
     * Uses the instance defined lambdas and performs the A* search from the given start node.
     * 
     * @param start node corresponding to the start state.
     * @return the first node that meets the end goal.
     */
    public N search(N start) {
        OpenSet<N> open = createOpenSet.get();
        ClosedSet closed = createClosedSet.get();

        open.add(start);
        
        while (!open.isEmpty()) {
            N q = open.poll();

            if (isGoal.test(q)) return q;
               
            Iterator<N> sitr = getSuccessors.apply(q).iterator();
            if (sitr != null) while (sitr.hasNext()) {
                N s = sitr.next();

                s.g = q.g + cost.applyAsDouble(q, s);
                s.f = s.g + heuristic.applyAsDouble(s);

                SearchNode<?> ccontender = closed.get(s.position);
                if (ccontender != null && ccontender.f <= s.f) continue;
                
                N ocontender = open.get(s.position);
                if (ocontender != null) {
                    if (ocontender.f <= s.f) continue;

                    open.replace(ocontender, s);
                }
                else {
                    open.add(s);
                } 
            }
            closed.offer(q);
        }
        return null;
    }

    public interface OpenSet<N extends SearchNode<?>> {
        
        public void add(N node);

        public N poll();

        public N get(Object position);

        public void replace(N oldNode, N newNode);

        public boolean isEmpty();
    }

    public interface ClosedSet {

        public SearchNode<?> get(Object position);

        public void offer(SearchNode<?> node);
    }

    /**
     * combines a hashmap and priority queue for fast get and poll operations
     */
    protected class MappedOpenSet implements OpenSet<N> {

        protected PriorityQueue<N> queue = new PriorityQueue<N>((a, b) -> a.f == b.f ? 0 : a.f > b.f ? 1 : -1);
        protected Map<Object, N> map = new HashMap<Object, N>();

		@Override
		public void add(N node) {
			queue.add(node);
            map.put(node.position, node);
		}

		@Override
		public N poll() {
			N q = queue.poll();
            map.remove(q.position, q);
			return q;
		}

		@Override
		public N get(Object position) {
			return map.get(position);
		        }

		@Override
		public void replace(N oldNode, N newNode) {
			queue.remove(oldNode);
            queue.add(newNode);
            map.replace(oldNode.position, oldNode, newNode);
        }
        
        @Override
		public boolean isEmpty() {
			return queue.isEmpty();
		}
    }

    /**
     * Uses a hashmap internally, only stores one node per position, keeps the node with the greatest f value.
     */
    protected class MappedClosedSet implements ClosedSet {

        protected Map<Object, SearchNode<?>> map = new HashMap<Object, SearchNode<?>>();

		@Override
		public SearchNode<?> get(Object position) {
			return map.get(position);
		}

		@Override
		public void offer(SearchNode<?> node) {
			map.merge(node.position, node, (a, b) -> a.f < b.f ? a : b);
		}
    }

    /**
     * Reinitializes the nodes that are placed in the closed set, since they may hold references to previous nodes (and other user defined fields)
     * which consume memory unecessarily 
     */
    protected class ReplacingMappedClosedSet extends MappedClosedSet {

		@Override
		public void offer(SearchNode<?> node) {
			map.merge(node.position, new SearchNode<Object>(node), (a, b) -> a.f < b.f ? a : b);
		}
    }
}
