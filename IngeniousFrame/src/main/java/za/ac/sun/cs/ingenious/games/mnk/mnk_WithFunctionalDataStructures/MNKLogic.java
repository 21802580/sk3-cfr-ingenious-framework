package za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures;

import java.util.Arrays;
import java.util.function.IntBinaryOperator;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;

/**
 * Game logic for the (m,n,k) game, specifically tailored to be compatible with functional data structures.
 * As such, board state is represented with a PVector, acting as a flattened array instead of a multi-dimensional array.
 * Therefore a set of coordinates {x: 0, y: 0} would correspond to position 0, and coordinates {x: m, y: n} would
 * correspond to m*n-1. For a given PVector board state, player tokens are represented by {@code Integer}s,
 * an empty position on the board is represented by {@code null}. A move consists of a position and a token.
 * 
 * @author Nicholas Robinson
 */
public class MNKLogic {
   
    /**
     * m = board width, n = board height, k = pieces in a row to win
     */
    public final int m, n, k;

    /**
     * The number of cells on the board, equal to m*n
     */
    public final int board_size;

    /**
     * create an instance of this logic class.
     * 
     * @param m board width.
     * @param n board height.
     * @param k pieces in a row needed to win.
     */
    public MNKLogic(int m, int n, int k) {
        this.m = m;
        this.n = n;
        this.k = k;

        board_size = m * n;
    }

    /**
     * Get the x-coordinate of a position on a board.
     * @param position a position on the board state.
     * @return x-coordinate of a position on a board.
     */
    public int getX(int position) {
        return position % m;
    }

    /**
     * Get the y-coordinate of a position on a board.
     * @param position a position on the board state.
     * @return y-coordinate of a position on a board.
     */
    public int getY(int position) {
        return position / n;
    }

    /**
     * Convert an xy-coordinate to its corresponding position on a board state.
     * 
     * @param x x-coordiante
     * @param y y-coordiante
     * @return position on board state
     */
    public int getPosition(int x, int y) {
        return x + y * m;
    }

    /**
     * count the number of position on the board with a value of {@code null}.
     */
    public int countNumberOfAvailablePositions(PVector<Integer> board)
    {
        int positions = 0;
        for (int i = 0; i < board_size; i++) {
            if (board.get(i) == null) {
                positions++;
            }
        }
        return positions;
    }

    /**
     * Determine whether a given move would result in a victory by scoring k tokens in a row.
     * 
     * @param board board state.
     * @param position position on board state that a token was placed.
     * @param token the token that was placed.
     * @return true if move results in a win.
     */
    public boolean assessVictoryMove(PVector<Integer> board, final int position, final int token)
    {
        /*
            To evaluate a line, start at position of move, X, tally number of player owned tokens in forward direction,
            X-->, until reaching a position that does not contain a player owned token. Then tally in reverse direction, 
            <--X. If the total number of tokens found is >= k then move results in a win.
        */ 

        IntBinaryOperator lineCast = (t, r) -> {
            int i = 0;
            for (int p = position; i < r; i++) {
                p += t;

                Integer item = board.get(p); 

                if (item == null || item != token) break;
            }
            return i;
        };

        int x = getX(position);
        int y = getY(position);
        
        int kBound = k - 1;

        int northBound = Math.min(y, kBound);
        int southBound = Math.min(n - y - 1, kBound);
        int eastBound = Math.min(m - x - 1, kBound);
        int westBound = Math.min(x, kBound);

        { // N_S
            int score = lineCast.applyAsInt(-m, northBound);

            if (score + southBound >= kBound) {
                score += lineCast.applyAsInt(m, southBound - score);
                if (score == kBound) return true;
            }
        }

        { // E_W
            int score = lineCast.applyAsInt(1, eastBound);

            if (score + southBound >= kBound) {
                score += lineCast.applyAsInt(-1, westBound - score);
                if (score == kBound) return true;
            }
        }

        { // NE_SW
            int northEastBound = Math.min(northBound, eastBound);
            int southWestBound = Math.min(southBound, westBound);

            int score = lineCast.applyAsInt(-m +1, northEastBound);

            if (score + southWestBound >= kBound) {
                score += lineCast.applyAsInt(m -1, southWestBound - score);
                if (score == kBound) return true;
            }
        }

        { // NW_SE
            int northWestBound = Math.min(northBound, westBound);
            int southEastBound = Math.min(southBound, eastBound);

            int score = lineCast.applyAsInt(-m -1, northWestBound);

            if (score + southEastBound >= kBound) {
                score += lineCast.applyAsInt(m +1, southEastBound - score);
                if (score == kBound) return true;
            }
        }

        return false;
    }

    /**
     * <p> An evaluation function used to assess the quality of a move.
     * 
     * <p> Here we define an "opportunity" as a line of k-length which contains 2 or more tokens that belong to the player and does not contain any tokens that
     * belong to their opponent.
     * 
     * <p> This function only considers the possible opportunities on a {@code board} that contain the specified move.
     * Point allocation is broken down into 3 categories:
     * 
     * <p><b>Discovering:</b> Where a move results in the creation of a new opportunity, awards 1 point.
     * <p><b>Reinforcing:</b> Where a move results in a token being added to an already existing opportunity, awards a point for each token already present.
     * <p><b>Blocking:</b> Where a move results in the destruction of an opponents opportunity, awards points equal to the total number of points that
     * the opponent would have gained throughout the formation and reinforcement of that opportunity.
     * 
	 * @param board mnk board state
     * @param position the position of the move played
     * @param token the token played
	 * @return positive integer value quantifying the quality of a move, or infinity if the move would position result in a win
     * 
	 */
    public int quantifyChangeInOpportunities(PVector<Integer> board, int position, int token) {

        final Integer[] sample = new Integer[Math.min(2 * k, Math.max(m, n)) - 1];
        
        int northBound = Math.min(getY(position), k - 1);
        int southBound = Math.min(n - getY(position) - 1, k - 1);
        int eastBound = Math.min(m - getX(position) - 1, k - 1);
        int westBound = Math.min(getX(position), k - 1);

        int merit = 0;

        for (int line = 0; line < 4; line++) {
            int translation = 0, reverseBound = 0, forwardBound = 0;
            switch (line) {
                case 0:
                {
                    // North to South
                    reverseBound = northBound;
                    forwardBound = southBound;
                    translation = m;
                    break;
                }
                case 1:
                {
                    // West to East
                    reverseBound = westBound;
                    forwardBound = eastBound;
                    translation = 1;
                    break;
                }
                case 2:
                {
                    // NorthWest to SouthEast
                    reverseBound = Math.min(northBound, westBound);
                    forwardBound = Math.min(southBound, eastBound);
                    translation = m + 1;
                    break;
                }
                case 3:
                {
                    // NorthEast to SouthWest
                    reverseBound = Math.min(northBound, eastBound);
                    forwardBound = Math.min(southBound, westBound);
                    translation = m - 1;
                    break;
                }
            }

            // obtain line sample

            // fill in reverse
            for (int i = reverseBound - 1, m = position; i >= 0; i--) {
                m -= translation;
                sample[i] = board.get(m);
            }

            // fill in forward
            for (int i = reverseBound, m = position; i < reverseBound + forwardBound; i++) {
                m += translation;
                sample[i] = board.get(m);
            }
            
            // slide a window across the line sample
            for (int s = 0; s < reverseBound + forwardBound + 1 - (k - 1); s++) {
                int friends = 0;
                int foes = 0;

                // count tokens
                for (int w = s; w < s + k - 1; w++) {
                    if (sample[w] == null) {
                        continue;
                    }
                    else if (sample[w] == token) {
                        friends++;
                    }
                    else {
                        foes++;
                    }
                }
                 
                if (friends == k - 1) {
                    // detected a win
                    return Integer.MAX_VALUE;
                }
                else if (friends >= 1 && foes == 0) {
                    // discover or reinforce
                    merit += friends;
                }
                else if (friends == 0 && foes >= 2) {
                    // nothing weird here, its Gauss
                    merit += (foes * (foes - 1)) / 2;
                }
            }
        }
        return merit;
    }

    /**
     * Return an array of the positions on the board that are empty
     */
    public int[] availableMoves(PVector<Integer> board)
    {
        int[] positions = new int[board_size];
        int c = 0;

        int p = 0;
        for (Integer token : board.range(0, board_size - 1)) {
            if (token == null) {
                positions[c++] = p;
            }
            p++;
        }

        return Arrays.copyOf(positions, c);
    }

    /**
     * Stringifies an (m,n,k) board state.
     */
    public String stringifyBoard(PVector<Integer> board) {
        StringBuilder sb = new StringBuilder();

        int i = 0;
        for (int y = 0; y < n; y++) {
            for (int x = 0; x < m; x++) {
                sb.append('[');
                Integer token = board.get(i++);
                sb.append(token == null ? ' ' : token);
                sb.append(']');
            }
            sb.append('\n');
        }

        return sb.toString();
    }
}