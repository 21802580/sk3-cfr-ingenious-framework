package za.ac.sun.cs.ingenious.core.network.external.messages.linearFunctionApproximation;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to receive approximated action values for linear features using linear function approximation.
 *
 * @author Steffen Jacobs
 */
public class LinearFeatureValuesMessage extends ExternalMessage {
    double[] payload;

    public LinearFeatureValuesMessage(double[] payload) {
        super(ExternalMessageType.LINEAR_FEATURE_VALUES);

        this.payload = payload;
    }

    public double[] getPayload() {
        return payload;
    }

    public void setPayload(double[] payload) {
        this.payload = payload;
    }
}
