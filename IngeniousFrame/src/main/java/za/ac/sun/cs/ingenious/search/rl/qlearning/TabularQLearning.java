package za.ac.sun.cs.ingenious.search.rl.qlearning;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Class containing an implementation of off-policy tabular Q-learning.
 *
 * @author Steffen Jacobs
 */
public class TabularQLearning extends QLearning implements Serializable {
    protected static final double DEFAULT_ALPHA_DOUBLE_LEARNING = 0.02; // Double learning

    /**
     * Table containing estimated expected reward Q(S, A).
     */
    private Map<GameState, Map<Action, Double>> qTable;

    /**
     * Second Q table reserved for double learning enhancement.
     */
    private Map<GameState, Map<Action, Double>> qTable2;

    /**
     * Indicates whether this instance uses the double learning enhancement.
     */
    private boolean useDoubleLearning;

    public TabularQLearning() {
        super("TabularQLearning");

        qTable = new HashMap<>();
        qTable2 = new HashMap<>();
        useDoubleLearning = false;
    }

    public TabularQLearning(
            String identifier,
            boolean enableTraining,
            double alpha,
            double epsilon,
            double epsilonStep,
            double epsilonMin,
            double gamma,
            boolean useDoubleLearning
    ) {
        super(identifier, enableTraining, alpha, epsilon, epsilonStep, epsilonMin, gamma);

        qTable = new HashMap<>();
        qTable2 = new HashMap<>();
        this.useDoubleLearning = useDoubleLearning;
    }

    public TabularQLearning withQTable(Map<GameState, Map<Action, Double>> qTable) {
        this.qTable = qTable;

        return this;
    }

    public TabularQLearning withQTable2(Map<GameState, Map<Action, Double>> qTable2) {
        this.qTable2 = qTable2;

        return this;
    }

    /**
     * Loads and returns a serialized instance of TabularQLearning from the specified file.
     *
     * @param fileName File containing serialized TabularQLearning object.
     * @return TabularQLearning instance contained in the file.
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static TabularQLearning fromFile(String fileName) throws IOException, ClassNotFoundException {
        try (FileInputStream fis = new FileInputStream(fileName)) {
            ObjectInputStream oos = new ObjectInputStream(fis);

            return (TabularQLearning) oos.readObject();
        }
    }

    public TabularQLearning withDoubleLearning() {
        this.qTable2 = new HashMap<>();
        this.useDoubleLearning = true;

        if (alpha == DEFAULT_ALPHA) {
            alpha = DEFAULT_ALPHA_DOUBLE_LEARNING;
        }

        return this;
    }

    public Map<GameState, Map<Action, Double>> getQTable() {
        return qTable;
    }

    public Map<GameState, Map<Action, Double>> getQTable2() {
        return qTable2;
    }

    public boolean isUsingDoubleLearning() {
        return useDoubleLearning;
    }

    /**
     * Updates the Q table entry Q(state, action), using the reward received by moving from state to newState.
     *
     * @param state Previous state.
     * @param action Action taken at previous state.
     * @param reward Reward received by transitioning to newState.
     * @param newState Resulting state after taking action at state.
     */
    @Override
    public void update(GameState state, Action action, ScalarReward reward, GameState newState) {
        super.update(state, action, reward, newState);

        if (useDoubleLearning) {
            if (!qTable.containsKey(state)) qTable.put(state, new HashMap<>());
            if (!qTable.containsKey(newState)) qTable.put(newState, new HashMap<>());
            if (!qTable2.containsKey(state)) qTable2.put(state, new HashMap<>());
            if (!qTable2.containsKey(newState)) qTable2.put(newState, new HashMap<>());

            if (rng.nextDouble() < 0.5) {
                updateDoubleLearning(state, action, reward, newState, qTable, qTable2);
            } else {
                updateDoubleLearning(state, action, reward, newState, qTable2, qTable);
            }

            return;
        }

        if (!qTable.containsKey(state)) qTable.put(state, new HashMap<>());
        if (!qTable.containsKey(newState)) qTable.put(newState, new HashMap<>());

        Map<Action, Double> qs = qTable.get(state);
        Map<Action, Double> qsDash = qTable.get(newState);

        double maxQForNewState = -Double.MAX_VALUE;
        for (Action a : qsDash.keySet()) {
            if (!qsDash.containsKey(a)) qsDash.put(a, DEFAULT_Q_VALUE);
            double q = qsDash.get(a);
            if (q > maxQForNewState) maxQForNewState = q;
        }

        // Ensure that if table entries have been initialized for Q(S',maxA), the default value is used.
        if (qsDash.keySet().size() == 0) {
            maxQForNewState = DEFAULT_Q_VALUE;
        }

        if (!qs.containsKey(action)) qs.put(action, DEFAULT_Q_VALUE);
        double newValue = qs.get(action) + alpha * (reward.getReward() + gamma * maxQForNewState - qs.get(action));

        if (enableTraining)
            qs.put(action, newValue);
    }

    private void updateDoubleLearning(GameState state, Action action, ScalarReward reward, GameState newState, Map<GameState, Map<Action, Double>> q1, Map<GameState, Map<Action, Double>> q2) {
        Map<Action, Double> q1s = q1.get(state);
        Map<Action, Double> q2s = q2.get(state);
        Map<Action, Double> q1sDash = q1.get(newState);
        Map<Action, Double> q2sDash = q2.get(newState);

        double maxQForNewState = -Double.MAX_VALUE;
        Action maxActionForNewState = null;

        for (Action a : q1sDash.keySet()) { // Get the max action from Q1(S',*)
            double q = q1sDash.get(a);
            if (q > maxQForNewState) {
                maxQForNewState = q;
                maxActionForNewState = a;
            }
        }

        if (!q1s.containsKey(action)) q1s.put(action, DEFAULT_Q_VALUE);
        if (maxActionForNewState != null && !q2sDash.containsKey(maxActionForNewState)) q2sDash.put(maxActionForNewState, DEFAULT_Q_VALUE);

        double q2sDashA = maxActionForNewState != null ? q2sDash.get(maxActionForNewState) : DEFAULT_Q_VALUE;
        double q1sa = q1s.get(action);
        double rewardValue = reward.getReward();

        double newValue = q1sa + alpha * (rewardValue + gamma * q2sDashA - q1sa);

        if (enableTraining)
            q1s.put(action, newValue);
    }

    /**
     * Greedily chooses the best action from the available actions with the greatest value in the Q table corresponding
     * to Q(state, *).
     *
     * @param state State from which the action is taken.
     * @param availableActions Available actions for the state.
     * @return The greedy action choice.
     */
    @Override
    public Action greedyActionForState(GameState state, List<Action> availableActions) {
        // If this table row does not exist, initialize it.
        if (!qTable.containsKey(state)) qTable.put(state, new HashMap<>());

        if (useDoubleLearning && !qTable2.containsKey(state)) qTable2.put(state, new HashMap<>());

        double bestQ = -Double.MAX_VALUE;
        List<Action> bestActions = new ArrayList<>();

        Map<Action, Double> qs = qTable.get(state);

        Map<Action, Double> q2s = null;
        if (useDoubleLearning) q2s = qTable2.get(state);

        // Iterate over actions and get the actions with the best value.
        for (Action a : availableActions) {
            if (!qs.containsKey(a)) qs.put(a, DEFAULT_Q_VALUE);
            if (useDoubleLearning && !q2s.containsKey(a)) q2s.put(a, DEFAULT_Q_VALUE);

            double q = qs.get(a);

            if (useDoubleLearning) q += q2s.get(a);

            if (q > bestQ) {
                bestQ = q;
                bestActions = new ArrayList<>();
                bestActions.add(a);
            } else if (q == bestQ) {
                bestActions.add(a);
            }
        }

        // If one action has the best value, it is chosen.
        if (bestActions.size() == 1) {
            return bestActions.get(0);
        }

        // If multiple actions are tied, choose randomly between them.
        return bestActions.get(rng.nextInt(bestActions.size()));
    }

    public void save(String fileName) {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void printQTable() {
        Log.info("Q Table: \n");
        Set<GameState> keySet = qTable.keySet();

        Log.info("==========================");
        for (GameState state : keySet) {
            Set<Action> actionSet = qTable.get(state).keySet();

            if (actionSet.size() == 0) continue;

            Log.info(state + "\t");

            for (Action action : actionSet) {
                Log.info("\t" + action + " " + qTable.get(state).get(action) + "\t");
            }
        }
        Log.info("==========================");
    }
}
