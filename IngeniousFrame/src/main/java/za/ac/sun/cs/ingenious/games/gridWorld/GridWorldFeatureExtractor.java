package za.ac.sun.cs.ingenious.games.gridWorld;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

import java.util.Set;

/**
 * Feature extractor implementation for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldFeatureExtractor implements StateFeatureExtractor<GridWorldState, GridWorldLogic> {
    @Override
    public double[] buildFeatures(GridWorldState state, GridWorldLogic logic) {
        int width = state.getWidth();
        int height = state.getHeight();

        double[] featureSet = new double[width * height];

        Coord playerCoord = state.findPlayerCoords(GridWorldState.DEFAULT_FIRST_PLAYER);
        featureSet[playerCoord.getY() * width + playerCoord.getX()] = 1;
        return featureSet;
    }

    @Override
    public double[] buildFeatures(GridWorldState state, Action action, GridWorldLogic logic) {
        GridWorldState clonedState = state.deepCopy();

        if (!logic.isTerminal(state)) {
            logic.makeMove(clonedState, action);
        }

        double[] featuresForAction = buildFeatures(clonedState, logic);
        return featuresForAction;
    }

    @Override
    public double[][][] buildTensor(GridWorldState state, GridWorldLogic logic) {
        return new double[][][]{
                buildAgentLocationChannel(state, logic),
                buildObstacleChannel(state, logic),
                buildObjectiveChannel(state, logic),
                buildPenaltyChannel(state, logic),
        };
    }

    /**
     * Constructs a one-hot encoded grid that indicates the position of the agent.
     *
     * @param state
     * @param logic
     * @return
     */
    private double[][] buildAgentLocationChannel(GridWorldState state, GridWorldLogic logic) {
        int width = state.getWidth();
        int height = state.getHeight();

        double[][] agentLocationChannel = new double[height][width];

        Coord playerCoord = state.findPlayerCoords(GridWorldState.DEFAULT_FIRST_PLAYER);
        agentLocationChannel[playerCoord.getY()][playerCoord.getX()] = 1;

        return agentLocationChannel;
    }

    /**
     * Constructs a one-hot encoded grid that indicates the positions of obstacles.
     *
     * @param state
     * @param logic
     * @return
     */
    private double[][] buildObstacleChannel(GridWorldState state, GridWorldLogic logic) {
        int width = state.getWidth();
        int height = state.getHeight();

        double[][] obstacleChannel = new double[height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (state.board[i][j] == GridWorldState.CELL_UNREACHABLE) {
                    obstacleChannel[i][j] = 1;
                }
            }
        }

        return obstacleChannel;
    }

    /**
     * Constructs a one-hot encoded grid that indicates the positions of objectives.
     *
     * @param state
     * @param logic
     * @return
     */
    private double[][] buildObjectiveChannel(GridWorldState state, GridWorldLogic logic) {
        int width = state.getWidth();
        int height = state.getHeight();

        double[][] objectiveChannel = new double[height][width];


        Set<Coord> terminalObjectives = logic.getTerminalObjectives();

        for (Coord objective : terminalObjectives) {
            objectiveChannel[objective.getY()][objective.getX()] = 1;
        }

        return objectiveChannel;
    }

    /**
     * Constructs a one-hot encoded grid that indicates the positions of penalties.
     *
     * @param state
     * @param logic
     * @return
     */
    private double[][] buildPenaltyChannel(GridWorldState state, GridWorldLogic logic) {
        int width = state.getWidth();
        int height = state.getHeight();

        double[][] penaltyChannel = new double[height][width];


        Set<Coord> terminalPentalties = logic.getTerminalPenalties();

        for (Coord penalty : terminalPentalties) {
            penaltyChannel[penalty.getY()][penalty.getX()] = 1;
        }

        return penaltyChannel;
    }
}
