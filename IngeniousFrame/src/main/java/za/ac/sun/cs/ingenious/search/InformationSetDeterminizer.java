package za.ac.sun.cs.ingenious.search;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * A class implementing this interface can produce a determinization of information sets
 * represented by some GameState
 * @author Michael Krause
 * @param <S> GameState this Determinizer works with
 */
public interface InformationSetDeterminizer<S extends GameState> {
	/**
	 * @param forState The information set to find a determinization for
	 * @param forPlayerID The player whos view the information set given in forState represents
	 * @return A determinization of forState, i.e: A new object of type S that contains all the
	 *         information already present in forState and additionally assigns a concrete
	 *         value to all partially or completely unknown information in forState.
	 */
	public S determinizeUnknownInformation(S forState, int forPlayerID);
	
	/**
	 * @param state Some state (can be an information set or a determinized state obtained)
	 * @param forPlayerID ID of a player who observes state
	 * @return The information set that forPlayerID observes for the given state
	 */
	public S observeState(S state, int forPlayerID);
}
