package za.ac.sun.cs.ingenious.core.network.game;

import com.esotericsoftware.minlog.Log;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * The socket wrapper basically is a wrapper for a pair of ObjectStreams to restrict
 * receiving and sending of objects to {@link Message}s. Furthermore it has some error handling.
 */
public abstract class SocketWrapper {

	protected ObjectOutputStream outputStream;
	protected ObjectInputStream inputStream;
	private Socket client;
	private boolean isAlive;

	public SocketWrapper(Socket client, ObjectInputStream is, ObjectOutputStream os){
		this.client = client;
		this.outputStream = os;
		this.inputStream = is;
		isAlive = true;
	}

	/**
	 * @return The received message or null if the transmission was unsuccessful
	 */
	public Message receiveMessage(){
		try {
			Object input = inputStream.readObject();
			if (input instanceof Message) {
				return (Message) input;
			} else {
				handleUnknownTransmission(input);
			}
		} catch (ClassNotFoundException e) {
			handleUnknownTransmission(null);
		} catch (IOException e) {
			handleIOException(e);
		}
		return null;
	}

	/**
	 * @param a Message to be sent
	 */
	public void sendMessage(Message a){
		try {
			outputStream.writeObject(a);
		} catch (IOException e) {
			handleIOException(e);
		}
	}

	/**
	 * @return True if the Socket is still active and did not go dead during a transmission
	 */
	public boolean isAlive() {
		return isAlive;
	}

	/**
	 * Disconnects IO-streams and closes socket catches and prints failures
	 */
	public void closeConnection() {
		try {
			inputStream.close();
			outputStream.flush();
			outputStream.close();
			client.close();
			isAlive = false;
		} catch (IOException e) {
			Log.error("SocketWrapper","Socket / streams failed to close");
		}
	}

	protected void handleUnknownTransmission(Object input) {
		Log.warn("Received object of an unknown type from server. Will attempt to continue.");
	}

	protected void handleIOException(IOException e) {
		isAlive = false;
		if (e instanceof EOFException) {
			Log.warn("SocketWrapper", "Socket was disconnected!");
		}else{
			e.printStackTrace();
			Log.error("SocketWrapper", "IOException during transmission, will try to close connection");
		}
		closeConnection(); // At least try to close the connection gracefully
	}
}
