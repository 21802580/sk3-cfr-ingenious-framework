package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;

/**
 * This class is used for Actions that represent movement by a player in a
 * specific {@link CompassDirection}.
 */
public class CompassDirectionAction implements Action {

	private static final long serialVersionUID = 1L;

	private int playerID;
	private CompassDirection dir;

	public CompassDirectionAction(int playerID, CompassDirection dir) {
		this.playerID = playerID;
		this.dir = dir;
	}

	public CompassDirection getDir() {
		return dir;
	}

	@Override
	public int getPlayerID() {
		return playerID;
	}

	@Override
	public String toString(){
		return "Player " + playerID + " CompassDirectionAction, dir " + dir;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass())) {
			return false;
		} else {
			return (this.playerID == ((CompassDirectionAction) obj).playerID)
					&& (this.dir.equals(((CompassDirectionAction) obj).dir));
		}
	}

	@Override
	public int hashCode() {
		return playerID + 31 * dir.ordinal();
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}

