package za.ac.sun.cs.ingenious.games.mnk;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;

public class MNKFeatureExtractor implements StateFeatureExtractor<MNKState, MNKLogic> {
    private int playerID;

    public MNKFeatureExtractor(int playerID) {
        this.playerID = playerID;
    }

    @Override
    public double[][][] buildTensor(MNKState state, MNKLogic logic) {

        MNKState observedState = logic.observeState(state, playerID);

        int numberOfPlayers = observedState.getNumPlayers();
        int height = observedState.getHeight();
        int width = observedState.getWidth();

        int[][] board = observedState.playerBoards.get(0).board;

        double[][][] features = new double[numberOfPlayers + 1][height][width];

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (board[i][j] > 0) {
                    // If player occupies position, set flag on the channel that corresponds to the player ID.
                    int playerID = board[i][j] - 1;
                    features[playerID][i][j] = 1;
                } else {
                    // If no player occupies the position, set flag on the channel that represents no player.
                    features[numberOfPlayers][i][j] = 1;
                }
            }
        }

        return features;
    }
}
