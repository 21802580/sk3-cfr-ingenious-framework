package za.ac.sun.cs.ingenious.core.network.external.messages.table;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;

/**
 * Used to receive action values from the table in the Python application.
 *
 * @author Steffen Jacobs
 */
public class TableStateActionValueMessage extends ExternalMessage {
    private double payload;

    public TableStateActionValueMessage(double payload) {
        super(ExternalMessageType.TABLE_STATE_ACTION_VALUE);

        this.payload = payload;
    }

    public double getPayload() {
        return payload;
    }

    public void setPayload(double payload) {
        this.payload = payload;
    }
}
