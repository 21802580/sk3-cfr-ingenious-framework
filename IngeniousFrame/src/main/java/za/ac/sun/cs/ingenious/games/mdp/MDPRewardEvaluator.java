package za.ac.sun.cs.ingenious.games.mdp;

import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Reward evaluator for the MDP game. Rewards are evaluated based on definitions specified in the MDP setting JSON file.
 *
 * @author Steffen Jacobs
 */
public class MDPRewardEvaluator<T> implements GameRewardEvaluator<MDPState<T>> {
    private Map<MDPState<T>, Map<MDPState<T>, Double>> r;

    public MDPRewardEvaluator(Map<MDPState<T>, Map<MDPState<T>, Double>> r) {
        this.r = r;
    }

    public static MDPRewardEvaluator<Long> fromSetting(MDPSetting mdpSetting) {
        return new MDPRewardEvaluator<>(createRewardsFromSetting(mdpSetting));
    }

    /**
     * Initialize reward lookups for (S, S') pairs in a nested map using the provided MDP settings file.
     *
     * @param mdpSetting Settings defining the MDP being created.
     */
    private static Map<MDPState<Long>, Map<MDPState<Long>, Double>> createRewardsFromSetting(MDPSetting mdpSetting) {
        Map<MDPState<Long>, Map<MDPState<Long>, Double>> r = new HashMap<>();

        // Rewards collection representing R(S, S').
        Collection<Collection<Double>> rewards = mdpSetting.getReward();

        long i = 0;
        // For each state, look at R(S = i, S').
        for (Collection<Double> rewardForNewState : rewards) {
            Map<MDPState<Long>, Double> rewardForNewStateMap = new HashMap<>();

            long j = 0;
            // For each new state, look at R(S = i, S' = k).
            for (double rewardValue : rewardForNewState) {
                rewardForNewStateMap.put(new MDPState<>(j++), rewardValue);
            }

            r.put(new MDPState<>(i++), rewardForNewStateMap);
        }

        return r;
    }

    @Override
    public double[] getReward(MDPState<T> state) {
        MDPState<T> previousState = new MDPState<T>(state.getPreviousGameState());
        double rewardForState = r.get(previousState).get(state);

        return new double[] { rewardForState };
    }
}
