package za.ac.sun.cs.ingenious.search.sss;

import za.ac.sun.cs.ingenious.search.sss.OpenQueue.BucketStackRelation;

/**
 * Encodes the state of the SSS* tree traversal.
 * 
 * <p> Contains 3 fields: 
 * <ul>
 *  <li> {@code node}: A reference to a node in search-tree.
 *  <li> {@code status}: An indication of whether the subtree rooted at node is evaluated.
 *  <li> {@code merit}: An upper bound on the true value of the node.
 * 
 * <p> In addition, this object itself also acts as the linked-list nodes in {@link OpenQueue}'s buckets
 * 
 * @author Nicholas Robinson
 */
class Descriptor implements BucketStackRelation {
    SSS.SearchNode<?> node;
    Status status;
    int merit;

    // for use in OpenQueue buckets
    BucketStackRelation next;
    Descriptor previous;

    /**
     * An indication of whether the subtree rooted at node is evaluated.
     */
    enum Status {
        /**
         * Has been evaluated
         */
        LIVE, 

        /**
         * Has not been evaluated
         */
        SOLVED
    }

    Descriptor(SSS.SearchNode<?> node, Status status, int merit) {
        this.node = node;
        this.status = status;
        this.merit = merit;
    }

    /**
     * Reassign a Descriptor Object that has been polled from the OpenQueue.
     * 
     * <p> Acts as a constructor.
     */
    Descriptor reuse(SSS.SearchNode<?> node, Status status, int merit) {
        this.node = node;
        this.status = status;
        this.merit = merit;

        return this;
    }

    /**
     * set this nodes reference to its previous neighbour (In OpenQueue buckets)
     */
    @Override
    public void setPrevious(Descriptor previous) {
        this.previous = previous;
    }

}