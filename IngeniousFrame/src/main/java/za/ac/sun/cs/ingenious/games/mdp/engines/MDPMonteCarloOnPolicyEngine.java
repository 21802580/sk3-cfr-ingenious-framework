package za.ac.sun.cs.ingenious.games.mdp.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.mdp.MDPEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.monteCarlo.TabularMonteCarloOnPolicy;

/**
 * Monte Carlo (on-policy) engine for MDP.
 *
 * @author Steffen Jacobs
 */
public class MDPMonteCarloOnPolicyEngine extends MDPEngine {
    TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg;

    public MDPMonteCarloOnPolicyEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new TabularMonteCarloOnPolicy<>();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "MDPMonteCarloOnPolicyEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);
        Log.info("RECV INIT GAME MSG: RESETTING ALGORITHM");

        alg.setLogic(logic);
        alg.setPlayerID(playerID);
        alg.beginNewEpisode();
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = alg.chooseActionFromPolicy(state);
        Log.info("RECV REQUEST ACTION MSG: Choosing action " + choice.toString());
        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        alg.setMostRecentReward(a.getReward());
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        alg.update();
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        alg.printMetrics();
    }
}
