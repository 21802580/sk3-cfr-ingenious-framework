package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldFinalEvaluator;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldRewardEvaluator;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.dp.PolicyIteration;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * Policy Iteration engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldPolicyIterationEngine extends GridWorldEngine {
    /**
     * Policy iteration algorithm used to solve the MDP.
     */
    private PolicyIteration<GridWorldState, MDPLogic<GridWorldState>, GridWorldRewardEvaluator> alg;

    private MDPState<GridWorldState> mdpState;

    public GridWorldPolicyIterationEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new PolicyIteration<>();
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldPolicyIterationEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        MDPLogic<GridWorldState> mdpLogic = MDPLogic.fromGameLogic(state.deepCopy(), logic);

        // Since this can't be set at time of algorithm initialization, doing it here. These are important.
        alg.setStates(mdpLogic.getStates());
        alg.setLogic(mdpLogic);
        alg.setEvaluator(new GridWorldRewardEvaluator(parser.getRewards(), parser.getStepReward()));
        alg.initializeValueTable();
        alg.initializePolicy();
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(state, a.getMove());

    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        alg.solve();

        Action choice = alg.chooseActionForState(new MDPState<>(state));

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        Log.info("Solved policy:");

        Map<MDPState<GridWorldState>, Action> policy = alg.getPolicy();

        // Quick and dirty map from Map<MDPState, Action> to Map<GridWorldState, Action>.
        Map<GameState, Action> mappedPolicy = policy.keySet()
                .stream()
                .collect(Collectors.toMap(MDPState<GridWorldState>::getGameState, policy::get));

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicy(state, mappedPolicy));

        Map<MDPState<GridWorldState>, Double> valueFunction = alg.getValueTable();

        // Quick and dirty map from Map<MDPState, Double> to Map<GameState, Double>.
        Map<GameState, Double> mappedValueFunction = valueFunction.keySet()
                .stream()
                .collect(Collectors.toMap(MDPState<GridWorldState>::getGameState, valueFunction::get));

        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicy(state, mappedValueFunction));
    }

    public PolicyIteration<GridWorldState, MDPLogic<GridWorldState>, GridWorldRewardEvaluator> getAlg() {
        return alg;
    }
}
