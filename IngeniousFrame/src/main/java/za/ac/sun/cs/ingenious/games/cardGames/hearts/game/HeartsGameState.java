package za.ac.sun.cs.ingenious.games.cardGames.hearts.game;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.EnumSet;

import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSuits;
import za.ac.sun.cs.ingenious.games.cardGames.classicalDeck.ClassicalSymbols;
import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;
import za.ac.sun.cs.ingenious.games.cardGames.core.cardGameState.CardGameState;
import za.ac.sun.cs.ingenious.games.cardGames.core.deck.CardDeck;
import za.ac.sun.cs.ingenious.games.cardGames.core.rack.CardRack;

/**
 * 
 * The game state for Hearts.
 * 
 * 
 * @author Joshua Wiebe
 */
public class HeartsGameState extends CardGameState<ClassicalSymbols, ClassicalSuits, HeartsLocations>{
	
	private boolean gameStarted;
	
	private int roundsThatWillBePlayed;

	/** Indicates if heart is already broken. */
	private boolean heartIsPlayed;
	
	private ClassicalSuits currentTrickSuit;
	
	private ClassicalSuits trickSuitOfLastRound;
	
	private ArrayList<Card<ClassicalSymbols, ClassicalSuits>> currentTrick;
	
	/** Gathers all tricks a players collect in the course of the game. */
	private CardRack<ClassicalSymbols, ClassicalSuits>[] playersTrickPiles;
	
	private int beginningPlayer;
	
	private int winnerOfLastRound;
	
	private int roundNR;
	
	/** Gathered tricks of players */
	private int[] nrTricksOfPlayers;
	
	private Card<ClassicalSymbols, ClassicalSuits> startingCard;
	
	public HeartsGameState(int numPlayers) {
		super(createAppropriateDeck(numPlayers), HeartsLocations.DRAWPILE, numPlayers);
		// Default
		gameStarted = false;
		heartIsPlayed = false;
		currentTrickSuit = null;
		currentTrick = new ArrayList<>();
		playersTrickPiles = new CardRack[numPlayers];
		roundsThatWillBePlayed = deck.getSize() / numPlayers;
		for(int i=0; i<playersTrickPiles.length; i++){
			playersTrickPiles[i] = new CardRack<ClassicalSymbols, ClassicalSuits>();
		}
		beginningPlayer = -1;
		winnerOfLastRound = -1;
		roundNR = 1;
		// Default starting card: two of clubs. Otherwise call other constructor.
		this.startingCard = setStartingCard(numPlayers);
		nrTricksOfPlayers = new int[numPlayers];
	}
	
	// ===================
	// = Getter & setter =
	// ===================
	
	// Players trick pile.
	public CardRack<ClassicalSymbols, ClassicalSuits>[] getPlayersTrickPiles() {
		return playersTrickPiles;
	}

	public void addCardToPlayersTrickPile(Card<ClassicalSymbols, ClassicalSuits> card, int player) {
		if(player<0 || player>getNumPlayers()){
			throw new IllegalArgumentException("Please provide a valid player id (0- " + getNumPlayers() + "): " + player + " is invalid!");
		}
		playersTrickPiles[player].addCard(card);
	}

	// heartIsPlayed
	public boolean isHeartIsPlayed() {
		return heartIsPlayed;
	}

	public void setHeartIsPlayed(boolean heartIsPlayed) {
		this.heartIsPlayed = heartIsPlayed;
	}

	// currentTrickSuit
	public ClassicalSuits getCurrentTrickSuit() {
		return currentTrickSuit;
	}

	public void setCurrentTrickSuit(ClassicalSuits currentTrickSuit) {
		this.currentTrickSuit = currentTrickSuit;
	}

	// currentTrick
	public ArrayList<Card<ClassicalSymbols, ClassicalSuits>> getCurrentTrick() {
		return currentTrick;
	}

	public void addCardToCurrentTrick(Card<ClassicalSymbols, ClassicalSuits> card) {
		this.currentTrick.add(card);
	}
	
	public void resetCurrentTrick(){
		currentTrick.clear();
	}

	// beginningPlayer
	public int getBeginningPlayer() {
		return beginningPlayer;
	}

	public void setBeginningPlayer(int beginningPlayer) {
		this.beginningPlayer = beginningPlayer;
	}
	
	// winnerOfLastRound
	public int getWinnerOfLastRound() {
		return winnerOfLastRound;
	}
	
	public void setWinnerOfLastRound(int winnerOfLastRound) {
		this.winnerOfLastRound = winnerOfLastRound;
	}

	// roundNR
	public int getRoundNR() {
		return roundNR;
	}

	public void incrementRoundNR() {
		roundNR++;
	}
	
	// startingCard
	public void setStartingCard(Card<ClassicalSymbols, ClassicalSuits> startingCard) {
		this.startingCard = startingCard;
	}
	
	public Card<ClassicalSymbols, ClassicalSuits> getStartingCard() {
		return startingCard;
	}
	
	// nrTricksOfPlayers
	public void incrementNrTricksOfPlayers(int player) {
		nrTricksOfPlayers[player]++;
	}
	
	public int[] getCopyOfNrTricksOfPlayers() {
		// deep copy..
		int[] copy = new int[nrTricksOfPlayers.length];
		for(int i=0; i<nrTricksOfPlayers.length; i++){
			copy[i] = nrTricksOfPlayers[i];
		}
		return copy;
	}
	
	// Trick suit of last round
	public void setTrickSuitOfLastRound(ClassicalSuits trickSuitOfLastRound) {
		this.trickSuitOfLastRound = trickSuitOfLastRound;
	}
	
	public ClassicalSuits getTrickSuitOfLastRound() {
		return trickSuitOfLastRound;
	}
	
	// initHandSize
	public int getRoundsThatWillBePlayed(){
		return roundsThatWillBePlayed;
	}
	
	// Current highest card of trick
	public Card<ClassicalSymbols, ClassicalSuits> getHighestCardOfTrick(){
		if(getCurrentTrick().isEmpty()){
			return null;
		}
		
		Card<ClassicalSymbols, ClassicalSuits> highestCard = getCurrentTrick().get(0);
		
		for(Card<ClassicalSymbols, ClassicalSuits> currentCard : getCurrentTrick()){
			
			// If symbol is lower then symbol of current lowest card: this is the new lowest card.
			if(currentCard.getf2().equals(this.currentTrickSuit) && currentCard.getf1().ordinal() > highestCard.getf1().ordinal()){
				highestCard = currentCard;
			}
		}		
		
		return highestCard;
		
	}
	
	/**
	 * Given the number of players (variant) modify deck appropriately.
	 * @param numPlayers 
	 * @return
	 */
	private static CardDeck<ClassicalSymbols, ClassicalSuits> createAppropriateDeck(int numPlayers){
		// Create actual deck.
		CardDeck<ClassicalSymbols, ClassicalSuits> deck = new CardDeck<>(EnumSet.allOf(ClassicalSymbols.class), EnumSet.allOf(ClassicalSuits.class));
		
		// Choose variant based on number of players.
		switch(numPlayers){
			case 3: case 6: rmAllTwosFromDeck(deck); return deck; 
			case 5: rmTwoOfDiamondsAndTwoOfSpades(deck); return deck;
			case 4: return deck;
			default: throw new IllegalArgumentException("===Hearts can only be played from 3 - 6 players===");
		}
	}
	
	/**
	 * Removes all twos from deck. (Also works these cards have been removed before.)
	 * @param deck Hearts deck.
	 */
	private static void rmAllTwosFromDeck(CardDeck<ClassicalSymbols, ClassicalSuits> deck){
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.CLUBS));
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.DIAMONDS));
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.SPADES));
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.HEARTS));
	}
	
	/**
	 * Removes two of diamonds and two of spades of the deck.
	 * @param deck Hearts deck.
	 */
	private static void rmTwoOfDiamondsAndTwoOfSpades(CardDeck<ClassicalSymbols, ClassicalSuits> deck){
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.DIAMONDS));
		deck.removeCardType(new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.SPADES));
	}
	
	/**
	 * Sets the starting card according to variant (number of Players)
	 * @param numPlayers Number of players.
	 * @return The starting card.
	 */
	private Card<ClassicalSymbols, ClassicalSuits> setStartingCard(int numPlayers) {
		
		switch(numPlayers){
		case 3: case 6: this.startingCard = new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.THREE, ClassicalSuits.CLUBS);
		return startingCard;
		case 4: case 5: this.startingCard = new Card<ClassicalSymbols, ClassicalSuits>(ClassicalSymbols.TWO, ClassicalSuits.CLUBS);
		return startingCard;
		default: throw new IllegalArgumentException("===Hearts can only be played from 3 - 6 players===");

		}
	}
	
	@Override
	public void printPretty(){
		
		Log.info("-----------------------------");
		
		if((new HeartsGameLogic().isTerminal(this))){
			Log.info("Game has terminated");
			return;
		}
		
		Log.info("State after move below is executed: ");
		Log.info();
		Log.info("Round: " + getRoundNR());
		if(getBeginningPlayer() != -1){
			Log.info("Player" + getBeginningPlayer() + " started this round");
		} else{
			Log.info("No player started this round");
		}
		if(getCurrentTrickSuit()==null){
			Log.info("Suit of the round: free choice");
		} else{
			Log.info("Suit of the round: " + getCurrentTrickSuit().toString());
		}
		Log.info("It's Player" + getCurrentPlayer() + "'s turn.");
		Log.info("Trick pile: " + currentTrick.size());
		Log.info("Tricks collected: " + getTricksCollected());
		Log.info();
		Log.info("-----------------------------");
	}
	
	private String getTricksCollected(){
		StringBuilder s = new StringBuilder();
		s.append("[");
		for(int i=0; i<nrTricksOfPlayers.length; i++){
			if(i==0){
				s.append(nrTricksOfPlayers[i]);
			}
			else{
				s.append(", " + nrTricksOfPlayers[i]);
			}
		}
		s.append("]");
		
		return s.toString();
	}

	public boolean isGameStarted() {
		return gameStarted;
	}

	public void setGameStarted(boolean gameStarted) {
		this.gameStarted = gameStarted;
	}
	
}
