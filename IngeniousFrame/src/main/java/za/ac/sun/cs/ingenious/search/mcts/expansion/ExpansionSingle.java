package za.ac.sun.cs.ingenious.search.mcts.expansion;

import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

public class ExpansionSingle<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>> implements ExpansionThreadSafe<C, N> {
	
	private GameLogic<S> logic;
	
	private ExpansionSingle(GameLogic<S> logic) {
		this.logic = logic;
	}

	/**
	 * Create a new child node from the current node and add it to the search tree.
	 * @param node the current node for which a child must be added
	 * @return
	 */
	public C expand(N node) {
		if (logic.isTerminal(node.getState())) {
			return node.getSelfAsChildType();
		}
        C expandedChild;
		expandedChild = node.getAnUnexploredChild(logic);
		if (expandedChild == null) {
			return node.getSelfAsChildType();
		}

		node.addChild(expandedChild);
		return expandedChild;
	}
	
	public static <SS extends GameState, CC, NN extends MctsNodeCompositionInterface<SS,CC,?>> ExpansionThreadSafe<CC, NN> newExpansionSingle(GameLogic<SS> logic) {
		return new ExpansionSingle<>(logic);
	}
}
