package za.ac.sun.cs.ingenious.core.commandline.client;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

import za.ac.sun.cs.ingenious.core.commandline.BaseArguments;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.TCPProtocol;

import java.io.IOException;

/**
 * Created by chriscz on 2016/07/18.
 */
@Parameters(commandDescription = "connects to a hosted game") public class ClientArguments
        extends BaseArguments {
    @Parameter(names = "-port", description = "GameServer port [default=" + TCPProtocol.PORT + "]")
    private int serverPort = -1;

    @Parameter(names = "-hostname", description = "GameServer hostname [default=localhost]")
    private String serverHostname = null;

    @Parameter(names = "-lobby", description =
	"Name of the lobby to connect to, (will display a list "
                    + "if there's more than one possibility)")
    private String lobbyName = null;

    @Parameter(names = "-game", description =
	"The referee of the game this client wishes to play (will display a list "
			+ "if there's more than one possibility). All referees are automatically registered with their class names. You can also supply the -C command to the server script to register a referee with any name you like.")
    private String gameType = null;

    @Parameter(names = "-engine", description = "Full java identifier for the engine you wish to play with")
    private String engineClassname = null;

    @Parameter(names = "-config", description = "Path to a file containing game specific configuration formatted as JSON")
    private String gameconfigPath = null;

    @Parameter(names = "-enhancement", description = "Path to a file containing enhancement specific configuration formatted as JSON")
    private String enhancementConfigPath = null;

    @Parameter(names = "-threadCount", description = "The number of threads to run for the current engine")
    private int threadCount = -1;

    @Parameter(names = "-turnLength", description = "The number of threads to run for the current engine")
    private int turnLength = -1;


    @Parameter(names = "-username", description = "Username to connect with [default=-engine]", required = true)
    private String username = null;


    @Override public void validateAndPrepareArguments(JCommander commander) {
        if (gameconfigPath != null) {
            MatchSetting m = null;
            try {
                m = new MatchSetting(gameconfigPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            // port
            if (serverPort == -1) {
                int configFilePort = m.getSettingAsInt("port", -1);
                if (configFilePort != -1) {
                    serverPort = configFilePort;
                } else {
                    serverPort = TCPProtocol.PORT;
                }
            }
            // hostname
            if (serverHostname == null) {
                String configFileHostname = m.getSettingAsString("hostname", "null");
                if (!configFileHostname.equals("null")) {
                    serverHostname = configFileHostname;
                } else {
                    serverHostname = "localhost";
                }
            }
            // lobby
            if (lobbyName == null) {
                String configFileLobbyName = m.getSettingAsString("lobby", "null");
                if (!configFileLobbyName.equals("null")) {
                    lobbyName = configFileLobbyName;
                }
            }
            // game
            if (gameType == null) {
                String configFileGameType = m.getSettingAsString("game", "null");
                if (!configFileGameType.equals("null")) {
                    gameType = configFileGameType;
                }
            }
            // engine
            if (engineClassname == null) {
                String configFileEngine = m.getSettingAsString("engine", "null");
                if (!configFileEngine.equals("null")) {
                    engineClassname = configFileEngine;
                }
            }
            // enhancement
            if (enhancementConfigPath == null) {
                String configFileEnhancement = m.getSettingAsString("enhancement", "null");
                if (!configFileEnhancement.equals("null")) {
                    enhancementConfigPath = configFileEnhancement;
                } else {
                    enhancementConfigPath = "EnhancementChoices/EnhancementChoiceVanilla.json";
                }
            }
            // threadCount
            if (threadCount == -1) {
                int configFileThreadCount = m.getSettingAsInt("threadCount", -1);
                if (configFileThreadCount != -1) {
                    threadCount = configFileThreadCount;
                } else {
                    threadCount = 1;
                }
            }
            // turnLength
            if (turnLength == -1) {
                int configFileThreadCount = m.getSettingAsInt("turnLength", -1);
                if (configFileThreadCount != -1) {
                    turnLength = configFileThreadCount;
                } else {
                    turnLength = 1000;
                }
            }
        } else {
            // port
            if (serverPort == -1) {
                serverPort = TCPProtocol.PORT;
            }
            // hostname
            if (serverHostname == null) {
                serverHostname = "localhost";
            }
            // enhancement
            if (enhancementConfigPath == null) {
                enhancementConfigPath = "EnhancementChoices/EnhancementChoiceVanilla.json";
            }
            // threadCount
            if (threadCount == -1) {
                threadCount = 1;
            }
            // turnLength
            if (turnLength == -1) {
                turnLength = 1000;
            }
        }
    }


    /* --- GETTERS -----------------------------------------------------------------------------*/

    public String getEnhancementConfigPath() {
        return enhancementConfigPath;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public int getTurnLength() {
        return turnLength;
    }

    public String getEngineClassname() {
        return engineClassname;
    }

    public String getServerHostname() {
        return serverHostname;
    }

    public int getServerPort() {
        return serverPort;
    }

    public String getGameType() {
        return gameType;
    }

    public String getLobbyName() {
        return lobbyName;
    }

    public String getUsername() {
        return username;
    }
}
