package za.ac.sun.cs.ingenious.games.mdp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;

/**
 * This class represents a state in an MDP. This MDP state internally keeps track of a game-specific state
 * representation defined by the class type parameter. Note that this allows for more complex state representation for
 * an MDP, such as grouping multiple states with their actions as a single MDP state.
 *
 * @author Steffen Jacobs
 */
public class MDPState<T> extends GameState {
    // Might need to put these constants somewhere else..
    public static final int DEFAULT_INITIAL_STATE = 0;
    public static final int DEFAULT_NUMBER_OF_PLAYERS = 1;

    /**
     * Initial game state used for referee match resets.
     */
    private T initialGameState;
    private T gameState;
    private T previousGameState;

    public MDPState(T initialGameState, T gameState) {
        super(DEFAULT_NUMBER_OF_PLAYERS);

        this.initialGameState = initialGameState;
        this.gameState = gameState;
    }

    public MDPState(T initialGameState) {
        super(DEFAULT_NUMBER_OF_PLAYERS);

        this.initialGameState = initialGameState;
        this.gameState = initialGameState;
    }

    public MDPState(int numPlayers, T initialGameState) {
        super(numPlayers);

        gameState = initialGameState;
    }

    public MDPState(MDPState<T> toCopy) {
        super(toCopy.getNumPlayers());

        initialGameState = toCopy.getInitialGameState();
        gameState = toCopy.getGameState();
        previousGameState = toCopy.getPreviousGameState();
    }

    /**
     * Initializes an MDPState using the specified file containing the dynamics of the MDP.
     *
     * @param mdpSetting Settings class containing a description of the MDP.
     */
    public static MDPState<Long> fromSetting(MDPSetting mdpSetting) {
        long initialGameState = mdpSetting.getInitialState();
        long gameState = mdpSetting.getInitialState();

        return new MDPState<>(initialGameState, gameState);
    }

    public T getGameState() {
        return gameState;
    }

    public T getInitialGameState() {
        return gameState;
    }

    public T getPreviousGameState() {
        return previousGameState;
    }

    public void setGameState(T gameState) {
        this.gameState = gameState;
    }

    public void nextGameState(T gameState) {
        this.previousGameState = this.gameState;
        this.gameState = gameState;
    }

    public void setPreviousGameState(T previousGameState) {
        this.previousGameState = previousGameState;
    }

    @Override
    public void printPretty() {
        Log.info("MDP State: " + gameState);
    }

    @Override
    public String toString() {
        return "MDPState{" +
                "state=" + gameState +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        MDPState<?> mdpState = (MDPState<?>) o;
        return gameState.equals(mdpState.gameState);
    }

    @Override
    public int hashCode() {
        return gameState.hashCode();
    }

    @Override
    public GameState deepCopy() {
        return new MDPState<T>(this);
    }
}
