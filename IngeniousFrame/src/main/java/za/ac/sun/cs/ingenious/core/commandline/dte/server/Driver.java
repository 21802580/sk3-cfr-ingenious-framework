package za.ac.sun.cs.ingenious.core.commandline.dte.server;

import com.esotericsoftware.minlog.Log;
import com.google.common.base.Strings;

import java.io.IOException;
import java.net.URISyntaxException;

import za.ac.sun.cs.ingenious.core.RefereeFactory;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.network.lobby.GameServer;
import za.ac.sun.cs.ingenious.core.network.lobby.LobbyHandler;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.GameSystem;
import za.ac.sun.cs.ingenious.logging.Logger;
import za.co.percipio.dce.DCEClientMessageHandler;
import za.co.percipio.mpl.MessageClient;

/**
 * Created by Chris Coetzee on 2016/07/28.
 * <p/>
 * Known Issues
 * - This driver lacks any means to load new referees, and will
 * only function with those that are defined in the RefereeFactory class
 */
public class Driver {

    private static Thread gameServerThread;

    public static void main(String[] args)
            throws IOException, URISyntaxException, ClassNotFoundException {
        String dceHostname = args[0];
        int dcePort = Integer.parseInt(args[1]);

        Config config = Config.loadFromConfigFile();

        /* 0. Build all the configuration */
        String username =
                "LobbyHoster_" + (int) (Math.random() * 1000) + "_" + System.currentTimeMillis();

        // a random name for the lobby
        String lobbyName =
                "Lobby_" + (int) (Math.random() * 1000) + "_" + System.currentTimeMillis();
        MatchSetting ms = new MatchSetting(config.getGameConfig());
        ms.setLobbyName(lobbyName);
        ms.setGameName(config.getGameName());

        Log.setLogger(new Logger(Constants.LogDirectory, "commandline.dte.server.Driver"));

        if (!Strings.isNullOrEmpty(config.getReferee())) {
            Log.info(String.format("Registering Referee[%s] for Game[%s]", config.getReferee(),
                                      config.getGameName()));
            RefereeFactory.getInstance().registerNewReferee(config.getGameName(),
                                                                  config.getReferee());
        } else {
            /* assume that the referee is registered internally */
        }

        /* 1. start a game server asynchronously */
        GameServer gs = new GameServer(config.getHostname(), config.getPort());
        gameServerThread = new Thread(gs);
        gameServerThread.start();

        /* FIXME HACK, wait for the thread to start and connection to be made */
        Thread.yield();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {}

        /* 2. create a lobby for the players to join */
        // here we use the port from the connected gameserver
        LobbyHandler host = new LobbyHandler();

        if (!host.connect(config.getHostname(), gs.getConnectedPort(),
                lobbyName)) {
			Log.info("Could not establish connection to server, terminating script.");
			gameServerThread.interrupt();
			System.exit(-1);
		}
        host.createGame(ms);

        /* 3. Listen to messages from DCE */
        // here we use the port from the connected gameserver
        DCEClientMessageHandler handler = new DCEClientMessageHandler(gs,
                                                                      config.getHostname(),
                                                                      gs.getConnectedPort(),
                                                                      config.getGameName(),
                                                                      lobbyName);

        // register as a handler on the main eventbus
        GameSystem.getInstance().getEventBus().register(handler);
        MessageClient client = new MessageClient(dceHostname, dcePort, handler);

        client.start();
    }
}
