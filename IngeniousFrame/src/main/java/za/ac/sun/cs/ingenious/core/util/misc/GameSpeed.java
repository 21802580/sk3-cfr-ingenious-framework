package za.ac.sun.cs.ingenious.core.util.misc;

/**
 * Used to define the delay between iterations of the game loop. Useful for when spectating an agent play a game that
 * has a UI.
 *
 * @author Steffen Jacobs
 */
public enum GameSpeed {
    REALTIME(0),
    VERY_FAST(9),
    FAST(27),
    MEDIUM(81),
    SLOW(243),
    VERY_SLOW(729);

    private int value;

    private GameSpeed(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
