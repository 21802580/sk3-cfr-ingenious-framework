package za.ac.sun.cs.ingenious.search.ismcts;

import com.esotericsoftware.minlog.Log;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SearchValue;
import za.ac.sun.cs.ingenious.search.mcts.legacy.TreeDescender;

/**
 * Implementation of TreeDescender for MOISMCTS. First, chooses a determinization compatible
 * with the current information set. Then, searches in the current player's search TreeEngine for the node
 * with the best search value until an expandable node is reached. Restricts nodes to descend and to 
 * expand to those that are available for the current determinization.
 */
public class MOISMCTSDescender<S extends TurnBasedGameState> implements TreeDescender<S, MOISMCTSNode<S>> {

	protected GameLogic<S> logic;
	protected SearchValue<S,ISMCTSNode<S>> searchValue;
	protected ActionSensor<S> sensor;
	
	protected S determinizedState;
	protected InformationSetDeterminizer<S> det;
	
	/**
	 * @param logic Logic object.
	 * @param searchValue Object with which to generate search values for nodes in the player trees. Most common is {@link SubsetUCT}.
	 * @param sensor ActionSensor for actions that are being applied to states of the differen players.
	 * 				 Actions applied to the determinization are observed as for the environment player.
	 * @param det Determinizer for information sets.
	 */
	public MOISMCTSDescender(GameLogic<S> logic, SearchValue<S, ISMCTSNode<S>> searchValue,
			ActionSensor<S> sensor,InformationSetDeterminizer<S> det) {
		this.logic = logic;
		this.sensor = sensor;
		this.searchValue = searchValue;
		
		this.det = det;
		this.determinizedState = null;
	}

	@Override
	public MOISMCTSNode<S> expand(MOISMCTSNode<S> parent) {
		// The simplest implementation just chooses a random move, but...
		List<Move> allMoves = parent.getUnexpandedMoves();
		List<Move> unexpandedMoves = new LinkedList<Move>();
		// ... restrict to moves that are available for the current determinization.
		for (Move m : allMoves) {
			if (logic.validMove(determinizedState, m)) {
				unexpandedMoves.add(m);
			}
		}
		if (unexpandedMoves.isEmpty()) {
			return null;
		}
		int index = (int) (Math.random() * unexpandedMoves.size());
		Move toExpand = unexpandedMoves.get(index);
		// Sanity check
		if (!(toExpand instanceof Action)) {
			String errorMsg = "Expanding move that is not an action.";
			Log.error("MOISMCTSDescender", errorMsg);
			throw new RuntimeException(errorMsg);
		}
		Action actionToExpand = (Action)toExpand;
		allMoves.remove(toExpand);

		MOISMCTSNode<S> child = createNodeForMove(actionToExpand, parent);
		parent.addChild(child);
		
		updatingDeterminizedState(actionToExpand);
		return child;
	}
	
	/**
	 * @param a Move that is being expanded or applied.
	 */
	protected void updatingDeterminizedState(Action a) {
		logic.makeMove(determinizedState, sensor.fromPointOfView(a, determinizedState, -1));
	}

	/**
	 * @param a Action that leads to the new node.
	 * @param parent Parent of the new node.
	 * @return New child node of parent.
	 */
	protected MOISMCTSNode<S> createNodeForMove(Action a, MOISMCTSNode<S> parent) {
		List<ISMCTSNode<S>> parentPlayerNodes = parent.getPlayerNodes();
		List<ISMCTSNode<S>> newPlayerNodes = new ArrayList<ISMCTSNode<S>>();		
		for (int i = 0; i < parentPlayerNodes.size(); i++) {
			ISMCTSNode<S> parentNodeForI = parentPlayerNodes.get(i);
			ISMCTSNode<S> childForI = null;
			Move aAsSeenByI = this.sensor.fromPointOfView(a, determinizedState, i);
			if (i == parent.getCurrentPlayer()) {
				@SuppressWarnings("unchecked")
				S newState = (S) parentNodeForI.getGameState().deepCopy();
				this.logic.makeMove(newState, aAsSeenByI);
				List<Move> unexpandedMoves = (i==newState.nextMovePlayerID?
						new ArrayList<Move>(logic.generateActions(newState, i)):
						null);
				childForI = new ISMCTSNode<S>(newState, logic, a, parentNodeForI,
						unexpandedMoves);
				parentNodeForI.addChild(childForI);
			} else {
				for (ISMCTSNode<S> child : parentNodeForI.getExpandedChildren()) {
					if (child.getMove().equals(aAsSeenByI)) {
						childForI = child;
						break;
					}
				}
				if (childForI == null) {
					@SuppressWarnings("unchecked")
					S newState = (S) parentNodeForI.getGameState().deepCopy();
					this.logic.makeMove(newState, aAsSeenByI);
					List<Move> unexpandedMoves = (i==newState.nextMovePlayerID?
							new ArrayList<Move>(logic.generateActions(newState, i)):
							null);
					childForI = new ISMCTSNode<S>(newState, logic, aAsSeenByI, parentNodeForI,
							unexpandedMoves);
					parentNodeForI.addChild(childForI);
				}
			}
			
			newPlayerNodes.add(childForI);
		}
		
		return new MOISMCTSNode<S>(newPlayerNodes.get(0).getCurrentPlayer(),
				newPlayerNodes, logic, a, parent);
	}
	

	@Override
	public MOISMCTSNode<S> descend(MOISMCTSNode<S> root, int totalNofPlayouts) {		
		// First, determinize the root state
		// We assume here that the player active in root also owns the information set in root (this is
		// clearly true during MOISMCTS)
		determinizedState = det.determinizeUnknownInformation(root.getGameState(), root.getCurrentPlayer());

		// Now takes a greedy path trough the TreeEngine by always picking
		// the node with the highest searchValue until it hits an expandable or terminal node.
		// The highest search value is calculated from the TreeEngine of the player who is active at that node.
		// However, only nodes that are available for the current determiniziation are considered.
		// Furthermore, the determinization is updated before the search continues.
		MOISMCTSNode<S> currentNode = root;
		while(currentNode != null && !currentNode.isTerminal() && !logic.isTerminal(determinizedState)){
			if (!currentNode.getUnexpandedMoves().isEmpty() ||
					currentNode.getExpandedChildren().isEmpty()) {
				// Node can be expanded OR no further descent possible, therefore descent ends here.
				break;
			}
			
			double highestValue = Double.NEGATIVE_INFINITY;
			MOISMCTSNode<S> bestChild = null;
			for(MOISMCTSNode<S> child : currentNode.getExpandedChildren()){
				if (!logic.validMove(determinizedState, child.getMove())) {
					continue;
				}
				// For subset multi armed bandit, update the availability count of each child node
				// that is available during this descent.
				child.increaseAvailabilityCount();
				// Important: Find the search value in the TreeEngine of the current player.
				double childValue = this.searchValue.get(child.getPlayerNodes().get(currentNode.getCurrentPlayer()), currentNode.getCurrentPlayer(), totalNofPlayouts);
				if(childValue > highestValue){
					bestChild = child;
					highestValue = childValue; 
				}
			}

			if (bestChild == null) {
				break;
			} else {				
				Action nextAction = (Action) bestChild.getMove();
				updatingDeterminizedState(nextAction);				
				currentNode = bestChild;
			}
		}
		
		return currentNode;
	}

	@Override
	public S getStateForPlayout(MOISMCTSNode<S> fromNode) {
		return determinizedState;
	}
	
}
