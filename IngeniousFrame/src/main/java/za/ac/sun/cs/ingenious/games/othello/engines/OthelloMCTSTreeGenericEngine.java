package za.ac.sun.cs.ingenious.games.othello.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.*;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloBoard;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloFinalEvaluator;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloLogic;
import za.ac.sun.cs.ingenious.games.othello.gamestate.OthelloMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsTree;
import za.ac.sun.cs.ingenious.search.mcts.backpropagation.*;
import za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionThreadSafe;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeTreeParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.CMCTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC.MctsCMCNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.LGR.LGRTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MAST.MastTable;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.MctsNodeExtensionParallelInterface;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.PW.MctsPWNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.RAVE.MctsRaveNodeExtensionParallel;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelection;
import za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionFinal;
import za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationThreadSafe;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Hashtable;

import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationAverage.newBackpropagationAverage;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationContextual.newBackpropagationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgr.newBackpropagationLgr;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationLgrf.newBackpropagationLgrf;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationMast.newBackpropagationMast;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationRave.newBackpropagationRave;
import static za.ac.sun.cs.ingenious.search.mcts.backpropagation.BackpropagationSimple.newBackpropagationSimple;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionMast.newExpansionMast;
import static za.ac.sun.cs.ingenious.search.mcts.expansion.ExpansionSingle.newExpansionSingle;
import static za.ac.sun.cs.ingenious.search.mcts.selection.FinalSelectionUct.newFinalSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionPW.newTreeSelectionProgressiveWidening;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionRave.newTreeSelectionRave;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUct.newTreeSelectionUct;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctFPU.newTreeSelectionFPU;
import static za.ac.sun.cs.ingenious.search.mcts.selection.TreeSelectionUctTunedFPU.newTreeSelectionFPUTuned;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationContextual.newSimulationContextual;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrFull.newSimulationLgrfFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrTree.newSimulationLgrTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationLgrfFull.newSimulationLgrFull;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastFullTree.newSimulationMastFullTree;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationMastTreeOnly.newSimulationMastTreeOnly;
import static za.ac.sun.cs.ingenious.search.mcts.simulation.SimulationRandom.newSimulationRandom;

public class OthelloMCTSTreeGenericEngine extends Engine {

    MctsTree<OthelloBoard, MctsNodeTreeParallel<OthelloBoard>> mcts;
    ArrayList<BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>>> backpropagationEnhancements = new ArrayList<>();
    Hashtable<String, MctsNodeExtensionParallelInterface> enhancementExtensionClasses = new Hashtable<>();
    String playerEnhancementConfig;

    protected int TURN_LENGTH; // in milliseconds
    protected int THREAD_COUNT;

    private MastTable visitedMovesTableMast = null;
    private LGRTable visitedMovesTableLgr = null;
    private CMCTable visitedMovesTableCmc = null;

    MctsNodeTreeParallel<OthelloBoard> root;

    protected ZobristHashing zobrist;
    protected OthelloBoard currentState = null;
    protected OthelloBoard board;
    protected OthelloLogic logic = OthelloLogic.defaultOthelloLogic;
    protected OthelloFinalEvaluator evaluator = new OthelloFinalEvaluator();

    /**`
     * @param toServer An established connection to the GameServer
     */
    public OthelloMCTSTreeGenericEngine(EngineToServerConnection toServer, String enhancementConfig, int threadCount, int turnLength) throws IOException, MissingSettingException, IncorrectSettingTypeException {
        super(toServer);

        THREAD_COUNT = threadCount;
        TURN_LENGTH = turnLength;

        currentState = null;

        int length = enhancementConfig.length();
        playerEnhancementConfig = enhancementConfig.substring(36,length-5);

        MatchSetting m = new MatchSetting(enhancementConfig);

        String SelectionEnhancement = m.getSettingAsString("Selection");
        String ExpansionEnhancement = m.getSettingAsString("Expansion");
        String SimulationEnhancement = m.getSettingAsString("Simulation");
        String BackpropagationEnhancements = m.getSettingAsString("Backpropagation");
        String[] BackpropagationEnhancement = BackpropagationEnhancements.split(",");

        // Selection strategy object
        TreeSelection<MctsNodeTreeParallel<OthelloBoard>> selection = getSelectionClass(SelectionEnhancement);

        // Final Selection strategy object
        TreeSelectionFinal<MctsNodeTreeParallel<OthelloBoard>> finalSelection = newFinalSelectionUct(logic);

        // Simulation strategy object
        boolean recordMoves = false;
        if (SelectionEnhancement.equals("Rave")) {
            recordMoves = true;
        }
        SimulationThreadSafe<OthelloBoard> simulation = getSimulationClass(SimulationEnhancement, recordMoves);

        // Expansion strategy object
        ExpansionThreadSafe<MctsNodeTreeParallel<OthelloBoard>,MctsNodeTreeParallel<OthelloBoard>> expansion = getExpansionClass(ExpansionEnhancement);


        // Backpropagation strategy objects
        int size = BackpropagationEnhancement.length;
        for (int i = 0; i < size; i++) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagationEnhancement = getBackpropagationClass(BackpropagationEnhancement[i]);
            if (backpropagationEnhancement != null) {
                backpropagationEnhancements.add(backpropagationEnhancement);
            }
        }

        this.mcts = new MctsTree<>(selection, expansion, simulation, backpropagationEnhancements, finalSelection, logic, THREAD_COUNT, playerID);

    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {
        zobrist = zobristHashing;
    }

    @Override
    public String engineName() {
        return "GoMCTSTreeVanillaEngine";
    }

    @Override
    /**
     * Terminal state has been reached, end game.
     */
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        OthelloFinalEvaluator eval = new OthelloFinalEvaluator();
        Log.info("Game has terminated");
        Log.info("Final scores:");
        double[] score = eval.getScore(currentState);
        for (int i = 0; i < score.length; i++) {
            try(FileWriter fw = new FileWriter("ResultsGo.txt", true);
                BufferedWriter bw = new BufferedWriter(fw);
                PrintWriter out = new PrintWriter(bw))
            {
                if (i == playerID) {
                    String str = playerEnhancementConfig + ": " + score[i];
                    out.println(str);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        Log.info("Final state:");
        currentState.printPretty();
        toServer.closeConnection();
        System.exit(0);
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        try {
            if (currentState == null) {
                this.currentState = new OthelloBoard( );
            }
        } catch (Exception ex) {
            Log.info("OthelloLegacyMCTSEngine error:Problem creating board - " + ex.getLocalizedMessage());
        }
    }

    @Override
    public void receivePlayedMoveMessage(PlayedMoveMessage a) {
        logic.makeMove(currentState, a.getMove());

//        if (a.getMove() instanceof XYAction) {
//            XYAction action = (XYAction) a.getMove();
//            long xyActionIDtoXOR = zobrist.getPiecePlacementID(action.getX(), action.getY(), currentState.getBoardSize(), action.getPlayerID());
//            long hash = zobrist.XOR(currentBoardHash, xyActionIDtoXOR);
//            currentBoardHash = hash;

//            long hash = zobrist.hashBoard(currentState);
//            zobrist.addBoardToHashtable(hash, currentState);
//        }
    }

    @Override
    /**
     * Use MCTS to compute the best action to play on this player's next move in the game
     */
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Hashtable<String, MctsNodeExtensionParallelInterface> newEnhancementExtensionClasses = new Hashtable<>();
        if(visitedMovesTableMast != null) {
            visitedMovesTableMast.resetVisitedMoves();
        }

        if(visitedMovesTableLgr != null) {
            visitedMovesTableLgr.resetVisitedMoves();
        }

        if(visitedMovesTableCmc != null) {
            visitedMovesTableCmc.resetVisitedMoves();
        }
        for (MctsNodeExtensionParallelInterface newExtension: enhancementExtensionClasses.values()) {
            try {
                newEnhancementExtensionClasses.put(newExtension.getID(), newExtension.getClass().newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

//        if (currentStateNode != null && currentStateNode.getPlayerID() == playerID) {
//            root = currentStateNode;
//        } else {
//            root = new MctsNodeTreeParallel<>(currentState, null, null, new ArrayList<>(), logic, newEnhancementExtensionClasses, playerID, previousRootAction);
//        }

        root = new MctsNodeTreeParallel<>(currentState, null, null, new ArrayList<>(), logic, newEnhancementExtensionClasses, playerID);

        MctsNodeTreeParallel finalChoice = mcts.doSearch(root, TURN_LENGTH, zobrist);
        Action action = finalChoice.getPrevAction();

        if (action == null) {
            return new PlayActionMessage(new ForfeitAction((byte) playerID));
        }

        return new PlayActionMessage(action);
    }

    /**
     * Create object for the selection class specified in the .json file.
     * @param selectionClass
     * @return
     */
    public TreeSelection<MctsNodeTreeParallel<OthelloBoard>> getSelectionClass(String selectionClass) {
        if (selectionClass.equals("Uct")) {
            return newTreeSelectionUct(logic, 0.24, playerID);
        } else if (selectionClass.equals("UctFPU")) {
            return newTreeSelectionFPU(logic, 1.89248, 0.24, playerID);
        } else if (selectionClass.equals("UctTunedFPU")) {
            return newTreeSelectionFPUTuned(logic, 1.89248, 0.24, playerID);
        } else if (selectionClass.equals("Rave")) {
            return newTreeSelectionRave(logic, 0.24, 1000, playerID);
        } else if (selectionClass.equals("PW")) {
            MctsPWNodeExtensionParallel parallelPWNodeExtension = new MctsPWNodeExtensionParallel();
            enhancementExtensionClasses.put("PW", parallelPWNodeExtension);
            return newTreeSelectionProgressiveWidening(logic);
        } else {
            System.out.println("\nInvalid selection strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the expansion class specified in the .json file.
     * @param expansionClass
     * @return
     */
    public ExpansionThreadSafe<MctsNodeTreeParallel<OthelloBoard>,MctsNodeTreeParallel<OthelloBoard>> getExpansionClass(String expansionClass) {
        if (expansionClass.equals("Single")) {
            return newExpansionSingle(logic);
        } else if (expansionClass.equals("Mast")) {
            return newExpansionMast(logic, visitedMovesTableMast, 30.0);
        } else {
            System.out.println("\nInvalid expansion strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the simulation class specified in the .json file.
     * @param simulationClass
     * @param recordMoves
     * @return
     */
    public SimulationThreadSafe<OthelloBoard> getSimulationClass(String simulationClass, boolean recordMoves) {
        if (simulationClass.equals("Random")) {
            return newSimulationRandom(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), recordMoves);
        } else if (simulationClass.equals("MastTreeOnly")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            visitedMovesTableMast =((BackpropagationMast<MctsNodeTreeParallel<OthelloBoard>>) backpropagationMast).getQMastTable(1.0);
            return newSimulationMastTreeOnly(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableMast, 1.0, recordMoves);
        } else if (simulationClass.equals("MastFullTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagationMast = newBackpropagationMast();
            backpropagationEnhancements.add(backpropagationMast);
            visitedMovesTableMast =((BackpropagationMast<MctsNodeTreeParallel<OthelloBoard>>) backpropagationMast).getQMastTable(1.0);
            return newSimulationMastFullTree(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableMast, 1.0, recordMoves);
        } else if (simulationClass.equals("Contextual")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagationContextual = newBackpropagationContextual();
            backpropagationEnhancements.add(backpropagationContextual);
            visitedMovesTableCmc =((BackpropagationContextual<MctsNodeTreeParallel<OthelloBoard>>) backpropagationContextual).getCMCTable();
            return newSimulationContextual(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableCmc, 0.67, 0.21, recordMoves);
        } else if (simulationClass.equals("LgrTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagation = newBackpropagationLgr();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgr<MctsNodeTreeParallel<OthelloBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrTree(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrFull")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagation = newBackpropagationLgr();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgr<MctsNodeTreeParallel<OthelloBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrFull(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrfTree")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagation = newBackpropagationLgrf();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgrf<MctsNodeTreeParallel<OthelloBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrTree(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableLgr, recordMoves);
        } else if (simulationClass.equals("LgrfFull")) {
            BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> backpropagation = newBackpropagationLgrf();
            backpropagationEnhancements.add(backpropagation);
            visitedMovesTableLgr =((BackpropagationLgrf<MctsNodeTreeParallel<OthelloBoard>>) backpropagation).getLGRTable();
            return newSimulationLgrfFull(logic, new OthelloMctsFinalEvaluator(), new PerfectInformationActionSensor<OthelloBoard>(), visitedMovesTableLgr, recordMoves);
        } else {
            System.out.println("\nInvalid simulation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }

    /**
     * Create object for the backpropagation class specified in the .json file.
     * @param backpropagationClass
     * @return
     */
    public BackpropagationThreadSafe<MctsNodeTreeParallel<OthelloBoard>> getBackpropagationClass(String backpropagationClass) {
        if (backpropagationClass.equals("Average")) {
            return newBackpropagationAverage();
        } else if (backpropagationClass.equals("Simple")) {
            return newBackpropagationSimple(playerID);
        } else if (backpropagationClass.equals("Mast")) {
            return null;
        } else if (backpropagationClass.equals("Rave")) {
            MctsRaveNodeExtensionParallel parallelRaveNodeExtension = new MctsRaveNodeExtensionParallel();
            enhancementExtensionClasses.put("Rave", parallelRaveNodeExtension);
            return newBackpropagationRave();
        } else if (backpropagationClass.equals("Contextual")) {
            MctsCMCNodeExtensionParallel parallelCMCNodeExtension = new MctsCMCNodeExtensionParallel();
            enhancementExtensionClasses.put("Contextual", parallelCMCNodeExtension);
            return null;
        } else {
            System.out.println("\nInvalid backpropagation strategy defined in the EnhancementChoice.json file.\n");
            System.exit(0);
        }
        return null;
    }
}
