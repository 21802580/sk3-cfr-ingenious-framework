package za.ac.sun.cs.ingenious.core.network.external.messages.type;

/**
 * Contains constants used to identify message types. These are kept in sync with similar constants in the Python
 * application.
 *
 * @author Steffen Jacobs
 */
public class ExternalMessageType {
    public static final int EXIT = 1;
    public static final int ACK = 2;
    public static final int REQ_TABLE_STATE_ACTIONS = 3;
    public static final int TABLE_STATE_ACTIONS = 4;
    public static final int REQ_TABLE_STATE_ACTION_VALUE = 5;
    public static final int TABLE_STATE_ACTION_VALUE = 6;
    public static final int SET_TABLE_STATE_ACTION_VALUE = 7;
    public static final int REQ_LINEAR_FEATURE_VALUES = 8;
    public static final int INIT_LINEAR_FEATURE_APPROXIMATION = 9;
    public static final int LINEAR_FEATURE_VALUES = 10;
    public static final int TRAIN_LINEAR_FEATURES = 11;
    public static final int REQ_NEURAL_NET_ACTION_VALUES = 12;
    public static final int INIT_NEURAL_NET = 13;
    public static final int NEURAL_NET_ACTION_VALUES = 14;
    public static final int NEURAL_NET_PUT_TRANSITION = 15;
    public static final int NEURAL_NET_CREATE_BACKUP = 16;
}
