package za.ac.sun.cs.ingenious.core.exception;

/**
 * Thrown by MatchSetting methods if settings have another type than the one that was requested.
 */
public class IncorrectSettingTypeException extends Exception {
	private static final long serialVersionUID = 1L;

	private final String settingName;
	private final String expectedType;
	public IncorrectSettingTypeException(String settingName, String expectedType) {
		super("MatchSetting element with name " + settingName + " is not of expected type " + expectedType);
		this.settingName = settingName;
		this.expectedType = expectedType;
	}
	
	public String getSettingName() {
		return settingName;
	}
	
	public String getExpectedType() {
		return expectedType;
	}

}
