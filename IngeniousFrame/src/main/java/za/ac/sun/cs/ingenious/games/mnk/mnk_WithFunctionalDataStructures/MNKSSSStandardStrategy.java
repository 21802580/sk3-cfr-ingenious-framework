package za.ac.sun.cs.ingenious.games.mnk.mnk_WithFunctionalDataStructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutorService;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.search.sss.SSS;

/**
 * A simple standard depth-limited strategy for finding moves in the MNK game using the SSS* search algorithm.
 * 
 * <p> This class acts as a module that extends {@link MNKSSSStrategy}. Provides nodes that extend SSS.SearchNode,
 * and calls the SSS.compute method with these nodes to determine the best player action.
 * 
 * @author Nicholas Robinson
 */
public class MNKSSSStandardStrategy extends MNKSSSStrategy {

    public MNKSSSStandardStrategy(int m, int n, int k) {
        super(m, n, k);
    }

    /**
     * determine the best move from the given list of moves, using the SSS* algorithm.
     * 
     * @param player the player to move.
     * @param board the board state on which the move acts
     * @param moves an array of the moves to be considered
     * @param bound SSS* bound
     * @param depthLimit maximum search tree depth
     * @param numberOfAvailableMoves number of empty positions on the game board
     * @return the results of the search
     */
    public SearchResult determineBestMove(int player, PVector<Integer> board, int[] moves, int bound, int depthLimit, int numberOfAvailableMoves) {
        // determining the number of available moves on a given state is a somewhat expensive operation.
        if (moves.length == 0) {
            return null;
        }

        Environment environment = new Environment(player, numberOfAvailableMoves, depthLimit);

        List<Environment.SearchNode> searches = new ArrayList<Environment.SearchNode>(moves.length);
        for (int m : moves) {
            searches.add(environment.new SearchNode(board.set(m, player(player)), null, quantifyChangeInOpportunities(board, m, player), 1));
        }

        SSS.SearchResult<Environment.SearchNode> result = SSS.compute(searches, bound);

        if (result == null) return null;
        return new SearchResult(moves[searches.indexOf(result.getRoot())], result.getScore(), result.getIterations());
    }

    /**
     * Return the best possible player action as determined by the SSS* algorithm using a vanilla search strategy.
     * Evenly distributes the possible moves from a given board state into a set of tasks that are run by an executor service.
     * 
     * @param player the player to move.
     * @param board the board state on which the player will act on.
     * @param depthLimit maximum search tree depth.
     * @param executor executor service.
     * @param threadCount number of threads in the executor service.
     * @param partitionSize move batching size.
     * @return the position of the best move as determined by the search.
     */
    public int generateMove(int player, PVector<Integer> board, int depthLimit, ExecutorService executor, int threadCount, int partitionSize) {
        return generateMove(player, board, executor, threadCount, partitionSize,
            (m, b, a) -> determineBestMove(player, board, m, b, depthLimit, a));
    }


    class Environment {

        final Integer player;
        final Integer opponent;
        final int baseNumberOfAvailableMoves;
        int depthLimit;

        public Environment(int player, int baseNumberOfAvailableMoves, int depthLimit)
        {
            this.player = player(player);
            this.opponent = player(player == 1 ? 2 : 1);
            this.baseNumberOfAvailableMoves = baseNumberOfAvailableMoves;
            this.depthLimit = depthLimit;
        }

        Integer getPlayer(int depth) {
            return ((depth & 1) == 0) ? player : opponent;
        }

        int getNumberOfAvailableMoves(int depth) {
            return baseNumberOfAvailableMoves - depth;
        }

        public class SearchNode extends SSS.SearchNode<SearchNode> implements Iterable<SearchNode> {

            PVector<Integer> board;
            SearchNode parent;
            SearchNode[] children;

            int score;
            int depth;

            int boardCursor = -1, childCursor = -1;

            private SearchNode(PVector<Integer> board, SearchNode parent, int score, int depth) {
                this.board = board;
                this.parent = parent;
                this.score = score;
                this.depth = depth;

                children = new SearchNode[getNumberOfAvailableMoves(depth)];
            }

            @Override
            public SearchNode getParent() {
                return parent;
            }

            @Override
            public Iterable<SearchNode> children() {
                return this;
            }

            @Override
            public Iterator<SearchNode> iterator() {

                return new Iterator<SearchNode>() {
                    int ptr = 0;

                    @Override
                    public boolean hasNext() {
                        return ptr < children.length;
                    }

                    @Override
                    public SearchNode next() {
                        return getChild(ptr++);
                    }
                };
            }

            @Override
            public int childCount() {
                return (children != null) ? children.length : getNumberOfAvailableMoves(depth);
            }

            @Override
            public SearchNode getChild(int index) {
                while (childCursor < index) {
                    while (board.get(++boardCursor) != null) {}
                    children[++childCursor] = createChild(boardCursor);
                }
                return children[index];
            }

            @Override
            public boolean isTerminal() {
                return depth >= depthLimit || gameHasWinner(score) || getNumberOfAvailableMoves(depth) == 0;
            }

            @Override
            public boolean isRoot() {
                return parent == null;
            }

            @Override
            public boolean isMinimiser() {
                return (depth & 1) == 1;
            }

            @Override
            public boolean isMaximiser() {
                return (depth & 1) == 0;
            }

            @Override
            public int evaluate() {
                return score;
            }

            @Override
            public void free() {
                board = null;
                children = null;
            }

            SearchNode createChild(int move) {
                Integer token = getPlayer(depth);
                int s, q = quantifyChangeInOpportunities(board, move, token); 
                if (q == Integer.MAX_VALUE) {
                    s = VICTORYSCORE + getNumberOfAvailableMoves(depth);
                    if (isMinimiser()) s = -s;
                }
                else {
                    s = score + (isMaximiser() ? q : -q);
                }
                return new SearchNode(board.set(move, token), this, s, depth + 1);
            }
        }
    }
}