package za.ac.sun.cs.ingenious.search.mcts.expansion;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;

public interface ExpansionThreadSafe<C, N extends MctsNodeCompositionInterface<?, C, ?>> {

	/**
	 * Create a new child node from the current node and add it to the search tree.
	 * @param node the current node for which a child must be added
	 * @return
	 */
	C expand(N node);

}