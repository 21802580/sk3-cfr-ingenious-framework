package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;

import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Human player engine for GridWorld that prompts the user for action choices.
 *
 * @author Steffen Jacobs
 */
public class GridWorldHumanPlayerEngine extends GridWorldEngine {
    private Scanner scanner;

    public GridWorldHumanPlayerEngine(EngineToServerConnection toServer) {
        super(toServer);
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldHumanPlayerEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        scanner = new Scanner(System.in);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        List<Action> availableActions = logic.generateActions(state, playerID);

        CompassDirection[] availableDirections = availableActions.stream()
                .map(x -> ((CompassDirectionAction)x).getDir())
                .toArray(CompassDirection[]::new);

        StringBuilder promptStringBuilder = new StringBuilder();

        promptStringBuilder.append("Current state: ")
                .append(state.toString(logic))
                .append("\nAvailable actions:\n");

        for (int i = 0; i < availableDirections.length; i++) {
            promptStringBuilder.append(i).append(" ").append(availableDirections[i]).append("\n");
        }

        promptStringBuilder.append("Choose an action: ");

        System.out.println(promptStringBuilder);

        try {
            int directionChoice = scanner.nextInt();

            while (directionChoice < 0 || directionChoice >= availableDirections.length) {
                System.out.println("Invalid direction.");
                directionChoice = scanner.nextInt();
            }

            return new PlayActionMessage(new CompassDirectionAction(playerID, availableDirections[directionChoice]));
        } catch (Exception e) {
            Log.error("An error occurred while reading user input.");
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        scanner.close();
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        Log.info("Reward received: " + a.getReward());
    }
}
