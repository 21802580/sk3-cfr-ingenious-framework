package za.ac.sun.cs.ingenious.games.othello.gamestate;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsGameFinalEvaluator;

/**
 * Class for evaluating terminal states for Othello, as well as evalutations
 * that return result values that can be used in MCTS searches.
 * 
 * @author Karen Laubscher
 */
public class OthelloMctsFinalEvaluator extends OthelloFinalEvaluator implements MctsGameFinalEvaluator<OthelloBoard> {

	// Othello is a zero-sum 2-player game
	private static final double WIN_VALUE  = 1;
	private static final double LOSE_VALUE = -1;
	private static final double DRAW_VALUE = 0;

	public OthelloMctsFinalEvaluator() {
		super();
	}

	/**
	 * Returns the scores for each player if the game is terminal. If the state
	 * is not terminal, then null is returned (scores from the normal evaluation
	 * function, which returns null in the case of a non terminal state).
	 *
	 * @return	The scores (used for MCTS results)
	 */
	public double[] getMctsScore(OthelloBoard forState) {
		double[] winScores = new double[2];
		GameFinalState winner = getWinnerEnum(forState);
		switch(winner) {
			case BLACK_WIN:
				 winScores[0] = WIN_VALUE;
				 winScores[1] = LOSE_VALUE;
				 break;
			case WHITE_WIN:
				 winScores[0] = LOSE_VALUE;
				 winScores[1] = WIN_VALUE;
				 break;
			case DRAW:
				 winScores[0] = DRAW_VALUE;
				 winScores[1] = DRAW_VALUE;
				 break;
			default:
				Log.error("OthelloMctsFinalEvaluator", "Error: getScore asked on a state that is not terminal");
				return super.getScore(forState);
		}
		
		return winScores;
	}
	
	
	/**
	 * Returns a GameFinalState enum for the winner; BLACK_WIN, WHITE_WIN,
	 * DRAW, or NOT_TERMINAL.
	 *
	 * @return	The enum representing the game state
	 */
	public GameFinalState getWinnerEnum(OthelloBoard forState) {
		if (!logic.isTerminal(forState)) {
			return GameFinalState.NOT_TERMINAL;
		}

		if (forState.blackScore > forState.whiteScore) {
			return GameFinalState.BLACK_WIN;
		} else if (forState.blackScore < forState.whiteScore) {
			return GameFinalState.WHITE_WIN;
		}

		return GameFinalState.DRAW;
	}
	
	/** 
	 * Getter for the value of a win (For the MCTS result)
	 * 
	 * @return 	The win value used in MCTS.
	 */
	public double getWinValue() {
		return (double) WIN_VALUE;
	}
	
	/**
	 * Getter for the value of a loss (For the MCTS result).
	 * 
	 * @return 	The loss value used in MCTS.
	 */
	public double getLossValue() {
		return (double) LOSE_VALUE;
	}	
}
