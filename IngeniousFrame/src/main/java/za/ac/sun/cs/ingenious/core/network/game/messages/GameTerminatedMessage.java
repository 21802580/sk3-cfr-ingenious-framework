package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.network.Message;


/**
 * This message will be sent by the server to the players after a game terminated. Can be
 * extended to store additional information that needs to be send after the game ends
 * (statistics about the different players' performances etc.)
 * 
 * @author Stephan Tietz
 */
public class GameTerminatedMessage extends Message {

	private static final long serialVersionUID = 1L;

}
