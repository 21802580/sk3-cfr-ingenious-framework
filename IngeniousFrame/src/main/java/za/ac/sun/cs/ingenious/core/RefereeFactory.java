package za.ac.sun.cs.ingenious.core;

import com.esotericsoftware.minlog.Log;

import org.reflections.Reflections;

import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.util.JarLoader;
import za.ac.sun.cs.ingenious.games.bomberman.BMReferee;
import za.ac.sun.cs.ingenious.games.domineering.DomineeringReferee;
import za.ac.sun.cs.ingenious.games.loa.LOAController;
import za.ac.sun.cs.ingenious.games.mnk.MNKReferee;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTReferee;
import za.ac.sun.cs.ingenious.games.othello.OthelloReferee;

/**
 * This class can generate new Referee objects. Which concrete class gets instantiated
 * depends on the "game name" that is specified as a commandline argument to the
 * commandline drivers or in the MatchSettings.
 * 
 * There are currently three ways how "game names" correspond to referees:
 * 
 * - Each class in the games-package extending the Referee class is registered as a
 * referee with its class name as the "game name".
 * 
 * - The -C argument to the server-commandline-driver can be used to register other
 * referees under different names
 * 
 * - Some game names are hardcoded in the RefereeFactory constructor for backward
 * compatibility. DO NOT ADD HARDCODED REFEREE NAMES ANYMORE
 */
public class RefereeFactory {

    public static final Class<?>          PLAYER_REPRESENTATION_ARRAY = new PlayerRepresentation[0]
            .getClass();
    public static       RefereeFactory instance                    = null;

    /* instance fields */
    private JarLoader                             loader;
    private Map<String, Class<? extends Referee>> referees;

    public static synchronized RefereeFactory getInstance() {
        if (instance == null) {
            instance = new RefereeFactory();
        }
        return instance;
    }

    private RefereeFactory() {
        referees = new HashMap<>();
        loader = new JarLoader();
        
        // These hardcoded referee names are left here for compatibility.
        // For new referees, do NOT add your referee here!! Instead, the auto-recognition below
        // will find your referee, or you can supply the class name to the start script
        // of the game server
        referees.put("LOA", LOAController.class);
        referees.put("TTT", TTTReferee.class);
        referees.put("bomberman", BMReferee.class);
        referees.put("MNK-Game", MNKReferee.class);
        referees.put("Dom", DomineeringReferee.class);
        referees.put("othello", OthelloReferee.class);
        
        // The following bit of code finds all referees in the project and registers their classes
        Reflections reflections = new Reflections("za.ac.sun.cs.ingenious");
        Set<Class<? extends Referee>> subTypes = reflections.getSubTypesOf(Referee.class);
        for (Class<? extends Referee> someReferee : subTypes) {
        	if (!Modifier.isAbstract(someReferee.getModifiers())) {
        		Log.info("Registering referee \"" + someReferee.getCanonicalName() + "\" as \"" + someReferee.getSimpleName() + "\"");
        		try {
					registerNewReferee(someReferee.getSimpleName(), someReferee.getCanonicalName());
				} catch (ClassNotFoundException e) {
					Log.info("Didn't work, could not find class:");
					e.printStackTrace();
				} catch (ClassCastException e) {
					Log.info("Didn't work, ClassCastException:");
					e.printStackTrace();
				}
        	}
        }
    }

    /**
     * @param match Match settings to initialise the new referee with. The game name setting of this object is used to identify, which referee to start. See also the documentation in this class's header. When the new referee object is created, this match setting object also gets passed to the constructor.
     * @param players When the new referee object is created, this array gets passed to the constructor.
     * @return Newly created referee object or null if there was an error.
     */
    public Referee getReferee(MatchSetting match, PlayerRepresentation[] players) {

    	String gameName = match.getGameName();

        /*  FIXME raise a reasonable exception here */
        if (gameName == null) {
            Log.error(
                    "Could not instantiate referee. gameName not set in MatchSettings.");
            return null;
        }

        if (referees.containsKey(gameName)) {
            Class<? extends Referee> cls = referees.get(gameName);

            try {
                return cls.getDeclaredConstructor(MatchSetting.class, PLAYER_REPRESENTATION_ARRAY)
                          .newInstance(match, players);
            } catch (Exception e) {
                e.printStackTrace();
                /* this is fatal and indicates a programming error */
                throw new RuntimeException(e);
            }

        } else {
            /*  FIXME raise a reasonable exception here */
            new ClassNotFoundException(
                    "The requiered referee: \"" + gameName + "\" is not known.")
                    .printStackTrace();
            return null;
        }
    }

    public boolean isRefereeRegistered(String simpleName) {
        return referees.containsKey(simpleName);
    }

    public void registerNewReferee(String simpleName, String className)
            throws ClassNotFoundException, ClassCastException {
        Class<?> cls = loader.loadClass(className);

        if (!Referee.class.isAssignableFrom(cls)) {
            throw new ClassCastException(String.format("Class %s does not extend %s", cls,
                                                       Referee.class.getCanonicalName()));
        }
        registerNewReferee(simpleName, (Class<Referee>) cls);
    }

    public void registerNewReferee(String simpleName, Class<Referee> referee)
            throws ClassNotFoundException, ClassCastException {

        if (referees.containsKey(simpleName)) {
            Log.warn(this.getClass().getName(), "Overriding previous class name " + referees.get(simpleName).getName() + " of " + simpleName + " with " + referee.getName() + "\n");
        }

        referees.put(simpleName, referee);
    }

}
