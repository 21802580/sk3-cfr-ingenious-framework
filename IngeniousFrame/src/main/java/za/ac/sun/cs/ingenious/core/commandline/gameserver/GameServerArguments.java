package za.ac.sun.cs.ingenious.core.commandline.gameserver;

import com.beust.jcommander.DynamicParameter;
import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import za.ac.sun.cs.ingenious.core.commandline.BaseArguments;
import za.ac.sun.cs.ingenious.core.network.TCPProtocol;
import za.ac.sun.cs.ingenious.core.util.GsonExtension;

/**
 * Argument class for the Game Server. All arguments (except for config) can be
 * loaded from a configuration file. When doing so, commandline arguments such
 * as port and hostname take precedence over those sepcified in the config file.
 * 
 * Created by Chris Coetzee on 2016/07/27.
 */
@Parameters(commandDescription = "Starts a game server to manage game hosting")
public class GameServerArguments extends BaseArguments {
    private static final String DEFAULT_HOSTNAME = "0.0.0.0";
    private static final int    DEFAULT_PORT     = TCPProtocol.PORT;

    private static class ControllerEntry {
        @GsonExtension.JsonRequired
        public String simpleName;
        @GsonExtension.JsonRequired
        public String className;
    }

    @Parameter(names = "-port", description = "GameServer port [default=" + DEFAULT_PORT + "]")
    private int port = -1;

    @Parameter(names = "-hostname", description = "GameServer hostname [default=" + DEFAULT_HOSTNAME
            + "]")
    private String hostname = null;

	@DynamicParameter(names = "-C", description = "Specifies a new referee. This should be of the form: simpleName=\"className\" (Note that className must be the canonical / java classname including packages )")
    private HashMap<String, String> controllersInternal = new HashMap<>();

    /* this field is used when loading from GSON */
    private List<ControllerEntry> controllers = new ArrayList<>();

	@Parameter(names = "-config", description = "File containing the configuration parameters for this Gameserver")
    private String config = null;

    public String getConfig() {
        return config;
    }

    public String getHostname() {
        return hostname;
    }

    public int getPort() {
        return port;
    }

    public HashMap<String, String> getControllers() {
        return controllersInternal;
    }

    @Override public void validateAndPrepareArguments(JCommander commander) {
        if (config != null) {
            GameServerArguments arguments;

            /* Attemt to read the game configuration */
            try {
                Gson gson = new Gson();
                arguments = gson.fromJson(new JsonReader(new FileReader(config)),
                                          GameServerArguments.class);

                if (this.hostname == null && arguments.getHostname() != null) {
                    this.hostname = arguments.getHostname();
                }

                if (this.port < 0 && arguments.getPort() >= 0) {
                    this.port = arguments.getPort();
                }

                /* extend the controllersInternal */
                for (ControllerEntry o : arguments.controllers) {
                    if (this.controllersInternal.containsKey(o.simpleName)) {
                        Log.error(
                                "Ignoring config entry for controller: " + o.simpleName + "="
                                        + o.className);
                    } else {
                        this.controllersInternal.put(o.simpleName, o.className);
                    }
                }

            } catch (IOException e) {
                Log.error("Error reading configuration file.");
                e.printStackTrace();
                throw new IllegalArgumentException(e);
            }
        }

        /* ensure that the defaults are set */
        if (hostname == null) {
            hostname = DEFAULT_HOSTNAME;
        }

        if (port < 0) {
            port = DEFAULT_PORT;
        }

    }
}
