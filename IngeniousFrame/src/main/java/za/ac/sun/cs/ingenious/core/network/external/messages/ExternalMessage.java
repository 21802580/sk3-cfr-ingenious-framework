package za.ac.sun.cs.ingenious.core.network.external.messages;

/**
 * Super class for all messages sent to Python application.
 *
 * @author Steffen Jacobs
 */
public class ExternalMessage {
    private int type;

    public ExternalMessage(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }
}
