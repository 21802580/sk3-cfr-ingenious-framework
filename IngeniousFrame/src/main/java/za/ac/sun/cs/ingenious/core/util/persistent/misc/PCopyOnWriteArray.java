package za.ac.sun.cs.ingenious.core.util.persistent.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Supplier;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.persistent.PVectorIterable;
import za.ac.sun.cs.ingenious.core.util.persistent.PVectorIterator;

/**
 * A wrapper for arrays of Objects to implement the PVector interface, utilizes a copy-on-write persistence strategy.
 * 
 * @author Nicholas Robinson
 */
public class PCopyOnWriteArray<T> implements PVector<T> {

    protected T[] array;

    /**
     * Create an instance of an empty array.
     */
    @SuppressWarnings("unchecked")
    public PCopyOnWriteArray() {
        array = (T[]) new Object[0];
    }

    /**
     * Create an instance of an empty array, with a set array capacity.
     * @param capacity
     */
    @SuppressWarnings("unchecked")
    public PCopyOnWriteArray(int capacity) {
        array = (T[]) new Object[capacity];
    }

    /**
     * Initialize the PCopyOnWriteArray with the data of a given array.
     * @param array the array of data to hold.
     */
    public PCopyOnWriteArray(T[] array) {
        this.array = Arrays.copyOf(array, array.length);
    }

    /**
     * Initialize the PCopyOnWriteArray with the data returned by an Iterable.
     * @param iterable
     */
    public PCopyOnWriteArray(Iterable<T> iterable) {
        this(((Supplier<List<T>>)() -> {
            List<T> list = new ArrayList<T>();
            iterable.forEach(list::add);
            return list;
        }).get());
    }

    /**
     * Initialze the PCopyOnWriteArray with data taken from a given collection.
     * @param collection the collection of data to hold.
     */
    @SuppressWarnings("unchecked")
    public PCopyOnWriteArray(Collection<T> collection) {
        array = collection.toArray((T[]) new Object[collection.size()]);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public T get(int index) {
        return (index < array.length) ? array[index] : null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PVector<T> set(int index, T value) {

        T[] copy = Arrays.copyOf(array, (index >= array.length) ? index + 1 : array.length);
        copy[index] = value;

        return new PCopyOnWriteArray<T>(copy);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PVector<T> multiSet(int[] indices, T[] values) {
        if (indices.length == 0) return this;

        int top = indices[0];
        for (int i = 1; i < indices.length; i++) {
            if (indices[i] > top) top = indices[i];
        }

        T[] copy = Arrays.copyOf(array, (top >= array.length) ? top + 1 : array.length);

        for (int i = 0; i < indices.length; i++) {
            copy[indices[i]] = values[i];
        }

        return new PCopyOnWriteArray<T>(copy);
    }

    @Override
    public PVectorIterable<T> range(int startIndex, int endIndex) {

        class RangeIterator implements PVectorIterator<T> {
            int currentIndex = startIndex - 1;

            @Override
            public boolean hasNext() {
                return currentIndex < endIndex;
            }

            @Override
            public T next() {
                return (++currentIndex < array.length) ? array[currentIndex] : null;
            }

            @Override
            public PVector<T> erase() {
                return PCopyOnWriteArray.this.set(currentIndex, null);
            }

            @Override
            public PVector<T> set(T item) {
                return PCopyOnWriteArray.this.set(currentIndex, item);
            }

			@Override
			public int getIndex() {
				return currentIndex;
			}
        }

        return new PVectorIterable<T>() {

            @Override
            public PVectorIterator<T> iterator() {
                return new RangeIterator();
            } 
        };
    }

    @Override
    public PVectorIterable<T> items() {
        return range(0, array.length);
    }
}