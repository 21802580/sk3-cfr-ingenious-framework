package za.ac.sun.cs.ingenious.core.network.game.messages;

import za.ac.sun.cs.ingenious.core.Reward;
import za.ac.sun.cs.ingenious.core.network.Message;

/**
 * Sent to a player from the referee to relay how good the previous action was.
 *
 * @author Elan van Biljon
 */
public class RewardMessage extends Message {
	private static final long serialVersionUID = 1L;

	private Reward m;

	public RewardMessage(Reward m) {
		this.m = m;
	}

	public Reward getReward() {
		return m;
	}
}
