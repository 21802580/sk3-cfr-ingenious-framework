package za.ac.sun.cs.ingenious.search.rl.dp;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.GameRewardEvaluator;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the Value Iteration dynamic programming algorithm. This algorithm requires full knowledge of all
 * states as well as the full dynamics of the environment in order to solve the MDP. Currently only supports the MDP
 * game because of this. In order to apply this algorithm to other games, they will first need to be converted to an MDP
 * game. Another assumption made is that class parameter T, in MDPState, extends GameState (as opposed to evaluating
 * some arbitrary game representation, such as a list of states).
 *
 * @param <T> Type wrapped by MDPState in the MDP.
 * @param <L> Logic type.
 * @param <E> Evaluator type.
 *
 * @author Steffen Jacobs
 */
public class ValueIteration<T, L extends MDPLogic<T>, E extends GameRewardEvaluator> {
    private static final double DEFAULT_THETA = 0.1;
    private static final double DEFAULT_STATE_VALUE = 0.0;
    private static final double DEFAULT_GAMMA = 1.0;

    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_WIDTH = 5;
    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_HEIGHT = 5;

    private int playerID;

    /**
     * Threshold specifying the accuracy of solved policy.
     */
    private double theta;

    /**
     * Discount factor.
     */
    private double gamma;

    /**
     * Game logic.
     */
    private L logic;

    /**
     * Evaluator for rewards.
     */
    private E evaluator;

    /**
     * Collection of non-terminal states.
     */
    private Collection<MDPState<T>> states;

    /**
     * Map used for retrieving the MDP State associated with the arbitrary value T.
     */
    private Map<T, MDPState<T>> statesLookup;

    /**
     * Table containing the value corresponding to each state.
     */
    private Map<MDPState<T>, Double> valueTable;

    public ValueIteration() {
        theta = DEFAULT_THETA;
        gamma = DEFAULT_GAMMA;
        statesLookup = new HashMap<>();
    }

    public ValueIteration<T, L, E> withValueTable(Map<MDPState<T>, Double> valueTable) {
        this.valueTable = valueTable;
        return this;
    }

    public void initializeValueTable() {
        valueTable = new HashMap<>();
        for (MDPState<T> state : this.states) {
            valueTable.put(state, DEFAULT_STATE_VALUE);
        }
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public void setStates(Collection<MDPState<T>> states) {
        this.states = states;

        for (MDPState<T> mdpState : states) {
            statesLookup.put(mdpState.getGameState(), mdpState);
        }
    }

    public void setLogic(L logic) {
        this.logic = logic;
    }

    public void setEvaluator(E evaluator) {
        this.evaluator = evaluator;
    }

    public Collection<MDPState<T>> getStates() {
        return states;
    }

    public Map<MDPState<T>, Double> getValueTable() {
        return valueTable;
    }

    /**
     * Solves the MDP by performing value iteration until the solution is within a certain accuracy specified by theta.
     */
    public void solve() {
        if (states == null || valueTable == null) {
            Log.error("Set of states or value table were not set for ValueIteration algorithm. Ensure that these are set after algorithm initialization.");
        }

        double delta = Double.MAX_VALUE;
        Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p = logic.getP();

        while (delta >= theta) {
            delta = 0.0;
            for (MDPState<T> state : states) {
                if (logic.isTerminal(state)) continue;

                double stateValue = valueTable.get(state);

                List<Action> availableActions = logic.generateActions(state, playerID);
                double maxActionValue = -Double.MAX_VALUE;

                for (Action a : availableActions) {
                    Action action = a;
                    double actionValue = 0;
                    for (MDPState<T> newState : p.get(state).get(action).keySet()) {
                        MDPState<T> clonedNewState = (MDPState<T>)newState.deepCopy();
                        clonedNewState.setPreviousGameState(state.getGameState());

                        GameState retrievedNewState = null;

                        if (clonedNewState.getGameState() instanceof GameState) {
                            retrievedNewState = (GameState)clonedNewState.getGameState();
                        } else {
                            retrievedNewState = clonedNewState;
                        }

                        double rewardForNewState = evaluator.getReward(retrievedNewState)[playerID];
                        double probabilityOfNewState = p.get(state).get(action).get(newState);
                        double valueOfNewState = valueTable.get(newState);

                        actionValue += probabilityOfNewState * (rewardForNewState + gamma * valueOfNewState);
                    }

                    if (actionValue > maxActionValue)
                        maxActionValue = actionValue;
                }
                valueTable.put(state, maxActionValue);

                double newDelta = Math.abs(stateValue - maxActionValue);
                if (newDelta > delta)
                    delta = newDelta;
            }
        }
    }

    /**
     * Chooses the action with the highest expected reward, based on the values of the states the action may transition
     * to. Ties are broken consistently in which the first action is chosen.
     *
     * @param s
     * @return An action with the highest expected reward.
     */
    public Action chooseActionForState(T s) {
        MDPState<T> state = statesLookup.get(s);
        Map<MDPState<T>, Map<Action, Map<MDPState<T>, Double>>> p = logic.getP();
        List<Action> availableActions = logic.generateActions(state, playerID);

        Action maxAction = null;
        double maxActionValue = -Double.MAX_VALUE;

        for (Action a : availableActions) {
            Action action = a;
            double actionValue = 0;
            for (MDPState<T> newState : p.get(state).get(action).keySet()) {
                MDPState<T> clonedNewState = (MDPState<T>)newState.deepCopy();
                clonedNewState.setPreviousGameState(state.getGameState());

                GameState retrievedNewState = null;
                if (clonedNewState.getGameState() instanceof GameState) {
                    retrievedNewState = (GameState)clonedNewState.getGameState();
                } else {
                    retrievedNewState = clonedNewState;
                }

                actionValue += p.get(state).get(action).get(newState)
                        * (evaluator.getReward(retrievedNewState)[playerID] + gamma * valueTable.get(newState));
            }

            if (actionValue > maxActionValue) {
                maxActionValue = actionValue;
                maxAction = a;
            }
        }

        return maxAction;
    }

    /**
     * Provides a visualization of the state values, applicable only to MDPs for games in a grid e.g. GridWorld.
     * @param h Grid height
     * @param w Grid width
     */
    public void printStateGrid(int h, int w) {
        Log.info("Value Table: \n");

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                double value = valueTable.get(state);
                sb.append(df.format(value)).append("\t");
            }
            sb.append("\n\n");
        }

        Log.info(sb);
    }
}
