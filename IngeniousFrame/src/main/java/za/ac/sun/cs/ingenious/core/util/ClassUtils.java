package za.ac.sun.cs.ingenious.core.util;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Map;
import java.util.Random;

/**
 * Created by Chris Coetzee on 2016/07/29.
 */
public class ClassUtils {
    private static final double DEFAULT_TOLERANCE = 1e-6;

    /**
     * Returns the directory relative to which this class is resolved
     */
    public static File getTopLevelDirectory(Class<?> referenceClass) throws URISyntaxException {
        final URL url = referenceClass.getProtectionDomain().getCodeSource().getLocation();
        final File pathToFile = new File(url.toURI()).getAbsoluteFile();
        return pathToFile;
    }

    public static void main(String[] args) throws Exception {
        Log.info(getTopLevelDirectory(ClassUtils.class));
    }

    /**
     * Chooses from set of objects in a map, based on the probability associated with each object. It checked that the
     * sum of the probabilities across the set of objects is equal to 1 within the default tolerance.
     *
     * @param distribution The distribution containing object, probability pairs.
     * @param rng Random number generator to use.
     * @return Chosen object.
     */
    public static <T> T chooseFromDistribution(Map<T, Double> distribution, Random rng) throws IncorrectlyNormalizedDistributionException {
        return chooseFromDistribution(distribution, rng, DEFAULT_TOLERANCE);
    }

    /**
     * Chooses from set of objects in a map, based on the probability associated with each object. It checked that the
     * sum of the probabilities across the set of objects is equal to 1 within the provided tolerance.
     *
     * @param distribution The distribution containing object, probability pairs.
     * @param rng Random number generator to use.
     * @param tolerance Used to deal with floating point error when summing the probabilities in the distribution.
     * @return Chosen object.
     */
    public static <T> T chooseFromDistribution(Map<T, Double> distribution, Random rng, double tolerance) throws IncorrectlyNormalizedDistributionException {
        double random = rng.nextDouble();
        double probabilitySum = 0.0;

        for (T object : distribution.keySet()) {
            double probabilityOfObject = distribution.get(object);
            probabilitySum += probabilityOfObject;

            if ((probabilitySum - tolerance) > 1.0)
                throw new IncorrectlyNormalizedDistributionException("Invalid distribution - sum of probabilities exceeds 1.0.");

            if (random <= probabilitySum) {
                return object;
            }
        }

        throw new IncorrectlyNormalizedDistributionException("Invalid distribution - sum of probabilities is too small.");
    }
}
