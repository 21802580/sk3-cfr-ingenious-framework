package za.ac.sun.cs.ingenious.games.loa.util;

import java.io.Serializable;

public enum Direction implements Serializable {
    RIGHT, LEFT, UP, DOWN, UP_RIGHT, UP_LEFT, DOWN_RIGHT, DOWN_LEFT
}
