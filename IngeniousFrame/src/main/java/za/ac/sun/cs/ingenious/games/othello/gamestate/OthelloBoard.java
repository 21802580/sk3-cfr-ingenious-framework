package za.ac.sun.cs.ingenious.games.othello.gamestate;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.util.state.TurnBased2DBoard;

/**
 * The GameState implementation for the Othello board game.
 *
 * @author Rudolf Stander
 */
public class OthelloBoard extends TurnBased2DBoard {

	/* Definitions of board block states and player IDs. DO NOT CHANGE */
	public static final int EMPTY = -1;
	public static final int BLACK = 0;
	public static final int WHITE = 1;
	/* Okay to change */
	public static final String EMPTY_STRING = "*";
	public static final String BLACK_STRING = "0";
	public static final String WHITE_STRING = "1";
	
	/* Default board sizes */
	public static final int DEFAULT_HEIGHT = 8;
	public static final int DEFAULT_WIDTH = 8;

	/* The player scores. To save memory they are saved as separate ints */
	protected int blackScore;
	protected int whiteScore;

	/**
	 * Constructs a new OthelloBoard object and sets up the board with specified
	 * dimensions.
	 *
	 * @param height	the board's height, should be a multiple of 2
	 * @param width		the board's width, should be a multiple of 2
	 */
	public OthelloBoard(int height, int width) {
		super(height, width, BLACK, 2);


		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				board[i][j] = EMPTY;
			}
		}

		/* Set up the initial state of the black player */
		board[height / 2 - 1][width / 2] = BLACK;
		board[height / 2][width / 2 - 1] = BLACK;
		/* Set up the initial state of the white player */
		board[height / 2 - 1][width / 2 - 1] = WHITE;
		board[height / 2][width / 2] = WHITE;

		/* The black player should make the first move */
		nextMovePlayerID = BLACK;

		/* Initialize the player scores */
		blackScore = 2;
		whiteScore = 2;
	}

	/**
	 * Constructs a new OthelloBoard object and sets up the board.
	 */
	public OthelloBoard() {
		this(DEFAULT_HEIGHT, DEFAULT_WIDTH);
	}

	/**
	 * Constructs a new OthelloBoard object that is a carbon copy of the given
	 * OthelloBoard.
	 *
	 * @param copy	the board to copy
	 */
	public OthelloBoard(OthelloBoard copy) {
		super(copy);
		blackScore = copy.blackScore;
		whiteScore = copy.whiteScore;
	}

	@Override
	public OthelloBoard deepCopy() {
		return new OthelloBoard(this);
	}

	/**
	 * Returns the board's current grid.
	 *
	 * @return	the board's grid
	 */
	public int[][] getGrid() {
		return board;
	}

	/**
	 * Returns a copy of the board's current grid.
	 *
	 * @return	a copy of the board's grid
	 */
	public int[][] getGridCopy() {
		int copy[][] = new int[height][width];

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				copy[i][j] = board[i][j];
			}
		}

		return copy;
	}

	/**
	 * Returns the number (ID) of the player who will play next.
	 *
	 * @return	the next player's number (ID)
	 */
	public int getCurrentPlayer() {
		return nextMovePlayerID;
	}

	/**
	 * Updates the nextMovePlayerID to the next player's ID
	 */
	public void updateCurrentPlayer() {
		nextMovePlayerID = nextMovePlayerID == BLACK ? WHITE : BLACK;
	}

	/**
	 * Returns the score of each of the players.
	 *
	 * @return	an array with the players' scores as entries
	 */
	public double[] getScore() {
		double scores[] = new double[2];
		scores[0] = blackScore;
		scores[1] = whiteScore;

		return scores;
	}

	/**
	 * Returns a string representation of the current board state
	 *
	 * @return	a string representation of the current board state
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		StringBuilder singleColumnDashedLine = new StringBuilder();
		StringBuilder dashedLine = new StringBuilder("\n");
		int columnCharWidth = 1 + (int) Math.log10(width - 1);
		String formattedEmpty = String.format("%"+columnCharWidth+"s", EMPTY_STRING);
		String formattedBlack = String.format("%"+columnCharWidth+"s", BLACK_STRING);
		String formattedWhite = String.format("%"+columnCharWidth+"s", WHITE_STRING);

		/* Generate the scores */
		sb.append("Black Score: ");
		sb.append(blackScore);
		sb.append("\n");
		sb.append("White Score: ");
		sb.append(whiteScore);
		sb.append("\n");

		/* Generate next player */
		sb.append("Next player: ");

		if (nextMovePlayerID == BLACK) {
			sb.append("black\n");
		} else {
			sb.append("white\n");
		}

		/* Generate the board */
		sb.append("Board:\n");
		
		/* Add spacer before line of column numbers */
		for (int i = 0; i < columnCharWidth; i++) {
			sb.append(" ");
			singleColumnDashedLine.append("-");
		}
		
		sb.append("|");

		/* Print column numbers */
		for (int i = 0; i < width; i++) {
			sb.append(String.format("%"+columnCharWidth+"d", i));
			dashedLine.append(singleColumnDashedLine);
		}

		dashedLine.append(singleColumnDashedLine);
		dashedLine.append("-");
		sb.append(dashedLine);
		sb.append("\n");

		for (int i = 0; i < height; i++) {
			sb.append(String.format("%"+columnCharWidth+"d", i));
			sb.append("|");

			for (int j = 0; j < width; j++) {
				if (board[i][j] == EMPTY) {
					sb.append(formattedEmpty);
				} else if (board[i][j] == BLACK) {
					sb.append(formattedBlack);
				} else {
					sb.append(formattedWhite);
				}
			}

			sb.append("\n");
		}

		return sb.toString();
	}

	/**
	 * Prints a representation of the current board state.
	 */
	@Override
	public void printPretty() {
		Log.info("\n"+toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + blackScore;
		result = prime * result + whiteScore;
		return result;
	}

	@Override
	public boolean equals(Object that) {
		if (this == that) {
			return true;
		}

		if (!super.equals(that)) {
			return false;
		}

		if (getClass() != that.getClass()) {
			return false;
		}

		OthelloBoard thatBoard = (OthelloBoard) that;

		if (blackScore != thatBoard.blackScore) {
			return false;
		}

		if (whiteScore != thatBoard.whiteScore) {
			return false;
		}

		return true;
	}
}
