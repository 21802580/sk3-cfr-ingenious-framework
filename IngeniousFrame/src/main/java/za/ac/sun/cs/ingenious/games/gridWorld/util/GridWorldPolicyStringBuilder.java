package za.ac.sun.cs.ingenious.games.gridWorld.util;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.StateFeatureExtractor;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.search.rl.util.ExternalNeuralNetwork;
import za.ac.sun.cs.ingenious.search.rl.util.ExternalTable;
import za.ac.sun.cs.ingenious.search.rl.util.LinearFunctionApproximation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Utility class providing methods to build policy strings of GridWorld states using different policy representations.
 * This class is used by reinforcement learning algorithms and tests to output policies in a format that is easy to
 * read.
 *
 * @author Steffen Jacobs
 */
public class GridWorldPolicyStringBuilder {
    /**
     * Build a policy string for the given final state and policy containing state action-values.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param policy Policy containing state action-values e.g. a Q-table.
     * @return String of the policy.
     */
    public static String buildUsingPolicyTable(GridWorldState finalState, Map<GameState, Map<Action, Double>> policy) {
        Map<GameState, Action> flatPolicy = new HashMap<>();

        for (GameState gameState : policy.keySet()) {
            double maxValue = -Double.MAX_VALUE;
            Action maxAction = null;
            for (Action action : policy.get(gameState).keySet()) {
                double value = policy.get(gameState).get(action);
                if (value > maxValue) {
                    maxValue = value;
                    maxAction = action;
                }
            }

            flatPolicy.put(gameState, maxAction);
        }

        return buildUsingPolicy(finalState, flatPolicy);
    }

    /**
     * Build a policy string for the given final state and policies containing state action-values. Used for the
     * scenario where an agent maintains two tables such as in double learning.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param policy1 First policy containing state action-values e.g. a Q-table.
     * @param policy2 Second policy containing state action-values e.g. a Q-table.
     * @return String of the policy.
     */
    public static String buildUsingPolicyTables(GridWorldState finalState, Map<GameState, Map<Action, Double>> policy1, Map<GameState, Map<Action, Double>> policy2) {
        Map<GameState, Action> flatPolicy = new HashMap<>();

        for (GameState gameState : policy1.keySet()) {
            double maxValue = -Double.MAX_VALUE;
            Action maxAction = null;
            for (Action action : policy1.get(gameState).keySet()) {
                double value = (policy1.get(gameState).containsKey(action) ? policy1.get(gameState).get(action) : 0) + (policy2.get(gameState).containsKey(action) ? policy2.get(gameState).get(action) : 0);
                if (value > maxValue) {
                    maxValue = value;
                    maxAction = action;
                }
            }

            flatPolicy.put(gameState, maxAction);
        }

        return buildUsingPolicy(finalState, flatPolicy);
    }

    /**
     * Build a policy string for the given final state and policy containing state-action pairs.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param policy Policy containing state-action pairs, indicating the preferred or greedy action for each state.
     * @return String of the policy.
     */
    public static String buildUsingPolicy(GridWorldState finalState, Map<GameState, Action> policy) {
        int width = finalState.getWidth();
        int height = finalState.getHeight();

        StringBuilder sb = new StringBuilder();

        Map<Coord, Action> actionForCell = new HashMap<>();
        for (GameState state : policy.keySet()) {
            GridWorldState gridWorldState = (GridWorldState) state;
            width = gridWorldState.getWidth();
            height = gridWorldState.getHeight();

            Coord playerCoord = gridWorldState.findPlayerCoords(GridWorldState.DEFAULT_FIRST_PLAYER);
            actionForCell.put(playerCoord, policy.get(state));
        }

        sb.append(width).append("x").append(height).append("\n");

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (finalState.board[i][j] == GridWorldState.CELL_UNREACHABLE) {
                    // Unreachable cell
                    sb.append(GridWorldLevelParser.LEVEL_TOKEN_UNREACHABLE);
                } else {
                    Action action = actionForCell.get(new Coord(j, i));
                    CompassDirectionAction compassDirectionAction = (CompassDirectionAction) action;

                    if (compassDirectionAction == null) {
                        sb.append("•");
                    } else {
                        sb.append(compassDirectionAction.getDir().toStringPretty());
                    }
                }
                sb.append(GridWorldLevelParser.LEVEL_TOKEN_SPACER);
            }
            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Build a value table string for the given final state and value function containing state action-values.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param valueTable Table containing action-values e.g. a Q-table.
     * @return String of the policy.
     */
    public static String buildStateValuesUsingPolicyTable(GridWorldState finalState, Map<GameState, Map<Action, Double>> valueTable) {
        Map<GameState, Double> flatTable = new HashMap<>();

        for (GameState gameState : valueTable.keySet()) {
            double maxValue = -Double.MAX_VALUE;
            for (Action action : valueTable.get(gameState).keySet()) {
                double value = valueTable.get(gameState).get(action);
                if (value > maxValue) {
                    maxValue = value;
                }
            }

            if (maxValue > -Double.MAX_VALUE)
                flatTable.put(gameState, maxValue);
        }

        return buildStateValuesUsingPolicy(finalState, flatTable);
    }

    /**
     * Build a valueTable string for the given final state and valueTable containing state-action pairs.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param valueTable Table containing state values.
     * @return String of the valueTable.
     */
    public static String buildStateValuesUsingPolicy(GridWorldState finalState, Map<GameState, Double> valueTable) {
        int width = finalState.getWidth();
        int height = finalState.getHeight();

        StringBuilder sb = new StringBuilder();

        Map<Coord, Double> valueForCell = new HashMap<>();
        for (GameState state : valueTable.keySet()) {
            GridWorldState gridWorldState = (GridWorldState) state;
            width = gridWorldState.getWidth();
            height = gridWorldState.getHeight();

            Coord playerCoord = gridWorldState.findPlayerCoords(GridWorldState.DEFAULT_FIRST_PLAYER);
            valueForCell.put(playerCoord, valueTable.get(state));
        }

        sb.append(width).append("x").append(height).append("\n");

        DecimalFormat df = new DecimalFormat("0.00");

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                if (finalState.board[i][j] == GridWorldState.CELL_UNREACHABLE) {
                    // Unreachable cell
                    sb.append(GridWorldLevelParser.LEVEL_TOKEN_UNREACHABLE);
                } else {
                    Double value = valueForCell.get(new Coord(j, i));

                    if (value == null) {
                        sb.append("••••");
                    } else {
                        sb.append(df.format(value));
                    }
                }
                sb.append(GridWorldLevelParser.LEVEL_TOKEN_SPACER);
            }
            sb.append("\n\n");
        }

        return sb.toString();
    }

    /**
     * Build a policy string for the given final state and external table residing in the python app.
     *
     * @param finalState State containing the dimensions and features of the GridWorld instance.
     * @param externalTable Interface maintaining the connection between the agent and python.
     * @return String of the policy.
     */
    public static String buildUsingExternalTable(GridWorldState finalState, ExternalTable externalTable) {
        Map<GameState, Map<Action, Double>> policy = new HashMap<>();

        Set<GameState> stateSet = externalTable.getKnownStates();

        for (GameState state : stateSet) {
            if (!policy.containsKey(state))
                policy.put(state, new HashMap<>());

            Map<Action, Double> actionValues = externalTable.get(state);
            policy.put(state, actionValues);
        }

        return buildUsingPolicyTable(finalState, policy);
    }

    public static String buildUsingExternalFunctionApprox(GridWorldState state, Action[] actions, LinearFunctionApproximation functionApproximation, StateFeatureExtractor featureExtractor, GridWorldLogic logic) {
        List<GridWorldState> stateList = getStatePermutations(state);

        Map<GameState, Map<Action, Double>> tabularPolicy = new HashMap<>();

        for (GridWorldState reachableState : stateList) {
            tabularPolicy.put(reachableState, new HashMap<>());

            List<Action> validActions = logic.generateActions(reachableState, GridWorldState.DEFAULT_FIRST_PLAYER);

            for (Action a : validActions) {
                double value = functionApproximation.get(new double[][]{featureExtractor.buildFeatures(reachableState, a, logic)})[0];
                tabularPolicy.get(reachableState).put(a, value);
            }
//            for (int i = 0; i < actions.length; i++) {
//                double value = functionApproximation.get(new double[][]{featureExtractor.buildFeatures(reachableState, actions[i], logic)})[0];
//                tabularPolicy.get(reachableState).put(actions[i], value);
//            }
        }

        return buildUsingPolicyTable(state, tabularPolicy);
    }

    private static List<GridWorldState> getStatePermutations(GridWorldState state) {
        List<GridWorldState> stateList = new ArrayList<>();

        Coord playerCoord = state.findPlayerCoords(GridWorldState.DEFAULT_FIRST_PLAYER);

        GridWorldState stateClone = state.deepCopy();
        stateClone.board[playerCoord.getY()][playerCoord.getX()] = GridWorldState.CELL_REACHABLE;

        // Iterate over all reachable positions on the board, and get the corresponding state.
        for (int i = 0; i < stateClone.getHeight(); i++) {
            for (int j = 0; j < stateClone.getWidth(); j++) {
                if (stateClone.board[i][j] == GridWorldState.CELL_REACHABLE) {
                    GridWorldState newState = stateClone.deepCopy();
                    newState.board[i][j] = GridWorldState.DEFAULT_FIRST_PLAYER;
                    stateList.add(newState);
                }
            }
        }

        return stateList;
    }

    public static String buildUsingExternalNeuralNetwork(GridWorldState state, ExternalNeuralNetwork externalNeuralNetwork, StateFeatureExtractor featureExtractor, GridWorldLogic logic) {
        List<GridWorldState> stateList = getStatePermutations(state);

        Map<GameState, Map<Action, Double>> qTable = new HashMap<>();

        for (GridWorldState reachableState : stateList) {
            Map<Action, Double> actionValues = externalNeuralNetwork.getActionValues(featureExtractor.buildTensor(reachableState, logic));

            qTable.put(reachableState, actionValues);
        }

        return buildUsingPolicyTable(state, qTable);
    }
}
