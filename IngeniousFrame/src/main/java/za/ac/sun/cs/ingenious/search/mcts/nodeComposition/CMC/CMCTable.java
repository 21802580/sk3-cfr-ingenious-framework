package za.ac.sun.cs.ingenious.search.mcts.nodeComposition.CMC;

import za.ac.sun.cs.ingenious.core.Action;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CMCTable {

    public ConcurrentHashMap<Action, StoredActions> moveCombinationScores;

    /**
     * Constructor to initialise the hashmap
     */
    public CMCTable() {
        this.moveCombinationScores = new ConcurrentHashMap<>();
    }

    /**
     * refresh the hashmap to a clean empty hashmap
     */
    public void resetVisitedMoves() {
        moveCombinationScores = new ConcurrentHashMap<>();
    }

    /**
     * @return the hashmap of move combinations and scores
     */
    public ConcurrentHashMap<Action, StoredActions> getMoveCombinationScores() {
        return moveCombinationScores;
    }

    /**
     * Initialise a new stored action to be used in the hasmap to hold one to many correspondences
     * @param action the action relating to one of the many actions corresponding to one hashmap entry
     * @param win a boolean indicating if the combination for which this storedAction relates yielded a win or not
     * @return the stored action
     */
    public StoredActions createNewStoredAction(Action action, boolean win) {
        return new StoredActions(action, win);
    }

    /**
     * A stored action which holds all actions relating to the single action index in the hasmap. This holds the
     * 'many' part of the one-to-many correspondence.
     */
    public class StoredActions {

        private final ReadWriteLock lock = new ReentrantReadWriteLock();
        private final Lock writeLock = lock.writeLock();
        private final Lock readLock = lock.readLock();

        public ArrayList<Action> actions = new ArrayList<>();
        private Hashtable<Action, Double> visitCounts = new Hashtable<>();
        private Hashtable<Action, Double> winCounts = new Hashtable<>();

        /**
         * Constructor initialising values according to the win boolean
         * @param action
         * @param win
         */
        public StoredActions(Action action, boolean win) {
            // add the first of a few possible actions
            actions.add(action);
            visitCounts.put(action, 1.0);
            if (win) {
                winCounts.put(action, 1.0);
            } else {
                winCounts.put(action, 0.0);
            }
        }

        /**
         * sets the read lock for this stored action object
         */
        public void setReadLock() {
            readLock.lock();
        }

        /**
         * unsets the read lock for this stored action object
         */
        public void unsetReadLock() {
            readLock.unlock();
        }

        /**
         * sets the write lock for this stored action object
         */
        public void setWriteLock() {
            writeLock.lock();
        }

        /**
         * unsets the write lock for this stored action object
         */
        public void unsetWriteLock() {
            writeLock.unlock();
        }

        /**
         * increments the number of times this action has been played in combination with the actions used
         * to index this object the hashmap by 1.
         * @param action
         */
        public void incVisitCount(Action action) {
            visitCounts.replace(action, visitCounts.get(action) + 1);
        }

        /**
         * @param action
         * @return the number of playouts that have taken place where this action has been played in combination
         * with the actions used to index this object.
         */
        public double getVisitCount(Action action) {
            return visitCounts.get(action);
        }

        /**
         * increments the number of winning playouts where this action has been played in combination with the actions used
         * to index this object the hashmap by 1.
         * @param action
         */
        public void incWins(Action action) {
            winCounts.replace(action, winCounts.get(action) + 1);
        }

        /**
         * @param action
         * @return the number of winning playouts that have taken place where this action has been played in combination
         * with the actions used to index this object.
         */
        public double getWins(Action action) {
            return winCounts.get(action);
        }

        /**
         * Add a new action to the list of action relating to the 'many' part of the one-to-many correspondence
         * @param action
         */
        public void addNewAction(Action action) {
            actions.add(action);
            visitCounts.put(action, 0.0);
            winCounts.put(action, 0.0);
        }

        /**
         * @return returns the list of actions relating to the 'many' part of the one-to-many correspondence
         */
        public ArrayList<Action> getActions() {
            return actions;
        }
    }

}
