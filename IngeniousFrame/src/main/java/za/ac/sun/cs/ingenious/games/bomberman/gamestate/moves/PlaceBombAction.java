package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.Action;

public class PlaceBombAction implements Action {

	private static final long serialVersionUID = 1L;

	private int playerID;

	public PlaceBombAction(int playerID) {
		this.playerID = playerID;
	}

	@Override
	public int getPlayerID() {
		return playerID;
	}

	@Override
	public String toString(){
		return "bomb";
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass())) {
			return false;
		} else {
			return (this.playerID == ((PlaceBombAction) obj).playerID);
		}
	}

	@Override
	public int hashCode() {
		return playerID;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
