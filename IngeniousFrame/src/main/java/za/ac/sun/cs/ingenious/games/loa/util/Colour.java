package za.ac.sun.cs.ingenious.games.loa.util;

import java.io.Serializable;

public enum Colour implements Serializable {
    EMPTY("."),
    BLACK("B"),
    WHITE("W");

    private final String stringRep;

    Colour(String stringRep) {
        this.stringRep = stringRep;
    }

    @Override
    public String toString() {
        return stringRep;
    }

    public Colour getOpponent() {
        switch (this) {
            case WHITE:
                return Colour.BLACK;
            case BLACK:
                return Colour.WHITE;
            default:
                return Colour.EMPTY;
        }
    }

    public int toInt() {
        switch (this) {
            case WHITE:
                return 0;
            case BLACK:
                return 1;
            default:
                return 2;
        }
    }

    public static Colour fromInt(int colour) {
        switch (colour) {
            case 0:
                return WHITE;
            case 1:
                return BLACK;
            default:
                return EMPTY;
        }
    }
}
