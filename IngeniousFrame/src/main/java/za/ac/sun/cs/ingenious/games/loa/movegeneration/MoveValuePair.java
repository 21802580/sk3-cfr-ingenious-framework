package za.ac.sun.cs.ingenious.games.loa.movegeneration;

import za.ac.sun.cs.ingenious.games.loa.util.LOAMove;

import java.io.Serializable;

public class MoveValuePair implements Comparable, Serializable {
	private final LOAMove move;
	private final double value;
	
	public MoveValuePair(LOAMove move, double value){
		this.move = move;
		this.value = value;
	}
	
	public LOAMove getMove() {
		return move;
	}
	
	public double getValue() {
		return value;
	}
	
	public String toString(){
		return move + " " + value;
	}

	@Override
	public int compareTo(Object o) {
		MoveValuePair other = (MoveValuePair)o;
		if (this.getValue() > other.getValue()){
			return 1;
		} else if (this.getValue() < other.getValue()){
			return -1;
		} else {
			return 0;
		}
	}
}
