package za.ac.sun.cs.ingenious.games.cardGames.core.actions;

import za.ac.sun.cs.ingenious.games.cardGames.core.card.Card;

/**
 * The Class PlayCardMove.
 *
 * A PlayCardMove is sent when a player actively moves a certain card from an old location (e. g. him self) to a new location (e. g. the discard pile).
 *
 * @param <Location> The generic location enum
 *
 * @author Joshua Wiebe
 */
public class PlayCardAction<Location extends Enum<Location>> extends CardAction<Location>{

	/** The serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new play card move.
	 *
	 * @param player Current player
	 * @param card The card which changes location
	 * @param oldLoc the old location
	 * @param newLoc the new location
	 */
	public PlayCardAction(int player, Card card, Location oldLoc, Location newLoc) {
		super(player, card, oldLoc, newLoc);
	}
}
