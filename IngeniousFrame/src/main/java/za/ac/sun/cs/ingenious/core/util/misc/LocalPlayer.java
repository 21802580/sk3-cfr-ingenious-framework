package za.ac.sun.cs.ingenious.core.util.misc;

// import java.lang.ClassCastException;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;

/**
 * Can be used to add engines as players to the referee that run locally in the same process. Basically,
 * this just wraps the {@link Engine} method into the {@link PlayerRepresentation} interface.
 */
public class LocalPlayer implements PlayerRepresentation {

	protected Engine engine;

	public LocalPlayer(Engine engine) {
		this.engine = engine;
	}

	@Override
	public int getID() {
		return engine.getPlayerID();
	}

	@Override
	public void initGame(InitGameMessage a) {
		engine.receiveInitGameMessage(a);
	}

	@Override
	public void playMove(PlayedMoveMessage a) {
		engine.receivePlayedMoveMessage(a);
	}

	@Override
	public PlayActionMessage genAction(GenActionMessage a) {
		return engine.receiveGenActionMessage(a);
	}

	@Override
	public void terminateGame(GameTerminatedMessage a) {
		engine.receiveGameTerminatedMessage(a);
	}

	@Override
	public void resetMatch(MatchResetMessage a) {
		engine.receiveMatchResetMessage(a);
	}

	@Override
	public void reward(RewardMessage a) {
		engine.receiveRewardMessage(a);
	}

	@Override
	public String toString() {
		return "LocalPlayer, id: " + getID() + ", running engine " + engine.engineName();
	}

}
