package za.ac.sun.cs.ingenious.search.ismcts;

import static java.lang.Math.sqrt;

import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SearchValue;

/**
 * This implements the UCT formula for subset multi-armed bandit. In the modified UCT formula,
 * the parent visit count becomes the availability count of this node.
 */
public class SubsetUCT<S extends TurnBasedGameState> implements SearchValue<S,ISMCTSNode<S>> {

	private final double C;
	
	/**
	 * @param c The constant in the UCT search value formula
	 */
	public SubsetUCT(double c) {
		C = c;
	}

	/**
	 * Sets C to a commonly used value near 0.7
	 */
	public SubsetUCT() {
		this(1.0/sqrt(2.0));
	}
	
	@Override
	public double get(ISMCTSNode<S> node, int forPlayerID, int totalNofPlayouts) {
		double availabilityCount = node.getAvailabilityCount();		
		double barX = node.getReward()[forPlayerID]/node.getVisits();		
		double uncertainty  = 2.0*C*sqrt(2.0*Math.log(availabilityCount)/node.getVisits());		
		return barX+uncertainty;
	}
}
