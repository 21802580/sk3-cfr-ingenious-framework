package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.monteCarlo.TabularMonteCarloOffPolicy;

import java.util.Map;
import java.util.Random;

/**
 * Monte Carlo (off-policy) engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldMonteCarloOffPolicyEngine extends GridWorldEngine {
    private final double GAMMA = 1.0;

    TabularMonteCarloOffPolicy<GridWorldState, GridWorldLogic> alg;

    public GridWorldMonteCarloOffPolicyEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new TabularMonteCarloOffPolicy<>("GridWorldMonteCarloOffPolicyEngine_" + playerID, GAMMA);
        alg.displayChart();
    }

    public void setDeterministic() {
        alg.withRandom(new Random(8234L));
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldMonteCarloOffPolicyEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        alg.setLogic(logic);
        alg.setPlayerID(playerID);
        alg.beginNewEpisode();
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = alg.chooseActionFromBehaviouralPolicy(state);
        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        alg.setMostRecentReward(a.getReward());
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.update();
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicy(state, alg.getPolicy()));
        alg.printMetrics();

        Map<GameState, Map<Action, Double>> qTable = alg.getQTable();
        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicyTable(state, qTable));
        alg.close();
    }

    public TabularMonteCarloOffPolicy<GridWorldState, GridWorldLogic> getAlg() {
        return alg;
    }
}
