package za.ac.sun.cs.ingenious.games.snake.moves;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;

/**
 * Action used to indicate the current state of stochastic elements that may have changed e.g. food spawn.
 *
 * @author Steffen Jacobs
 */
public class SnakeSyncAction implements Action {
    private static final long serialVersionUID = 1L;

    private int playerID;
    private Coord food;

    public SnakeSyncAction(int playerID, Coord food) {
        this.playerID = playerID;
        this.food = food;
    }

    @Override
    public Action deepCopy() {
        return new SnakeSyncAction(playerID, new Coord(food));
    }

    @Override
    public int getPlayerID() {
        return playerID;
    }

    public Coord getFood() {
        return food;
    }
}
