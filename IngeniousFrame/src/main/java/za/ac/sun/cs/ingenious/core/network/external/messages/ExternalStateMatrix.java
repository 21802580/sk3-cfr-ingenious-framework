package za.ac.sun.cs.ingenious.core.network.external.messages;

/**
 * Wraps a set of matrices to be sent to the Python application.
 *
 * @author Steffen Jacobs
 */
public class ExternalStateMatrix {
    private final static String TYPE_STR = "StateMatrix";
    private String type;
    private double[][][] data;

    public ExternalStateMatrix(double[][][] state) {
        this.type = TYPE_STR;
        this.data = state;
    }

    public ExternalStateMatrix(double[][] state) {
        this.type = TYPE_STR;

        if (state == null) {
            this.data = null;
        } else {
            this.data = new double[][][]{state};
        }
    }

    public ExternalStateMatrix(double[] state) {
        this.type = TYPE_STR;

        if (state == null) {
            this.data = null;
        } else {
            this.data = new double[][][]{new double[][] {state}};
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double[][][] getData() {
        return data;
    }

    public void setData(double[][][] data) {
        this.data = data;
    }
}
