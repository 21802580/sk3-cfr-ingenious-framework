package za.ac.sun.cs.ingenious.search.rl.monteCarlo;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameLogic;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.Reward;
import za.ac.sun.cs.ingenious.core.exception.probability.IncorrectlyNormalizedDistributionException;
import za.ac.sun.cs.ingenious.core.util.ClassUtils;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.util.EpisodeStep;
import za.ac.sun.cs.ingenious.search.rl.util.ReinforcementLearningInstrumentation;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * Class containing an implementation of Monte Carlo that maintains a target policy separate to its behavioural policy,
 * using weighted importance sampling for target policy updates.
 *
 * @param <S>
 * @param <L>
 * @author Steffen Jacobs
 */
public class TabularMonteCarloOffPolicy<S extends GameState, L extends GameLogic<S>> extends ReinforcementLearningInstrumentation {
    private static final double DEFAULT_GAMMA = 1.0;
    private static final double INITIAL_RETURN = 0.0;
    public static final double INITIAL_C_VALUE = 0.0;

    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_WIDTH = 5;
    /**
     * Used for logging grid.
     */
    private static final int MDP_GRID_HEIGHT = 5;

    /**
     * Game logic in order to retrieve list of available actions for any given state.
     */
    private L logic;

    /**
     * ID of the player using this MC instance.
     */
    private int playerID;

    /**
     * Random number generator.
     */
    private Random rng;

    /**
     * Discount factor.
     */
    private double gamma;

    /**
     * Target policy being improved on.
     */
    private Map<GameState, Action> policy;

    /**
     * Behavioural policy.
     */
    private Map<GameState, Map<Action, Double>> b;

    /**
     * Table containing averaged returns for state-action pairs.
     */
    private Map<GameState, Map<Action, Double>> qTable;

    private Map<GameState, Map<Action, Double>> cTable;

    /**
     * List of episode steps in the current episode.
     */
    private List<EpisodeStep> steps;

    public TabularMonteCarloOffPolicy() {
        rng = new Random();
        gamma = DEFAULT_GAMMA;

        policy = new HashMap<>();
        b = new HashMap<>();
        qTable = new HashMap<>();
        cTable = new HashMap<>();
        steps = new ArrayList<>();
    }

    public TabularMonteCarloOffPolicy(String identifier, double gamma) {
        super(identifier);

        rng = new Random();
        this.gamma = gamma;

        policy = new HashMap<>();
        b = new HashMap<>();
        qTable = new HashMap<>();
        cTable = new HashMap<>();
        steps = new ArrayList<>();
    }

    public TabularMonteCarloOffPolicy<S, L> withGamma(double gamma) {
        this.gamma = gamma;
        return this;
    }

    public TabularMonteCarloOffPolicy<S, L> withRandom(Random random) {
        this.rng = random;
        return this;
    }

    public TabularMonteCarloOffPolicy<S, L> withLogic(L logic) {
        this.setLogic(logic);
        return this;
    }

    public void setLogic(L logic) {
        this.logic = logic;
    }

    public void setPlayerID(int playerID) {
        this.playerID = playerID;
    }

    public void setQTable(Map<GameState, Map<Action, Double>> qTable) {
        this.qTable = qTable;
    }

    public void setSteps(List<EpisodeStep> steps) {
        this.steps = steps;
    }

    public void setCTable(Map<GameState, Map<Action, Double>> cTable) {
        this.cTable = cTable;
    }

    public List<EpisodeStep> getSteps() {
        return steps;
    }

    public Map<GameState, Map<Action, Double>> getB() {
        return b;
    }

    public Map<GameState, Action> getPolicy() {
        return policy;
    }

    public Map<GameState, Map<Action, Double>> getQTable() {
        return qTable;
    }

    public Map<GameState, Map<Action, Double>> getCTable() {
        return cTable;
    }

    public void beginNewEpisode() {
        steps = new ArrayList<>();
        b = new HashMap<>();

    }

    public Action chooseActionFromBehaviouralPolicy(S state) {
        incrementStep();

        GameState clonedState = state.deepCopy();
        performLazyBehaviouralPolicyInitialization(clonedState);

        Action choice = null;
        try {
            choice = ClassUtils.chooseFromDistribution(b.get(clonedState), rng);
        } catch (IncorrectlyNormalizedDistributionException e) {
            e.printStackTrace();
        }

        steps.add(new EpisodeStep(clonedState, choice));

        return choice;
    }

    /**
     * Greedily chooses the best action from the available actions with the greatest value in the Q table corresponding
     * to Q(state, *). Ties are broken consistently.
     *
     * @param state State from which the action is taken.
     * @return The greedy action choice.
     */
    public Action greedyActionForState(GameState state) {
        Map<Action, Double> qs = qTable.get(state);
        Set<Action> actions = qs.keySet();

        double bestQ = -Double.MAX_VALUE;
        Action bestAction = null;

        /*
         * Iterate over actions and get the actions with the best value. By default, ties are consistently broken with
         * first taking priority.
         */
        for (Action a : actions) {
            double q = qs.get(a);

            if (q > bestQ) {
                bestQ = q;
                bestAction = a;
            }
        }

        return bestAction;
    }

    /**
     * Sets the reward for the most recent episode step. This is necessary since there is a delay between performing
     * the action and receiving the corresponding reward.
     *
     * @param reward Reward to set.
     */
    public void setMostRecentReward(Reward reward) {
        addReward(((ScalarReward) reward).getReward());
        steps.get(steps.size() - 1).setReward(reward);
    }

    /**
     * Performs lazy initialization of the behavioural policy for the given state. All action probabilities for the
     * state are initialized with random probability.
     *
     * @param state State for which the policy is initialized.
     */
    public void performLazyBehaviouralPolicyInitialization(GameState state) {
        List<Action> availableActions = logic.generateActions((S)state, playerID);

        double[] randomWeights = new double[availableActions.size()];
        double weightSum = 0.0;

        // Generate random number for each action.
        for (int i = 0; i < randomWeights.length; i++) {
            randomWeights[i] = rng.nextDouble();
            weightSum += randomWeights[i];
        }

        if (!b.containsKey(state))
            b.put(state, new HashMap<>());

        // Assign value used as a probability to each action in b(S,*).
        if (b.get(state).keySet().size() == 0) {
            int i = 0;
            for (Action a : availableActions) {
                b.get(state).put(a, randomWeights[i++] / weightSum);
            }
        }
    }

    /**
     * Updates the target policy, using steps from the current episode. This method should be called at the end of
     * an episode.
     */
    public void update() {
        totalEpisodes++;

        double g = 0;
        double w = 1;

        for (int t = steps.size() - 1; t >= 0; t--) {
            EpisodeStep currentStep = steps.get(t);
            GameState state = currentStep.getState();
            Action action = currentStep.getAction();
            ScalarReward reward = (ScalarReward) currentStep.getReward();

            // Some lazy initialization of maps.
            performLazyInitialization(state, action);
            performLazyBehaviouralPolicyInitialization(state);

            // Update g with new reward.
            g = gamma * g + reward.getReward();
            cTable.get(state).put(action, cTable.get(state).get(action) + w);

            double qsa = qTable.get(state).get(action) != null ? qTable.get(state).get(action) : 0;

            // Update Q Table
            double newQValue = qsa + (w / cTable.get(state).get(action)) * (g - qsa);
            qTable.get(state).put(action, newQValue);


            // Choose max action in Q(S, *), set target policy action to this.
            Action greedyAction = greedyActionForState(state);
            policy.put(state, greedyAction);

            if (!action.equals(greedyAction)) break;
            w = w * (1 / b.get(state).get(action));
        }

//        printPolicy();
//        printPolicyGrid(MDP_GRID_WIDTH, MDP_GRID_HEIGHT);
    }

    /**
     * Performs lazy initialization of maps for the specified state, action and reward.
     *
     * @param state State.
     * @param action Action.
     */
    private void performLazyInitialization(GameState state, Action action) {
        if (!cTable.containsKey(state))
            cTable.put(state, new HashMap<>());

        if (!cTable.get(state).containsKey(action))
            cTable.get(state).put(action, INITIAL_C_VALUE);

        if (!qTable.containsKey(state))
            qTable.put(state, new HashMap<>());

        // Initialize returns arbitrarily
        Map<Action, Double> actionReturnsForState = qTable.get(state);
        if (actionReturnsForState.keySet().size() == 0) {
            List<Action> actions = logic.generateActions((S)state, playerID);
            for (Action a : actions) {
                qTable.get(state).put(a, rng.nextDouble());
            }

            // Initialize policy for state, having initialized qTable arbitrarily.
            policy.put(state, greedyActionForState(state));
        }

    }

    public void printPolicy() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        Set<GameState> states = policy.keySet();
        for (GameState state : states) {
            sb.append(state).append("\n");
            sb.append("\t").append(policy.get(state)).append("\n");
        }

        sb.append("\n");
        Log.info(sb);
    }

    public void printQPolicy() {
        StringBuilder sb = new StringBuilder();
        sb.append("\n");

        Set<GameState> states = qTable.keySet();
        for (GameState state : states) {
            sb.append(state).append("\n");
            Set<Action> actions = qTable.get(state).keySet();

            for (Action action : actions) {
                sb.append("\t").append(action).append(" : ").append(qTable.get(state).get(action)).append("\n");
            }
        }

        sb.append("\n");
        Log.info(sb);
    }

    /**
     * This method is rather hardcoded, but for the moment is used to print gridworld MDPs.
     *
     * @param w
     * @param h
     */
    public void printPolicyGrid(int w, int h) {
        Log.info("Q Table: \n");
        Set<GameState> keySet = policy.keySet();

        DecimalFormat df = new DecimalFormat("0.00");
        StringBuilder sb = new StringBuilder().append("\n");

        long k = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                MDPState<Long> state = new MDPState<>(k++);

                Action a = policy.get(state);

                if (a == null) {
                    sb.append("• ");
                    continue;
                }

                switch (((DiscreteAction) a).getActionNumber()) {
                    case 0:
                        sb.append("↑ ");
                        break;
                    case 1:
                        sb.append("→ ");
                        break;
                    case 2:
                        sb.append("↓ ");
                        break;
                    case 3:
                        sb.append("← ");
                        break;
                }
            }
            sb.append("\n");
        }

        sb.append("\n\n");

        Log.info(sb);
        Log.info("==========================");
    }
}
