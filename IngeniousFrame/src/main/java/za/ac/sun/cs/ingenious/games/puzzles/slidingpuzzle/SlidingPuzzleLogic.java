package za.ac.sun.cs.ingenious.games.puzzles.slidingpuzzle;

import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.persistent.PVector;

/**
 * Class that contains logic for the sliding puzzle.
 */
public class SlidingPuzzleLogic {

    public final int width, height, size;

    public SlidingPuzzleLogic(int width, int height) {
        this.width = width;
        this.height = height;
        this.size = width * height;
    }
    
    /**
     * Determine whether a given move on a tile is within bounds.
     * @param location location of tile.
     * @param move cardinal direction of move applied to tile.
     * @return true if it is within bounds, otherwise false.
     */
    public boolean legalMove(int location, CompassDirection move) {
        switch (move) {
            case N: return toY(location) > 0;
            case E: return toX(location) < width - 1;
            case S: return toY(location) < height - 1;
            case W: return toX(location) > 0;
            default: throw new IllegalArgumentException();
        }
    }

    /**
     * Obtain the resulting location from moving a tile in a cardinal direction.
     * @param location location of tile.
     * @param move cardinal direction of move applied to tile.
     * @return location of tile after applying a move.
     */
    public int moveLocation(int location, CompassDirection move) {
        switch (move) {
            case N: return location - width;
            case E: return location + 1;
            case S: return location + width;
            case W: return location - 1;
            default: throw new IllegalArgumentException();
        } 
    }

    /**
     * Extract the x-coordinate from a given board location
     * @param location location on board
     * @return corresponding x-coordinate
     */
    public int toX(int location) {
        return location % width;
    }

    /**
     * Extract the y-coordinate from a given board location
     * @param location location on board
     * @return corresponding y-coordinate
     */
    public int toY(int location) {
        return location / width;
    }

    /**
     * convert an xy-coordinate to a location on the board
     * @param x x-coordinate
     * @param y y-coordinate
     * @return board location
     */
    public int toLocation(int x, int y) {
        return width * y + x;
    }

    /**
     * Manhattan distance heuristic
     * @param tile value of the tile
     * @param location location of the tile
     * @return the sum of the x and y distances of a tile and its goal position
     */
    public int manhattanDistance(int tile, int location) {
        return Math.abs(toX(tile) - toX(location)) + Math.abs(toY(tile) - toY(location));
    }

    /**
     * Chebyshev distance heuristic
     * @param tile value of the tile
     * @param location location of the tile
     * @return the max of the x and y distances of a tile and its goal position
     */
    public int chebyshevDistance(int tile, int location) {
        return Math.max(Math.abs(toX(tile) - toX(location)), Math.abs(toY(tile) - toY(location)));
    }

    /**
     * Return a string visual representation of a given sliding puzzle board state.
     * 
     * for example<pre>
     *[ ][1][2]
     *[3][4][5]
     *[6][7][8]
     * </pre>
     * 
     * @param board PVector of sliding puzzle board state
     * @return string visual representation
     */
    public String stringifyBoard(PVector<Integer> board) {
        StringBuilder sb = new StringBuilder();

        final int padding = String.valueOf(width * height - 1).length();
        for (int y = 0, p = 0; y < height; y++) {
            for (int x = 0; x < width; x++, p++) {
                Integer tile = board.get(p);
                String s = tile == null ? "!" : tile == 0 ? "" : String.valueOf(tile);

                sb.append('[');
                sb.append(" ".repeat(padding - s.length())); 
                sb.append(s);
                sb.append(']');
            }
            sb.append('\n');
        }
        return sb.toString();
    }
}
