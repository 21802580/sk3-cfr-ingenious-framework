package za.ac.sun.cs.ingenious.games.go.engines;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.util.move.ForfeitAction;
import za.ac.sun.cs.ingenious.core.util.search.mcts.MctsNodeSimple;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.go.GoEngine;
import za.ac.sun.cs.ingenious.games.go.GoMctsFinalEvaluator;
import za.ac.sun.cs.ingenious.search.mcts.MctsAlgorithm.MctsRoot;

import java.io.IOException;
import java.util.ArrayList;

public class GoMCTSRootEngine extends GoEngine {
    int threadCount;
    int TURN_LENGTH = 500;
    MctsRoot<TurnBasedSquareBoard, MctsNodeSimple<TurnBasedSquareBoard>> mcts;

    /**
     * @param toServer An established connection to the GameServer
     */
    public GoMCTSRootEngine(EngineToServerConnection toServer) throws MissingSettingException, IncorrectSettingTypeException, IOException {
        super(toServer);
        threadCount = 4;
        this.mcts = new MctsRoot<>(logic, new GoMctsFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), playerID, threadCount);
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        MctsNodeSimple<TurnBasedSquareBoard> root = new MctsNodeSimple<TurnBasedSquareBoard>(currentState, null, null, new ArrayList<MctsNodeSimple<TurnBasedSquareBoard>>(), logic, playerID);

        Action action = mcts.doSearch(root, TURN_LENGTH);

        if (action == null) {
            return new PlayActionMessage(new ForfeitAction((byte) playerID));
        }

        return new PlayActionMessage(action);
    }
}
