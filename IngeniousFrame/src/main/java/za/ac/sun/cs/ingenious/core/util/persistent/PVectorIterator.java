package za.ac.sun.cs.ingenious.core.util.persistent;

import java.util.Iterator;

/**
 * An iterator for PVectors, contains a few extra methods.
 * 
 * @author Nicholas Robinson
 */
public interface PVectorIterator<T> extends Iterator<T>
{
    /**
     * Obtain a new PVector by setting the item at the current index to null.
     * @return new PVector containing the a null at the current index of the iterator.
     */
    public PVector<T> erase();

    /**
     * Obtain a new PVector by setting the item at the current index to a value provided.
     * @param item value to be held at the current index.
     * @return new PVector containing the value at the current index of the iterator.
     */
    public PVector<T> set(T item);

    /**
     * @return The index of the latest item returned.
     */
    public int getIndex();
}