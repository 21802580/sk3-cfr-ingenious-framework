package za.ac.sun.cs.ingenious.core.util.state;

import com.esotericsoftware.minlog.Log;

import java.util.Arrays;

import za.ac.sun.cs.ingenious.core.GameState;

/**
 * Represents the state of a game that is played on a square board with players taking
 * turns. Useful for games like TicTacToe.
 *
 * @author Michael Krause
 */
public class TurnBasedSquareBoard extends TurnBasedGameState {

	public byte[] board;
	private int boardSize;

	/**
	 * Initializes an empty board.
	 */
	public TurnBasedSquareBoard(int boardSize, int firstPlayer, int numPlayers) {
		super(firstPlayer, numPlayers);
		this.boardSize = boardSize;
		board = new byte[this.boardSize*this.boardSize];
	}

	/**
	 * Copy constructor. Duplicates the given board.
	 */
	public TurnBasedSquareBoard(TurnBasedSquareBoard toCopy) {
		super(toCopy.nextMovePlayerID, toCopy.numPlayers);
		this.boardSize = toCopy.getBoardSize();
		this.board = toCopy.board.clone();
	}

	/**
	 * @return The size !in one direction! of the board. Example: for TicTacToe this is 3, not 9.
	 */
	public int getBoardSize() {
		return boardSize;
	}

	@Override
	public GameState deepCopy() {
		return new TurnBasedSquareBoard(this);
	}

	/**
	 * @return The index into the board array for the given x and y coordinates.
	 */
	public int xyToIndex(int x, int y){
		return y*this.boardSize+x;
	}

	@Override
	public void printPretty(){
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i =0; i < boardSize; i++){
			s.append("-");
		}
		s.append("+");
		s.append("\n");

		for (short y =0; y < boardSize; y++){
			s.append("|");
			for (short x =0; x < boardSize; x++){
				s.append(board[xyToIndex(x, y)]);
			}
			s.append("|");
			s.append("\n");
		}

		s.append("+");
		for (short i =0; i < boardSize; i++){
			s.append("-");
		}
		s.append("+");
		Log.info(s.toString());
	}

	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("\n");
		s.append("+");
		for (short i =0; i < boardSize; i++){
			s.append("-");
		}
		s.append("+");
		s.append("\n");

		for (short y =0; y < boardSize; y++){
			s.append("|");
			for (short x =0; x < boardSize; x++){
				s.append(board[xyToIndex(x, y)]);
			}
			s.append("|");
			s.append("\n");
		}

		s.append("+");
		for (short i =0; i < boardSize; i++){
			s.append("-");
		}
		s.append("+");
		return (s.toString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Arrays.hashCode(board);
		result = prime * result + boardSize;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TurnBasedSquareBoard other = (TurnBasedSquareBoard) obj;
		if (boardSize != other.boardSize)
			return false;
		if (!Arrays.equals(board, other.board))
			return false;
		return true;
	}

}
