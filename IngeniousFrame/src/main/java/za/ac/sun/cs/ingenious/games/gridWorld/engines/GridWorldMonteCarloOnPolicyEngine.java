package za.ac.sun.cs.ingenious.games.gridWorld.engines;

import com.esotericsoftware.minlog.Log;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.util.hashing.ZobristHashing;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldLogic;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.search.rl.monteCarlo.TabularMonteCarloOnPolicy;

import java.util.Map;
import java.util.Random;

/**
 * Monte Carlo (on-policy) engine for GridWorld.
 *
 * @author Steffen Jacobs
 */
public class GridWorldMonteCarloOnPolicyEngine extends GridWorldEngine {
    private final double EPSILON = 0.5;
    private final double GAMMA = 1.0;

    TabularMonteCarloOnPolicy<GridWorldState, GridWorldLogic> alg;

    public GridWorldMonteCarloOnPolicyEngine(EngineToServerConnection toServer) {
        super(toServer);
        alg = new TabularMonteCarloOnPolicy<>("GridWorldMonteCarloOnPolicyEngine_" + playerID, EPSILON, GAMMA);
        alg.displayChart();
    }

    public void setDeterministic() {
        alg.withRandom(new Random(8234L));
    }

    @Override
    public void setZobrist(ZobristHashing zobristHashing) {}

    @Override
    public String engineName() {
        return "GridWorldMonteCarloOnPolicyEngine";
    }

    @Override
    public void receiveInitGameMessage(InitGameMessage a) {
        super.receiveInitGameMessage(a);

        alg.setLogic(logic);
        alg.setPlayerID(playerID);
        alg.beginNewEpisode();
    }

    @Override
    public PlayActionMessage receiveGenActionMessage(GenActionMessage a) {
        Action choice = alg.chooseActionFromPolicy(state);

        return new PlayActionMessage(choice);
    }

    @Override
    public void receiveRewardMessage(RewardMessage a) {
        alg.setMostRecentReward(a.getReward());
    }

    @Override
    public void receiveMatchResetMessage(MatchResetMessage a) {
        super.receiveMatchResetMessage(a);
        alg.update();
    }

    @Override
    public void receiveGameTerminatedMessage(GameTerminatedMessage a) {
        super.receiveGameTerminatedMessage(a);

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicyTable(state, alg.getPolicy()));
        alg.printMetrics();

        Map<GameState, Map<Action, Double>> qTable = alg.getQTable();
        Log.info(GridWorldPolicyStringBuilder.buildStateValuesUsingPolicyTable(state, qTable));
        alg.close();
    }

    public TabularMonteCarloOnPolicy<GridWorldState, GridWorldLogic> getAlg() {
        return alg;
    }
}
