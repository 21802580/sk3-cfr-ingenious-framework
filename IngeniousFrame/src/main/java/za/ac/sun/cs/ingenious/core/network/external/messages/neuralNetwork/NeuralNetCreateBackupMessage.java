package za.ac.sun.cs.ingenious.core.network.external.messages.neuralNetwork;

import za.ac.sun.cs.ingenious.core.network.external.messages.ExternalMessage;
import za.ac.sun.cs.ingenious.core.network.external.messages.type.ExternalMessageType;


/**
 * Used to notify the Python application to create a backup of the DQN network using the provided identifier.
 *
 * @author Steffen Jacobs
 */
public class NeuralNetCreateBackupMessage extends ExternalMessage {
    String payload;

    public NeuralNetCreateBackupMessage(String identifier) {
        super(ExternalMessageType.NEURAL_NET_CREATE_BACKUP);

        payload = identifier;
    }
}
