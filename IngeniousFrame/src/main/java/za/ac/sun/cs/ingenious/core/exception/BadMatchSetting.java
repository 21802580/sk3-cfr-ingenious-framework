package za.ac.sun.cs.ingenious.core.exception;

/**
 * Thrown by MatchSetting.checkConfiguration if settings have an invalid value.
 */
public class BadMatchSetting extends Exception {
	private static final long serialVersionUID = 1L;

	public BadMatchSetting(String msg) {
        super(msg);
    }
}
