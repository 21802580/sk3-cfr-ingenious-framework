package za.ac.sun.cs.ingenious.core.util.move;

import za.ac.sun.cs.ingenious.core.Action;

/**
 * An Action indicating a player has forfeit.
 *
 * @author Rudolf Stander
 */
public class ForfeitAction implements Action {

	/* For serialization. */
	private static final long serialVersionUID = 1L;
	/* The player who has forfeit. */
	private byte player;

	/**
	 * Constructs a new ForfeitAction object.
	 *
	 * @param player	the player who has forfiet
	 */
	public ForfeitAction(byte player) {
		this.player = player;
	}

	/**
	 * Get the player who has forfeit.
	 *
	 * @return	the player who has forfeit
	 */
	@Override
	public int getPlayerID() {
		return player;
	}

	public String toString() {
		return "Action: ForfeitAction, Player: "+player;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
