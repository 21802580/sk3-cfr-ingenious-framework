package za.ac.sun.cs.ingenious.search.sss;

import java.util.List;

import za.ac.sun.cs.ingenious.core.util.nodes.trees.TreeNode;
import za.ac.sun.cs.ingenious.search.sss.Descriptor.Status;

/**
 * An implementation of the SSS* algorithm, (Stockmans State Space Search).
 * It is a best first algorithm for searching minimax trees. Assumes a 2-player turn based game.
 * 
 * <p> The algorithm works by interacting directly with instances of an abstract class 
 * called {@code SearchNode}, which is used to represent nodes in a game tree.
 * Should a user wish to incorporate the SSS* search in their game engine then they will 
 * have to create a class that extends the {@code SSS.SearchNode} class.
 * 
 * @author Nicholas Robinson
 */
public final class SSS {
    private SSS() {}

    public static abstract class SearchNode<SELF extends SearchNode<SELF>> implements TreeNode<SELF>, TreeNode.Bidirectionality<SELF> {

        Descriptor descriptor = null;
        int latestChild = -1;
        boolean active = false;

        /**
         * Does this node represent a MAX node?
         * I.e. does this node represent a game state where it is the turn to move 
         * for the player who's score we are trying to maximise.
         * 
         * @return true if this node is a MAX node.
         */
        public abstract boolean isMaximiser();

        /**
         * Does this node represent a MIN node?
         * I.e. does this node represent a game state where it is the turn to move 
         * for the opponent who's score we are trying to minimise.
         * 
         * @return true if this node is a MIN node.
         */
        public boolean isMinimiser() {
            return !isMaximiser();
        }

        /**
         * Does this node represent a terminal state?
         * Do we not wish to search past this node? (e.g. node is at max search depth limit)
         * 
         * @return true if this node is a leaf node.
         */
        public abstract boolean isTerminal();

        /**
         * Was this node part of the originally provided list of roots?
         * The first node to return true on this function represents the "best action" for our player to take.
         * 
         * @return true if this node is a root node.
         */
        public abstract boolean isRoot();

        /**
         * Return an evaluation of the game state represented by this node, from the perspective of the MAX player.
         * 
         * @return evaluation of state
         */
        public abstract int evaluate();

        /**
         * This method is called to indicate that this node (and all of its descendents) will no longer be visited.
         * It is advised that this method be overriden to dereference the children of this node in order to free up the
         * memory that is associated with the subtree rooted at this node.
         */
        public abstract void free();

        
        // unnecessary methods that do not need to be overriden

        @Override
        public void setParent(SELF parent) {}

        @Override
        public Iterable<SELF> children() {
            throw new UnsupportedOperationException();
        }

        @Override
        public boolean appendChild(SELF child) {
            throw new UnsupportedOperationException();
        }

        @Override
        public SELF removeChild(int index) {
            throw new UnsupportedOperationException();
        }

        @Override
        public SELF removeChild(SELF child) {
            throw new UnsupportedOperationException();
        }

        @Override
        public SELF setChild(int index, SELF replacement) {
            throw new UnsupportedOperationException();
        }

        @Override
        public SELF replaceChild(SELF child, SELF replacement) {
            throw new UnsupportedOperationException();
        }

        public SELF clone() {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * An object that contains the results of an SSS* search, returned by the {@code compute} operation.
     */
    public static class SearchResult<N extends SearchNode<N>> {
        
        private final N node;
        private final int score;
        private final int iterations;

        /**
         * The true value of {@code node}.
         */
        public N getRoot() {
            return node;
        }

        /**
         * The number of iterations performed during the SSS* search
         */
        public int getScore() {
            return score;
        }

        /**
         * The root node corresponding to the best action for the player to take.
         */
        public int getIterations() {
            return iterations;
        }

        SearchResult(N node, int score, int iterations) {
            this.node = node;
            this.score = score;
            this.iterations = iterations;
        }
    }


    public static <N extends SearchNode<N>> SearchResult<N> compute(List<N> roots) {
        return compute(roots, Integer.MIN_VALUE);
    }

    /**
     * Performs the SSS* search on a list of roots. The {@code SearchResult} returned by this function will contain the 
     * root which produced the best score.
     * 
     * @param <N> Game tree nodes that extend {@code SSS.SearchNode}.
     * @param roots A list of nodes, corresponding to move trees to be evaluated within the same function call.
     * @param bound Used for pruning. The search will terminate early if it can no longer guaruntee a score that is greater than the bound.
     * Default value should be -infinity.
     * @return A {@code SearchResult} containing the results of the search.
     */
    @SuppressWarnings("unchecked")
    public static <N extends SearchNode<N>> SearchResult<N> compute(List<N> roots, int bound) {
        
        OpenQueue open = new OpenQueue();
        for (SearchNode<?> root : roots) {
            open.put(new Descriptor(root, Status.LIVE, Integer.MAX_VALUE));
        }

        int iterations = 0;
        while (true) {
            iterations++;
            Descriptor p = open.poll();

            if (p.merit <= bound) {
                return null;
            }

            switch (p.status) {
                case LIVE: {
                    if (p.node.isTerminal()) {
                        open.put(p.reuse(p.node, Status.SOLVED, Math.min(p.merit, p.node.evaluate())));
                    }
                    else if (p.node.isMaximiser()) {
                        p.node.latestChild = p.node.childCapacity() - 1;
                        for (int c = 0; c <= p.node.latestChild; c++) {
                            open.put(new Descriptor(p.node.getChild(c), Status.LIVE, p.merit));
                        }
                    }
                    else {
                        p.node.latestChild = 0;
                        open.put(p.reuse(p.node.getChild(0), Status.LIVE, p.merit));
                    }
                    break;
                }
                case SOLVED: {
                    if (p.node.isRoot()) {
                        return new SearchResult<N>((N)p.node, p.merit, iterations);
                    }

                    SearchNode<?> parent = p.node.getParent();

                    if (p.node.isMinimiser()) {
                        open.purgeAllDescendentsOf(parent);
                        p.node.free();
                        open.put(p.reuse(parent, Status.SOLVED, p.merit));
                    }
                    else if (parent.latestChild < parent.childCapacity() - 1) {
                        p.node.active = false;
                        p.node.free();
                        open.put(p.reuse(parent.getChild(++parent.latestChild), Status.LIVE, p.merit));
                    }
                    else {
                        p.node.active = false;
                        p.node.free();
                        open.put(p.reuse(parent, Status.SOLVED, p.merit));
                    }
                    break;
                }
            }
        } 
    }
    
}