package za.ac.sun.cs.ingenious.core.network.game;

/**
 * This class is used as a dummy EngineToServerConnection implementation that does not maintain a socket connection.
 * Using this class is mainly applicable in JUnit tests for algorithms, a scenario in which a socket connection is not
 * applicable. This avoids null pointer exceptions that would arise from an Engine invoking methods in the standard
 * implementation of EngineToServerConnection.
 *
 * @author Steffen Jacobs
 */
public class DummyEngineToServerConnection extends EngineToServerConnection {

    public DummyEngineToServerConnection(int playerID) {
        super(null, null, null, playerID);
    }

    @Override
    public void closeConnection() {
        // Do nothing, as there is no connection to close.
    }
}
