package za.ac.sun.cs.ingenious.core.util.persistent.tries;

import java.util.Iterator;

import za.ac.sun.cs.ingenious.core.util.nodes.trees.BinaryTreeNode;
import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.persistent.PVectorIterable;
import za.ac.sun.cs.ingenious.core.util.persistent.PVectorIterator;

/**
 * A generic functional data structure that implements the {@link PVector} interface.
 * 
 * <p> Internally this structure is a dynamically growing binary trie that uses the path copying
 * technique in order to gain persistence.
 * A brand new PBitOrderedTrie will have use space that is linear to the number of items stored in it, while 
 * new versions of the structure (obtained by modifying the structure with a set-method) will use space that 
 * is logarithmic to each modification.
 * 
 * <p> The PBitOrderedTrie is highly optimized in terms of space efficiency and speed. It is also inherently thread safe.
 * 
 * <p> The indices that correspond to nodes are not stored in the nodes themselves (with the exception of root nodes).
 * Instead they are decided by the path taken to reach it. The nodes are ordered in a sort of manner where their respective 
 * indices would correspond to a perfectly sorted binary search tree, thus an in-order traversal on the trie will visit the nodes
 * in ascending order of their indices.
 * 
 * <p> Note of caution: this object also acts as the root node of its binary trie, this is an intentional design decision 
 * which was made in order to further reduce the memory footprint of this data structure. The drawback is that it unavoidably
 * exposes methods provided by the TreeNode interfaces (although this may be desireable for debugging purposes). In order to
 * hide these methods, it is advised that the user references an instance of the PBitOrderedTrie as a {@link PVector}.
 * 
 * @author Nicholas Robinson
 */
public class PBitOrderedTrie<T> extends BitNode<T> implements PVector<T> {
    
    private int rootIndex;

    private PBitOrderedTrie(int rootIndex, BitNode<T> left, BitNode<T> right, T item) {
        this.rootIndex = rootIndex;
        this.left = left;
        this.right = right;
        this.item = item;
    }

    private PBitOrderedTrie(PBitOrderedTrie<T> toCopy) {
        rootIndex = toCopy.rootIndex;
        left = toCopy.left;
        right = toCopy.right;
        item = toCopy.item;
    }

    private PBitOrderedTrie(int rootIndex) {
        this.rootIndex = rootIndex;
    }

    /**
     * Constructs an empty PBitOrdredTrie
     */
    public PBitOrderedTrie() {
        rootIndex = 0;
    }

    /**
     * Constructs a PBitOrderedTrie containing the items of an {@link Iterable} element.
     * 
     * The index of the first item starts at 0, subsequent items are indexed in the order that they
     * are returned by the iterator.
     * 
     * @param iterable Iterable element to iterate over a set of items
     */
    public PBitOrderedTrie(Iterable<T> iterable) {
        this(iterable.iterator());
    }

    /**
     * Constructs a PBitOrderedTrie containing the items of an iterable element.
     * 
     * The index of the first item starts at 0, subsequent items are indexed in the order that they
     * are returned by the iterator.
     * 
     * @param iterable iterator to iterate over a set of items
     */
    public PBitOrderedTrie(Iterator<T> itr) {

        class Builder {
            void descend(BitNode<T> node, int height){
                if (itr.hasNext() && height > 1) {
                    node.setLeft(new BitNode<T>());
                    descend(node.getLeft(), height - 1);
                }
            
                if (itr.hasNext()) node.item = itr.next();

                if (itr.hasNext() && height > 1) {
                    node.setRight(new BitNode<T>());
                    descend(node.getRight(), height - 1);
                }
            }
        }
        
        Builder builder = new Builder();

        int height = 0;

        BitNode<T> head = new BitNode<T>();
        if (itr.hasNext()) head.item = itr.next();

        while (itr.hasNext()) {
            height++;
            BitNode<T> node = new BitNode<T>();
            node.setLeft(head);
            node.item = itr.next();

            if (itr.hasNext() && height > 1) {
                node.setRight(new BitNode<T>());
                builder.descend(node.getRight(), height - 1);
            }

            head = node;
        }

        this.rootIndex = height > 0 ? 1 << height - 1 : 0;
        this.item = head.item;
        this.left = head.getLeft();
        this.right = head.getRight();
    }

    /** 
     * {@inheritDoc}
     */
    @Override
    public T get(int index) { 
        if (index < 0) throw new IndexOutOfBoundsException();
        if (!treeContainsIndex(rootIndex, index)) return null;

        int key, b;
        key = b = rootIndex;
        BitNode<T> cursor = this;

        while (index != key) {
            if (index < key) {
                key -= b;
                cursor = cursor.getLeft();
            }
            else {
                cursor = cursor.getRight();
            }
            if (cursor == null) return null;
            b >>>= 1;
            key += b;
        }
        return cursor.item;
    }

    /** 
     * {@inheritDoc}
     */
    public PBitOrderedTrie<T> set(int index, T item) {
        if (index < 0) throw new IndexOutOfBoundsException();

        PBitOrderedTrie<T> head;

        if (treeContainsIndex(rootIndex, index)) {
            head = new PBitOrderedTrie<T>(this);
        }
        else {
            head = new PBitOrderedTrie<T>(Integer.highestOneBit(index));

            BitNode<T> tail = head;
            int key = head.rootIndex >>> 1;
            while (key != rootIndex) {
                tail.setLeft(new BitNode<T>());
                tail = tail.getLeft();
                key >>>= 1;
            }
            tail.setLeft(this);
        }

        head.cascade(head, head.rootIndex, index).item = item;
        return head;
    }

    /**
     * {@inheritDoc}
     * 
     * <p> Performs a {@link PVector#multiSet multiSet} operation by doing a recursive in-ordered path copy.
     * This operation requires the indices and items to be sorted in ascending order of indices. The method will check if they are sorted,
     * and if they are not sorted then it will automatically sort the arguments via a stable-selection sort.
     * Consider sorting the arguments with a more efficient algorithm before calling this method with a very large number of mutations.
     */
    public PBitOrderedTrie<T> multiSet(int[] indices, T[] items) {

        if (indices.length == 0) return this;

        if (!isSorted(indices)) sortArgs(indices, items);

        if (indices[0] < 0) throw new IndexOutOfBoundsException();

        class MultiSetter {
            int i; // current position in arguments

            PBitOrderedTrie<T> multiSet() {
                i = 0;

                int headIndex = PBitOrderedTrie.this.rootIndex;
                BitNode<T> headLeft = PBitOrderedTrie.this.getLeft();
                BitNode<T> headRight = PBitOrderedTrie.this.getRight();
                T headItem = PBitOrderedTrie.this.item;

                // left
                if (indices[i] < headIndex) {
                    if (headLeft == null) {
                        rcascadeCreate(headLeft = new BitNode<T>(), 0, headIndex >>> 1);
                    }
                    else {
                        rcascadeCopy(headLeft = new BitNode<T>(headLeft), 0, headIndex >>> 1);
                    }
                }

                while (i < indices.length) {

                    // match
                    if (indices[i] == headIndex) {
                        headItem = items[i];
                        i++;
                        continue;
                    }

                    // right
                    if (treeContainsIndex(headIndex, indices[i])) {
                        if (headRight == null) {
                            rcascadeCreate(headRight = new BitNode<T>(), headIndex, headIndex >>> 1);
                        }
                        else {
                            rcascadeCopy(headRight = new BitNode<T>(headRight), headIndex, headIndex >>> 1);
                        }
                        if (i == indices.length) break;
                    }

                    // up
                    headIndex = (headIndex == 0) ? 1 : headIndex << 1;
                    headLeft = new BitNode<T>(headLeft, headRight, headItem);
                    headRight = null;
                    headItem = null;
                }
                return new PBitOrderedTrie<T>(headIndex, headLeft, headRight, headItem);
            }

            void rcascadeCopy(BitNode<T> copy, int key, int b) {
                key += b;

                // left
                if (indices[i] < key) {
                    if (copy.getLeft() == null) {
                        copy.setLeft(new BitNode<T>());
                        rcascadeCreate(copy.getLeft(), key - b, b >>> 1);
                    }
                    else {
                        copy.setLeft(new BitNode<T>(copy.getLeft()));
                        rcascadeCopy(copy.getLeft(), key - b, b >>> 1);
                    }
                    if (i == indices.length) return;
                }

                // match
                while (indices[i] == key) {
                    copy.item = items[i];
                    i++;
                    if (i == indices.length) return;
                }

                // right
                if (indices[i] < key + b) {
                    if (copy.getRight() == null) {
                        copy.setRight(new BitNode<T>());
                        rcascadeCreate(copy.getRight(), key, b >>> 1);
                    }
                    else {
                        copy.setRight(new BitNode<T>(copy.getRight()));
                        rcascadeCopy(copy.getRight(), key, b >>> 1);
                    }
                }
            }

            void rcascadeCreate(BitNode<T> node, int key, int b) {
                key += b;

                // left
                if (indices[i] < key) {
                    node.setLeft(new BitNode<T>());
                    rcascadeCreate(node.getLeft(), key - b, b >>> 1);
                    if (i == indices.length) return;
                }

                // match
                while (indices[i] == key) {
                    node.item = items[i];
                    i++;
                    if (i == indices.length) return;
                }

                // right
                if (indices[i] < key + b) {
                    node.setRight(new BitNode<T>());
                    rcascadeCreate(node.getRight(), key, b >>> 1);
                }
            }
        }
        return new MultiSetter().multiSet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
	public PVectorIterable<T> range(int a, int b) {
		return new RangeIterator(a, b);
	}

    /**
     * {@inheritDoc}
     */
	@Override
	public PVectorIterable<T> items() {
		return new RangeIterator(minIndex(), maxIndex());
	}

    @Override
    public String toString() {
        return "PBitOrderedTrie {item: " + item + ", rootIndex: " + rootIndex + "}";
    }

    /**
     * @return The index of the right-most node in this tree
     */
    public int minIndex() {
        int key = rootIndex;

        BitNode<T> node = this;
        while (node.getLeft() != null) {
            node = node.getLeft();
            key >>= 1;
            
        }
        return key;
    }

    /**
     * @return The index of the right-most node in this tree
     */
    public int maxIndex() {
        int key = rootIndex;
        int b = rootIndex;

        BitNode<T> node = this;
        while (node.getRight() != null) {
            node = node.getRight();
            b >>= 1;
            key += b;
        }
        return key;
    }

    private BitNode<T> cascade(BitNode<T> fromCopy, int fromCopyIndex, int toIndex) { 
        BitNode<T> copy = fromCopy;
        int key = fromCopyIndex;
        int b = Integer.lowestOneBit(fromCopyIndex);

        while (key != toIndex) {
            if (key > toIndex) {
                if (copy.getLeft() == null) break;
                key -= b;
                copy.setLeft(new BitNode<T>(copy.getLeft()));
                copy = copy.getLeft();
            }
            else {
                if (copy.getRight() == null) break;
                copy.setRight(new BitNode<T>(copy.getRight()));
                copy = copy.getRight();
            }
            b >>>= 1;
            key += b;
        }

        while (key != toIndex) {
            if (key > toIndex) {
                key -= b;
                copy.setLeft(new BitNode<T>());
                copy = copy.getLeft();
            }
            else {
                copy.setRight(new BitNode<T>());
                copy = copy.getRight();
            }
            b >>>= 1;
            key += b;
        }

        return copy;
    }

    /**
     * Check if indices are already sorted in ascending order
     */
    private boolean isSorted(int[] a) {
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) return false;
        }
        return true;
    }

    /**
     * Sort multi-setter arguments with stable selection sort
     */
    protected void sortArgs(int[] indices, T[] items) {
        
        // stable selection sort
        for (int i = 0; i < indices.length - 1; i++) {
            int m = i;
            for (int j = i + 1; j < indices.length; j++) {
                if (indices[j] < indices[m]) m = j;
            }

            int tempI = indices[m];
            T tempT = items[m];

            while (m > i) {
                indices[m] = indices[m - 1];
                items[m] = items[m - 1];
                m--;
            }
            indices[i] = tempI;
            items[i] = tempT; 
        }
    }

    private static boolean treeContainsIndex(int rootIndex, int index) {
        if (index == rootIndex) return true; // rootIndex may be 0
        return index <= (rootIndex * 2) - 1; // rootIndex may be 2^31
    }

    protected class RangeIterator implements PVectorIterable<T>, PVectorIterator<T> {

        @SuppressWarnings("unchecked")
        BitNode<T>[] stack = (BitNode<T>[]) new BitNode[33 - Integer.numberOfLeadingZeros(rootIndex)];
        int top = 0;

        int b = rootIndex;
        int key = rootIndex;

        int index;
        final int endIndex;

        protected RangeIterator(int startIndex, int endIndex) {
            index = startIndex - 1;
            this.endIndex = endIndex;
            stack[0] = PBitOrderedTrie.this;

            while (key != startIndex) {
                if (index < key) {
                    if (stack[top].getLeft() == null) break;
                    stack[top + 1] = stack[top++].getLeft();
                    key -= b;
                }
                else {
                    if (stack[top].getRight() == null) break;
                    stack[top + 1] = stack[top++].getRight();
                }
                b >>>= 1;
                key += b;
            }
        }

		@Override
		public boolean hasNext() {
			return index < endIndex - 1;
		}

		@Override
		public T next() {
			index++;

            if (top < 0) return null;

            if (key == index) return stack[top].item;

            if (key > index) return null;

            if (stack[top].getRight() != null) {
                stack[top + 1] = stack[top++].getRight();
                key += (b >>>= 1);

                while (stack[top].getLeft() != null) {
                    stack[top + 1] = stack[top++].getLeft();
                    key -= b;
                    key += (b >>>= 1);
                }
            }
            else {
                do {
                    if (--top < 0) return null;
                    key -= b;
                    key |= (b = (b == 0 ? 1 : b << 1));
                } while (stack[top + 1] == stack[top].getRight());
            }
            if (key == index) return stack[top].item;

            return null;
		}

		@Override
		public PBitOrderedTrie<T> erase() {
			return PBitOrderedTrie.this.set(index, null);
		}

		@Override
		public PBitOrderedTrie<T> set(T item) {
			return PBitOrderedTrie.this.set(index, item);
		}

		@Override
		public int getIndex() {
			return index;
		}

		@Override
		public PVectorIterator<T> iterator() {
			return this;
		}  
    }
}

/**
 * These are the objects that make up the individual nodes in the PBitOrderedTrie
 */
class BitNode<T> extends BinaryTreeNode<BitNode<T>> {
    T item;

    public BitNode() {}

    public BitNode(BitNode<T> toCopy) {
        item = toCopy.item;
        left = toCopy.left;
        right = toCopy.right;
    }

    protected BitNode(BitNode<T> left, BitNode<T> right, T item) {
        this.left = left;
        this.right = right;
        this.item = item;
    }

	@Override
	public BitNode<T> clone() {
		return new BitNode<T>(this);
    }
    
    @Override
    public String toString() {
        return "BitNode {item: " + item + "}";
    }
}
