package za.ac.sun.cs.ingenious.core.network.game;

import com.esotericsoftware.minlog.Log;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import za.ac.sun.cs.ingenious.core.Engine;
import za.ac.sun.cs.ingenious.core.network.Message;
import za.ac.sun.cs.ingenious.core.network.game.messages.GameTerminatedMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.GenActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.InitGameMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.MatchResetMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayActionMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.PlayedMoveMessage;
import za.ac.sun.cs.ingenious.core.network.game.messages.RewardMessage;
import za.ac.sun.cs.ingenious.core.network.lobby.StringMessage;

/**
 * This is used to handle communication from client to server. It stores a reference to
 * the {@link Engine} this player is playing with and conversely, the engine stores a
 * reference to this connection object. The run method constantly listens to messages
 * sent by the server and relays them to the engine. There are also methods for time
 * control for when there is a restriction on how much time a player is allocated for
 * their action.
 *
 * @author Stephan Tietz
 */
public class EngineToServerConnection extends SocketWrapper implements Runnable {

    /** The engine this player is communicating with the server for */
	private Engine engine;
	private int id;

	/**
	 * @return ID of the player that communicates with the server
	 */
	public int getId() {
		return id;
	}

	/**
	 * Create a new EngineToServerConnection from a socket and two object streams.
     *
	 * <b>Note</b>: You should call connectEngine after constructing the connection.
	 */
	public EngineToServerConnection(Socket socket, ObjectInputStream ios,
			ObjectOutputStream oos, int playerID) {
		super(socket, ios, oos);
		this.id = playerID;
	}

	/**
	 * Specifies the engine this connection will be responsible for handling communication for, and
     * launches a thread for doing so.
	 */
	public void connectEngine(Engine engine) {
		this.engine = engine;
		new Thread(this).start();
	}

    /** Overridden to inform the engine of the communication problem as well. */
    @Override
	protected void handleIOException(IOException e) {
		super.handleIOException(e);
		engine.serverDisconnected();
	}

	/**
	 * Event loop on client side.
	 * Listens for messages and calls corresponding methods on the engine.
	 */
    @Override
	public void run() {
		while (isAlive()) {
			Message a = receiveMessage();

			if (a == null) {
				break;
			} else if(a instanceof StringMessage){
				StringMessage sa = (StringMessage) a;
				String[] msg = sa.asArray();
				Log.info("received " + msg[0]);
				Log.error("UNEXPECTED STRING TRANSMISSION");
			} else if (a instanceof GenActionMessage){
				sendMessage(asyncHandleReceiveGenMove((GenActionMessage) a));
			} else if (a instanceof PlayedMoveMessage) {
				engine.receivePlayedMoveMessage((PlayedMoveMessage) a);
			} else if (a instanceof InitGameMessage){
				engine.receiveInitGameMessage((InitGameMessage) a);
			} else if (a instanceof GameTerminatedMessage){
				handleGameTerminateMessage((GameTerminatedMessage) a);
			} else if (a instanceof MatchResetMessage){
				engine.receiveMatchResetMessage((MatchResetMessage) a);
			} else if (a instanceof RewardMessage) {
				engine.receiveRewardMessage((RewardMessage) a);
			} else {
				Log.warn("Received unknown message of type " + a.getClass().getName());
			}
		}
	}


    /** Pass game termination message to engine, then close connection */
	private void handleGameTerminateMessage(GameTerminatedMessage a){
		engine.receiveGameTerminatedMessage(a);
		closeConnection();
	}


	/**
     * Constructs a CalculatorThread which requests the Engine to generate a move
     * and place it in a buffer, and returns the move to the server.
     *
     * If the move is not made in time, as specified by the Clock passed to the
     * engine in the GenMoveMessage, a PlayMoveMessage wrapping a null move is returned.
     * After receiving a move, or timing out in this way, the spwaned CalculatorThread
     * is marked as outdated, so that future responses by these threads do not cause incorrect
     * moves to be returned to the server.
     *
	 * Note that if the timeout of the clock is -1, this method will not return until a move is
     * returned by the engine.
     *
	 * Note that this method does not terminate the spawned CalculatorThread or any search
     * being performed by the engine when the clock times out - ensuring the blocking call
     * returns and any cleanup of threads spawned by the engine is the responsibility of the developer of the engine.
	 *
	 * @param a Instruction to generate a move, including a Clock object specifying the time available.
	 * @return The move to be played.
	 */
	private PlayActionMessage asyncHandleReceiveGenMove(final GenActionMessage a){
		final PlayActionMessage[] buffer = new PlayActionMessage[1];
		CalculatorThread calcThread = new CalculatorThread(buffer, a);
		calcThread.start();

		synchronized(this){
			try{
				if(a.getClock().getTimeout() == -1){
					wait();
				}else{
					wait( a.getClock().getTimeout());
				}
			}catch(InterruptedException e){
                // No logic necessary here.
			}
		}
		calcThread.setOutdated();
		if (buffer[0] == null) {
			return new PlayActionMessage(null);
		} else {
			return buffer[0];
		}
	}

	/** Thread responsible for requesting and returning a single move from the engine. */
	class CalculatorThread extends Thread{

        /** Buffer for engine to place generated move */
		private PlayActionMessage[] buffer;
        /** Whether the move that has been requested is still needed */
		private boolean stillNeeded = true;
        /** Identity of thread that spawned this thread */
		private Thread callingThread;
        /** Instruction to generate a move, including a corresponding clock */
		private GenActionMessage a;

		public CalculatorThread(PlayActionMessage[] buffer, GenActionMessage a) {
			this.buffer = buffer;
			this.a = a;
			callingThread = Thread.currentThread();
		}

        /**
         * Requests engine to generate move with a blocking call, and interrupts
         * the spawning thread so that it can retrieve the result.
         *
         * If the move requested is no longer needed (typically because the allowed time
         * to play the move has expired, the interrupt is not performed, so that the move
         * will not be used.
         *
         * <b>Important</b>: Note that this thread does not terminate until the engine returns a move.
         */
		@Override
		public void run() {
			buffer[0] = engine.receiveGenActionMessage(a);
			if(stillNeeded){
				callingThread.interrupt();
			}
		}

        /**
         * Notes that the requested move is no longer needed, for example if the clock has timed out.
         */
		public synchronized void setOutdated(){
			stillNeeded = false;
		}
	}
}
