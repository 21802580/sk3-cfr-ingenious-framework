package za.ac.sun.cs.ingenious.search.mcts.enhancements;

import za.ac.sun.cs.ingenious.search.mcts.nodeComposition.BasicNode.MctsNodeCompositionInterface;
import za.ac.sun.cs.ingenious.core.GameState;

public interface Enhancement<S extends GameState, C, N extends MctsNodeCompositionInterface<S, C, ?>> {

	public default void preSelection(N nodeToSelectFrom) {
	}
	
	public default void postSelection(N nodeToSelectFrom, C selectedChild) {
	}
	
	public default void preExpansion(N nodeToBeExpandedFrom) {
	} 
	
	public default void postExpansion(N nodeToSelectFrom, C expandedChild) {
	}
	
	public default void preSimulation(S state) {
	}
	
	public default void postSimulation(double[] resultFromSimulation) {
	}
	
	public default void preBackpropagation(N node, double[] results) {
	}
	
	public default void postBackpropagation(N node) {
	}
}

// TODO: comment
