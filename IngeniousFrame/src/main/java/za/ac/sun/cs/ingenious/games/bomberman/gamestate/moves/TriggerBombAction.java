package za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves;

import za.ac.sun.cs.ingenious.core.Action;

public class TriggerBombAction implements Action{

	private static final long serialVersionUID = 1L;

	int playerID;

	public TriggerBombAction(int id) {

		this.playerID = id;
	}

	@Override
	public String toString(){
		return "trigger by "+playerID;
	}

	@Override
	public int getPlayerID() {
		return playerID;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !(this.getClass() == obj.getClass())) {
			return false;
		} else {
			return (this.playerID == ((TriggerBombAction) obj).playerID);
		}
	}

	@Override
	public int hashCode() {
		return playerID;
	}

	@Override
	public Action deepCopy() {
		return Action.cloner.deepClone(this);
	}
}
