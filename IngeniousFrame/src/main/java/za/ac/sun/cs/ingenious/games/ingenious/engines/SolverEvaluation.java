package za.ac.sun.cs.ingenious.games.ingenious.engines;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousMinMaxBoardInterface;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;
import za.ac.sun.cs.ingenious.games.ingenious.IngeniousRack;

public class SolverEvaluation extends IngeniousEvaluator {
	@Override
	public double evaluate(IngeniousScoreKeeper scores,IngeniousMinMaxBoardInterface<IngeniousAction, IngeniousRack> board, ArrayList<IngeniousRack> racks,int playerId){
		if(board.full()){
			return scores.getScore(playerId).compare(scores.getScore(playerId), scores.getScore(1-playerId));
		}else{
			return 0;
		}
	}
}
