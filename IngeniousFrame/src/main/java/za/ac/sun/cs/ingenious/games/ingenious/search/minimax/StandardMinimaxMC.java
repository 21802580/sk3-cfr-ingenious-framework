package za.ac.sun.cs.ingenious.games.ingenious.search.minimax;

import java.util.ArrayList;

import za.ac.sun.cs.ingenious.games.ingenious.IngeniousAction;

public class StandardMinimaxMC extends MiniMax {
	
	public StandardMinimaxMC(int depth){
		this.searchDepth = depth;
	}
	@Override
	public ArrayList<IngeniousAction> moveOrder(ArrayList<IngeniousAction> moves, SearchState state){
	//	Collections.sort(moves,new MoveValueComparator(state.getScores(),state.getBoard()));
		return moves;
	}
	
}
