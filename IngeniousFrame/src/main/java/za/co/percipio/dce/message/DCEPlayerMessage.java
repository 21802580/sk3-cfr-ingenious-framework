package za.co.percipio.dce.message;

import za.co.percipio.mpl.Message;

/**
 * Created by Chris Coetzee on 2016/07/28.
 */
public class DCEPlayerMessage implements Message {
    private String username;
    private String playerPath;

    public DCEPlayerMessage(String username, String playerPath) {
        this.username = username;
        this.playerPath = playerPath;
    }

    public String getPlayerPath() {
        return playerPath;
    }

    public String getUsername() {
        return username;
    }
}
