import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;

import org.junit.Test;

import za.ac.sun.cs.ingenious.core.util.persistent.PVector;
import za.ac.sun.cs.ingenious.core.util.persistent.tries.PBitOrderedTrie;

public class PBitOrderedTrieTest {
    
    int CAP_MAX = 1 << 20;
    int CAP_MIN = 1 << 15;
    Random rng = new Random(123L);

    Integer[] ia = new Integer[(int) ((rng.nextDouble() * (CAP_MAX - CAP_MIN)) + CAP_MIN)];
    {
        ia = new Integer[16];
        for (int i = 0; i < ia.length; i++) ia[i] = i;
    }

    @Test
    public void testIterableConstructor_get() {
        
        PVector<Integer> pv = new PBitOrderedTrie<Integer>(Arrays.asList(ia));

        for (int i = 0; i < ia.length; i++) {
            assertTrue(pv.get(i).equals(ia[i]));
        }
    }

    @Test
    public void testIterableConstructor_range() {
        
        PVector<Integer> pv = new PBitOrderedTrie<Integer>(Arrays.asList(ia));

        for (Integer i : pv.range(0, ia.length)) {
            assertTrue(i.equals(ia[i]));
        }
    }

    @Test
    public void testSet() {
        testSet(1000000);
    }

    public void testSet(int cap) {
        PVector<Integer> pv = new PBitOrderedTrie<Integer>();
        
        HashSet<Integer> hs = new HashSet<Integer>();

        for (int i = 0; i < cap; i++) {
            int r = (int)(rng.nextDouble() * cap);

            pv = pv.set(r, r);
            hs.add(r);
        }

        for (Integer r : hs) {
            assertTrue(pv.get(r).equals(r));
        }
    }

    @Test
    public void testIndex() {
        PVector<String> pv = new PBitOrderedTrie<String>();

        pv = pv.set(2147483647, "max index");
        assertTrue(pv.get(2147483647).equals("max index"));

        pv = pv.set(0, "min index");
        assertTrue(pv.get(0).equals("min index"));
    }

    @Test
    public void testFullPersistence() {
        testFullPersistence(64, 3, 10);
    }

    public void testFullPersistence(int stateSize, int branchingFactor, int maxDepth) {

        class VersionTester {

            void build(Version previous, int depth) {
                if (depth == 0) {
                    previous.children = new Version[0];
                    return;
                }
                previous.children = new Version[branchingFactor];

                for (int i = 0; i < previous.children.length; i++) {
                    Version v = new Version();
                    int r1 = (int)(rng.nextDouble() * stateSize);
                    int r2 = (int)(rng.nextDouble() * stateSize);

                    v.pv = previous.pv.set(r1, r2);
                    v.a = Arrays.copyOf(previous.a, stateSize);
                    v.a[r1] = r2;
                    
                    previous.children[i] = v;


                }
                
                for (int i = 0; i < previous.children.length; i++) {
                    build(previous.children[i], depth - 1);
                }
            }

            void test(Version v) {
                assertTrue(v.statesEqual());
                for (int i = 0; i < v.children.length; i++) {
                    test(v.children[i]);
                }
            }
        };

        VersionTester vt = new VersionTester();
        Version root = new Version(new PBitOrderedTrie<Integer>(), new Integer[stateSize]); 
        try {
            vt.build(root, maxDepth);
            vt.test(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    @Test
    public void testFullPersistence_multiSetter() {
        try {
            testFullPersistence_multiSetter(64, 3, 3, 20);
        } catch (StackOverflowError e) {
            e.printStackTrace();
        }
        
    }

    public void testFullPersistence_multiSetter(int stateSize, int branchingFactor, int maxDepth, int maxEdits) {

        class VersionTester {

            void build(Version previous, int depth) {
                if (depth == 0) {
                    previous.children = new Version[0];
                    return;
                }
                previous.children = new Version[branchingFactor];

                for (int i = 0; i < previous.children.length; i++) {
                    Version v = new Version();

                    v.a = Arrays.copyOf(previous.a, stateSize);

                    int[] indices = new int[(int)(rng.nextDouble() * maxEdits)];
                    Integer[] values = new Integer[indices.length];

                    for (int j = 0; j < indices.length; j++) {
                        int r1 = (int)(rng.nextDouble() * stateSize);
                        int r2 = (int)(rng.nextDouble() * stateSize);

                        indices[j] = r1;
                        values[j] = r2;

                        v.a[r1] = r2;
                    }
                    v.pv = previous.pv.multiSet(indices, values);

                    previous.children[i] = v;
                }
                
                for (int i = 0; i < previous.children.length; i++) {
                    build(previous.children[i], depth - 1);
                }
            }

            void test(Version v) {
                assertTrue(v.statesEqual());
                for (int i = 0; i < v.children.length; i++) {
                    test(v.children[i]);
                }
            }
        };

        VersionTester vt = new VersionTester();
        Version root = new Version(new PBitOrderedTrie<Integer>(), new Integer[stateSize]);
        try {
            vt.build(root, maxDepth);
            vt.test(root);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }
}

class Version {

    PVector<Integer> pv;
    Integer[] a;

    Version[] children;

    Version() {}

    Version(PVector<Integer> pv, Integer[] a) {
        this.pv = pv;
        this.a = a;
    }

    boolean statesEqual() {
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null) {
                if (pv.get(i) != null) {
                    return false;
                }
            }
            else {
                if (!a[i].equals(pv.get(i))) {
                    return false;
                }
            }  
        }
        return true;
    }
}