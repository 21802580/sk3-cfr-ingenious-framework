import static org.junit.Assert.assertTrue;

import org.junit.Test;

import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.EngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.Constants;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.XYAction;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedSquareBoard;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTFinalEvaluator;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTLogic;
import za.ac.sun.cs.ingenious.games.tictactoe.TTTReferee;
import za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTMCTSEngine;
import za.ac.sun.cs.ingenious.games.tictactoe.engines.TTTMinimaxEngine;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;

public class TicTacToeTests {
	
	/**
	 * Checks that MCTS correctly identifies the central position as the first spot to play in TicTacToe.
	 */
	@Test
	public void testMCTSFirstAction() {
		TurnBasedSquareBoard currentState = new TurnBasedSquareBoard(3,0,2);
		TTTLogic logic = new TTTLogic();
		MCTSNode<TurnBasedSquareBoard> root = new MCTSNode<TurnBasedSquareBoard>(currentState, logic, null, null);
		XYAction a = (XYAction) MCTS.generateAction(root,
				new RandomPolicy<TurnBasedSquareBoard>(logic, new TTTFinalEvaluator(), new PerfectInformationActionSensor<TurnBasedSquareBoard>(), false),
				new MCTSDescender<TurnBasedSquareBoard>(logic, new UCT<>(), new PerfectInformationActionSensor<>()), 
				new SimpleUpdater<MCTSNode<TurnBasedSquareBoard>>(),
				Constants.TURN_LENGTH);
		assertTrue(a.getX()==1 && a.getY()==1);
	}

	/**
	 * Checks that the game can be played using MiniMax search and the resulting terminal state is a draw.
	 */
	@Test
	public void testMiniMaxResult() throws MissingSettingException, IncorrectSettingTypeException {
		TTTMinimaxEngine engine0 = new TTTMinimaxEngine(new EngineToServerConnection(null, null, null, 0));
		TTTMinimaxEngine engine1 = new TTTMinimaxEngine(new EngineToServerConnection(null, null, null, 1));
		PlayerRepresentation[] players = new PlayerRepresentation[] {
				new LocalPlayer(engine0),
				new LocalPlayer(engine1)
		};
		TTTReferee referee = new TTTReferee(null, players);
		referee.run();
		double[] scores = new TTTFinalEvaluator().getScore(referee.getCurrentState());
		assertTrue(scores[0] == 0.5 && scores[1] == 0.5);
	}
	
	/**
	 * Checks that the an MCTS player can achieve a draw against MiniMax.
	 */
	@Test
	public void testMiniMaxVSMCTS() throws MissingSettingException, IncorrectSettingTypeException {
		TTTMinimaxEngine engine0 = new TTTMinimaxEngine(new EngineToServerConnection(null, null, null, 0));
		TTTMCTSEngine engine1 = new TTTMCTSEngine(new EngineToServerConnection(null, null, null, 1));
		PlayerRepresentation[] players = new PlayerRepresentation[] {
				new LocalPlayer(engine0),
				new LocalPlayer(engine1)
		};
		TTTReferee referee = new TTTReferee(null, players);
		referee.run();
		double[] scores = new TTTFinalEvaluator().getScore(referee.getCurrentState());
		assertTrue(scores[0] == 0.5 && scores[1] == 0.5);
	}

}
