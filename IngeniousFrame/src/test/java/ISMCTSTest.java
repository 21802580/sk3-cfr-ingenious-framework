import static org.junit.Assert.assertTrue;

import com.esotericsoftware.minlog.Log;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.ActionSensor;
import za.ac.sun.cs.ingenious.core.GameFinalEvaluator;
import za.ac.sun.cs.ingenious.core.Move;
import za.ac.sun.cs.ingenious.core.util.sensor.PerfectInformationActionSensor;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameLogic;
import za.ac.sun.cs.ingenious.core.util.state.TurnBasedGameState;
import za.ac.sun.cs.ingenious.search.InformationSetDeterminizer;
import za.ac.sun.cs.ingenious.search.ismcts.ISMCTSNode;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSDescender;
import za.ac.sun.cs.ingenious.search.ismcts.SOISMCTSPOMDescender;
import za.ac.sun.cs.ingenious.search.ismcts.SubsetUCT;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTS;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSDescender;
import za.ac.sun.cs.ingenious.search.mcts.legacy.MCTSNode;
import za.ac.sun.cs.ingenious.search.mcts.legacy.RandomPolicy;
import za.ac.sun.cs.ingenious.search.mcts.legacy.SimpleUpdater;
import za.ac.sun.cs.ingenious.search.mcts.legacy.UCT;
import za.ac.sun.cs.ingenious.search.pimc.PIMC;

/**
 * This test implements the example from Fig.1 and 2 from Cowling et al.s paper on ISMCTS
 */
public class ISMCTSTest {

	static class TestTreeState extends TurnBasedGameState {

		public int whichIS; // -1 for unknown, 0 for x, 1 for y
		public int firstMove; // -1 for not yet made, 0 for a1, 1 for a2
		public int secondMove; // -1 for not yet made, 0 for a3, 1 for a4

		public TestTreeState(int IS, int firstMove, int secondMove) {
			super(0,1);
			whichIS = IS;
			this.firstMove = firstMove;
			this.secondMove = secondMove;
		}

		@Override
		public void printPretty() {
			System.out.println("IS: " + whichIS + ", FirstMove: " + firstMove + ", SecondMove: " + secondMove);
		}

	}

	static class TestStateEvaluator implements GameFinalEvaluator<TestTreeState> {

		@Override
		public double[] getScore(TestTreeState forState) {
			if (forState.firstMove == 1)
				return new double[]{0.75};
			else if (forState.firstMove == 0) {
				if (forState.secondMove == 0) {
					if (forState.whichIS == 0)
						return new double[]{0};
					else if (forState.whichIS == 1)
						return new double[]{1};
				} else if (forState.secondMove == 1) {
					if (forState.whichIS == 0)
						return new double[]{1};
					else if (forState.whichIS == 1)
						return new double[]{0};
				}
			}
			throw new RuntimeException("Trying to evaluate non-terminal state");
		}

	}

	static class TestAction implements Action {
		public int which; // 0 for a1, 1 for a2, 2 for a3, 3 for a4
		public TestAction(int which) {
			this.which = which;
		}

		@Override
		public String toString() {
			return "Action: " + which;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + which;
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			TestAction other = (TestAction) obj;
			if (which != other.which)
				return false;
			return true;
		}

		@Override
		public int getPlayerID() {
			return -1;
		}

		public Action deepCopy() {
			return Action.cloner.deepClone(this);
		}
	}

	static Random rand = new Random();
	static class TestLogic implements TurnBasedGameLogic<TestTreeState>, InformationSetDeterminizer<TestTreeState>{

		@Override
		public boolean validMove(TestTreeState fromState, Move move) {
			TestAction a = (TestAction) move;
			switch (a.which) {
			case 0:
				if (fromState.firstMove == -1) {
					return true;
				}
				break;
			case 1:
				if (fromState.firstMove == -1) {
					return true;
				}
				break;
			case 2:
				if (fromState.firstMove == 0 && fromState.secondMove == -1) {
					return true;
				}
				break;
			case 3:
				if (fromState.firstMove == 0 && fromState.secondMove == -1) {
					return true;
				}
				break;
			}
			return false;
		}

		@Override
		public boolean makeMove(TestTreeState fromState, Move move) {
			TestAction a = (TestAction) move;
			switch (a.which) {
			case 0:
				if (fromState.firstMove == -1) {
					fromState.firstMove = 0;
					return true;
				}
				break;
			case 1:
				if (fromState.firstMove == -1) {
					fromState.firstMove = 1;
					return true;
				}
				break;
			case 2:
				if (fromState.firstMove == 0 && fromState.secondMove == -1) {
					fromState.secondMove = 0;
					return true;
				}
				break;
			case 3:
				if (fromState.firstMove == 0 && fromState.secondMove == -1) {
					fromState.secondMove = 1;
					return true;
				}
				break;
			}
			return false;
		}

		@Override
		public void undoMove(TestTreeState fromState, Move move) {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<Action> generateActions(TestTreeState fromState, int forPlayerID) {
			List<Action> ret = new ArrayList<>();
			if (fromState.firstMove == -1) {
				ret.add(new TestAction(0));
				ret.add(new TestAction(1));
			} else if (fromState.firstMove == 0 && fromState.secondMove == -1) {
				ret.add(new TestAction(2));
				ret.add(new TestAction(3));
			}
			return ret;
		}

		@Override
		public boolean isTerminal(TestTreeState state) {
			return state.firstMove == 1 || (state.firstMove == 0 && state.secondMove != -1);
		}

		@Override
		public TestTreeState determinizeUnknownInformation(TestTreeState forState, int forPlayerID) {
			return new TestTreeState(rand.nextInt(2), forState.firstMove, forState.secondMove);
		}

		@Override
		public TestTreeState observeState(TestTreeState state, int forPlayerID) {
			return new TestTreeState(-1, state.firstMove, state.secondMove);
		}

	}

	TestLogic logic = new TestLogic();
	TestStateEvaluator eval = new TestStateEvaluator();
	TestTreeState startState = new TestTreeState(-1, -1, -1);
	ActionSensor<TestTreeState> sensor = new PerfectInformationActionSensor<TestTreeState>();
	@Test
	public void PIMCTest() {
		Log.set(Log.LEVEL_TRACE);
		TestAction a = (TestAction) PIMC.generateAction(startState,
				logic,
				logic,
				new RandomPolicy<TestTreeState>(logic, eval, sensor,false),
				new MCTSDescender<TestTreeState>(logic, new UCT<>(), sensor),
				new SimpleUpdater<MCTSNode<TestTreeState>>(),
				sensor, 0, 4000, 80);
		assertTrue(a.which == 0);
	}

	@Test
	public void SOISMCTSTest() {
		Log.set(Log.LEVEL_TRACE);
		ISMCTSNode<TestTreeState> root = new ISMCTSNode<TestTreeState>(startState,
				logic, null, null,
				new ArrayList<Move>(logic.generateActions(startState, startState.nextMovePlayerID)));
		TestAction a = (TestAction) MCTS.generateAction(root,
				new RandomPolicy<TestTreeState>(logic, eval, sensor, false),
				new SOISMCTSDescender<TestTreeState>(logic, new SubsetUCT<>(), sensor, 0, logic),
				new SimpleUpdater<ISMCTSNode<TestTreeState>>(),
				4000);
		assertTrue(a.which == 1);
	}

	@Test
	public void SOISMCTSPOMTest() {
		Log.set(Log.LEVEL_TRACE);
		ISMCTSNode<TestTreeState> root = new ISMCTSNode<TestTreeState>(startState,
				logic, null, null,
				new ArrayList<Move>(logic.generateActions(startState, startState.nextMovePlayerID)));
		TestAction a = (TestAction) MCTS.generateAction(root,
				new RandomPolicy<TestTreeState>(logic, eval, sensor, false),
				new SOISMCTSPOMDescender<TestTreeState>(logic, new SubsetUCT<>(), sensor, 0, logic),
				new SimpleUpdater<ISMCTSNode<TestTreeState>>(),
				4000);
		assertTrue(a.which == 1);
	}
}
