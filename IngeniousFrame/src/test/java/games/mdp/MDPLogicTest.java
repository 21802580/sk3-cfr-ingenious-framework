package games.mdp;

import games.mdp.testData.MDPTestData;
import org.junit.Test;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.exception.games.mdp.BadMDPSettingException;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for MDPLogic. All unit tests follow the AAA pattern as well as the naming convention described here:
 * https://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
 */
public class MDPLogicTest {
    @Test
    public void validMove_givenValidMove_returnsTrue() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(0L);
        DiscreteAction action = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0);

        // Act
        boolean result = logic.validMove(state, action);

        // Assert
        assertTrue(result);
    }

    @Test
    public void validMove_givenInvalidMove_returnsFalse() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(1L);
        DiscreteAction action = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0);

        // Act
        boolean result = logic.validMove(state, action);

        // Assert
        assertFalse(result);
    }

    @Test
    public void makeMove_deterministic_updatesToCorrectState() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting02.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting02);

        MDPState<Long> state = new MDPState<>(0L);
        DiscreteAction action = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1);
        MDPState<Long> expectedNextState = new MDPState<>(2L);

        // Act
        boolean result = logic.makeMove(state, action);

        // Assert
        assertTrue(result);
        assertEquals(expectedNextState, state);
    }

    @Test
    public void makeMove_stochastic_updatesToStateInDistribution() throws BadMDPSettingException {
        // Confirmed that this seed results in both valid states being reached.
        Random rng = new Random(1234L);

        for (int i = 0; i < 10; i++) {
            // Set up
            MDPTestData.testMdpSetting02.ensureValid();
            MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting02).withRandom(rng);

            MDPState<Long> state = new MDPState<>(0L);
            DiscreteAction action = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0);
            Collection<MDPState<Long>> validNextStates = Arrays.asList(
                    new MDPState<>(1L),
                    new MDPState<>(3L)
            );

            // Act
            boolean result = logic.makeMove(state, action);

            // Assert
            assertTrue(result);
            assertTrue(validNextStates.contains(state));
        }
    }

    @Test
    public void generateActions_stateWithActions_returnsActionsForState() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(0L);
        List<DiscreteAction> expectedActions = Arrays.asList(
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0),
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)
        );

        // Act
        List<Action> actions = logic.generateActions(state, MDPLogic.DEFAULT_PLAYER_TO_ACT);

        // Assert
        // Note that this assumes no duplicate actions will occur, and thus ignores them.
        boolean equal = expectedActions.containsAll(actions) && actions.containsAll(expectedActions);
        assertTrue(equal);
    }

    @Test
    public void generateActions_stateWithNoActions_returnsEmptyList() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(1L);

        // Act
        List<Action> actions = logic.generateActions(state, MDPLogic.DEFAULT_PLAYER_TO_ACT);

        // Assert
        assertEquals(Collections.EMPTY_LIST, actions);
    }

    @Test
    public void isTerminal_terminalState_returnsTrue() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(1L);

        // Act
        boolean isTerminal = logic.isTerminal(state);

        // Assert
        assertTrue(isTerminal);
    }

    @Test
    public void isTerminal_nonTerminalState_returnsFalse() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(0L);

        // Act
        boolean isTerminal = logic.isTerminal(state);

        // Assert
        assertFalse(isTerminal);
    }
}
