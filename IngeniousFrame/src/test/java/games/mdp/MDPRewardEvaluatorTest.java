package games.mdp;

import games.mdp.testData.MDPTestData;
import org.junit.Test;
import za.ac.sun.cs.ingenious.core.exception.games.mdp.BadMDPSettingException;
import za.ac.sun.cs.ingenious.games.mdp.MDPFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.MDPRewardEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;

import static org.junit.Assert.assertEquals;

public class MDPRewardEvaluatorTest {

    @Test
    public void getScore_givenState_ReturnsCorrectReward() throws BadMDPSettingException {
        // Set up
        MDPTestData.testMdpSetting01.ensureValid();
        MDPRewardEvaluator<Long> evaluator = MDPRewardEvaluator.fromSetting(MDPTestData.testMdpSetting01);

        MDPState<Long> state = new MDPState<>(0L);
        state.nextGameState(1L);

        // Act
        double[] result = evaluator.getReward(state);

        // Assert
        assertEquals(-5.0, result[0], 0.0);
    }
}
