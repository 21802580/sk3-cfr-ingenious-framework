package search.rl.testData;

import com.google.common.collect.ImmutableMap;
import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ReinforcementLearningTestData {
    public static String cliffLevel = "Rewards:\n" +
            "A = 50.0 1\n" +
            "D = -100.0 1\n" +
            "Board:\n" +
            "-1.0 0\n" +
            "X X X X X X X X\n" +
            "X             X\n" +
            "X             X\n" +
            "X             X\n" +
            "X S X D D X A X\n" +
            "X X X D D X X X";

    public static Map<Coord, Set<CompassDirectionAction>> getExpectedOnPolicyCliffLevelActions(int playerId) {
        Map<Coord, Set<CompassDirectionAction>> expectedActionForCoord = new HashMap<>();
        expectedActionForCoord.put(
                new Coord(1, 4),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.N)
                ))
        );
        expectedActionForCoord.put(
                new Coord(1, 3),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.N),
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(1, 2),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.N),
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(1, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(2, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(2, 2),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.N),
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(3, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(4, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.E),
                    new CompassDirectionAction(playerId, CompassDirection.S)
                ))
        );
        expectedActionForCoord.put(
                new Coord(5, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.E),
                    new CompassDirectionAction(playerId, CompassDirection.S)
                ))
        );
        expectedActionForCoord.put(
                new Coord(6, 1),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.S)
                ))
        );
        expectedActionForCoord.put(
                new Coord(6, 2),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.S)
                ))
        );
        expectedActionForCoord.put(
                new Coord(6, 3),
                new HashSet<>(Arrays.asList(
                    new CompassDirectionAction(playerId, CompassDirection.S)
                ))
        );

        return expectedActionForCoord;
    }

    public static Map<Coord, CompassDirectionAction> getOptimalCliffLevelActions(int playerId) {
        Map<Coord, CompassDirectionAction> expectedActionForCoord = new HashMap<>();
        expectedActionForCoord.put(new Coord(1, 4), new CompassDirectionAction(playerId, CompassDirection.N));
        expectedActionForCoord.put(new Coord(1, 3), new CompassDirectionAction(playerId, CompassDirection.E));
        expectedActionForCoord.put(new Coord(1, 3), new CompassDirectionAction(playerId, CompassDirection.E));
        expectedActionForCoord.put(new Coord(2, 3), new CompassDirectionAction(playerId, CompassDirection.E));
        expectedActionForCoord.put(new Coord(3, 3), new CompassDirectionAction(playerId, CompassDirection.E));
        expectedActionForCoord.put(new Coord(4, 3), new CompassDirectionAction(playerId, CompassDirection.E));
        expectedActionForCoord.put(new Coord(5, 3), new CompassDirectionAction(playerId, CompassDirection.E));

        return expectedActionForCoord;
    }

    public static String cliffLevelWind = "Rewards:\n" +
            "A = 1.0 1\n" +
            "D = -1.0 1\n" +
            "Wind:\n" +
            "Column 3 1 0\n" +
            "Column 4 1 0\n" +
            "Column 5 1 0\n" +
            "Board:\n" +
            "-0.01 0\n" +
            "X X X X X X X X\n" +
            "X             X\n" +
            "X             X\n" +
            "X             X\n" +
            "X S X D D X A X\n" +
            "X X X D D X X X";

    public static Map<Coord, Set<CompassDirectionAction>> getOptimalCliffLevelWindActions(int playerId) {
        Map<Coord, Set<CompassDirectionAction>> expectedActionForCoord = new HashMap<>();
        expectedActionForCoord.put(
                new Coord(1, 4),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.N)))
        );
        expectedActionForCoord.put(
                new Coord(1, 3),
                new HashSet<>(Arrays.asList(
                        new CompassDirectionAction(playerId, CompassDirection.E),
                        new CompassDirectionAction(playerId, CompassDirection.N)
                ))
        );
        expectedActionForCoord.put(
                new Coord(1, 2),
                new HashSet<>(Arrays.asList(
                        new CompassDirectionAction(playerId, CompassDirection.E),
                        new CompassDirectionAction(playerId, CompassDirection.N)
                ))
        );
        expectedActionForCoord.put(
                new Coord(1, 1),
                new HashSet<>(Arrays.asList(
                        new CompassDirectionAction(playerId, CompassDirection.E)
                ))
        );
        expectedActionForCoord.put(
                new Coord(2, 3),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.N)))
        );
        expectedActionForCoord.put(
                new Coord(2, 2),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.N)))
        );
        expectedActionForCoord.put(
                new Coord(2, 1),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.E)))
        );
        expectedActionForCoord.put(
                new Coord(3, 2),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.E)))
        );
        expectedActionForCoord.put(
                new Coord(4, 3),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.E)))
        );
        expectedActionForCoord.put(
                new Coord(5, 3),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.E)))
        );
        expectedActionForCoord.put(
                new Coord(6, 3),
                new HashSet<>(Arrays.asList(new CompassDirectionAction(playerId, CompassDirection.S)))
        );

        return expectedActionForCoord;
    }
}
