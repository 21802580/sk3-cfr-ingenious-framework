package search.rl;

import com.google.common.collect.ImmutableMap;
import games.mdp.testData.MDPTestData;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldMonteCarloOffPolicyEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.monteCarlo.TabularMonteCarloOffPolicy;
import za.ac.sun.cs.ingenious.search.rl.util.EpisodeStep;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for TabularMonteCarloOffPolicy. All unit tests follow the AAA pattern as well as the naming convention described
 * here: https://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
 */
public class TabularMonteCarloOffPolicyTest {

    @Test
    public void chooseActionFromBehaviouralPolicy_stateInDistribution_returnsActionWithNonZeroProbability() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);

        // Act
        Action action = alg.chooseActionFromBehaviouralPolicy(state);

        // Assert
        assertTrue(alg.getB().get(state).get(action) > 0);
    }

    @Test
    public void chooseActionFromBehaviouralPolicy_stateInDistribution_addsEpisodeStepToSteps() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);

        // Act
        Action action = alg.chooseActionFromBehaviouralPolicy(state);

        // Assert
        EpisodeStep expected = new EpisodeStep(state, action);
        assertEquals(1, alg.getSteps().size());
        assertEquals(expected, alg.getSteps().get(0));
    }

    @Test
    public void greedyActionForState_validState_returnsGreedyAction() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        MDPState<Long> state = new MDPState<>(0L);
        List<Action> actions = logic.generateActions(state, MDPLogic.DEFAULT_PLAYER_TO_ACT);

        Map<GameState, Map<Action, Double>> qTable = new HashMap<>();
        Map<Action, Double> actionMap = ImmutableMap.of(
                actions.get(0), 3.0,
                actions.get(1), 5.0
        );
        qTable.put(state, actionMap);

        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);
        alg.setQTable(qTable);

        // Act
        Action action = alg.greedyActionForState(state);

        // Assert
        assertEquals(actions.get(1), action);
    }

    @Test
    public void setMostRecentReward_validReward_setsRewardForLastStep() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);
        alg.chooseActionFromBehaviouralPolicy(state);

        // Act
        alg.setMostRecentReward(new ScalarReward(123.0));

        // Assert
        ScalarReward reward = (ScalarReward)alg.getSteps().get(0).getReward();
        assertEquals(123.0, reward.getReward(), 0.0);
    }

    @Test
    public void performLazyBehaviouralPolicyInitialization_validState_createsValidDistribution() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting02);
        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);

        // Act
        alg.performLazyBehaviouralPolicyInitialization(state);

        // Assert
        Collection<Double> actionProbabilities = alg.getB().get(state).values();
        assertTrue(actionProbabilities.stream().allMatch(x -> x > 0.0 && x < 1.0));

        double probabilitySum = actionProbabilities.stream().reduce(0.0, Double::sum);
        assertEquals(1.0, probabilitySum, 0.0);
    }

    @Test
    public void update_validSteps_updatesTablesAndTargetPolicy() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting02);
        TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOffPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withRandom(rng);
        alg.setLogic(logic);

        MDPState<Long> state = new MDPState<>(0L);
        Action finalAction = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1);

        List<EpisodeStep> episodeSteps = Arrays.asList(
                new EpisodeStep(new MDPState<>(3L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(state, new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0)),
                new EpisodeStep(new MDPState<>(3L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(state, finalAction)
        );
        episodeSteps.get(0).setReward(new ScalarReward(-1));
        episodeSteps.get(1).setReward(new ScalarReward(-1));
        episodeSteps.get(2).setReward(new ScalarReward(-1));
        episodeSteps.get(3).setReward(new ScalarReward(10));
        alg.setSteps(episodeSteps);

        // Act
        alg.update();

        // Assert
        Action policyAction = alg.getPolicy().get(state);
        assertEquals(finalAction, policyAction);

        assertTrue(alg.getQTable().get(state).get(finalAction) != 0);
        assertTrue(alg.getCTable().get(state).get(finalAction) != TabularMonteCarloOffPolicy.INITIAL_C_VALUE);
    }

    @Test
    public void tabularMonteCarloOffPolicy_gridWorldCliff_solvesPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 300;
        int playerId = 0;

        GridWorldMonteCarloOffPolicyEngine engine = new GridWorldMonteCarloOffPolicyEngine(
                new DummyEngineToServerConnection(playerId)
        );
        engine.setDeterministic();

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Action> policy = engine.getAlg().getPolicy();

        // ====================================== Assert ======================================

        // Test along the optimal path.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            CompassDirectionAction maxAction = (CompassDirectionAction) policy.get(gameState);

            assertEquals(expectedActionForCoord.get(coord), maxAction);
        }
    }
}
