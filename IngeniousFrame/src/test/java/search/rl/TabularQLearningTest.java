package search.rl;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldTabularQLearningEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.qlearning.TabularQLearning;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for TabularQLearning. All unit tests follow the AAA pattern as well as the naming convention described
 * here: https://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
 */
public class TabularQLearningTest {

    @Test
    public void update_validSARS_updatesQTableValue() {
        // Set up
        TabularQLearning tabularQLearning = new TabularQLearning();
        GameState state = new MDPState<>(0L);
        Action action = new DiscreteAction(0, 1);
        ScalarReward reward = new ScalarReward(10.0);
        GameState newState = new MDPState<>(1);

        double oldQValue = 0.0;

        // Act
        tabularQLearning.update(state, action, reward, newState);

        // Assert
        double newQValue = tabularQLearning.getQTable().get(state).get(action);
        assertNotEquals(newQValue, oldQValue);
    }

    @Test
    public void greedyActionForState_populatedActionList_choosesMaxValueAction() {
        // Set up
        MDPState<Long> state = new MDPState<>(0L);
        List<Action> actions = Arrays.asList(
                new DiscreteAction(0, 0),
                new DiscreteAction(0, 1),
                new DiscreteAction(0, 2)
        );

        Map<GameState, Map<Action, Double>> qTable = new HashMap<>();
        Map<Action, Double> actionMap = ImmutableMap.of(
                actions.get(0), 3.0,
                actions.get(1), 5.0,
                actions.get(2), 4.0
        );
        qTable.put(state, actionMap);

        TabularQLearning tabularQLearning = new TabularQLearning().withQTable(qTable);

        // Act
        Action result = tabularQLearning.greedyActionForState(state, actions);

        // Assert
        assertEquals(actions.get(1), result);
    }

    @Test
    public void randomAction_populatedActionList_choosesAnAction() {
        Random rng = new Random(1234L);

        for (int i = 0; i < 10; i++) {
            // Set up
            List<Action> actions = Arrays.asList(
                    new DiscreteAction(0, 0),
                    new DiscreteAction(0, 1),
                    new DiscreteAction(0, 2)
            );

            TabularQLearning tabularQLearning = (TabularQLearning) new TabularQLearning().withRandom(rng);

            // Act
            Action result = tabularQLearning.randomAction(actions);

            // Assert
            assertTrue(actions.contains(result));
        }
    }

    @Test
    public void tabularQLearning_gridWorldCliff_solvesOptimalPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 500;
        int playerId = 0;

        GridWorldTabularQLearningEngine engine = new GridWorldTabularQLearningEngine(
                new DummyEngineToServerConnection(playerId)
        );
        engine.setRng(new Random(1234L));
        engine.setEpsilon(0.5);
        engine.setTraining();
        engine.setLoadFromPrevious(false);

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Map<Action, Double>> policy = engine.getAlg().getQTable();

        // ====================================== Assert ======================================

        // Test along the optimal path. Could maybe add additional coordinates here as well to cover more of the policy.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Map<Action, Double> policyForState = policy.get(gameState);
            Optional<Action> maxAction = policyForState.keySet().stream().max(Comparator.comparingDouble(policyForState::get));

            assertTrue(maxAction.isPresent());
            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction.get();
            assertEquals(expectedActionForCoord.get(coord), maxCompassAction);
        }
    }

    @Test
    public void tabularQLearning_gridWorldCliffWithDouble_solvesOptimalPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 300;
        int playerId = 0;

        GridWorldTabularQLearningEngine engine = new GridWorldTabularQLearningEngine(
                new DummyEngineToServerConnection(playerId)
        );
        engine.setLoadFromPrevious(false);
        engine.setDoubleLearning(true);
        engine.setAlpha(0.05);
        engine.setRng(new Random(1234L));
        engine.setTraining();

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Map<Action, Double>> policy = engine.getAlg().getQTable();
        Map<GameState, Map<Action, Double>> policy2 = engine.getAlg().getQTable2();

        // ====================================== Assert ======================================

        // Test along the optimal path. Could maybe add additional coordinates here as well to cover more of the policy.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Map<Action, Double> policyForState = policy.get(gameState);
            Map<Action, Double> policy2ForState = policy2.get(gameState);
            Optional<Action> maxAction = policyForState.keySet().stream().max(Comparator.comparingDouble(x -> policyForState.get(x) + policy2ForState.get(x)));

            assertTrue(maxAction.isPresent());
            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction.get();
            assertEquals(expectedActionForCoord.get(coord), maxCompassAction);
        }
    }

    @Test
    public void tabularQLearning_stochasticGridWorldCliffWithDouble_solvesOptimalPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 300;
        int playerId = 0;
        double epsilon = 1.0;
        double epsilonMin = 0.1;
        double epsilonStep = 1.0 / 1000;
        double learningRate = 0.05;

        GridWorldTabularQLearningEngine engine = new GridWorldTabularQLearningEngine(
                new DummyEngineToServerConnection(playerId)
        );

        engine.setTraining();
        engine.setAlpha(learningRate);
        engine.setEpsilon(epsilon);
        engine.setEpsilonStep(epsilonStep);
        engine.setEpsilonMin(epsilonMin);
        engine.setDoubleLearning(true);
        engine.setRng(new Random(1234L));
        engine.setLoadFromPrevious(false);

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevelWind);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Map<Action, Double>> policy = engine.getAlg().getQTable();
        Map<GameState, Map<Action, Double>> policy2 = engine.getAlg().getQTable2();

        // ====================================== Assert ======================================

        // Test along the optimal path. Could maybe add additional coordinates here as well to cover more of the policy.
        Map<Coord, Set<CompassDirectionAction>> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelWindActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Map<Action, Double> policyForState = policy.get(gameState);
            Map<Action, Double> policy2ForState = policy2.get(gameState);

            // Determine best action according to policy.
            Optional<Action> maxAction = policyForState.keySet().stream().max(Comparator.comparingDouble(x -> policyForState.get(x) + policy2ForState.get(x)));

            assertTrue(maxAction.isPresent());
            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction.get();

            // Check that the best action determined by the algorithm is in the list of specified optimal actions for this coordinate.
            assertTrue(expectedActionForCoord.get(coord).contains(maxCompassAction));
        }
    }
}
