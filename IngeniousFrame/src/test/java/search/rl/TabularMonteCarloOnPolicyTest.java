package search.rl;

import com.google.common.collect.ImmutableMap;
import games.mdp.testData.MDPTestData;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldMonteCarloOnPolicyEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.monteCarlo.TabularMonteCarloOnPolicy;
import za.ac.sun.cs.ingenious.search.rl.util.EpisodeStep;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for TabularMonteCarloOffPolicy. All unit tests follow the AAA pattern as well as the naming convention described
 * here: https://osherove.com/blog/2005/4/3/naming-standards-for-unit-tests.html
 */
public class TabularMonteCarloOnPolicyTest {

    @Test
    public void chooseActionFromPolicy_stateInDistribution_addsEpisodeStepToSteps() {
        // Set up
        MDPState<Long> state = new MDPState<>(0L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        Map<Action, Double> policyForState = ImmutableMap.of(
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0), 0.5,
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1), 0.5
        );
        Map<GameState, Map<Action, Double>> policy = new HashMap<>();
        policy.put(state, policyForState);

        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withPolicy(policy);

        // Act
        Action chosenAction = alg.chooseActionFromPolicy(state);

        // Assert
        assertEquals(1, alg.getSteps().size());
        EpisodeStep expected = new EpisodeStep(state, chosenAction);
        assertEquals(expected, alg.getSteps().get(0));
    }

    @Test
    public void chooseBestActionFromPolicy_validState_returnsActionWithHighestProbability() {
        // Set up
        MDPState<Long> state = new MDPState<>(0L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        Action expectedAction = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1);

        Map<Action, Double> policyForState = ImmutableMap.of(
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0), 0.4,
                expectedAction, 0.6
        );
        Map<GameState, Map<Action, Double>> policy = new HashMap<>();
        policy.put(state, policyForState);

        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withPolicy(policy);

        // Act
        Action chosenAction = alg.chooseBestActionFromPolicy(state);

        // Assert
        assertEquals(expectedAction, chosenAction);
    }

    @Test
    public void setMostRecentReward_validReward_setsRewardForLastStep() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting01);
        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);
        alg.chooseActionFromPolicy(state);

        // Act
        alg.setMostRecentReward(new ScalarReward(123.0));

        // Assert
        ScalarReward reward = (ScalarReward)alg.getSteps().get(0).getReward();
        assertEquals(123.0, reward.getReward(), 0.0);
    }

    @Test
    public void performLazyPolicyInitialization_validState_createsValidDistribution() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting02);
        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withLogic(logic)
                .withRandom(rng);

        MDPState<Long> state = new MDPState<>(0L);

        // Act
        alg.performLazyPolicyInitialization(state);

        // Assert
        Collection<Double> actionProbabilities = alg.getPolicy().get(state).values();
        assertTrue(actionProbabilities.stream().allMatch(x -> x > 0.0 && x < 1.0));

        double probabilitySum = actionProbabilities.stream().reduce(0.0, Double::sum);
        assertEquals(1.0, probabilitySum, 0.0);
    }

    @Test
    public void update_validSteps_updatesQTableAndPolicy() {
        // Set up
        Random rng = new Random(1234L);
        MDPLogic<Long> logic = MDPLogic.fromSetting(MDPTestData.testMdpSetting03);
        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>>()
                .withRandom(rng)
                .withLogic(logic);
        MDPState<Long> state = new MDPState<>(1L);
        Action finalAction = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1);

        List<EpisodeStep> episodeSteps = Arrays.asList(
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(state, new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0)),
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(state, finalAction)
        );
        episodeSteps.get(0).setReward(new ScalarReward(-1));
        episodeSteps.get(1).setReward(new ScalarReward(-1));
        episodeSteps.get(2).setReward(new ScalarReward(-1));
        episodeSteps.get(3).setReward(new ScalarReward(10));
        alg.setSteps(episodeSteps);

        // Act
        alg.update();

        // Assert
        Action policyAction = alg.chooseBestActionFromPolicy(state);
        assertEquals(finalAction, policyAction);

        assertTrue(alg.getQTable().get(state).get(finalAction) != 0);
    }

    @Test
    public void averageReturns_validStateActionPair_returnsAverageReturn() {
        // Set up
        GameState state = new MDPState<>(0L);
        Action action = new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0);

        Map<GameState, Map<Action, Collection<Double>>> returns = new HashMap<>();
        Map<Action, Collection<Double>> returnsForState = ImmutableMap.of(
                action,
                Arrays.asList(2.0, 4.0, 8.0, 1.0)
        );
        returns.put(state, returnsForState);

        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<>();
        alg.setReturns(returns);

        // Act
        double average = alg.averageReturns(state, action);

        // Assert
        double expected = 3.75;
        assertEquals(expected, average, 0.0);
    }

    @Test
    public void stateActionOccursInPreviousStep_stateActionDoesOccurLater_returnsTrue() {
        // Set up
        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<>();

        List<EpisodeStep> episodeSteps = Arrays.asList(
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(new MDPState<>(1L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0)),
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(new MDPState<>(1L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1))
        );
        alg.setSteps(episodeSteps);

        // Act
        boolean result = alg.stateActionOccursInPreviousStep(
                new MDPState<>(0L),
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1),
                1
        );

        // Assert
        assertTrue(result);
    }

    @Test
    public void stateActionOccursInPreviousStep_stateActionDoesNotOccur_returnsFalse() {
        // Set up
        TabularMonteCarloOnPolicy<MDPState<Long>, MDPLogic<Long>> alg = new TabularMonteCarloOnPolicy<>();

        List<EpisodeStep> episodeSteps = Arrays.asList(
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(new MDPState<>(1L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 0)),
                new EpisodeStep(new MDPState<>(0L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1)),
                new EpisodeStep(new MDPState<>(1L), new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1))
        );
        alg.setSteps(episodeSteps);

        // Act
        boolean result = alg.stateActionOccursInPreviousStep(
                new MDPState<>(1L),
                new DiscreteAction(MDPLogic.DEFAULT_PLAYER_TO_ACT, 1),
                1
        );

        // Assert
        assertFalse(result);
    }

    @Test
    public void tabularMonteCarloOnPolicy_gridWorldCliff_solvesPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 20;
        int playerId = 0;

        GridWorldMonteCarloOnPolicyEngine engine = new GridWorldMonteCarloOnPolicyEngine(
                new DummyEngineToServerConnection(playerId)
        );
        engine.setDeterministic();
        engine.getAlg().withEpsilon(0.4);

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Map<Action, Double>> policy = engine.getAlg().getPolicy();

        // ====================================== Assert ======================================

        // Test along the "safe" path.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Map<Action, Double> policyForState = policy.get(gameState);
            Optional<Action> maxAction = policyForState.keySet().stream().max(Comparator.comparingDouble(policyForState::get));

            assertTrue(maxAction.isPresent());
            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction.get();
            assertTrue(expectedActionForCoord.get(coord).equals(maxCompassAction));
        }
    }
}
