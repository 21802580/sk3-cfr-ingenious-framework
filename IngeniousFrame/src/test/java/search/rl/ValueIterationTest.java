package search.rl;

import com.esotericsoftware.minlog.Log;
import games.mdp.testData.MDPTestData;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.games.mdp.MDPRewardEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.util.MDPSetting;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldValueIterationEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.games.mdp.MDPFinalEvaluator;
import za.ac.sun.cs.ingenious.games.mdp.MDPLogic;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.dp.ValueIteration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ValueIterationTest {
    @Test
    public void solve_deterministicMDP_choosesMaxValueAction() {
        // Set up
        MDPSetting mdpSetting = MDPTestData.testMdpSetting01;

        ArrayList<MDPState<Long>> states = new ArrayList<>();
        for (int i = 0; i < mdpSetting.getStates(); i++) {
            states.add(new MDPState<>((long) i));
        }
        MDPState<Long> state = states.get(0);

        ValueIteration<Long, MDPLogic<Long>, MDPRewardEvaluator<Long>> valueIteration = new ValueIteration<>();

        valueIteration.setStates(states);
        valueIteration.setLogic(MDPLogic.fromSetting(mdpSetting));
        valueIteration.setEvaluator(MDPRewardEvaluator.fromSetting(mdpSetting));
        valueIteration.initializeValueTable();
        valueIteration.solve();

        // Act
        Action result = valueIteration.chooseActionForState(state.getGameState());

        // Assert
        assertEquals(new DiscreteAction(0, 1), result);
    }

    @Test
    public void valueIteration_gridWorldCliff_solvesOptimalPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 1;
        int playerId = 0;

        GridWorldValueIterationEngine engine = new GridWorldValueIterationEngine(
                new DummyEngineToServerConnection(playerId)
        );

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Action> policy = new HashMap<>();
        for (MDPState<GridWorldState> state : engine.getAlg().getStates()) {
            policy.put(state.getGameState(), engine.getAlg().chooseActionForState(state.getGameState()));
        }

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicy(referee.getCurrentState(), policy));

        // ====================================== Assert ======================================

        // Test along the optimal path.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Action maxAction = policy.get(gameState);

            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction;
            assertEquals(expectedActionForCoord.get(coord), maxCompassAction);
        }
    }
}
