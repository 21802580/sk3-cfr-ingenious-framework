package search.rl;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.core.util.move.DiscreteAction;
import za.ac.sun.cs.ingenious.core.util.reward.ScalarReward;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldTabularSarsaEngine;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;
import za.ac.sun.cs.ingenious.search.rl.sarsa.TabularSarsa;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class TabularSarsaTest {

    @Test
    public void update_validSARS_updatesQTableValue() {
        // Set up
        TabularSarsa tabularSarsa = new TabularSarsa();
        GameState state = new MDPState<>(0L);
        Action action = new DiscreteAction(0, 1);
        ScalarReward reward = new ScalarReward(10.0);
        GameState newState = new MDPState<>(1);

        List<Action> actionsAvailable = Arrays.asList(action, new DiscreteAction(0, 0), new DiscreteAction(0, 2));

        double oldQValue = 0.0;

        // Act
        tabularSarsa.update(state, action, reward, newState, actionsAvailable);

        // Assert
        double newQValue = tabularSarsa.getQTable().get(state).get(action);
        assertNotEquals(newQValue, oldQValue);
    }

    @Test
    public void greedyActionForState_populatedActionList_choosesMaxValueAction() {
        // Set up
        MDPState<Long> state = new MDPState<>(0L);
        List<Action> actions = Arrays.asList(
                new DiscreteAction(0, 0),
                new DiscreteAction(0, 1),
                new DiscreteAction(0, 2)
        );

        Map<GameState, Map<Action, Double>> qTable = new HashMap<>();
        Map<Action, Double> actionMap = ImmutableMap.of(
                actions.get(0), 3.0,
                actions.get(1), 5.0,
                actions.get(2), 4.0
        );
        qTable.put(state, actionMap);

        TabularSarsa tabularSarsa = new TabularSarsa().withQTable(qTable);

        // Act
        Action result = tabularSarsa.greedyActionForState(state, actions);

        // Assert
        assertEquals(actions.get(1), result);
    }

    @Test
    public void randomAction_populatedActionList_choosesAnAction() {
        Random rng = new Random(1234L);

        for (int i = 0; i < 10; i++) {
            // Set up
            List<Action> actions = Arrays.asList(
                    new DiscreteAction(0, 0),
                    new DiscreteAction(0, 1),
                    new DiscreteAction(0, 2)
            );

            TabularSarsa tabularSarsa = new TabularSarsa().withRandom(rng);

            // Act
            Action result = tabularSarsa.randomAction(actions);

            // Assert
            assertTrue(actions.contains(result));
        }
    }

    @Test
    public void tabularSarsa_gridWorldCliff_solvesPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 100;
        int playerId = 0;

        GridWorldTabularSarsaEngine engine = new GridWorldTabularSarsaEngine(
                new DummyEngineToServerConnection(playerId)
        );
        engine.getAlg().withRandom(new Random(1234L)).withEpsilon(0.4);

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<GameState, Map<Action, Double>> policy = engine.getAlg().getQTable();
//        engine.getAlg().printQTable();

        // ====================================== Assert ======================================

        // Test along the optimal path. Could maybe add additional coordinates here as well to cover more of the policy.
        Map<Coord, Set<CompassDirectionAction>> expectedActionForCoord = ReinforcementLearningTestData.getExpectedOnPolicyCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<GameState> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Map<Action, Double> policyForState = policy.get(gameState);
            Optional<Action> maxAction = policyForState.keySet().stream().max(Comparator.comparingDouble(policyForState::get));

            assertTrue(maxAction.isPresent());
            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction.get();
            assertTrue(expectedActionForCoord.get(coord).contains(maxCompassAction));
        }
    }
}
