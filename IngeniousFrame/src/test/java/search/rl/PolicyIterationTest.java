package search.rl;

import com.esotericsoftware.minlog.Log;
import org.junit.Test;
import search.rl.testData.ReinforcementLearningTestData;
import za.ac.sun.cs.ingenious.core.Action;
import za.ac.sun.cs.ingenious.core.GameState;
import za.ac.sun.cs.ingenious.core.PlayerRepresentation;
import za.ac.sun.cs.ingenious.core.configuration.MatchSetting;
import za.ac.sun.cs.ingenious.core.exception.IncorrectSettingTypeException;
import za.ac.sun.cs.ingenious.core.exception.MissingSettingException;
import za.ac.sun.cs.ingenious.core.network.game.DummyEngineToServerConnection;
import za.ac.sun.cs.ingenious.core.util.misc.Coord;
import za.ac.sun.cs.ingenious.core.util.misc.LocalPlayer;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldReferee;
import za.ac.sun.cs.ingenious.games.gridWorld.GridWorldState;
import za.ac.sun.cs.ingenious.games.gridWorld.engines.GridWorldPolicyIterationEngine;
import za.ac.sun.cs.ingenious.games.gridWorld.util.GridWorldPolicyStringBuilder;
import za.ac.sun.cs.ingenious.games.mdp.MDPState;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PolicyIterationTest {
    @Test
    public void policyIteration_gridWorldCliff_solvesOptimalPolicy() throws MissingSettingException, IncorrectSettingTypeException {
        // ====================================== Set up ======================================

        int matchCount = 1;
        int playerId = 0;

        GridWorldPolicyIterationEngine engine = new GridWorldPolicyIterationEngine(
                new DummyEngineToServerConnection(playerId)
        );

        // Create the player representation for our agent.
        PlayerRepresentation[] players = new PlayerRepresentation[] { new LocalPlayer(engine) };

        MatchSetting matchSetting = new MatchSetting();
        matchSetting.setNumMatches(matchCount);
        matchSetting.setSetting("levelContent", ReinforcementLearningTestData.cliffLevel);

        // Instantiate and run the referee.
        GridWorldReferee referee = new GridWorldReferee(matchSetting, players);
        referee.disposeUI();

        // ====================================== Act ======================================

        referee.run();

        Map<MDPState<GridWorldState>, Action> policy = engine.getAlg().getPolicy();

        // Quick and dirty map from Map<MDPState, Action> to Map<GridWorldState, Action>.
        Map<GameState, Action> mappedPolicy = policy.keySet()
                .stream()
                .collect(Collectors.toMap(MDPState<GridWorldState>::getGameState, policy::get));

        Log.info(GridWorldPolicyStringBuilder.buildUsingPolicy(referee.getCurrentState(), mappedPolicy));

        // ====================================== Assert ======================================

        // Test along the optimal path.
        Map<Coord, CompassDirectionAction> expectedActionForCoord = ReinforcementLearningTestData.getOptimalCliffLevelActions(playerId);

        // Assert that, for each of the coordinates above, the correct actions would be chosen.
        for (Coord coord : expectedActionForCoord.keySet()) {
            Optional<MDPState<GridWorldState>> foundGameState = policy.keySet()
                    .stream()
                    .filter(x -> ((GridWorldState) x.getGameState()).findPlayerCoords(playerId).equals(coord))
                    .findFirst();

            // If game state for this coordinate is not in policy, test should fail.
            assertTrue(foundGameState.isPresent());

            GameState gameState = foundGameState.get();
            Action maxAction = policy.get(gameState);

            CompassDirectionAction maxCompassAction = (CompassDirectionAction) maxAction;
            assertEquals(expectedActionForCoord.get(coord), maxCompassAction);
        }
    }
}
