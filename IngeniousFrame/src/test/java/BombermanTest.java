import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.awt.Point;

import za.ac.sun.cs.ingenious.core.util.misc.CompassDirection;
import za.ac.sun.cs.ingenious.core.util.move.CompassDirectionAction;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMBoard;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.BMLogic;
import za.ac.sun.cs.ingenious.games.bomberman.gamestate.moves.PlaceBombAction;
import za.ac.sun.cs.ingenious.games.bomberman.network.UpdateBombsMessage;

public class BombermanTest {

    BMLogic logic = BMLogic.defaultBMLogic;
	
// TODO - sort out these commented out tests - see issue 200
//	@Test
//	public void createBoard(){
//		BMBoard board = new BMBoard();
//		board.printPretty();
//		for( int i = 0; i < 4; i++){
//			assertEquals((char)('A'+i),board.getCharAt(board.getPlayer(i).getPosition()));
//		}
//	}
	
	
//	@Test
//	public void validMoves(){
//		BMBoard board = new BMBoard();
//		
//		assertEquals(3,board.generateMoves(0, 0).size());
//		
//		
//		board.makeMove(new MoveMove(0, 1));
//		board.printPretty();
//		assertTrue(new Point(2,1).equals(board.getPlayer(0).getPosition()));
//		assertEquals('A', board.getCharAt(board.getPlayer(0).getPosition()));
//		
//		
//		board.makeMove(new PlaceBombMove(0));
//		board.printPretty();
//		assertEquals('a', board.getCharAt(board.getPlayer(0).getPosition()));
//		assertEquals(1,board.getBombs().size());
//		
//		board.undoMove();
//		board.printPretty();
//		assertTrue(new Point(2,1).equals(board.getPlayer(0).getPosition()));
//		assertEquals('A', board.getCharAt(board.getPlayer(0).getPosition()));
//		
//		board.undoMove();
//		board.printPretty();
//		assertTrue(new Point(2,1).equals(board.getPlayer(0).getPosition()));
//		assertEquals('A', board.getCharAt(board.getPlayer(0).getPosition()));
//		
//	}
	
	@Test
	public void boardCreatedCorrect(){
		BMBoard board = new BMBoard(createTestBoard(), 4, false, false);
		
		//Test 1,3 -> B
		Point p = new Point(1,3);
		assertTrue(board.isPlayer(p));
		assertFalse(board.isBomb(p));
		
        /* TODO: Removed 'c' and '8' from createTestBoard since no longer supported.  Still need to test this stuff:
		//Test 2,3 -> c
		p = new Point(2,3);
		assertTrue(board.isPlayer(p));
		assertTrue(board.isBomb(p));
		
		//Test 3,3 -> 8
		p = new Point(3,3);
		assertFalse(board.isPlayer(p));
		assertTrue(board.isBomb(p));
   		assertEquals(8, board.getBombAt(p).getTimer());
        */
		
		//Test 4,3 -> +
		p = new Point(4,3);
		assertFalse(board.isPlayer(p));
		assertFalse(board.isBomb(p));
		assertTrue(board.isCradle(p));
		
		//Test 4,3 -> +
		p = new Point(5,3);
		assertFalse(board.isPlayer(p));
		assertFalse(board.isBomb(p));
		assertFalse(board.isCradle(p));
		assertTrue(board.isWall(p));
	}
	
	
	@Test
	public void canMove(){
		BMBoard board = new BMBoard(createTestBoard(), 4, false, false);
		Point p = board.getPlayer(0).getPosition();
		logic.makeMove(board, new CompassDirectionAction(0,CompassDirection.E));
		assertTrue(1.0 == p.distance(board.getPlayer(0).getPosition()));
	}
	
	
	@Test
	public void canBombTwice(){
		BMBoard board = new BMBoard(createTestBoard(), 4, false, false);
		logic.makeMove(board, new PlaceBombAction(0));
		assertFalse(logic.validMove(board, new PlaceBombAction(0)));
	}
	
    /* 
    TODO - build an alternative test for this - setAlive is no longer publicly accessible, and should not be - see issue 181
	@Test
	public void testTerminalThroughPlayerDead(){
		BMBoard board = new BMBoard();
		board.getPlayer(0).setAlive(false);
		board.getPlayer(1).setAlive(false);
		board.getPlayer(2).setAlive(false);
		assertTrue(logic.isTerminal(board));
	} */
	
	@Test
	public void canDestroyCradleAndPlayer(){
		BMBoard board = new BMBoard(createTestBoard(), 4, false, false);
		logic.makeMove(board,new CompassDirectionAction(0,CompassDirection.E));
		logic.makeMove(board,new PlaceBombAction(0));
		logic.makeMove(board,new CompassDirectionAction(0,CompassDirection.W));
		
		assertTrue(board.isCradle(new Point(3,1)));
		assertTrue(board.isCradle(new Point(4,1)));
		assertTrue(board.getPlayer(0).isAlive());
		board.printPretty();
		for(int i = 0; i < 8; i++){
			logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), board);
		}
		board.printPretty();
		assertFalse(board.getPlayer(0).isAlive());
		assertFalse(board.isPlayer(new Point(1,1)));
		assertFalse(board.isCradle(new Point(3,1)));
		assertTrue(board.isCradle(new Point(4,1)));
	}
	
	@Test
	public void cannotWalkOnBomb(){
		BMBoard board = new BMBoard(createTestBoard(), 4, false, false);
		logic.makeMove(board,new PlaceBombAction(0));
		logic.makeMove(board,new CompassDirectionAction(0,CompassDirection.E));
		assertFalse(logic.validMove(board,new CompassDirectionAction(0,CompassDirection.W)));
	}
	
	@Test
	public void canDie(){
		char[][] cboard = createEmptyBoard();
		cboard[3][1] = 'A';
		cboard[19][19] = 'B';
		cboard[18][19] = 'C';
		cboard[17][19] = 'D';
		
		BMBoard board = new BMBoard(cboard, 4, false, false);
        // It seems legal moves here should be:
        // idle, trigger, up, down, right,
        // so this assertion seems wrong
		// assertEquals(2, logic.generateMoves(0, board).size());
		logic.makeMove(board, new PlaceBombAction(0));
		logic.makeMove(board, logic.generateActions(board, 0).get(1));
		logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), board);
		logic.applyPlayedMoveMessage(new UpdateBombsMessage(-1, false), board);
		assertFalse(board.getPlayer(0).isAlive());
	}
	
	@Test
	public void testSimilarity(){
		char[][] cboard = createEmptyBoard();
		cboard[3][1] = 'A';
		cboard[19][19] = 'B';
		cboard[18][19] = 'C';
		cboard[17][19] = 'D';
		
		BMBoard board = new BMBoard(cboard, 4, false, false);
        assertTrue(board.similar(board));
        assertFalse(board.similar(null));
		BMBoard tboard = new BMBoard(createTestBoard(), 4, false, false);
        assertFalse(board.similar(tboard));
        assertFalse(tboard.similar(board));
        BMBoard clonedBoard = (BMBoard) board.deepCopy();
        assertTrue(clonedBoard.similar(board));
        assertTrue(board.similar(clonedBoard));
		logic.makeMove(board, new PlaceBombAction(0));
        assertFalse(board.similar(clonedBoard));
        assertFalse(clonedBoard.similar(board));
        // TODO - expand tests to explicitly cover similarity tests for players, nodes, and bombs
	}
	
	
    /*
    TODO: fix this test - currently not compiling!
	@Test
	public void testMCTS(){
		
		char[][] cboard = createEmptyBoard();
		cboard[15][18] = 'A';
		cboard[16][19] = '1';
		
		cboard[1][18] = 'B';
		cboard[2][18] = 'C';
		cboard[3][18] = 'D';
		BMBoard board = new BMBoard(cboard);
		BMEngine eng = new BMEngineMCTS(board, 'A', 0);
		
		eng.MCTSGenMove(0, 0);
		assertTrue(board.getPlayer(0).isAlive());
		
	}
    */
	
	public static char[][] createEmptyBoard(){
		char[][] board = new char[21][21];
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board.length; j++) {
				if ((i % 2 == 0 && j % 2 == 0) || i % 20 == 0 || j % 20 == 0) {
					board[i][j] = '#';
				} else {
					board[i][j] = ' ';
				}
			}
		}
		return board;
	}
	
	public static char[][] createTestBoard(){
        char[][] board = createEmptyBoard();
        board[1][1] = 'A';
        board[1][3] = '+';
        board[1][4] = '+';
        
        board[3][1] = 'B';
        board[3][2] = ' ';
        board[3][3] = ' ';
        board[3][4] = '+';
        board[3][5] = '#';
        
        board[18][19] = 'C';
        board[19][19] = 'D';
        
        return board;
	}
}
