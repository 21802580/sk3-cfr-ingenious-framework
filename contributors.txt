Marc Christoph - contributions to Lines of Action, parallel MCTS algorithms, and integration with Distributed Tournament Engine
Chris Coetzee - contributions to networking code, development of command-line scripts, and integration with Distributed Tournament Engine
Nicole du Toit -  contributions to parallel MCTS algorithms
Jako Groenewald - contributions to parallel MCTS algorithms
Cornelia Inggs -  contributions to parallel MCTS algorithms
Steffen Jacobs - contributions to reinforcement learning
Michael Krause - intern on project 2016 (general framework aspects, Domineering, shadow Tic-Tac-Toe, ISMCTS)
Steve Kroon - project co-ordinator
Steven Labrum - original Ingenious implementation for 2016 Stellenbosch University Computer Science Honours project
Karen Laubscher - contributions to parallel MCTS algorithms
Nicholas Robinson - contributions to functional/persistent data structures for search
Rudolf Stander - contributions to Othello
Stephan Tietz - intern on project 2016 (general framework aspects, Bomberman, Carcassonne)
Elan van Biljon - adding early functionality for reinforcement learning
Joshua Wiebe - intern on project 2016 (general framework aspects, mnk-game, card games)
